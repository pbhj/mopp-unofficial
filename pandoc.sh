#!/bin/bash

# convert downloaded html to markdown using pandoc

cd scraped-html || exit 

mddir="../html-to-md"

for i in *
    # --quiet will suppress warnings
    do pandoc -f html -t markdown -i "$i" -o "$mddir"/"$i".md &
done