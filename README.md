# MoPP Unofficial Pronoun project
## Actions to seed Gitlab

+ Updates
+ [Acquire html] by stripping it from live files on web.
+ [Convert to Markdown] using Pandoc
+ [Find pronouns] doing basic parsing or in VSCode
+ [Push to git repo] on Gitlab

### Updates

Most recent first:

####October 2021

`getsections.sh` is erroring again as the page structure appears to have changed again, I'm going to need to dynamically calculate the XPath, so change pending there whilst I think about how to do it so as to be robust, and whether to bother for this project. 

Again, updates appear to be going well, I've added a screenshot for the results of a manual check on the Sections that have the most pronouns remaining:

    section-14-the-application_part.html.md, 25, 14.65 “his capabilities”
    section-46-patentee-s-application-for-entry-in-register-that-licences-are-available-as-of-right_part.html.md, 25, 46.16 “he should intervene”; 46.27 “he has power”; 46.39 “he thought fit”
    section-1-patentability_part.html.md, 24, none
    section-18-substantive-examination-and-grant-or-refusal-of-patent_part.html.md, 15, 18.06 “allocated to him”
    section-123-rules_part.html.md, 14, 123.17 “he requires evidence”; 123.37 “protect his invention”; 123.45 “comptroller himself”; 

Seems like these last ones are mainly judgements as to whether or not to refer to the Comptroller, currently male, as "he". Personally I'd use "they" in order to avoid making changes each time the Comptroller changes. A couple of those above might be within quotes, it's hard to tell readily, this was just a quick look.

####September 2021

Latest test using `getsections.sh` resulted in an error so I've had to refactor slightly, the base HTML being parsed had changed and so the XPATH needed adjusting. A sanity check has been addd to make sure the $sections variable is a reasonable size before it is written out to a new file. A little more work to  find the div that holds the sections list would be useful.

As for updates to MoPP itself, there's been some progress. The most changed Section is S.60 where several changes to a section on "warehousemen" and "draftsmen" have improved the counts for that section. The worst section now is Section 123 with 53 pronouns needing addressing -- mostly in a section talking about the Secretary of State and referring to them as "he". 

Good progress.

### Acquire html

Contents of MoPP are found on 

https://www.gov.uk/guidance/manual-of-patent-practice-mopp

Each page has a general search box, then a breadcrumb for the page in an ol.breadcrumb-trail and then div.manual-body. The Xpaths of these two are:

    /html/body/div[6]/main/div/ol
    //*[@id="content"]

I'm guessing that the ol is made by the web framework on the gov.uk pages, but it's easy enough to reproduce afterwords so I'll take pages in the Manual and extract the div="content" and write that to markdown using that to populate the git.

Any page can be acquired by using wget following the advice in this [StackOverflow question](https://stackoverflow.com/questions/46209747/wget-fetch-multiple-xpaths-from-same-source-using-only-one-http-request):

    #!/bin/bash

    name_query="html/body/div[3]/div/div[1]/div[3]/div[1]/h1/text()"

    content=$(wget -O - "https://example.com/section-1/name-1/financial-data/")

    # Use xargs to TRIM result.
    header=$($content | xmllint --html --xpath "$name_query" - 2>/dev/null | xargs)

    echo $content | 
        xmllint --html --xpath '//*[@id="financial-data"]/div/table/tbody' - 2>/dev/null |
        xmlstarlet ed --subnode "/tbody/tr" --type elem -n td -v "$header" >> /Applications/parser/output.txt

For our purposes we just need, with changes to var names (as the id of the XML node we're looking at is "content"): 

    #!/bin/bash
    # convert page

    name_query="//*[@id="content"]"

    wholepage=$(wget -O - "https://www.gov.uk/guidance/manual-of-patent-practice-mopp/manual-of-patent-practice-introduction" --referer="https://www.gov.uk/guidance/manual-of-patent-practice-mopp" --user-agent="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0; pbhj")

    # Use xargs to TRIM result.
    partpage=$($wholepage | xmllint --html --xpath "$name_query" - 2>/dev/null | xargs)

#### Getting a list of sections

The list of sections, [sections.txt] is acquired from the list of links shown on the [Manual](https://www.gov.uk/guidance/manual-of-patent-practice-mopp) front page. One links has an error and so it starts "-section-4A" instead of "section-4A" but the page is at the erroneously named url, so it's of no real consequence, it should be fixed and a redirect permanent (ie a 301) added to the server. We'll see how long that takes to happen.

#### Using the list of sections

    ./convertpages

Downloads the sections using `wget`, does some tidying up and saves the file. The file is then converted to markdown as that makes for easier textual comparison and easier searching when looking for textual changes/errors and such.

## Convert to markdown
### Command lines

Here are some command lines used:

    pandoc -f html -t markdown -i ./section-14-the-application__breadcrumb+manual-body.html -o ./section-14-the-application__breadcrumb+manual-body.md

    pandoc -f markdown -t html -i ./section-14-the-application__breadcrumb+manual-body_markdown.md 
    pandoc -f markdown -t html -i ./section-14-the-application__breadcrumb+manual-body_markdown.md ./section-14-the-application__breadcrumb+manual-body_markdown.html
    pandoc -f markdown -t html -i ./section-14-the-application__breadcrumb+manual-body_markdown.md -o ./section-14-the-application__breadcrumb+manual-body_markdown.html
    pandoc -f markdown-auto_identifiers -t html -i ./section-14-the-application__breadcrumb+manual-body_markdown.md -o ./section-14-the-application__breadcrumb+manual-body_markdown.html

BASH One-liner to convert all files in directory to markdown. Does not escape the filenames and so will not handle special characters in names, eg spaces (note & here backgrounds the jobs which might make things run quicker):

    mkdir="../html-to-md_$timenow"; for i in *; do pandoc -f html -t markdown -i "$i" -o "$mddir"/"$i".md &; done

## Find pronouns

Having looked at a few instances there are broadly 3 different occurences of pronouns in the Manual:

+ In quotes of Sections of The Act,
+ In quotes of case reports,
+ In general prose.

However, some instances are not as clear cut as others.

Consider this
    ### 10.07 {https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-10-handling-of-application-by-joint-applicants/#ref10-07}
    The hearing officer declined to make any direction under s.8 or s.12 but, under s.10, directed the second applicant to assign **his** interest in the UK patent application to the first, in return for a free non-assignable, non-revocable licence excluding any right to
    sub-licence.

here, "their" would be a reasonable replacement even despite the passage clearly referring to a situation in which a recognised person is being mentioned. The context is specific but the statement is substantially general. This then becomes stylistic.

Indeed, one might choose to be radical and enter they/them/their in place of quoted pronouns in texts where it was judged the original text should itself be edited, eg quotes of the Act. No doubt in the future these will also be edited in this way and so this would probably be more of a pre-emption.

Some sections have complex text:

    ### 100.01

    s.60(1)(c) is also relevant

    This section relates to the burden of proof with regard to whether a
    product was obtained by a patented process. It is relevant to
    infringement proceedings since a person may infringe a patent for a
    process if **he** disposes of, offers to dispose of, uses or imports any product obtained directly by means of that process or keeps any such product whether for disposal or otherwise.

requires a lot of endings to be adjusted to ensure agreement with the pronoun used. LanguageTool (a grammar assistant that does lexical parsing and can autofix) may be useful for such cases.

This section is complicated by the language being a reference back to the act and that "proceedings before them" seems stilted and implies plural presiding officers:

    ### 107.01

    This section provides for the award of costs (expenses in Scotland) by the comptroller *in proceedings before him*. It also provides for
    enforcement of orders made by the comptroller awarding costs, and allows the comptroller to require security for the costs of certain proceedings *before him*. (editor:my emphasis added)

the emphasised parts can, in my opinion, be excised to useful effect.

#### Example changes

    he has -> they have
    his memory -> their memory
    his officers -> their officers
    apparent to him -> apparent to them (alternative: apparent)
    giving him -> giving them
    he does not -> they do not
    he may -> they may
    his group -> their group
    he thinks fit -> they think fit
    his opinion -> the comptroller's opinion
    he is -> they are
    he is the applicant or his agent -> they are the applicant or the applicant's agent
    do it himself -> do it theirself [[https://cliffhays.weebly.com/blog/theirself-or-themself](see for example this discussion)]
    to the party himself -> to the party themself
    he exercises -> they exercise
    He -> They
    decided by him -> that they decide

Section 123.05.7 = most occurences of masculine pronouns in a para.!

### Other fixes?

+ comptroller -> Comptroller ?
+ dash -> mdash/ndash
+ [118.18](#ref:118-18) -> [118.18](#ref118-18)

### Some regexes
#### Regexes for finding pronouns in normal text

This finds the pronouns when they're not within a blockquote (in markdown that starts with ">"; these are used in the Manual to quote sections of caselaw reports):

    ^(?!>).*\b(he|him|himself|his|she|her|herself|hers)\b

Actually, I've noticed that quoted sections of the Act are indented by exactly 2 spaces in all the files I've manually examined, so this regex will include lines that start with 2-spaces within the negative lookahead:

    ^(?!(>|\s{2})).*\b(he|him|himself|his|she|her|herself|hers)\b

You can use this in a search in your IDE or whatever.

#### Regexes for finding reference errors

This finds lines in the markdown file that have the wrong reference, eg "### 14.174.1 {#section-69}":

    ; 2 number references, eg 1.01
    ^(\#+)\s([0-9]+)\.([0-9]+)\s\{\#(?!ref\2-\3).*$
    ; 3 number references, eg 1.01.1
    ^(\#+)\s([0-9]+)\.([0-9]+)\.([0-9]+)\s\{\#(?!ref\2-\3-\4).*$

The replacement will then be, eg "### 14.174.1 {#ref14-174-1}":

    ; 2 number references, eg 1.01
    \1 \2.\3 {#ref\2-\3}
    ; 3 number references, eg 1.01.1
    \1 \2.\3.\4 {#ref\2-\3-\4}

Note in  Section 14 of the manual this fixed 59 x 2-part references and 19 x 3-part references. Replacing all is >300 errors.

#### More errors

The reference in this section:

    ### 110.03

    s\. 130(1) is also relevant.

    It is an offence, except as provided for in subsections (3) and (4),
    [see 110.07 and 110.08](#ref110-07-110-08), for a person to falsely
    represent that anything disposed of by him for value is a patented
    product. [...]

has been munged and there are probably other similar errors.

### Using the regex

These regex were tested in the Kate editor search-replace boxes but I'm considering using the python app [Massedit](https://github.com/elmotec/massedit) (found via [SO](https://stackoverflow.com/questions/4427542/how-to-do-sed-like-text-replace-with-python "of course!") as a sed replacement rather than trying to get this to work in sed. Too many options!

### Statistics

VS Code's search when run with the second regex above, for finding pronouns, gave 1236 results when first tested [edit: 696 as of March 2021].

As a check on this from the html-to-md directory I ran:

    grep -i -E --regexp="\b(he|him|himself|his|she|her|herself|hers)\b" --count -r | sort -V | sed 's/:/,/' >> ../section-pronoun-counts.csv

which gives a CSV file (Excel compatible) with the name of each section followed by the number of pronouns found. The total should be larger than the total found in VSCode as grep is using a naive regex that captures all the gendered pronouns in all sections and so includes the quoted parts (from caselaw and from The Patents Act). To get the number you can open it in Excel/LibreOffice Sheets/whatever or you can do this:

    cut -d, -f2 section-pronoun-counts.csv | paste -s -d+ | bc --

(I wasn't aware of paste but found this line on a [blog post](https://www.miskatonic.org/2017/02/27/command-line-column-sum/).)

which takes the second field (cut) and passes that to paste which puts all the fields on one line with a "+" between them, and then passes it to bc which treats that line as a maths query and gives the answer. For the 2020-11-18 run the answer here is 1756, which acts as a sanity check. A double check might be to find all the quoted pronouns and see if things don't add up ... they didn't for me, the problem was I had forgotten to do a case-insensitive grep (grep -i).

#### logging

Do count and save a dated copy of csv:

    cd html-to-md; grep -i -E --regexp="\b(he|him|himself|his|she|her|herself|hers)\b" --count -r | sort -V | sed 's/:/,/' >> ../section-pronoun-counts.csv; cp ../section-pronoun-counts.csv ../logs/$(date -Iminutes)_section-pronoun-counts.csv; cd ..

Do total count and log total:

    cut -d, -f2 section-pronoun-counts.csv | paste -s -d+ | bc -- | tee total-pronouns-rough.txt | ts | tee -a logs/section-pronoun-counts.log

Note `ts` adds a timestamp but `printf '%(%F %T)T\n'` could be used without calling another program.

## Git commands

To clone the repo into a subdirectory called mopp-unofficial:

    git clone https://gitlab.com/pbhj/mopp-unofficial.git

To get changes and merge them with your local code (you may only want to git fetch, this [Stackoverflow answers details the differences between git fetch/git pull](https://stackoverflow.com/questions/292357/what-is-the-difference-between-git-pull-and-git-fetch?rq=1)):

    git pull

To add all files in current dir to the next commit:

    git add ./*

To upload the "commit", the changes that have been made, merging them in to the master copy; for which you need to be authorised (your app should ask for your credentials):

    git commit; git push # you'll be asked for your gitlab credentials unless you've set up use of public keys

To offer changes to the project admins you can make a "pull request" which means to suggest code that the project admin can then "pull" in to the project. You need to have write access to the repo to do this. Or you can do it through the gitlab.com interface either on the website or by making a branch in your own repo and then using the website to initiate the pull request:

    ???

To store your credentials you can search for information on "git config credential.helper", one helper is "manager", another is "store"; this is operating system specific.

    ???

## Other useful info

https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html%23raw-tex#Verbatim_(code)_blocks

I'm using shellcheck like so, for checking my BASH scripts are well-formed:

    `shellcheck -s bash -S style ./*.sh`
