#!/bin/bash
# getsections.sh
# get all the sections from the URLs linked in the contents page so they can be used to download the manual pages; 
#
# 20211017 xpath appears to be wrong due to on page changes, /html/body/div[4]/main/div/article/div/ul
# 20210327 rewrite to not have dates in the filenames, also using different xpath
#
# TODO: 20211017 make more robust to handle xpath changes, maybe extract the xpath from the page somehow??
# TODO: combine with convertpage to do in a one-er. 
# TODO: this doesn't get all the links needed, just the ones that match the section-* naming.

manualurl="https://www.gov.uk/guidance/manual-of-patent-practice-mopp"

## XPATHs
## content of the tag with id="content"
# name_query="//*[@id=\"content\"]" 
## all anchor's href values 
# name_query="//a/@href" 
## specific section tags that should include sections not called "section-.*", the li[n]/a are the different section links (including intro, etc.)

#name_query="/html/body/div[6]/main/div/article/div/ol/li/a/@href" # expired on 20211017
name_query="/html/body/div[4]/main/div/article/div/ul/li/a/@href"


wholepage=$(wget -O - $manualurl --referer="https://www.gov.uk/guidance/manual-of-patent-practice-mopp" --user-agent="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0; pbhj")

# #debug
# echo "$wholepage" >> ${manualsection}_whole.html

# pipe xpath output through grep to get sections (including intro, and all)
# pass through sed (previously used grep) to trim head and tail of each line
sections=$(echo "$wholepage" | xmllint --html --xpath "$name_query" - 2>/dev/null | sed -e s,href=\"\/guidance\/manual-of-patent-practice-mopp\/,, -e s,\",, | uniq -u)

# #backup before write, then write out $sections as txt file
# sectionsize=$#sections 
# if [ $sectionsize -lt 100 ]
# then
    printf "\n\$sections has content size %d" ${#sections}
    mv --backup=numbered sections.txt sections.old
    sectionsfile="sections.txt"
    echo "$sections" >> "$sectionsfile"
    # files can be compared with `diff -y --suppress-common-lines ./sections.{old,txt}`
# else
#     printf "\nError: \$sections is too small"
# fi

#write out file made to console, good confirmation
cat sections.txt 

printf "\n%s written to file. 
\nThis is a list of all link href attributes 
\rfound on the contents page of the Manual.\n\n" "$sectionsfile"
