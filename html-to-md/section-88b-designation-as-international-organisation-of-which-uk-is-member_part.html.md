::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 88B: Designation as international organisation of which UK is member {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (88B.01 - 88B.03) last updated: April 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 88B.01 {#b01}

Section 88B was introduced by the Intellectual Property Act 2014 and
came into force on 1 October 2014.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 88B**
  The Unified Patent Court is to be treated for the purposes of section 1 of the International Organisations Act 1968 (organisations of which the United Kingdom is a member) as an organisation to which that section applies.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 88B.02 {#b02}

This provides for the Unified Patent Court to be treated as an
organisation to which section 1 of the International Organisations Act
1968 applies.

### 88B.03 {#b03}

This will allow an Order in Council to be made granting privileges and
immunities to Unified Patent Court and its staff, in accordance with the
usual arrangements for international organisations located in the UK.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
