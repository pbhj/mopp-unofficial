::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 44: Avoidance of certain restrictive conditions \[repealed with savings\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (44.01 - 44-12) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 44.01 {#ref44-01}

s.130(1) and s.44(3) is also relevant

This section renders ineffective certain restrictive conditions or terms
of contracts for the supply of a patented product or of licences to work
a patented invention or of related contracts. (By patented invention is
meant an invention for which a patent is granted. Patented product means
a product which is a patented invention or, in relation to a patented
process, a product obtained directly by means of the process or to which
the process has been applied.) It also concerns use of the presence of
such a condition or term as a defence in infringement proceedings.

### 44.01.1 {#ref44-01-1}

This section ceased to have effect for new agreements when Section 70 of
the Competition Act 1998 came into effect along with the main powers of
that Act from 1 March 2000. The provisions of the Competition Act are
therefore effective instead to prevent restrictive practices. However a
saving in The Competition Act 1998 (Transitional, Consequential and
Supplemental Provisions) Order 2000 means that section 44 will continue
to apply to agreements entered into before 1 March 2000. Under Section
52 of the Competition Act, the Office of Fair Trading will publish
guidelines about the application and enforcement of the prohibitions
contained in that Act. See also
[45.01.1](/guidance/manual-of-patent-practice-mopp/section-45-determination-of-parts-of-certain-contracts-repealed-with-savings/#ref45-01-1).

### 44.02 {#ref44-02}

The present s.44 is substantially a re-enactment of s.57 of the 1949 Act
which in turn was based on s.38 of the 1907 Act. The mischief to which
that s.38 was directed was to prevent a patentee from abusing their
monopoly by placing restrictions on the acquisition and use of products
other than the patented products (observed in the judgment of the Court
of Appeal in Fichera v. Flogates Ltd \[1984\] RPC 257, see also
[44.06](#ref44-06), [44.07](#ref44-07) [44.10](#ref44-10) and
[44.12](#ref44-12)).

### 44.03 {#section}

s.77(1) is also relevant

The provisions of s.44 apply in relation to not only patents granted
under the 1977 Act, but also granted European patents (UK).

### 44.04 {#section-1}

Restrictive conditions may also offend against the Treaty establishing
the European Economic Community, particularly Article 81 (previously 85)
thereof which prohibits agreements affecting trade between member states
and preventing, restricting or distorting competition within the common
market. The position with regard to provisions in patent licence
agreements is also governed by EEC Regulation No. 240/96, see \[1996\]
FSR 397.

  -----------------------------------------------------------------------
   

  **Section 44(1)**

  Subject to the provisions of this section, any condition or term of a
  contract for the supply of a patented product or of a licence to work a
  patented invention, or of a contract relating to any such supply or
  licence, shall be void in so far as it purports -\
  (a) in the case of a contract for supply, to require the person
  supplied to acquire from the supplier, or his nominee, or prohibit him
  from acquiring from any specified person, or from acquiring except from
  the supplier or his nominee, anything other than the patented product;\
  (b) in the case of a licence to work a patented invention, to require
  the licensee to acquire from the licensor or his nominee, or prohibit
  him from acquiring from any specified person, or from acquiring except
  from the licensor or his nominee, anything other than the product which
  is the patented invention or (if it is a process) other than any
  product obtained directly by means of the process or to which the
  process has been applied;\
  (c) in either case, to prohibit the person supplied or licensee from
  using articles (whether patented products or not) which are not
  supplied by, or any patented process which does not belong to, the
  supplier or licensor, or his nominee, or to restrict the right of the
  person supplied or licensee to use any such articles or process.
  -----------------------------------------------------------------------

### 44.05 {#section-2}

Subject to the exceptions to which [44.08](#ref44-08) and
[44.11](#ref44-11) and [44.12](#ref44-12) refer, any condition or term
of such a contract or licence ([see 44.01](#ref44-01)) is void in so far
as it purports to impose any of the requirements, prohibitions or
restrictions set out in s.44(1)(a), (b) and (c). These relate to the
acquisition of anything other than the patented product (as defined in
44.01) and the use of articles and patented processes.

### 44.06 {#ref44-06}

Subsection (1) is of a highly penal nature since it not only renders the
condition or term void but also (by virtue of s.44(3), [see
44.09](#ref44-09)) affords a defence to an infringer. It should
therefore be construed strictly (observed in the judgment of the Court
of Appeal in Fichera v Flogates Ltd \[1984\] RPC 257).

### 44.07 {#ref44-07}

It was also held in Fichera v Flogates Ltd that s.44 is concerned only
with conditions and terms which are enforceable by action for damages
for breach of contract.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 44(2)**
  Subsection (1) above applies to contracts and licences whether made or granted before or after the appointed day, but not to those made or granted before 1st January 1950.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 44.08 {#ref44-08}

Subsection (1) applies to contracts and licences made or granted on or
since 1 January 1950 (the day on which the 1949 Act came into operation)
but not those made or granted previously.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 44(3)**
  In proceedings against any person for infringement of a patent it shall be a defence to prove that at the time of the infringement there was in force a contract relating to the patent made by or with the consent of the plaintiff or pursuer or a licence under the patent granted by him or with his consent and containing in either case a condition or term void by virtue of this section.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 44.09 {#ref44-09}

The presence in a relevant contract or licence of a condition or term
void by virtue of s.44 is, if proved by the infringer, a defence in
infringement proceedings. The contract or licence must have been in
force at the time of the infringement of the patent in question.

### 44.10 {#ref44-10}

In Fichera v. Flogates Ltd [see 44.02](#ref44-02), it was held that none
of the conditions or terms offended under s.44(1), the attempted defence
under s.44(3) therefore failed and the plaintiffs were entitled to an
enquiry as to damages in respect of the infringement. However, an
infringement action in Chiron Corpn v Organon Teknika Ltd \[1994\] FSR
202 was successfully defended because an agreement required the
defendant to buy raw materials from the proprietors whether it was
patented or not. Earlier in Chiron Corpn v Organon Teknika Ltd \[1993\]
FSR 324 and 567 the Court of Appeal held that it was immaterial to the
operation of s.44(3) that the agreement in question was governed by a
foreign law.

  -----------------------------------------------------------------------
   

  **Section 44(4)**

  A condition or term of a contract or licence shall not be void by
  virtue of this section if -\
  (a) at the time of the making of the contract or granting of the
  licence the supplier or licensor was willing to supply the product, or
  grant a licence to work the invention, as the case may be, to the
  person supplied or licensee, on reasonable terms specified in the
  contract or licence and without any such condition or term as is
  mentioned in subsection (1) above; and\
  (b) the person supplied or licensee is entitled under the contract or
  licence to relieve himself of his liability to observe the condition or
  term on giving to the other party three months' notice in writing and
  subject to payment to that other party of such compensation (being, in
  the case of a contract to supply, a lump sum or rent for the residue of
  the term of the contract and, in the case of a licence, a royalty for
  the residue of the term of the licence) as may be determined by an
  arbitrator or arbiter appointed by the Secretary of state.
  -----------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 44(5)**
  If in any proceeding it is alleged that any condition or term of a contract or licence is void by virtue of this section it shall lie on the supplier or licensor to prove the matters set out in paragraph (a) of subsection (4) above.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 44.11 {#ref44-11}

s.44(4)(a) s.44(5) and s.44(4)(b) is also relevant

Subsection (1) does not operate to void a condition or term if (a) at
the time the contract or licence was made, the supplier or licensor was
willing to agree to different terms which were reasonable and excluded
any offending under subsection (1) (the onus being on the supplier or
licensor to prove these matters); and (b) the contract or licence
entitles the person supplied or licensee to relieve themselves of their
liability to observe the offending condition or term, subject to three
months notice and payment of compensation.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 44(6)**
  A condition or term of a contract or licence shall not be void by virtue of this section by reason only that it prohibits any person from selling goods other than those supplied by a specific person or, in the case of a contract for the hiring of or licence to use a patented product, that it reserves to the bailor (or, in Scotland, hirer) or licensor, or his nominee, the right to supply such new parts of the patented product as may be required to put or keep it in repair.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 44.12 {#ref44-12}

A condition or term is not made void by subsection (1) by reason only
that\
(a) it prohibits any person from selling goods other than those supplied
by a specific person, or\
(b) in the case of a contract for the hiring of or licence to use a
patented product, it reserves the right to supply such new parts of the
patented product as may be required to put or keep it in repair. The
latter is wide enough to provide an exception for the replacement of
worn as well as damaged parts (judgment of Court of Appeal in Fichera v.
Flogates Ltd - [see 44.02](#ref44-02)).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
