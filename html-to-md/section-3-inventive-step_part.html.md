::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 3: Inventive step {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (3.01 - 3.101) last updated: April 2024.
:::

::: govuk-grid-column-two-thirds
::: {#default-id-0a305b24 .gem-c-accordion .govuk-accordion .govuk-!-margin-bottom-6 module="govuk-accordion gem-accordion ga4-event-tracker" ga4-expandable="" anchor-navigation="true" show-text="Show" hide-text="Hide" show-all-text="Show all sections" hide-all-text="Hide all sections" this-section-visually-hidden=" this section"}
::: govuk-accordion__section
::: govuk-accordion__section-header
## [Introduction]{#default-id-0a305b24-heading-1 .govuk-accordion__section-button} {#introduction .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Introduction\",\"index_section\":1,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-1 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-1" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Introduction\",\"index_section\":1,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
This section of the Manual is presented as three sub-sections:

-   general approach & legal background
-   examining for inventive step
-   assessing obviousness

The third sub- section provides some of the more prominent methods for
answering the final question in the Windsurfing/Pozzoli approach -- ie
"is it obvious?"

### 3.01 {#ref3-01}

s.130(7) is also relevant

This section is concerned with the second of the tests for patentability
set out in s.1(1). It is intended to have, as nearly as practicable, the
same effect as the corresponding provisions of the EPC, PCT and CPC, ie
Article 56 of the EPC.

### 3.02 {#section}

The question of whether or not an invention is obvious is a matter which
is normally decided on the technical facts of the particular case rather
than on any general legal principles, but insofar as any such principles
can be derived from decisions given under previous legislation they will
generally continue to be relevant.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 3**
  An invention shall be taken to involve an inventive step if it is not obvious to a person skilled in the art, having regard to any matter which forms part of the state of the art by virtue only of section 2(2) above (and disregarding section 2(3) above).
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Subsection 1: General approach and legal background]{#default-id-0a305b24-heading-2 .govuk-accordion__section-button} {#subsection-1-general-approach-and-legal-background .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Subsection 1: General approach and legal background\",\"index_section\":2,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-2 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-2" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Subsection 1: General approach and legal background\",\"index_section\":2,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.03 {#ref3-03}

What constitutes an inventive step may depend on the nature of the
invention. The matter was considered by Lord Hoffmann in [Biogen Inc v
Medeva plc \[1997\] RPC
1](https://doi.org/10.1093/rpc/1997rpc1){rel="external"} (at page 34) as
follows:

> Whenever anything inventive is done for the first time it is the
> result of the addition of a new idea to the existing stock of
> knowledge. Sometimes, it is the idea of using established techniques
> to do something which no one had previously thought of doing. In that
> case the inventive idea will be doing the new thing. Sometimes it is
> finding a way of doing something which people had wanted to do but
> could not think how. The inventive idea would be the way of achieving
> the goal. In yet other cases, many people may have a general idea of
> how they might achieve a goal but not know how to solve a particular
> problem which stands in their way. If someone devises a way of solving
> the problem, his inventive step will be that solution, but not the
> goal itself or the general method of achieving it.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Obviousness must be decided on an objective test]{#default-id-0a305b24-heading-3 .govuk-accordion__section-button} {#obviousness-must-be-decided-on-an-objective-test .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Obviousness must be decided on an objective test\",\"index_section\":3,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-3 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-3" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Obviousness must be decided on an objective test\",\"index_section\":3,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.04 {#ref3-04}

EPC aa.52& 56 is also relevant

The test for obviousness should, as far as is possible, be an objective
one. The question is whether the invention would have been obvious to a
skilled person in the art, and not whether it was or would have been
obvious to the inventor or to some other particular worker. It is
immaterial whether the invention was the result of independent work and
research done without knowledge of the prior art ([Allmanna Svenska
Elektriska AB v The Burntisland Shipbuilding Co Ltd, 69 RPC
63](https://doi.org/10.1093/rpc/69.4.63){rel="external"} at page 70).
Although evidence of what was in the inventor's mind may be admissible
as evidence of the state of the art, it would seldom be otherwise
admissible ([The Wellcome Foundation v VR Laboratories (Australia) Pty.
Ltd, \[1982\] RPC
343](https://doi.org/10.1093/rpc/1982rpc343){rel="external"}). The EPO
Board of Appeal has held that the subjective achievement of the inventor
is not relevant; the fact that an invention relating to steel refining
came, not from the steel industry, but from an applicant who normally
deals with other fields of technology is not evidence for the existence
of an inventive step (Decisions T36/82, OJEPO 7/83).

### 3.05 {#ref3-05}

In the judgment of the Court of Appeal in [Windsurfing International
Inc. v Tabur Marine (Great Britain) Ltd, \[1985\] RPC
59](https://doi.org/10.1093/rpc/1985rpc59){rel="external"} (in
considering whether claims relating to a sailboard were obvious) it was
stated that:

> the question of whether the alleged invention was obvious has to be
> answered objectively by reference to whether, at the material time
> (that is, immediately prior to the priority date), the allegedly
> inventive step or concept would have been obvious to a skilled
> addressee" and that "what has to be determined is whether what is now
> claimed as inventive would have been obvious, not whether it would
> have appeared commercially worthwhile to exploit it

### 3.06 {#section-1}

In [Molnlycke AB v Procter & Gamble Ltd \[1994\] RPC
49](https://doi.org/10.1093/rpc/1994rpc49){rel="external"} the Court of
Appeal recognised the usefulness of the analysis formulated in
Windsurfing but did not consider that it assisted to ask whether the
patent discloses something sufficiently inventive to deserve the grant
of a monopoly. The criterion for deciding the issue of inventive step as
laid down by statute was held to be a wholly objective qualitative and
not quantitative test.

### 3.07 {#ref3-07}

The Court of Appeal followed Windsurfing in [Hallen Co v Brabantia (UK)
Ltd \[1991\] RPC
195](https://doi.org/10.1093/rpc/1991rpc195){rel="external"}, observing
that "obvious" in s.3 is not directed to whether an advance is
"commercially obvious" and stating:

> We do not think that the hypothetical technician must also be taken as
> applying his mind to the commercial consequences which might follow if
> the step or process in question were found in practice to achieve or
> assist the objective which he had in view.

However the Court of Appeal has more recently appeared to retreat from
this position, stating in [Dyson Appliances Ltd v Hoover Ltd \[2002\]
RPC 22](https://doi.org/10.1093/rpc/2002rpc22){rel="external"} that:

> commercial realities cannot necessarily be divorced from the kinds of
> practical outcome which might occur to the skilled addressee as
> worthwhile

and so it followed that a:

> commercial mindset will have played a part in setting the notional
> skilled addressee's mental horizon

### 3.08 {#ref3-08}

In [Petra Fischer's Application \[1997\] RPC
899](https://doi.org/10.1093/rpc/1997rpc899){rel="external"} it was held
that a diesel cabriolet was obvious even though there may be commercial
prejudice against the idea; Jacob J stated that:

> The patentee in her patent has told the skilled man nothing which he
> did not know before, to whit, that in the engine space of a basic
> production model he could put a diesel engine, if he wanted to.
> Whether it was worth doing that or not is another matter. Whether he
> thinks it will sell or not, that is another matter

### 3.09 {#ref3-09}

It is also unsound to fasten on the word "step" and to look at the steps
which were actually taken by the inventor; this interpretation places
too much weight on the choice of the particular word "step" whereas the
word used in the French and German texts of the corresponding provisions
of the European Patent Convention means "activity" (judgment of Court of
Appeal in [Genentech Inc's Patent \[1989\] RPC
147](https://doi.org/10.1093/rpc/1989rpc205){rel="external"} at page
275). It is necessary to ask by what routes it would have been possible
for the skilled person to proceed to the goal (ie the invention) from
the starting point, considering how obstacles might be overcome or
avoided on any such route, not only that followed by the inventor.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Structured approach needed when assessing obviousness]{#default-id-0a305b24-heading-4 .govuk-accordion__section-button} {#structured-approach-needed-when-assessing-obviousness .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Structured approach needed when assessing obviousness\",\"index_section\":4,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-4 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-4" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Structured approach needed when assessing obviousness\",\"index_section\":4,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.10 {#section-2}

Anyone who is considering the question of whether or not an invention is
obvious must beware of hindsight or ex post facto analysis. It can be
very easy to be misled by a line of reasoning that involves working
forward from the stated problem in a succession of easy steps when one
knows the desired solution. In particular one must avoid looking at a
prior publication under the influence of the patent or patent
application in question, and one should attempt to place oneself in the
shoes of the skilled person faced with the problem at hand. This is
necessarily an artificial position, since the patent or patent
application presents both the solution (the invention) as well as the
problem (or instead of the problem, a pointer to the problem since, in
some cases, one may be only able to infer this from the description of
the invention).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The four-step Windsurfing approach]{#default-id-0a305b24-heading-5 .govuk-accordion__section-button} {#the-four-step-windsurfing-approach .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The four-step Windsurfing approach\",\"index_section\":5,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-5 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-5" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The four-step Windsurfing approach\",\"index_section\":5,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.11 {#ref3-11}

In [Windsurfing International Inc. v Tabur Marine (Great Britain) Ltd,
\[1985\] RPC 59](https://doi.org/10.1093/rpc/1985rpc59){rel="external"},
the Court of Appeal held that the question of obviousness

> has to be answered, not by looking with the benefit of hindsight at
> what is known now and what was known at the priority date and asking
> whether the former flows naturally and obviously from the latter, but
> by hypothesizing what would have been obvious at the priority date to
> a person skilled in the art to which the patent in suit relates.

Thus the court formulated a four-step approach to assessing obviousness:

\(1\) Identify the claimed inventive concept.

\(2\) Assume the mantle of the normally skilled but unimaginative
addressee in the art at the priority date and to impute to them what
was, at that date, common general knowledge of the art in question.

\(3\) Identify what, if any, differences exist between the matter cited
as being "known or used" and the alleged invention.

\(4\) Decide, without any knowledge of the alleged invention, whether
these differences constitute steps which would have been obvious to the
skilled person or whether they require any degree of invention.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The Windsurfing/Pozzoli approach; a reformulation of the Windsurfing approach]{#default-id-0a305b24-heading-6 .govuk-accordion__section-button} {#the-windsurfingpozzoli-approach-a-reformulation-of-the-windsurfing-approach .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The Windsurfing/Pozzoli approach; a reformulation of the Windsurfing approach\",\"index_section\":6,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-6 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-6" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The Windsurfing/Pozzoli approach; a reformulation of the Windsurfing approach\",\"index_section\":6,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.12 {#ref3-12}

In [Pozzoli SPA v BDMO SA \[2007\] EWCA Civ
588](http://www.bailii.org/ew/cases/EWCA/Civ/2007/588.html){rel="external"},
Jacob LJ restated and elaborated upon the Windsurfing approach. This
decision of the Court of Appeal does not formally replace, supersede or
supplant the Windsurfing approach. It is included here because it gives
an insight to the Court's latest thinking on obviousness, and for its
very useful discussion of the processes that should be adopted when one
uses the Windsurfing approach. There is no suggestion that outcomes will
be any different under the Pozzoli reformulation, it is a matter of
style and clarity of approach and not substance. It follows that
Windsurfing and Pozzoli taken together should now be seen as the
precedent for deciding questions of obviousness.

### 3.13 {#ref3-13}

At paragraph 23 of Pozzoli, Jacob LJ reformulated the Windsurfing
approach as follows:

(1)(a) Identify the notional "person skilled in the art"

(1)(b) Identify the relevant common general knowledge of that person;

\(2\) Identify the inventive concept of the claim in question or if that
cannot readily be done, construe it;

\(3\) Identify what, if any, differences exist between the matter cited
as forming part of the "state of the art" and the inventive concept of
the claim or the claim as construed;

\(4\) Viewed without any knowledge of the alleged invention as claimed,
do those differences constitute steps which would have been obvious to
the person skilled in the art or do they require any degree of
invention?

The reasoning behind this reformulation is set out at paragraphs 15 & 16
of Pozzoli :

> First one must actually conduct the first two operations in the
> opposite order -- mantle first, then concept. For it is only through
> the eyes of the skilled man that one properly understand what such a
> man would understand the patentee to have meant and thereby set about
> identifying the concept.
>
> Next, that first step actually involves two steps, identification of
> the attributes of the notional "person skilled in the art" (the
> statutory term) and second identification of the common general
> knowledge ("cgk") of such a person.

Later, at paragraphs 21 & 22, Jacob LJ discusses the prior art and the
third step in the Windsurfing approach:

> Identification of the concept is not the place where one takes into
> account the prior art. You are not at this point asking what was new.
> Of course the claim may identify that which was old (often by a
> pre-characterising clause) and what the patentee thinks is new (if
> there is characterising clause) but that does not matter at this
> point.
>
> The third step also requires a little reformulation -- Windsurfing was
> a case under the 1949 Act where the statutory words for the prior art
> were "known or used". The European Patent Convention uses the words
> "state of the art.

### 3.13.1 {#ref3-13-1}

In [Lalvani et al's Patent
BLO/220/13](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F220%2F13&submit=Go+%BB){rel="external"}
the hearing officer addressed the question of whether, following the
decision in [Human Genome Sciences v Eli Lilly \[2011\] UKSC 51,
\[2012\] RPC
6](http://rpc.oxfordjournals.org/content/129/2/102.abstract?sid=776ed992-edab-4f22-ae90-c49ba10697b5){rel="external"}
the Office should follow the problem-solution approach of the EPO when
deciding questions of obviousness. The hearing officer decided that
although decisions of the EPO Boards of Appeal are persuasive, the
Office is bound to follow the reformulated Windsurfing approach set out
by the Court of Appeal in Pozzoli.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [What does this "4-step" approach do?]{#default-id-0a305b24-heading-7 .govuk-accordion__section-button} {#what-does-this-4-step-approach-do .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"What does this “4-step” approach do?\",\"index_section\":7,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-7 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-7" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"What does this “4-step” approach do?\",\"index_section\":7,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.14 {#ref3-14}

In [DSM NV's Patent \[2001\] RPC
35](https://doi.org/10.1093/rpc/2001rpc35){rel="external"}, Neuberger J
pointed out that the four-step Windsurfing approach ends up with the
original issue to be resolved being embodied in the final question.
Nevertheless it was held appropriate to apply this structured approach
in order to ensure a reasoned, methodical and consistent analysis of
obviousness,

> not merely because it has been approved and applied in a number of
> previous cases, including in the Court of Appeal. It is also because
> it ensures that one does not go straight to the question of
> obviousness by reference to a general impression as to the evidence as
> a whole. By adopting the structured approach one ensures that there is
> a measure of discipline, reasoning and method in one's approach.
> Indeed, it helps to ensure that there is consistency of approach in
> different cases involving the issue of obviousness.

### 3.15 {#section-3}

Thus the first three steps of the Windsurfing/Pozzoli approach are
preliminary or preparatory steps to put one in the proper frame of mind
to answer the question posed in the fourth step; and the final step of
the Windsurfing/Pozzoli approach can be seen as a restatement of the
statutory test for inventive step -- ie "is it obvious?"
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Consequences of not using the Windsurfing/Pozzoli approach]{#default-id-0a305b24-heading-8 .govuk-accordion__section-button} {#consequences-of-not-using-the-windsurfingpozzoli-approach .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Consequences of not using the Windsurfing/Pozzoli approach\",\"index_section\":8,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-8 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-8" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Consequences of not using the Windsurfing/Pozzoli approach\",\"index_section\":8,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.16 {#ref3-16}

The Court of Appeal in [Wheatley v Drillsafe Ltd \[2001\] RPC
7](https://doi.org/10.1093/rpc/2001rpc7){rel="external"}, in overturning
the decision of the Patents Court on obviousness, highlighted how a
failure to use the structured Windsurfing approach to assessing
obviousness had led the Patents Court to fall into the trap of using
hindsight and to fail to distinguish what was known from what was common
general knowledge.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Collocations]{#default-id-0a305b24-heading-9 .govuk-accordion__section-button} {#collocations .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Collocations\",\"index_section\":9,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-9 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-9" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Collocations\",\"index_section\":9,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.17 {#ref3-17}

Special consideration is needed when an invention may be to a
combination or a collocation. In [SABAF SpA v MFI Furniture Centres Ltd
\[2005\] RPC 10](https://doi.org/10.1093/rpc/2005rpc10){rel="external"},
Lord Hoffmann held that before you can ask whether the invention
involves an inventive step, you first have to decide what the invention
is. In particular, the first step is to decide whether you are dealing
with one invention or, for the purposes of s.3, two or more inventions.
If two integers interact upon each other, if there is synergy between
them, they constitute a single invention having a combined effect and
one applies s.3 to the idea of combining them. But if each integer
performs its own proper function independently of any of the others, and
the claim is a mere aggregation or juxtaposition of features, then each
is, for the purposes of s.3, a separate invention. This concept was
applied in \[Garmin (Europe) Ltd v Koninklijke Philips N.V \[2019\] EWHC
107\](https://www.bailii.org/ew/cases/EWHC/Ch/2019/107.html (at
paragraphs 182-189) where the synergy between a portable performance
monitor and a wider feedback system was considered. The combination of a
series of known or obvious features, each playing its usual part in the
final entity, will be a matter of design or mere collocation, not of
invention, and objection should be raised under s.3 (for discussion of
anticipatory disclosures in multiple documents [see also
2.09](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-09))

In [SABAF SpA v MFI Furniture Centres Ltd \[2005\] RPC
10](https://doi.org/10.1093/rpc/2005rpc10){rel="external"}, Lord
Hoffmann quoted with approval passages from the EPO Guidelines for
Substantive Examination, providing guidance on how to determine whether
two features display synergy. This guidance was re-stated and further
explained in the EPO Technical Board of Appeal decision in T 1054/05:

> Two features interact synergistically if their functions are
> interrelated and lead to an additional effect that goes beyond the sum
> of the effects of each feature taken in isolation. It is not enough
> that the features solve the same technical problem or that their
> effects are of the same kind and add up to an increased but otherwise
> unchanged effect.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The individual steps of the Windsurfing/Pozzoli approach]{#default-id-0a305b24-heading-10 .govuk-accordion__section-button} {#the-individual-steps-of-the-windsurfingpozzoli-approach .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The individual steps of the Windsurfing/Pozzoli approach\",\"index_section\":10,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-10 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-10" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The individual steps of the Windsurfing/Pozzoli approach\",\"index_section\":10,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.18 {#section-4}

What follows are some discussions of some of the steps within the
Windsurfing/Pozzoli approach, and some of the factors that will
influence one's thinking when using the Windsurfing/Pozzoli approach.

### 3.19 {#section-5}

One may also wish to consider the discussions of these steps available
in guides such as "Terrell on the Law of Patents" 16th (2006) Edition
and the "CIPA Guide to the Patents Acts" (8th Edition). The locations of
the relevant discussions in these guides are summarized in the table
below.

  --------------------------------------- ------------- ----------------
  **Topic**                               **Terrell**   **CIPA Guide**
  person skilled in the art               8.01, 12.73   3.07
  common general knowledge                8.56, 12.54   3.11
  identifying the inventive concept       12.74         3.18
  identification of differences           12.80         3.25
  difference involves an inventive step   12.81         3.26
  --------------------------------------- ------------- ----------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The skilled person]{#default-id-0a305b24-heading-11 .govuk-accordion__section-button} {#the-skilled-person .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The skilled person\",\"index_section\":11,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-11 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-11" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The skilled person\",\"index_section\":11,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.20 {#ref3-20}

The "person skilled in the art" is not a highly skilled expert or a
Nobel prize winner, nor are they some form of lowest common denominator.
Instead they are best seen as someone who is good at their job, a
fully-competent worker. To a large degree the capacities of the skilled
person will be determined by the nature of the common general knowledge
identified as being "relevant". An important consequence of imparting to
this person the relevant common general knowledge is that they will
generally not be aware of individual patent specifications, scientific
papers, or the like -- see the extract from [General Tire & Rubber Co v
Firestone Tyre & Rubber Co
Ltd](https://doi.org/10.1093/rpc/78.2.21){rel="external"} at
[3.32](#ref3-32) below.

### 3.21 {#ref3-21}

They should be taken to be a person who has the skill to make routine
workshop developments but not to exercise inventive ingenuity or think
laterally - as set out, for example, by Laddie J in [Pfizer Ltd's Patent
\[2001\] FSR
16](https://uk.westlaw.com/Document/IE0E6F1E0E42711DA8FC2A0F0355337E9/View/FullText.html){rel="external"}
at paragraphs 62 and 63, and by the Court of Appeal in [Technip France
SA's Patent \[2004\] RPC
46](https://doi.org/10.1093/rpc/2004rpc46){rel="external"}, per Jacob LJ
at paragraphs 6 to 10, who held that the person skilled in the art, if
real, would be very boring - a nerd.

### 3.22 {#ref3-22}

Their level of skill will, however, depend on the scope of the subject
matter of the patent in question (see, for example, [Dyson Appliances
Ltd v Hoover Ltd \[2001\] RPC
26](https://doi.org/10.1093/rpc/2001rpc26){rel="external"}, upheld by
the [Court of Appeal \[2002\] RPC
22](https://doi.org/10.1093/rpc/2002rpc22){rel="external"} [see also
14.75](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-75))

### 3.23 {#ref3-23}

They are assumed to be at least sufficiently interested to address their
mind to the subject and to consider the practical application of the
information which they are deemed to have (see [Windsurfing
International Inc. v Tabur Marine (Great Britain) Ltd \[1985\] RPC
59](https://doi.org/10.1093/rpc/1985rpc59){rel="external"}. While
recognising this, the Court of Appeal in [PLG Research Ltd and anr. v
Ardon International Ltd and others \[1995\] RPC
287](https://doi.org/10.1093/rpc/1995rpc287){rel="external"} held that
knowing a piece of prior art is one thing but appreciating its
significance to the solution to the problem in hand was another.
Whitford J had similarly warned in [Sandoz Ltd (Frei's Application)
\[1976\] RPC 449](https://doi.org/10.1093/rpc/93.18.449){rel="external"}
against too ready an assumption that the significance of existing
published material in relation to the problem dealt with would
necessarily be apparent to the hypothetical skilled person.

### 3.24 {#ref3-24}

The skilled person should not be expected to try all combinations unless
they have a problem in mind and particular combinations might assist
them in solving it; they are not to be expected to take steps or try
processes which they would not regard as worthwhile as a possible means
of achieving or assisting in practice the objective which they have in
view (see the judgment of the Court of Appeal in [Hallen Co v Brabantia
(UK) Ltd \[1991\] RPC
195](https://doi.org/10.1093/rpc/1991rpc195){rel="external"}, [see also
3.07](#ref3-07).

### 3.25 {#ref3-25}

Although obviousness generally is assessed on the basis of technical
rather than commercial considerations the Court of Appeal in Dyson v
Hoover [see 3.07](#ref3-07) appeared to indicate a softening of that
approach, holding that commercial realities cannot always be divorced
from the kinds of practical outcome which might appear worthwhile to the
skilled addressee and that commercial considerations will play a part in
the mindset of the skilled person.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The skilled person seeks expert advice, and can be considered a team]{#default-id-0a305b24-heading-12 .govuk-accordion__section-button} {#the-skilled-person-seeks-expert-advice-and-can-be-considered-a-team .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The skilled person seeks expert advice, and can be considered a team\",\"index_section\":12,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-12 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-12" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The skilled person seeks expert advice, and can be considered a team\",\"index_section\":12,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.26 {#ref3-26}

With a prospective solution in mind, the skilled person might need to
seek advice from an expert in another field. In [Tetra Molectric Ltd v
Japan Imports Ltd \[1976\] RPC
547](https://doi.org/10.1093/rpc/93.21.547){rel="external"} the Court of
Appeal held that a claim to a smoker's lighter using piezoelectric
ignition was obvious. Since the possibility of using piezoelectricity in
a lighter would have occurred to the industry, a skilled lighter
manufacturer, himself not an expert in piezoelectricity, could
reasonably be expected to seek advice from those who were. If such
experts had been consulted, they would have advised that the suggestion
was definitely worth trying, and they could have solved such problems as
arose. The person skilled in the art may, in such circumstances, be
regarded as combining their own common general knowledge with expert.

### 3.26.1 {#ref3-26-1}

In [Vernacare Ltd v Moulded Fibre Products (MFP) \[2022\] EWHC 2197
(IPEC)](https://caselaw.nationalarchives.gov.uk/ewhc/ipec/2022/2197){rel="external"},
Nicholas Caddick QC (sitting as a Deputy High Court Judge), clarified
that whilst the skilled person can seek out specialist advice in
appropriate circumstances, that does not mean that any solution offered
by the specialist advisor should be treated as being obvious to the
skilled person, or, that the common general knowledge of the specialist
advisor becomes part of the common general knowledge of the skilled
person for the purposes of assessing inventiveness.

### 3.27 {#ref3-27}

Alternatively the hypothetical skilled person acting under expert advice
can be seen as a team composed of skilled persons from each of the
relevant fields, see [Genentech Inc's Patent \[1989\] RPC
147](https://doi.org/10.1093/rpc/1989rpc205){rel="external"} at pages
278 & 280 or [Halliburton Energy Services Inc v Smith International
(North Sea) Ltd \[2006\] RPC
2](https://doi.org/10.1093/rpc/2006rpc2){rel="external"} at paras 39 &
40. Where the skilled person is represented by a team of individuals
each practicing their particular art, it makes no difference whether
they work together as a single unit or as sub-contractors. The standard
for skills is the same. In the example of the piezoelectric lighter
above the team might be composed of a skilled person from the field of
piezoelectricity and a skilled person from the field of lighter
manufacture. (See also [3.44](#ref3-44) & [3.83](#ref3-83)).

### 3.28 {#ref3-28}

However, it cannot be assumed -- for the purpose of assessing inventive
step -- that the "person skilled in the art" necessarily comprises a
team made up of experts with all the different skills needed to perform
the invention. In [Schlumberger Holdings Ltd v Electromagnetic
Geoservices AS \[2010\] RPC
33](https://www.bailii.org/ew/cases/EWCA/Civ/2010/819.html){rel="external"},
the Court of Appeal considered the inventiveness of a method for oil
exploration which utilised an electromagnetic surveying technique known
as CSEM to determine whether a potential undersea reservoir actually
contained oil or gas. CSEM was a known technique, but this application
of it had not been contemplated. Jacob LJ held that the correct approach
was to ask whether the exploration geophysicist would realise that there
was a solvable problem and that CSEM might realistically be the
solution. The problem also needed to be approached from the other side;
would the CSEM expert be aware of the exploration geophysicist's problem
and appreciate that CSEM was a potential solution?

> if it would not be obvious to either of the notional persons or teams
> alone and not obvious to either sort of team to bring in the other,
> then the invention cannot fairly be said to be obvious.

### 3.28.1 {#section-6}

Jacob LJ held that in such a case, the "person skilled in the art" for
the assessment of inventiveness under s.3 (Art. 56 EPC) was not
necessarily the same person or team as that used to assess sufficiency
([see
14.64](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-64))
or the scope of the claims. For the latter two purposes, this "person"
would be a person or team with all the skills necessary to perform the
invention -- in this case, an exploration geophysicist and a CSEM
expert. This was not necessarily the case for inventiveness, if there
was no reason for this team to be assembled without the benefit of
hindsight.

### 3.28.2 {#ref3-28-2}

The question of whether a team with disparate skills would be assembled
was also at issue in [Mutoh Industry Ltd's Application \[1984\] RPC
35](https://doi.org/10.1093/rpc/1984rpc35){rel="external"}. In this
case, the hearing officer held that a drawing board employing magnetic
bearings was obvious, since it was reasonable for the drawing-board
person concerned with the problem of reducing friction to consult a
bearings expert. The Patents Court however allowed an appeal, finding
that users of the known device were not struggling to overcome a problem
which inhibited their activities, nor were manufacturers failing to put
the known device on the market because it was not sufficiently
friction-free; there was therefore no reason for the manufacturer or
user to look for outside assistance. In ABT Hardware Ltd's Application
[BL
O/36/87](https://www.gov.uk/government/publications/patent-decision-o03687),
the hearing officer distinguished the circumstances from those in Mutoh
and held the invention to be obvious. It was concerned with the use in a
letterplate of a known type of magnet comprising an elastomer loaded
with ferrite powder to hold a flap in sealing engagement with a frame
over an opening in the frame. There were specific problems associated
with prior magnetic letterplates which could arguably have led the
applicants to seek specialist advice, and the general availability and
widespread use of the magnets in question might also reasonably be
expected to have led the applicants naturally to consider their adoption
in letterplates, with or without consultation of specialists.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Common general knowledge]{#default-id-0a305b24-heading-13 .govuk-accordion__section-button} {#common-general-knowledge .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Common general knowledge\",\"index_section\":13,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-13 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-13" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Common general knowledge\",\"index_section\":13,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.29 {#section-7}

One cannot overstate the importance of the notion of common general
knowledge. It is central to everything that is required of the
hypothetical skilled person, for example in reading and understanding
the patent for the purposes of purposive construction, or in
understanding and reacting to the cited prior art. Common general
knowledge can, perhaps, be summarized as a part of the mental equipment
or mental toolkit needed so as to be competent in the art concerned. It
is what makes the skilled person skilled.

### 3.30 {#ref3-30}

The courts have attempted to define the common general knowledge many
times over the years. In [Raychem Corp's Patents \[1998\] RPC
31](https://doi.org/10.1093/rpc/1998rpc31){rel="external"} Laddie J
explained common general knowledge as follows:

> The common general knowledge is the technical background of the
> notional man in the art against which the prior art must be
> considered. This is not limited to material he has memorized and has
> at the front of his mind. It includes all that material in the field
> he is working in which he knows exists, which he would refer to as a
> matter of course if he cannot remember it and which he understands is
> generally regarded as sufficiently reliable to use as a foundation for
> further work or to help understand the pleaded prior art. This does
> not mean that everything on the shelf which is capable of being
> referred to without difficulty is common general knowledge nor does it
> mean that every word in a common text book is either. In the case of
> standard textbooks, it is likely that all or most of the main text
> will be common general knowledge. In many cases common general
> knowledge will include or be reflected in readily available trade
> literature which a man in the art would be expected to have at his
> elbow and regard as basic reliable information.

### 3.30.0 {#ref3-30-0}

In [Teva UK Limited & Anor v AstraZeneca AB \[2014\] EWHC 2873
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2014/2873.html){rel="external"}
Sales J said that guidance on what constitutes common general knowledge
(CGK) needs to be kept up to date in the age of the internet and digital
databases of journal articles. He stated that searches of databases are
part and parcel of the routine sharing of information in the scientific
community and are an ordinary research technique, and further added
that:

> if there is sufficient basis ... in the background CGK relating to a
> particular issue to make it obvious to the ... skilled person that
> there is likely to be -- not merely a speculative possibility that
> there may be -- relevant published material bearing directly on that
> issue which would be identified by such a search, the relevant CGK
> will include material that would readily be identified by such a
> search.

### 3.30.1 {#ref3-30-1}

A set of industry standards may be considered to be part of the common
general knowledge, even if they are of such length and complexity that
no skilled worker could possibly be expected to know even a fraction of
the information contained therein, providing the skilled person would
know where to find the information relevant to the task in hand ([Nokia
v Ipcom \[2010\] EWHC
3482](http://www.bailii.org/ew/cases/EWHC/Patents/2010/3482.html){rel="external"};
upheld at appeal [\[2011\] EWCA Civ
6](http://www.bailii.org/ew/cases/EWCA/Civ/2011/6.html){rel="external"}).

### 3.30.2 {#ref3-30-2}

Unconventional knowledge can still be part of the common general
knowledge. In [Apimed Medical Honey Ltd v Brightwake Ltd \[2011\] EWPCC
2, \[2011\] RPC 16](https://doi.org/10.1093/rpc/rcr009){rel="external"},
the patent concerned surgical dressings for wounds comprising honey and
a gelling agent. The court held that, at the priority date, there may
have only been a few people working within the wound care field who
would have seen a clinical future in treating wounds with honey, but
that fact did not eliminate the idea from being a part of the common
general knowledge.

### 3.30.3 {#ref3-30-3}

In [Generics (UK) Ltd (t/a Mylan) v Warner-Lambert Company LLC \[2015\]
EWHC
2548](http://www.bailii.org/ew/cases/EWHC/Patents/2015/2548.html){rel="external"}
Arnold J held that matter being relied on as common general knowledge
must be common general knowledge in the UK. He explained:\
\
"The reason for this is that, whether one is concerned with the validity
of a European Patent (UK), or a UK patent, one is concerned with a right
in respect of the UK. It is true that the prior art may have been
published anywhere in the world, but I do not think that alters the need
for the skilled team to consider that art as if they were located in the
UK. I do not think it matters that a fact was common general knowledge
in (say) China, if it was not common general knowledge here. The
position may be different if all the persons skilled in a particular art
in the UK are acquainted with the position in China".

### 3.31 {#ref3-31}

A much older definition comes from Fletcher-Moulton LJ in [British Ore
Concentrate Syndicate Ltd v Minerals Separation Ltd (1909) 26 RPC
124](https://doi.org/10.1093/rpc/26.6.124){rel="external"} (the
reference to "public general knowledge" should be read as common general
knowledge):

> \[The court\] has to arrive as closely as it can at the mental
> attitude of a well- instructed representative of the class to whom the
> Specification is addressed, and no more. In other words, in the
> performance of this part of its task it has to ask itself what ought
> fairly to be considered to be the state of knowledge in the trade or
> profession at the date of the patent with respect to the matters in
> question, and if any facts or documents are such that in ordinary
> probability they would not be known to competent members of such trade
> or profession they ought not to be taken, either for or against the
> public on the one hand, or the inventor on the other, as forming part
> of public general knowledge.

### 3.32 {#ref3-32}

A much longer passage from the speech by Sachs LJ in [General Tire &
Rubber Co v Firestone Tyre & Rubber Co Ltd \[1972\] RPC
457](https://doi.org/10.1093/rpc/78.2.21){rel="external"} is of
particular interest because it sets out the relationship of patent
specifications to the common general knowledge ("it is clear that
individual patent specifications and their contents do not normally form
part of the relevant common general knowledge"). It also sets out the
relationship of scientific journals and papers to the common general
knowledge ("it is not sufficient to prove common general knowledge that
a particular disclosure is made in an article, or series of articles, in
a scientific journal"). This passage is taken from pages 482-483 of the
General Tire judgment:

> The common general knowledge imputed to such an addressee must, of
> course, be carefully distinguished from what in patent law is regarded
> as public knowledge. This distinction is well explained in Halsbury's
> Laws of England, Vol. 29, para. 63. As regards patent specifications
> it is the somewhat artificial (see per Lord Reid in the Technograph
> case \[1971\] FSR 188 at 193) concept of patent law that each and
> every specification, of the last 50 years, however unlikely to be
> looked at and in whatever language written, is part of the relevant
> public knowledge if it is resting anywhere in the shelves of the
> Patent Office. On the other hand, common general knowledge is a
> different concept derived from a commonsense approach to the practical
> question of what would in fact be known to an appropriately skilled
> addressee---the sort of man, good at his job, that could be found in
> real life.

> The two classes of documents which call for consideration in relation
> to common general knowledge in the instant case were individual patent
> specifications and 'widely read publications'.

> As to the former, it is clear that individual patent specifications
> and their contents do not normally form part of the relevant common
> general knowledge, though there may be specifications which are so
> well known amongst those versed in the art that upon evidence of that
> state of affairs they form part of such knowledge, and also there may
> occasionally be particular industries (such as that of colour
> photography) in which the evidence may show that all specifications
> form part of the relevant knowledge.

As regards scientific papers generally, it was said by Luxmoore J. in
British Acoustic Films (53 RPC 221 at 250):

> In my judgment it is not sufficient to prove common general knowledge
> that a particular disclosure is made in an article, or series of
> articles, in a scientific journal, no matter how wide the circulation
> of that journal may be, in the absence of any evidence that the
> disclosure is accepted generally by those who are engaged in the art
> to which the disclosure relates. A piece of particular knowledge as
> disclosed in a scientific paper does not become common general
> knowledge merely because it is widely read, and still less because it
> is widely circulated. Such a piece of knowledge only becomes general
> knowledge when it is generally known and accepted without question by
> the bulk of those who are engaged in the particular art; in other
> words, when it becomes part of their common stock of knowledge
> relating to the art.'

And a little later, distinguishing between what has been written and
what has been used, he said:

> It is certainly difficult to appreciate how the use of something which
> has in fact never been used in a particular art can ever be held to be
> common general knowledge in the art.'

Those passages have often been quoted, and there has not been cited to
us any case in which they have been criticised. We accept them as
correctly stating in general the law on this point, though reserving for
further consideration whether the words 'accepted without question' may
not be putting the position rather high: for the purposes of this case
we are disposed, without wishing to put forward any full definition, to
substitute the words 'generally regarded as a good basis for further
action'."

### 3.33 {#ref3-33}

Over time the growth of proprietary and specialist knowledge, that is
knowledge known only within certain organisations or companies or known
only to a few experts, makes it increasingly difficult to distinguish
the common general knowledge from the state of the art. Although a
feature, item or concept may be well-known to a few, it is not part of
the common general knowledge unless it can be shown to be known to and
accepted by the large majority of those skilled in the art. In [Beloit v
Valmet (No.2) \[1997\] RPC
489](https://doi.org/10.1093/rpc/1997rpc489){rel="external"} Aldous L.J.
put it as follows:

> It has never been easy to differentiate between common general
> knowledge and that which is known by some. It has become particularly
> difficult with the modern ability to circulate and retrieve
> information. Employees of some companies, with the use of libraries
> and patent departments, will become aware of information soon after it
> is published in a whole variety of documents; whereas others, without
> such advantages, may never do so until that information is accepted
> generally and put into practice. The notional skilled addressee is the
> ordinary man who may not have the advantages that some employees of
> large companies may have. The information in a patent specification is
> addressed to such a man and must contain sufficient details for him to
> understand and apply the invention. It will only lack an inventive
> step if it is obvious to such a man. It follows that evidence that a
> fact is known or even well-known to a witness does not establish that
> that fact forms part of the common general knowledge. Neither does it
> follow that it will form part of the common general knowledge if it is
> recorded in a document.

### 3.33.1 {#ref3-33-1}

The Hearing Officer in [Maxluck Biotechnology's Application BL
O/130/10](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F130%2F10&submit=Go+%BB){rel="external"}
highlighted (citing [Ratiopharm GmbH v Napp Pharmaceutical Holdings
\[2009\] RPC 11](https://doi.org/10.1093/rpc/rcp027){rel="external"})
the dangers of selectivity in deciding what is common general knowledge
in the field:

> However, when deciding what is common general knowledge, one cannot
> just take those parts of it that support (or rebut) the objection that
> is being made. To do so opens oneself up to an accusation of ex post
> facto selection. The notional skilled person comes armed with all the
> common general knowledge and cannot pick and choose selectively with
> the benefit of hindsight. Some aspects of the common general knowledge
> may lead the skilled person from the prior art towards the inventive
> concept; but equally other aspects of common general knowledge may
> lead him away from the inventive concept.

### 3.33.2 {#ref3-33-2}

The Patents Court in [KCI Licensing Inc & Ors v Smith & Nephew Plc & Ors
\[2010\] EWHC
1487](https://www.bailii.org/ew/cases/EWHC/Patents/2010/1487.html){rel="external"}
identified a further class of information, which, although not part of
the common general knowledge, may nevertheless be taken into account in
determining obviousness. In a passage endorsed by the Court of Appeal
(\[2010\] EWCA Civ 1260), Arnold J observed that:

> even if information is neither disclosed by a specific item of prior
> art nor common general knowledge, it may nevertheless be taken into
> account as part of a case of obviousness if it is proved that the
> skilled person faced with the problem to which the patent is addressed
> would acquire that information as a matter of routine. For example, if
> the problem is how to formulate a particular pharmaceutical substance
> for administration to patients, then it may be shown that the skilled
> formulator would as a matter of routine start by ascertaining certain
> physical and chemical properties of that substance (e.g. its aqueous
> solubility) from the literature or by routine testing. If so, it is
> legitimate to take that information into account when assessing the
> obviousness of a particular formulation. But that is because it is
> obvious for the skilled person to obtain the information, not because
> it is common general knowledge.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Identifying the inventive concept]{#default-id-0a305b24-heading-14 .govuk-accordion__section-button} {#identifying-the-inventive-concept .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Identifying the inventive concept\",\"index_section\":14,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-14 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-14" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Identifying the inventive concept\",\"index_section\":14,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.34 {#ref3-34}

Applying Windsurfing International v Tabur Marine and [Molnlycke v
Procter & Gamble Ltd \[1994\] RPC
49](https://doi.org/10.1093/rpc/1994rpc49){rel="external"} Jacob J
observed in [Unilever PLC v Chefaro Proprietaries Ltd \[1994\] RPC
567](https://doi.org/10.1093/rpc/1994rpc567){rel="external"} at page 580
that it is the inventive concept of the claim in question which must be
considered, not some generalised concept to be derived from the
specification as a whole. Different claims can, and generally will, have
different inventive concepts. The first stage in identifying the
inventive concept of a claim is likely to involve a purposive
construction of the claim ([see
125.14](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-14))
-- what does it mean to the skilled person? However merely doing that
can be too wooden because one does not distinguish between portions
which matter and portions which, although limiting the ambit of the
claim, do not. It is the essence of the claim that should be identified
when considering the inventive concept. Finding that essence will
involve constructing something akin to a précis, stripping out
unnecessary verbiage from the purposively construed claim.

### 3.34.1 {#ref3-34-1}

In [Generics (UK) Limited and others v H Lundbeck A/S
\[2009\]](http://rpc.oxfordjournals.org/content/126/6/407.abstract?sid=b743ad69-fc34-4d39-803a-210deacfe599){rel="external"}
UKHL 12, \[2009\] RPC 13, Lord Walker explained that there is a
difference between the "inventive concept" of a claimed invention and
its "technical contribution to the art". He stated at paragraph 30:

> Inventive concept" is concerned with the identification of the core
> (or kernel, or essence) of the invention---the idea or principle, of
> more or less general application (see [Kirin-Amgen \[2005\] RPC
> 169](https://doi.org/10.1093/rpc/2005rpc9){rel="external"} paras
> 112-113) which entitles the inventor's achievement to be called
> inventive. The invention's technical contribution to the art is
> concerned with the evaluation of its inventive concept---how far
> forward has it carried the state of the art? The inventive concept and
> the technical contribution may command equal respect but that will not
> always be the case.

This case related to a straightforward product claim. The House of Lords
held that the novel and non-obvious product claimed formed the technical
contribution to the art, whilst the process of how it had been made
formed the inventive concept (see also [Examination Guidelines for
Patent Applications Relating to Chemical
Inventions](https://www.gov.uk/government/publications/examining-patent-applications-relating-to-chemical-inventions).

### 3.34.2 {#ref3-34-2}

Although the inventive concept can in certain respects be broader than
the claim (because immaterial features of the claim may be ignored), it
cannot be narrower than the claim. In [Datacard Corp. v Eagle
Technologies Ltd. \[2011\] EWHC 244 (Pat), \[2011\] RPC
17](https://doi.org/10.1093/rpc/rcr010){rel="external"}, Arnold J held
that the inventive concept cannot be defined in terms which apply only
to a narrow sub-group of embodiments with certain technical advantages,
and which do not apply to the rest of the claim. If the patentee has
chosen to claim the invention broadly, the inventive concept must be of
at least equivalent breadth.

### 3.35 {#ref3-35}

In [Raychem Corp.'s Patents \[1998\] RPC
31](https://doi.org/10.1093/rpc/1998rpc31){rel="external"} (upheld on
appeal - \[1999\] RPC 497) the practice of drafting claims in an
unnecessarily complicated way was criticised. It was pointed out that a
properly drafted claim will state the inventive concept concisely, but
it was held that where the claims were prolix and opaque the court
should break free of the language and concern itself with what they
really meant.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The state of the art]{#default-id-0a305b24-heading-15 .govuk-accordion__section-button} {#the-state-of-the-art .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The state of the art\",\"index_section\":15,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-15 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-15" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The state of the art\",\"index_section\":15,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.36 {#section-8}

The state of the art for the purposes of deciding whether an invention
is obvious is defined by s.2(2); that is, it includes everything which
has been made available to the public, anywhere in the world, before the
priority date of the invention ([see further
2.21-2.29](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-21)).
Matter which forms part of the state of the art by virtue of s.2(3) is
specifically excluded from consideration.

### 3.37 \[moved to 3.75.1\] {#moved-to-3751}
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Consideration of the prior art: age of documents and other criteria]{#default-id-0a305b24-heading-16 .govuk-accordion__section-button} {#consideration-of-the-prior-art-age-of-documents-and-other-criteria .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Consideration of the prior art: age of documents and other criteria\",\"index_section\":16,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-16 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-16" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Consideration of the prior art: age of documents and other criteria\",\"index_section\":16,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.37.1 {#ref3-37-1}

Any disclosure falling within the s.2(2) field may be used as the
starting-point for an inventive step objection. The objection cannot be
overcome by an argument that the skilled person would simply not have
discovered a document in the course of their work, if that document has
been made public anywhere in the world, in any language, at any time
before the priority date. (However, as discussed at
[3.40-3.42](#ref3-40), this does not necessarily apply when considering
whether the skilled person would have considered a further document
together with the first document, as then the likelihood of the skilled
person considering the two documents together must be assessed.) For
example, in [Wake Forest University Health Sciences & Ors v Smith &
Nephew Plc & Anor \[2009\] EWCA Civ
848](https://www.bailii.org/ew/cases/EWCA/Civ/2009/848.html){rel="external"},
the prior art in question was a Russian language research paper
("Bagautdinov") which had been shown to have been made available in four
libraries in the former Soviet Union. Jacob LJ noted that it was highly
unlikely that the inventor or anyone involved in the litigation knew of
its existence at the priority date of the claimed invention. However, he
observed (emphasis added) that "Mr Alexander \[the patentee's
representative\] accepts, as he must, that the skilled person will have
read Bagautdinov and have done so carefully but with no imagination".
This general principle was set out by Laddie J in [Pfizer Ltd's Patent
\[2001\] FSR
16](https://uk.westlaw.com/Document/IE0E6F1E0E42711DA8FC2A0F0355337E9/View/FullText.html){rel="external"}:

> A real worker in the field may never look at a piece of prior art for
> example he may never look at the contents of a particular public
> library or he may be put off because it is in a language he does not
> know. But the notional addressee is taken to have done so. This is a
> reflection of part of the policy underlying the law of obviousness.
> Anything which is obvious over what is available to the public cannot
> subsequently be the subject of valid patent protection even if, in
> practice, few would have bothered looking through the prior art or
> would have found the particular items relied on.

### 3.37.2 {#ref3-37-2}

However, this does not mean that all prior art will be accorded equal
weight or significance; any piece of prior art must be viewed through
the eyes of the skilled person at the priority date, and the common
general knowledge in the field may cause them to disregard it. For
example, in [Actavis v Merck \[2008\] RPC
26](https://doi.org/10.1093/rpc/rcn023){rel="external"}, the Court of
Appeal considered whether the use of a drug to treat alopecia was
obvious over a prior art document which disclosed the use of the same
drug, to treat the same condition, at a different dosage. It was held
that, at the time the document was published, the claimed invention
would have been obvious from this disclosure. However, by the priority
date, the accepted wisdom in the field would suggest that this drug
would be ineffective at any dosage. Jacob LJ therefore highlighted the
importance of assessing inventiveness at the priority date, and not
before or after:

> one might assume that when an invention becomes obvious it must remain
> so thereafter. But such an assumption would be wrong: obviousness must
> be determined as of a particular date. There is at least one other
> well-known example showing how an invention which might be held
> obvious on one date, would not be so held at a later date. That is
> where there has been commercial success following a long-felt want.
> Time can indeed change one's perspective. The perspective the court
> must bring to bear is that of the skilled man at the priority date and
> not any earlier time.

### 3.38 {#ref3-38}

An old specification which teaches specifically the solution of the
problem which an invention seeks to overcome so that the skilled person
should readily appreciate its significance can form a good basis for an
obviousness objection ([Jamesigns (Leeds) Limited's Application \[1983\]
RPC 68](https://doi.org/10.1093/rpc/1983rpc68){rel="external"}). A
further example of a circumstance in which a lack of inventive step
objection based on an old document could be sustained is a case where
the modification of the older invention could not have been effected
before recent technological advances had been made, such as the
development of a new material. Documents which have resulted in
practical application or which are acknowledged as well known are also
likely to have greater force.

### 3.39 {#ref3-39}

Be wary of uncritical ageism in relation to the prior art. In [Brugger
and others v Medic-Aid Ltd (\[1996\] RPC
635](https://doi.org/10.1093/rpc/1996rpc635){rel="external"}) Laddie J
held (at 653 and 655) that:

> The fact that a document is old does not, per se, mean that it cannot
> be a basis for an obviousness attack. On the contrary, if a
> development of established and ageing art is or would be obvious to
> the skilled worker employed by a hungry new employer, it cannot be the
> subject of valid patent protection even if those who have been in the
> trade for some time, through complacency or for other reasons, have
> not taken that step. Each pleaded piece of prior art must therefore be
> assessed as if it was being considered afresh at the priority date. It
> is not to be excluded from this exercise merely because it is old.
> There is no rule of commerce that every new product or process must be
> developed and put on the market or published in literature as soon as
> it becomes obvious.

and

> It is only when the answer to the question "why was this not developed
> earlier" is "a likely and reasonable explanation is that people
> looking for a way round an existing problem did not see this as the
> answer" that the age of the prior art should play a part in meeting an
> obviousness attack. If it is likely that in the real world no one was
> looking for an answer the fact that none was found says nothing about
> whether the answer proposed in the patent under attack was obvious.

### 3.39.1 {#ref3-39-1}

In [Merck Sharp & Dohme Corp v Teva UK Ltd \[2011\] EWCA Civ
382](https://www.bailii.org/ew/cases/EWCA/Civ/2011/382.html){rel="external"}
the argument was made that a document published only six days before the
priority date was not relevant for inventiveness, as it would not have
been possible to perform the steps leading from the prior art to the
claimed invention in the period between the publication of the prior art
and the priority date. This argument was decisively rejected by the
Court of Appeal; all that needs to be decided is whether the claimed
invention is obvious over the prior art, not whether there would in fact
be time to arrive at the invention by the priority date.

### 3.39.2 {#ref3-39-2}

In [Emson v Hozelock \[2020\] EWCA Civ
871](https://www.bailii.org/ew/cases/EWCA/Civ/2020/871.html){rel="external"}
the Court of Appeal dealt with the issue of assessing the obviousness of
a technically simple invention (a garden water hose), where the item of
prior art under consideration belonged to a technical field remote from
the invention (an aerospace oxygen supply hose) that the skilled person
would be unfamiliar with. The court endorsed the view of the original
trial judge that the skilled person was ''not specifically a garden hose
designer but a designer of hoses in general'' and as a result the
skilled person would read the prior art with interest ''even though the
\[prior art\] hose is designed for use in a very particular and
different field.''

See also [3.78-3.79](#ref3-78), [3.80-3.81.2](#ref3-80) and
[3.97-3.101](#ref3-97).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Combining documents, "mosaicing"]{#default-id-0a305b24-heading-17 .govuk-accordion__section-button} {#combining-documents-mosaicing .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Combining documents, “mosaicing”\",\"index_section\":17,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-17 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-17" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Combining documents, “mosaicing”\",\"index_section\":17,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.40 {#ref3-40}

While it is not possible to combine the disclosure of a given document
with other matter to demonstrate lack of novelty ([see
2.09](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-09)),
it is permitted to combine any of the prior art (whether published
documents, instances of prior use or common knowledge) in order to argue
that an inventive step is lacking. However, although a single
disclosure, however remote, of the whole invention will destroy novelty,
in order to establish that a combination of teachings from the prior art
shows an invention to be obvious, it must be likely that the skilled
person would have considered those teachings together. Laddie J in
[Pfizer Ltd's Patent \[2001\] FSR
16](https://uk.westlaw.com/Document/IE0E6F1E0E42711DA8FC2A0F0355337E9/View/FullText.html){rel="external"}
at paragraph 66 stated:

> When any piece of prior art is considered for the purposes of an
> obviousness attack, the question asked is "what would the skilled
> addressee think and do on the basis of the disclosure?" He will
> consider the disclosure in the light of the common general knowledge
> and it may be that in some cases he will also think it obvious to
> supplement the disclosure by consulting other readily accessible
> publicly available information. This will be particularly likely where
> the pleaded prior art encourages him to do so because it expressly
> cross-refers to other material. However, I do not think it is limited
> to cases where there is an express cross-reference. For example if a
> piece of prior art directs the skilled worker to use a member of a
> class of ingredients for a particular purpose and it would be obvious
> to him where and how to find details of members of that class, then he
> will do so and that act of pulling in other information is itself an
> obvious consequence of the disclosure in the prior art.

### 3.41 {#section-9}

There is no simple rule as to whether information from different
documents, or from different parts of a single document, can properly be
combined as a "mosaic" to provide a case that an invention is obvious.
The greater the number of documents which must be so combined to reach
the invention, the more likely on the whole that there is an inventive
step, but regard must be paid to the nature of the features which are
combined. The combination of a series of known features, each playing
its usual part in the final entity, is often simply a matter of design
or mere collocation, and not of invention [see 3.17](#ref3-17).

### 3.42 {#ref3-42}

In [Dow Chemical Company (Mildner's Patent), \[1973\] RPC
804](https://doi.org/10.1093/rpc/90.26.804){rel="external"}, Whitford J
indicated that in order to establish obviousness from a combination of
documents it is necessary to consider the extent to which you can
conclude that the documents are ones which the seeker after information
would come across and would consider together. Two extremes that are
sometimes put forward are (a) that no two documents may be combined to
make a mosaic unless at least one is well known, and (b) that all the
information in any set of documents can be combined provided they are
all in the same art. Neither of these extremes is acceptable as a
general principle. Although the second extreme may be the more likely to
reflect the true situation it should not be used as a pretext for not
investigating beyond the immediate field of the invention see
[3.26-3.27](#ref3-26).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Factors to consider before combining documents]{#default-id-0a305b24-heading-18 .govuk-accordion__section-button} {#factors-to-consider-before-combining-documents .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Factors to consider before combining documents\",\"index_section\":18,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-18 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-18" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Factors to consider before combining documents\",\"index_section\":18,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.43 {#section-10}

In deciding whether or not it is obvious to combine the disclosure in
two or more documents, the following considerations are likely to be
relevant:-

\(a\) How the nature and the contents of the documents influence whether
the person skilled in the art would combine them. For example where the
disclosed features seem at first sight to have an inherent
incompatibility or where one document has a tendency to lead from the
mosaic, this would be a pointer towards the combinations being inventive
[see 3.91](#ref3-91)

\(b\) Whether the documents came from the same technical field or from
neighbouring or remote technical fields [see 3.26-3.28.2](#ref3-26) and
[see 3.44](#ref3-44)

\(c\) The presence of references in one document to another

\(d\) The amount of selection required to isolate the separate
disclosures from the surrounding documentary material

\(e\) Whether the contents of one document are so well known that the
skilled person would always have them in mind in reading other documents
[see 3.45](#ref:3-45)

\(f\) The age of the documents [see 3.37.2-3.39.1](#ref3-37-2)

### 3.44 {#ref3-44}

Where the documents are from different technical fields the question is
whether the problem would have prompted search in those fields. In [Dow
Chemical Company (Mildner's) Patent \[1973\] RPC
804](https://doi.org/10.1093/rpc/90.26.804){rel="external"}, an
invention residing in an electrical cable in which a plastics jacket was
securely bonded to a metal shield using a specified copolymer was held
to be obvious in the light of one document disclosing all the features
of the cable but not mentioning the adhesive copolymer, and other
documents disclosing the copolymer. Although these latter documents did
not refer to cable manufacture, they did refer to the copolymer as
having high moisture resistance and being suitable for bonding plastics
to metal, both essential properties in adhesives for use in cables. It
was therefore reasonable to expect the skilled person concerned with the
problem of adhering plastics to metal in cables to have found and
considered these documents. The Technical Board of Appeal of the EPO has
considered it reasonable to expect a person skilled in the art, unable
to fulfil a need in the relevant field, to look for suitable parallels
in a neighbouring field so closely related that they would take
developments therein into account, or in the broader general field in
which the same or similar problems extensively arise and of which they
must be expected to be aware (Decision T 176/84, OJEPO 2/86).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Combining documents with common general knowledge]{#default-id-0a305b24-heading-19 .govuk-accordion__section-button} {#combining-documents-with-common-general-knowledge .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Combining documents with common general knowledge\",\"index_section\":19,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-19 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-19" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Combining documents with common general knowledge\",\"index_section\":19,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.45 {#ref3-45}

If the invention can be produced by combining the teaching of one
document with common general knowledge or with standard practice in the
art, then even if the inventor has not conceived it nor the applicant
presented it in such terms, there is a strong presumption that such a
combination would be obvious to the skilled person. If, in their
application, the applicant refers to prior art as "conventional", this
may be taken to indicate that the prior art is common general knowledge
([NEC Corporation's Application BL
O/038/00](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F038%2F00&submit=Go+%BB){rel="external"})
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Examining for inventive step]{#default-id-0a305b24-heading-20 .govuk-accordion__section-button} {#examining-for-inventive-step .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Examining for inventive step\",\"index_section\":20,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-20 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-20" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Examining for inventive step\",\"index_section\":20,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### A warning against overelaborating the question of obviousness

### 3.46 {#ref3-46}

When considering an inventive step objection the examiner should always
bear in mind this warning from Jacob LJ in [Angiotech Pharmaceuticals v
Conor Medsystems Inc \[2007\] EWCA Civ
5v](https://www.bailii.org/ew/cases/EWCA/Civ/2007/5.html){rel="external"}:

> one can overelaborate a discussion of the concept of "obviousness" so
> that it becomes metaphysical or endowed with unwritten and unwarranted
> doctrines, sub- doctrines or even sub-sub-doctrines. .... In the end
> the question is simply "was the invention obvious?
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [A reminder to consider obviousness whenever a novelty objection is overcome]{#default-id-0a305b24-heading-21 .govuk-accordion__section-button} {#a-reminder-to-consider-obviousness-whenever-a-novelty-objection-is-overcome .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"A reminder to consider obviousness whenever a novelty objection is overcome\",\"index_section\":21,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-21 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-21" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"A reminder to consider obviousness whenever a novelty objection is overcome\",\"index_section\":21,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.47 {#section-11}

The examiner should always consider the obviousness of a claim when a
novelty objection in respect of that claim has been overcome.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Determining obviousness]{#default-id-0a305b24-heading-22 .govuk-accordion__section-button} {#determining-obviousness .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Determining obviousness\",\"index_section\":22,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-22 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-22" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Determining obviousness\",\"index_section\":22,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.48 {#ref3-48}

The determination as to whether an invention is obvious or not is of
considerable importance both to applicants and the public, and the
substantive examiner must be prepared to put considerable effort into
this task. It is particularly important that the examiner correctly
identifies the relevant common general knowledge that is to be imparted
to the skilled person. In conferring on the substantive examiner power
to consider inventive step, s.18(2) implies that the examiner is
required to exercise an expertise, and that they must be recognised as
qualified to do so. This view has been reinforced by Pumfrey J in
[Degussa-Huls AG v The Comptroller-General of Patents \[2005\] RPC
29](https://doi.org/10.1093/rpc/2005rpc29){rel="external"}:

> Examiners are appointed because of their general technical skills.
> They will, in the classes which they examine, acquire extensive
> knowledge and they will be able to form a clear view of the qualities
> which are to be expected of the person skilled in the art in that
> particular field.

### 3.49 {#ref3-49}

The nature of the expertise implied depends on the matter to be
determined and on the circumstances of the application. For example the
substantive examiner is not in most cases in a position to deny simply
from their own knowledge facts (such as whether a given technique is
well-known, or whether a given reaction has specified by-products) to
which evidence has been brought forward from a witness -possibly the
applicant themselves - who can claim relevant expert knowledge. In such
circumstances the examiner is likely to have to accept the evidence from
the applicant unless they can produce documentary evidence of contrary
effect. Nevertheless, the substantive examiner must be taken to be
qualified to decide, given the prior art and other relevant technical
facts, whether the resulting position implies the presence or absence of
an inventive step.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The Windsurfing/Pozzoli approach]{#default-id-0a305b24-heading-23 .govuk-accordion__section-button} {#the-windsurfingpozzoli-approach .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The Windsurfing/Pozzoli approach\",\"index_section\":23,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-23 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-23" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The Windsurfing/Pozzoli approach\",\"index_section\":23,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.50 {#ref3-50}

From the discussions at the beginning of this Section it is clear that
the examiner must use the Windsurfing/Pozzoli approach when deciding
whether or not to object and, if objecting, setting out that objection.
Before raising a lack of inventive step objection a substantive examiner
should be clear in their own mind as to what point in the prior art they
are starting from and the nature and number of steps it would take for a
person skilled in the art to get from the starting point to the
invention.

### 3.51 {#section-12}

The examiner need not spend an undue amount of time in debating the
precise nature of the skilled person, whether that person is a designer,
a technician, a manufacturer, etc. As indicated in 3.20 the character of
this person is largely set by the relevant common general knowledge
imparted to them, rather than their position in any organizational
chart.

### 3.51.1 {#ref3-51-1}

In [Eli Lilly & Co. v Human Genome Sciences, Inc. \[2008\] EWHC 1903
(Pat), \[2008\] RPC
29](https://doi.org/10.1093/rpc/rcn027){rel="external"}, which related
to a patent for a polynucleotide sequence encoding a Neutrokine-
polypeptide, Kitchin J. applied the Windsurfing/Pozzoli test and reached
the conclusion that the claimed invention was not obvious from the prior
art. However, he nevertheless held that the claims lacked an inventive
step on the grounds that the patent made no technical contribution to
the art and did not solve a technical problem. Kitchin J. held that this
should be determined by considering whether the invention lies in making
the products of the claim or rather whether the invention must lie in a
disclosure that the DNA products of the claim code for useful proteins
and, if so, whether the specification does no more than speculate as to
what those uses might be. Any deficiency in this regard cannot be
remedied by evidence coming into existence after the application has
been filed. However, this objection appears to be specific to certain
types of biotechnology patents, wherein the inventiveness of a nucleic
acid or protein lies in the identification of its function rather than
in its production per se.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Consider different starting points]{#default-id-0a305b24-heading-24 .govuk-accordion__section-button} {#consider-different-starting-points .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Consider different starting points\",\"index_section\":24,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-24 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-24" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Consider different starting points\",\"index_section\":24,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.52 {#section-13}

A matter which requires invention when tackled from one starting point
may be commonplace when started from a different disclosure or with a
different known problem in mind. The applicant may for example have
presented their invention as a combination of features A, B, C, D which
they admit are known in combination, with a further feature E which it
would undoubtedly be inventive to add to the acknowledged combination.
It may be however that a prior document discloses the combination of
features A and E, and that the addition of the remaining features B, C,
D is then obvious.

### 3.53 {#section-14}

The starting point of the invention from the point of view of the
applicant may be apparent from the specification. For example, the
description may contain an account of the prior art and/or the problem
to be solved, or the main claim may be in the two-part form, in which a
preamble specifying a set of features known in combination in the prior
art is followed by a characterising portion setting out the new
features. The applicant is however not obliged to do this, nor does a
reader of the specification have to accept the applicant's assessment of
the invention.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [No rules of thumb; precedents to be treated with caution]{#default-id-0a305b24-heading-25 .govuk-accordion__section-button} {#no-rules-of-thumb-precedents-to-be-treated-with-caution .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"No rules of thumb; precedents to be treated with caution\",\"index_section\":25,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-25 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-25" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"No rules of thumb; precedents to be treated with caution\",\"index_section\":25,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.54 {#ref3-54}

The decision to raise an objection of lack of inventive step must be
made on a proper consideration of the facts of the particular case and
any kind of rule of thumb approach should be avoided. Caution should be
exercised in relying on precedent cases, since, more than with any other
topic to be decided by the substantive examiner, attempts to line up a
particular case with some decided case can mislead.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Raise any sound objection]{#default-id-0a305b24-heading-26 .govuk-accordion__section-button} {#raise-any-sound-objection .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Raise any sound objection\",\"index_section\":26,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-26 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-26" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Raise any sound objection\",\"index_section\":26,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.55 {#ref3-55}

The substantive examiner should not raise an objection when they have no
good reason to suppose that there is no invention just to fish for a
weakness in the applicant's position. As in any other area of
examination it is thoroughly bad practice for an examiner to raise an
objection which, with thought, they would see was not justified, merely
to pass the burden of thinking about the matter to the applicant.

### 3.56 {#section-15}

However, even if the substantive examiner thinks they can foresee how
the applicant will amend or argue to overcome the objection, the
examiner should raise any sound obviousness objection. The applicant may
amend in any way they think fit; there is no guarantee that the
applicant will act as the examiner has predicted.

### 3.57 {#ref3-57}

A substantive examiner faced with a prima facie case of obviousness
which might be capable of rebuttal by special information of a sort
which is not available to the examiner, but which must be available to
the applicant, should always put the objection to the applicant. An
example of such information is whether the applicant's process avoids a
drawback which experts in the art would expect it to have, or whether
the applicant is merely prepared to tolerate the drawbacks, so that they
have not made any inventive contribution. If the applicant fails to make
a satisfactory answer to such an objection the examiner can safely
assume that this is because the facts are against the applicant [see
3.64](#ref3-64).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Mosaics]{#default-id-0a305b24-heading-27 .govuk-accordion__section-button} {#mosaics .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Mosaics\",\"index_section\":27,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-27 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-27" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Mosaics\",\"index_section\":27,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.58 {#section-16}

In practice the guidance above on combining or "mosaicing" documents can
be difficult to satisfy [see 3.40--3.44](#ref3-40). In particular the
examiner must explain why they believe that the skilled person would
make that combination of documents.

### 3.59 {#section-17}

The examiner must always keep sight of what use they are making of the
cited documents. Oftentimes the examiner is not making a true
combination in the sense of using a combination of two or more documents
to arrive at a solution to a particular problem. Instead the examiner
will find that they are using some of those documents to illustrate the
common general knowledge, acknowledged prior art or some other point as
in 3.62. In these circumstances the examiner may find the combination
easier to justify.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Content of the report]{#default-id-0a305b24-heading-28 .govuk-accordion__section-button} {#content-of-the-report .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Content of the report\",\"index_section\":28,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-28 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-28" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Content of the report\",\"index_section\":28,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.60 {#ref3-60}

In addition to citing documents the first examination report should
state a prima facie case, not going into overmuch detail. This prima
facie case does not have to explicitly mention Windsurfing or Pozzoli,
but it should leave the applicant in no doubt that the
Windsurfing/Pozzoli approach has been adopted. The aim should be to
oblige the applicant to state what they consider the inventive step to
be and to justify their assertion. This first report should, so far as
is practicable, set out explicitly any assumptions of a technical
character which have been made. Where it seems unlikely that the
applicant will challenge these assumptions, support for the assumptions
need not be sought unless and until the applicant challenges them (but
[see 3.62](#ref3-62)).

### 3.61 {#section-18}

Subsequent reports responding to the applicant's amendments and/or
arguments should be more detailed, explicitly setting out the examiner's
reasoning for each of the Windsurfing steps.

### 3.62 {#ref3-62}

When matter considered to be well-known in the art is used in
formulating an obviousness objection, it need not be supported by
document references unless and until it is challenged. However it is
probably best that all document references that the examiner will be
using are put before the applicant at the earliest opportunity. When
documents are cited as examples illustrating common general knowledge or
background art, for example, where the contention is that the invention
claimed comprises an obvious modification to known apparatus and
documents are cited, one showing the modification per se and one or more
others exemplifying the apparatus, this should be made clear at the
outset. Documents should be included in the initial citations where
necessary to substantiate background art considered to fall short of
well-known. It should be made clear whether documents cited are being
relied on individually or in combination and/or in conjunction with
common knowledge.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Reporting both novelty and inventive step objections]{#default-id-0a305b24-heading-29 .govuk-accordion__section-button} {#reporting-both-novelty-and-inventive-step-objections .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Reporting both novelty and inventive step objections\",\"index_section\":29,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-29 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-29" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Reporting both novelty and inventive step objections\",\"index_section\":29,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.63 {#ref3-63}

In putting to the applicant objections of lack of novelty and inventive
step the substantive examiner should make clear which claims are
attacked on each of these grounds. Where the examiner attacks the main
claims but defers consideration of the inventiveness of subordinate
claims they should make this clear to the applicant ([see
18.43](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-43))
In attacking subordinate claims, where it is not clear what response the
applicant is likely to make to the objections to the main claims, it is
sufficient to indicate the general nature of an objection to lack of
inventive step leaving any elaboration until this is made necessary by
the applicant's response.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Responding to the applicant's case]{#default-id-0a305b24-heading-30 .govuk-accordion__section-button} {#responding-to-the-applicants-case .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Responding to the applicant’s case\",\"index_section\":30,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-30 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-30" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Responding to the applicant’s case\",\"index_section\":30,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.64 {#ref3-64}

An objection should not be withdrawn merely because an argued response
has been submitted - the argument must be adequate to overcome the
objection. In [Degussa-Huls AG v The Comptroller-General of Patents
\[2005\] RPC 29](https://doi.org/10.1093/rpc/2005rpc29){rel="external"},
in dismissing an appeal against the Hearing Officer's finding of
obviousness, Pumfrey J held that:

> when a case of prima facie obviousness or anticipation is made out ...
> the evidential burden shifts on to the applicant to produce evidence
> upon which the Examiner can properly act in saying either that there
> is insufficient material before him to say that the objection of
> obviousness is in fact made out or possibly even to say that the
> objection of obviousness must, on the material available to him, fail
> ...

> Where an Examiner forms a view upon what, at first sight, are sound
> grounds for supposing there is a strong case of obviousness, then it
> is up to the applicant to put convincing material in front of the
> Examiner upon which, as I have said, he can act contrary to what was
> to him the apparent position.

### 3.65 {#section-19}

In that case, evidence that the invention had unforeseeable advantages
which were unrelated to the reasons for the initial finding of
obviousness did not overcome this finding. Evidence from the applicant,
or from a witness provided by the applicant, to the effect that the
situation does imply an inventive step (as distinct from evidence on a
point of fact), must therefore be regarded solely as argument which the
examiner accepts or rejects of their own judgement, supplemented of
course by whatever advice they decide to seek from their colleagues.

### 3.66 {#ref3-66}

A technical assertion on behalf of the applicant which is unsupported by
published material should be in the form of sworn evidence unless the
substantive examiner can confirm independently that it is correct. It
should be clear who is making the assertion and what their status is. A
statement attributed to a third party, whether identified or not, is
hearsay evidence - see
[123-18](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-18).

If the assertion runs counter to what is published, this fact should be
commented on and justified. Any evidence should preferably be filed
before the matter is brought to a hearing since if it is not then it is
unlikely to be allowed to be filed in support of an appeal..
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Standard of certainty, benefit of the doubt]{#default-id-0a305b24-heading-31 .govuk-accordion__section-button} {#standard-of-certainty-benefit-of-the-doubt .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Standard of certainty, benefit of the doubt\",\"index_section\":31,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-31 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-31" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Standard of certainty, benefit of the doubt\",\"index_section\":31,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.67 {#ref3-67}

When a prima facie objection of lack of inventive step is contested the
examiner will determine the matter on the balance of the evidence
available, the standard of certainty being the same pre-grant as
post-grant, ie it is determined on the balance of probabilities.

### 3.68 {#ref3-68}

An objection of obviousness should not be pursued if there is a genuine
possibility that there is an inventive step. The possibility must
however be real and it is far from sufficient in rebuttal of an
objection that there is merely a case to be answered, or that the
applicant asserts that there is doubt. The matter should be decided on
the balance of the evidence available.

### 3.69 {#ref3-69}

If the substantive examiner is unable to reach a conclusion on inventive
step because of lack of technical knowledge which they cannot readily
rectify and there seems a strong prima facie case that the invention is
obvious, it is reasonable for the examiner to put a specific query to
the applicant or to object that there is no inventive step and see what
reply the applicant makes. If expert evidence would be required for them
to judge whether the applicant's reply to an objection establishes that
there is invention, only then must the applicant be given the benefit of
the doubt. This is consistent with the test applied in [Blacklight Power
Inc. v The Comptroller-General of Patents \[2009\] RPC
6](https://doi.org/10.1093/rpc/rcn035){rel="external"} (see 4.05.2 for a
case where there was substantial doubt on a question of the scientific
validity of the basis of an invention). In this case, the judge held
that the examiner should consider whether the evidence provided by the
applicant gives rise to a reasonable prospect that, if the issue were to
be fully investigated at trial with the benefit of expert evidence, it
would be resolved in the applicant's favour.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Other matters]{#default-id-0a305b24-heading-32 .govuk-accordion__section-button} {#other-matters .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Other matters\",\"index_section\":32,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-32 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-32" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Other matters\",\"index_section\":32,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.70 {#ref3-70}

The question of whether a claim involves an inventive step does not
normally arise if the claim lacks novelty. It would be illogical to say
that there is no step but that the step is non-inventive. In particular
the same prior art should not be formally cited against the same claims
under s.1(1)(a) and (b) simultaneously. However, in appropriate cases an
objection that an invention is not new may be followed by a "fall-back"
objection that if the claim were shown to be novel then it involves no
inventive step. Thus the substantive examiner should investigate whether
there is invention in a claim which they have objected to as lacking
novelty where the novelty objection depends, for example, on a verbal
coincidence or a matter of interpretation or construction and does not
strike at the basic inventive concept. When a document is relevant to a
lack of novelty objection against certain claims and also to an
obviousness objection against other claims at the same action, it should
be cited in respect of both objections. As a general rule, and subject
to the above considerations, the first report under s.18(3) should refer
(by formal citation or otherwise) to all the prior art documents
considered to be relevant to novelty and/or inventive step (although if
the documents referred to are only examples of many, that fact should be
stated without listing all of the many).

### 3.71 {#ref3-71}

The substantive examiner may also object that the claim is not clear but
that so far as the claim can be understood it appears that the invention
is not new, and/or does not involve an inventive step. If there is no
substantial novelty objection some indication of the prima facie case
for the lack of inventive step against the assumed invention should
normally be given.

\[RC5 should be used\].
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Subsection 3: Assessing obviousness]{#default-id-0a305b24-heading-33 .govuk-accordion__section-button} {#subsection-3-assessing-obviousness .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Subsection 3: Assessing obviousness\",\"index_section\":33,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-33 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-33" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Subsection 3: Assessing obviousness\",\"index_section\":33,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.72 {#ref3-72}

The question is, does the invention make available to the person skilled
in the art something that they would not reach by normal exercise of
their skill? If so, the inventor has made a contribution to the art
which provides the consideration justifying the grant of a patent. The
contribution must be of a technical nature. This is not to say that it
must be technically complex; simplicity does not count against an
invention and may indeed point to its being non-obvious. There may be
invention in appreciating commercial features, for example in realising
that there is a market for a new product. The Court of Appeal has
provided competing views on this, see [Hallen Co v Brabantia (UK) Ltd
\[1991\] RPC
195](https://doi.org/10.1093/rpc/1991rpc195){rel="external"} [at
3.07](#ref3-07).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Deciding the fourth Windsurfing/Pozzoli step]{#default-id-0a305b24-heading-34 .govuk-accordion__section-button} {#deciding-the-fourth-windsurfingpozzoli-step .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Deciding the fourth Windsurfing/Pozzoli step\",\"index_section\":34,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-34 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-34" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Deciding the fourth Windsurfing/Pozzoli step\",\"index_section\":34,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.73 {#section-20}

Many different approaches to deciding the fourth Windsurfing/Pozzoli
step, the substantive assessment of inventive step, have been put
forward over the years. What follows are discussions of some of the more
prominent suggestions. The approach taken at this stage will depend upon
the particular circumstances of the case under consideration.

### 3.74 {#section-21}

One may also wish to consider the discussions of these and other
approaches available within guides such as "Terrell on the Law of
Patents" 19th (2020) Edition and the "CIPA Guide to the Patents Acts"
(8th Edition). The locations of the relevant discussions in these guides
are summarized in the table below.

  ----------------------------------------- --------------------------- ----------------
  **Topic**                                 **Terrell**                 **CIPA Guide**
  simplicity no objection                   12.87                       3.26
  avoidance of hindsight                    12.134                      3.40
  technical not commercial obviousness      12.154                      3.33
  obvious to try                            12.74                       3.30
  long-felt want; commercial success        12.95                       3.32, 3.33
  right to work                             \-                          3.26
  lying in the road                         \-                          3.26
  "would" not "could"                       12.114                      3.12
  if obvious, why was it not done before?   \-                          3.34
  selection                                 12.122                      3.31
  overcoming a technical prejudice          12.19-12.29, 12.97-12.102   3.17
  ----------------------------------------- --------------------------- ----------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The Haberman questions]{#default-id-0a305b24-heading-35 .govuk-accordion__section-button} {#the-haberman-questions .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The Haberman questions\",\"index_section\":35,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-35 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-35" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The Haberman questions\",\"index_section\":35,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.75 {#ref3-75}

A number of issues should be considered in determining whether a
development is obvious or not. In [Haberman v Jackel \[1999\] FSR
683](https://uk.westlaw.com/Document/IB62E0E20E42711DA8FC2A0F0355337E9/View/FullText.html){rel="external"}
(at 699 to 701), Laddie J considered the following non-exhaustive list
of relevant questions some of which may not be answerable before grant
or without evidence:

\(a\) What was the problem which the patented development addressed?\
(b) How long had that problem existed?\
(c) How significant was the problem seen to be?\
(d) How widely known was the problem and how many were likely to be
seeking a solution?\
(e) What prior art would have been likely to be known to all or most of
those who would have been expected to be involved in finding a
solution?\
(f) What other solutions were put forward in the period leading up to
the publication of the patentee's development?\
(g) To what extent were there factors which would have held back the
exploitation of the solution even if it was technically obvious?\
(h) How well had the patentee's development been received?\
(i) To what extent could it be shown that the whole or much of the
commercial success was due to the technical merits of the development?

### 3.75.1 {#ref3-75-1}

In determining whether an invention is obvious in the light of a given
document combined with common general knowledge, other documents, or
instances of prior use, there are two major considerations: (i) whether
the skilled person could reasonably be expected to find the document in
conducting a diligent search for material relevant to the problem in
hand [see 3.26](#ref3-26) and [see 3.44](#ref3-44)) and (ii) whether, if
they had found the document, they would have given it serious
consideration. So far as (ii) is concerned, relevant factors may be the
age of the document ([see 3.37.2](#ref3-37-2) and [see
3.39.1](#ref3-39-1)) and whether, if it is one of a large number, there
was any reason why the skilled person should have selected it ([see
3.88](#ref3-88)). Passages which lead away from the applicant's
invention must be taken into account as well as those that lead towards
it. It is relevant in looking at a prior document to consider whether
the matter of interest to the obviousness question constitutes a
principal feature of the prior document or whether it is mentioned
merely as a detail in the performance of an entirely different concept,
without any recommendation to the reader which would encourage them to
use it in different circumstances.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Right to work]{#default-id-0a305b24-heading-36 .govuk-accordion__section-button} {#right-to-work .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Right to work\",\"index_section\":36,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-36 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-36" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Right to work\",\"index_section\":36,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.76 {#ref3-76}

Just as an invention will lack novelty if the claim to it would
re-monopolise something already disclosed (the so-called
"post-infringement test" - [see
2.03](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-03)),
likewise it will be regarded as obvious if a claim to it would inhibit
the rights of a skilled workperson to carry out routine modifications of
what is already in the public domain. Just as the notion behind
anticipation is that it would be wrong to enable the patentee to prevent
a person from doing what they have lawfully done before the patent was
granted, that behind obviousness is that it would be wrong to prevent a
person from doing something which is merely an obvious extension of what
they have been doing or of what was known in the art before the priority
date of the patent granted (judgment of Court of Appeal in [Windsurfing
International Inc. v Tabur Marine (Great Britain) Ltd, \[1985\] RPC
59](https://doi.org/10.1093/rpc/1985rpc59){rel="external"} at page 77).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Lying in the road]{#default-id-0a305b24-heading-37 .govuk-accordion__section-button} {#lying-in-the-road .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Lying in the road\",\"index_section\":37,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-37 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-37" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Lying in the road\",\"index_section\":37,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.77 {#ref3-77}

In [Philips (Bosgra's) Application, \[1974\] RPC
241](https://doi.org/10.1093/rpc/91.8.241){rel="external"} the
applicants proposed to amend a claim (which had been held obvious) to a
method of producing a vaccine in order to specify the use of certain
emulsifying agents. The hearing officer held that the amended claim was
not obvious since, although these emulsifying agents were well known, it
was not certain that a notional research group would be directly led as
a matter of course to try these particular agents. On appeal however,
Whitford J ruled that this was not the correct question; although the
skilled person would not necessarily be led directly to try these
materials, they were obvious in the sense that they were lying in the
road (ob via) for the worker to use, and it was wrong that they should
be stopped by a monopoly from doing so. Moreover, just as it has been
established that to impugn novelty, prior enabling disclosure is
required, so a claim to a product will only be obvious if not only the
idea of the product is obvious but also a way of producing the product
is obvious (Boehringer Mannheim GmbH v Genzyme Ltd \[1993\] FSR 716).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Why was it not done before?]{#default-id-0a305b24-heading-38 .govuk-accordion__section-button} {#why-was-it-not-done-before .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Why was it not done before?\",\"index_section\":38,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-38 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-38" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Why was it not done before?\",\"index_section\":38,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
see also [3.37.2-3.39](#ref3-37-2) and [3.80-3.81.2](#ref3-80) and
[3.97-3.101](#ref3-97)

### 3.78 {#ref3-78}

The fact that no-one has followed a particular path before does not of
course dispose of an obviousness objection, otherwise any invention
which was new would automatically be inventive. However the reasons why
this has not been done before may well be important. If the inventor has
solved a long-standing problem by using in a conventional way materials
or techniques which have only recently become available, then this is
not inventive.

### 3.79 {#ref3-79}

Nor is it inventive to respond to a change in economic circumstances;
for example if a product has not been made from a particular material or
by a particular process for reason of cost, and the material or process
becomes cheaper or the market value of the product increases, it is not
inventive to take advantage of this. And if a newly-arisen problem is
solved by the use of available resources in an obvious way, then there
is no inventive step (unless the inventor has been the first to identify
the problem). But if the inventor has solved a long-recognised problem
by means which others could have used but did not, then there may be an
inventive step ([Minnesota Mining and Manufacturing Co v Rennicks (UK)
Ltd \[1992\] RPC
331](https://doi.org/10.1093/rpc/1992rpc331){rel="external"}). In Chiron
Corpn v Organon Teknika Ltd \[1994\] FSR 202 a claim to a polypeptide
comprising an antigenic determinant of the hepatitis C virus was found
to be non-obvious because despite the attempts of numerous research
groups over a 10 year period to identify the agent responsible for
Non-A, Non-B Hepatitis (latterly named Hepatitis C), the patentees
succeeded in a unique fashion by adopting a known technique which would
not have been obvious to try in the circumstances.

### 3.79.1 {#ref3-79-1}

In [Emson v Hozelock \[2020\] EWCA Civ
871](https://www.bailii.org/ew/cases/EWCA/Civ/2020/871.html){rel="external"}
the court reaffirmed that the age of prior art is a relevant
consideration in considering obviousness, but that there is a
distinction between prior art that has been ''long disregarded'' (which
would point towards inventiveness) and prior art that would simply be
unknown to the skilled person in their technical field, and therefore
likely to have remained unexploited. In this case the invention related
to a garden water hose and the prior art to an aerospace oxygen hose,
with the court finding that the skilled person working in the former
field would be unlikely to have been aware of, and therefore exploit,
developments in the latter.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Fulfilling a need]{#default-id-0a305b24-heading-39 .govuk-accordion__section-button} {#fulfilling-a-need .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Fulfilling a need\",\"index_section\":39,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-39 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-39" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Fulfilling a need\",\"index_section\":39,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
see also [3.37.2-3.39](#ref3-37-2) and [3.78-3.79](#ref3-78) and
[3.97-3.101](#ref3-97-3-10)

### 3.80 {#ref3-80}

Evidence that an invention fulfils a long-felt want and has been
commercially successful may be taken into account in assessing
obviousness (see for example [Hickman v Andrews, \[1983\] RPC
147](https://doi.org/10.1093/rpc/1983rpc147){rel="external"} and [PLG
Research Ltd v Ardon International Ltd, \[1993\] FSR
197](https://uk.westlaw.com/Document/I232CE320E42811DA8FC2A0F0355337E9/View/FullText.html){rel="external"}). 
Aldous J held in [Optical Coating Laboratory Inc. v Pilkington P.E. Ltd.
\[1995\] RPC
145](https://doi.org/10.1093/rpc/1995rpc145){rel="external"} at page 166
that while it is always important to consider why a possibly inventive
step had not been suggested before, without evidence of a long-felt want
or unsuccessful attempts to solve a particular problem, any evidence as
to novelty, years of delay in developing the prior art and an advantage
stemming from the invention carries no weight.

### 3.81 {#ref3-81}

Moreover the commercial success of the invention may be attributable to
factors achieved independently of the invention, such as the quality or
price of the product, or to superior marketing. In [Haberman v
Jackel](https://uk.westlaw.com/Document/IB62E0E20E42711DA8FC2A0F0355337E9/View/FullText.html){rel="external"}
[see 3.75](#ref3-75) the development was a small and simple change to a
"trainer cup" to make it leakproof. The new product achieved large
success despite small advertising budgets and "unconsidered aesthetics"
in its original version. The materials for the design had long been
readily available and the advantages were immediately apparent once it
was thought of. In this context, the commercial success was held to
demonstrate that the invention itself fulfilled a significant long-felt
want and that if the development had been obvious it would have been
found by others earlier. Similarly, in [Schlumberger Holdings Ltd v
Electromagnetic Geoservices AS \[2010\] RPC
33](https://www.bailii.org/ew/cases/EWCA/Civ/2010/819.html){rel="external"},
the Court of Appeal considered the evidence from published material at
the time of the invention and held that:

> The plain fact is that there was no real explanation of why the idea
> was not taken up well before the date of the Patent. The simplest
> explanation -- indeed the only one that fits the known facts -- is
> that the inventors hit upon something which others had missed.

### 3.81.1 {#ref3-81-1}

In [Tetra Molectric Ltd v Japan Imports Ltd, \[1976\] RPC
547](https://doi.org/10.1093/rpc/93.21.547){rel="external"} on the other
hand, it was held that the commercial success of a cigarette lighter was
due in large part to hammer mechanisms developed since the date of the
invention; although claim 1 covered lighters which had enjoyed
commercial success, it also covered lighters which could never do so,
and no features which might ensure success were recited.

### 3.81.2 {#ref3-81-2}

In [Dr Reddy's Laboratories (UK) Ltd v Eli Lilly and Co Ltd \[2008\]
EWHC 2345 (Pat), \[2009\] FSR
5](http://www.bailii.org/ew/cases/EWHC/Patents/2008/2345.html){rel="external"},
the Patents Court pointed out at paragraph 187 that arguments based on
commercial success of products the subject of anterior patent protection
are more or less doomed to failure because the existence of that patent
(application) provides a clear explanation of why no third party ever
launched the product. The earlier cases of [Cipla Ltd v Glaxo Group Ltd
\[2004\] EWHC
477](http://www.bailii.org/ew/cases/EWHC/Patents/2004/477.html){rel="external"},
RPC 43 (at \[115\]) and [Generics (UK) Ltd v H Lundbeck A/S \[2007\]
EWHC
1040](http://www.bailii.org/ew/cases/EWHC/Patents/2007/1040.html){rel="external"},
\[2007\] RPC 32 (at \[251\]) were referred to.

### 3.81.3 {#ref3-81-3}

The issue of commercial success was also considered in Emson v Hozelock
\[2020\] EWCA Civ 871. The court found that the commercial success of
Emsons's garden hose and the absence of a comparable device on the
market, could be explained by the fact that the prior art being used to
attack their patent ''would not in the normal course come to the
attention of the skilled but unimaginative person skilled in the art''.
As a result, commercial success in this instance did not point towards
inventiveness.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Advantages of the invention]{#default-id-0a305b24-heading-40 .govuk-accordion__section-button} {#advantages-of-the-invention .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Advantages of the invention\",\"index_section\":40,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-40 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-40" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Advantages of the invention\",\"index_section\":40,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.82 {#ref3-82}

Where a variation from published matter proposed by the applicant has no
advantages, or is even disadvantageous, although it can be argued that
the resulting inferior procedure is not obvious in the sense that no
skilled person would regard it as obvious to do something inferior, the
application should nevertheless, if the variation is one whose
possibility a skilled person would appreciate, be refused on the ground
that there is no inventive step. Such a view was taken by the Technical
Board of Appeal of the EPO in Decision T119/82, OJEPO 5/84, see
particularly paragraph 16. The position is of course different if the
applicant has discovered that a variation thought to be disadvantageous
is in fact not so, or if from a large number of variants which would
have been regarded as no more than feasible alternatives with no
advantages, the applicant has selected a variant with an unexpected
advantage ([see also 3.88-3.93](#ref3-88))
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Obvious to try]{#default-id-0a305b24-heading-41 .govuk-accordion__section-button} {#obvious-to-try .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Obvious to try\",\"index_section\":41,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-41 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-41" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Obvious to try\",\"index_section\":41,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.83 {#ref3-83}

In [Johns-Manville Corporation's Patent, \[1967\] RPC
479](https://doi.org/10.1093/rpc/84.18.479){rel="external"} it was held
that where a skilled worker in a particular field could be expected to
know of a use of material to achieve a certain result in that field, an
invention which is concerned with the use of that material to achieve
the same result in a part of that field which had not been previously
disclosed is obvious if a person versed in the art would assess the
likelihood of success sufficient to warrant a trial. In more recent
cases the courts have however departed somewhat from this view ([see
3.87.2](#ref3-87-2)). The invention was concerned with the use of
particular flocculating agents in asbestos cement manufacturing. It was
held that, filtration processes being common to many industries, two
cited documents, although addressed primarily to the mining and paper
industries respectively, were likely to be read by those concerned with
the asbestos cement industry, and that such readers would have realised
that here was a newly- introduced flocculating agent which it was well
worth trying out in their filtration process.

### 3.84 {#ref3-84}

In [Brugger and others v Medic-Aid Ltd \[1996\] RPC
635](https://doi.org/10.1093/rpc/1996rpc635){rel="external"} Laddie J
held that if a particular route is an obvious one to try, it is not
rendered any less obvious from a technical point of view merely because
there are a number, and perhaps a large number, of other obvious routes
as well. Similarly in [Bristol-Myers Squibb Co v Baker Norton
Pharmaceuticals Inc \[1999\] RPC
253](https://doi.org/10.1093/rpc/1999rpc253){rel="external"}, Jacob J
held that an effect which was revealed by following the obvious course
of action did not make the action non-obvious.

### 3.85 {#section-22}

\[Deleted\]

### 3.86 {#ref3-86}

The Court of Appeal in [Saint-Gobain PAM SA v Fusion Provida Ltd and
Electrosteel Castings Ltd \[2005\] EWCA Civ
177](https://www.bailii.org/ew/cases/EWCA/Civ/2005/177.html){rel="external"},
\[2005\] IP & T 880 held that the mere possible inclusion of something
within a research programme on the basis you will find out more and
something might turn up is not enough to show obviousness. If it were
otherwise there would be few inventions that were patentable. The cited
prior art pointed to the possibility that using a Zn/Al alloy as a
coating for a cast iron pipe to be buried in soil might be beneficial by
showing results for this alloy coating for buried steel plates. It was
not however possible for the skilled person to predict success, so the
invention was not obvious. Jacob LJ held that

> the 'obvious to try' test really only works where it is more-or-less
> self- evident that what is being tested ought to work.

### 3.87 {#ref3-87}

An invention can therefore only be said to be "obvious to try" if there
is a reasonable expectation of success (see also 3.99). In [Angiotech
Pharmaceuticals Inc's Patent (Application for Revocation by Conor
Medsystems Inc) \[2006\] RPC
28](https://doi.org/10.1093/rpc/2006rpc28){rel="external"}, the Patents
Court held that the contribution to the art made by the specification
had to be assessed in order to decide whether it was sufficient to show
that something was an obvious candidate for testing without any
expectation of success, or whether it was necessary to show that the
skilled person must have had an expectation of success sufficient to
induce them to use it in practice. This decision was upheld by the Court
of Appeal (\[2007\] RPC 20) but was later overturned in the House of
Lords ([Conor Medsystems Inc v Angiotech Pharmaceuticals Inc \[2008\]
RPC 716](https://doi.org/10.1093/rpc/rcn025){rel="external"}) where this
distinction was rejected. Lord Hoffmann stated

> ...there is in my opinion no reason as a matter of principle why, if a
> specification passes the threshold test of disclosing enough to make
> the invention plausible, the question of obviousness should be subject
> to a different test according to the amount of evidence which the
> patentee presents to justify a conclusion that his patent will work.

In this case, the patent specification disclosed that taxol could be
incorporated on a stent (a tubular device which acts as scaffolding to
hold a diseased artery open). The prior art disclosed a very similar
stent with a large range of drugs which could be incorporated on the
stent to prevent restenosis. While taxol was not specifically mentioned,
a range of classes, which could include taxol, were listed as possible
candidates. The lower courts considered that the specification gave no
suggestion that taxol would be safe or prevent restenosis (closure of
the lumen of the artery caused by proliferation of smooth muscle cells)
i.e. there was no disclosure that taxol would be a better candidate than
any other possible candidate. Consequently a claim to a taxol-coated
stent was held by the lower courts to be invalid as it was concluded to
be obvious to a skilled person that taxol should be incorporated onto a
stent with a view to seeing if it prevents restenosis and is safe. The
House of Lords, however, held that there was evidence provided in the
specification as filed that taxol was a particularly effective
anti-angiogenic agent, and the invention was based on the principle that
inhibition of angiogenesis could be used to prevent restenosis. The
issue to be decided was therefore whether it was obvious to use a
taxol-coated stent to prevent restenosis, not whether taxol was an
obvious candidate for further investigation. Lord Hoffman held that the
claim was not obvious and confirmed that the notion of something being
obvious to try was useful only in a case where there was a fair
expectation of success. How much of an expectation would be needed would
depend upon the particular facts of the case. While the House of Lords
accepted that the absence of any evidence to support a speculative claim
could lead to an objection of lack of support or insufficiency (quoting
the decision in [Prendergast's Applications \[2000\] RPC
446](https://doi.org/10.1093/rpc/2000rpc446){rel="external"}), they held
that this requirement should not be confused with the requirement for
inventiveness.

### 3.87.1 {#ref3-87-1}

The main issue to be decided in [Omnipharm Limited v Merial \[2011\]
EWHC 3393
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2011/3393.html){rel="external"}
related to a claim to a "spot on" formulation for the treatment of fleas
in pets, the closest prior art being a "spray on" formulation of the
same active ingredient. Floyd J held that since "spot on" formulations
have advantages in terms of ease of application it would be obvious to
try to develop a spot on formulation; however in this particular
situation the skilled team was considered to have no common general
knowledge basis on which to make a prediction as to whether a "spot on"
formulation would work. Therefore the skilled person would not have had
sufficient expectation of success to render the invention obvious.

### 3.87.2 {#ref3-87-2}

In [Novartis AG v Generics (UK) Ltd (trading as Mylan) \[2012\] EWCA Civ
1623](http://www.bailii.org/ew/cases/EWCA/Civ/2012/1623.html){rel="external"},
it was held that it is important to have regard to "all the
circumstances of the case including, where appropriate, whether it was
obvious to try a particular route with a reasonable or fair expectation
of success. What is a reasonable or fair expectation of success will
again depend upon all the circumstances and will vary from case to
case". Kitchin LJ went on to hold that "it may be appropriate to
consider whether it is more or less self-evident that what is being
tested ought to work" and "simply including something in a research
project in the hope that something might turn up is unlikely to be
enough. But I reject the submission that the court can only make a
finding of obviousness where it is manifest that a test ought to work.
That would be to impose a straightjacket upon the assessment of
obviousness which is not warranted by the statutory test and would, for
example, preclude a finding of obviousness in a case where the results
of an entirely routine test are unpredictable". In [Actavis Group PTC
EHF v ICOS Corporation & Ors \[2017\] EWCA Civ
1671](http://www.bailii.org/ew/cases/EWCA/Civ/2017/1671.html){rel="external"},
it was held that a dosage of 5mg of tadalafil was obvious over prior art
which stated 50mg of tadalafil was possible. The court held that a
skilled team would try different doses of the drug, in a routine
clinical trial, and would reach a dosage of 5mg. The judge noted that
"there are some steps which can be characterised as so routine that the
skilled person would carry them out simply because they are routine, and
irrespective of any prospect of success. An example is routine dose
ranging studies in the clinical testing of a known drug". The court went
on to say that they have "been at pains to warn against the
over-elaboration of the "obvious to try" line of cases. While there are
a number of factors which, depending on the circumstances, may bear on
the question it is not always necessary for all of them to be ticked off
as if on a checklist". This decision was upheld on appeal in [Actavis
Group PTC EHF v ICOS Corporation & Ors \[2019\] UKSC
15](https://www.bailii.org/uk/cases/UKSC/2019/15.html){rel="external"}
which elaborated further on the role of obviousness when patenting
dosage regimes. Similarly, in [Teva Pharmaceutical Industries Ltd & Ors
v Bayer Healthcare LLC \[2021\] EWHC 2690 (Pat) (PDF,
476KB)](https://www.bailii.org/ew/cases/EWHC/Patents/2021/2690.pdf){rel="external"}
Bayer's patent covering the compound sorafenib tosylate was found to be
obvious over prior art that disclosed preliminary clinical trial data in
humans but did not detail the exact salt form of the drug used. The
court held that the skilled team would have derived reassurance from the
prior art that a suitably soluble formulation of sorfenib must exist as
one had been used in the clinical trial. Given this the skilled team
would have comprised a formulation chemist (the so-called ''skilled
formulator''), and therefore the tosylate salt would have been
identified in a routine salt screen as a candidate for formulation. Also
see [Gedeon Richter v Bayer Schering \[2011\] EWHC 583
(Pat).](http://www.bailii.org/ew/cases/EWHC/Patents/2011/583.html){rel="external"}

::: call-to-action
### 3.87.3 {#ref3-87-3}

\[Deleted\]
:::
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Selection]{#default-id-0a305b24-heading-42 .govuk-accordion__section-button} {#selection .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Selection\",\"index_section\":42,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-42 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-42" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Selection\",\"index_section\":42,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
[See also
2.18-2.20](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-18)

### 3.88 {#ref3-88}

Although there is no inventive step if it is clear from the prior art
that taking that step is likely to lead to success, there may be
invention if that is only one of many courses possible, and there is no
reason to infer from the prior art that this one is more likely than the
others to be profitable. In [Bayer AG (Baatz's) European Application
\[1982\] RPC
321](https://doi.org/10.1093/rpc/1982rpc321){rel="external"}, carbonless
copying paper was characterised by microcapsules made of a particular
polymer, which was already known for forming coatings on textiles,
leather, wool and metal. Even if these were thought to be neighbouring
fields, there was no reason to expect that improved results would be
obtained by the use of this material (as the results of comparative
experiments showed they were), and thus it was not obvious to select it
from the enormous number possible. And in [Olin Mathieson Chemical
Corporation v Biorex Laboratories Ltd, \[1970\] RPC
157](https://doi.org/10.1093/rpc/87.7.157){rel="external"} at page 192,
it was held not to be obvious that a useful drug would be obtained by
substituting -CF3 for -Cl in a known drug, given the large amount of
prior material, leading in a number of different directions, which was
before the skilled person at the date of the invention.

### 3.89 {#ref3-89}

When faced with claims that may relate to a selection invention the
prima facie inventive step objection should be raised using the
Windsurfing/Pozzoli approach, unless the selection is so clear-cut as to
make this unnecessary. If the applicant/agent maintains (or it is clear
from the specification) that the inventiveness may lie in a selection
invention, then the approach used by the Court of Appeal in [Dr Reddy's
Laboratories (UK) Ltd v Eli Lilly & Co Ltd \[2010\]
RPC](https://doi.org/10.1093/rpc/rcq020){rel="external"} should be
followed. In such cases, the question to be asked is whether the
invention makes a hitherto unknown technical contribution or is merely
an arbitrary selection. If it is merely an arbitrary selection then the
invention is obvious. In [Generics \[UK\] LTD (t/a Mylan) v Yeda
Research and Development co. LTD & Anor \[2013\] EWCA Civ
925](http://www.bailii.org/ew/cases/EWCA/Civ/2013/925.html){rel="external"}
the Court of Appeal considered the law regarding selection inventions,
with reference to Dr Reddy's and the EPO Board of Appeal decision in T
939/92 AGREVO/Triazoles 6 OJEPO 309. The position following the judgment
in Generics is as follows:

i\) Article 56 of the EPC is in part based on the underlying principle
that the scope of the patent monopoly must be justified by the
patentee's contribution to the art;

ii\) If the alleged contribution is a technical effect which is not
common to substantially everything covered by a claim, it cannot be used
for the purposes of judging obviousness;

iii\) In such circumstances the claim must either be restricted to the
subject matter which makes the technical contribution, or a different
contribution common to the whole claim must be found;

iv\) A selection from the prior art which is purely arbitrary and cannot
be justified by some useful technical property is likely to be held to
be obvious because it does not make a real technical advance;

v\) A technical effect which is not rendered plausible by the patent
specification may not be taken into account in assessing inventive step;

vi\) Later evidence may be cited to support a technical effect made
plausible by the specification;

vii\) Provided the technical effect is made plausible, no further proof
of the existence of the effect is to be demanded of the specification
before judging obviousness by reference to the technical effect put
forward.

### 3.89.1 {#section-23}

\[Deleted\]

### 3.90 {#ref3-90}

The Court of Appeal in [Dr Reddy's Laboratories (UK) Ltd v Eli Lilly &
Co Ltd \[2010\] RPC
9](https://doi.org/10.1093/rpc/rcq020){rel="external"} disregarded the
criteria set out in [IG Farbenindustrie AG's Patents 47 RPC
289](https://doi.org/10.1093/rpc/47.9.289){rel="external"}. In Dr
Reddy's Jacob LJ stated that as these rules related to pre-1977 law they
could be regarded:

> as part of legal history, not as part of the living law.

### 3.91 {#ref3-91}

Patent Act s.118 refers

The hitherto unknown technical effect (i.e. advantage gained or
disadvantage avoided) relied upon to justify a selection invention
should be clearly identified or otherwise made plausible (e.g.
discernible from tests provided in the application), in the
specification at the time of filing (see also T 1329/04 Johns Hopkins
University School of Medicine/Growth Differentiation Factor \[2006\]
EPOR 8). Later-filed evidence may be used to provide support for the
presence of such an effect or the fact that it is common to everything
claimed, but unexpected bonus effects not described in the specification
cannot form the basis of a valid claim to a selection invention (see
[Glaxo Group Ltd's Patent \[2004\] RPC
43](https://doi.org/10.1093/rpc/2004rpc43){rel="external"}). If there is
no statement of advantage in the specification at the time of filing it
may not be added later, since, as stated by Jacob J. in
[Richardson-Vicks Inc.'s Patent \[1995\] RPC
568](https://doi.org/10.1093/rpc/1995rpc568){rel="external"} at 581, in
the context of synergy, whether or not the advantage was demonstrated

> by experiments conducted after the date of the patent cannot help show
> obviousness or non-obviousness ... and it would be quite wrong for
> later-acquired knowledge to be used to justify the amended claim.

### 3.91.1 {#ref3-91-1}

The judgment in [Generics \[UK\] LTD (t/a Mylan) v Yeda Research and
Development co. LTD & Anor \[2013\] EWCA Civ
925](http://www.bailii.org/ew/cases/EWCA/Civ/2013/925.html){rel="external"}
also addressed the question of what happens if the technical property or
effect made plausible by the specification does not exist in fact. The
lower court had held that since later evidence cannot be used to support
a technical effect not indicated in the specification, neither can it be
used to refute such an effect. The Court of Appeal held however that in
considering later evidence on this issue one is not judging the
obviousness of the invention by reference to later evidence; one is
simply defining by evidence what the invention is. The Judge allowed the
admission of later evidence which, according to Mylan, showed that the
composition as claimed did not demonstrate the relied upon technical
effect. The Judge however found that the evidence provided did not prove
the absence of the technical effect and rejected the appeal.

### 3.92 {#ref3-92}

Although the size of the class from which a member or members have been
chosen is not relevant to the question of novelty of a selection
invention, it may be relevant to the question of obviousness (Du Pont de
Nemours &c (Witsiepe's) Application, \[1982\] FSR 303 at page 310), [see
2.19 --
2.20](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-19).

### 3.93 {#ref3-93}

The technical significance of the parameters by which the product or
process is selected should be considered. Where unusual parameters are
used in a claim it may be difficult to prove whether or not the prior
art would have inevitably exhibited those parameters, but in [Raychem
Corp.'s Patents \[1998\] RPC
31](https://doi.org/10.1093/rpc/1998rpc31){rel="external"} it was held
(at pp.46-47) that:

> although it may not be obvious, in the common use of that word, to
> limit a claim by reference to some particular meaningless and
> arbitrary parameter, that had nothing to do with patentability.
> Patents are not given for skill in inventing technically meaningless
> parameters.

If a product or process with obviously desirable characteristics happens
to fall within the limits of such claims then they cover what is obvious
and will thus be invalid.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Additional advantage not inventive]{#default-id-0a305b24-heading-43 .govuk-accordion__section-button} {#additional-advantage-not-inventive .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Additional advantage not inventive\",\"index_section\":43,\"index_section_count\":43}"}
:::

::: {#default-id-0a305b24-content-43 .govuk-accordion__section-content aria-labelledby="default-id-0a305b24-heading-43" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Additional advantage not inventive\",\"index_section\":43,\"index_section_count\":43}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 3.94 {#ref3-94}

Although the discovery of an unexpected advantage may point to a step
not being obvious if it was only one of many steps which could have been
tried (see 3.88) or if it was one taken counter to accepted views (see
3.97), if the prior art leads directly to the step then it is not made
inventive by any additional advantage obtained. In [Inventa AG's
Application, \[1956\] RPC
45](https://doi.org/10.1093/rpc/73.3.45){rel="external"}, the use for
spinning nylon of a process which had been disclosed (before the
introduction of nylon) for spinning artificial filaments in general was
held to be obvious, and not to be saved by an additional advantage,
since no further modification of the process was required to secure this
advantage. And in [Union Carbide Corporation (Hostettler's) Application,
\[1972\] RPC 601](https://doi.org/10.1093/rpc/89.20.601){rel="external"}
at page 609, Whitford J observed (obiter) that "if in fact the step
taken was an obvious step, it remains an obvious step however
astonishing the result of taking it may be

> if in fact the step taken was an obvious step, it remains an obvious
> step however astonishing the result of taking it may be.

### 3.95 {#ref3-95}

The Court of Appeal in [Hallen Co v Brabantia (UK) Ltd \[1991\] RPC
195](https://doi.org/10.1093/rpc/1991rpc195){rel="external"} held that
if a claimed invention is obvious for one reason, the existence of an
additional benefit does not render the claim inventive. In Hallen, it
was held to be obvious to coat a corkscrew of self-pulling type with
PTFE to facilitate its penetration into a cork; the claimed invention
was not saved by the non-obvious additional advantage of facilitating
extraction of the cork from the bottle (although it might have been
saved as a selection patent if the specification had contained clear
assertions that the corkscrew in question turned the use of PTFE to
special advantage over other corkscrews in the extraction stage, thus
overcoming a problem of all previous self-pullers). This position was
emphasised in [Vernacare Limited v Moulded Fibre Products Limited
\[2023\] EWCA Civ
831](https://www.bailii.org/ew/cases/EWCA/Civ/2023/841.html){rel="external"}.
In Vernacare, it was held to be obvious to use a fluorocarbon to provide
detergent resistance to a moulded paper washbowl, in light of a
disclosure of a similar fluorocarbon being used to provide water, oil,
and grease resistance to such a bowl.

### 3.96 {#ref3-96}

In general, an otherwise obvious combination is not saved from a finding
of obviousness by some unexpected advantage caused by an unpredictable
co-operation between the elements of the combination (see [Glaxo Group
Ltd's Patent \[2004\] RPC
43](https://doi.org/10.1093/rpc/2004rpc43){rel="external"}).

### Overcoming a technical prejudice

### 3.97 {#ref3-97}

An invention may be regarded as non-obvious if it goes against the
generally accepted views and practices in the art. As the Patents Court
in [Dyson Appliances Ltd v Hoover Ltd \[2001\] RPC
26](https://doi.org/10.1093/rpc/2001rpc26){rel="external"} made clear
(upheld by the Court of Appeal \[2002\] RPC 22), the common general
knowledge held by the skilled person may have both positive and negative
aspects, and it is necessary to take account of both; in other words to
take account of what the skilled person would consider doing and what
the skilled person would be prejudiced against doing, as a result of
that knowledge. If the common general knowledge was such that the
skilled person did not perceive a problem with the prior art, it becomes
"considerably more difficult" to establish the obviousness of taking a
particular step which would bring that prior art within the scope of the
claims in question. In the case in question it was held that the common
general knowledge of the skilled person at the relevant time, along with
a lack of a perceived problem, would mean that the skilled person would
never have considered using anything other than bag technology in a
vacuum cleaner. Further examples are if persons skilled in the art would
regard certain materials or techniques as unsuitable for a particular
purpose, then if the inventor has found that this prejudice is not
well-founded, then they have made an inventive contribution to the art.
A patentee who advances the art by showing that, contrary to the
prevailing view, the idea will work or is practical has shown something
new and inventive (see [Pozzoli SPA v BDMO SA \[2007\] EWCA Civ
588](http://www.bailii.org/ew/cases/EWCA/Civ/2007/588.html){rel="external"}.
Likewise the omission of a step hitherto thought to be necessary may
constitute an inventive step.

### 3.98 {#ref3-98}

It must however be clear that the technical prejudice which the
applicant claims to have overcome did in fact exist, and that it was not
justified. A prejudice which is insufficiently widespread for it
properly to be regarded as commonly shared should not be attributed to
the notionally skilled person. In [Glaxo Group's Patent \[2004\] RPC
43](https://doi.org/10.1093/rpc/2004rpc43){rel="external"}, it was held
that a prejudice does not exist if some people engaged in the art
laboured under a particular prejudice if a substantial number of others
did not. Thus a rooted objection to the regular use of b2-antagonists in
the treatment of asthma, which was the subject of an ongoing dispute
amongst specialist physicians, was not ascribed to the skilled person.
Another situation is where scientific opinion is out of accord with what
is done in the market, as occurred in [Ancare New Zealand Ltd's Patent
\[2003\] RPC 8](https://doi.org/10.1093/rpc/2006rpc28){rel="external"}
for a sheep drench comprising two known agents, one active against
roundworms and one active against tapeworms. Here, the patentee argued
that an inventive step lay in including the tapeworm agent because there
was scientific hostility against treating tapeworms in sheep. However,
it was common practice for New Zealand farmers to treat their lambs for
tapeworm at the priority date. The Privy Council, upholding judgments of
the New Zealand High Court and Court of Appeal to revoke the patent for
obviousness and not involving any inventive step over what was known or
used before the priority date of the claim in New Zealand, held that:

> the fact that scientific opinion might have thought that something was
> perfectly useless did not mean that practising it, or having the idea
> of making a preparation to do it, was an inventive step. Otherwise,
> anyone who adopted an obvious method for doing something which was
> widely practised but which the best scientific opinion thought was
> pointless could obtain a patent.

### 3.99 {#ref3-99}

For an invention to overcome a technical prejudice, and therefore be
non-obvious, a genuine prejudice must exist which would have prevented
the skilled person from taking the step that would lead to the claimed
invention. In [Teva UK Ltd and others v AstraZeneca AB \[2012\] EWHC
655](http://www.bailii.org/ew/cases/EWHC/Patents/2012/655.html){rel="external"},
the patent claimed a sustained release formulation of a drug,
quetiapine, for the treatment of schizophrenia. AstraZeneca, the patent
holder, argued that there were technical considerations preventing the
skilled person from considering developing a sustained release
formulation of quetiapine, so called "lions in the path", and therefore
the patent was inventive. They argued that the skilled team in light of
the prior art would be deterred from considering a sustained release
formulation because quetiapine is rapidly metabolized by the liver ([see
also 3.83-3.87.1](#ref3-83)). The claimants however, argued that it
would be obvious to formulate quetiapine for sustained release and that
the alleged prejudices against doing so were illusory, mere "paper
tigers". The judge held that an immediate release once-daily formulation
would not be efficacious without increasing the dose and therefore there
was rationale for the skilled person to seek a sustained release
formulation. The judge also held that on the evidence quetiapine is not
metabolised in a way which would deter the skilled person from
attempting a sustained release formulation. Accordingly AstraZeneca's
patent was held to be obvious and was revoked. In \[Actavis UK Ltd v
Novartis AG \[2010\] EWCA Civ 82\]( a claim to a sustained release
formulation for the drug fluvastatin was also held to lack an inventive
step since the obstacle put forward in the patent against being able to
make such a formulation was illusory.

### 3.100 {#section-24}

There is also no invention in merely tolerating the disadvantages which
have deterred others. For example if an inexpensive plastics material is
thought unsuitable for making tools because it is not durable, there is
no invention in using it to make a cheap screwdriver intended only for
light work and accepting that it will have only a short life.

### 3.101 {#section-25}

Some of these points may be illustrated by a hypothetical example.
Suppose that it has been stated for years in textbooks that a particular
class of chemical reaction carried out under elevated pressure, gives
poor yields, and an inventor now claims the synthesis of a particular
compound by such a process. If all they have done is take advantage of
the high price commanded by the product, or the cheapness of the
starting materials, and has decided to accept the disadvantage of low
yield, then that is not inventive; it is an obvious response to
prevailing economic circumstances. On the other hand, if the inventor
has discovered that good yields can be obtained by the use of still
higher pressures, a fact not suggested in the prior art, then that would
be inventive. But if higher yields would be expected at
difficult-to-obtain pressures, and the inventor has merely taken
advantage of new techniques making such pressures more available, then
that is not inventive. Finally, if the inventor has discovered that the
standard accepted views on the low yields, while being normally true for
this reaction, are not in fact true for this particular compound, then
there is inventive step in the choice of this process.
:::
:::
:::
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
