::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 122: Crown\'s right to sell forfeited articles {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated: February 2008
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 122**
  Nothing in this Act affects the right of the Crown or any person deriving title directly or indirectly from the Crown to dispose of or use articles forfeited under the laws relating to customs or excise.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 122.01

This section provides that nothing in the 1977 Act affects the Crown's
right to dispose of or use articles forfeited under the customs and
excise laws.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
