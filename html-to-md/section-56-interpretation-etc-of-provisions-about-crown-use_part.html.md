::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 56: Interpretation, etc., of provisions about Crown use {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (56.01 - 56.04) last updated: April 2020.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 56.01

This is the second of the group of sections relating to Crown use of
patented inventions. It concerns the interpretation of certain terms
used in the provisions of the Act relevant to Crown use, especially
s.55. With regard to ss.55 to 59 in general, [see
55.01-03](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-01).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 56(1)**
  Any reference in section 55 above to a patented invention, in relation to any time, is a reference to an invention for which a patent has before that time been, or is subsequently, granted.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 56.02

In including an invention for which a patent is yet to be granted, the
meaning of "patented invention" in s.55 is an exception to the
definition in s.130(1). [See also
55.03](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-03).

  -----------------------------------------------------------------------
   

  **Section 56(2)**

  In this Act, except so far as the context otherwise requires, "the
  services of the Crown" includes\
  (a) the supply of anything for foreign defence purposes;\
  (b) the production or supply of specified drugs and medicines; and\
  (c) such purposes relating to the production or use of atomic energy or
  research into matters connected therewith as the Secretary of State
  thinks necessary or expedient;\
  \
  and "use for the services of the Crown" shall be construed accordingly.
  -----------------------------------------------------------------------

### 56.03 {#ref56-03}

s.130(1) is also relevant

Except so far as the context otherwise requires, "services of the Crown"
and "use for the services of the Crown" in the Act have the meanings
assigned to them by s.56(2) including, as respects any period of
emergency within the meaning of s.59, the meanings assigned to them by
s.59. The services of the Crown thus includes inter alia those things
listed in s.56(2)(a) to (c), subsections (2)(a) and (b) being
interpreted in accordance with subsections (3) and (4) respectively.

In [IPCom GmbH v Vodafone \[2020\] EWHC 132
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2020/132.html){rel="external"},
the court held that the list of activities in s.56(2) is not exhaustive.

[See also
55.04](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-04).

  -----------------------------------------------------------------------
   

  **Section 56(3)**

  In section 55(1)(a) above and subsection (2)(a) above, references to a
  sale or supply of anything for foreign defence purposes are references
  to a sale or supply of the thing\
  (a) to the government of any country outside the United Kingdom, in
  pursuance of an agreement or arrangement between His Majesty's
  Government in the United Kingdom and the government of that country,
  where the thing is required for the defence of that country or of any
  other country whose government is party to any agreement or arrangement
  with His Majesty's Government in respect of defence matters; or\
  (b) to the United Nations, or to the government of any country
  belonging to that organisation, in pursuance of an agreement or
  arrangement between His Majesty's Government and that organisation or
  government, where the thing is required for any armed forces operating
  in pursuance of a resolution of that organisation or any organ of that
  organisation.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 56(4)**

  For the purposes of section 55(1)(a) and (c) above and subsection
  (2)(b) above, specified drugs and medicines are drugs and medicines
  which are both\
  (a) required for the provision of\
  (ai) primary medical services under part 1 of the National Health
  Service Act 1977, part I of the National Health Service (Scotland) Act
  1978 or any corresponding provisions of the law in force in Northern
  Ireland or the Isle of Man or primary dental services under part 1 of
  the National Health Service Act 1977, or any corresponding provisions
  of the law in force in Northern Ireland or the Isle of Man, or\
  (i) pharmaceutical services, general medical services or general dental
  services under Part II of the National Health Service Act 1977 (in the
  case of pharmaceutical services), Part II of the National Health
  Service (Scotland) Act 1978 (in the case of pharmaceutical services or
  general dental services), or the corresponding provisions of the law in
  force in Northern Ireland or the Isle of Man, or\
  (ii) personal medical services or personal dental services provided in
  accordance with arrangements made under section 17C of the 1978 Act (in
  the case of personal dental services), or the corresponding provisions
  of the law in force in Northern Ireland or the Isle of Man, or\
  (iii) local pharmaceutical services provided under a pilot scheme
  established under section 28 of the Health and Social Care Act 2001 or
  an LPS scheme established under Schedule 8A to the National Health
  Service Act 1977, or under any corresponding provision of the law in
  force in the Isle of Man, and\
  (b) specified for the purposes of this subsection in regulations made
  by the Secretary of State.
  -----------------------------------------------------------------------

### 56.04 {#section-2}

Paragraph (4)(a) was last amended by the Health and Social Care
(Community Health and Standards) Act 2003, which inserted subparagraph
(4)(a)(ai) and made consequential amendments to subparagraphs (4)(a)(i)
and (ii). This came into force in relation to primary medical service in
England and Wales on 1 April 2004 (The Health and Social Care (Community
Health and Standards) Act 2003 Commencement (No. 2) Order 2004 SI 2004
No. 288 and The Health and Social Care (Community Health and Standards )
Act 2003 Commencement (No.1) (Wales) Order 2004 SI 2004 No. 480). The
reference in subparagraph 4(a)(ai) to the National Health Service
(Scotland) Act 1978, and the consequential amendments to subparagraphs
4(a)(i) and (ii) were made by the Primary Medical Services (Scotland)
Act 2004 (Consequential Modifications) Order (SI 2004 No. 957), which
also came into force on 1 April 2004. Subparagraph (4)(a)(ai) came into
force for primary dental services in England on 1 January 2006 (The
Health and Social Care (Community Health and Standards) Act 2003
Commencement (No. 8) Order 2005 SI 2005 No. 2925) and in Wales on 15
February 2006 (The Health and Social Care (Community Health and
Standards) Act 2003 Commencement (No. 8) Order 2005 SI 2006 No. 345).
Subparagraph (4)(a)(iii) was inserted by the Health and Social Care Act
2001, and came into force in Wales on 1 July 2002 (The Health and Social
Care Act 2001 (Commencement No. 2) (Wales) Order 2002 SI 2002 No. 1475),
and in England on 1 January 2003 (The Health and Social Care Act 2001
(Commencement No. 11) (England) Order 2002 SI 2003 No. 53).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
