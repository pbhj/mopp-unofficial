::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 103: Extension of privilege for communications with solicitors relating to patent proceedings {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (103.1 - 103.3) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 103.1

This section declares that the privilege from disclosure enjoyed by the
clients of solicitors extends to communications with solicitors for the
purpose of pending or contemplated proceedings under the 1977 Act, EPC
or PCT. This applies to proceedings before the comptroller or, for the
EPC and PCT only, before the relevant convention court (as defined in
s.130(1)).

### 103.2

Section 280 of the CDP Act makes similar provision in respect of
communications with patent agents. Section 103 does not extend to
Scotland where section 105 (and section 280(4) of the CDP Act) apply
instead.

  -----------------------------------------------------------------------
   

  Section 103(1)

  It is hereby declared that the rule of law which confers privilege from
  disclosure in legal proceedings in respect of communications made with
  a solicitor or a person acting on his behalf, or in relation to
  information obtained or supplied for submission to a solicitor or a
  person acting on his behalf, for the purpose of any pending or
  contemplated proceedings before a court in the United Kingdom extends
  to such communications so made for the purpose of any pending or
  contemplated -\
  (a) proceedings before the comptroller under this Act or any of the
  relevant conventions, or\
  (b) proceedings before the relevant convention court under any of those
  conventions.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  Section 103(2)

  In this section -\
  "legal proceedings" includes proceedings before the comptroller;\
  the references to legal proceedings and pending or contemplated
  proceedings include references to applications for a patent or a
  European patent and to international applications for a patent; and\
  "the relevant conventions" means the European Patent Convention and the
  Patent Co-operation Treaty.
  -----------------------------------------------------------------------

  --------------------------------------------
   
  Section 103(3)
  This section shall not extend to Scotland.
  --------------------------------------------

### 103.3

The reference to a solicitor etc includes references to bodies
recognised under the Administration of Justice Act 1985, s.9, by the
Solicitors' Incorporated Practices Order 1991, SI 1991 No 2684.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
