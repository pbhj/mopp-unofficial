::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 277: Persons entitled to describe themselves as European patent attorneys, &c {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (277.01 - 277.04) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 277.01

Article 134 of the European Patent Convention (EPC) provides that
professional representation in proceedings under the Convention may only
be undertaken by persons whose names appear on the list maintained for
the purpose by the European Patent Office (the "European list"). This
section provides that an individual who is on the European list is
allowed to use the title "European patent attorney" or "European patent
agent" (EPA). It also relates to the composition of partnerships and
bodies corporate practising as EPAs in a similar manner to section 276
in respect of UK patent attorneys. The section replaced section 84 of
the Patents Act 1977 but with changes paralleling the relaxation of
rules governing practice as a UK patent attorney. In particular, it is
no longer an offence under UK law for a person not on the European list
to carry out the functions of an EPA although such people will be
prevented by the operation of the EPC from being recognised as agents.

  ---------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 277(1)**
  The term "European patent attorney" or "European patent agent" may be used in the following cases without any contravention of section 276.
  ---------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 277(2)**

  An individual who is on the European list may ­\
  (a) carry on business under a name or other description which contains
  the words "European patent attorney" or "European patent agent", or\
  (b) otherwise describe himself, or permit himself to be described, as
  a"European patent attorney" or "European patent agent".
  -----------------------------------------------------------------------

### 277.02

Subsection (1) provides that section 276 is not contravened by the use
of the titles "European patent attorney" and "European patent agent" in
the cases set out in the following subsections. Paragraph (a) of
subsection (2) provides that a person may carry on a business using
these titles if they are entered in the European list. Paragraph (b)
relates to the act of otherwise describing oneself or permitting oneself
to be described as an EPA.

  -----------------------------------------------------------------------
   

  **Section 277(3)**

  A partnership of which not less than the prescribed number or
  proportion of partners is on the European list may ­\
  (a) carry on a business under a name or other description which
  contains the words "European patent attorneys" or "European patent
  agents", or\
  (b) otherwise describe itself, or permit itself to be described, as a
  firm which carries on the business of a "European patent attorney" or
  "European patent agent".
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 277(4)**

  A body corporate of which not less than the prescribed number or
  proportion of directors is on the European list may ­\
  (a) carry on a business under a name or other description which
  contains the words "European patent attorney" or "European patent
  agent", or\
  (b) otherwise describe itself, or permit itself to be described as, a
  company which carries on the business of a "European patent attorney"
  or "European patent agent".
  -----------------------------------------------------------------------

### 277.03

Subsections (3) and (4) relate to the activities of partnerships and
bodies corporate. They may use the titles "European patent attorney" or
"European patent agent" if a prescribed number or proportion of partners
or directors are on the European list. 'Directors' may include 'members'
in accordance with section 286. Paragraphs (a) and (b) of each
subsection mirror paragraphs (a) and (b) of subsection (2).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 277(5)**
  Where the term "European patent attorney" or "European patent agent" may, in accordance with this section, be used in reference to an individual, partnership or body corporate, it is equally permissible to use other expressions in reference to that person, or to his business or place of business, which are likely to be understood as indicating that he is entitled to be described as a "European patent attorney" or "European patent agent."
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 277.04

The entitlement extends to other expressions likely to be understood as
indicating a right to be described as a European patent attorney or
European patent agent, for example "European patent agency".
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
