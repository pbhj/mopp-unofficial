::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 4A: Methods of treatment or diagnosis {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (4A.01 - 4A.31) last updated October 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 4A.01 {#hi}

This section relates to exceptions to patentability and novelty. It is
so framed to have, as nearly as practicable, the same effect in the UK
as the corresponding provisions of the EPC, PCT and CPC, ie Articles
53(c), 54(4) and 54(5) of the EPC.

[The Examination Guidelines for Patent Applications relating to Medical
Inventions in the UK Intellectual Property
Office](https://www.gov.uk/government/publications/examining-patent-applications-for-medical-inventions)
provide further details on practice in this field.

s.130(7) is also relevant

  -----------------------------------------------------------------------
   

  **Section 4A(1)**

  A patent shall not be granted for the invention of-\
  (a) a method of treatment of the human or animal body by surgery or
  therapy, or\
  (b) a method of diagnosis practised on the human or animal body.
  -----------------------------------------------------------------------

### 4A.02 {#ref4A-02}

The term "therapy" includes the prevention as well as the treatment or
cure of disease, as held by the Patents Court in [Unilever Limited
(Davis') Application, \[1983\] RPC
219](https://doi.org/10.1093/rpc/1983rpc219){rel="external"}. Although
some medical dictionaries cited pointed towards a narrow interpretation
of the term, other works of reference, including non-specialist
dictionaries, indicated a more general meaning; this was preferred in
this case, following the principle that words in statutes dealing with
matters relating to the general public are presumed to be used in their
popular, rather than their narrowly legal or technical, sense. However,
for a treatment to constitute therapy there must be a direct link
between the treatment and disease state being cured, prevented or
alleviated, as held by the hearing officer in [Commonwealth Scientific
and Industrial Research Organization's Application (BL
O/248/04)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F248%2F04&submit=Go+%BB){rel="external"}.
A photodynamic method of controlling wool growth to reduce the incidence
of blowfly strike and balanitis in sheep was therefore determined not to
be a method of therapy. Furthermore, in [Schering AG's Application
\[1971\] RPC
337](https://doi.org/10.1093/rpc/88.14.337){rel="external"}, it was held
that a method of chemical contraception was not considered to be
therapeutic. The prohibition does apply if a method of chemical
contraception is associated with a therapeutic method by means of
combined delivery system. This was decided by the EPO Technical Board of
Appeal in [The General Hospital Corporation's Application T820/92 (OJEPO
3/95)](https://www.epo.org/law-practice/case-law-appeals/recent/t920820ex1.html){rel="external"}
in which certain steroids were used in conjunction with a main
contraceptive ingredient to alleviate health problems caused by that
ingredient. The EPO Board of Appeal in another case, T74/93 (OJEPO
headnote 4/95), confirmed that methods of contraception in general are
not considered to be therapeutic, as pregnancy is not a disease. The
Board held that the claimed method in this case lacked industrial
applicability as it was a purely personal method carried out in private.
In the UK methods of contraception are not considered to lack industrial
application merely because they are for "private and personal use".
Under
[60(5)(a)](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-22)
of the Patents Act 1977 the private use of such a method would not
constitute an infringing act, and so a patent to such a method is
allowable.

### 4A.03 {#a03}

It appears that any medical treatment of a disease, ailment, injury or
disability, ie anything that is wrong with a patient and for which they
would consult a doctor, as well as prophylactic treatments such as
vaccination and inoculation, is to be regarded as therapy. The same
considerations apply for animals as for human patients, so that for
example prophylaxis and immunotherapy in animals are regarded as
therapy.

### 4A.03.1 {#ref4A-03-1}

In [Bristol-Myers Squibb v Baker Norton Pharmaceuticals Inc \[1999\] RPC
253](https://doi.org/10.1093/rpc/1999rpc253){rel="external"}, Jacob J
held that the exception should be construed narrowly. Its purpose is
merely to keep patent law from interfering directly with what a doctor
actually does to a patient, not to stop patent monopolies from
controlling what the doctor administers to the patient or the implements
that they use on the patient. However methods of therapy carried out on
materials temporarily removed from the body, for example when blood is
circulated through an apparatus while remaining in living communication
with the body, are not patentable (cf [Calmic Engineering Co Ltd's
Application, \[1973\] RPC
684](https://doi.org/10.1093/rpc/90.21.684){rel="external"}).

### 4A.03.2 {#ref4A-03-2}

In Ciba-Geigy AG's Application (BL O/30/85), objection was raised under
now repealed section 4(2) (equivalent to s.4A(1)) to certain claims for
a method of controlling parasitic helminths (worms which may develop in
the animal body, for example, in the intestinal tract of animals such as
sheep) by the use of a particular (novel and inventive) anthelmintic
composition. The hearing officer considered that such an infestation was
a disease requiring medical treatment of the animal and that such
treatment, whether curative or preventative, constituted therapy
practised on the animal body and consequently held that the claims in
question were not allowable.

### 4A.03.3 {#a033}

[Section 4A(1)](#hi) excludes only treatment by surgery or therapy, and
it follows that other methods of treatment of live human beings or
animals, eg treatment of a sheep in order to promote growth, to improve
the quality of mutton or to increase the yield of wool, are patentable
provided that (as would probably be the case) such methods are of a
technical, and not essentially biological, character. However, where an
increase in meat yield or other industrial benefit is merely an
inevitable consequence of improved health through therapeutic treatment,
then such a method is unpatentable. On the other hand, a claim to the
non-therapeutic use of antibiotics or other drugs may be acceptable if
the claimed effect is not a mere consequence of improved health. Where
the method set out in the claim may be patentable but could also cover
non-patentable embodiments, a claim must be clearly limited (e.g. by a
disclaimer) to methods which are patentable, and there must be support
in the description for a non-therapeutic method. If necessary, the
description should be amended to clarify that therapeutic methods do not
form the invention.

### 4A.04 {#ref4A-04}

Application of substances to the body for purely cosmetic purposes is
not therapy. In allowing claims to a process for improving the strength
and elasticity of human hair and finger nails, the High Court of
Australia observed that, while a process for the treatment of the human
body as a means of curing or preventing a disease or other disorder was
not patentable,

> Those who apply chemical preparations to the skin to prevent sunburn
> in climates which enjoy sunshine and moderate air temperatures can
> scarcely be regarded either as, in a relevant sense, treating their
> bodies or as undergoing treatment. On the other hand, the application
> to the skin of an ointment designed and effective to remove keratoges
> from the skin would be an instance of medical treatment. To be
> treatment in the relevant sense, it seems to me that the purpose of
> the application to the body whether of a substance or a process must
> be the arrest or cure of a disease or diseased condition or the
> correction of some malfunction or the amelioration of some incapacity
> or disability.([Joos v Commisioner of Patents, \[1973\] RPC
> 59](https://doi.org/10.1093/rpc/90.3.59){rel="external"}).

### 4A.05 {#ref4A-05}

In [Oral Health Products Inc (Halstead's) Application, \[1977\] RPC
612](https://doi.org/10.1093/rpc/1977rpc612){rel="external"}, claims to
a method of removing dental plaque and/or caries were refused, as was a
claim to a method of cleaning teeth which embraced both curative and
cosmetic effects. This decision has been followed under the 1977 Act in
ICI Ltd's Application No 7827383 (BL O/73/82), where a claim was refused
to a method of cleaning teeth which removed both plaque and stains; it
was argued that when applied to perfectly healthy teeth the method was
purely cosmetic, but the hearing officer observed that practically all
medical treatments which are preventative in nature (such as
vaccination) must at times be applied to people who would have remained
healthy anyway, but they remained medical treatments. It was held in
[Lee Pharmaceuticals' Applications, \[1978\] RPC
51](https://doi.org/10.1093/rpc/1978rpc51){rel="external"}, that since
one of the results of sealing pits and fissures in teeth was to prevent
the onset of dental decay, the purpose of the treatment was therapeutic
rather than cosmetic.

### 4A.06 {#ref4A-06}

Diagnosis is the identification of the nature of a medical illness,
usually by investigating its history and symptoms and by applying tests.
Determination of the general physical state of an individual (e.g. a
fitness test) is not considered to be diagnostic if it is not intended
to identify or uncover a pathology. [Section 4A(1)](#hi) relates to
methods of diagnosis practised on the human or animal body; diagnosis in
itself is a method of performing a mental act and is excluded from
patentability under section 1(2)(c). The scope of the term "diagnostic
methods practised on the human or animal body" within the meaning of
Article 53(c) EPC (equivalent to [section 4A(1)](#hi) is discussed by
the EPO Enlarged Board of Appeal decision in G 01/04 Diagnostic Methods
\[2006\] 5 OJEPO 334, \[2006\] EPOR 15. Typically, the process of
diagnosis involves a number of steps leading towards identification of a
condition. The Enlarged Board characterised these steps as being; (i)
the examination and collection of data, (ii) comparison of the data with
normal values, (iii) finding any deviation from the norm, and finally
(iv) attributing the deviation to a particular clinical picture. The
Enlarged Board held that for a claim to fall under this prohibition, it
must include both the deductive step of making the diagnosis (step iv)
and the preceding steps constructive for making that diagnosis involving
specific interactions of a technical nature with the human or animal
body. The exclusion is therefore a narrow one, and also requires all the
method steps of a technical nature to be practised on the body. However,
the Board pointed out that the exclusion could not be circumvented by
omitting one of the essential features of the diagnostic method as the
claim would then not satisfy Article 84 EPC (equivalent to section
14(5)(a) to (c)), since this requires that all the essential features
necessary for clearly and completely defining a particular invention are
present in the claim. In determining whether or not a method is
diagnostic, the Board held that it is irrelevant whether it is necessary
for a medical or veterinary practitioner to be involved. Furthermore, a
method is "practised on the human or animal body" if it involves any
interaction which necessitates the presence of the patient, so will
include both invasive and non-invasive methods.

### 4A.06.1 {#a061}

In practice, there are two key questions to be asked with any claim to a
diagnostic method. Firstly, does the claimed method include both the
first, measurement step and the final deductive step; i.e. does it allow
the disease or condition to be identified? (The intermediate steps (ii)
and (iii) may be inferred if the first and last steps are clearly
included.) Second, is the examination or measurement (step (i))
practised on the body? The remaining steps (ii) to (iv) are never
practised on the body, but are generally not technical in nature. If the
answer to both of these questions is "yes", objection should be made
under [Section 4A](#hi).

### 4A.07 {#a07}

Methods of diagnosis performed on tissues or fluids which have been
permanently removed from the body are not excluded, and so, for example,
genetic or immunological tests on blood or urine samples are patentable.
"Body" should be taken to mean living body, and a method practised on a
dead body, for example in order to determine the cause of death, would
not be excluded.

### 4A.08 {#ref4A-08}

Surgery is defined as the treatment of disease or injury by operation or
manipulation. It is not limited to cutting the body but includes
manipulation such as the setting of broken bones or relocating
dislocated joints (sometimes called "closed surgery"), and also dental
surgery. In general, any operation on the body which required the skill
and knowledge of a surgeon would be regarded as surgery (see also
[4A.10](#ref4A-10)). The Enlarged Board of Appeal in G 01/07
MEDI-PHYSICS/Treatment by surgery \[2011\] 3 OJEPO 134 held that a
method should be excluded if it comprises or encompasses an invasive
step which constitutes a substantial physical intervention on the body,
and which entails a significant health risk even when carried out by a
medical professional.

### 4A.09 {#ref4A-09}

In [Unilever Limited (Davis') Application, \[1983\] RPC
219](https://doi.org/10.1093/rpc/1983rpc219){rel="external"} (see also
[4A.02](#ref4A-02)), Falconer J observed that any method of surgical
treatment, whether curative, prophylactic or cosmetic, is not
patentable. This view, which was obiter, was cited by the hearing
officer in Occidental Petroleum Corporation's Application (BL O/35/84)
in refusing to allow claims to a method of implanting an embryo
transplant from a donor mammal into the uterus of a recipient mammal,
since the method would necessarily have to be carried out by a surgeon
or veterinary surgeon. In contrast, the EPO Technical Board of Appeal in
T 383/03 GENERAL HOSPITAL/Hair removal method \[2005\] 3 OJEPO 159 held
that the exclusion only applies to surgical methods which are
potentially suitable for "maintaining and restoring the health, the
physical integrity, and the physical well-being of a human being or
animal, and to prevent diseases". This was overturned by the Enlarged
Board of Appeal in G 01/07 MEDI-PHYSICS/Treatment by surgery \[2011\] 3
OJEPO 134, in which it was held that the purpose of the method is
irrelevant -- the exclusion is not limited to therapeutic or
reconstructive surgery. Instead, surgery is defined by the nature of the
method, and in particular the level of skill required and risk incurred.
This is consistent with previous UK practice, and with the Office
decision in Virulite Ltd's Application BL O/058/10. In this decision a
claimed method of treating the skin with electromagnetic radiation was
held not to be surgical on the grounds that it was not a significant
physical intervention -- the treatment did not damage cells or burn the
skin -- and the patent, if granted, would not interfere with the work of
a medical or veterinary practitioner in their treatment of patients.

### 4A.10 {#ref4A-10}

It may often be helpful, in deciding whether or not a method is excluded
under
[s.4A(1)](https://www.gov.uk/government/publications/the-patents-act-1977)
to consider who would in practice carry out the method. The apparent
purpose of
[s.4A(1)](https://www.gov.uk/government/publications/the-patents-act-1977)
is to prevent a medical practitioner being inhibited by legal
monopolies. Thus for example bone setting is regarded as surgery, while
applying a plaster cast is not, since the former is carried out by a
doctor, the latter by a technician. And in [Upjohn Co (Kirton's)
Application, \[1976\] RPC
324](https://doi.org/10.1093/rpc/93.12.324){rel="external"}, the hearing
officer observed that abortion in humans, in contradistinction to
contraception, necessarily involved a registered medical practitioner.
See also [4A.11](#ref4A-11). However, this consideration is not
decisive, and a method may still be considered to be therapeutic,
diagnostic or even surgical even if it can be carried out by non-medical
personnel. In G 01/07 MEDI-PHYSICS/Treatment by surgery \[2011\] 3 OJEPO
134 it was held that whether a method is excluded or not cannot depend
on who carries it out, not least because of the changing medical roles
in healthcare systems.

### 4A.11 {#ref4A-11}

Once it has been decided, in the way indicated in the preceding
paragraphs, that a method constitutes surgery, therapy or diagnosis
practised on the human or animal body, it is necessarily non-patentable.
For example, methods of abortion or induction of labour are always
considered to be unpatentable, irrespective of the reason for the
treatment. The fact that abortion or induction of labour may be carried
out for social reasons, or that these or other methods may be practised
on animals for reasons of agricultural economies, does not save them
from exclusion.

### 4A.11.1 {#ref4A-11-1}

Unlike section 1(2) of the Act, there is no proviso in s.4A(1) that
methods are only excluded "to the extent that a patent or application
for a patent relates to that thing as such". The EPO Enlarged Board of
Appeal in G 01/07 MEDI-PHYSICS/Treatment by surgery \[2011\] 3 OJEPO 134
confirmed a body of earlier EPO case law by holding that any multi-step
method which includes a step comprising a method of surgery or therapy
is excluded from patentability.

### 4A.12 {#a12}

Claims objected to under [section 4A(1)](#hi) are often in the form "A
method (or process) for treating medical or veterinary condition Y by
.....", but other forms of claim are also objected to such as "The use
of substance (or composition) X in treating medical condition Y by
.....", "A compound (or composition) X when used in the treatment of
medical condition Y by ....." and "The use of substance (or composition)
X as a pharmaceutical". The last of these claims is interpreted as a
method claim to the use of the substance in therapeutic treatment,
rather than simply a claim to its use in a pharmaceutical formulation.

### 4A.13 {#a13}

Patents may, however, be obtained for surgical, therapeutic or
diagnostic instruments or apparatus intended for use in such methods.
Also the manufacture of prostheses or artificial limbs and taking
measurements therefore on the human body are patentable. In addition,
tests on human or animal bodies should be regarded as patentable
inventions if they do not meet the criteria for "methods of diagnosis"
set out in [4.06](#hn).

### 4A.14 {#ref4A-14}

If a claim is capable of embracing both excluded and non-excluded
methods, amendment is necessary to limit the claim to methods which are
patentable, for example by disclaiming the excluded methods, and if
necessary, by amending the description to exclude the non-patentable
methods. Any disclaimers must meet the requirements set out in
14.126-14.127. The Enlarged Board of Appeal in G 01/07 MEDIPHYSICS/
Treatment by surgery \[2011\] 3 OJEPO 134 confirmed that an "undisclosed
disclaimer" (i.e. one where neither the disclaimer nor the subject
matter excluded by it is disclosed in the application as filed) to
exclude methods of treatment by therapy or surgery, or methods of
diagnosis practised on the human or animal body, is in principle
allowable and does not necessarily constitute added matter. Any form of
disclaimer is allowed which removes the objection and leaves the scope
of the monopoly remaining clear. While in [ICI (Richardson's)
Application \[1981\] FSR
609](https://uk.westlaw.com/Document/IC5D8ACE0E42711DA8FC2A0F0355337E9/View/FullText.html){rel="external"}
a claim which included a disclaimer employing the wording of the Act was
disallowed since it left the scope of what was being claimed unclear,
there is no objection in principle to any other form of disclaimer,
provided always that the scope of the monopoly remaining is clear, and
there is support in the description for the invention claimed.
Regardless of whether the disclaimer itself is disclosed, the subject
matter remaining in the claim after the introduction of the disclaimer
must be disclosed in the application as filed (G 02/10
SCRIPPS/Disclaimer OJEPO 2012, 376).

  --------------------------------------------------------------------------------------------------------------------------
   
  **Section 4A(2)**
  Subsection (1) above does not apply to an invention consisting of a substance or composition for use in any such method.
  --------------------------------------------------------------------------------------------------------------------------

### 4A.15 {#a15}

This subsection is for the avoidance of doubt; the exclusions of s.4A(1)
apply only to methods and not to materials to be used in such methods.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 4A(3)**
  In the case of an invention consisting of a substance or composition for use in any such method, the fact that the substance or composition forms part of the state of the art shall not prevent the invention from being taken to be new if the use of the substance or composition in any such method does not form part of the state of the art.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 4A.16 {#ref4A-16}

Section 4A(3) has the effect that a known substance or composition may
be patented for use in a method of treatment by surgery or therapy or a
method of diagnosis practised on the human or animal body, provided that
its use in any such method is new ("first medical use"). That is to say,
if a known substance or composition not previously used in surgery,
therapy or diagnosis is found to be useful in treating, say a human
disease, or to obtain a specific "therapeutic" effect (eg analgesic or
antibiotic), a patent for the substance or composition for use in
therapy (unspecified) may be obtained, i.e. the claim need not be
limited to the specific therapeutic effect; additional claims directed
towards more than one specific therapeutic effect may be allowed in the
same patent application, provided of course that they are supported by
the description. The terms surgery, therapy and diagnosis are discussed
in more detail in paragraphs [4A.02](#ref4A-02) - [4A.14](#ref4A-14).

### 4A.17 {#ref4A-17}

s.4A(1) is also relevant

This is an exception to the general rule ([see
2.14](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-14))
that a claim to a substance or composition for a particular purpose is
construed as a claim to the material per se, and that if the invention
lies in a new method of using a known material only the new method can
be claimed. (Where the new use is a method of surgery, therapy or
diagnosis practised on the human or animal body a claim to the method
would not be allowable). In a case where the main claims related to a
contraceptive composition comprising compounds that were already known
as pharmaceuticals, the EPO Technical Board of Appeal, in decisions
T303/90 and T401/90 (not reported), was of the opinion that the
composition as claimed could not be considered as novel and the added
word "contraceptive" did not change the product claim into a use claim.

### 4A.18 {#ref4A-18}

The first medical use of a known substance can be claimed for use in a
method of medical treatment the first time such a use is disclosed. Once
the use of the compound in any method of medical treatment has formed
part of the state of the art the same substance or composition may
subsequently be patented for use in treating a different disease,
whether human or animal, or in surgery or diagnosis as a second use (see
[4A.26](#hp) - [4A.31](#hq)).

### 4A.19 {#ref4A-19}

s.1(2)(d) is also relevant

In [Bayer A G (Meyer's) Application, \[1984\] RPC
11](https://doi.org/10.1093/rpc/1984rpc11){rel="external"}, the hearing
officer refused claims of the form "Commercial package containing as an
active pharmaceutical agent compound X together with (or bearing)
instructions ..." for treating condition Y; it was only the content of
the instructions which distinguished these claims from the prior art,
and this was mere presentation of information and not an invention [see
1.40.3](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-40-3)
See also [4A.20](#hr) below.

### 4A.20 {#ref4A-20}

A similar conclusion was reached in [John Wyeth and Brother Ltd's
Application, \[1985\] RPC
545](https://doi.org/10.1093/rpc/1985rpc545){rel="external"}. In that
case it was argued on behalf of the applicant that a claim to the use of
X in treating Y included procedures which are not practised on the human
or animal body but which are of an industrial nature, such as the
preparation of the compounds and of compositions containing them and
their packaging, including the enclosure of instructions, for the new
use; a decision of the German Federal Court of Justice
(Bundesgerichtshof), reported at OJEPO 1/84 pages 26-41, was cited in
support of this view. The hearing officer rejected a suggestion that he
was obliged, because of s.130(7), to follow this decision, and observed
that there appeared to be a fundamental difference of approach to the
validity of claims, since the German Court seemed to take the view that
because a claim includes some patentable matter it should be allowed,
whereas under UK law a claim as a whole is regarded as bad if it
includes something that is non-patentable. In the judgment on ([Schering
AG's Application \[1985\] RPC
545](https://doi.org/10.1093/rpc/1985rpc545){rel="external"} at page
556) and on the Wyeth case, the Patents Court agreed with the findings
of the hearing officer in the [Bayer AG (Meyer's) Application, \[1984\]
RPC 11](https://doi.org/10.1093/rpc/1984rpc11){rel="external"} but
followed the EPO Enlarged Board of Appeal (G 05/83 EISAI/Second medical
indication, \[1985\] 3 OJEPO 64) in allowing Swiss-type claims of the
form "the use of substance X for the manufacture of a medicament for a
specified new and inventive therapeutic application"; this type of claim
has now been superseded by the direct form of second medical use claim
as discussed in [4A.26](#hp) - [4A.28](#ref4A-28).

### 4A.21 {#a21}

A claim to a known substance or composition for the first use in
surgical, therapeutic and/or diagnostic methods should be in a form such
as "Substance or composition X ...." followed by the indication of the
use, for instance ".... for use as a medicament", ".... for use as an
antibiotic", or ".... for use in treating disease Y". There is no
objection to a claim being directed to substance X for use in a medical
method in the case where X is novel, whether or not the substance is
also claimed per se.

### 4A.22 {#ref4A-22}

EPO case law in relation to both first (T 1758/07) and second (T
1099/09) medical use claims indicates that protection for medical uses
of known substances or compositions is only available for the use of a
substance or composition as an active agent in medicine. The use of a
known substance or composition as an inactive carrier or excipient for a
therapeutic agent cannot therefore be protected by a first medical use
claim. Following the same logic, the previous use of the compound purely
as an inactive carrier or excipient for a therapeutic agent does not
anticipate a first medical use claim, as held in T 1758/07.

### 4A.23 {#ref4A-23}

[Section 4A(3)](#hu) is restricted to substances and compositions;
apparatus cannot be so protected. Claims may be allowed however under
[Section 4A(3)](#hu) to compositions having a surface and in the form of
an article ie having a shaped form.

### 4A.24 {#a24}

Since cosmetic methods of treatment of the human body are not considered
to be therapeutic ([see 4A.04](#ref4A-04)), a substance or composition
for use in a non-surgical cosmetic method cannot be protected by
[Section 4A(3)](#hu).

### 4A.25 {#a25}

To provide evidence of prior use of a substance or composition in
therapy, actual disclosure of therapeutic use must be found. It is not
sufficient for a research paper to disclose experiments which show an
activity which would make the substance or composition suitable for use
in therapy, or discloses in vitro testing for such a use. The section
requires the use of the substance or composition in a method of therapy
to form part of the state of the art. Such disclosures of experiments
and tests might of course be used as a basis for an obviousness attack
under section 3.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 4A(4)**
  In the case of an invention consisting of a substance or composition for a specific use in any such method, the fact that the substance or composition forms part of the state of the art shall not prevent the invention from being taken to be new if that specific use does not form part of the state of the art.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 4A.26 {#ref4A-26}

Before implementation in 2007 of the medical provisions of the 2000
revision of the EPC (EPC 2000 -- introduced into UK law by the Patents
Act 2004), a second or subsequent medical use could only be protected by
a claim in the "Swiss type" format of "the use of substance X for the
manufacture of a medicament to treat disease Y". Section 4A(4) now
enables patent protection to be obtained for a second, or subsequent,
different use of a substance or composition in a method of treatment or
diagnosis by a direct claim in the form "substance X for use in the
treatment of disease Y". A claim of this type for a known substance or
composition for use in a specific method of treatment or diagnosis is
treated as new if that specific use was previously unknown. This does
not extend the availability of patent protection in respect of an
invention consisting of a substance or a composition for use in a method
of treatment or diagnosis; it simplifies and clarifies the manner in
which patent protection may be obtained for such inventions.

### 4A.27 {#ref4A-27}

Following implementation of the EPC 2000, the practice in both this
Office and the EPO was to allow inventions relating to second medical
uses to be claimed using either the direct second medical use claim
format under Section 4A(4), or the Swiss-type format, or both, pending
guidance from the UK courts and/or the EPO Boards of Appeal. In February
2010, the EPO Enlarged Board of Appeal issued its decision in G 02/08
ABBOTT RESPIRATORY/Dosage regime \[2010\] 10 OJEPO 456. This decided
that applicants may no longer claim second medical use inventions in the
Swiss format. The Enlarged Board held that Swiss-type claims were
previously accepted (in G 05/83 EISAI/Second medical indication \[1985\]
3 OJEPO 64) as the only possible means of protecting inventions relating
to second medical uses in order to fill a loophole in the provisions of
the EPC 1973. The new Article 54(5) (equivalent to Section 4A(4)) fills
this loophole by explicitly allowing claims to the further specific use
of a known drug, and so the reason for this special, "judge-made" law no
longer exists.

### 4A.27.1 {#ref4A-27-1}

Claims in the "Swiss-type" format in new or pending applications should
be objected to on grounds of clarity, and must either be deleted or
replaced by claims in the direct second medical use format "substance X
for use in the treatment of disease Y". This change in practice was
announced in a Practice Notice issued on 26 May 2010. The basis of the
objection is that a Swiss-type claim lacks clarity because, although it
is worded as defining a method of manufacturing a medicament, the
invention in fact relates to the intended use of the product rather than
the method of manufacture. As stated in G 02/08, there is no functional
relationship between the feature conferring novelty (the intended use)
and the claimed manufacturing process. Amendment of a patent application
to replace a Swiss-type claim with the new form of second medical use
claim is not considered to add matter, since regardless of the wording
or scope of the claim, the technical disclosure (i.e., a new medical use
for a substance or composition) is the same. Similarly, replacement of
an unpatentable method of treatment claim with a second medical use
claim protecting the same use does not add matter. This practice does
not have any bearing on the validity of patents already granted and
including Swiss-type claims.

A request to make a post-grant amendment to replace Swiss-type claims
with the new form of second medical use claim is unlikely to succeed,
particularly as the Enlarged Board in G 02/08 suggested that the new
form of claim may have a broader scope than Swiss-type claims. Moreover,
in an earlier Technical Board of Appeal decision (T 250/05) it was held
that a post-grant amendment from a Swiss-type claim to the new form of
second medical use claim would extend the scope of protection and thus
would not be allowable under Art. 123(3) EPC (equivalent to s.76(3)(b)).
The Boards of Appeal in T 1780/12 and T 879/12 highlighted this
difference in scope and also claim category between Swiss-type claims
(which, strictly, are process claims) and the new form of second medical
use claims (which are "purpose-limited" product claims). For this
reason, it was concluded in both these decisions that where a divisional
application included the new claim form, and the granted parent claimed
the same medical use in the Swiss form, this did not constitute
"double-patenting". No objection should therefore be raised under
s.18(5) or s.73(2) to conflict between applications including the new
form of second medical use claim and granted GB or EP patents which
protect the same medical use solely through Swiss-type claims. The
Patents Court in [Generics (UK) Ltd (t/a Mylan) v Warner-Lambert Company
LLC \[2015\] EWHC
2548](http://www.bailii.org/ew/cases/EWHC/Patents/2015/2548.html){rel="external"}
held that Swiss-type claims are process claims -- this construction was
confirmed by the Supreme Court ([Warner-Lambert Company LLC v Generics
(UK) Ltd (t.a. Mylan) & Anor. \[2018\] UKSC
56](http://www.bailii.org/uk/cases/UKSC/2018/56.html){rel="external"}) -
this has consequences for infringement as discussed in
[60.16.2](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-16-2)
and
[60.19.2](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-19-2).

### 4A.28 {#ref4A-28}

The protection of second medical uses by Swiss-type claims was
originally allowed by the Enlarged Board of Appeal in G 05/83
EISAI/Second medical indication \[1985\] 3 OJEPO 64, and this was
followed by the Patents Court in John Wyeth's and Schering's
Applications \[1985\] RPC 545. The Enlarged Board in G 05/83 also
decided that a claim to the use of a substance for treatment was
regarded as confined to the step of treatment and was thus contrary to
a.52(4) EPC \[1973\]. (See also 4A.20). Despite the fact that Swiss-type
claims are no longer allowable, the case law that has developed from the
UK courts and EPO Boards of Appeal concerning second medical use claims
in the Swiss form continues to govern our practice in relation to the
validity of inventions in this field. Second medical use claims which
relate to the use of a substance for the same therapeutic application as
the prior art, but claiming a different technical effect or mechanism of
action, should be objected to for lack of novelty, and may also be
excluded from patentability as a discovery. In [Bristol-Myers Squibb v
Baker Norton Pharmaceuticals \[1999\] RPC
253](https://doi.org/10.1093/rpc/1999rpc253){rel="external"}, the
Patents Court held that a new piece of information about how a treatment
worked did not constitute an invention if it did not lead to a new use,
which was upheld by the Court of Appeal \[2001\] RPC 1. In this case,
the drug, method of administration and therapeutic purpose were exactly
the same as given in a prior lecture; the only difference was the
discovery that if the drug was infused over a shorter time period, an
undesirable side-effect was less and the therapeutic effect remained.
This decision was followed by the Patents Court in [El-Tawil's
Application \[2012\] EWHC
185](https://www.bailii.org/ew/cases/EWHC/Ch/2012/185.html){rel="external"}.
In this case a claim was considered to relate to a combination of newly
discovered technical effects, and newly discovered advantages of a known
treatment, neither of which conferred novelty. Similarly, in [Actavis UK
Ltd v Janssen Pharmaceutica NV \[2008\] EWHC 1422, \[2008\] FSR
35](https://www.bailii.org/ew/cases/EWHC/Patents/2008/1422.html){rel="external"},
it was held that "merely explaining the mechanism which underlies a use
already described in the prior art cannot, without more, give rise to
novelty".

### 4A.28.1 {#ref4A-28-1}

The Court of Appeal in [Bristol-Myers Squibb v Baker Norton
Pharmaceuticals \[2001\] RPC
1](https://doi.org/10.1093/rpc/2001rpc0){rel="external"} also held that
second medical use claims must define a new and inventive purpose; ie
the treatment of a new disease. It was held that claims which defined
the new use in terms of, for example, a new dosage regime, were
disguised method of treatment claims, and also lacked novelty over the
use of the substance to treat the same disease at a different dosage.
However, the Court of Appeal in [Actavis v Merck \[2008\] RPC
26](https://doi.org/10.1093/rpc/rcn023){rel="external"} reversed this
aspect of the Bristol-Myers Squibb v Baker Norton Pharmaceuticals
decision and decided that a second medical use claim which defined the
use in terms of a new and inventive dosage regime was valid, despite the
fact that the substance in question had been used in the prior art to
treat the same condition (alopecia) at a different dosage. It was
emphasised that in most cases a new dosage regime would not be
inventive, as investigating appropriate dosage regimes is standard
practice.

### 4A.28.2 {#ref4A-28-2}

The reasons the Court of Appeal gave in Actavis UK Ltd v Merck & Co Inc.
\[2008\] RPC 26 for departing from its own precedent (in [Bristol-Myers
Squibb v Baker Norton Pharmaceuticals \[2001\] RPC
1](https://doi.org/10.1093/rpc/2001rpc0){rel="external"}) were
threefold. Firstly it was decided that Bristol-Myers Squibb v Baker
Norton Pharmaceuticals provided no clear ratio in relation to novelty,
and so was not binding in this respect. Secondly, that the ratio of
Bristol-Myers Squibb v Baker Norton Pharmaceuticals in respect of the
method of treatment objection was on the narrow grounds that the claim
at issue in that case was directed at the actions of the doctor rather
than the manufacturer, and this was not the case with dosage regime
claims in general. Finally, the Court of Appeal in any case felt it was
able (but not bound) to depart from its own precedent on the grounds
that the earlier decision was inconsistent with the "settled view" of
European patent law as interpreted by EPO Board of Appeal decisions;
which had allowed second medical use claims which define a new dosage
regime or method of administration. The Enlarged Board of Appeal in G
02/08 ABBOTT RESPIRATORY/Dosage regime \[2010\] 10 OJEPO 456 confirmed
earlier EPO case law in allowing second medical use claims to new and
inventive dosage regimes, even where the substance or composition, and
the disease to be treated, are the same as in the prior art. Second
medical use claims which are distinguished from the prior art solely by
the dosage regime used are therefore allowable if the claimed use is
both new and inventive.

### 4A.28.3 {#ref4A-28-3}

As with first medical use claims, second medical use claims can only
derive novelty from their intended use if the use is in a medical method
excluded under s.4A(1), and so cannot be used to protect the new use of
a known substance in, for example, a cosmetic method. Section 4A(4) is
restricted to substances and compositions; apparatus cannot be so
protected. In T 1099/09 it was held that second medical use claims can
only be used to protect the use of a known substance or composition as
an active agent. The use of a known substance or composition as an
inactive carrier or excipient for a therapeutic agent cannot therefore
be protected by a second medical use claim.

### 4A.29 {#ref4A-29}

Second medical use claims to the further medical use of a substance or
composition must be supported by evidence, in the application as filed,
that the substance or composition is (or at least is likely to be)
effective for the specified use. In [Prendergast's Applications \[2000\]
RPC 446](https://doi.org/10.1093/rpc/2000rpc446){rel="external"}, it was
held that tests showing that the known substance or composition works in
the proposed new circumstances are an essential part of the description
if second medical use claims are to be adequately supported under
s.14(5)(c). (Also [see
14.152](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-152)).
This ensures that purely speculative second medical use claims are not
allowed. However, Neuberger J emphasised that rudimentary tests would
suffice and that full, detailed and rigorous testing of the drug for the
proposed condition is not necessary. The requirement for some
experimental support for second medical use claims was confirmed by the
Patents Court decision in [El-Tawil's Application \[2012\] EWHC
185](https://www.bailii.org/ew/cases/EWHC/Ch/2012/185.html){rel="external"},
and so an objection of lack of support should be made if such evidence
is lacking, or is only present for some of the claimed diseases.
Nevertheless, a claim to a broader class of diseases may be justified if
the applicant can show that it could reasonably be predicted from the
demonstrated activity that the substance or composition will treat the
diseases in question. In Regeneron Pharmaceuticals Inc v Genentech Inc
\[2012\], EWHC 657 (Pat) (upheld at appeal \[2013\] EWCA Civ 93), it was
held, on the facts of the case, that it was reasonable to predict that
an anti-angiogenic effect demonstrated in tumours would also extend to
non-cancerous diseases characterised by excessive angiogenesis (growth
of new blood vessels into a tissue).
:::

::: {#default-id-5d2da248 .gem-c-accordion .govuk-accordion .govuk-!-margin-bottom-6 module="govuk-accordion gem-accordion ga4-event-tracker" ga4-expandable="" anchor-navigation="true" show-text="Show" hide-text="Hide" show-all-text="Show all sections" hide-all-text="Hide all sections" this-section-visually-hidden=" this section"}
::: govuk-accordion__section
::: govuk-accordion__section-header
## [4A.29.1]{#default-id-5d2da248-heading-1 .govuk-accordion__section-button} {#ref4A-29-1 .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"4A.29.1\",\"index_section\":1,\"index_section_count\":3}"}
:::

::: {#default-id-5d2da248-content-1 .govuk-accordion__section-content aria-labelledby="default-id-5d2da248-heading-1" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"4A.29.1\",\"index_section\":1,\"index_section_count\":3}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
\[Moved to 4A.30.1\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [4A.29.2]{#default-id-5d2da248-heading-2 .govuk-accordion__section-button} {#ref4A-29-2 .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"4A.29.2\",\"index_section\":2,\"index_section_count\":3}"}
:::

::: {#default-id-5d2da248-content-2 .govuk-accordion__section-content aria-labelledby="default-id-5d2da248-heading-2" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"4A.29.2\",\"index_section\":2,\"index_section_count\":3}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
These decisions relate to the requirement under s.14(5)(c) that a second
medical use claim must be supported by the description. In addition,
there is a body of case law relating to the requirements for sufficiency
under s.14(3) for applications or patents relating to a second medical
use. The UK Courts have followed the EPO Technical Board of Appeal's
decision in [T
609/02](https://www.epo.org/law-practice/case-law-appeals/recent/t020609eu1.html){rel="external"}
in holding that the claimed medical use is a "functional technical
feature" of the claim. This means, firstly, that if the agent is not
effective for the treatment of the disease, then the application or
patent is insufficient. In [Eli Lilly & Co v Janssen Alzheimer
Immunotherapy \[2013\] EWHC
1737](http://www.bailii.org/ew/cases/EWHC/Patents/2013/1737.html){rel="external"},
evidence obtained after the filing date (the failure of subsequent
clinical trials) was held to show that the teaching of the patent was
insufficient.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [4A.29.3]{#default-id-5d2da248-heading-3 .govuk-accordion__section-button} {#ref4A-29-3 .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"4A.29.3\",\"index_section\":3,\"index_section_count\":3}"}
:::

::: {#default-id-5d2da248-content-3 .govuk-accordion__section-content aria-labelledby="default-id-5d2da248-heading-3" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"4A.29.3\",\"index_section\":3,\"index_section_count\":3}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
Secondly, the Board of Appeal in T 609/02 held that the specification
must disclose the suitability of the agent for the claimed therapeutic
application. Absolute proof of efficacy, or clinical trials, was not
necessary, but a simple assertion was not enough. The approach taken in
T 609/02 was followed by the Court of Appeal in [Regeneron
Pharmaceuticals Inc v Genentech Inc \[2013\] EWCA Civ
93](http://www.bailii.org/ew/cases/EWCA/Civ/2013/93.html){rel="external"},
which held that the specification must make it "plausible" that the
invention would work (i.e. be effective to treat the disease) across the
claimed scope. This concept was summarised by Birss J in [Hospira UK Ltd
v Genentech Inc \[2014\] EWHC
1094](http://www.bailii.org/ew/cases/EWHC/Patents/2014/1094.html){rel="external"}:
"The term "plausibility" has been coined to characterise what it is that
a patent specification must provide in order to be sufficient, short of
full clinical proof of efficacy."

### 4A.29.4 {#ref4A-29-4}

This requirement for plausibility to render an application or patent
relating to a second medical use sufficient was confirmed by the Supreme
Court in Warner-Lambert Company LLC v Generics (UK) Ltd (t.a. Mylan) &
Anor. \[2018\] UKSC 56. Moreover, the majority view in this case (as set
out by Lord Sumption) set out a number of principles to assess the
sufficiency of medical use claims:\
\
i) The proposition that a product is effective for the treatment of a
given condition must be plausible.\
\
ii) It is not made plausible by a bare assertion to that effect, and the
disclosure of a mere possibility that it will work is no better than a
bare assertion.\
\
iii) The claimed therapeutic effect may be rendered plausible by a
specification showing that something is worth trying for a reason; i.e.
not just because there is an abstract possibility that it would work but
because reasonable scientific grounds are disclosed for expecting that
it might well work. The disclosure of those grounds marks the difference
between a speculation and a contribution to the art.\
\
iv) Although the disclosure need not definitively prove the assertion
that the product works for the designated purpose, there must be
something that would cause the skilled person to think that there was a
reasonable prospect that the assertion would prove to be true.\
\
v) That reasonable prospect must be based on a direct effect on a
mechanism specifically involved in the disease, this mechanism being
either known from the prior art or demonstrated in the patent per se.\
\
vi) This effect on the disease process need not necessarily be
demonstrated by experimental data. It can also be demonstrated by a
priori reasoning.\
\
vii) This evidence or reasoning must appear in the patent. The
disclosure may be supplemented or explained by the common general
knowledge of the skilled person. However, it is not enough that the
patentee can prove that the product can reasonably be expected to work
in the designated use, if the skilled person would not derive this from
the teaching of the patent.\
\
It was emphasised that the specification as filed must make the claimed
use plausible; data filed after the filing date of the patent can only
be used to confirm an effect made plausible in the specification or to
refute a contention that the treatment does not actually work; it cannot
be a substitute for sufficient disclosure in the specification.

It is at present unclear whether the requirements for support for second
medical use claims based on the decisions discussed in 4A.29 to 4A.29.3
is in any way different to the requirements for sufficiency following
the criteria set out in [Warner-Lambert Company LLC v Generics (UK) Ltd
(t.a. Mylan) & Anor. \[2018\] UKSC 56 \[2018\] RPC
21](http://www.bailii.org/uk/cases/UKSC/2018/56.html){rel="external"}.
It is common ground between these decisions that mere assertion of
efficacy is not enough. However, in [Prendergast's
Applications](https://doi.org/10.1093/rpc/2000rpc446){rel="external"} it
was not suggested that the "material to enable the relevantly skilled
man to say this medicament does treat the condition alleged" could be
anything other than experimental evidence (of at least rudimentary
nature). On the other hand, in Warner Lambert v Generics it was
explicitly stated that plausibility could instead be derived from a
priori reasoning (point vi). In view of the observation in [Biogen Inc v
Medeva plc \[1997\] RPC
1](https://doi.org/10.1093/rpc/1997rpc1){rel="external"} that the
requirements for sufficiency in s.14(3) and for support in s.14(5)(b)
both relate to the requirement for an enabling disclosure, it is likely
that the Courts would consider these to be essentially the same
requirement.

### 4A.29.4.1 {#ref4A-29-4-1}

In [Sandoz Ltd v Teva Pharmaceutical Industries Ltd \[2022\] EWHC 822
(Pat)](https://www.bailii.org/cgi-bin/format.cgi?doc=/ew/cases/EWHC/Patents/2022/822.html&query){rel="external"}
the court held that the use of a factor Xa inhibitor, apixaban, for the
treatment of thromboembolic disorders, a product falling within the
scope of Bristol-Myers Squib's (BMS) patent and the subject of a
corresponding SPC, was not plausible based on the specification as
filed. The patent in question contained a single statement that some
compounds had been tested and found to have some binding affinity for
factor Xa, however it was held that there was no way for the skilled
person to derive any inference from this statement alone about any
individual compound, be it apixaban or any other, regarding factor Xa
binding. BMS attempted to argue that apixaban's activity could be
predicted by the skilled person a priori as a consequence of structural
considerations based solely on their common general knowledge. Rejecting
this argument, Meade J also reaffirmed the importance of the 7th
Warner-Lambert principle (see discussion of Warner-Lambert above) that
the requirement for plausibility must be met by a contribution from the
patentee that is in the specification as filed -- complete reliance on
common general knowledge (CGK) is not enough, as CGK does not form part
of the contribution of the patentee.

### 4A.29.5 {#ref4A-29-5}

The Supreme Court in in [Warner-Lambert Company LLC v Generics (UK) Ltd
(t.a. Mylan) & Anor. \[2018\] UKSC
56](http://www.bailii.org/uk/cases/UKSC/2018/56.html){rel="external"}
followed the principle set out in [Biogen Inc v Medeva plc \[1997\] RPC
1](https://doi.org/10.1093/rpc/1997rpc1){rel="external"} that
sufficiency needs to be present across the breadth of the claim, and so
for second medical use claims covering different conditions, the
invention must be enabled across their full scope. Nevertheless, a claim
to a broader class of diseases may be justified if the applicant can
show that it could reasonably be predicted from the demonstrated
activity that the substance or composition will treat the diseases in
question. In Regeneron Pharmaceuticals Inc v Genentech Inc \[2012\],
EWHC 657
(Pat)\](http://www.bailii.org/ew/cases/EWHC/Patents/2012/657.html)
(upheld at appeal [\[2013\] EWCA Civ
93)](http://www.bailii.org/ew/cases/EWCA/Civ/2013/93.html){rel="external"},
it was held, on the facts of the case, that it was reasonable to predict
that an anti-angiogenic effect demonstrated in tumours would also extend
to non-cancerous diseases characterised by excessive angiogenesis
(growth of new blood vessels into a tissue).

### 4A.30 {#ref4A-30}

Insufficiency by excessive claim breadth has also been considered by the
Courts in terms of the breadth of the class of agents, in addition to
the range of conditions to be treated. The Court of Appeal in [American
Home Products Corp. v Novartis Pharmaceuticals UK Ltd \[2000\] EWCA Civ
231](http://www.bailii.org/ew/cases/EWCA/Civ/2000/231.html){rel="external"},
\[2001\] RPC 8\](https://academic.oup.com/rpc/article/118/5/159/1587754)
considered second medical use claims for a new use of a known
antibiotic, rapamycin. The Court found that, had the claim been
construed as covering derivatives (which it was not -- [see
4A.31](#ref4A-31)), the patent would have been insufficient because
there was no disclosure in the description enabling the skilled person
to decide which of the many possible derivatives would have worked ([see
also 14.82-
14.84](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-82)).
However, if the specification discloses a general principle capable of
general application, a claim in correspondingly general terms may be
acceptable -- in [Regeneron Pharmaceuticals Inc v Genentech Inc \[2012\]
EWHC 657
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2012/657.html){rel="external"}
(upheld at appeal [\[2013\] EWCA Civ
93)](http://www.bailii.org/ew/cases/EWCA/Civ/2013/93.html){rel="external"},
this test was applied and a functionally-defined claim to the use of
antagonists of a particular receptor was considered to be a fair
generalisation.

### 4A.30.1 {#ref4A-30-1}

The Patent's Court in [Hospira UK Ltd v Genentech Inc \[2014\] EWHC
1094](http://www.bailii.org/ew/cases/EWHC/Patents/2014/1094.html){rel="external"}
held that in order to establish a priority claim in the case of a second
medical use invention, the earlier application must show that the
claimed therapeutic effect is plausible. If this is not the case, then
the earlier disclosure would not be considered enabling. Therefore, if a
claimed priority application does not include any evidence beyond mere
assertion of a therapeutic effect then the examiner should disregard
this claimed priority date for the purpose of assessing novelty and
inventive step ([see
5.23.2](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-23-2)).

### 4A.31 {#ref4A-31}

The Court of Appeal's decision in [American Home Products Corp. v
Novartis Pharmaceuticals UK Ltd \[2000\] EWCA Civ 231, \[2001\] RPC
8](http://www.bailii.org/ew/cases/EWCA/Civ/2000/231.html){rel="external"}
also considered issues relating to the construction of second medical
use claims. The Court of Appeal reversed the lower court's decision and
held that the claim -- to the use of rapamycin for the preparation of a
medicament for inhibiting organ or tissue transplant rejection -- did
not cover derivatives of rapamycin, thus finding the claim not infringed
by the use of a rapamycin derivative as an immunosuppressant.
Furthermore, the claim was held not to be infringed by the use of
rapamycin to produce a derivative-based medicament, since this would
require a wide construction of the term "medicament" in the claim (that
is, to mean a medicament not restricted to one comprising rapamycin),
and this would leave the claim hopelessly broad. The court also held
that the claim was not infringed if rapamycin was present as an impurity
in the derivative-based medicament, since there was no suggestion that
at the quoted impurity levels the rapamycin had any effect.

In [Bristol-Myers Squibb Co. v Baker Norton Pharmaceuticals Inc.
\[2001\] RPC
1](http://www.bailii.org/ew/cases/EWCA/Civ/2000/169.html){rel="external"},
it was held that a second medical use claim directed to the manufacture
of a medicament "for treating cancer" should be construed as "suitable
for trying to treat cancer", since the skilled person would realise that
drugs which are suitable for treatment will not always have a 100%
success rate. However, drugs which are perceived as being suitable for
treatment, but actually have no effect, do not fall within the scope of
the claim because they are not, in fact, suitable. Where two or more
substances are used together to treat a disease, then each falls within
the scope of a second medical use claim for the use of that substance
([Pfizer Ltd's Patent \[2001\] FSR
16](https://uk.westlaw.com/Document/IE0E6F1E0E42711DA8FC2A0F0355337E9/View/FullText.html){rel="external"}
at paragraphs 40-43). However, the previous use of the compound purely
as an inactive carrier or excipient for a therapeutic agent does not
anticipate a second medical use claim, as held in T 1758/07. In [Hospira
UK Ltd v Genentech Inc \[2014\] EWHC
1094](http://www.bailii.org/ew/cases/EWHC/Patents/2014/1094.html){rel="external"}
it was further explained (at paragraph 56) that "for treatment of
\[disease Y\]" means "suitable and intended for treatment" -- this
definition was considered to apply to claims both in the Swiss form and
in the "product for use" form. As second medical use claims are
construed as being limited to the intentional treatment of the disease,
the fact that the prior art use of the substance to treat a different
condition may have inherently treated or prevented the claimed disease
in some patients does not constitute an anticipation if the prior art
does not disclose the new therapeutic use.

In an interim decision, the Court of Appeal in [Warner-Lambert Company,
LLC v Actavis Group Ptc EHF & Ors \[2015\] EWCA Civ
556](http://www.bailii.org/ew/cases/EWCA/Civ/2015/556.html){rel="external"}
held that "intended for" means that the manufacturer would know or could
reasonably foresee that the product would be used for the claimed
therapeutic method. In applying this test at the full trial ([Generics
(UK) Ltd (t/a Mylan) v Warner-Lambert Company LLC \[2015\] EWHC
2548](http://www.bailii.org/ew/cases/EWHC/Patents/2015/2548.html){rel="external"})
Arnold J held that this meant the manufacturer would know or could
reasonably foresee that a doctor or pharmacist would intentionally
prescribe or administer the manufacturer's drug for the claimed purpose.
On appeal from this decision, the Court of Appeal (Warner-Lambert
Company v Generics (UK) Ltd (t/a Mylan) & Ors \[2016\] EWCA Civ 1006)
held that Arnold J's interpretation was too restrictive -- instead, all
that is required (to fall within the scope of the claim) is that the
manufacturer could reasonably foresee that there would be intentional
use of the drug for the claimed medical use. However, the Supreme Court
([Warner-Lambert Company LLC v Generics (UK) Ltd (t.a. Mylan) & Anor.
\[2018\] UKSC
56](http://www.bailii.org/uk/cases/UKSC/2018/56.html){rel="external"})
unanimously rejected the Court of Appeal's interpretation of the scope
of "Swiss-type" second medical use claims, and held that it was too
broad an interpretation to say that "the use of \[substance X\] in the
manufacture of a medicament for the treatment of \[disease Y\]"
encompassed any manufacture of the product where the manufacturer could
"reasonably foresee" that the product would be used for the claimed
therapeutic method, although it was emphasised that this question (which
related to infringement) was obiter as the patent was found invalid
([see
60.16.1](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-16-1)).
:::
:::
:::
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
