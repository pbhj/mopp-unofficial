::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 41: Amount of compensation {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (41.01 - 41.11) last updated: January 2020.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 41.01

This is the third of the group of sections relating to inventions made
by employees, and concerns the amount of compensation to be awarded to
an employee inventor by the court or the comptroller in proceedings
under s.40(1) or (2), [see
40.04-15](/guidance/manual-of-patent-practice-mopp/section-40-compensation-of-employees-for-certain-inventions/#ref40-04)
It also makes provision for further applications under s.40 after
refusal to order an award of compensation, for variation of such an
order and for enforcement of such an order made by the comptroller. This
section was amended by the Patents Act 2004 in consequence to the
changes made to section 40 to allow compensation to be awarded in
respect of all outstanding benefits deriving from a patented invention,
applying to patent applications made from 1 January 2005 onwards ([see
40.02](/guidance/manual-of-patent-practice-mopp/section-40-compensation-of-employees-for-certain-inventions/#ref40-02)).

### 41.02

For the applicability of s.41 and interpretation of certain terms used
therein, [see
39.01-04](/guidance/manual-of-patent-practice-mopp/section-39-right-to-employees-inventions/#ref39-01).

  -----------------------------------------------------------------------
   

  **Section 41(1)**

  An award of compensation to an employee under section 40(1) and (2)
  above in relation to a patent for an invention shall be such as will
  secure for the employee a fair share (having regard to all the
  circumstances) of the benefit which the employer has derived, or may
  reasonably be expected to derive, from any of the following ­\
  (a) the invention in question;\
  (b) the patent for the invention;\
  (c) the assignment, assignation or grant of ­\
  (i) the property or any right in the invention, or\
  (ii) the property in, or any right in or under, an application for the
  patent, to a person connected with the employer.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 41(2)**

  For the purposes of subsection (1) above the amount of any benefit
  derived or expected to be derived by an employer from the assignment,
  assignation or grant of ­\
  (a) the property in, or any right in or under, a patent for the
  invention or an application for such a patent; or\
  (b) the property or any right in the invention; to a person connected
  with him shall be taken to be the amount which could reasonably be
  expected to be so derived by the employer if that person had not been
  connected with him.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 41(3)**

  Where the Crown , United Kingdom Research and Innovation or a Research
  Council in its capacity as employer assigns or grants the property in,
  or any right in or under, an invention, patent or application for a
  patent to a body having among its functions that of developing or
  exploiting inventions resulting from public research and does so for no
  consideration or only a nominal consideration, any benefit derived from
  the invention, patent or application by that body shall be treated for
  the purposes of the foregoing provisions of this section as so derived
  by the Crown, United Kingdom Research and Innovation or the Research
  Council as the case may be).\
  \
  In this subsection "Research Council" means a body which is a Research
  Council for the purposes of the Science and Technology Act 1965.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 41(4)**

  In determining the fair share of the benefit to be secured for an
  employee in respect of an invention which has always belonged to an
  employer, the court or the comptroller shall, among other things, take
  the following matters into account, that is to say\
  (a) the nature of the employee's duties, his remuneration and the other
  advantages he derives or has derived from his employment or has derived
  in relation to the invention under this Act;\
  (b) the effort and skill which the employee has devoted to making the
  invention;\
  (c) the effort and skill which any other person has devoted to making
  the invention jointly with the employee concerned, and the advice and
  other assistance contributed by any other employee who is not a joint
  inventor of the invention; and\
  (d) the contribution made by the employer to the making, developing and
  working of the invention by the provision of advice, facilities and
  other assistance, by the provision of opportunities and by his
  managerial and commercial skill and activities.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 41(5)**

  In determining the fair share of the benefit to be secured for an
  employee in respect of an invention which originally belonged to him,
  the court or the comptroller shall, among other things, take the
  following matters into account, that is to say\
  (a) any conditions in a licence or licences granted under this Act or
  otherwise in respect of the invention or the patent for it;\
  (b) the extent to which the invention was made jointly by the employee
  with any other person; and\
  (c) the contribution made by the employer to the making, developing and
  working of the invention as mentioned in subsection (4)(d) above.
  -----------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 41(6)**
  Any order for the payment of compensation under section 40 above may be an order for the payment of a lump sum or for periodical payment, or both.
  ----------------------------------------------------------------------------------------------------------------------------------------------------

### 41.03

s.43(7) is also relevant.

An award of compensation should be such as to secure for the employee
who made the invention a fair share (having regard to all the
circumstances) of the benefit derived or reasonably expected to be
derived by the employer. Benefit means benefit in money or money's worth
and, where the employer has died, is determined in accordance with
s.43(5) ([see
40.07](/guidance/manual-of-patent-practice-mopp/section-40-compensation-of-employees-for-certain-inventions/#ref40-07)).

### 41.04 {#ref41-04}

Where the property or any right in the invention or any patent for it or
application for such a patent has been assigned or granted to another
person or body, s.41(2) and (3) may be applicable to assessment of the
benefit to the employer. Subsection (2) applies where it has been
assigned or granted to a person connected with the employer, The effect
of this subsection is that the award is based on the benefit that the
employer would have been expected to derive from the transaction had
that person not been connected to the employer. The test for determining
whether the other party is connected to the employer is set out in
s.43(8).

The Supreme Court in Shanks v Unilever \[2019\] UKSC 45 held that,
although s.41(2) says that it has effect for the purposes of s.41(1), it
must also have effect for the purposes of s.40 for the legislative
scheme to operate effectively.

### 41.04.1 {#ref41-04-1}

The Court of Appeal in Shanks v Unilever \[2011\] RPC 12 held that "that
person" in s.41(2) refers to the actual assignee, with the same
attributes as the real person, but without the connection to the
employer. Therefore, if the actual assignee had not fully exploited the
invention or patent, then this would be reflected in the value of the
compensation. This overturned the decision of the Patents Court (Shanks
v Unilever \[2010\] RPC 11), which had held that the assessment of this
hypothetical benefit should be based on the premise that the transaction
with "that person" was with a notional non-connected counterparty
operating in the appropriate market at the appropriate time. At the same
time, the Court of Appeal held that the compensation should take account
of the actual benefit that the assignee had derived from the patent or
invention to date, together with the likely future benefit if the patent
is still in force. The Court rejected the argument (from the employer
and assignee) that the compensation should be based on what the likely
value of the transaction would have been on the open market at the time
it was made, without the benefit of hindsight. It was considered that
this would lead to an unjust result as in many cases the value of a
patent is not known at the time of assignment.

### 41.05 {#ref41-05}

Matters to be taken into account in determining a fair share of the
benefit are set out, where the invention has always belonged to the
employer, in s.41(4) and, where the invention originally belonged to the
employee, in s.41(5). Kelly & Anor v GE Healthcare Ltd \[2009\] EWHC 181
(Pat), \[2009\] RPC 12 was the first successful s.40 employee
compensation claim in the UK. In this case, having determined the
benefits of the patents to the company under the pre-2005 form of s.40,
Floyd J took each of the factors set out in s.41(4) into account before
deciding what was a fair and just share of the benefit for each of the
employees concerned.

### 41.06 {#section-3}

The court or the comptroller may order payment of a lump sum and/or
periodical payment as compensation, to be paid by the employer. For
enforcement of orders made by the comptroller for such payment, [see
41.11](#ref41-11).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 41(7)**
  Without prejudice to section 32 of the Interpretation Act 1889 (which provides that a statutory power may in general be exercised from time to time), the refusal of the court or the comptroller to make any such order on an application made by an employee under section 40 above shall not prevent a further application being made under that section by him or any successor in title of his.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Further applications under s.40 {#further-applications-under-s40}

### 41.07 {#ref41-07}

The refusal of the court or the comptroller to order the payment of
compensation under s.40 does not prevent the employee or any successor
in title from making a further application under s.40. The reference to
the Interpretation Act 1978 in s.41(7) replaced the previous reference
to the Interpretation Act 1889, this amendment having been effected by
s.25(2) of the 1978 Act.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 41(8)**
  Where the court or the comptroller has made any such order, the court or he may on the application of either the employer or the employee vary or discharge it or suspend any provision of the order and revive any provision so suspended, and section 40(5) above shall apply to the application as it applies to an application under that section.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Variation of order for payment of compensation

### 41.08 {#ref41-08}

Where the court or the comptroller has made an order for the payment of
compensation under s.40, the employer or the employee may under s.41(8)
apply for any provision of the order to be varied, discharged, suspended
or revived. The application may be made to the court or to the
comptroller.

### 41.09 {#section-4}

PR part 7, r.51(3)(a) is also relevant.

Such an application to the comptroller under s.41(8) should be made on
Patents Form 2 accompanied by a copy thereof and a statement of grounds
in duplicate. This starts proceedings before the comptroller, the
procedure for which is discussed [at 123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05)
Documents filed at the Office in connection with the application are not
open to public inspection unless the comptroller otherwise directs.

### 41.10 {#section-5}

The comptroller may, under s.40(5), decline to deal with the application
made under s.41(8) ([see
40.17-18](/guidance/manual-of-patent-practice-mopp/section-40-compensation-of-employees-for-certain-inventions/#ref40-17)).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 41(9)**
  In England and Wales any sums awarded by the comptroller under section 40 above shall, if the county court so orders, be recoverable by execution issued from the county court or otherwise as if they were payable under an order of that court.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 41(10)**
  In Scotland an order made under section 40 above by the comptroller for the payment of any sums may be enforced in like manner as an extract registered decree arbitral bearing a warrant for execution issued by the sheriff court of any sheriffdom in Scotland.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 41(11)**
  In Northern Ireland an order made under section 40 above by the comptroller for the payment of any sums may be enforced as if it were a money judgment.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 41(12)**
  In the Isle of Man an order made under section 40 above by the comptroller for the payment of any sums may be enforced as if it were a judgement or order of the court for the payment of money.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Enforcement of order made by the comptroller

### 41.11 {#ref41-11}

Orders made by the comptroller for the payment of compensation under
s.40 may be enforced in different parts of the UK as set out in s.41(9)
to (12). Subsection (12) was added by S.I. 1978 No. 621, which has since
been replaced by S.I. 2013 No. 2602.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
