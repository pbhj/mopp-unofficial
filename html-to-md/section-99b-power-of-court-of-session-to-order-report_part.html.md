::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 99B: Power of Court of Session to order report {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (99B.01 - 99B.03) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 99B.01 {#b01}

The CDP Act gave the court in England and Wales (see 99A.01-02) and in
Scotland the power to order reports by the Office in proceedings under
the 1977 Act.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 99B(1)**
  In any proceedings before the Court of Session under this Act the court may, either of its own volition or on the application of any party, order the Patent Office to inquire into and report on any question of fact or opinion.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 99B.02 {#b02}

The Court of Session may order the Office to inquire into and report on
any question of fact or opinion, either of its own motion or on the
application of any party to the relevant proceedings.

\[ For report procedure, [see
99A.01](/guidance/manual-of-patent-practice-mopp/section-99a-power-of-patents-court-to-order-report/#ref99A-01).
\]

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 99B(2)**
  Where the court makes an order under subsection (1) above of its own volition the fee payable to the Patent Office shall be at such rate as may be determined by the Lord President of the Court of Session with the consent of the Treasury and shall be defrayed out of moneys provided by Parliament.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 99B(3)**
  Where the court makes an order under subsection (1) above on the application of a party, the fee payable to the Patent Office shall be at such rate as may be provided for in rules of court and shall be treated as expenses in the cause.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 99B.03 {#b03}

Subsections (2) and (3) provide for the payment of fees to the Office
where the Court of Session makes such an order. These subsections cover
the rate of payment and who is required to provide the money, depending
on whether or not the order is made on the application of a party.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
