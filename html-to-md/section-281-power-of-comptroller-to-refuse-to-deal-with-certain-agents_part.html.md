::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 281: Power of comptroller to refuse to deal with certain agents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated (281.01 - 281.05) April 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 281.01 {#ref281-01}

This enables the making of rules governing the circumstances in which
the comptroller may refuse to recognise persons acting as agent in
patent or registered design proceedings. It also forbids the recognition
of agents who neither reside nor practice in the UK or any state in the
European Economic Area. The references to "patent agents" have been
amended to "patent attorneys" by section 208(1) and Schedule 21,
paragraph 78 of the Legal Services Act 2007.

  -----------------------------------------------------------------------------------------------------------------------
   
  **Section 281(1)**
  This section applies to business under the Patents Act 1949, the Registered Designs Act 1949 or the Patents Act 1977.
  -----------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 281(2)**

  The Secretary of State may make rules authorising the comptroller to
  refuse to recognise as agent in respect of any business to which this
  section applies -\
  \
  (a) a person who has been convicted of an offence under section 88 of
  the Patents Act 1949, section 114 of the Patents Act 1977 or section
  276 of this Act;\
  \
  (b) a person whose name has been erased from and not restored to, or
  who is suspended from, the register of patent attorneys on the ground
  of misconduct;\
  \
  (c) a person who is found by the Secretary of State to have been guilty
  of such conduct as would, in the case of a person registered in the
  register of patent attorneys, render the person liable to have the
  person's name erased from the register on the ground of misconduct;\
  \
  (d) a partnership or body corporate of which one of the partners or
  directors is a person whom the comptroller could refuse to recognise
  under paragraph (a), (b) or (c) above.
  -----------------------------------------------------------------------

### 281.02 {#section}

The Secretary of State is empowered to make rules authorising the
comptroller to refuse to recognise persons acting as agent in respect of
any of the business listed in subsection (1). Such rules (The Patent
Agents (Non-recognition of Certain Agents by Comptroller) Rules 1990)
came into force on 13 August 1990 and were amended by the Legal Services
Act 2007 (Consequential Amendments) Order 2009 (SI 2009 No.3348). The
types of person whom the rules may specify are listed in subsection
(2)(a) to (d); rule 3(a) to (d) of the above-mentioned rules specifies
those same types of person. (Similar provisions apply to the right of
audience in proceedings before the comptroller, see 102.03.) Paragraphs
(b) and (c) of subsection (2), and subsection (3) below, effectively
place registered and unregistered agents on the same footing as regards
the consequences of "misconduct" in relation to recognition by the
comptroller.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 281(3)**
  The rules may contain such incidental and supplementary provisions as appear to the Secretary of State to be appropriate and may, in particular, prescribe circumstances in which a person is or is not to be taken to have been guilty of misconduct.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 281.03 {#section-1}

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Subsection (3) provides that the rules may contain additional provisions, in particular in relation to what may or may not be regarded as misconduct. Such provisions might facilitate the treatment of unregistered persons in the same way as registered patent attorneys in this respect. In fact, the above-mentioned rules do not contain any such provisions.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 281(4)**
  Rules made under this section shall be made by statutory instrument which shall be subject to annulment in pursuance of a resolution of either House of Parliament.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 281(5)**
  'The comptroller shall refuse to recognise as agent in respect of any business to which this section applies a person who neither resides nor has a place of business in the United Kingdom, the Isle of Man or a member State.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 281.04 {#ref281-04}

Subsection (5) concerns the need for a person acting as agent to be
resident or have a place of business in the UK, the Isle of Man, or the
European Economic Area in order to be recognised. (Section 2 of the
European Economic Area Act 1993 provides that, in an Act which predates
the entry into force of the treaty creating the EEA, such as the CDP
Act, any reference to the European Union (or to related terms like
"member State") is to be construed as a reference to the EEA.)

### 281.05 {#ref281-05}

The address for service requirements (see 14.04.14) allow for an address
for service to be provided in the Channel Islands or Gibraltar, which
are not part of the United Kingdom or the European Economic Area. Agents
in those territories may be asked to demonstrate they meet the
requirements of this section if it is unclear from the documentation on
file.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
