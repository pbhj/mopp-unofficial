::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 104: Privilege for communications with patent agents relating to patent proceedings \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (104.01 - 104.02) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 104.01

This section provided privilege from disclosure to communications with
patent agents akin to that applying to communications with solicitors
(the latter being specifically applied in relation to patent proceedings
in accordance with s.103).

### 104.02

Section 104, together with other provisions of the 1977 Act concerning
patent agents, has been repealed by the CDP Act and replaced by Part V
of the CDP Act (Patent Agents and Trade Mark Agents). In particular,
section 280 of the CDP Act provides privilege for communications with
patent agents, [see
280.01-05](/guidance/manual-of-patent-practice-mopp/section-280-privilege-for-communications-with-patent-agents/#ref280-01).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
