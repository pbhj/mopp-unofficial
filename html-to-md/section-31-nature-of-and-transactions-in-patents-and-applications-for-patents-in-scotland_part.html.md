::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 31: Nature of, and transactions in, patents and applications for patents in Scotland {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (31.01 - 31.05) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 31.01

In Scotland, the nature of patents and applications as property, and
transactions which may be made therein, are laid down by s.31 instead of
by s.30. The provisions of the two sections are similar.

### 31.02

s.77(1), s.78(2) is also relevant.

Section 31 applies in relation to not only 1977 Act patents and
applications but also granted European patents (UK) and applications for
European patents (UK).

  ------------------------------------------------------------------------------------------------------------------------
   
  **Section 31(1)**
  Section 30 above shall not extend to Scotland, but instead the following provisions of this section shall apply there.
  ------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 31(2)**
  Any patent or application for a patent, and any right in or under any patent or any such application, is incorporeal moveable property, and the provisions of the following sub- sections and of section 36(3) below shall apply to any grant of licences, assignations and securities in relation to such property.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 31.03

s.130(1) is also relevant.

A patent, application or right therein or thereunder is thus
"incorporeal moveable property" in Scotland. The term "right" includes
an interest in the patent or application. Any reference to a right in a
patent includes a reference to a share in the patent.

### 31.04

s.36(3) is also relevant.

Licences (including sub-licences), assignations and securities may be
granted subject to s.31(3) to (7) and provided that, where two or more
persons are proprietors of or applicants for a patent, one of them does
not grant a licence or assignation or cause or permit security to be
granted over the patent or application without the consent of the other
or others (subject to ss.8, 12 and 37 (which relate to the determination
of questions about entitlement to applications and patents) and to any
agreement in force).

  -----------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 31(3)**
  Any patent or any such application, or any right in it, may be assigned and security may be granted over a patent or any such application or right.
  -----------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 31(4)**
  A licence may be granted, under any patent or any application for a patent, for working the invention which is the subject of the patent or the application.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 31(5)**
  To the extent that any licence granted under subsection (4) above so provides, a sub-licence may be granted under any such licence and any such licence or sub-licence may be assigned and security may be granted over it.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 31(6)**
  Any assignation or grant of security under this section may be carried out only by writing subscribed in accordance with the Requirements of Writing (Scotland) Act 1995.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 31.04.1

Section 31(6) was amended by the Requirements of Writing (Scotland) Act
1995. Where an assignment is done in Scotland the transaction document
need only be signed by or on behalf of its granter at the end of the
last page, rather than by or on behalf of all parties.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 31(7)**
  An assignation of a patent or application for a patent or a share in it, and an exclusive licence granted under any patent or any such application, may confer on the assignee or licensee the right of the assignor or licensor to bring proceedings by virtue of section 61 or 69 below for a previous infringement or to bring proceedings under section 58 below for a previous act.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 31.05

Section 31(7) is virtually identical to s.30(7), see 30.09.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
