::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 24: Publication and certificate of grant {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (24.01 - 24.05) last updated: April 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 24(1)**
  As soon as practicable after a patent has been granted under this Act the comptroller shall publish in the journal a notice that it has been granted.
  -------------------------------------------------------------------------------------------------------------------------------------------------------

### 24.01 {#ref24-01}

At the same time as the applicant is informed, in a "grant" letter, that
grant has taken place [see
18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent#ref18-86)
they are notified of the date on which it will be advertised in the
Journal. This is generally several weeks after the date of the "grant"
letter.

### 24.02 {#section}

s.77(1) is also relevant.

A European patent (UK) is advertised in the Journal on the date on which
its grant is mentioned in the European Patent Bulletin.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 24(2)**
  The comptroller shall, as soon as practicable after he publishes a notice under subsection (1) above, send the proprietor of the patent a certificate in the prescribed form that the patent has been granted to the proprietor.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 24.03 {#section-1}

r.34 is also relevant.

The certificate includes the name of the proprietor of the patent, the
date of filing the application and is dated with the date that the
patent took full effect i.e. the date that the entry for grant shows in
the journal and the B document is published. A notice on the back of the
certificate reminds the proprietor of their responsibility for setting
up effective arrangements for paying renewal fees [see
25.06-25.08](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-06).

\[The Office may provide replacement certificates of grant either to
correct an error on an original certificate or to replace one that has
been lost. Return of the original certificate is not required.
Replacement of old-style certificates will be with certificates in the
current style.\]

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 24(3)**
  The comptroller shall, at the same time as he publishes a notice under subsection (1) above in relation to a patent publish the specification of the patent, the names of the proprietor and (if different) the inventor and any other matters constituting or relating to the patent which in the comptroller's opinion it is desirable to publish.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 24(4)**
  Subsection (3) above shall not require the comptroller to identify as inventor a person who has waived his right to be mentioned as inventor in any patent granted for the invention.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 24.04 {#ref24-04}

The specification is reproduced directly from the typescript of the
specification as in order. It is published accompanied by a front page
bearing the same publication number as the published application (A
document) but followed by B, and this document is often referred to as
the B document. The front page also carries the same bibliographic data
as the A document, a list of all documents cited as relevant to the
issues of lack of novelty or of inventive step, either in the search
report or in subsequent proceedings, and revised classification data. It
includes the date on which the patent was published, which is the
effective date of grant ([see
25.02-03](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-02)).
It does not include an abstract. [See
13.03](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-03)
for suppression of inventor details. Computer programs, sequences and
other ancillary material omitted from the A publication ([see
16.27](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-27))
should also be omitted from the B publication.

### 24.04.1 {#section-2}

If the B document contains a printer's error, whether in the
specification, the list of citations, or in the bibliographic or
classification data, it may be corrected by the issue of an erratum. In
this context, "printer's error" is interpreted broadly to embrace any
error originating within the Office or during the publication process.
However, it does not extend to errors made elsewhere, such as by the
applicant. An erratum should not be issued to correct an error in the
specification which is detected by the Office unless the error is
significant, in the sense that it misleads or introduces doubt. An
erratum should be issued for an error in the specification which is
notified by the applicant or by a member of the public or for any error
in the bibliographic or classification data. [See
16.33](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-33)
for procedure.

### 24.05 {#section-3}

a.98 EPC is also relevant.

European patents (UK) are published by the EPO on the date on which the
mention of grant is published in the Bulletin.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
