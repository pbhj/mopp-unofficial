::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 70B: Permitted communications {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (70B .01-70B.0.06) last updated: September 2017.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 70B.01 {#b01}

This section defines "permitted communications". Threats contained in a
permitted communication are not actionable, as defined by section
70A(5). Permitted communications allow parties to exchange information
without triggering a threats action in situations where a person
aggrieved may otherwise be entitled to bring such an action.

### Section 70B(1)

For the purposes of section 70A(5), a communication containing a threat
of infringement proceedings is a "permitted communication" if--- (a) the
communication, so far as it contains information that relates to the
threat, is made for a permitted purpose; (b) all of the information that
relates to the threat is information that--- (i) is necessary for that
purpose (see subsection (5)(a) to (c) for some examples of necessary
information), and (ii) the person making the communication reasonably
believes is true.

### 70B.02 {#ref70B-02}

A communication which contains an implied threat of infringement
proceedings is a permitted communication if it satisfies certain
conditions. (A communication containing an express threat of
infringement proceedings cannot be a permitted communication -- ([see
70A.08](/guidance/manual-of-patent-practice-mopp/section-70a-actionable-threats/#ref70A-08))
To be a "permitted communication", the information relating to the
threat must be made for a "permitted purpose" ([see 70B.04 -
70B.05](#ref70B-04)). Furthermore, the information that relates to the
threat must be necessary for the permitted purpose and reasonably
believed to be true by the person making the communication.

### 70B.03 {#b03}

The conditions of section 70B(1) for a communication containing a threat
of infringement proceedings to be a "permitted communication" only apply
to the part of the communication that constitutes a threat within the
section 70 test ([see 70.03
-70.04](/guidance/manual-of-patent-practice-mopp/section-70-remedy-for-groundless-threats-of-infringement-proceedings/#ref70-03)).
Other material in the communication is separately subject to the test
for a threat under section 70. If that material does not satisfy the
test for a threat, then the "permitted communication" provisions are not
engaged in respect of that material.

### Section 70B(2)

Each of the following is a "permitted purpose"--- (a) giving notice that
a patent exists;\
(b) discovering whether, or by whom, a patent has been infringed by an
act mentioned in section 70A(2)(a) or (b);\
(c) giving notice that a person has a right in or under a patent, where
another person's awareness of the right is relevant to any proceedings
that may be brought in respect of the patent.

### Section 70B(3)

The court may, having regard to the nature of the purposes listed in
subsection (2)(a) to (c), treat any other purpose as a "permitted
purpose" if it considers that it is in the interests of justice to do
so.

### Section 70B(4)

But the following may not be treated as a "permitted purpose"-\
(a) requesting a person to cease doing, for commercial purposes,
anything in relation to a product or process,\
(b) requesting a person to deliver up or destroy a product, or\
(c) requesting a person to give an undertaking relating to a product or
process.

### 70B.04 {#ref70B-04}

Examples of permitted purposes are listed by section 70B(2). These,
among other things, allow a patentee to use the permitted communications
to contact a secondary infringer (e.g. a retailer) in order to identify
a manufacturer or importer of an allegedly infringing product.

### 70B.05 {#b05}

The list of permitted purposes is not exhaustive and gives examples of
purposes which will be considered to be permitted purposes. While having
regard to the nature of that list, the court has discretion to treat any
other purpose as permitted, if it is in the interests of justice to do
so. However, section 70B(4) makes clear that certain purposes can never
be a "permitted purpose". These are: requesting that a person stops
doing something in relation to a product or process, or requesting that
a person delivers up or destroys a product, or seeking an undertaking
from a person in relation to a product or process.

### Section 70B(5)

If any of the following information is included in a communication made
for a permitted purpose, it is information that is "necessary for that
purpose" (see subsection (1)(b)(i))\
(a) a statement that a patent exists and is in force or that an
application for a patent has been made;\
\
(b) details of the patent, or of a right in or under the patent,
which---\
(i) are accurate in all material respects, and\
(ii) are not misleading in any material respect; and\
\
(c) information enabling the identification of the products or processes
in respect of which it is alleged that acts infringing the patent have
been carried out.

### 70B.06 {#b06}

Section 70B(5) sets out examples of information that will fulfil the
condition of being necessary for a permitted purpose. These include: a
statement that a patent exists, accurate and not misleading details of
the patent and enough information to identify the product or process
that is allegedly being infringed. The inclusion of this information in
a permitted communication satisfying the other conditions of this
section will not give rise to an actionable threat.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
