::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 279: Power to prescribe conditions etc for mixed partnerships and bodies corporate \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (279.01 - 279.02) last updated: April 2011
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 279.01

This section empowered the Secretary of State to make rules governing
the composition and business practices of partnerships and bodies
corporate doing business as patent agents or European patent attorneys,
where not all the partners or directors are qualified persons ("mixed"
firms). Prior to the coming into force of this section, such firms were
not permitted with the exception of the special case of pre-1917
companies (section 114(2)(a) of the Patents Act 1977, re-enacted as
section 276(4)). Such rules were only made in respect of s.276 ([see
276.03.1](https://draft-origin.publishing.service.gov.uk/guidance/manual-of-patent-practice-mopp/section-276-persons-entitled-to-describe-themselves-as-patent-agents/#ref276-03-1){rel="external"}).

### 279.02

This section was repealed on 1 January 2010 by the Legal Services Act
2007, sections 185(5) and 210 and schedule 23. The rules made under the
section (The Patent Agents (Mixed Partnerships and Bodies Corporate)
Rules 1994 SI 1994/362) therefore also ceased to have effect on that
date.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
