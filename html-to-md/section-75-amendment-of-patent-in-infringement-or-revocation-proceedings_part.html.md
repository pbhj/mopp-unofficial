::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 75: Amendment of patent in infringement or revocation proceedings {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (75.01 - 75.21.2) last updated: April 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 75.01 {#ref75-01}

This section is concerned with the amendment of the specification of a
granted patent, including a European patent (UK), during proceedings in
which the validity of the patent may be put in issue. Such proceedings
are defined in s.74(1), and the definition is somewhat broader than the
title of s.75 might appear to imply. It does not include revocation
proceedings initiated by the comptroller under s.73. The Patents Act
2004 amended s.75(1) to remove the necessity for validity to have been
put in issue before a proprietor can apply to amend under s.75. For
proceedings brought on or after 1 January 2005, a patent proprietor can
propose amendments during the course of any proceedings in which it is
possible for the validity of the patent to be put in issue, whether or
not validity has actually been raised.

### 75.01.1 {#section}

In Chiron Corp v Organon Teknika Ltd \[1994\] FSR 258 Aldous J held that
s.74 does not bar an opponent from raising lack of support under
s.14(5)(c) in amendment proceedings. He was also upheld on appeal
(\[1995\] FSR 559) in finding that a proposal to delete claims, which
had been found to be invalid, should not open the door to an allegation
that other valid claims lacked support, when the proposed amendment did
not impinge on the invention claimed in the valid claims. In particular,
Aldous J concluded that there was no justification for requiring
amendment of the valid claims to disclaim the matter of the claims to be
deleted.

### 75.02 {#section-1}

There are two distinct procedures, depending on whether the proceedings
are taking place before the comptroller or the court. In the former case
the procedure is governed by Part 7 of the Patents Rules 2007, while in
the latter case it is subject to rule 63.10 of Part 63 of the Civil
Procedure Rules 1998 (CPR 63) and section 12 of the Practice Direction
supplementing CPR 63 (CPR 63PD). Further information concerning
procedures before the court is provided in The Patents Court Guide
available through the website of the [Court
Service](http://www.justice.gov.uk/){rel="external"}. It should also be
noted that slightly different procedures again apply where the
proceedings are before the Court of Session. These are not detailed
below, but are governed by paragraph 55.5 of the Rules of the Court of
Session.

### 75.02.1 {#ref75-02-1}

s.74(8), CPR and 63.10(3) are also relevant.\
\

Rule 35(2) requires a proposed amendment to a specification of a patent
to be delivered in electronic form or using electronic communications if
reasonably possible. Similarly, CPR 63 requires the application notice
for permission to amend under s.75 before the court to be served on the
comptroller electronically if reasonably possible.
[Directions](https://www.gov.uk/government/publications/submitting-post-grant-amendments-by-e-mail/directions-on-submitting-post-grant-amendments-by-e-mail)
prescribing the form and manner for electronic delivery of applications
to amend under s.75 were published in the PDJ No. 5938 on 12 March 2003,
and came into force with the Patents Act 1977 (Electronic Communications
Order) 2003 (S.I. 2003 No. 512) on 1 April 2003. Electronic delivery
should be made either by email to <litigationamend@ipo.gov.uk> entitled
"Proposal to amend under s.75 before the comptroller \[or the courts\]"
as appropriate, or on an electronic carrier delivered to the Office
accompanied by an identifying letter [see
27.03.1](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-03-1)
and the Directions for further information on the formats for electronic
submission).

### 75.03 {#section-2}

\[deleted\]

### Section 75(1)

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 75(1)**
  In any proceedings before the court or the comptroller in which the validity of a patent may be put in issue the court or, as the case may be, the comptroller may, subject to section 76 below, allow the proprietor of the patent to amend the specification of the patent in such manner, and subject to such terms as to advertising the proposed amendment and as to costs, expenses or otherwise, as the court or comptroller thinks fit.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Exercising discretion

### 75.04 {#ref75-04}

The allowance of amendments is a matter of discretion and similar
considerations apply as under s.27 see
[27.07-27.11.1](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-03),
[27.11](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-11)
and
[27.32](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-32).
However, section 75(5) was added on 13 December 2007 and provides that
in considering whether to allow a proposed amendment the court or
comptroller shall have regard to relevant principles under the European
Patent Convention (EPC) [(see 75.21 and 75.21.2)](#ref75-21). Various
judgments provide discussion of the exercise of discretion prior to the
introduction of s.75(5). The extent to which these cases are now
relevant is set out below.

In Smith Kline & French Laboratories Ltd v Evans Medical Ltd \[1989\]
FSR 561, Aldous J identified the following principles concerning
discretion as to whether or not to allow amendment

\(i\) the onus to establish that amendment should be allowed is upon the
patentee and full disclosure must be made of all relevant matters;

\(ii\) amendment will be allowed provided the amendments are permitted
under the Act and no circumstances arise which would lead the court to
refuse the amendment;

\(iii\) it is in the public interest that amendment is sought promptly,
so a patentee who delays for an unreasonable period before seeking
amendment must show reasonable grounds for their delay; This principle
is no longer relevant in light of s.75(5).

\(iv\) a patentee who seeks to obtain an unfair advantage from a patent,
which they knew or should have known should be amended, will not be
allowed to amend;

\(v\) the court is concerned with the conduct of the patentee and not
with the merit of the invention. This principle is no longer relevant in
light of s.75(5).

The exercise of discretion was also discussed by the Court of Appeal in
Kimberly-Clark Worldwide Inc v Procter & Gamble Ltd (\[2000\] RPC 422).
However, the intended effect of s.75(5) is that the old law relating to
covetousness and delay is swept away and these matters are no longer an
issue in post-grant amendment (see 75.21 and 75.21.1). This effect was
explicitly confirmed by Floyd J in Zipher Ltd v Markem Systems Ltd & Anr
\[2008\] EWCH 1379 (Pat):

'It follows that if I am to have regard to the principles applicable
under the EPC, the discretion which I have to refuse amendments which
comply with the Act has been limited. Considerations such as those
formerly considered relevant to the discretion, such as the conduct of
the patentee, are no longer relevant.' r.35(6)

### 75.05 {#ref75-05}

r.38(6) is also relevant.

It is considered that the above factors apply not only to the allowance
of amendments but also to whether to give an opportunity to amend. In
ICI Plc v Ram Bathrooms Plc and Rohm GmbH (\[1994\] FSR 181) Aldous J
held that, in actions where the parties have in effect settled their
dispute, he would only allow patents to be amended where (a) the
amendments are not substantial in amount or effect; (b) there is no
apparent matter of controversy; and (c) no matter of public interest
arises. It follows that if any of these matters exist the patentee
should apply to the comptroller for amendment under s.27. Jacob J
allowed amendment in Mabuchi Motor KK's Patents \[1996\] RPC 387 despite
a patent in question not having been framed with reasonable skill and
knowledge and despite the proprietors being dilatory in pursuing the
matter of amendment after having started proceedings, because no actual
or likely prejudice to the public had been shown and interested parties
could have learnt of the existence of the amendment proceedings from the
register. It may also be made a condition for allowing the amendments
that a new specification or replacement pages be provided, in the same
way as described in
[27.18](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-18).
However, when comparing ss.27 and 75, the hearing officer in Osterman's
Patent \[1985\] RPC 579 held that where the application to amend is
under s.75 there is a difference; the Act and the Rules make no specific
provision for the giving of reasons, because there is a presumption (in
proceedings under s.72) that the amendments are offered with a view to
meeting the grounds for revocation. (Note that the proprietor may amend
their patent centrally before the EPO to meet the grounds for revocation
see
[72.44.1](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-44-1).
He declined to direct that the patentees should give evidence directed
towards the proposed amendments, and in particular the reasons for them,
prior to the applicants for revocation giving their evidence in chief on
those amendments and on the revocation issue. The Patents Court held
that the hearing officer had not wrongly exercised his discretion.

### 75.05.1 {#ref75-05-1}

In [Koninklijke Philips Electronics n.v. v Nintendo of Europe GmbH
\[2014\] EWHC 1959
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2014/1959.htm){rel="external"}
the Patents Court found that a double patenting objection taken as a
ground for refusing a post-grant amendment to a claim can be taken but
should only be made in the following circumstances:\
\
i) The two patents must have the same priority dates and be held by the
same applicant (or its successor in title);\
\
ii) The two claims must be for the same invention, that is to say they
must be for the same subject matter and have the same scope. The scope
is considered as a matter of substance. Trivial differences in wording
will not avoid the objection but if one claim covers embodiments which
the other claim does not, then the objection does not arise.\
\
iii) The two claims must be independent claims. If two independent
claims have different scope then there is no reason to object even if
the patents contain dependent claims with the same scope.\
\
If the objection arises, and both patents are before the comptroller or
court, then it can be cured by an amendment or amendments to either
patent. Even if the objection properly arises in the sense that two
relevant claims have the same scope, if the patentee has a legitimate
interest in maintaining both claims then the amendment should not be
refused (see paragraphs 296-309 of the judgment).

### Amendment during proceedings before the comptroller

### 75.06 {#section-3}

Much of what follows in paragraph 75.07 relates specifically to
amendment during revocation proceedings under s.72, but is applicable
mutatis mutandis to other proceedings in which validity is put in issue.
The outcome of any offer to amend the patent is recorded in the
Register.

### 75.07 {#ref75-07}

r.75 is also relevant.

When amendments have been proposed during a hearing [see
72.42](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-42)
or following an interim decision [see
72.44](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-44)
the Comptroller has discretion as to whether or not to advertise them in
the Journal as open to opposition. In exercising this discretion, the
comptroller will consider how significant the amendments are and the
likelihood of another party wishing to object. Where the amendments are
being proposed to avoid revocation it is likely they will be significant
amendments to which another party may wish to object. If however the
amendments are clearly not allowable, or would not cure invalidity, or
are clearly minor, then the comptroller may consider that advertisement
is not necessary. Where an amendment is advertised in the Journal,copies
of the amendment are available through Tribunal Section, and, if the
amendment has been filed electronically, through the website. The
amendments will be available in the form subsisting after the proprietor
has considered any objection to them by the Office or by the other
party. The hearing officer will normally defer issuing a final decision
terminating the proceedings until the period allowed for opposing the
amendments [see 75.15](#ref75-15) has expired or until any opposition
has been resolved.

\[ If it is decided that the amendments should be advertised as open to
opposition, Tribunal Section is responsible for seeing that this is
done. \]

### 75.08 {#section-4}

\[deleted\]

### Amendment during proceeding before the court

### 75.09 {#section-5}

CPR 6.10(1) is also relevant.

An application to the court under section 75 of the 1977 Act for
permission 63.10(1) to amend the specification of a patent by the
proprietor of the patent must be made by application notice.

### 75.09.1 {#section-6}

CPR63.10(2) is also relevant.

The application notice must

\(a\) give particulars of -

\(i\) the proposed amendment sought; and

\(ii\) the grounds upon which the amendment is sought;

\(b\) state whether the applicant will contend that the claims prior to
amendment are valid;

\(c\) be served by the applicant on all parties and the comptroller
within 7 days of its issue.

General rules about applications for court orders are governed by Part
23 of the Civil Procedure Rules.

### 75.09.2 {#section-7}

CPR 63.10(3), CPD 63PD para 12.2 is also relevant.

The application notice must, if it is reasonably possible, be served on
the comptroller electronically. When served electronically, the
application notice must comply with any requirements for the sending of
electronic communications to the comptroller [see Directions under
section
124A](https://www.gov.uk/government/publications/directions-under-section-124a-of-the-patents-act-1977/directions-under-section-124a-of-the-patents-act-1977),
[Submitting post-grant amendments by
e-mail](https://www.gov.uk/government/publications/submitting-post-grant-amendments-by-e-mail/directions-on-submitting-post-grant-amendments-by-e-mail),
and also
[27.03.1](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-03-1)
and [75.02.1](#ref75-02-1).

### 75.10 {#section-8}

CPR 63.10(4) is also relevant

Unless the Court otherwise orders, the comptroller will forthwith
advertise the application to amend in the Journal.

### 75.10.1 {#section-9}

CPR 63.10(5) is also relevant.

The advertisement will contain an electronic copy of the amendments or
will state that any person may apply to the comptroller for a copy of
the application notice.

### 75.11 {#ref75-11}

CPR 63.10(6) is also relevant.

Within 14 days of the first appearance of the advertisement any person
who wishes to oppose the application must file and serve on all parties
and the comptroller a notice opposing the application which must include
the grounds relied on.

\[ Any notice opposing the application should be forwarded to the Group
Head responsible for preparing the comptroller's comments on the
proposed amendments (see 75.12).\]

### 75.11.1 {#section-10}

CPR 63.10(7) is also relevant.

Within 28 days of the first appearance of the advertisement the
applicant must apply to the Court for directions.

### 75.12 {#ref75-12}

A report should be prepared in the Office for the benefit of the Court
on the question as to whether the proposals are considered by the Office
to be prima facie allowable. In general, the Office need only consider
whether the amendments meet the requirements of ss.14(5) and 76: there
is no point in considering whether they meet any objection that has been
raised as to, e.g. lack of novelty, obviousness or insufficiency,
because these are likely to be considered by the court with the benefit
of material to which the comptroller does not have access. In order to
avoid any misunderstanding that the Office has considered all aspects
relevant to allowance of the amendment, the report should make clear
that the Office's consideration has only extended to ss.14(5) and 76. In
addition, if the amendments appear prima facie allowable the report
should say that the comptroller considers that the amendments meet the
requirements of these sections, but if they are not prima facie
allowable the reason for this should be explained in the report. All
parties to the proceedings before the court should be informed of the
Office views on the amendments and whether the comptroller has decided
to be represented in Court. The Office does not normally communicate the
report direct to the Court but instead asks the applicant to pass it on
[see 75.20](#ref75-20). In paragraph 128 of L'Oréal Ltd v RN Ventures
Ltd \[2018\] EWHC 173 (Pat), Carr J discusses the report prepared in
this case and agrees with it. Further judgments which discuss the report
prepared by the Office include Curt G Joan Inc v Fameccanica Data SpA
\[2017\] EWHC 1251 (IPEC) and Cantel Medical et al v Arc Medical
\[2018\] EWHC 345 (Pat).

\[ The application to amend should be referred for the report as soon as
advertisement has been arranged, but the report generally should not be
finalised or issued until the 14 day opposition period has passed (see
75.11), which will allow any notice opposing the application to be taken
into account. The Group Head is responsible for preparing the report or
having it prepared. The report may take the form of a letter to the
applicant informing them of the comptroller's views. It is for the Court
to decide ultimately on the allowability of the amendments and the
Office merely gives its views without becoming involved in the
discussion.\]

### 75.12.1 {#ref75-12-1}

CPR 63.10(3), CPR 63PD para 12.2 are also relevant.

Not less than 2 days before the first hearing date, the applicant, the
comptroller if they wish to be heard, the parties to the proceedings and
any other opponent, must file and serve a document stating the
directions sought.

### 75.13 {#ref75-13}

CPR 63.10(8) is also relevant.

Unless the Court otherwise orders, the applicant must within 7 days
serve on the comptroller any order of the Court on the application.

\[ Where the Court allows a specification to be amended, the original
copy of the specification should be amended by Tribunal Section who will
also prepare the appropriate certificate for the signature of the Group
Head. The Group Head is thus responsible for approving the wording of
the certificate and certifying that the amendments comply with the Court
order. It should be noted that the Deputy Director or Group Head
concerned is not acting for the comptroller in this particular situation
since the allowability of the amendments has been decided by the Court.
\]

\[ A decision by the Court or comptroller to allow an amendment should
be actioned immediately and not deferred if an appeal is entered, unless
the decision has been stayed pending appeal. Accordingly appropriate
Register/Journal entries should be made and the Office copy, ie the
authentic version of the patent specification, should be amended. Notice
of any appeal should be entered into the Register/Journal to advise the
public of the potential transient nature of the decision. It is also
desirable that the public should have a published version of the amended
patent as soon as reasonably possible. Thus, unless the Office is aware
that an early date for an appeal hearing has been fixed, a 'C'
publication should be produced with a footnote that the decision to
allow the amendment is under appeal as of a certain date. A 'C'
publication of the amended patent without the footnote should not be
produced once the final appeal has been disposed of unless the final
appeal further amends the patent. The procedure for production of the
'C' publication is as described in
[27.20](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-20).
If a decision by the Court or comptroller has been stayed pending an
appeal and an appeal has been lodged, the Office will note the Register
and Journal that the decision is stayed pending appeal and that an
appeal has been lodged on a certain date. \]

### Section 75(2)

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 75(2)**
  A person may give notice to the court or the comptroller of his opposition to an amendment proposed by the proprietor of the patent under this section, and if he does so the court or the comptroller shall notify the proprietor and consider the opposition in deciding whether the amendment or any amendment should be allowed.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 75.14 {#ref75-14}

The considerations referred to in [27.24 and
27.28](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-24)
also apply to an opposition under s.75.

### Opposition to amendment before the comptoller

### 75.15 {#ref75-15}

PR part 7, r.75, r.76(2)(a) and r.108(1) are also relevant.

Where in any proceedings before them, the comptroller requires an
amendment proposed under s.75 to be advertised, notice of the amendment
is advertised in the Journal, stating that details of the amendment are
available through Tribunal Section, and if the amendment has been filed
electronically, through the website. Notice of opposition should be
given on Form 15 which should be filed within two weeks of the date of
the advertisement of the amendments sought; this period may not be
extended. The form should be accompanied by a copy thereof and a
statement of grounds (in duplicate). This starts opposition proceedings
before the comptroller, the procedure for which is discussed at [123.05
--
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).
However, if an applicant for revocation has opposed an amendment offered
by the proprietor during revocation proceedings, the applicant for
revocation does not subsequently need also to give notice of opposition
to the amendment on Form 15 [Eickhoff Maschinenfabrik's Patent, BL
O/031/87](https://www.gov.uk/government/publications/patent-decision-o03187);
[Harding's Patent, BL
O/094/90](https://www.gov.uk/government/publications/patent-decision-o09490).

### 75.15.1 {#ref75-15-1}

In [Ability International Ltd v Monkey Tower Ltd (BL
O/484/14)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/484/14){rel="external"},
the hearing officer decided that any opposition to amendment under s.75
must be limited to questioning whether the amendment overcomes the
stated defects (that is, the defects which led to the amendment
request), and whether the amendment meets the requirements of s.76. A
similar limitation applies in opposition proceedings to amendments made
under s.27 [see
27.28](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-28).

### 75.16 {#section-11}

r.77(1) and (r.77(2) are also relevant.

The Office will send a copy of the notice of opposition and the
statement to r.77(2) the proprietor and to any other party to the
proceedings before the comptroller.

### 75.17 {#section-12}

The comptroller may stay the other proceedings and decide the
opposition,if necessary at a hearing, or may decide both actions
together.

### Opposition to amendment before the court

### 75.18 {#ref75-18}

[See 75.11](#ref75-11).

### Section 75(3)

  --------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 75(3)**
  An amendment of a specification of a patent under this section shall have effect and be deemed always to have had effect from the grant of the patent.
  --------------------------------------------------------------------------------------------------------------------------------------------------------

### 75.19 {#section-13}

[See
27.22](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-22).

### Section 75(4)

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 75(4))**
  Where an application for an order under this section is made to the court, the applicant shall notify the comptroller, who shall be entitled to appear and be heard and shall appear if so directed by the court.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 75.20 {#ref75-20}

[See also 75.12](#ref75-12). If, as is normally the case, the
comptroller is not to be represented, the applicant should be asked to
inform the Court of the comptroller's views. The Court may, however,
decide that the comptroller should be represented.

Section 75(5) In considering whether or not to allow an amendment
proposed under this section, the court or the comptroller shall have
regard to any relevant principles applicable under the European Patent
Convention.

### 75.21 {#ref75-21}

Section 75(5) confers on the comptroller or court discretion to allow or
refuse an amendment to the patent. Articles 105a(1) and 123 EPC also
confer on the EPO discretion to allow or refuse an amendment of the
European patent. The comptroller or court continue to have discretion to
allow or refuse an amendment, but in exercising that discretion, section
75(5) requires the comptroller or court to have regard to any relevant
principles which are applicable to amendment or limitation proceedings
under the EPC. These may include relevant regulations made under the
EPC, any relevant guidelines produced by the EPO, and decisions of the
Opposition Division and Boards of Appeal. This should ensure that, as
far as possible, there is consistency in approach as regards postgrant
amendment in national proceedings and before the EPO. The EPO do not
consider the behaviour of the patent proprietor when exercising
discretion in allowing amendments. The intended effect of section 75(5)
is that the behaviour of the patent proprietor will no longer be an
issue to be considered in the UK when deciding whether to allow an
amendment to be made under section 75. This effect was confirmed by
Floyd J in Zipher v Markem [see 75.04](#ref75-04) and below.

### 75.21.1 {#ref75-21-1}

In Zipher Ltd v Markem Systems Ltd & Anr \[2008\] EWCH 1379 (Pat), Floyd
j (as he then was) summarised the position under the EPC as follows: i)
in opposition proceedings, appropriateness of the amendments to the
proceedings, their necessity and procedural fairness are the main,
perhaps only, factors considered relevant to the discretion to allow
amendment; ii) in central amendment proceedings, compliance with the
procedural requirements gives rise to a right to have the patent limited
in accordance with the request.

These are therefore the factors which should be taken into account when
considering whether or not to allow an amendment under this section.

### 75.21.2 {#ref75-21-21}

The Supreme Court in [Warner-Lambert Company LLC v Generics (UK) Ltd
(t.a. Mylan) & Anor. \[2018\] UKSC
56](http://www.bailii.org/uk/cases/UKSC/2018/56.html){rel="external"}
rejected the argument that s.75(5) and Art.138 EPC should give the
proprietor the right to amend a patent to exclude the invalid part of a
claim without any discretion on the part of the Courts. Instead, the
Supreme Court followed the Court of Appeal in [Nikken Kosakusho Works &
Anor. v Pioneer Trading Co & Anor. \[2005\] EWCA Civ
906](http://www.bailii.org/ew/cases/EWCA/Civ/2005/906.html){rel="external"}
\[2006\] FSR 4 in distinguishing between (a) pre-trial amendments, (b)
post-trial amendments to delete claims found to be invalid, and (c) post
trial amendments to set up a new claim which had not been considered at
trial. Type (c) amendments are likely to be refused if they would need a
new trial concerning their validity, on grounds of procedural fairness.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
