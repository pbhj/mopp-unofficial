::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 20: Failure of application {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (20.01 - 20.10) last updated April 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 20.01

Rule 30 is relevant to this section. This rule sets out the compliance
period, ie the period for putting an application in order.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 20(1)**
  If it is not determined that an application for a patent complies before the end of the prescribed period with all the requirements of this Act and the rules, the application shall be treated as having been refused by the comptroller at the end of that period, and section 97 below shall apply accordingly.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 20.02 {#ref20-02}

r.30, r.2(2), r.108(3) r.108(4) is also relevant.

The period prescribed for the purposes of this subsection (and for
s.18(4)) is the compliance period. The compliance period is (i) four
years and six months calculated from the declared priority date or,
where there is none, from the filing date of the application; or (ii)
twelve months calculated from the date that the first substantive
examination report is sent to the applicant, whichever expires the
later. Where the first substantive examination report is not sent to the
applicant before the expiry of the period set out at (i) above, that
period is extended to such date as that report is sent to the applicant
and the period set out at (ii) then applies. The above periods, set out
in r.30, ensure that applicants always have time in which to complete
prosecution of their applications, and ensure that the application does
not lapse if the first s.18 report does not issue until after 4½ years.

However, r.30(3)(a) specifies that where the application claims an
earlier date of filing under s.8(3), 12(6) or 37(4), the period is
either (i) the period prescribed by r.30(2) in relation to the earlier
application, or is (ii) eighteen months from the actual date of filing,
whichever period expires the later.

From 1st May 2023 the way divisional applications are accorded their
compliance date will change ([see also
15.20.1](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-20-1)):

\(i\) For divisional applications filed prior to 1st May 2023, any
extension made under rule 108 to the compliance period prescribed on the
earlier application (the so-called ''parent'' application) will also
apply to any divisional applications filed on or after the date that the
period was extended. The compliance period for any such divisional
applications will therefore be the period as extended. However, if the
compliance period prescribed on the earlier application is later
extended, that extension will not apply to any divisional applications
which have already been filed; a separate request will be required to
extend the compliance period for any such divisional applications.

\(ii\) For divisional applications filed on or after 1st May 2023, any
extensions to the compliance period of the earlier application under
r.108(2)/r.108(3) will no longer be inherited upon filing. From 1st May
2023 all divisional applications will be accorded a compliance date that
is the same as the un-extended compliance date of their parent
application in accordance with r.30(3)(b).

The period for putting any application in order may be extended in two
month tranches in accordance with r.108(2) or (3) and (4) to (7), [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).
The period may also be extended by the provisions of s.20(2) ([see
20.08 - 20.10](#ref20-08)). For the periods in the case of divisional
applications, in the case of new applications filed under s.8(3), 12(6)
or 37(4), or in the case of European patents (UK) converted under s.81
also see
[15.20.1](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-20-1),
[8.25.1](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-25-1),
[12.16.1](/guidance/manual-of-patent-practice-mopp/section-12-determination-of-questions-about-entitlement-to-foreign-and-convention-patents-etc/#ref12-16-1),
[37.17.1](/guidance/manual-of-patent-practice-mopp/section-37-determination-of-right-to-patent-after-grant/#ref37-17-1)
and
[81.19](/guidance/manual-of-patent-practice-mopp/section-81-conversion-of-european-patent-applications/#ref81-19)
respectively.

In Xu's Application ([BL
O/610/22](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/610/22){rel="external"})
the hearing officer (HO) considered what factors should be taken into
account when deciding whether or not to exercise the Comptroller's
discretion to allow a 4th extension to the compliance period under
r.108(3), for the purposes of allowing an applicant the opportunity to
overcome the objections set out in a s.18(3) report ([see also
18.59](guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-59)).
The HO decided that the key issue was to balance the reason why the
discretionary extension was being requested against the need to provide
certainty for third parties. Furthermore, the number of extensions
already allowed and how close an application was to being acceptable,
were relevant factors to consider to avoid the ''spectre of endless
extensions.''

In Munchkin Inc's Application ([BL
O/623/17](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F623%2F17&submit=Go+%BB){rel="external"})
the hearing officer considered, among other things, whether to exercise
the Comptroller's discretion to allow an extension to the compliance
period for the purpose of filing amendments on both a parent and
divisional application. The hearing officer concluded that the workload
faced by the Applicant's legal staff working on an application was not
grounds for a discretionary extension to the compliance period under
r.108(3). A short time scale for responding to objections set out in a
s.18(3) examination report was also not considered to be exceptional.

The hearing officer emphasised in [BL
O/623/17](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F623%2F17&submit=Go+%BB){rel="external"}
that for pre-grant amendments to be considered it is necessary for them
to be filed before the end of the compliance period. Where an extension
to the latest date for reply is requested under s.117B ([see
18.53-18.60](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-53))
such that the extended latest date for reply would exceed the compliance
period, the applicant must first request an extension to the compliance
period and have that request accepted under r.108 so that the latest
date for response would fall within the extended compliance period. That
is to say, the compliance date under s.20 takes precedence over any
request to extend the specified latest date for reply. Where amendments
are filed after the end of the compliance period and an extension to the
compliance period is disallowed, those amendments cannot be considered,
as concluded in Optinose's application ([BL
O/144/12](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F144%2F12&submit=Go+%BB){rel="external"}).

::: call-to-action
The hearing officer in Metabiotech's application ([BL
O/359/23](https://www.ipo.gov.uk/p-challenge-decision-results/o035923.pdf){rel="external"})
concluded that neither geographical distance between the applicant and
agent, nor complexity of subject matter in isolation constituted
sufficient grounds for a discretionary extension of the compliance
period under r.108(3). However, an exam report was issued one day before
the end of the compliance period which introduced a new citation and
objections. Discretion was therefore exercised to extend the compliance
period to give the applicant an opportunity to respond to the updated
objections.
:::

While r.108 requires that any request for an extension to the compliance
period is accompanied by a Form 52, under certain circumstances the
explicit use of the form is not essential. The hearing officer in 4D
Pharma's application ([BL
O/202/21](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F202%2F21&submit=Go+%BB){rel="external"})
concluded that where a request for a discretionary extension was filed
via email and the associated fee had been waived due to Covid-19
provisions, a written request for a discretionary extension was
sufficient to allow an extension, provided that it included all the
necessary information, i.e., a request for a discretionary extension to
the compliance period and an appropriate reason for doing so, on the
basis of r.4(2) and r.4(5). However, as the hearing officer concluded in
Optinose' application ([BL
O/144/12](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F144%2F12&submit=Go+%BB){rel="external"}),
the applicant must provide a clear indication that they are requesting
an extension to the compliance period, and provide evidence to support a
request for a discretionary extension, even if a form 52 is used.

In [Walmart Apollo's Applications BL
O/0704/23](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/0704/23){rel="external"},
the hearing officer decided that a larger than average number of
examination reports and the agreement of allowable claims close to the
compliance date did not amount to exceptional circumstances when
considering whether to allow an extension of the compliance period under
r.108(3) for the purpose of filing a divisional application. The hearing
officer therefore refused to exercise discretion to allow the extensions
of time.

It should be noted that while decisions relating to discretionary
extensions of specified periods under r.109, such as the latest date for
reply to a s.18(3) examination report, are legally distinct from those
under r.108, the reasons for those decisions (detailed in 18.56) may
nonetheless be relevant, as highlighted in [BL
O/610/22](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F610%2F22&submit=Go+%BB){rel="external"}.

\[ ELC4 should be added to the covering letter when the first report
under s.18 is issued later than three years and six months from the
priority or filing date. For the procedure if ELC4 was omitted from a
first examination letter issued later than three years and six months
from the earliest date, [see
18.47](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-47),
3rd indented paragraph. \]

\[ ELC5 should be added to the covering letter of the first s.18 report
on a divisional application when the first report on the parent
application was issued later than three years and six months from the
priority or filing date. It should also be used in the covering letter
for a subsequent s.18(3) report on such a divisional application if it
was not used at the time of the first report.\]

### 20.02.1 {#ref20-02-1}

r.30(4), r.29(3)(b) and r.29(4) is also relevant.

R.30(4) provides for an automatic extension of the prescribed period
when, as a consequence of observations under s.21 are filed near to the
end of the period and give rise to a report under s.18(3) is issued.
Where the date of the letter embodying that report is within three
months of the end of the existing compliance period, the compliance
period becomes three months from the date of the letter. This applies
only to the first s.18(3) report based on those particular observations,
if there is more than one such report. Rule 30(4) refers to this report
as a "first observations report", and this term is defined in r.29(3)(b)
and r.29(4). In Akron Brass Company's application [BL
O/012/19](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F012%2F19&submit=Go+%BB){rel="external"}
the hearing officer found that only observations relating to s.1(1) (for
example novelty or inventive step) can satisfy rule 29(4)(a). Therefore,
observations in respect of other issues (such as added matter and
clarity) cannot be used as a basis for an extension under r.30(4). Where
observations relate to both s.1(1) and other issues, only the parts
relating to s.1(1) may be relied on as the basis for an extension under
r.30(4). That is, the report under s.18(3) must be issued as a
consequence of the parts of the observations relating to s.1(1). The
hearing officer held that the examination report in question was not
issued as a consequence of the observations, since there was no reason
to doubt that a competent examiner would have independently arrived at
the objections raised in the examination report. The hearing officer
therefore found that an extension was not possible under r.30(4).

\[ ELC6 should be used in the examination (covering) letter to inform
the applicant of the extension of the period following s.21
observations. \]

### 20.02.2 {#ref20-02-2}

In Anning's Patent Application \[2007\] EWHC 2770 (Pat), it was held
that the compliance period is a period during which the requirements of
the Act and Rules must be complied with. It does not impose any
additional requirement on the applicant. Therefore it follows that the
compliance date does not roll over to the next working day if it falls
on a non-working day, since it is not of itself a deadline for doing
anything under the Act. [See
120.07](/guidance/manual-of-patent-practice-mopp/section-120-hours-of-business-and-excluded-days/#ref120-07).

### 20.03 {#ref20-03}

Outstanding applications are periodically checked to detect those on
which a reply to a report issued under s.18(3) is overdue where the
compliance period has nearly expired. A letter is sent to the applicant
forewarning of the intention to refuse each such application under
s.20(1) and giving the applicant an opportunity to submit any
observations which might affect refusal. When the compliance period has
expired, the application is forwarded to the relevant formalities group
for refusal to be authorised. If it is found at that time that the
forewarning letter has not been sent, another letter forewarning of the
intention to refuse is sent before refusal is confirmed, allowing one
week for the applicant to submit any observations. When refusal has
occurred, it is advertised in the Journal.

\[ The COPS records are combed weekly and applications that are 4¼ years
from the declared priority date or filing date and have a certain
processing status are identified on the "Section 20 Pick List". The
selection is verified by the formalities groups who should confirm eg
whether a Section 18(3) report has been issued to which no reply has
been received or indeed whether the report issued in the first place. If
it is ascertained that a reply to a Section 18(3) report is overdue a
standard letter (WR1) with a copy on file should be issued, forewarning
the applicant of the intended refusal. If a reply is not overdue at the
time of vetting, the appropriate COPS action should be taken, but if the
reply subsequently becomes overdue the letter (WR1) should then be sent.

\[ After 4⅔ years have elapsed from the declared priority date or filing
date, those applications are selected by COPS on the "Section 20 Time
Limit Report" and the relevant Formalities Manager takes appropriate
action for treatment as refused under s.20. All such applications should
have had the standard letter issued and a copy placed on file. If it is
detected that the letter (WR1) has not been issued, the Formalities
Manager should issue the alternative forewarning letter (WR2) before
sending the file to the relevant Formalities Manager. The Formalities
Manager should then therefore not only verify that the application
should be treated as refused but also check that one of the letters has
been issued (see below if it has not) and that no response which would
prevent treatment as refused has been subsequently received.

\[ The Formalities Manager should add the appropriate label to the PDAX
dossier cover and add a minute to the dossier. Where the application is
a paper case the Formalities manager should sign the square on the flap
of the shell after verifying that the application is being correctly
terminated. (If for example it is found that the application had been
incorrectly stored after amendments had been filed, the case should be
referred to the Divisional Director; it may be possible to exercise
discretion under r.107 to extend the prescribed period.) The Formalities
Manager should then annotate the file accordingly, carry out the
appropriate COPS action (so that the termination is advertised in the
Journal) and forward the file to Nine Mile Point.

\[ If it is discovered that a letter (WR1 or WR2) has not been issued
the Formalities Manager should, after verifying that the application
should be treated as refused, arrange for the alternative letter (WR2)
to issue. After allowing time for any response, the Formalities Manager
should arrange for action as in the last sentence of the preceding
paragraph. \]

### 20.04 {#section-1}

Treatment of the application as having been refused at the end of the
prescribed period is mandatory if it fails to comply with any
requirement of the Act and Rules within that period. It follows
therefore that if a hearing is necessary to resolve a disagreement this
should if possible be held and a decision be given and communicated to
the applicant before the end of the prescribed period. (The reasons for
the decision may be given later if necessary.) It is then possible for
the applicant to amend within the period (including any extension by
virtue of s.20(2) - [see 20.08 - 20.10)](#ref20-08) in order to meet the
terms of an adverse decision. If however the hearing is held after the
end of the period it may only be for the purpose of deciding whether or
not the application was in order at the end of the period. If it is
decided that it was not, then it is not possible to amend to rectify any
faults unless the period is extended under r.108(2) or (3). Under these
circumstances, the comptroller may consider that, for the benefit of the
applicant, a period of less than the minimum fourteen days' notice of a
hearing usually given is appropriate, if this would otherwise lead to
the hearing being held after the end of the prescribed period. The
wishes of the applicant should however be taken into account.

### 20.05 {#section-2}

It is also in an applicant's own interest, when filing amendments near
the end of the prescribed period, to mark the covering letter URGENT,
prominently and boldly and preferably in red.

### 20.06 {#ref20-06}

If a response filed within the prescribed period fails to put an
application in order, but it is not possible to elicit a further
response or to arrange a hearing before the end of two months after the
expiry of the normal compliance period (ie the period prescribed by r.30
as extended by r.108(2)), the applicant should be told that the
application will be treated as having been refused unless within
fourteen days they submit observations and/or request a hearing to
demonstrate that the application was in fact in order (or request an
extension of the compliance period under r.108(3) to (7), [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34)).

\[ EL5 may be used. \]

### 20.07 {#section-3}

Provided that an application was in order at the end of the compliance
period there is no statutory bar to its being amended after the end of
that period; the opportunity to amend an application is only terminated
by the issue of the letter informing the applicant that a patent has
been granted. ([See
19.22](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-22)).

  -----------------------------------------------------------------------
   

  **Section 20(2)**

  If at the end of that period an appeal to the court is pending in
  respect of the application or the time within which such an appeal
  could be brought has not expired, that period\
  ­ (a) where such an appeal is pending, or is brought within the said
  time or before the expiration of any extension of that time granted (in
  the case of a first extension) on an application made within that time
  or (in the case of a subsequent extension) on an application made
  before the expiration of the last previous extension, shall be extended
  until such date as the court may determine;\
  (b) where no such appeal is pending or is so brought, shall continue
  until the end of the same time or, if any extension of that time is so
  granted, until the expiration of the extension or last extension so
  granted.
  -----------------------------------------------------------------------

### 20.08 {#ref20-08}

CPR 63.17, CPR 52.6(1), CPR 52.6(2) is also relevant.

The time allowed for bringing an appeal is governed by [Part 52 of the
Civil Procedure
Rules](http://www.justice.gov.uk/courts/procedure-rules/civil/rules/part52){rel="external"}
and the Practice Directions supporting CPR 52. The comptroller will
normally use the discretionary power provided by CPR 52.4(2)(a) to
direct that notice of appeal must be filed at the court within 28 days
after the date of a decision. However, a different period can be set
where appropriate. Where the appeal notice has not yet been filed, an
application to extend the period for appeal can be made to the
comptroller, per [Aujla v Sanghera \[2004\] EWCA Civ
121](http://www.bailii.org/ew/cases/EWCA/Civ/2004/121.html){rel="external"}.
However, once the notice of appeal has been filed, any request for
extension must be made to the appeal court. The parties cannot
themselves extend the appeal period by agreement.

### 20.09 {#section-4}

When a decision is given before the end of the compliance period ([see
20.02](#ref20-02)) and at the end of that period the time (including any
extension granted by the Court or the comptroller) allowed for bringing
an appeal is still running, the period for putting the application in
order is automatically extended, for all purposes, until the end of that
time. Therefore, if an adverse decision on a substantive matter is given
less than 28 days from the end of the compliance period, and the
comptroller has directed that any appeal must be filed within 28 days
after that decision, the applicant has 28 days (plus any extension
granted by the Court or the comptroller) from the date of the decision
in which to submit amendments with a view to meeting the terms of the
decision. If no appeal is brought within this time, this opportunity to
amend ends automatically at the end of this time. If there is an appeal
the Court may prescribe a period within which amendments may be made.

### 20.10 {#section-5}

Any extension of the prescribed period which is given under r.108
applies only to the period as prescribed by r.30, and is not in addition
to any extension arising by virtue of s.20(2). Thus, for example, a
substantive decision given 14 days before the end of the prescribed four
years six months has the effect of extending the period by 14 days; a
request on Form 52 extends the original period by two months, i.e. the
extensions run concurrently.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
