::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 99A: Power of Patents Court to order report {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (99A.01 - 99A.04) last updated last updated January 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 99A.01 {#ref99A-01}

The CDP Act inserted sections 99A and 99B into the 1977 Act. These
sections give to the Patents Court and the Court of Session,
respectively, the power to order the Office to inquire into and report
on any question of fact or opinion.

\[ A request for a report will come to PDTMD Legal Section who will pass
it to a Deputy Director or Group Head or Legal Adviser (as appropriate)
for action. The report may be subject to close scrutiny in the court
proceedings, so it is particularly important for it to be thorough and
accurate. There is likely to be an early time limit for completion. The
Office will charge for this service so a record of the number of
hours/days taken should be kept. \]

### 99A.02 {#a02}

A similar power is given to the Patents County Court by section 291 of
the CDP Act (see 291.05).

### 99A.03 {#a03}

The Patents Court is empowered to make such an order ([see
99A.01](#ref99A-01)) of its own motion or on the application of any
party to the relevant proceedings. Rules of court are necessary for this
purpose; s.99A(1) requires those rules to make such provision. No such
rules have yet been made.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 99A(1)**
  Rules of court shall make provision empowering the Patents Court in any proceedings before it under this Act, on or without the application of any party, to order the Patent Office to inquire into and report on any question of fact or opinion.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 99A(2)**
  Where the court makes such an order on the application of a party, the fee payable to the Patent Office shall be at such rate as may be determined in accordance with rules of court and shall be costs of the proceedings unless otherwise ordered by the court.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 99A(3)**
  Where the court makes such an order of its own motion, the fee payable to the Patent Office shall be at such rate as may be determined by the Lord Chancellor with the approval of the Treasury and shall be paid out of money provided by Parliament.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 99A.04 {#a04}

Subsections (2) and (3) provide for the payment of fees to the Office
where the Patents Court makes such an order. These subsections cover the
rate of payment and who is required to provide the money, depending on
whether or not the order is made on the application of a party.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
