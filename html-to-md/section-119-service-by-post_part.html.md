::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 119: Service by post {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (119.01 - 119.08) last updated October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 119**
  Any notice required or authorised to be given by this Act or rules, and any application or other document so authorised or required to be made or filed, may be given, made or filed by post.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 119.01 {#ref119-01}

This section allows any notice or document required or authorised by the
1977 Act or rules thereunder to be served by post. It does not preclude
other methods of service, eg by hand or e-filing. The reference to
"post" is construed widely to mean any item of mail delivered by any
service in exchange for payment, and is thus not restricted to delivery
services operated by the Post Office.

### 119.02 {#section}

Service of a document by post is deemed to be effected by properly
addressing, paying for and posting a letter containing the document.

### 119.03 {#ref119-03}

Documents are accorded the date of receipt within the Office. This
contrasts with practice under r.97 of the Patents Rules 1995, where a
document sent by post was deemed to have been received at the Office on
the day when it would have been delivered in the ordinary course of
post. This deeming arrangement applied to any document posted prior to
17 December 2007.

### 119.04 {#section-1}

\[deleted\]

### 119.05 {#section-2}

\[deleted\]

### 119.06 {#section-3}

\[deleted\]

### 119.07 {#section-4}

s.120 is also relevant

Where a communication sent by post is received at the Office on an
excluded day ([see
120.05](/guidance/manual-of-patent-practice-mopp/section-120-hours-of-business-and-excluded-days/#ref120-05))
it is treated as having been received on the next non-excluded day, any
time limit expiring on the former day being extended to the latter.
However, if its receipt on the excluded day was the result of postal
delay, r.110 or r.111 may apply, [see 119.08](#ref119-08).

### 119.08 {#ref119-08}

r.110 and r.111 are also relevant

Under r.110 the comptroller is empowered to certify any day where there
r.111 is a general interruption or subsequent dislocation in the UK
postal services as an interrupted day; any time limits expiring on such
an interrupted day are automatically extended to the next day which is
not an interrupted day, [see 123.43 to
123.45](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-43).
Under r.111 the comptroller may extend any period of time specified in
the Act or Rules, in a particular case of failure to meet that time
period, where the Comptroller is satisfied that the failure was wholly
or mainly attributable to a failure or delay in a communication service,
[see
123.46-47](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-46).

\[Where correspondence from the Office is reported as never having
arrived at its intended destination, or is reported as being misdirected
or delayed, this fact should be recorded by sending a minute to the
relevant formalities group with any relevant details.\]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
