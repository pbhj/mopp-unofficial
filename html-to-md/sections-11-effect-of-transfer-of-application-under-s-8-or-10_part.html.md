::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 11: Effect of transfer of application under s.8 or 10 {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (11.01 - 11.10) last updated: October 2019.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 11.01

Orders or directions regarding the name(s) in which a patent application
should proceed may be given under s.8 (determination before grant of
questions about entitlement to patents under the Act) or s.10 (disputes
between joint applicants). The effects of such orders or directions on
licences or other rights in or under the application are laid down by
s.11. This section also gives certain rights to applicants or licensees
who worked the invention, or prepared to do so, before the registration
of a reference under s.8, see [11.05](#ref11-05) to [11.07](#ref11-07).

### 11.02 {#ref11-02}

s.12(5) is also relevant

Section 11 is also applicable in relation to orders made under s.12(1)
(determination before grant of questions about entitlement to patents
under foreign or international law) and orders made by a convention
court with respect to corresponding questions. For the purposes of s.11,
those orders are treated as if made under s.8. The directions under s.10
to which s.11 applies include those given in relation to patents under
foreign or international law by virtue of s.12(4). With regard to the
applicability of sections 8 to 12 in relation to European patents, see
also
[8.02](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-02),
[10.03](/guidance/manual-of-patent-practice-mopp/section-10-handling-of-application-by-joint-applicants/#ref10-03)
and
[12.09](/guidance/manual-of-patent-practice-mopp/section-12-determination-of-questions-about-entitlement-to-foreign-and-convention-patents-etc/#ref12-09).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 11(1)**
  Where an order is made or directions are given under section 8 or 10 above that an application for a patent shall proceed in the name of one or some of the original applicants (whether or not it is also to proceed in the name of some other person), any licences or other rights in or under the application shall, subject to the provisions of the order and any directions under either of those sections, continue in force and be treated as granted by the persons in whose name the application is to proceed.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Where an original applicant is retained

### 11.03 {#section-1}

This subsection applies where the effect of an order or directions under
s.8 or 10 (or s.12, see 11.02) is that the name(s) in which the
application proceeds include one or more of the original applicants.
Licences or other rights continue in force as if granted by the
person(s) in whose name the application proceeds, unless the order or
directions provide otherwise.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 11(2)**
  Where an order is made or directions are given under section 8 above that an application for a patent shall proceed in the name of one or more persons none of whom was an original applicant (on the ground that the original applicant or applicants was or were not entitled to be granted the patent), any licences or other rights in or under the application shall, subject to the provisions of the order and any directions under that section and subject to subsection (3) below, lapse on the registration of that person or those persons as the applicant or applicants or, where the application has not been published, on the making of the order.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Where none of the original applicants is retained

### 11.04 {#section-2}

Subsections (2) to (5) apply where the effect of an order or directions
under s.8 (or s.12, see [11.02](#ref11-02) is that the name(s) in which
the application proceeds include none of the original applicants.
Licences or other rights lapse on registration of the new name(s) or,
where the application is unpublished, on the making of the order, unless
in either case the order or directions provide otherwise but see
[11.05](#ref11-05) to [11.10](#ref11-10).

  -----------------------------------------------------------------------
   

  **Section 11(3)**

  If before registration of a reference under section 8 above resulting
  in the making of any order mentioned in subsection (2) above -\
  (a) the original applicant or any of the applicants, acting in good
  faith, worked the invention in question in the United Kingdom or made
  effective and serious preparations to do so; or\
  (b) a licensee of the applicant, acting in good faith, worked the
  invention in the United Kingdom or made effective and serious
  preparations to do so;\
  that or those original applicant or applicants or the licensee shall,
  on making a request within the prescribed period to the person in whose
  name the application is to proceed, be entitled to be granted a licence
  (but not an exclusive licence) to continue working or, as the case may
  be, to work the invention.
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 11(3A)**
  If, before registration of a reference under section 8 above resulting in the making of an order under subsection (3) of that section, the condition in subsection (3)(a) or (b) above is met, the original applicant or any of the applicants or the licensee shall, on making a request within the prescribed period to the new applicant, be entitled to be granted a licence (but not an exclusive licence) to continue working or, as the case may be, to work the invention so far as it is the subject of the new application.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 11.05 {#ref11-05}

The making of a reference under s.8 (or s.12) is recorded in the
register, see
[123.05.4](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-4).
If the reference results in an order under s.8 (or s.12) to the effect
that none of the original applicants is retained or that a new
application for a patent may be made, the Office notifies all original
applicants and their licensees of whom it is aware of the making of the
order.

### 11.06 {#ref11-06}

r.90(1) and r.108(1) is also relevant

An applicant or licensee so notified may make a request under s.11(3)
(or under that subsection as applied by s.12(5)) to the person in whose
name the application is to proceed for a non-exclusive licence to work
or continue working the invention. Similarly, following the insertion of
s.11(3A) by the Patents Act 2004 with effect from 1 January 2005, an
applicant or licensee notified may make a request to the person making a
new application under s.8(3) for a non-exclusive licence to work or
continue working the invention. Any such request should be made within
two months of the date of the order under s.8 or, where s.11 is applied
by s.12(5), under s.12(1), this period being extensible at the
discretion of the comptroller. (For the meanings of "exclusive licence"
and "non-exclusive licence", see s.130(1).)

### 11.07 {#ref11-07}

The original applicant or licensee making the request is entitled to
such a licence if they, acting in good faith, worked the invention in
the UK or made effective and serious preparations to do so, before the
registration of the reference. The period and terms of the licence
should comply with s.11(4), ie, be "reasonable". Any dispute regarding
such period or terms or whether they are entitled to a licence may be
referred to the comptroller under s.11(5) by either party, see
[11.08](#ref11-08) to [11.10](#ref11-10).

  ----------------------------------------------------------------------------------------------------------------
   
  **Section 11(4)**
  A licence under subsection (3) or (3A) above shall be granted for a reasonable period and on reasonable terms.
  ----------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 11(5)**
  Where an order is made as mentioned in subsection (2) or (3A) above, the person in whose name the application is to proceed or, as the case may be, who makes the new application, or any person claiming that he is entitled to be granted any such licence may refer to the comptroller the question whether the latter is so entitled and whether any such period is or terms are reasonable, and the comptroller shall determine the question and may, if he considers it appropriate, order the grant of such a licence.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 11.08 {#ref11-08}

r.76(4)(c) is also relevant PR part 7 is also relevant

The dispute see [11.07](#ref11-07) should be referred by filing Patents
Form 2 accompanied by a copy thereof and a statement of grounds in
duplicate. This starts proceedings before the comptroller, the procedure
for which is discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).
The statement should include the period or terms of the licence which
the claimant is prepared to accept or grant.

### 11.09 {#section-3}

r.77 is also relevant

The Office sends a copy of the reference and statement to every person
(not being the claimant) in whose name the application is to proceed or,
as the case may be, who makes the new application, and every person
claiming to be entitled to a licence. If any recipient does not agree to
grant or accept a licence as specified they should file a counter-
statement in the proceedings.

### 11.10 {#ref11-10}

s.108 is also relevant

When the comptroller determines the question to which the reference
relates they may, if appropriate, order the grant of such a licence [see
11.06](#ref11-06). Without prejudice to any other method of enforcement,
such an order has effect as if it were a deed, executed by the
proprietor of the patent and all other necessary parties, granting a
licence in accordance with the order.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
