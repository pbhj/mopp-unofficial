::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 34: Rectification of register {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (34.01 - 34.06) last updated : April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 34.01

This section relates to rectification of the register of patents by
order of the court in response to an application by an aggrieved person.
It authorises rules of court to provide for relevant procedures, the
relevant rule being paragraph 16 of the Practice Direction supplementing
CPR 63 (CPR 63PD).

### 34.02

s.77(1), s.78(2) is also relevant.

The section also applies to register entries relating to granted
European patents (UK) but not to applications for European patents (UK).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 34(1)**
  The court may, on the application of any person aggrieved, order the register to be rectified by the making, or the variation or deletion, of any entry in it.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------

### 34.03

CPR 63PD, para 16.1 is also relevant.

An application for rectification should be made using a claim form,
which should be served both on the other party and on the comptroller.

### 34.04

s.130(1) CPR 63.3(1) is also relevant.

The "court" means the High Court or any patents county court having
jurisdiction by virtue of an order under s.287 of the CDP Act (or the
Court of Session in Scotland). Patent proceedings before the High Court
of England and Wales are in fact assigned to Chancery Division and taken
by the Patents Court.

### 34.05

Rectification of the register may take the form of the making of an
entry therein or the variation or deletion of an existing entry.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 34(2)**
  In proceedings under this section the court may determine any question which it may be necessary or expedient to decide in connection with the rectification of the register.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 34(3)**
  Rules of court may provide for the notification of any application under this section to the comptroller and for his appearance on the application and for giving effect to any order of the court on the application.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 34.06

CPR 63PD, paras 16.1 and 16.2 is also relevant.

A copy of the claim form and the accompanying documents must be served
on the comptroller, who is entitled to appear and to be heard on the
application.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
