::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 124: Rules, regulations and orders; supplementary {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated: April 2007
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 124.01

The ways in which rules, orders etc under the Act are made are governed
by this section.

  ----------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 124(1)**
  Any power conferred on the Secretary of State by this Act to make rules, regulations or orders shall be exercisable by statutory instrument.
  ----------------------------------------------------------------------------------------------------------------------------------------------

### 124.02

The Secretary of State is given a general power to make rules by s.123
and also has the power to make rules for specific purposes under a
number of other sections, eg s.125A concerning biological material;
s.25(5) concerning notification of non-payment of renewal fees; s.32(1)
and (2) concerning entries in the register; s.92(3) and (4) concerning
evidence for use under the EPC; s.130(2) concerning international
exhibitions; and supplementary protection certificates ([see
123.02.1](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-02-01)).

### 124.03 {#ref124-03}

Certain provisions of the Act may be varied by rules made under s.25(2)
(to vary the effective date of grant), s.48(2) (to vary the period after
which compulsory licences become available) and ss.77(9) and 78(8) (to
bring into effect, and to remove, requirements regarding translations of
European patents (UK) and of the claims of applications for such
patents).

### 124.04 {#section-2}

\[deleted\]

### 124.05 {#ref124-05}

The Secretary of State may by order under s.1(5) vary those things
excluded from being treated as inventions.

### 124.06 {#ref124-06}

All rules, regulations or orders such as mentioned above are made by
statutory instrument.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 124(2)**
  Any Order in Council and any statutory instrument containing an order, rules or regulations under this Act, other than an order or rule required to be laid before Parliament in draft or an order under section 132(5) below, shall be subject to annulment in pursuance of a resolution of either House of Parliament.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 124.07 {#ref124-07}

Orders in Council which may be made under the Act are under s.54(1)
where a patented invention is being worked abroad, under s.59(3) and (4)
regarding Crown use during a period of emergency, under s.90(1) and (2)
to declare a country to be a convention country and under s.132(2) to
modify the Act as it applies in the Isle of Man.

### 124.08 {#ref124-08}

Such Orders in Council and statutory instruments, with the following
exceptions, are annulled if either House of Parliament so resolves. The
exceptions are statutory instruments containing an order or rule
required to be laid before Parliament in draft, ie an order under s.1(5)
as referred to [in 124.05](#ref124-05) or a rule under s.25(2) or
s.48(2) as referred to [in 124.03](#ref124-03), or an order under
s.59(3) and (4) as referred to [in 124.07](#ref124-07) or under s.132(5)
as referred to [in 124.05](#ref124-05).

  ---------------------------------------------------------------------------------------------------------------
   
  **Section 124(3)**
  Any Order in Council or order under any provision of this Act may be varied or revoked by a subsequent order.
  ---------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
