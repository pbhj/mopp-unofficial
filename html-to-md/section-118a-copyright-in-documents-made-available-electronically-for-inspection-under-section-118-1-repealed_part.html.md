::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 118A: Copyright in documents made available electronically for inspection under section 118(1) \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections 118A.01 -118A.02) last updated April 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 118A.01 {#a01}

Section 118A was introduced into the Patents Act 1977 on 1 October 2011.
The section provided that copyright was not infringed if documents from
the published patent file were made available under section 118(1)
electronically so that the public can access them. Copyright was also
not infringed by copying the documents in order to enable this
electronic access.

### 118A.02 {#a02}

Section 118A was repealed by the [Copyright (Public Administration)
Regulations 2014 (SI 2014/1385) which came into force on 1st June
2014](http://www.legislation.gov.uk/uksi/2014/1385/contents/made){rel="external"}.
These Regulations introduced into the \[Copyright, Designs and Patents
Act 1988 new exceptions to copyright infringement, which apply to
certain documents which are published online by the relevant public
body, such as the Comptroller. The documents must be ones which are open
to public inspection or on an official register and concern material
communicated to the Crown in the course of public business. The new
exceptions mean that section 118A became redundant.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
