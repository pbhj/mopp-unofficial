::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 5: Priority date {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (5.01 - 5.32) last updated: January 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 5.01

s.130(7) is also relevant

This section is intended to have, as nearly as practicable, the same
effect as the corresponding provisions of the EPC, PCT and CPC. Articles
87 and 88 and rule 53 of the EPC and Article 8 of the PCT appear to
generally correspond to s.5. Relevant requirements are prescribed in
rr.6 to 9. The principles of the section derive from Article 4 of the
Paris Convention for the Protection of Industrial Property.

### 5.02

Section 5 was amended by the Regulatory Reform (Patents) Order 2004
(S.I. 2004 No. 3204) to incorporate the principles of Article 13 of the
Patent Law Treaty. The amended section applies to applications made on
or after 1 January 2005; for earlier applications, the provisions of
section 5 prior to the Order coming into force apply. These earlier
provisions are indicated in the text where they may be relevant to
pending applications made before 1 January 2005.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 5(1)**
  For the purposes of this Act the priority date of an invention to which an application for a patent relates and also of any matter (whether or not the same as the invention) contained in any such application is, except as provided by the following provisions of this Act, the date of filing the application.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 5.03 {#ref5-03}

The priority date of all matter (whether claimed or not) in an
application is, prima facie, the date of filing of the application. The
onus is therefore on an applicant who wishes to claim an earlier
priority date to comply with the specified conditions [see
5.04](#ref5-04)

  -----------------------------------------------------------------------
   

  **Section 5(2)**

  If in or in connection with an application for a patent (the
  application in suit) a declaration is made, whether by the applicant or
  any predecessor in title of his, complying with the relevant
  requirements of rules and specifying one or more earlier relevant
  applications for the purposes of this section made by the applicant or
  a predecessor in title of his and the application in suit has a date of
  filing during the period allowed under subsection (2A)(a) or (b) below,
  then-\
  (a) if an invention to which the application in suit relates is
  supported by matter disclosed in the earlier relevant application or
  applications, the priority date of that invention shall instead of
  being the date of filing the application in suit be the date of filing
  the relevant application in which that matter was disclosed or, if it
  was disclosed in more than one relevant application, the earliest of
  them;\
  (b) the priority date of any matter contained in the application in
  suit which was also disclosed in the earlier relevant application or
  applications shall be the date of filing the relevant application in
  which that matter was disclosed or, if it was disclosed in more than
  one relevant application, the earliest of them.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 5(2A)**

  The periods are-\
  (a) the period of twelve months immediately following the date of
  filing of the earlier specified relevant application, or if there is
  more than one, of the earliest of them; and\
  (b) where the comptroller has given permission under subsection (2B)
  below for a late declaration to be made under subsection (2) above, the
  period commencing immediately after the end of the period allowed under
  paragraph (a) above and ending at the end of the prescribed period.
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------
   
  **Section 5(2B)**
  The applicant may make a request to the comptroller for permission to make a late declaration under subsection (2) above.
  ---------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 5(2C)**

  The comptroller shall grant a request made under subsection (2B) above
  if, and only if-\
  (a) the request complies with the relevant requirements of rules; and\
  (b) the comptroller is satisfied that the applicant's failure to file
  the application in suit within the period allowed under subsection
  (2A)(a) above was unintentional.
  -----------------------------------------------------------------------

### 5.04 {#ref5-04}

If the date of filing of an earlier application is to be treated as the
priority date of some or all of the matter in an application in suit,
the following conditions must be met:-

\(a\) a declaration must be made, and this must comply with the
"relevant requirements" as set out in rr.6 and 7 [see
5.07-5.13](#ref5-07);

\(b\) the earlier application should have a date of filing not more than
twelve months earlier than that of the application in suit [see
5.18](#ref5-18);

\(c\) an applicant can nevertheless request permission from the
comptroller to make a declaration of priority in respect of an
application filed within the period prescribed by r.7(1) (i.e. within
two months of the expiry of 12 months from the date of filing of the
earlier application) but such permission will only be granted when the
request complies with r.7 and the comptroller is satisfied that failure
to file the application within the 12 month period was unintentional
[see 5.26.3-4](#ref5-26-3-4);

\(d\) the earlier application must have been made by the same applicant
as the application in suit or by their predecessor in title [see
5.19-19.2](#ref5-19); and

\(e\) it must be a "relevant application" [see 5.30](#ref5-30).

The earlier application must also be one which is not disregarded under
the provisions of s.5(3) [see 5.27- 5.28.5](#ref5-27). However so long
as it has a filing date, the outcome of the earlier application is not
relevant, and it may serve to establish a priority date even if it is
subsequently withdrawn or refused.

### 5.05 {#ref5-05}

The priority date conferred by an earlier application cannot be earlier
than the date on which it was filed, even if the authority with which it
was filed has allowed it to be antedated, for example to the date on
which it was shown at an exhibition ([La Soudure Electrique Autogene
SA's Application, 56 RPC
218](https://doi.org/10.1093/rpc/56.8.218){rel="external"}).

### 5.06 {#section-2}

Priority may be claimed from any number of earlier applications. So long
as they are all "relevant applications" [see 5.30](#ref5-30) there is no
requirement that they should have been filed under the same national law
or international agreement. However it should be remembered that the
periods for complying with r.6 to 8 [see 5.08-5.12](#ref5-08) are
measured from the date of the earliest declared application [see
5.16](#ref5-16), and that all necessary documents should be supplied
within this period.

### Relevant requirements

  ----------------------------------------------------------------------------------------------------------------------------
   
  **The Declaration**
  Where the earliest priority date to be claimed is not more than 12 months before date of filing of the application in suit
  ----------------------------------------------------------------------------------------------------------------------------

### 5.07 {#ref5-07}

s.5(2A), r.6(1), r.6(4) is also relevant

Where the earliest priority date to be claimed is not more than 12
months before the date of filing of the application in suit, the
declaration should be made at the time of filing. However, there are
possibilities to make a declaration after filing the application [see
5.07.1](#ref5-07-1). In all cases, the declaration must state the date
of filing of the (or each) earlier application and the country it was
filed in or in respect of. Such a declaration is made by completing the
relevant part of Form 1. Errors or mistakes in declarations can be
rectified by correction ([see
117.19](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-19)).
In order for the earlier application to be uniquely identified, it is
advisable to give the application number of that application at the
earliest time that it is available although this information may be
added later [see 5.08](#ref5-08).

### 5.07.1 {#ref5-07-1}

r.6(2), r.6(3), PR Sch.4. part 1 is also relevant

If a declaration is not made at the time of filing, it can be made (or
another declaration can be added) at any time up to sixteen months after
the earliest priority date claimed provided that the declaration is made
on a Form 3 accompanied by the prescribed fee and a request to publish
the application under section 16(1) has either not been made or any such
request was withdrawn before the preparations for publication of the
application had been completed. This sixteen month period cannot be
extended. The provisions allowing a declaration of priority to be made
later than the filing date only apply to applications filed on or after
1 January 2005.

  ------------------------------------------------------------------------------------------------------------------------
   
  Where the earliest priority date to be claimed is more than 12 months before date of filing of the application in suit
  ------------------------------------------------------------------------------------------------------------------------

### 5.07.2 {#ref5-07-2}

s.5(2A)(b), s.5(2B), r.7(1), s.5(2C)(a), s.5(2C)(b), r.7(8) is also
relevant

An applicant may request permission to make a declaration of priority in
respect of an application filed in the two months following the expiry
of the period specified in s.5(2A)(a) but the comptroller shall only
grant such permission if:-

\(a\) the permission is requested in accordance with rules, and

\(b\) the comptroller is satisfied that failure to file the application
within the time specified in s.5(2A)(a) was unintentional [see
5.26.4](#ref5-26-4), and

\(c\) either there is no request for accelerated publication under
section 16(1), or if a request for accelerated publication was made, it
was withdrawn before the preparations for publication were completed.

### 5.07.3 {#section-3}

PR Sch. 4, Part 1, r.7(4), r.7(6) is also relevant

Except for the situation set out in [5.07.4](#ref5-07-4), the request
must be made on Form 3 and must be filed before expiry of the two month
period prescribed in r.7(1), which cannot be extended. The declaration
must be made at the time of filing the request and the reason why the
application was not filed within the period allowed by s.5(2A)(a) must
be stated. Evidence supporting that reason must also be provided. Where
that evidence does not accompany the request, the Comptroller must
specify a period within which the evidence must be filed. Where an
international application has entered the national phase, a request may
be made under s.5(2B) within one month of the date the national phase of
the application begins.

### 5.07.4 {#ref5-07-4}

If, however, the application in suit is a new application filed under
section 8(3), 12(6) or 37(4) or as mentioned in section 15(9), the
request may be made in writing instead of on Patents Form 3, no evidence
need accompany the request, and both the new application and the request
may be made after the end of the two-month period prescribed in r.7(1)
but must be made on the same date.

### Priority documents

### Certified Copies

r.8(1)-(2), r.8(5), r.21, PR Sch. 4 is also relevant

### 5.08 {#ref5-08}

The application number of the priority application must be supplied
within sixteen months (extendable in accordance with r.108(2) or (3),
[see
123.34-42](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34))
of the declared priority date. Moreover, except in the circumstances
referred to in paragraph [5.09](#ref5-09), a copy of the earlier
application, either certified by the authority with which it was filed
or otherwise verified to the satisfaction of the comptroller, should
also be supplied within this period. Where the application claims an
earlier date of filing, these conditions should be complied with within
two months of the date of filing (or later in accordance with an
extension under r.108(2) or (3) and (4) to (7), [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34))
if the sixteen month period has already expired ([but see
5.09](#ref5-09)), except if the new application is filed less than six
months before the compliance date, in which case these conditions should
be complied with on the filing date (extendable under r.108(2) or (3)
and (4) to (6)). In the case of a divisional application the sixteen
month period is automatically extended if that period has been extended
in respect of the earlier application. Failure to comply with these
conditions within the prescribed period cannot be rectified under s.117
([Klein Schanzlin & Becker AG's Application \[1985\] RPC
241](https://doi.org/10.1093/rpc/1985rpc241){rel="external"}, [see
117.01](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-01)).

### 5.09 {#ref5-09}

r.8(3) is also relevant

However, if an application specified in the declaration is an
application for a patent under the Act, an international application for
a patent which was filed at the Office, or any other application of
which a copy has been filed pursuant to a declaration of priority in
respect of another application under the Act, then, under r.112 any
necessary certified copy will be prepared in the Office without the
applicant having to request it or pay any associated fee. If the earlier
application has been withdrawn or treated as withdrawn, documents may be
transferred to serve as priority documents for the application in suit
([see
15A.18](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-18)).
In all cases, the applicant should inform the Office of the number of
the file on which the earlier application is to be found.

### 5.10 {#section-4}

r.8(4), r.8(5), s.81(3)(c) is also relevant

Where the application in suit is an international application for a
patent (UK), the requirements referred to in [5.07](#ref5-07) and
[5.08](#ref5-08) are regarded as having been complied with to the extent
that the requirements of r.4.10(a) and (b), subject to rules 26bis.1 and
26bis.2(b), and of rule 17.1 of the PCT have been fulfilled. (A copy of
the priority documents is supplied to the Office by the International
Bureau). Likewise if the application is one converted from a European
application under s.81, these conditions are treated as having been met
to the extent that r.52 and 53 of the EPC have been complied with.

### Translations

### Applications filed on or after 1 January 2005:

r.9 is also relevant

### 5.11 {#ref5-11}

Translations of foreign-language certified copies of priority
applications (or declarations that the application in suit is a complete
translation into English of the priority application) will only be
required where the matters disclosed in the earlier application are
relevant to the determination of whether or not an invention, to which
the application in suit relates, is new or involves an inventive step.
Where an examiner considers that a translation is necessary, a direction
will be issued to the applicant and a period for complying with the
direction will be specified. The period is extendable under s.117B and
r.109. Failure to comply with a direction will result in the declaration
made for the purposes of s.5(2) being disregarded. [See also
18.15](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-15).

### 5.12 {#section-5}

r.115 is also relevant

There is no requirement for any translation filed in response to a
direction of the comptroller under r.9(1) to be verified. Where there is
a genuine doubt with respect to the accuracy of a translation the
comptroller may notify the applicant of the reasons for the doubts and
require evidence to establish the accuracy of the translation.

### Applications filed before 1 January 2005:

PR 1995, r.6(6), pre- RRO is also relevant

### 5.13 {#section-6}

\[deleted\]

### 5.13.1 {#section-7}

\[deleted\]

### 5.13.2 {#section-8}

\[deleted\]

### Loss of priority

### 5.14 {#section-9}

If any of the requirements of rr.6-9 are not met in respect of an
application declared for priority purposes, then the declaration is
invalid in respect of that application and any rights which would have
derived from it are lost. If (but only if) preparations for publication
have not yet been completed, the declared priority date may be affected
[see 5.16](#ref5-16).

### Declared priority date

s.130(1) is also relevant

### 5.15 {#ref5-15}

"Priority date", as used in the Act, is defined in s.130(1) as "the date
determined as such under section 5", and is a property ascribed to the
invention(s) and other matter contained in an application but not to the
application itself. Instead the term "declared priority date" of an
application is defined (in r.3); this date confers no rights, but serves
only as a "marker" from which various time limits are measured. It may
be noted that while the contents of an application may have several
priority dates (one of which may be the filing date), an application can
only have one declared priority date, and may have none (in which case
prescribed periods are measured from the filing date).

### 5.16 {#ref5-16}

r.3 is also relevant

In the case of an application made under the Act, "declared priority
date" means the date of filing of the earliest relevant application
specified in a declaration made for the purposes of s.5 where the date
has not been lost or abandoned, or the declaration withdrawn, before the
completion of preparations for publication. Thus for example if a
certified copy of the (or the earliest) declared application is not
supplied within the prescribed period ([see 5.08](#ref5-08)), then not
only is the priority date claimed from that application lost, but also,
since the application in suit will necessarily not yet have been sent
for publication, that date will cease to be the declared priority date;
its place is taken by the next earliest declared application or, where
there is none, the filing date, and time limits and dates, including the
date when publication is due, are reckoned from this new date. On the
other hand, where a translation is required and it (or a declaration
that the application in suit is a complete translation into English of
the priority application) is not filed within the specified period ([see
5.11-5.13.1](#ref5-11)), then although this will result in the priority
claim being disregarded, if (as will generally be the case) this takes
place after completion of preparations for publication, the declared
priority date will remain unchanged.

### 5.17 {#section-10}

Similarly, the declared priority date of an international application is
the earliest priority date which has not been lost or abandoned under
the provisions of the PCT, i.e. before entering the national phase ([see
89A.04](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-04)).
And in the case of a converted European application it is the earliest
priority date still extant when the comptroller directs under s.81 that
it be treated as an application under the Act.

### Period for claiming priority

### 5.18 {#ref5-18}

In accordance with Article 4C of the Paris Convention, priority may be
claimed from an earlier application if the application in suit has a
date of filing not more than twelve months later than that of the
earlier application, and this is reflected, via s.5(2), in s.5(2A)(a).
However if this period expires on an excluded day or on a day which is
certified as one on which there was an interruption under r.110(1) ([see
123.43](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-43))
then the period is extended to include the first following day which is
not excluded or certified. The period may also be extended under r.111
in particular cases affected by a failure or undue delay in a
communication service ([see
123.46-47](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-46)).
The period may also be extended under r.107 in particular cases where
the circumstances of r.107(3) exist ([see
123.06-10](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-06)).
In no other circumstances may the twelve months period be extended --
although in limited circumstances it is possible for the date of filing
of the application in suit to be outside the twelve months period; [see
5.07.2 to 5.07.3](#ref5-07-2), and [5.26.2 to 5.26.4](#ref5-26-2).

### The applicant

### 5.19 {#ref5-19}

For priority to be claimed, the person making the application in suit
must either be the same person who made the earlier application or must
be their successor in title. Furthermore, for priority to be claimed in
the circumstances where the earlier application has more than one
applicant, the application in suit must be made by all of the same
applicants, or their successor(s) in title. It is not sufficient if the
application in suit is made by only one or some of the earlier
applicants, as confirmed by the Patents Court in [Edwards Lifesciences
AG v Cook Biotech Inc \[2009\] EWHC 1304 (Pat); \[2009\] FSR
27](https://uk.westlaw.com/Document/I46FD42E0CDAE11DEA210FB50F156DB33/View/FullText.html){rel="external"}.

### 5.19.1 {#section-11}

Where the applicant of the application in suit is not the applicant of
the earlier application, the applicant for the application in suit must
have acquired from the earlier applicant (or applicants) the right to be
granted a patent, for example by virtue of employment or by assignment.
It does not matter if the original applicant retains some rights in the
earlier application -- they may for instance have assigned the rights to
some only of the matter contained in that application, or have assigned
only the right to apply in certain countries -- so long as they have
transferred to the present applicant the right to be granted a patent on
the application in suit. In order for the declaration of priority to be
valid at the time it is made, i.e. at the time of filing the application
in suit, the transfer must already have taken place. This was also
confirmed in Edwards Lifesciences v Cook Biotech.

### 5.19.2 {#section-12}

In [KCI Licensing Inc & Ors v Smith & Nephew Plc & Ors \[2010\] EWHC
1487
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2010/1487.html){rel="external"};
Arnold J considered whether an application having two applicants could
validly claim priority from an earlier application made by only one of
those applicants, where no formal assignment of priority rights had been
made. He concluded that the circumstances surrounding the filing of the
later application inferred that the earlier applicant had agreed by
conduct to transfer part of their interest in the invention to the
second applicant, and that was sufficient to make the second applicant a
successor in title for the purposes of claiming priority. It follows
that, where an application names a further applicant (or applicants) in
addition to the applicant(s) named on a priority application, a formal
assignment may not necessarily be required for the priority claim to be
valid.

### Priority date of invention or other matter

### 5.20 {#ref5-20}

The test for deciding whether an invention is supported and sufficiently
described by matter disclosed in an earlier application is basically the
same as that for deciding whether a claim of a specification is
supported and sufficiently described by the description ([see
14.60-14.104](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-60)
and
[14.142-14.156](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-142)).

### 5.21 {#section-13}

s.130(3), s.125(2) is also relevant

As Article 4H of the Paris Convention makes clear, it is not necessary
that the invention be found in the claims of the earlier application. In
order to determine whether there is support for an invention or other
matter in the earlier application, everything claimed or disclosed
(other than by way of disclaimer or acknowledgement of prior art) in the
earlier application may be taken into account ([see
14.171.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-171-1))
for practice relating to what is included in an abstract). Support may
not however be derived by combining the teaching of separate
applications unless one of them contains directions to do so.

### 5.22 {#section-14}

The priority date of a feature or combination of features is the date of
the earliest application whose disclosure supports that feature or
combination and if different ways of putting an invention into practice
were disclosed at different dates, they will have different priority
dates, even if they are covered by a single claim.

### 5.23 {#ref5-23}

The earlier application must disclose the particular combination of
features which make up the invention and also provide an enabling
disclosure of that invention ([Asahi Kasei Kogyo KK \[1991\] RPC
485](https://doi.org/10.1093/rpc/1991rpc485){rel="external"}). Thus, in
[Biogen Inc v Medeva Plc \[1997\] RPC
1](https://doi.org/10.1093/rpc/1997rpc1){rel="external"} it was
confirmed that the same test for sufficiency of disclosure applies
whether directed to an earlier application for determining whether a
claim is entitled to a priority date or to the description of the
application or patent in suit under sections 14(3) and 14(5)(c). The
Court of Appeal adopted this approach in [Pharmacia Corp. v Merck & Co.
Inc. \[2002\] RPC
41](https://doi.org/10.1093/rpc/2002rpc41){rel="external"}, in which it
was held that the technical contribution to the art contained in the
earlier application must justify the claimed monopoly in the application
in suit, with the result that the earlier application contains
sufficient disclosure to constitute an enabling disclosure across the
entire width of the claim. In the case in question, the application in
suit claimed a narrower range of compounds than that disclosed in the
earlier application. The court held that the priority claim was not
supported, since the earlier application did not provide an enabling
disclosure concerning the technical contribution to the art made by
selecting the subclass of compounds claimed in the application in suit.

### 5.23.1 {#ref5-23-1}

In [Hospira UK Generics (UK) Ltd (t/a Mylan) v Novartis AG \[2013\] EWCA
Civ
1663](http://www.bailii.org/ew/cases/EWHC/Patents/2013/516.html){rel="external"}
the Court of Appeal held that claim 7 of the Novartis patent was not
entitled to priority because the disclosure of the priority document was
either too broad or too specific. Claim 7 contained features relating to
a drug for the treatment of osteoporosis, an intravenous mode of
administration, a dosage size of about 2-10 mg and a dosing interval of
about once a year. Although the priority document contained a passage
disclosing "2-10 mg once a year", it did not tell the reader that this
dosage range would be suitable no matter what condition was being
treated or mode of administration was used. The passage instead
suggested that, dependent on the method of administration and condition
being treated, some doses within the given range may be suitable. The
priority document also contained an example (example 5) specific to
intravenous administration which showed that 4 mg, once a year was
effective in treating post-menopausal osteoporosis. Example 5 did not
however teach anything about other doses at that dosage interval and so
the other doses claimed were not supported by the disclosure in this
example.

### 5.23.2 {#ref5-23-2}

In [Evans Medical Ltd's Patent \[1998\] RPC
517](https://doi.org/10.1093/rpc/1998rpc517){rel="external"}, Laddie J
held that what is important is what the document teaches, not how the
contents got there, saying "if an inventor through clever foresight or
lucky guess work describes something which works and how to do it, his
disclosure is enabling". Nevertheless, in a case relating to a second
medical use invention, the Patent's Court in [Hospira UK Ltd v Genentech
Inc \[2014\] EWHC
1094](http://www.bailii.org/ew/cases/EWHC/Patents/2014/1094.html){rel="external"}
held that the earlier application must show that the claimed therapeutic
effect was plausible. If this was not the case, then the earlier
disclosure could not be considered enabling ([see
4A.30.1](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-30-1)).

### 5.23.3 {#ref5-23-3}

In [Unilin Beheer BV v Berry Floor NV \[2004\] EWCA (Civ)
1021](http://www.bailii.org/ew/cases/EWCA/Civ/2004/1021.html){rel="external"},
Jacob LJ held that the approach to determining priority is not
formulaic, it is a question about technical disclosure, explicit or
implicit. The question is whether there is enough in the priority
document to give the skilled person the same information as is in the
subject of the claim and enables them to work the invention in
accordance with that claim. He goes on to say that a priority document
should contain the information that justifies the later claim
irrespective of whether it is expressed in a claim, consistory clause,
statement of invention, other text or drawing or in any combination of
these. In this case it was submitted that the priority document
disclosed three features, A+B+C, in combination. It was therefore argued
that a claim to one of those features, without the other two, cannot
have priority. However, the judge rejected this argument, noting "that
when features A+B+C are disclosed, a lot must turn on what they actually
are. Some inventions consist of a combination of features -- the
invention consists in the very idea of putting them together. In other
cases that is simply not so -- the features are independent one from the
other. Whether, given a disclosure of A+B+C, there is also a disclosure
of A or B or C independently depends on substance, not a formula. The
ultimate question is simply whether the skilled man can derive the
subject-matter of the claim from the priority document as a whole."

### 5.24 {#ref5-24}

The criteria to be applied can be said to be similar to those used to
decide whether a claim is anticipated by the disclosure of an earlier
document ([see 2.03-
2.20](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-03)).
Thus if a priority document is silent about any essential element for
which a patent is sought, the right to priority cannot generally be
established. In particular, if a claim in a priority document is broad
enough to cover a particular specific technical feature, it does not
follow that it discloses that feature for the purpose of claiming
priority. On the other hand, a feature which would necessarily be
present when the teaching of the earlier application is put into
practice may be treated as having been disclosed by implication (c.f.
2.07). In [Letraset Ltd v Rexel Ltd \[1974\] RPC
175](https://doi.org/10.1093/rpc/91.6.175){rel="external"} at pages
195-7, a claim which included the feature that adhesion between indicia
and a carrier sheet was breakable by local stretching of the sheet was
held to be fairly based on a provisional specification which was silent
on this feature, since it was in fact something which could be shown to
happen when the material described in the provisional specification was
made and used.

### 5.25 {#ref5-25}

This approach is also consistent with the Opinion of the Enlarged Board
of Appeal in Requirement for claiming priority of the "same invention"
\[2002\] EPOR 17 (G 2/98), in which it was held that implicit disclosure
in the earlier application may be taken into account, so that a priority
claim is valid if the skilled person can derive the subject matter of
the claim of the application in suit "directly and unambiguously, using
common general knowledge, from the previous application as a whole".
This would appear to be consistent with the earlier decision in
Biogen/Human beta-interferon \[1999\] EPOR 451 (T207/94), in which it
was held that the disclosure of the earlier application was enabling in
respect of the claims of the later application, despite the fact that
the disclosure would have required a non-negligible amount of work on
the part of the skilled person to achieve the invention as claimed in
the later application. This was acceptable since the work required by
the skilled person would have been quite feasible given the existing
state of the art. Nevertheless, the criteria for assessing a valid
priority claim are not identical to those used when considering
anticipation. As the Enlarged Board concluded in G2/98, priority can
only be validly claimed where the skilled person can derive the
subject-matter of the claim directly and unambiguously, using common
general knowledge, from the previous application as a whole. However it
is also possible for a single claim to set out a number of alternative
subject matters (an "OR"-claim) which may each be entitled to different
priority dates -- see below.

### 5.25.1 {#ref5-25-1}

A claim in a patent application can have multiple priority dates, see
[125.08](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-08).
In [Novartis AG v Johnson & Johnson \[2009\] EWHC (Pat)
1671](http://www.bailii.org/ew/cases/EWHC/Patents/2009/1671.html){rel="external"},
Kitchin J referred to G 2/98 and said that different priority dates can
be assigned to different parts of a patent claim where those parts
represent a limited number of clearly defined alternatives and those
alternatives have been disclosed by different priority documents. This
is known as partial priority.

### 5.25.2 {#ref5-25-2}

In [Nestec SA & Ors v Dualit Ltd & Ors \[2013\] EWHC 923
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2013/923.htm){rel="external"}
the claims of a patent were held to lack novelty over the disclosure of
the document from which it claimed priority. In this case the claims of
the patent were found not to be entitled to the claimed priority date,
as they covered a whole range of different arrangements (i.e. not a
limited number of clearly defined alternatives) -- some of which were
not disclosed in the priority document. The earlier document was
published, thereby forming part of the state of the art under s. 2(3),
and anticipated the claims of the patent (see [5.26](#ref5-26),
[15.22](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-22)).
This situation is sometimes referred to as 'poisonous priority'. It is
important to note that this situation can also arise when the state of
the art is not the priority document itself but a published application
sharing a priority claim, for example in a divisional application ([see
15.22](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-22)).

### 5.25.3 {#section-15}

This issue of partial and poisonous priority was considered by the EPO
Enlarged Board of Appeal in [G
01/15](https://register.epo.org/application?documentId=EZ0WSAB00165684&number=EP98203458&lng=en&npl=false){rel="external"}.
In its decision, the EBA concluded that, where claims are generalised,
they may be conceptually divided into two parts corresponding to that
aspect which was disclosed in the priority application and is entitled
to partial priority, and that aspect which was not disclosed and is not
entitled to priority. Following this decision, the previous requirement
(arising from G 02/98) that a claim must provide a "limited number of
clearly defined alternative subject-matters" to be allowed partial
priority, no longer applies at the EPO. Decisions of the Enlarged Board
of Appeal are not binding on UK courts, although they are persuasive
(see 0.09), so it remains to be seen whether the UK courts adopt this
approach or continue with the approach set out in Nestec above. In the
meantime, examiners remain bound by, and must therefore follow, the
approach in Nestec above.

### Examination of claim to priority

### Application filed within 12 months of earliest priority date claimed

### Declaration made at the time of filing

### 5.26 {#ref5-26}

r.23, r.6(1), r.24(1), r.6(4), r.6(5) is also relevant

Where the declaration of priority is made at the time of filing of the
application in suit, the formalities examiner must determine during the
preliminary examination whether the requirements of r.6 to 9 have been
met and whether any priority date claimed is more than twelve months
before the filing date of the application in suit; any other
discrepancies noticed in the declaration or the priority documents
should be reported at this stage ([see
15A.13-15A.17](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-13))
. In general no comment should be made on the content of the earlier
application, except that the applicant should be informed if it is so
different from the application in suit that it appears likely that they
have declared and/or filed the wrong earlier application. The question
of whether the invention is supported by matter disclosed in the earlier
application should be considered only when this is necessary in order to
determine whether or not a given document forms part of the state of the
art ([see
18.15-18.17](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-15))
Examiners should be aware that if the declaration of priority is found
invalid the priority document itself, if published, may form part of the
state of the art ([see 5.25.2](#ref5-25-2)). Even if it is found that an
invention is not supported by an earlier application, the "declared
priority date" should remain unchanged ([see 5.15](#ref5-15)).

### Declaration made after time of filing

### 5.26.1 {#ref5-26-1}

Where a declaration is made or added after the date of filing of the
application in suit, the formalities examiner must ensure the Form 3 was
filed within the sixteen month period prescribed in r.6(2)(b), that
option (ii) on part 4 of the Form has been correctly indicated, and that
the application has not already been published. If the applicant
requested accelerated publication b but the preparations for publication
have not yet been completed, the publication cycle should be suspended
and the applicant given the opportunity to withdraw the request to
enable the priority declaration to be accepted. The criteria are
otherwise as given in [5.20-25](#ref5-20) above, and, if the declaration
is validly made, the priority date must be recorded and other dates
recalculated as necessary.

### Application filed more than 12 months after earliest priority date claimed

### 5.26.2 {#ref5-26-2}

r.7 is also relevant

Whenever permission to make a late declaration of priority is made on a
Form 3, the formalities examiner must check:-

\(a\) that option (i) is correctly indicated on part 4 of the Form;

(b)(i) that for domestic applications both the request and the
application in suit were filed before expiry of the two-month period
prescribed in r.7(1) (unless the application in suit is a new
application filed under section 8(3), 12(6) or 37(4) or as mentioned in
section 15(9), in which case see [5.07.4](#ref5-07-4);

(b)(ii) that for PCT applications, the application in suit was filed
before expiry of the two-month period prescribed in r.7(1) and the
request was filed within one month of entry into the national phase;

\(c\) that the reason for failing to file the application within the
period specified in s.5(2A)(a) is entered into part 5 of the Form and is
supported by evidence;

\(d\) that the declaration has been correctly made in part 6 of the
Form.

### 5.26.3 {#ref5-26-3-4}

Where a request is correctly made in compliance with rules and the
comptroller is satisfied that the failure to file the application in
suit within the period allowed by s.5(2A)(a) was unintentional, the
comptroller shall grant the permission and treat the declaration as
valid. The priority date should be recorded and other dates recalculated
as necessary, but the date of filing of the application in suit will
remain the date on which it was actually filed.

### 5.26.4 {#ref5-26-4}

There is no definition in the Act or rules as to what is meant by
"unintentional" as stated in section 5(2C)(b) in determining whether to
allow a request for permission to make a late declaration of priority
under section 5(2B). In [Abaco Machines (Australasia) Pty Ltd's
Application \[2007\] EWHC 347
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2007/347.html){rel="external"}
Lewison J held that an intention to file a PCT application was not an
intention to file the "application in suit", which the judge expanded by
way of the meaning given to "application in suit" in section 5(2) of the
Act and the definition of "application" provided by section 130(1) of
the Act to read "an application for a patent under this Act". In the
similar case of [Sirna Therapeutics Inc's Application \[2006\] RPC
12](https://doi.org/10.1093/rpc/2006rpc12){rel="external"}, the hearing
officer held that the unintentional failure by the applicant to file the
application within twelve months of the earliest priority date must
however relate to the application in suit in its entirety and not merely
a failure to file the subject matter of the application. As in the case
of Abaco this case involved a request for late declaration of priority
which was refused as the application that was unintentionally not filed
was a PCT application and there was no intention to file the application
in suit within the twelve-month priority period. The hearing officer
observed that the requirement to show an intention to file an
application in time differed from the test of "continual underlying
intention to proceed" as held in [Heatex Group Ltd's Application
\[1995\] RPC
546](https://doi.org/10.1093/rpc/1995rpc546){rel="external"} when
exercising discretion under rule 110(4) of the Patents Rules 1995, which
is equivalent to rule 108(3) under the Patents Rules 2007 ([see
123.37](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-37)),
but case law under this rule may be of relevance in analysing the
evidence to establish the applicant's intentions.

### 5.26.5 {#ref5-26-5}

[Sirna Therapeutics Inc's Application \[2006\] RPC
12](https://doi.org/10.1093/rpc/2006rpc12){rel="external"} and [Anning's
Application (BL
O/374/06)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F374%2F06&submit=Go+%BB){rel="external"}
established that the "continual underlying intention" test in
[Heatex](https://doi.org/10.1093/rpc/1995rpc546){rel="external"} is not
applicable in determining the meaning of the word "unintentional" (in
section 5(2B) or section 20A).

### 5.26.6 {#ref5-26-6}

In [Matsushita Electric Industrial Co. v Comptroller General of Patents
\[2008\] EWHC 2071
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2008/2071.html){rel="external"},
which concerned a request for restoration under s.28, Mann J gave some
guidance on the level of evidential burden required to "satisfy" the
Comptroller that the failure in section 28(3) was "unintentional". The
applicant in that case chose not to file any evidence beyond a bald
assertion of the statutory test that the failure to pay the renewal fee
on time was unintentional. It argued that that was all the statute
required to satisfy the comptroller. It was held by the Judge that a
mere assertion that the failure to pay the renewal fee was unintentional
is not sufficient to enable the Comptroller to determine that the
requirements of s.28(3) are fulfilled. He said:

> the Act requires a judgment to be formed by the Comptroller so that he
> can be satisfied of the relevant matters. A judgment usually has to be
> made on the basis of evidence... The evidence required in any
> particular case where satisfaction is required depends on the nature
> of the enquiry and the nature and purpose of the decision to be
> made... A significant matter requires significant proof. I repeat, the
> Act does not require a statement that the failure to pay fees was
> unintentional. It requires the Comptroller to be satisfied of that
> fact.

### 5.26.7 {#section-16}

It is clear from this judgment that while there is no universal rule as
to what level of evidence has to be provided to satisfy the comptroller
of the unintentional lapse in section 28(3), and by implication in
sections 5(2B) and 20A, some evidence is required above and beyond a
bald assertion that the statutory test has been met.

  -----------------------------------------------------------------------
   

  **Section 5(3)**

  Where an invention or other matter contained in the application in suit
  was also disclosed in two earlier relevant applications filed by the
  same applicant as in the case of the application in suit or a
  predecessor in title of his and the second of those relevant
  applications was specified in or in connection with the application in
  suit, the second of those relevant applications shall, so far as
  concerns that invention or matter, be disregarded unless -\
  (a) it was filed in or in respect of the same country as the first;
  and\
  (b)not later than the date of filing the second, the first (whether or
  not so specified) was unconditionally withdrawn, or was abandoned or
  refused, without -\
  (i) having been made available to the public (whether in the United
  Kingdom or elsewhere);\
  (ii) leaving any rights outstanding; and\
  (iii) having served to establish a priority date in relation to another
  application, wherever made.
  -----------------------------------------------------------------------

### 5.27 {#ref5-27}

Subsection (3) derives from Article 4C(4) of the Paris Convention and
deals with the situation where two (or more) earlier applications both
contain the subject matter of the application in suit. The effect of the
subsection is that, in most circumstances, the priority date of the
subject matter in question is established by the earliest previous
application containing that matter. However, the subsection goes on to
provide an exception to that position, by which a second (or subsequent)
earlier application can be used to establish the priority date of that
matter. This exception only applies when certain specific conditions
have been met regarding the earlier applications.

### 5.28 {#section-17}

The specific conditions which must be met in order for the application
in suit to use the second (or subsequent) earlier application as the
basis for a priority claim are derived directly from Article 4C(4) Paris
Convention, and are as follows:

\(a\) the second application was filed in or for the same country as the
first application, and

\(b\) at the date of filing of the second application, the first
application had been unconditionally withdrawn, or abandoned or refused,
had not been published, and did not leave "any rights outstanding", and

\(c\) at the date of filing of the second application, the first
application had not been used for priority purposes for any other
application.

### 5.28.1 {#section-18}

Article 4C(4) goes on to make clear that, once these conditions have
been met, the first application cannot be used subsequently as a basis
for claiming priority. The effect of meeting these conditions is
therefore that the first application is entirely disregarded, and the
second application can legitimately be used as a basis for claiming
priority and as the starting point of the 12 month priority period ([see
5.18](#ref5-18)). Withdrawing the first application and filing a second
application while meeting these conditions is often referred to as
"regenerating the priority date".

### 5.28.2 {#section-19}

An applicant who files a second application and wants to use it for
priority purposes must therefore ensure that the first application has
not been published or used to form the basis of a priority claim. They
must also ensure that, at the time of filing the second application, the
first application has been withdrawn, abandoned or refused "without
leaving any rights outstanding". On an application that has been refused
by a decision of the comptroller, an outstanding right may be the right
to appeal that decision, or the right to request reinstatement of the
application under section 20A ([see 20A.02 to
20A.07](/guidance/manual-of-patent-practice-mopp/section-20a-reinstatement-of-applications/#ref20A-02)).
On an abandoned application (i.e. one that is treated as refused or
withdrawn), the right to request reinstatement under section 20A may
again be an outstanding right. On a withdrawn application, an
outstanding right may be the right to request correction of an erroneous
withdrawal under section 14(10) and section 117 ([see
14.209](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-209)
and
[117.22.1](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-22-1)).

### 5.28.3 {#section-20}

In order to regenerate the priority date successfully, the applicant
therefore needs to ensure that any such outstanding rights on the first
application have explicitly been given up at or before the date of
filing of the second application. For example, the applicant may make an
explicit statement, on withdrawing the first application, that the
withdrawal is done "without leaving any rights outstanding". Making such
a statement leaves open the possibility of filing a second application
later and using it as the priority application. However, the statement
would also seem to rule out the possibility of later making a request to
correct the withdrawal of the first application. Another example may be
where the first application has been treated as withdrawn. If the
applicant wishes to regenerate the priority date, they will need to make
clear (at or before filing the second application) that no rights are
being left outstanding on the first application. Again, such a statement
would seem to rule out the possibility of later making a request to
reinstate the first application.

### 5.28.4 {#section-21}

If the conditions for regenerating the priority date have not been met,
and the application in suit is filed too late to use the first
application as a basis for claiming priority ([see 5.18](#ref5-18)),
then the application in suit will not be able to use either the first or
the second application as a basis for claiming priority of the subject
matter in question. The effect is that the priority date of that subject
matter, contained in both the earlier applications, is the filing date
of the application in suit. If either of the earlier applications has
been published then that matter will also form part of the state of the
art with respect to the application in suit.

### 5.28.5 {#section-22}

Section 5(3) is only concerned with subject matter which appears in both
the first and second earlier applications. Therefore, it does not rule
out the possibility that the application in suit may legitimately claim
priority from both the earlier applications. For example, the two
earlier applications may contain different subject matter, all of which
is relevant to the application in suit. Alternatively, the two earlier
applications may share matter, but the second earlier application may
contain further matter relevant to the application in suit, which is not
present in the first application. The fact that the second application
contains matter which is also present in the first application does not
affect the legitimate use of the first application to establish the
priority date for this matter.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 5 (4)**
  The foregoing provisions of this section shall apply for determining the priority date of an invention for which a patent has been granted as they apply for determining the priority date of an invention to which an application for that patent relates.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 5.29 {#section-23}

The priority date of an invention in a specification is therefore
determined in the same way regardless of whether the specification forms
part of an application or of a granted patent.

  -----------------------------------------------------------------------
   

  **Section 5 (5)**

  In this section "relevant application" means any of the following
  applications which has a date of filing, namely -\
  (a) an application for a patent under this Act;\
  (aa) an application in or for a country (other than the United Kingdom)
  which is a member of the World Trade Organisation for protection in
  respect of an invention which, in accordance with the law of that
  country or a treaty or international obligation to which it is a party,
  is equivalent to an application for a patent under this Act;\
  (b) an application in or for a convention country (specified under
  section 90 below) for protection in respect of an invention or an
  application which, in accordance with the law of a convention country
  or a treaty or international convention to which a convention country
  is a party, is equivalent to an application for a patent under this
  Act.
  -----------------------------------------------------------------------

### 5.30 {#ref5-30}

s.78,89 is also relevant

Priority may be claimed only from a "relevant application", which must
be:

\(a\) an application for a patent under the Act, including a European or
International application designating the UK,

(aa) an application for protection for an invention filed in a country
which is a member of the World Trade Organisation (WTO), or

\(b\) an application for protection for an invention which was filed
either under the laws of a convention country ([see
90.02-90.03](/guidance/manual-of-patent-practice-mopp/section-90-orders-in-council-as-to-convention-countries/#ref90-02))
or under the PCT or EPC (designating a country other than the UK) or
some other international agreement to which a convention country is a
party.

s.130(7) is also relevant

What constitutes "protection for an invention" is not specified in the
Act; however s.5 is intended to have the same effect as the
corresponding provisions of the EPC, and it appears likely that the term
is restricted to the kinds of application referred to in Art.87(1) EPC,
which allows priority to be claimed only from an application for a
patent, for the registration of a utility model (e.g. German
Gebrauchsmuster), for a utility certificate or for an inventor's
certificate. During pre-grant proceedings in the Office no objection
should be raised to a claim to priority based on a US provisional patent
application (notice in Official Journal (Patents), 31 January 1996).
Such a provisional application is valid in the USA as a priority
document for a subsequent patent application and expires after one year.
Following a change in US law in 1999, a provisional application can be
converted into a full US patent application within one year of filing.
In [Adamson Jones IP Limited v American Isostatic Presses Inc
(BLO/00992/23)](https://www.ipo.gov.uk/p-challenge-decision-results/o009223.pdf){rel="external"}
the hearing officer accepted the claimant's argument that a US
provisional patent application was a relevant application for the
purposes of s.5(3). An application for a registered design is not a
relevant application ([Agfa-Gevaert AG's Application \[1982\] RPC
441](https://doi.org/10.1093/rpc/1982rpc441){rel="external"}).

  -------------------
   
  **Section 5 (6)**
  -------------------

### 5.31 {#section-24}

Section 5(6) was added by the Patents and Trade Marks (World Trade
Organisation) Regulations 1999 with effect from 29 July 1999, with the
aim of enabling WTO member countries to be automatically treated as
convention countries. However, an Order in Council was still needed to
declare such WTO member countries as convention countries.

### 5.32 {#section-25}

Section 5(5)(aa) was inserted and section 5(6) was repealed on 1 October
2014 by the Intellectual Property Act 2014, with the effect that from 1
October 2014 an application filed in or for a country which is a member
of the WTO is automatically treated as a "relevant application".
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
