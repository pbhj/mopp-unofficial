::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 64: Right to continue use begun before priority date {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (64.01 - 64.06) last update July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 64.01

Under this section, a person has rights to continue an act which they
did or prepared to do before the priority date of an invention and which
would otherwise be an infringement of a patent for the invention. The
recipients of products disposed of in exercise of such rights are
protected by subsection (3).

### 64.02

For the applicability of s.64 in relation to European patents, [see
60.02](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-02).

### 64.03

The wording of section 64 generally corresponds to that of subsections
(4) to (6) of section 28A, both being concerned with protecting the
rights of third parties who take steps which would have constituted
infringement of a patent if that patent had been in force at the time.

  -----------------------------------------------------------------------
   

  **Section 64(1)**

  Where a patent is granted for an invention, a person who in the United
  Kingdom before the priority date of the invention\
  \
  (a) does in good faith an act which would constitute an infringement of
  the patent if it were in force, or\
  \
  (b) makes in good faith effective and serious preparations to do such
  an act, has the right to continue to do the act or, as the case may be,
  to do the act, notwithstanding the grant of the patent; but this right
  does not extend to granting a licence to another person to do the act
  -----------------------------------------------------------------------

  ----------------------------------- -----------------------------------
                                       

  **Section 64(2)**                    

  If the act was done, or the         .
  preparations were made, in the      
  course of a business, the person    
  entitled to the right conferred by  
  subsection (1) may -\               
  \                                   
  (a) authorise the doing of that act 
  by any partners of his for the time 
  being in that business, and\        
  \                                   
  (b) assign that right, or transmit  
  it on death (or in the case of a    
  body corporate on its dissolution), 
  to any person who acquires that     
  part of the business in the course  
  of which the act was done or the    
  preparations were made              
  ----------------------------------- -----------------------------------

### 64.04 {#ref64-04}

The way in which a person acquires rights under this section is to
perform a potentially infringing act or make "effective and serious
preparations" to do so, before the priority date of the invention. The
priority date is determined for each individual invention in accordance
with s.5 and may differ for different matter in the same patent, [see
5.20 to
5.25](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-20).
The performance or preparations must be in the UK and "in good faith".
In Lubrizol Corporation v Esso Petroleum Co. Ltd. \[1998\] RPC 727 the
Court of Appeal affirmed (at page 770) that the protection afforded by
the section to the prior user is not strictly limited to acts identical
to those which were performed before the priority date but "cannot be a
right to manufacture any product, nor a right to expand into other
products". Jacob J's statement in the Patents Court was upheld that "if
the protected act has to be exactly the same (whatever that may mean) as
the prior art then the protection given by the section would be
illusory. The section is intended to give practical protection to enable
a man to continue in substance what he was doing before." In the event,
two customer trials by the defendant in the UK of small samples imported
from the US with a view to possible later manufacture in the UK but with
no decision yet made, were held, although serious, not to be "effective"
preparations to do an infringing act. Brooke LJ (at page 785) amplified
that it is not "sufficient to show that the serious preparations, if
pursued to finality, will have the requisite effect." In the Patents
Court (\[1997\] RPC 195), Jacob J also pointed out that in deciding
whether an activity is substantially the same as the prior act, both
technical and commercial matters must be taken into account, but there
should be no account taken of how the patentee may have chosen to cast
their monopoly. Furthermore, it was noted obiter that although the
decision was governed by Section 64 as it stood prior to amendment by
the CDP 1988, the amendment would not make any difference if it were
applicable.

### 64.05 {#section-3}

Although this section makes no distinction between public and secret
acts, if the act was public it might constitute prior disclosure of the
invention whereby it became part of the state of the art under s.2(2),
thus depriving the invention of novelty and impugning the validity of
the patent ([see 2.27 to
2.29](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-27)).
If the patent was thus invalid, infringement would not arise and a user
would not need the protection of s.64.

### 64.06 {#section-4}

The rights resulting from such an act, or preparations therefor, prior
to the priority date are that the person can continue to do, or do, that
act without infringing the patent in question. They cannot grant a
licence to any other person to do that act but, if the prior act or
preparations occurred in the course of a business, they can assign or
transmit the right to do it or authorise it to be done by a partner as
set out in subsection (2).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 64(3)**
  Where a product is disposed of to another in exercise of the rights conferred by subsection (1) or (2), that other and any person claiming through him may deal with the product in the same way as if it had been disposed of by the registered proprietor of the patent.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
