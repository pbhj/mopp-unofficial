::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 286: Interpretation {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (286.01 - 286.05) last updated April 2011.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 286.01

This defines certain terms used in Part V of the CDP Act (patent agents
and trade mark agents). This section was amended on 1 January 2010 by
section 208(1) and Schedule 21, paragraph 79 of the Legal Services Act
2007.

  -----------------------------------------------------------------------
   

  **Section 286**

  In this part "\
  \
  the comptroller" means the Comptroller-General of Patents, Designs and
  Trade Marks;\
  \
  "director", in relation to a body corporate whose affairs are managed
  by its members, means any member of the body corporate;\
  \
  "the European list" means the list of professional representatives
  maintained by the European Patent Office in pursuance of the European
  Patent Convention;\
  \
  "registered patent attorney" has the meaning given by section 275(2).
  -----------------------------------------------------------------------

### 286.02

The comptroller means the Comptroller-General of Patents, Designs and
Trade Marks. "Proceedings before the comptroller" therefore includes
proceedings in the Patent Office, the Trade Marks Registry and the
Designs Registry.

### 286.03

Director includes "member" in the case of a body corporate managed by
its members. The normal operation of company law means that a chief
executive or other most senior manager who is not a director would also
be deemed to be a director for the purposes of this Act.

### 286.04

The European list is defined to mean the list of persons qualified to
represent applicants before the European Patent Office. This list is
established under Article 134(1) of the European Patent Convention.

### 286.05

A registered patent attorney is a person whose name is entered in the
register of patent agents ([see
275.04](/guidance/manual-of-patent-practice-mopp/section-275-the-register-of-patent-attorneys/#ref275-04)).
The reference to registered trade mark attorney in section 286 was
repealed by Schedule 5 to the Trade Marks Act 1994. A registered trade
mark attorney is now a person whose name is entered on the register of
trade mark attorneys in accordance with s.83 of the Trade Marks Act
1994.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
