::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 102: Right of audience, etc., in proceedings before the comptroller {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (102.1 - 102.7) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 102.1

The CDP Act split the old section 102 (Right of audience in patent
proceedings) of the 1977 Act into two sections 102 (Right of audience,
etc., in proceedings before the comptroller) and 102A (Right of
audience, etc., in proceedings on appeal from the comptroller). Section
102 has been amended by section 208(1), schedule 21, paragraph 40 of the
Legal Services Act 2007. Section 102A, together with other provisions in
the CDP Act, has been repealed by section 210, schedule 23 of the Legal
Services Act 2007.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 102(1)**
  A party to proceedings before the comptroller under this Act, or under any treaty or international convention to which the United Kingdom is a party, may appear before the comptroller in person or be represented by any person whom he desires to represent him.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 102(2)**
  No offence is committed under the enactments relating to the preparation of documents by persons not legally qualified by reason only of the preparation by any person of a document, other than a deed, for use in such proceedings.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 102(2A)**
  For the purposes of subsection (2), as it has effect in relation to England and Wales, "the enactment relating to the preparation of documents by persons not qualified" means section 14 of the Legal Services Act 2007 (offence to carry on a reserved legal activity if not entitled) as it applies in relation to an activity which amounts to the carrying on of reserved instrument activities within the meaning of that Act.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 102(3)**
  Subsection (1) has effect subject to rules made under section 281 of the Copyright, Designs and Patents Act 1988 (power of comptroller to refuse to recognise certain agents).
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 102.2

A party to patent proceedings before the comptroller may appear in
person or be represented by any person of their choice, subject to rules
empowering the comptroller to refuse to recognise certain agents. This
applies to any proceedings before the comptroller under the 1977 Act or
any relevant treaty etc.

### 102.3

The rules in question are The Patent Agents (Non-recognition of Certain
Agents by Comptroller) Rules 1990 (SI 1990 No 1454) as amended by the
Legal Services Act 2007 (Consequential Amendments) Order 2009 (SI 2009
No 3348) which authorise the comptroller to refuse to recognise as an
agent -

a\. a person who has been convicted of an offence under s.88 of the
Patents Act 1949, s.114 of the Patents Act 1977 or s.276 of the CDP Act;

b\. a person whose name has been erased from and not restored to the
register on the ground of misconduct;

c\. a person who is found by the Secretary of State to have been guilty
of such conduct as would, in the case of a person registered in the
register, render the person liable to have the person's name erased from
the register on the ground of misconduct;

d\. a partnership or body corporate of which one of the partners or
directors is a person whom the comptroller could refuse to recognise
under paragraph (a), (b) or (c) above.

The "register" in the above is the register of patent attorneys required
to be kept under s.275 of the CDP Act.

### 102.4

Subsection (2) protects any person who prepares documents (other than
deeds) for use in such proceedings, but is not legally qualified. It
extends to other such persons the protection previously afforded to
patent agents by section 114(6) of the 1977 Act which the CDP Act
repealed.

### 102.5

Subsection (2A) ensures the reference to "the preparation of documents
by persons not legally qualified" in subsection (2) refers to section 14
of the Legal Services Act 2007 (Offence to carry on a reserved legal
activity if not entitled) insofar as it has effect in England and Wales.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 102(4)**
  In its application to proceedings in relation to application for, or otherwise in connection with, European patents, this section has effect subject to any restrictions imposed by or under the European Patent Convention.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 102.6

The effect of section 102 on proceedings concerning European patents is
made subject to any restrictions under the European Patent Convention by
subsection (4). Such restrictions relate to the need for representatives
to be on the "European list" maintained by the European Patent Office.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 102(5)**
  Nothing in this section is to be taken to limit any entitlement to prepare deeds conferred on a registered patent attorney by virtue of the Legal Services Act 2007.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 102.7

Sub-section (5) was added by the Courts and Legal Services Act 1990,
s.125(3), Sch 18, para 20 and subsequently amended by the Legal Services
Act 2007, s.208(1), Sch 21, para 40.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
