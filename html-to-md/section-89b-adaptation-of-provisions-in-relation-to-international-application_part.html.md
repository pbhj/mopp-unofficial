::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 89B: Adaptation of provisions in relation to international application {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (89B.01 - 89B.18) updated January 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 89B.01 {#b01}

This is the third and last section concerned specifically with
international applications for patents (UK), and was introduced by the
CDP Act. It modifies the requirements to be met by the application under
the 1977 Act in view of its status as an international application.
Section 89 makes provision for an international application for a patent
(UK) to be treated as an application under the 1977 Act, subject to
sections 89A and 89B ([see
89.01-05](/guidance/manual-of-patent-practice-mopp/section-89-effect-of-international-application-for-patent/#ref89-01)).

  -----------------------------------------------------------------------
   

  **Section 89B(1)**

  Where an international application for a patent (UK) is accorded a
  filing date under the Patent Co-operation Treaty -\
  \
  (a) that date, or if the application is re-dated under the Treaty to a
  later date that later date, shall be treated as the date of filing the
  application under this Act,\
  \
  (b) any declaration of priority made under the Treaty shall be treated
  as made under section 5(2) above, and where in accordance with the
  Treaty any extra days are allowed, the period of 12 months allowed
  under section 5(2A) above shall be treated as altered accordingly, and\
  \
  (c) any statement of the name of the inventor under the Treaty shall be
  treated as a statement filed under section 13(2) above.
  -----------------------------------------------------------------------

### 89B.02 {#ref89B-02}

r.3 is also relevant

Subsection (1) provides for various acts (concerning filing, priority
and naming of the inventor) done under the PCT to be treated as having
been done under the 1977 Act. With regard to subsection (1)(b), the
declaration of priority must have been made under Article 8 of the PCT,
must not have been lost or abandoned and must fulfil certain
requirements of the PCT Rules (see 5.10). Note that if a priority right
was restored by a another receiving office under the PCT ''due care
criterion'', no new request needs to be filed with the IPO following
entry into the GB national phase as the office will treat the period of
12 months allowed under section 5(2A) as altered accordingly. With
regards to whether, under subsection (1)(c), information concerning the
inventor given under Article 4 of the PCT obviates the need for filing
of Form 7, [see
13.12.1](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-12-1),
[89A.12](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-12)
and
[89A.18](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-18).

\[ See section 117 for the procedure for correcting an error made by the
receiving office under the PCT whereby an international application for
a patent (UK) has been accorded an incorrect date of filing, or where
the declaration made under Article 8(1) of the PCT (ie claiming
priority) has been considered not to have been made by the receiving
office or by the International Bureau because of an error made by that
office or Bureau. Alternatively, rule 108 may be applicable.\]

### 89B.03 {#b03}

r.25(3) is also relevant

Certain of the UK national formal requirements concerning the form and
manner of presentation of the drawings and other documents included in
an international application, as well as the filing of a request for
grant, are treated as having been complied with provided that the
corresponding provisions of the PCT Regulations have been fulfilled
([see
15A.10](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-10)).
However, any translations which are filed at this Office by the
applicant and are to be included in the re-published application ([see
89A.14.2](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-14-2))
(including drawings re-executed because the originals contained textual
matter in a foreign language) must comply with the UK national rules,
especially rule 14. All amendments filed after entry into the national
phase are subject to the UK national rules. ([see
19.15.1](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-15-1)
for voluntary amendments filed after entry into the national phase and
before issue of the first examination report).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 89B(2)**
  If the application, not having been published under this Act, is published in accordance with the Treaty it shall be treated, for purposes other than those mentioned in subsection (3), as published under section 16 above when the national phase of the application begins or, if later, when published in accordance with the Treaty.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 89B.04 {#ref89B-04}

r.66(1) is also relevant

Subsection (2) provides for publication under the PCT to be treated for
most purposes as publication under s.16 of the 1977 Act when the
conditions for entering the national phase (as set out in subsection (3)
of section 89A) are met. An international application published under
the PCT thus becomes part of the state of the art under s.2(3) upon
entering the national phase (whether this occurs on expiry of the
prescribed 31 month period ([see
89A.05](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application#ref89A-05))
or occurs early at the applicant's request ([see 89A.16 to
89A.21](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-16)).
The file and register entry become open to public inspection at the same
time. There is then no need for the specification to be republished in
full in the UK, since publication under s.16 is deemed to have already
taken place. Instead, the administrative act of re-publication takes
place ([see
89A.14.2](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-14-2)).
If an application has not been published under the PCT when it enters
the national phase, the application is not treated as published under
section 16 until it is either published under the PCT or published in
full under s.16 following a request from the applicant for accelerated
publication upon early entry to the national phase ([see
89A.20.1](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-20-1)).

  -----------------------------------------------------------------------
   

  **Section 89B(3)**

  For the purposes of section 55 (use of invention for service of the
  Crown) and section 69 (infringement of rights conferred by publication)
  the application, not having been published under this Act, shall be
  treated as published under section 16 above -\
  \
  (a) if it is published in accordance with the Treaty in English, on its
  being so published; and\
  \
  (b) if it is so published in a language other than English\
  \
  (i) on the publication of a translation of the application in
  accordance with section 89A(6) above, or (ii) on the service by the
  applicant of a translation into English of the specification of the
  application on the government department concerned or, as the case may
  be, on the person committing the infringing act.\
  \
  The reference in paragraph (b) (ii) to the service of a translation on
  a government department or other person is to its being sent by post or
  delivered to that department or person.
  -----------------------------------------------------------------------

### 89B.05 {#b05}

The purposes excepted under subsection (2) are those mentioned in
subsection (3). The latter relates to the provision of the text of the
application in English in order to secure certain rights regarding Crown
use and infringement, and is substantially a continuation of the
provisions of the old section 89(7). For the purposes of sections 55 and
69, the publication of the international application by the
International Bureau is, if it is in English, considered to constitute
publication under section 16. If in any other language, it is not
treated as published under section 16 for the purposes of sections 55
and 69 until a translation of the application under section 89A(6) is
published ([see
89A.26](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-26)).
It is therefore necessary to file a written request with the prescribed
fee that publication for the purposes of s.89A(6) is sought in order to
obtain the rights in question in this way. These rights may also be
obtained in relation to use or acts done before publication of that
translation from the date on which the applicant sends a translation to
the government department or potential infringer in question under
section 55 or 69 respectively.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 89B(4)**
  During the international phase of the application, section 8 above does not apply (determination of questions of entitlement in relation to application under this Act) and section 12 above (determination of entitlement in relation to foreign and convention patents) applies notwithstanding the application; but after the end of the international phase, section 8 applies and section 12 does not.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 89B.06 {#b06}

Subsection (4) sets out which sections of the 1977 Act apply to any
disputes about entitlement to the application during and after the
international phase. Prior to entry into the national phase, questions
about entitlement to the international application may be determined
under s.12. Upon entry, s.12 ceases to be applicable and such questions
become subject to ss.8 to 11.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 89B(5)**
  When the national phase begins the comptroller shall refer the application for so much of the examination and search under sections 15A, 17 and 18 above as he considers appropriate in view of any examination or search carried out under the Treaty.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 89B.07 {#ref89B-07}

Subsection (5) provides for preliminary examination, search and
substantive examination of the application after it enters the national
phase.

### 89B.07.1 {#ref89B-07-1}

s\. 13(2), s. 89B(5) and r.25 is also relevant

Rule 25(1) designates certain requirements of the rules as formal
requirements. Under rule 25(3), an international application is treated
as complying with these formal requirements to the extent that it
complies with any corresponding provisions of the PCT. However,
preliminary examination under section 15A covers not just the formal
requirements but also the requirements under section 13(2) \[in respect
of the inventor(s)\] and under section 15(10) (see 15.50). Section
89B(5) permits an examiner to determine, during the national phase,
whether these requirements are met (see Thaler's application
[O/447/22](https://www.ipo.gov.uk/p-challenge-decision-results/o44722.pdf){rel="external"}).

### 89B.08 {#ref89B-08}

r.106(2)(a) is also relevant

Filing of a Form 9A with the appropriate fee, including any excess
claims fees, is required. There is a lower fee for a search under
section 17(1) where the application has already been the subject of a
search by the International Searching Authority before the request for
search is made. If the International Search Authority has issued a
declaration under article 17(2) of the PCT that no international search
has been established, the application is considered to have been
"subject of a search" and the lower fee applies. If neither an
international search nor a declaration that no international search has
been established has been issued by the date of request for search, the
full fee is payable (see also
[89A.19-20.1](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-19)).
If the lower fee has been paid apparently without justification, the
applicant should be contacted to establish whether or not a search has
been performed. If not, the full fee should be requested. In the event
that an international search report becomes available before the
examiner commences the search, then the examiner should use the
information from the international search report and arrange for the
excess fee to be refunded. Similarly, if in other circumstances the
larger fee has been paid unnecessarily, the excess should be refunded
before the application is forwarded to the examiner. These are the only
situations in which rule 106(2)(a) can be used to provide a refund,
under no circumstances can rule 106(2)(a) be used to refund the reduced
search fee. If the application is withdrawn prior to substantive
examination, the whole of the search fee may be refunded unless it has
been necessary to perform a search within the Office. If an
international search report has been established on the application, but
the claims are amended so that a supplementary search is necessary under
s.17(8), a full supplementary search fee in addition to the reduced
search fee is payable -- no refund of the latter can be given.

\[At the examination stage, where the examiner performs a search beyond
the topup search (see 89B.12), whether or not a full search fee has been
paid, there is no requirement to produce an external search report on
the application. Any new citations found should be included in the
substantive examination report issued to the applicant. For procedures
to be followed when a full search is to be carried out, [see
89A.18-21](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-18).\]

### 89B.09 {#ref89B-09}

r.68(4) is also relevant

Patents Form 10 must be filed together with the appropriate fee,
including any excess claims fees, within 33 months of the priority date
or, if none, the filing date of the application, or within two months of
the date on which the national phase begins, whichever expires the
later. This period may be extended in two month tranches, but any
extension is not available after the end of the period of two months
beginning immediately after the expiry of the period to be extended, in
accordance with r.108(2) or (3) and (4) to (7) - [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).

### Substantive examination

PCT r.44bis. 1(a) are also relevant

### 89B.10 {#ref89B-10}

When Form 10 has been filed, and after re-publication or publication
under s.16, r.44bis.1(a) the application is referred to the appropriate
examining group, where it ranks with national applications of the same
priority date, for its substantive examination. If the application is
withdrawn prior to substantive examination, the examination fee may be
refunded. The examination process is much like that for any other UK
application (see section 18) except for the regard paid to the results
of the International Search Report, any Supplementary International
Search Reports and the International Preliminary Report on Patentability
(IPRP). The examiner should check
[Patentscope](https://patentscope.wipo.int/search/en/search.jsf){rel="external"}
for the presence of an International Search Report, any Supplementary
International Search Reports and an International Preliminary Report on
Patentability (IPRP), where these have not already been received from
the International Bureau and imported onto the PDAX dossier. If such
documents are found, a minute should be added to the PDAX dossier, with
an appropriate title (e.g. EXR IPRP), detailing what the document is,
along with when and where it was found. A Supplementary International
Search Report will only be present if the applicant requested such a
search in the international phase ([see
89.03](/guidance/manual-of-patent-practice-mopp/section-89-effect-of-international-application-for-patent/#ref89-03)).
It should be noted that the "written opinion" issued to the applicant on
every PCT application has the same contents as the IPRP if the applicant
has not requested international preliminary examination (IPE) under
Chapter II of the PCT. If IPE has been requested, the written opinion
will have been superseded by an IPRP issued under Chapter II. There
should therefore be no need to check the contents of the written opinion
as long as the IPRP has been referred to. If accelerated processing
under the PCT(UK) Fast Track is requested, the procedure described in
[89B.17](#ref89B-17) should be followed.

\[ The application should be checked to confirm that re-publication has
in fact taken place. Applications not re-published should be referred to
Publishing Section (although if examination is due or overdue, sending
for re-publication may be deferred until after examination).

The examination document will comprise the documents in the TOC with the
codes DESC, CLMS and DRWG, each annotated as the "working copy", which
will have been assembled by Formalities.

\[Deleted\]

Care should be taken in the case of a translation that the translation
of the application and not that of the priority document is examined.
Serious errors discovered in PCT applications should be brought to the
attention of the appropriate legal adviser in the Registered Rights
Legal Team, who will liaise with the International Bureau if necessary.
Other errors should be ignored. Where the wrong abstract has been
published in the international phase, [see
89A.14.2](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-14-2).
\]

### 89B.11 {#ref89B-11}

r.113(5), (6) are also relevant

The International Search Report (and any Supplementary International
Search Reports) should be inspected before examination becomes due and
copies of cited documents ordered. If there are a large number of
citations, the case may be referred to the examiner to confirm whether
all of them are needed. A translation of the search report, when not in
English, is available. Where a cited document is not in the English
language, an equivalent which is in English should be obtained if
available. Rule 113(5) allows the comptroller to request a translation
of any document or part thereof which is in a language other than
English and is referred to in an International Search Report or
International Preliminary Report on Patentability, or cited in an
International Preliminary Examination Report of the application. If the
family of any document listed in the International Search Report as X or
Y category does not include an English language equivalent, the
application should be referred to the examiner prior to examination to
determine whether they wish to make such a request. The examiner should
bear in mind that translation costs can be a significant burden to the
applicant and should not request a translation unless they believe that
they will not otherwise easily be able to determine the relevance of the
document to the patentability of the invention. The examiner may call
for a partial translation, eg only of the claims, if that is felt likely
to be sufficient. A fuller translation may be requested at a later stage
if it is found to be necessary.

\[Where the International Search Report includes a list of family
members this can normally be relied on to identify any English language
equivalents of cited documents. If there is no list of family members
provided, they should be identified by using EPOQUE. In the case of
recently published citations, the family listing by either of these
routes may not be complete and it may be advisable to make a further
inquiry when conducting the substantive examination if an English
language equivalent was not identified earlier. \]

\[Letter EL6 should be used for requesting a translation. \]

### 89B.12 {#ref89B-12}

The substantive examiner should consider the International Search Report
(and any Supplementary International Search Reports) and should
re-search the application only if they are reasonably sure that such a
search will yield more pertinent art. Circumstances where re-searching
might be needed include: if the examiner is already aware of documents
that would be relevant (e.g. from an earlier case), or if a particularly
relevant classification area has been created in the interim or
otherwise missed in the international search. Such re-searching should
not be done on a purely speculative basis -- i.e. if the examiner is
surprised that no relevant documents have been found. Otherwise, their
searching will be restricted to the "top-up" search (if necessary in
light of any top-up search performed in the international phase -- [see
89B.12.1](#ref89B-12-1)) and any supplementary search under s.17(8)
necessitated by allowable amendment of the claims, as with national
applications ([see
17.120-17.123](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-120)).

### 89B.12.1 {#ref89B-12-1}

Where international preliminary examination under Chapter II of the PCT
has been requested, and the request was made after 1 July 2014, a top-up
search is normally performed as part of the international preliminary
examination process. The International Preliminary Report on
Patentability (IPRP) indicates whether a top-up search has been
performed and includes any documents identified in this search that are
relevant to patentability; a separate search report will not be issued
by the International Preliminary Examining Authority. Where such a
search has been performed, the examiner is not required to perform any
further searching in the national phase, unless the top-up search was
performed before 21 months from the priority date (in which case, a
standard top-up search should be performed), or the circumstances in
[89B.12](#ref89B-12) apply.

\[All documents cited in the PCT top-up search should be recorded in
PROSE by adding them to the Citations list, even if they are not relied
upon in the examination report, as per
[18.85](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-85).\]

### 89B.12.2 {#b122}

Where the International Searching Authority (ISA) has issued a
declaration under article 17(2) of the PCT that no International Search
Report has been established, but the substantive examiner considers that
the claims relate to a patentable invention, a full search should be
performed but no additional fee may be requested from the applicant. If
the substantive examiner disagrees with the ISA's assessment of
plurality, and as a result determines that further searching is needed
(bearing in mind 89B.12), this should be carried out without requiring
an additional fee. Where amendments have been made during the
international or national phase which necessitate a supplementary search
under s.17(8), a full supplementary search fee -- in addition to the
reduced search fee payable on entry to the national phase for all
international applications with an International Search Report (see
89B.08) -- is payable under ss.17(8), 19(1) and 89A(5).

### 89B.12.3 {#b123}

If any searching has been performed, no search report should be issued
to the applicant; if additional citations are found, these should be
brought to the applicant's attention on the examination report. (This
does not apply to a supplementary search under s.17(8), or to a UK
application divided from an international application, for which the
preparation and issue of a search report is necessary, see 15.38 and
17.121). Copies of additional documents cited by the UK examiner are
issued to the applicant, but not copies of the documents cited in the
international search report or the IPRP ([see 17.104.1,
17.105.2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-104-1)).

\[The examiner should record details of the top-up search in an internal
search report. If no top-up searching is necessary, this should be
recorded in the report instead. Otherwise, the report should include a
search statement and details of subclasses searched additional to the
international search report. Where only topup searching is done then
there are no "additional fields of search". However, where a full
further search is conducted in areas not corresponding to areas covered
by the international search then they should be recorded as "additional
fields of search" for inclusion on the front page of the B document. \]

### 89B.13 {#ref89B-13}

PCT r.44bis.2 and PCT r.94.2 are also relevant

Where the UK has been elected under Chapter II of the PCT, a copy of the
International Preliminary Report on Patentability (IPRP) issued under
Chapter II together with any annexes (amendments) is communicated to the
United Kingdom Office by the International Bureau. Similarly, where no
election under Chapter II is made, a copy of the International
Preliminary Report on Patentability (IPRP) issued under Chapter I is
communicated to the United Kingdom Office by the International Bureau.
The IPRP is either prepared in English or a translation into English is
made available. If the report or translation into English is not on
file, the examiner should check
[Patentscope](https://patentscope.wipo.int/search/en/search.jsf){rel="external"}
for its presence. If such documents are found, a minute should be added
to the PDAX dossier, with an appropriate title (e.g. EXR IPRP),
detailing what the document is, along with when and where it was found.
If the IPRP was issued under Chapter II, the examiner should check for
the presence of amendments annexed to it and a minute should be sent to
formalities asking them to import the IPRP (Chapter II) and any
amendments into the dossier. If the document in question is not
available via
[Patentscope](https://patentscope.wipo.int/search/en/search.jsf){rel="external"},
the examiner should ask Formalities to obtain it so that the report, and
any amendments annexed to it, can be taken into account in the
substantive examination (see 89B.15). (On the rare occasion that
Formalities are unable to obtain the document, they should refer the
request to the National Phase Officer so that the report can be ordered
from WIPO). An EPO online file inspection could also be considered to
view missing documents if the application has an equivalent going
through the European regional phase. As a final resort, the agent for
the applicant may be approached to obtain a missing report. In
exceptional cases, the examiner may consider that it is necessary to see
other documents, such as written opinions, kept on file at the
international authority. The National Phase Officer should be asked to
obtain a copy of the document(s) in question.

\[The copy of the IPRP communicated by the International Bureau is added
to the dossier and set as "open to public inspection". Any other
documents obtained from the IPEA at the examiner's request are also
added to the dossier and set as "open to public inspection". \]

### 89B.14 {#b14}

Substantive examination of a PCT application after an IPRP is based on
the PCT pamphlet as published (which may include new or amended claims)
and as amended during the international phase and/or after entry into
the national phase. Any amendments made during the international phase
which have been taken into account in the IPRP will be attached as
"annexes" to the IPRP. Under section 89A(5) amendments during the
international phase are normally to be treated as if made under the Act
provided that, if the amendments are not in English, the applicant has
filed a translation ([see
89A.25](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-25)).
If such a translation is not filed then those amendments, made under the
PCT, should be "disregarded" as described in
[89A.25](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-25)
unless subsequently re-submitted under the Act. The first report under
section 18 should indicate whether the international application
examined has been amended since publication and if so which pages, so
that the applicant can check whether the Office has the correct form of
the specification.

\[ If the specification has been amended during the international phase,
copies of replacement pages embodying these amendments in their latest
form are incorporated in the examination document by the formalities
examiner before the case is forwarded to the substantive examiner (see
under [89B.10](#ref89B-10)). The substantive examiner should check that
they are examining the latest version of the application, taking into
account any amendments made during the international phase. If the
substantive examiner notices that amendments made during the
international phase have not been incorporated into the specification, a
minute should be sent to formalities asking them to do this (as long as
the required translation has been filed if the amendments are not in
English). RC8A, RC8B and RC8C respectively should be used to indicate
that an international application has not been, or has been, amended
since publication; where the amendments are annexed to the IPRP, the
date of the letter for RC8B is that given in the report. If amendment
has occurred the new pages with their filing dates should be listed. \]

### 89B.14.1 {#b141}

Where a number of conflicting amendments are on file, the latest to be
filed are considered unless they are to be disregarded ([see
89A.25](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-25)).
The date of filing of any amendments is stated in the IPRP; if
subsequently filed amendments or statements make the applicant's
intentions appear uncertain, then the matter should be cleared up before
examination begins.

### 89B.15 {#ref89B-15}

Since the question of patentability will have been considered by the
international examiner in the light of the international search report,
any amendments filed during the international phase are probably a
response to one or more written opinions of the international phase
examiner. The IPRP ([see 89B.13](#ref89B-13)) contains an explicit
statement for each claim examined in respect of novelty, inventive step
and industrial applicability, and may also include observations on
plurality of invention, clarity, support, and whether any amendment adds
matter. It should be noted that documents which appear to be included in
the state of the art as defined in section 2(3) are not part of the art
for the purposes of any international opinion or preliminary examination
on patentability: however such documents can be listed in the IPRP under
the heading 'certain published documents'. The IPRP is not binding, and
the final responsibility for determining whether the application
complies with the Act and Rules belongs to the UK examiner, who should,
however, derive as much assistance as possible from the report to reduce
the work of substantive examination and to avoid going over the same
ground as the international phase examiner. For example, the claims
should be considered first in the light of any citations and
explanations in the IPRP and in the written opinions, if available.
Similarly, other issues should be approached on the basis of any
relevant observations in the IPRP. Where a report is issued under
s.18(3) for the first time, the examiner should include a paragraph
indicating that the IPRP has been considered. On no account, however,
should an IPRP objection be repeated unless the examiner is satisfied it
is proper under the UK law or practice.

### 89B.15.1 {#ref89B-15-1}

If the IPRP raises major objections, and the applicant has not responded
to these either by amendment or argument, the examiner should consider
whether to issue an abbreviated examination report (AER) as the first
report under s.18(3).

### 89B.15.2 {#b152}

Consideration should be given to observations or objections on unity of
invention in the international search report or IPRP, but as for other
substantive issues, the examiner should raise an objection under
s.14(5)(d) if it is considered justified under UK practice. The need for
search of a second or later invention should then be considered in
accordance with [89B.12](#ref89B-12), and in the light of the claims
actually covered by the international search report.

#### Third party observations

### 89B.16 {#b16}

Where third party observations have been filed in the international
phase, the substantive examiner should consider them in the same way as
they would third party observations filed on a domestic application
([see 21.12 and
21.21](/guidance/manual-of-patent-practice-mopp/section-21-observations-by-third-party-on-patentability/#ref21-12)).
However, there is no need to acknowledge the third party observations or
bring them to the attention of the applicant, since this will have been
done in the international phase. Any comments filed by the applicant in
the international phase in response to the observations should also be
considered. Depending on when they were received, the observations may
have been considered by the International Searching Authority (ISA)
and/or the International Preliminary Examining Authority (IPEA). When
checking the International Preliminary Report on Patentability (IPRP) as
part of the routine examination procedure, the substantive examiner
should consider any comments regarding the observations and related
prior art, but ultimately should come to their own view on their
relevance. Where a PCT application enters the national phase early,
third party observations may be received by the International Bureau
(IB) and appear on Patentscope later, after the application has been
received by the examiner. In such cases the examiner should consider any
such observations they discover as part of their usual checks of
Patentscope during the examination process.

\[The National Phase Officer should have imported any third party
observations and response documents from the applicant into the PDAX
dossier and ordered copies of any non-patent literature referred to in
the observations. However, if the substantive examiner comes across any
third party observations and/or related comments on Patentscope that are
not present on the PDAX dossier, they should import them to the dossier
or create a minute giving details of the documents.\]

\[Just as for domestic applications, third party observations filed in
the international phase are made available on IPSUM along with any
response documents from the applicant. The formalities examiner should
skim all documents for any personal information and redact if necessary,
adding the OLFI annotation to the redacted versions as usual. It should
not be necessary to check for libellous or offensive material as the IB
carries out such a check before publishing the observations on
Patentscope. However, if the formalities or substantive examiner notices
any such material that has been missed by the IB, it should be redacted
as it would be for observations received under s.21 of the Act ([see
21.06](/guidance/manual-of-patent-practice-mopp/section-21-observations-by-third-party-on-patentability/#ref21-06)).\]

Accelerated processing -- PCT(UK) Fast Track

### 89B.17 {#ref89B-17}

As set out in the [Practice
Notice](https://www.gov.uk/government/publications/practice-notice-pctuk-fast-track/relaxation-of-requirements-for-pctuk-fast-track)
issued on 8 June 2012, patent applicants can request accelerated
examination in the UK national phase through the PCT(UK) Fast Track if
their PCT application has received a positive International Preliminary
Report on Patentability (IPRP) in respect of at least one claim, under
either PCT Chapter I or Chapter II. If the IPRP has not yet been issued,
a positive Written Opinion of the International Searching Authority
(WO-ISA) is also acceptable, since the WO-ISA will later be re-issued as
the IPRP under PCT Chapter I. In order to qualify for this service, all
claims present in the application at the time of the request must
sufficiently correspond to one or more claims that have been examined in
the IPRP or WO-ISA and found to meet the requirements for novelty,
inventive step and industrial applicability. The claim correspondence
requirements are the same as those applied under the Office's Patent
Prosecution Highway (PPH) agreements. Claims are considered to
"sufficiently correspond" where, accounting for differences due to
translations and claim format, the claims on file in the UK national
phase are:

\(i\) of the same or similar scope as the claims found acceptable in the
IPRP or WOISA, or

\(ii\) narrower in scope than the claims found acceptable in the IPRP or
WO-ISA.

With regard to (ii), a claim that is narrower in scope occurs when a
claim found to be acceptable in the IPRP or WO-ISA is amended to be
further limited by an additional feature that is supported in the
specification (description and/or claims). A claim in the UK national
phase which is in a new or different category from those claims
indicated as acceptable in the IPRP or WO-ISA is not considered to
"sufficiently correspond". For example, where the acceptable claims
relate to a process of manufacturing a product, then the claims in the
UK national phase are not considered to sufficiently correspond if they
introduce product claims that are dependent on the corresponding process
claims.

To request accelerated treatment, the applicant must make a request in
writing before UK examination has commenced, indicating that the claims
currently on file sufficiently correspond to one or more claims that
were indicated as acceptable in the IPRP or WOISA; no further reasons
for acceleration are needed. Though it should be noted that accelerated
examination cannot take place until the international search has been
performed or the applicant pays the additional search fee and a search
is performed ([see 89B.08](#ref89B-08)). If the request for accelerated
treatment is accepted, the usual procedure for accelerated examination
is followed ([see
18.07-18.07.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07)).
As with all PCT applications, the examiner is not bound by the IPRP or
WO-ISA and may come to their own conclusion in respect of novelty,
inventive step or industrial applicability. Other matters will require
full consideration, taking into account any observations which have been
made in the IPRP or WO-ISA. For restrictions on the earliest date a
s.18(4) report may be issued, [see
18.07.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07-2).

\[The Formalities Examiner should check all requests for accelerated
processing and on identifying a PCT(UK) Fast Track request should apply
the "PCT(UK) Fast Track" label to the PDAX dossier cover, in addition to
the appropriate Acceleration label(s).\]

\[Where the IPRP (and any associated amendments) or any other necessary
documents are not already available on the dossier, the formalities
examiner should obtain them using
[Patentscope](http://patentscope.wipo.int/search/en/search.jsf){rel="external"}
and import them into the dossier. If there is no IPRP, but there is a
Written Opinion, then this should be imported into the dossier. Where
the documents are not available on
[Patentscope](http://patentscope.wipo.int/search/en/search.jsf){rel="external"},
the Formalities Examiner should ask the National Phase Officer to
request the IPRP or Written Opinion and any amendments from the
International Bureau. If the documents are still unavailable they should
be requested from the applicant/attorney.\]

\[If the application has not been published in the international phase
(i.e. because the application is entering the national phase early) and
the applicant has requested accelerated publication, the practice set
out in
[89A.20.1](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-20-1)
should be followed.\]

\[The examiner should take a prima facie view as to whether all claims
present in the application sufficiently correspond to one or more claims
that have been examined and found to meet the requirements for novelty,
inventive step and industrial applicability in the IPRP or WO-ISA. If
Box III of the IPRP indicates that certain claims have not been
examined, the request for acceleration may only be accepted if those
claims have been deleted. However, where the ISA has made no attempt to
assess the industrial applicability of certain claims because they
consider them to relate to a method of treatment or diagnosis, but the
IPRP or WOISA is positive in respect of the novelty and inventive step
of those claims, this can be treated as an exception and the request for
acceleration should be allowed if the claims in the application
sufficiently correspond to those claims. If there are any objections in
Box V to novelty, inventive step or industrial applicability, the
request may only be accepted if those claims have been deleted or
amended such that they sufficiently correspond to allowable claims.
Where the claims have been narrowed in scope, there is no need for the
examiner to check whether the limitations to the claims are supported by
the specification when considering the request for acceleration; the
request may be accepted as long as the examiner is satisfied that the
claims are narrower that those found acceptable in the IPRP or WO-ISA.
However, a full assessment of support should be performed at substantive
examination. If it is not clear from the request how the claims
correspond to those indicated as acceptable in the IPRP or WO-ISA, the
examiner may contact the applicant to request further clarification.
Outstanding matters raised in the IPRP in relation to other issues, such
as clarity, support or formal requirements, do not need to have been
overcome for acceleration to be allowed.\]

\[If the application is in the publication or republication cycle an
examination report can still issue -- [see
18.64](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-64)
square brackets\].

\[If the request is refused, the examiner should write to the applicant
explaining the reasons for the refusal (PROSE letter EL29 may be
adjusted for this purpose) and remove the accelerated examination label
from the PDAX dossier, but leave the PCT(UK) Fast TRACK label active\]

### Grant

### 89B.18 {#b18}

r.30 is also relevant

The process of examination and re-examination in response to the agent's
or applicant's replies continues in the same way as for any other UK
application until, if the examiner is satisfied that the application
complies with all the requirements of the Act within the appropriate
period, the grant of a patent follows. That period (and permissible
extensions of the period) is as set out in
[20.02](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)
except that any reference to the priority date or date of filing is
taken to refer to the priority date or date of filing of the PCT
application [see 89B.02](#ref89B-02). For restrictions on the earliest
date a s.18(4) report may be issued, including allowing time for third
party observations, see
[18.07.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07-2).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
