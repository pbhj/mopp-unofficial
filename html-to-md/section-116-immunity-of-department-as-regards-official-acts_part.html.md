::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 116: Immunity of department as regards official acts {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (116.01) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------
   

  **Section 116**

  Neither the Secretary of State nor any officer of his\
  (a) shall be taken to warrant the validity of any patent granted under
  this Act or any treaty or international convention to which the United
  Kingdom is a party; or\
  (b) shall incur any liability by reason of or in connection with any
  examination or investigation required or authorised by this Act or any
  such treaty or convention, or any report or other proceedings
  consequent on any such examination or investigation.
  -----------------------------------------------------------------------

### 116.01

This section protects the Secretary of State and their officers,
including officials of the Patent Office, from any liability arising
from official acts connected with examination or investigation under the
1977 Act or the EPC or PCT. It also provides that no warranty is given
in respect of the validity of patents, including 1977 Act patents and
European patents.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
