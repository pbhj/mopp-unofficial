::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 88A: Implementation of Agreement on a Unified Patent Court {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (88A.01 - 88A.05) last updated: January 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 88A.01 {#a01}

Section 88A was introduced by the Intellectual Property Act 2014 and
came into force on 1 October 2014. The section provides a general power
to amend any legislation, including the Act itself, in order to bring
the Agreement on a Unified Patent Court into effect. It also allows any
provisions of the Act to be aligned with any provisions in the
Agreement.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 88A(1)**
  The Secretary of State may by order make provision for giving effect in the United Kingdom to the provisions of the Agreement on a Unified Patent Court made in Brussels on 19 February 2013.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 88A(2)**

  In order under this section may, in particular, make provision-\
  (a) to confer jurisdiction on a court, remove jurisdiction from a court
  or vary the jurisdiction of a court;\
  (b) to require the payment of fees.
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 88A(3)**
  An order under this section may also make provision for varying the application of specified provisions of this Act so that they correspond to provision made by the Agreement.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 88A(4)**

  An order under this section may-\
  (a) make provision which applies generally or in relation only to
  specified cases;\
  (b) make different provision for different cases.
  -----------------------------------------------------------------------

  ------------------------------------------------------------------------
   
  **Section 88A(5)**
  An order under this section may amend this Act or any other enactment.
  ------------------------------------------------------------------------

### 88A.02 {#a02}

Subsection (1) gives the Secretary of State a general power to amend
legislation, including the Act, to allow the Unified Patent Court
Agreement signed on 19 February 2013 to be brought into effect.

### 88A.03 {#a03}

Subsections (2), (3), and (4) list the main areas which may be included
in such an order, for example provisions relating to jurisdiction of the
Unified Patent Court and for the payment of fees. There is also
provision for the order to make different provisions in different cases
and to align provisions in the Act with equivalent provisions in the
Agreement on a Unified Patent Court. This would allow provisions in the
Act relating to all types of patents to be aligned with those in the
Agreement.

  --------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 88A(6)**
  An order under this section may not be made unless a draft of the order has been laid before, and approved by resolution of, each House of Parliament.
  --------------------------------------------------------------------------------------------------------------------------------------------------------

### 88A.04 {#a04}

Subsection (6) states that any statutory instruments made under this
section must be approved by resolution of both Houses of Parliament
before it can come into law (this is known as the 'affirmative
procedure').

  ---------------------------------------------------------------------------------------------------------------
   
  **Section 88A(7)**
  The meaning of "court" in this section is not limited by the definition of that expression in section 130(1).
  ---------------------------------------------------------------------------------------------------------------

### 88A.05 {#a05}

The Unified Patent Court is not within the definition of court given in
the Act, which is in section 130(1). The effect of subsection (7) is
that the definition in section 130(1) does not apply to section 88A.
This means that in section 88A the word court includes the Unified
Patent Court.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
