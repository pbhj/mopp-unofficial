::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 13: Mention of inventor {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (13.01 - 13.19) last updated: April 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 13.01

This section deals with the right of an inventor to be mentioned in an
application or a patent, the obligation on the applicant to identify the
inventor(s) and the right of any person to object to a mentioned
inventor. Relevant procedures are prescribed in r.10 and part 7 of the
Patents Rules 2007. The inventor may also apply to waive their right to
have their name and address mentioned following the procedures
prescribed by r.11.

### 13.02

s.125(1) is also relevant

Since the invention to which a patent or an application relates is
determined by the claims, which may differ as between application and
patent, it is possible that a person may be entitled to be named as an
inventor in the application but not in the patent. The meaning of
"inventor" is discussed in [paragraph
7.12](/guidance/manual-of-patent-practice-mopp/section-7-right-to-apply-for-and-obtain-a-patent/#ref7-12)

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 13(1)**
  The inventor or joint inventors of an invention shall have a right to be mentioned as such in any patent granted for the invention and shall also have a right to be so mentioned if possible in any published application for a patent for the invention and, if not so mentioned, a right to be so mentioned in accordance with rules in a prescribed document.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 13.03 {#ref13-03}

r.11(1), r.11(2), r.11(3), r.26(2), r.44(2), r.51(3)(e) is also relevant

The front pages published with the specification of the application and
any patent granted normally each contain separate lists of all the
applicants and all the inventors even when these lists are identical.
However, before the preparations for publication have been completed,
anyone identified as an inventor may apply in writing to the comptroller
to waive their right to have their name and address mentioned as those
of the inventor, or to waive this right in respect of their address
only. If the application is to withhold both the inventor's name and
address, satisfactory reasons must be given, but this is not required if
the application is to withhold just the address. Where an application to
withhold an inventor's details has been accepted, they will not appear
in the patent application as published. The comptroller will also omit
the details from the Register, and unless otherwise directed, no
document bearing them shall be open to public inspection.

### 13.03.1 {#section-2}

r.11(4), r.11(5) is also relevant

Where an application to waive an inventor's right to be mentioned has
been accepted, the inventor can later apply to the comptroller to end
the waiver. An agreement by the comptroller to end such a waiver may be
subject to any conditions they may direct.

### 13.04 {#ref13-04}

PR part 7 is also relevant

Under s.13(1) and r.10(2) any person who alleges that they or another
person ought to have been mentioned as the or an inventor in a granted
patent or a published application may apply on Form 2 to have the matter
rectified. There is no time limit on when an application may be made.
Form 2 should be accompanied by a copy thereof and by a statement of
grounds in duplicate; this starts proceedings before the comptroller,
the procedure for which is discussed [at 123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05)
. There is no need to file an additional Form 2 and statement to deal
with inventorship when it has been put in issue by a reference in
entitlement proceedings under section 8 or 37.

### 13.04.1 {#ref13-04-1}

s.78(2) is also relevant

In the case of an application for a European patent (UK) an incorrect
designation of an inventor may also be rectified before the EPO under
r.21 EPC. (An application under s.13(1) may be made once the grant of a
patent has been mentioned in the European Patent Bulletin ([see also
13.18](#ref13-18))).

\[The front page of the specification will be republished together with
an amendment sheet giving details of the changes indicating that the
addition is only in respect of EP(UK) (see file of EP 0370590).
Publication Section will send a copy of the republished specification to
the applicant. A copy should also be sent to the EPO for information
suggesting that they might like to associate it with the B publication
(again see EP 0370590). \]

### 13.05 {#section-3}

r.77 is also relevant

Provided that they have not given written consent to the application,
the comptroller will send a copy of Form 2 and the statement to the or
each proprietor of, or applicant for, the patent, to each person
identified as an inventor either in the application or on Form 7 ([see
13.08](#ref13-08)) and to any other person whose interests the
comptroller considers may be affected by the application under s.13.
Copies are not of course sent to the person making the application. Any
recipient of such a copy who wishes to oppose the application should
file a counter-statement in the proceedings.

### 13.06 {#section-4}

\[deleted\]

### 13.07 {#section-5}

r.10(1) is also relevant

If the comptroller is satisfied that a person should have been mentioned
in the published application or patent an addendum or erratum to this
effect should be issued. The Register should be altered but not Form 7.

  -----------------------------------------------------------------------
   

  **Section 13(2)**

  Unless he has already given the Patent Office the information
  hereinafter mentioned, an applicant for a patent shall within the
  prescribed period file with the Patent Office a statement -\
  (a) identifying the person or persons whom he believes to be the
  inventor or inventors; and\
  (b) where the applicant is not the sole inventor or the applicants are
  not the joint inventors, indicating the derivation of his or their
  right to be granted the patent;\
  and, if he fails to do so, the application shall be taken to be
  withdrawn.
  -----------------------------------------------------------------------

### 13.08 {#ref13-08}

r.10(4) is also relevant

The request for grant of a patent (Form 1) requires an indication as to
whether the applicant(s) is/are the sole/joint inventor(s). If the or
any applicant is not an inventor or if any inventor is not an applicant
it will be necessary to file Form 7 identifying the inventor(s) and
indicating how any applicant who is not an inventor derives the right to
be granted a patent.

\[The surname or family name of the inventor(s) should be underlined in
black ink. Where this has not been done the underlining can be effected
in the Office. However, caution must be exercised in the case of
overseas inventor since the surname or family name may not be obvious.
Guidance from the applicant/agent should be sought in such cases. \]

### 13.09 {#ref13-09}

While it is not necessary to give details of the derivation of right,
sufficient information particular to the application should be given.
For example it is acceptable to indicate that the applicant is the
employer of the inventor, or has rights by virtue of an assignment from
the inventor to the applicant, or that the applicant is the personal
representative of a deceased inventor. Alternatively, at the very least
an indication should be given of which of the s.7(2) categories the
applicant falls under ([Nippon Piston Ring Co Ltd's Application \[1987\]
RPC 120](https://doi.org/10.1093/rpc/1987rpc120){rel="external"}). Thus,
vague statements to the effect that the applicant acquires their rights
"by operation of law", or "by virtue of s.7(2)" are not sufficient, nor
is an incomplete chain of title, such as "by assignment from A to B",
the applicant being C. However "By assignment" on its own is, in the
context of Form 7, sufficient to indicate an assignment directly from
the inventor to the applicant.

In [Thaler v Comptroller-General of Patents \[2021\] EWCA Civ
1374](https://www.bailii.org/ew/cases/EWCA/Civ/2021/1374.html){rel="external"}
Arnold LJ, relying on
[Nippon](https://academic.oup.com/rpc/article/104/6/120/1584132?login=true){rel="external"},
reiterated that the requirement imposed by section 13(2)(b) cannot be
ignored and that, if it is not complied with, then an application must
be deemed withdrawn. The COA noted that it was not for the Comptroller
to investigate in detail the correctness or factual accuracy of the
information provided on F7's filed before them, rather such information
had to be taken at face value.

::: call-to-action
This position was emphasised in the Supreme Court decision in [Thaler v
Comptroller-General of Patents, Designs and Trademarks \[2023\] UKSC
49](https://www.bailii.org/uk/cases/UKSC/2023/49.html){rel="external"}
at paragraph 96. In this case the applicant had deliberately identified
a non-person as the inventor and brought this to the attention of the
Comptroller. The question was therefore one of law; did this statement,
of itself, provide a proper basis for accepting the application. The
Comptroller was correct to take the statement at face value and in the
absence of any explanation of a satisfactory derivation of right was
correct to treat the applications as having been withdrawn.
:::

\[Where the Formalities Examiner discovers a defective description of
derivation of right, the matter must be immediately discussed with the
Formalities Manager. If the Formalities Manager confirms the objection,
and provided at least three weeks remain of the prescribed sixteen-month
period for submitting the Form 7, the agent/applicant must be telephoned
and advised to correct the defect within two weeks by submission either
of a fresh Form 7 or of a written request for correction. (A telephone
conversation report should issue in the usual way, with a copy placed on
file). If, however, there is less than three weeks left of the
sixteen-month period, or the agent refuses to correct the defect, or the
Form 7 or written request for correction is submitted after expiry of
the sixteen-month period, the Formalities Manager must refer the matter
immediately to the Divisional head of admin for further action. \]

### 13.10 {#ref13-10}

s.7(4) is also relevant

An applicant is not required to substantiate a statement of derivation
of right to the invention. In the absence of anything being established
to the contrary they are assumed prima facie to be entitled to be
granted a patent [see
7.13](/guidance/manual-of-patent-practice-mopp/section-7-right-to-apply-for-and-obtain-a-patent/#ref7-13).

### 13.10.1 {#ref13-10-1}

In [Thaler v Comptroller-General of Patents \[2021\] EWCA Civ
1374](https://www.bailii.org/ew/cases/EWCA/Civ/2021/1374.html){rel="external"}
the court held that for the purposes of sections 7 and 13 of the Patents
Act 1977, an inventor must be a natural person.

::: call-to-action
This position was emphasised by the Supreme Court when upholding the
earlier decision in [Thaler v Comptroller-General of Patents, Designs
and Trademarks \[2023\] UKSC
49](https://www.bailii.org/uk/cases/UKSC/2023/49.html){rel="external"}.
:::

In this case, the applicant had designated an artificial intelligence
(AI) machine, which he called DABUS, as inventor. The court found that
because DABUS was not a natural person it could not be regarded as an
inventor, and furthermore could not own intellectual property or by
extension transfer ownership of that intellectual property to the
applicant. As a result, derivation of right through ownership of DABUS
did not meet the requirements of section 7(2) and the applicant was not
entitled to apply for the patents in question ([see also
7.11.1](/guidance/manual-of-patent-practice-mopp/section-7-right-to-apply-for-and-obtain-a-patent/#ref7-11-1)).

### 13.11 {#ref13-11}

r.10(3), r.21 is also relevant

The period prescribed for filing Form 7 is sixteen months from the
declared priority date or, where there is none, from the filing date.
This period may be extended in accordance with r.108(2) or (3) and (4)
to (7), [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34)
if for example all the required information has not been supplied in
time. A longer time limit may however apply in the case of a divisional
application ([see
15.26](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-26))
or other application claiming an earlier date of filing. Different
periods also apply in converted European applications ([see
81.11](/guidance/manual-of-patent-practice-mopp/section-81-conversion-of-european-patent-applications/#ref81-11))
and international applications ([see
89A.12](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-12)
[and
89A.18](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-18));
[see also 13.12](#ref13-12)).

### 13.12 {#ref13-12}

s.81(3)(c) and s.89B(1)(c) is also relevant

In the case of a converted European application the requirements of
s.13(2) are regarded as being met, and hence Form 7 is not needed, if
the provisions of the EPC requiring the applicant to give the full name
and address of each inventor and an indication of the derivation of
title to the invention have been met.

### 13.12.1 {#ref13-12-1}

s.13(2) and s.89B(1)(c) is also relevant

In the case of an international application which enters the UK national
phase, where a declaration of the name and address of each inventor was
filed in the international phase then that declaration is treated as
having been provided under section 13(2) \[this is provided for
explicitly by s.89B(1)(c)\]. Similarly, a declaration in the
international phase of the applicant's entitlement to apply for and be
granted a patent is also treated as having been provided under section
13(2) \[this is not stipulated by s.89B but follows from the provisions
of the PCT itself\]. It does not follow that the declarations
necessarily meet the requirements of section 13(2) to identify the
inventor(s) and indicate the derivation of the applicant's right, it
being open to the Comptroller to determine these matters (see 89B.07.1
and Thaler's application
[O/447/22](https://www.ipo.gov.uk/p-challenge-decision-results/o44722.pdf){rel="external"}).

### 13.13 {#ref13-13}

If, in a case where it is required, a properly-completed Form 7 is not
filed within the prescribed period ([see 13.11](#ref13-11)) the
application is taken to be withdrawn, and is consequently not published.
([See also
15A.12](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-12)).

\[For termination procedure, [see
14.199](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-199)
and
[15.55](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-55)
\]

### 13.14 {#ref13-14}

A defect in the form may be rectified by filing a fresh form, provided
that the prescribed period has not expired or, if it has, an extension
has been allowed ([see 13.11](#ref13-11)). (If the application is one
where copies of Form 7 are sent to the inventors ([see
13.15](#ref13-15)), they should be sent copies of the replacement form).
No evidence is needed to substantiate this alteration, but if the
prescribed period and any extension has expired the only way in which
the information on the form can be changed is (in an appropriate case)
by submitting a written request to correct an error ([see
117.22](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-22)).
However if any discrepancies are minor, so that the requirements of
r.10(3) can be regarded as having been complied with, they can be
rectified, within a period specified by the Office. An insufficient
indication of the derivation of title ([see 13.09](#ref13-09)) is not
regarded as a minor discrepancy, and if the prescribed period has
expired an extension of the period would need to be sought under r.108
to enable the required information to be supplied. The decision in
[Payne's Application \[1985\] RPC
193](https://doi.org/10.1093/rpc/1985rpc193){rel="external"} means that
s.117 cannot be invoked to overcome the mandatory requirements of
s.13(2) ([see 13.13](#ref13-13)).

\[If the fresh form is acceptable the Formalities Examiner should
endorse it as the effective form\]

### 13.14.1 {#section-6}

In the case of an international application in its international phase,
a request to record any change in the person, name or address of the
inventor should be made to the International Bureau under PCT Rule 92
bis before the expiry of 30 months from the priority date.

### 13.15 {#ref13-15}

There is no longer a need for more than one copy of Form 7 to be filed
for multiple inventors.

### 13.16 {#section-7}

\[Deleted\]

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 13(3)**
  Where a person has been mentioned as sole or joint inventor in pursuance of this section, any other person who alleges that the former ought not to have been so mentioned may at any time apply to the comptroller for a certificate to that effect, and the comptroller may issue such a certificate; and if he does so, he shall accordingly rectify any undistributed copies of the patent and of any documents prescribed for the purposes of subsection (1) above.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 13.17 {#ref13-17}

PR part 7 is also relevant

Anyone may apply for a certificate to the effect that a person should
not have been mentioned as inventor in a published application or a
granted patent. The application is made by filing Form 2 and a statement
of grounds in duplicate. This starts proceedings before the comptroller,
the procedure for which is discussed [at 123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05)
and is essentially similar to that for an application under s.13(1),
([see 13.04 - 13.05](#ref13-04)). If the person making the application
satisfies the comptroller they will issue a certificate accordingly and
will correct, by means of an erratum, any copies of the patent which are
subsequently distributed. The formal decision incorporates any such
certificate. The Register is altered accordingly, but not Form 7.

### 13.18 {#ref13-18}

Where before a patent is granted, an application is made on Form 2 for a
certificate that a person should not have been mentioned in a published
application, the hearing officer should issue a certificate in respect
of the application if appropriate and make an order that that person is
not entitled to be mentioned as an inventor in any patent granted for
the invention.

\[The order shall be given effect as far as the register is concerned by
the head of Tribunal Section. If an erratum or addendum is required for
the published application, these will be issued by the appropriate
formalities group at the earliest opportunity in the pre-grant cycle. If
the patent is granted, responsibility for requesting republication of
the front page and production of an amendment sheet giving details of
the changes rests with Tribunal Section\]

### 13.18.1 {#ref13-18-1}

Action under s.13(3) is not immediately appropriate where, following
amendment of an application before a patent is granted, it is no longer
fitting for a person correctly mentioned as an inventor in the published
application still to be mentioned in any granted patent. Form 2 should
not be filed in such cases. Instead the relevant facts accompanied by,
whenever possible, the agreement of all parties to the person not being
so mentioned, should be filed in writing at the Office. If necessary,
the Office will write to any party who has not already consented, giving
an opportunity for comment. If the Office is satisfied that all
registered applicants, named inventors and any other person whose
interests it considers might be affected, agree, it will not mention the
inappropriate person as an inventor in any granted patent. If the matter
cannot be agreed and the patent application becomes in order, then the
application should be granted and the question resolved by an action
under s.13(3).

### 13.19 {#section-8}

s.78(2) is also relevant

In the case of an application for a European patent (UK) an application
under s.13(3) may be made either before or after the mention of grant of
a patent in the European Patent Bulletin but if made before such mention
of grant, the relief available in respect of a published application is
limited to the issue of a certificate ([see also 13.04.1](#ref13-04-1)).
An incorrect designation of an inventor may also be cancelled before the
EPO under r.21 EPC.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
