::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 22: Information prejudicial to national security or safety of public {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (22.01 - 22.29) last updated: April 2022.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 22.01

This section gives the comptroller the power to prohibit the
communication of information disclosed in an application filed at the
Office, specifies how such an application is to be dealt with by the
Office, lays down how prohibition directions are to be reviewed by the
Secretary of State, confers certain rights on an applicant where grant
of a patent is prevented by such a direction, and finally specifies
penalties for failure to comply with such a direction. S.22 applies to
all applications filed at the Office, whether filed under the 1977 Act
or filed at the Office in its capacity as a Receiving Office under the
EPC or the PCT. It also applies to applications under the 1949 Act,
except that where such an application was, on the date (1st June 1978)
on which the 1977 Act came into force, already the subject of directions
under s.18 of the 1949 Act, those directions continue in force; if and
when directions on such an application are revoked any patent is
published and granted under the 1949 Act (unless the application is
withdrawn in which case it is neither granted nor, following paragraph 1
of Schedule 5 to the CDP Act, published). S.22 was amended by the
Patents Act 2004 with effect from 1 January 2005, which substituted the
original term "the defence of the realm" with "national security"
throughout this section, without any intended change of scope. The 2004
Act also amended s.22(6) to remove references to the United Kingdom
Atomic Energy Authority (UKAEA).

### 22.02

The term "Secretary of State" when used in a statute means "one of his
Majesty's Principal Secretaries of State" (Interpretation Act 1978,
Schedule 1).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 22(1)**
  Where an application for a patent is filed in the Patent Office (whether under this Act or any treaty or international convention to which the United Kingdom is a party and whether before or after the appointed day) and it appears to the comptroller that the application contains information of a description notified to him by the Secretary of State as being information the publication of which might be prejudicial to national security, the comptroller may give directions prohibiting or restricting the publication of that information or its communication to any specified person or description of persons.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 22(2)**
  If it appears to the comptroller that any application so filed contains information the publication of which might be prejudicial to the safety of the public, he may give directions prohibiting or restricting the publication of that information or its communication to any specified person or description of persons until the end of a period not exceeding three months from the end of the period prescribed for the purposes of section 16 above.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 22.03 {#ref22-03}

Every application filed at the Office, is, once it has passed through
Document Reception and New Applications (or has been filed securely -
[see 22.07](#ref22-07)), scrutinised by an examiner in Security Section,
who is supplied with a list of material, the publication of information
about which might be prejudicial to national security. If such
information is disclosed, the application is removed from the general
stream of applications and directions are given under s.22(1)
prohibiting the publication of the application and the communication of
its contents. Similar directions are given under s.22(2) if the
information disclosed is such that its publication might be prejudicial
to the safety of the public. In this case no general guidance is given
by the Secretary of State, and the decision as to what falls into this
category is a matter for the comptroller.

### 22.03.1 {#section-2}

Every application arriving in an examining group will therefore have
been screened by Security Section and usually no further checks under
s.22 are needed. However, if an examiner becomes aware that material
potentially relevant to s.22 has subsequently been placed on file, for
example in correspondence from the agent or by amendment (whether
allowable under s.76 or not), the examiner should refer the application
to Security Section in Room G.R70 for advice.

### 22.04 {#section-3}

Directions under s.22(1) continue in force until revoked ([see
22.14](#ref22-14)), while directions under s.22(2), unless confirmed
([see 22.13](#ref22-13)), automatically lapse at the end of the period
of twenty-one months from the priority date or, where there is no
priority date, the filing date of the application.

### 22.05 {#section-4}

s.97(1)(c) is also relevant

In neither case is there any appeal from a decision by the comptroller
to issue prohibition directions.

### 22.06 {#section-5}

Although prohibition directions initially impose a blanket prohibition
against any disclosure of the patent application and information
therein, permission may be sought from the comptroller for disclosure to
specified persons. If granted such permission will impose conditions on
the persons so specified; thus they should not without specific
authorisation disclose the information to any other person. Permission
must be sought for filing corresponding applications abroad [see
23.04](/guidance/manual-of-patent-practice-mopp/section-23-restrictions-on-applications-abroad-by-united-kingdom-residents/#ref23-04).

### 22.07 {#ref22-07}

Although all applications are inspected by Security Section, anyone
filing an application, knowing that a Government department or a foreign
government wishes its contents to be kept secret, or the contents of
which relate to a classified Government contract, should file the
application at Room G.R70, Concept House, and not to the usual Front
Office. Such applications may be filed by hand at the Newport or London
office, in envelopes marked "For the attention of GR70", but only
between the hours of 9am and 5pm. The receptionist should be informed
that the application is for GR70 rather than the usual Front Office.
Documents which might include information of relevance to national
defence or security should not be filed by facsimile transmission. In
respect of a filing connected with a classified Government contract the
application should be accompanied by a notification of the number of the
contract together with the name and address of the Government agency
involved in the contract.

### 22.08 {#section-6}

Documents involved in applications subject to prohibition directions
under Section 22(1) must be despatched under security rules; Room G.R70
will always advise on this procedure. If such documents are being
despatched to the Office, the envelope should be clearly marked "for
attention of Room G.R70" and should be addressed to Room G.R70, Concept
House, Cardiff Road, Newport, South Wales, NP10 8QQ.

  -----------------------------------------------------------------------
   

  **Section 22(3)**

  While directions are in force under this section with respect to an
  application -\
  (a) if the application is made under this Act, it may proceed to the
  stage where it is in order for the grant of a patent, but it shall not
  be published and that information shall not be so communicated and no
  patent shall be granted in pursuance of the application;\
  (b) if it is an application for a European patent, it shall not be sent
  to the European Patent Office; and\
  (c) if it is an international application for a patent, a copy of it
  shall not be sent to the International Bureau or any international
  searching authority appointed under the Patent Co-operation Treaty.
  -----------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 22(4)**
  Subsection (3)(b) above shall not prevent the comptroller from sending the European Patent Office any information which it is his duty to send that office under the European Patent Convention.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 22.09 {#section-7}

For as long as prohibition directions are in force an application for a
patent under the Act is dealt with by an examiner in Security Section.
Search and substantive examination are carried out in the usual way for
the period allowed for requesting substantive examination, [see
18.02](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-02),
but the application is not published, and at no stage is it mentioned in
the Journal. When it appears to the examiner that the application is in
order, a formal report indicating that the application complies with the
Act and Rules is issued under s.18(4). The application does not however,
proceed to publication and grant whilst the prohibition directions
remain in force.

EPC r.37(2), EPC a.135, s.81 is also relevant.

### 22.10 {#section-8}

An application for a European patent which is filed at the Office is not
forwarded to the European Patent Office while it is the subject of
prohibition directions. If as a consequence it does not reach the
European Office before the end of fourteen months from the declared
priority date or, where there is none, the filing date, the European
application is deemed to be withdrawn. The applicant may then apply for
the application to be converted to one for a patent under the Act [see
81.03-06](/guidance/manual-of-patent-practice-mopp/section-81-conversion-of-european-patent-applications#ref81-03)
and/or, subject to the comptroller's permission, [see
23.04](/guidance/manual-of-patent-practice-mopp/section-23-restrictions-on-applications-abroad-by-united-kingdom-residents/#ref23-04)
to appropriate foreign applications.

### 22.11 {#section-9}

PCT a.27(8), PCT rr.22.1(a),15.6(iii),16.2(iii) is also relevant.

Likewise an international application filed at the Office which has
attracted prohibition directions is not forwarded to WIPO or to the
International Searching Authority. An international application subject
to prohibition directions is no longer treated as such and must enter
the national phase early if it is to proceed. Any international and
search fees paid will be refunded to the applicant. There is no
provision for the application to be converted to one for a patent under
the Act although as soon as the conditions of s.15(1) are complied with
a filing date under the Act may be accorded.

  -----------------------------------------------------------------------
   

  **Section 22(5)**

  Where the comptroller gives directions under this section with respect
  to any application, he shall give notice of the application and of the
  directions to the Secretary of State, and the following provisions
  shall then have effect -\
  (a) the Secretary of State shall, on receipt of the notice, consider
  whether the publication of the application or the publication or
  communication of the information in question would be prejudicial to
  national security or the safety of the public;\
  (b) if the Secretary of State determines under paragraph (a) above that
  the publication of the application or the publication or communication
  of that information would be prejudicial to the safety of the public,
  he shall notify the comptroller who shall continue his directions under
  subsection (2) above until they are revoked under paragraph (e) below;\
  (c) if the Secretary of State determines under paragraph (a) above that
  the publication of the application or the publication or communication
  of that information would be prejudicial to national security or the
  safety of the public, he shall (unless a notice under paragraph (d)
  below has previously been given by the Secretary of State to the
  comptroller) reconsider that question during the period of nine months
  from the date of filing the application and at least once in every
  subsequent period of twelve months;\
  (d) if on consideration of an application at any time it appears to the
  Secretary of State that the publication of the application or the
  publication or communication of the information contained in it would
  not, or would no longer, be prejudicial to national security or the
  safety of the public, he shall give notice to the comptroller to that
  effect; and\
  (e) on receipt of such a notice the comptroller shall revoke the
  directions and may, subject to such conditions (if any) as he thinks
  fit, extend the time for doing anything required or authorised to be
  done by or under this Act in connection with the application, whether
  or not that time has previously expired.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 22(6)**

  The Secretary of State may do the following for the purpose of enabling
  him to decide the question referred to in subsection (5)(c) above -\
  (a) where the application contains information relating to the
  production or use of atomic energy or research into matters connected
  with such production or use, he may at any time do one or both of the
  following, that is to say,\
  (i) inspect the application and any documents sent to the comptroller
  in connection with it;\
  (ii) authorise a government body with responsibility for the production
  of atomic energy or for research into matters connected with its
  production or use, or a person appointed by such a government body, to
  inspect the application and any documents sent to the comptroller in
  connection with it; and\
  (b) in any other case, he may at any time after (or, with the
  applicant's consent, before) the end of the period prescribed for the
  purposes of section 16 above inspect the application and any such
  documents; and where a government body or a person appointed by a
  government body carries out an inspection which the body or person is
  authorised to carry out under paragraph (a) above, the body or (as the
  case may be) the person shall report on the inspection to the Secretary
  of State as soon as is practicable.
  -----------------------------------------------------------------------

### 22.12 {#ref22-12}

When directions under s.22(1) or (2) have been given with respect to an
application the Secretary of State must be so informed, and must advise
whether or not it should continue in force. Such advice will not be
tendered in advance of the Secretary of State (usually, in practice, the
Ministry of Defence) inspecting the application. This inspection is
however done immediately if the application contains information
relating to the production or use of atomic energy or research into
matters connected with such production or use, and the Secretary of
State may authorise a government body with responsibility for the
production of atomic energy or for research into its production or use,
or a person appointed by such a body to inspect the application. ([See
also 22.27-22.29](#ref22-27)). Otherwise the inspection cannot take
place until after the expiry of eighteen months from the declared
priority date or, where there is none, the filing date, unless the
applicant gives permission for an earlier inspection. It is therefore
advantageous, if early consideration for revocation of the directions is
desired, to complete and return to Room G.R70 Concept House together
with a copy of the specification the Form of Assent to Inspection which
is despatched with the letter sent to the applicant stating that the
order has been imposed. Even when early revocation is not being sought,
it is desirable to acknowledge receipt of the letter imposing the
directions.

### 22.13 {#ref22-13}

If the Secretary of State informs the comptroller that in their opinion
the application contains information the publication of which would be
prejudicial to the safety of the public, then the direction given under
s.22(2) are confirmed. They then do not lapse, but continue in force
until revoked.

### 22.14 {#ref22-14}

If the Secretary of State notifies the comptroller that publication of
the information is not considered prejudicial the directions given under
s.22(1) or (2) are revoked. The application then proceeds as described
[in 22.19-22.23](#ref22-19).

### 22.15 {#section-10}

If on the other hand the Secretary of State decides that publication
would be prejudicial to defence or public safety, so that the
prohibition directions given by the comptroller continue in force, then
the Secretary of State must periodically reconsider their decision. This
must be done within nine months of the application being filed, and at
least once a year thereafter (but [see 22.12](#ref22-12)). The applicant
is not informed when such reconsideration has taken place, but it is
open to the applicant at any time to enquire whether directions could be
revoked. If and when it is decided that publication would no longer be
prejudicial, the procedure in [22.14](#ref22-14) is followed.

### 22.16 {#ref22-16}

The letter notifying revocation of prohibition directions on an
application may indicate that any subsequent application claiming
priority from it may nevertheless need to be scrutinized. In such cases
Room G.R70 Concept House should be informed of any such subsequent
application made in this office, preferably by means of a letter stating
whether there are changes in the specification compared with that of the
earlier application. If they are at all substantial, the changes should
be shown on a copy of the later application or the relevant parts
thereof. The same procedure should be followed before making such
subsequent applications abroad (with or without benefit of priority)
under the EPC, PCT or national routes [see also
23.03](/guidance/manual-of-patent-practice-mopp/section-23-restrictions-on-applications-abroad-by-united-kingdom-residents/#ref23-03).
Correspondence with Room G.R70 Concept House on these and other matters
not germane to the examination of the application will not be placed
open to public inspection. It should be noted that failure to mention
the existence of an earlier application for the same or similar matter
when priority is not claimed can sometimes lead to unnecessary
prohibition.

### 22.17 {#section-11}

s.16(1) is also relevant.

Following revocation of prohibition directions an application for a
patent under the Act will normally be published under s.16 as soon as
practicable, and details of the application will be published in the
Journal. It will not be so published however if it is withdrawn before
the prohibition directions are revoked or preparations for publication
have been completed [see
16.07](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-07).

### 22.18 \[not used\]

### Procedure after directions have been revoked

### 22.19 {#ref22-19}

When prohibition directions are revoked before a search has been carried
out, the application is sent either for search in an ordinary examining
group or to await the filing of Form 9A and/or claims; it then proceeds
in the normal way [see 17.02 et
seq](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-02).

\[ When the directions are revoked in this situation, the application is
forwarded to the appropriate Formalities Manager; it is then either
forwarded after fine allocation to the appropriate examining group or
(if it is a paper case) put in store to await filing of the outstanding
document(s). \]

### 22.20 {#ref22-20}

If the directions are revoked after issue of a search report but before
issue of a substantive examination report the application is sent to the
appropriate examining group. The group examiner should consider the
search which has been carried out by Security Section and should perform
an additional search if they is reasonably sure that it would yield more
pertinent prior art; any relevant document so discovered should be dealt
with as described in [17.105 -
17.105.2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-105).
The application should then be classified and sent for s.16 publication
[see
17.102](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-102),
[16.30](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-30),
and will then proceed as an ordinary application being dealt with in the
appropriate examining group.

\[An additional search will not often be required. \]

### 22.21 {#ref22-21}

If the directions are revoked after issue of a substantive examination
report but before the application is in order, the application should be
sent at once for publication, after following the procedure described
[in 22.20](#ref22-20). In addition to the text of the specification as
filed, the published application should include the claims in their
current form if this differs from that originally filed. Substantive
examination is then continued, either by the Security Section examiner
who prepared the first examination report or, if thought appropriate, by
the examiner responsible for the subject-matter concerned.

\[Unless the specification has remained unamended, the security examiner
should annotate the appropriate description and claims in the table of
contents in PDAX. If the drawings have been amended a copy of the
unamended formal drawings as filed should also be annotated in the table
of contents. If the application is a paper case, the security examiner
should place a copy of the specification, flagged "TO BE PRINTED AS 'A'
DOCUMENT", in front of the original specification in the shell. If the
abstract has been reframed, the reframed original copy of the abstract
should replace any duplicate copy in the duplicate specification during
production of the 'A' document. Also, a copy of the claims in their
current form if this differs from that on filing should be attached to
the duplicate specification. If the drawings have been amended a copy of
the unamended formal drawings as filed should be flagged "TO BE PRINTED
IN 'A' DOCUMENT" andattached to the specification sent for publication.

The above preparations for "A" publication are made by the examiner in
security section; any subsequent changes therein should be made only
after consulting the security examiner. When in due course, the
application is returned for completion of the substantive examination,
the examiner who is completing the examination should ensure that the
set of documents is properly re-assembled and that the duplicate copy of
the abstract, if removed as above, has been returned to its correct
position (paper cases only). \]

### 22.22 {#section-12}

As a result of the directions having been in force, it may be necessary
to extend the compliance period using the discretion conferred on the
comptroller by s.22(5)(e). Such discretion should not be exercised
earlier than necessary in a particular case. It should be noted that
discretion under s.22(5)(e) can be exercised only after the prohibition
directions have been revoked. It is not regarded as enabling the
comptroller to resuscitate an application which, when the prohibition
was revoked, had already been treated as having been withdrawn or
refused, for example through failure to file Form 9A or Form 10 in time
or through not being in order at the end of the compliance period.

### 22.23 {#section-13}

When the prohibition directions are revoked after an application has
been put in order, it should first of all be published under s.16, after
following the procedure described [in 22.20](#ref22-20). As a
consequence of the wording of s.16(1) the published application should
include both the text of the specification as filed and the claims in
their final form, if different. The application should then proceed to
grant [see
18.85-18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-85).

\[ For s.16 publication procedure, [see 22.21](#ref22-21). When in due
course the file is returned to the examiner for revision of the
classification prior to B publication, he should see that the documents
going forward for publication have been properly assembled. \]

  -----------------------------------------------------------------------
   

  **Section 22(7)**

  Where directions have been given under this section in respect of an
  application for a patent for an invention and, before the directions
  are revoked, that prescribed period expires and the application is
  brought in order for the grant of a patent, then-\
  (a) if while the directions are in force the invention is worked by (or
  with the written authorisation of or to the order of) a government
  department, the provisions of sections 55 to 59 below shall apply as if
  -\
  (i) the working were use made by section 55;\
  (ii) the application had been published at the end of that period; and\
  (iii) a patent had been granted for the invention at the time the
  application is brought in order for the grant of a patent (taking the
  terms of the patent to be those of the application as it stood at the
  time it was so brought in order); and\
  (b) if it appears to the Secretary of State that the applicant for the
  patent has suffered hardship by reason of the continuance in force of
  the directions, the Secretary of State may, with the consent of the
  Treasury, make such payment (if any) by way of compensation to the
  applicant as appears to the Secretary of State and the Treasury to be
  reasonable having regard to the inventive merit and utility of the
  invention, the purpose for which it is designed and any other relevant
  circumstances.
  -----------------------------------------------------------------------

### 22.24 {#section-14}

Thus if an application was brought in order within the compliance period
and, while a prohibition order was still in force, was used for the
services of the Crown, that use is to be treated as though the patent
had been granted and published, and the applicant and other parties have
the rights provided for in ss.55-59. Moreover if the prohibition
direction has caused hardship the applicant may be awarded reasonable
compensation.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 22(8)**
  Where a patent is granted in pursuance of an application in respect of which directions have been given under this section, no renewal fees shall be payable in respect of any period during which those directions were in force.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 22.25 {#section-15}

r.37, r.38 is also relevant.

No renewal fees are payable either in respect of years preceding that in
which the direction is revoked or in respect of the year during which it
is revoked. The first renewal date in respect of which fees are payable
will be the first anniversary of the date of filing following revocation
of the direction, or three months after the date of grant, whichever is
the later. The period within which fees for subsequent years must be
paid is governed by by r.38.

  -----------------------------------------------------------------------
   

  **Section 22(9)**

  A person who fails to comply with any direction under this section
  shall be liable -\
  (a) on summary conviction, to a fine not exceeding the prescribed sum;
  or\
  (b) on conviction on indictment, to imprisonment for a term not
  exceeding twoyears or a fine, or both.
  -----------------------------------------------------------------------

### 22.26 {#section-16}

It should be noted that a failure to comply with a direction under s.22
(or with the requirements of s.23(1)) is a criminal offence. A notice
drawing attention to these matters appears prominently in every issue of
the Patents Journal.

### 22.26.1 {#ref22-26-1}

The Magistrates' Courts Act 1980 amended s.22(9) to set the maximum fine
on summary conviction as the "prescribed sum". The reference to
"indictment" in (b) is treated as a reference to "information" for the
Isle of Man only (S.I. 2003 No. 1249).

### Appendix

### Euratom Treaty

::: call-to-action
### 22.27 {#ref22-27}

The provisions of Art.16 of the Euratom Treaty, requiring the UK to
inform the European Commission of the existence of applications relating
to atomic energy no longer apply following the UK's withdrawal from the
European Union.
:::

::: call-to-action
### 22.28 {#section}

\[Deleted. \]
:::

::: call-to-action
### 22.29 {#section}

\[Deleted. \]
:::
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
