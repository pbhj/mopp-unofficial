::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 51: Powers exercisable in consequence of report of Competition and Markets Authority {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (51.01 - 51.06) last udpated: April 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 51.01 {#ref51-01}

This section makes provision for Ministers to apply to the comptroller
in response to reports by the Competition and Markets Authority
(previously known as the Competition Commission of situations operating
against the public interest. Such an application may lead to
cancellation or modification of conditions in existing licences, and/or
a "licences of right" entry in the register, in respect of a patent. In
April 2014, this section was amended by the Enterprise and Regulatory
Reform Act 2013 (Competition) (Consequential, Transitional and Saving
Provisions) Order 2014 to reflect the change in name and
responsibilities from the Competition Commission to the Competition and
Markets Authority.

### 51.02 {#section}

The application may be made in any of the circumstances set out in
subsection (1), the relief available being as set out in subsection (3).

### 51.03 {#section-1}

s.51(2), PR part 7 and r.75 is also relevant

Before making the application, the Minister should publish notice of it
and consider any representations made within thirty days by persons
likely to be affected. The application should be made on Patents Form 2
accompanied by a copy thereof and a statement of grounds in duplicate.
This starts proceedings before the comptroller, the procedure for which
is discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).
The application is advertised in the journal; this gives an opportunity
for opposition under section 52(1) ([see
52.03-06](/guidance/manual-of-patent-practice-mopp/section-52-opposition-appeal-and-arbitration/#ref52-03)),
[123.05.3](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-3)
and
[123.05.6](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-6).

### 51.04 {#section-2}

Applications under s.51 are subject to ss.52 to 54.

### 51.05 {#section-3}

For the comptroller to take action under s.51, the report of the
Competition and Markets Authority must indicate that factors operating
against the public interest include the terms or conditions in patent
licences or a refusal to grant such licences on reasonable terms.
Paragraphs (a) and (b) of section 51(1) which covered reports of the
Competition and Markets Authority following monopoly and merger
references, respectively, were repealed by the Enterprise Act 2002,
which inserted section 50A into the 1977 Act to provide powers
exercisable by the comptroller following merger and market
investigations ([see
50A.01-04](/guidance/manual-of-patent-practice-mopp/section-50a-powers-exercisable-following-merger-and-market-investigations/#ref50A-01)).

### 51.06 {#section-4}

Statements in reports of the Competition and Markets Authority are also
treated as prima facie evidence in s.48 proceedings, [see
53.04](/guidance/manual-of-patent-practice-mopp/section-53-compulsory-licences-supplementary-provisions/#ref53-04).

  -----------------------------------------------------------------------
   

  **Section 51(1)**

  Where a report of the Competition and Markets Authority has been laid
  before Parliament containing conclusions to the effect -\
  (a) and (b) \[Repealed\]\
  (c) on a competition reference, that a person was engaged in an anti-
  competitive practice which operated or may be expected to operate
  against the public interest, or\
  (d) on a reference under section 11 of the Competition Act 1980
  (reference of public bodies and certain other persons), that a person
  is pursuing a course of conduct which operates against the public
  interest, the appropriate Minister or Ministers may apply to the
  comptroller to take action under this section.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 51(2)**
  Before making an application the appropriate Minister or Ministers shall publish, in such manner as he or they think appropriate, a notice describing the nature of the proposed application and shall consider any representations which may be made within 30 days of such publication by persons whose interests appear to him or them to be affected.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 51(3)**

  If on an application under this section it appears to the comptroller
  that the matters specified in the Competition and Markets Authority
  report as being those which in the opinion of the Competition and
  Markets Authority operate, or operated or may be expected to operate,
  against the public interest include -\
  (a) conditions in licences granted under a patent by its proprietor
  restricting the use of the invention by the licensee or the right of
  the proprietor to grant other licences, or\
  (b) a refusal by the proprietor of a patent to grant licences on
  reasonable terms\
  \
  he may by order cancel or modify any such condition or may, instead or
  in addition, make an entry in the register to the effect that licences
  under the patent are to be available as of right.
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 51(4)**
  In this section "the appropriate Minister or Ministers" means the Minister or Ministers to whom the report of the Competition and Markets Authority was made.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
