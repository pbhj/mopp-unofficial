::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 47: Cancellation of entry made under s.46 {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (47.01 - 47.13) last updated: October 2013.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 47.01

r.43(3) is also relevant

This section provides for the cancellation of an entry made in the
register under s.46, ie that licences under a patent are to be available
as of right. It also provides for opposition to the cancellation.
Relevant procedures are prescribed by rule r.43(3). A WTO proprietor of
a patent for which an entry has been made under section 48 (compulsory
licences) that licences are available as of right may apply to have the
entry cancelled under section 52(2) on the grounds provided in that
section [see
52.07](/guidance/manual-of-patent-practice-mopp/section-52-opposition-appeal-and-arbitration/#ref52-07)

### 47.02

[See
46.01-07](/guidance/manual-of-patent-practice-mopp/section-46-patentee-s-application-for-entry-in-register-that-licences-are-available-as-of-right/#ref46-01)
for general discussion of the licence of right provisions (ss.46 and 47)
including the relationship thereof with the compulsory licence
provisions (ss.48 to 54).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 47(1)**
  At any time after an entry has been made under section 46 above in respect of a patent, the proprietor of the patent may apply to the comptroller for cancellation of the entry.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Application by proprietor

### 47.03 {#ref47-03}

r.43(3) and s.46(3)(d) is also relevant

The proprietor of the patent may apply for cancellation of the "licences
of right" entry at any time after it is made. The application should be
made on Patents Form 30 and should be accompanied by fees to the amount
of the balance of all renewal fees which would have been payable if the
entry had not been made (only half fees having been payable while the
entry is present).

### 47.04 {#section-2}

r.75 is also relevant

The application is advertised in the Journal, to give an opportunity for
opposition to the cancellation under s.47(6), [see
47.11-13](#ref47-11-13).

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 47(2)**
  Where such an application is made and the balance paid of all renewal fees which would have been payable if the entry had not been made, the comptroller may cancel the entry, if satisfied that there is no existing licence under the patent or that all licensees under the patent consent to the application.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 47.05 {#section-3}

Form 30 includes a declaration that there is no existing licence under
the patent or that all licensees consent to the application. If
satisfied that this is so, the balance of renewal fees having been paid
and any opposition to cancellation having been disposed of, the
comptroller may cancel the entry in the register. The applicant will be
informed by letter once the entry has been cancelled.

### 47.06 {#ref47-06}

In Cassou's Patent \[1971\] RPC 91, in which cancellation of a "licences
of right" endorsement was applied for during the course of proceedings
to settle the terms of such a licence, cancellation was not permitted
before the grant of the licence.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 47(3)**
  Within the prescribed period after an entry has been made under section 46 above in respect of a patent, any person who claims that the proprietor of the patent is, and was at the time of the entry, precluded by a contract in which the claimant is interested from granting licences under the patent may apply to the comptroller for cancellation of the entry.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Application by other persons

### 47.07 {#ref47-07}

s.46(2), r.43(4), r.108(1) and PR part 7 is also relevant

The comptroller makes a "licences of right" entry only if satisfied that
the proprietor of the patent is not precluded by contract from granting
licences under the patent. Nevertheless there is an opportunity after
the entry is made for cancellation to be applied for by any person who
claims that the proprietor is, and was at the time of the entry,
precluded by a contract in which the claimant is interested from
granting licences. An application by such a person should be made within
two months after the making of the entry; this period cannot be
extended. The application should be made by filing Form 2 with a copy
thereof and a statement of grounds in duplicate. This starts proceedings
before the comptroller, the procedure for which is discussed at
[123.05](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05)
--
[123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-13).

### 47.08 {#section-4}

r.77 and r.75 is also relevant

The Office sends a copy of the application and statement to the
proprietor of the patent. The application is also advertised in the
Journal.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 47(4)**
  Where the comptroller is satisfied, on an application under subsection (3) above, that the proprietor of the patent is and was so precluded, he shall cancel the entry; and the proprietor shall then be liable to pay, within a period specified by the comptroller, a sum equal to the balance of all renewal fees which would have been payable if the entry had not been made, and the patent shall cease to have effect at the expiration of that period if that sum is not so paid.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 47.09 {#section-5}

Where the comptroller is satisfied that the proprietor is and was
precluded as indicated in [47.07](#ref47-07), any opposition by the
proprietor under s.47(6) having been disposed of ([see
47.11-13](#ref47-11-13)), the entry is cancelled. s.46(3)(d)

### 47.10 {#section-6}

The Office informs the proprietor of the cancellation. Within such
period as the comptroller specifies, the proprietor should pay fees to
the amount of the balance of all renewal fees which would have been
payable if the entry had not been made (only half fees having been
payable while the entry was present). The patent ceases to have effect
at the expiration of the specified period if the necessary sum is not so
paid.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 47(5)**
  Where an entry is cancelled under this section, the rights and liabilities of the proprietor of the patent shall afterwards be the same as if the entry had not been made.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 47(6)**

  Where an application has been made under this section, then -\
  (a) in the case of an application under subsection (1) above, any
  person, and\
  (b) in the case of an application under subsection (3) above, the
  proprietor of the patent,\
  \
  may within the prescribed period give notice to the comptroller of
  opposition to the cancellation; and the comptroller shall, in
  considering the application, determine whether the opposition is
  justified.
  -----------------------------------------------------------------------

### Opposition to cancellation

### 47.11 {#ref47-11-13}

Applications under s.47(1) or (3) for cancellation of a "licences of
right" entry may be opposed. Where the application is by the proprietor
of the patent under s.47(1), any person may make such an opposition.
Where the application is under s.47(3), only the proprietor of the
patent in question may oppose the cancellation.

### 47.12 {#section-7}

PR part 7 r.75, r.76(2), r.77(10) and r.108(1) is also relevant

Every application under s.47(1) or (3) is advertised in the Journal.
Notice of opposition to the cancellation of an entry should be given
within four weeks after the advertisement; this period cannot be
extended. Opposition to an application under section 47(1) should be
made by filing Patents Form 15 accompanied by a copy of the form and by
a statement of grounds in duplicate. This starts proceedings before the
comptroller, the procedure for which is discussed at
[123.05](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05)
--
[123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-13);
a defendant should therefore file a counter-statement in accordance with
rr.77(5)-(6) [see
123.05.3](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-03)
and
[123.05.5](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).
In the case of an application under section 47(3), the applicant has
already started proceedings by filing Form 2 and a statement of grounds
([see 47.07](#ref47-07)), and the opposition should therefore be made by
filing a counter-statement in accordance with rr.77(7)-(8) [see
123.05.3](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-03)
and
[123.05.6](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-06).

### 47.13 {#section-8}

\[deleted\]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
