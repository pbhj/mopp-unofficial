::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 20A: Reinstatement of applications {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (20A.01 - 20A.16) last updated: April 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 20A.01 {#ref20A-01}

This section specifies the circumstances for reinstatement of a patent
application which has been either refused or treated as having been
refused or withdrawn because the applicant failed to do something within
a period prescribed or specified for doing that thing. The relevant
procedures are prescribed in rule 32. This section was introduced by
Regulatory Reform (Patents) Order 2004 which came into effect on 1
January 2005, and applies to patent applications filed both before and
after that date.

  -----------------------------------------------------------------------
   

  **Section 20A(1)**

  Subsection (2) below applies where an application for a patent is
  refused, or is treated as having been refused or withdrawn, as a direct
  consequence of a failure by the applicant to comply with a requirement
  of this Act or rules within a period which is\
  (a) set out in this Act or rules, or\
  (b) specified by the comptroller.
  -----------------------------------------------------------------------

### 20A.02 {#ref20A-02}

Subject to the requirements of section 20A(2), the exclusions of section
20A(3) and a request being made in accordance with the rules, section
20A provides for reinstatement of an application which has been refused
or treated as having been withdrawn because an applicant has failed to
comply with a requirement with respect to the application within a
period prescribed or specified for complying with that requirement. In
Anning's Patent Application \[2007\] EWHC 2770 (Pat) the applicant had
failed to reply to a substantive examination report within the period
specified in the report and had taken no action in response to a
subsequent letter informing him that his application would be treated as
refused if he did not reply by the end of the compliance period. The
application was subsequently treated as refused under s.20(1) at the end
of the compliance period. Pumfrey J held that the "failure to comply"
that led to the application being treated as refused was the failure to
respond to the examination report, not the failure to place the
application in order for acceptance within the compliance period. He
stated:

> The letter \[informing the applicant that no reply to the examination
> report has been received and warning that the application will be
> treated as refused at the end of compliance period\] does not itself
> either require any act to be done or extend the period of time for
> doing that act, and is for this reason not within s.20A: and in my
> judgment the prescribed period under s.20(1) is merely the period
> during which the requirements of the Act and Rules must be complied
> with. It imposes no additional requirement on the applicant.

It therefore follows that an application may not be reinstated if the
only failure by the applicant has been a failure to comply with s.20(1)
[(see also 20A.11.1)](#ref20-11-1).

  -----------------------------------------------------------------------
   

  **Section 20A(2)**

  Subject to subsection (3) below, the comptroller shall reinstate the
  application if, and only if\
  (a) the applicant has requested him to do so,\
  (b) the request complies with the relevant requirements of rules; and\
  (c) he is satisfied that the failure to comply referred to in
  subsection (1) above was unintentional.
  -----------------------------------------------------------------------

### 20A.03 {#ref20A-03}

r.32, PR Sch4 part 1 is also relevant.

An application for reinstatement shall be made on a Form 14. The reason
why the applicant failed to comply with the time allowed should be
stated on the form and should be supported by evidence. A reinstatement
request must be filed within 12 months beginning immediately after the
date on which the application was terminated. This period may not be
altered.

### 20A.04 {#a04}

The outstanding requirements may be filed at the same time as the
request Form 14. However, this is not strictly necessary as a further
period for filing documents and fees will be provided for if
reinstatement is allowed.

\[If a Form 14, fee or supporting evidence is not included with a
reinstatement request, the formalities manager should contact the
applicant/agent and instruct them to file the omitted documents within
14 days; otherwise the application will remain withdrawn. When a Form 14
has been filed the formalities manager should record the request on COPS
using function REGFIL, which will create a Journal entry.\]

### 20A.05 \[Deleted\] {#a05-deleted}

### 20A.06 {#a06}

For guidance on what is meant by "unintentional" for the purposes of
section 20A, [see 20A.13](#ref20A-13).

### 20A.07 {#a07}

r.32(7), r.32(8), r.32(9) is also relevant.

If, after considering the applicant's reasons for reinstatement and the
supporting evidence, the comptroller concludes that a case has not been
made out, then the applicant is informed by letter. The letter will
explain why the comptroller has reached their conclusion and will advise
the applicant that, unless within one month he the applicant requests a
hearing, the request for reinstatement will be refused. If no request
for a hearing is received within this time, a formal decision is issued
refusing reinstatement. If the Office receives a formal written
notification within that time that the request has been formally
withdrawn, then only an acknowledgment of receipt of the notification is
issued. If, on the other hand, the applicant asks for a hearing within
the time allowed then a hearing must be held, following which either a
decision refusing the request is issued or a conditional offer of
reinstatement is made. There is no provision for a request for
reinstatement to be opposed. \[If no reply is received within the one
month specified in the Office's letter refusing reinstatement, the
formalities manager should record the decision on COPS using function
REG F14 which will create a Journal entry. Any fees paid on un-actioned
forms, such as Forms 9A and 10 should be refunded.\]

  -----------------------------------------------------------------------
   

  **Section 20A(3)**

  The comptroller shall not reinstate the application if\
  (a) an extension remains available under this Act or rules for the
  period referred to in subsection (1) above; or\
  (b) the period referred to in subsection (1) above is set out or
  specified\
  (i) in relation to any proceedings before the comptroller;\
  (ii) for the purposes of section 5(2A)(b) above; or\
  (iii) for the purposes for a request under this section or section 117B
  below.
  -----------------------------------------------------------------------

### 20A.08 {#ref20A-08}

This subsection precludes the comptroller from reinstating an
application if it is still possible to extend a period. Rule 108
curtails (but does not exclude) the retrospective availability of
extension for certain prescribed periods. The subsection also prescribes
those periods to which subsection (1) does not apply, namely: (i)
periods which relate to inter partes proceedings before the comptroller;
(ii) the period of two months prescribed by rule 7(1) for the purposes
of filing a late declaration of priority under section 5(2); (iii) the
twelve or two month period prescribed by rule 32(1) for requesting an
extension of a period under section 20A; and (iv) the two month period
prescribed by rule 109(2) for requesting an extension under section 117B
of a period specified by the comptroller.

\[If after receiving a request for reinstatement, it is established that
an extension of time is still available, the applicant/agent should be
informed as soon as possible\].

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 20A(4)**
  Where the application was made by two or more persons jointly, a request under subsection (2) above may, with the leave of the comptroller, be made by one or more of the persons without joining the other.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 20A.09 {#a09}

Where an application has been filed in the name of two or more
applicants, it is permissible, subject to the comptroller's agreement,
for one of them to apply for a reinstatement without joining the others.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 20A(5)**
  If the application has been published under section 16 above, then the comptroller shall publish notice of a request under subsection (2) above in the prescribed manner.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 20A.10 {#a10}

r.32(4) is also relevant.

If the application for which reinstatement is requested has been
published, the comptroller is required to publish a notice of the
reinstatement request in the Patents Journal.

  ---------------------------------------------------------------------------
   
  **Section 20A(6)**
  The reinstatement of an application under this section shall be by order.
  ---------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 20A(7)**
  If an application is reinstated under this section the applicant shall comply with the requirements referred to in subsection (1) above within the further period specified by the comptroller in the order reinstating the application.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------
   
  **Section 20A(8)**
  The further period specified under subsection (7) above shall not be less than two months.
  --------------------------------------------------------------------------------------------

### 20A.11 {#ref20-11}

The order for reinstatement will specify that reinstatement is allowed
subject to the applicant, within a specified period, meeting the
requirements under the Act or rules which they failed to comply with,
leading to the application being refused, or treated as having been
refused or withdrawn. The specified period will normally be two months
from the date of the order but a longer period may be provided. If the
request for reinstatement has been published in the Journal, the final
decision allowing or refusing the request is also advertised in the
Journal and noted in the Register.

\[If it is decided to allow reinstatement the formalities manager should
inform the applicant/agent in writing and issue the order. The Head of
Administration or formalities manager should record the decision on COPS
using function REC F14 which will create the Journal entry and change
the processing status of the application.\]

### 20A.11.1 {#ref20A-11-1}

In Anning's Patent Application \[2007\] EWHC 2770 (Pat), Pumfrey J held
that the "failure to comply" that led to the application being treated
as refused was the failure to respond to the examination report, not the
failure to place the application in order for acceptance within the
compliance period ([see 20A.02](#ref20A-02)). It follows that where an
application is treated as refused at the end of the compliance period
due to failure to respond to an examination report and is subsequently
reinstated, section 20A(7) provides the applicant with a further period
to respond to the examination report. In Ali et al's Patent Application
[BL
O/264/10](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F264%2F10&submit=Go+%BB){rel="external"}
the hearing officer considered Anning and the wording of section 18(3),
and concluded that the requirement which the applicant must be given a
chance to meet following reinstatement in these circumstances is the
requirement in section 18(3) to make observations or amendments which
bring the application into compliance. Section 20A(7) provides a power
to specify a period for that requirement to be met, and to do that
properly it is necessary to specify both a new period for responding to
the examination report and a new period for overall compliance.

\[ If reinstatement is allowed in these circumstances, the order for
reinstatement should specify both a further period for responding to the
examination report and a further period for compliance. \]

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 20A(9)**
  If an application fails to comply with subsection (7) above the application shall be treated as having been withdrawn on the expiry of the period specified under that subsection.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 20A.12 {#a12}

If the applicant fails to comply with any outstanding requirements
referred to in the order for reinstatement within the two months
specified in the order, the application shall be treated as withdrawn on
the expiry of that two month period.

### Meaning of unintentional

### 20A.13 {#ref20A-13}

There is no definition in the Act or rules as to what is meant by
"unintentional" as it applies for determining whether to allow a request
for reinstatement of a patent application. In Sirna Therapeutics Inc's
Application \[2006\] RPC 12, which related to a request to make a late
declaration of priority under section 5(2B), the hearing officer
observed that the requirement to show an intention to file an
application in time differed from the test of "continual underlying
intention to proceed" that was applied in Heatex Group Ltd's Application
(\[1995\] RPC 546) in deciding whether to exercise discretion favourably
to allow a period of time to be extended under rule 108 ([see
123.37](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-37)).
However, case law under rule 108 may be of relevance in analysing the
evidence to establish the applicant's intentions. In Anning's
Application ([BL
O/374/06](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F374%2F06&submit=Go+%BB){rel="external"}),
which related to a request for reinstatement under section 20A, the
hearing officer took a similar approach and warned against the danger of
going beyond the clear meaning of the statute. He interpreted
"unintentional" according to its normal English meaning. In this case
the hearing officer held that although there was a continual underlying
intention to proceed it did not follow that the failure to reply to an
examination report was unintentional.

### 20A.14 {#ref20A-14}

Sirna Therapeutics Inc's Application \[2006\] RPC 12 and Anning's
Application ([BL
O/374/06](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F374%2F06&submit=Go+%BB){rel="external"})
established that the "continual underlying intention" test in Heatex is
not applicable in determining the meaning of the word "unintentional"
(in section 5(2B) or section 20A).

### 20A.14.1 {#ref20A-14-1}

In Green's Application [BL
O/087/09](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F087%2F09&submit=Go+%BB){rel="external"}
the applicant was unable to pay the prescribed fee required to enter his
international application into the national phase, due to severe
financial difficulties. The hearing officer held that, despite the
applicant's underlying intention to enter the application into the
national phase, the decision not to pay the prescribed fee in time was a
conscious one based on his knowledge of his impecunious state, and as
such the failure to comply could not have been unintentional.

### 20A.15 {#ref20a-15}

In Matsushita Electric Industrial Co. v Comptroller General of Patents
\[2008\] EWHC 2071 (Pat), which concerned a request for restoration
under s.28, Mr. Justice Mann gave some guidance on the level of
evidential burden required to "satisfy" the Comptroller that the failure
in section 28(3) was "unintentional". The applicant in that case chose
not to file any evidence beyond a bald assertion of the statute that the
failure to pay the renewal fee on time was unintentional. It argued that
that was all the statute required to satisfy the comptroller. It was
held by the Judge that a mere assertion that the failure to pay the
renewal fee was unintentional is not sufficient to enable the
Comptroller to determine that the requirements of s.28(3) are fulfilled.
He said:

> the Act requires a judgment to be formed by the Comptroller so that he
> can be satisfied of the relevant matters. A judgment usually has to be
> made on the basis of evidence... The evidence required in any
> particular case where satisfaction is required depends on the nature
> of the enquiry and the nature and purpose of the decision to be
> made... A significant matter requires significant proof. I repeat, the
> Act does not require a statement that the failure to pay fees was
> unintentional. It requires the Comptroller to be satisfied of that
> fact.

### 20A.16 {#a16}

It is clear from this judgment that while there is no universal rule as
to what level of evidence has to be provided to satisfy the comptroller
of the unintentional lapse in section 28(3), and by implication in
sections 5(2B) and 20A, some evidence above and beyond a bald assertion
of the law is required.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
