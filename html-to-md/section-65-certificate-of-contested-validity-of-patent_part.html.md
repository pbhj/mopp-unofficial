::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 65: Certificate of contested validity of patent {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (65.04-65.06) last updated July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 65.01

Both the court and the comptroller are empowered by this section to
grant a certificate of contested validity. A party successfully relying
on such a certificate in subsequent infringement or revocation
proceedings is, unless the court or comptroller otherwise directs,
entitled to their costs (expenses in Scotland) as detailed in s.65(2).

### 65.02

For the applicability of s.65 in relation to European patents, [see
60.02](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-02).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 65(1)**
  If in any proceedings before the court or the comptroller the validity of a patent to any extent is contested and that patent is found by the court or the comptroller to be wholly or partially valid, the court or the comptroller may certify the finding and the fact that the validity of the patent was so contested.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 65.03

The conditions in which a certificate may be issued are thus that the
validity of the patent in question is contested, at least in part, and
it is found to be valid, at least in part. This must occur in
proceedings before the court or the comptroller each of which then has
discretion to issue such a certificate certifying that validity was so
contested and the resultant finding. Validity may be put in issue in any
of the proceedings listed in s.74(1), [see
74.03](/guidance/manual-of-patent-practice-mopp/section-74-proceedings-in-which-validity-of-patent-may-be-put-in-issue/#ref74-03).

### 65.04

\[deleted\]

### 65.05 {#ref65-05}

In Brupat Ltd v Smith \[1985\] FSR 156, the proprietors of a patent
sought a certificate of contested validity in respect of two claims, the
validity of both of which had been unsuccessfully challenged. Even
though the other party had conceded the validity of one of those claims
during the course of the hearing, the court held that since issue had
been fully joined on the matter of that claim, the certificate should
cover both claims.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 65(2)**
  Where a certificate is granted under this section, then, if in any subsequent proceedings before the court or the comptroller for infringement of the patent concerned or for revocation of the patent a final order or judgment or interlocutor is made or given in favour of the party relying on the validity of the patent as found in the earlier proceedings, that party shall, unless the court or the comptroller otherwise directs, be entitled to his costs or expenses as between solicitor and own client (other than the costs or expenses of any appeal in the subsequent proceedings).
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 65.06 {#ref65-06}

In SmithKline Beecham Plc v Apotex Europe Ltd (No.2) \[2005\] FSR 24,
the Court of Appeal held that the reference to "costs as between
solicitor and own client" should be taken to mean "costs on an indemnity
basis" within the meaning of r.44.4 of the Civil Procedure Rules. Where
the patentee is issued with a certificate of validity from earlier
proceedings, and the patentee is again victorious in a subsequent action
on the validity of the patent, they can have their costs of the victory
on an indemnity basis, but only at first instance. Jacob LJ held that
the words "(other than the costs or expenses of any appeal in the
subsequent proceedings)" had the effect that the provision did not apply
to appeals because "subsequent proceedings" could not mean anything
other than the second proceedings vexing the patentee.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
