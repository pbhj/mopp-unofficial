::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 45: Determination of parts of certain contracts \[repealed with savings\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (45.01 - 45.05) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 45.01

This section provides for the termination or variation of contracts and
licences relating to patented inventions when the patents concerned have
ceased to be in force.

### 45.01.1 {#ref45-01-1}

This section, along with section 44, ceased to have effect for most
purposes from 1 March 2000 when Section 70 of the Competition Act 1998
came into force. However a saving in The Competition Act 1998
(Transitional, Consequential and Supplemental Provisions) Order 2000
means that this section will continue to apply where an application was
made or notice was given under section 45(1) or (3) before that date.
See also
[44.01.1](/guidance/manual-of-patent-practice-mopp/section-44-avoidance-of-certain-restrictive-conditions-repealed-with-savings/#ref44-01-1).

### 45.02 {#section-1}

s.77(1), Sch.2, Para 1 and s.45(5) is also relevant

The provisions of s.45 apply in relation to patents granted under the
1977 Act patents, including granted European patents (UK). It is
immaterial whether the contract or licence was made before or after the
day the section came into operation (1 June 1978).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 45(1)**
  Any contract for the supply of a patented product or licence to work a patented invention, or contract relating to any such supply or licence, may at any time after the patent or all the patents by which the product or invention was protected at the time of the making of the contract or granting of the licence has or have ceased to be in force, and notwithstanding anything to the contrary in the contract or licence or in any other contract, be determined, to the extent (and only to the extent) that the contract or licence relates to the product or invention, by either party on giving three months' notice in writing to the other party.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 45(2)**
  In subsection (1) above "patented product" and "patented invention" include respectively a product and an invention which is the subject of an application for a patent, and that subsection shall apply in relation to a patent by which any such product or invention was protected and which was granted after the time of the making of the contract or granting of the licence in question, on an application which had been filed before that time, as it applies to a patent in force at that time.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 45.03 {#section-2}

This section applies to any contract for the supply of a patented
product or licence to work a patented invention, or contract relating to
any such supply or licence. For the purposes of this section, the normal
definitions of "patented product" and "patented invention" in s.130(1)
are modified in accordance with s.45(2). After the cessation of the
patent or patents by which the product or invention was protected,
either party may on giving three months' notice in writing to the other
party terminate the contract or licence to the extent that it relates to
the product or invention.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 45(3)**
  If, on an application under this subsection made by either party to a contract or licence falling within subsection (1) above, the court is satisfied that, in consequence of the patent or patents concerned ceasing to be in force, it would be unjust to require the applicant to continue to comply with all the terms and conditions of the contract or licence, it may make such order varying those terms or conditions as, having regard to all the circumstances of the case, it thinks just as between the parties.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 45.04 {#section-3}

CPR 63.5 is also relevant

It is also possible for a party to such a contract or licence to apply
to the court for variation of its terms or conditions. Such proceedings
must be begun by issue of a claim form. The court may vary any such
terms or conditions which, in consequence of the cessation of the patent
or patents concerned and having regard to all the circumstances, it
considers to be unjust.

### 45.05 {#section-4}

\[deleted\]

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 45(4)**
  Without prejudice to any other right of recovery, nothing in subsection (1) above shall be taken to entitle any person to recover property bailed under a hire-purchase agreement (within the meaning of the Consumer Credit Act 1974).
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------
   
  **Section 45(5)**
  The foregoing provisions of this section apply to contracts and licences whether made before or after the appointed day.
  --------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 45(6)**
  The provisions of this section shall be without prejudice to any rule of law relating to the frustration of contracts and any right of determining a contract or licence exercisable apart from this section.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
