::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 111: Unauthorised claim that patent has been applied for {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (111.01 - 111-07) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 111.01

s\. 78(2) s.89(1) is also relevant

This section relates to representation that a patent has been applied
for in respect of an article, and the circumstances in which this
constitutes an offence. It applies to claims to the existence of
applications under the 1977 Act, including applications for European
patents (UK) and international applications for a patent (UK).

### 111.02

It is similar to s.110 whereby a false claim to patent rights may be an
offence.

  -----------------------------------------------------------------------
   

  **Section 111(1)**

  If a person represents that a patent has been applied for in respect of
  any article disposed of for value by him and\
  (a) no such application has been made, or\
  (b)any such application has been refused or withdrawn,\
  \
  he shall, subject to the following provisions of this section, be
  liable on summary conviction to a fine not exceeding level 3 on the
  standard scale.
  -----------------------------------------------------------------------

### 111.03

Representation by a person that a patent has been applied for in respect
of an article may be an offence if such an article is disposed of for
value by them. In order for it to be an offence, any such application
for a patent must have either been refused or withdrawn, or never been
made. This is subject to the exceptions in subsections (2) and (4), [see
111.05](#ref111-05) and [111.07](#ref111-07). With regard to what
constitutes such representation, [see 111.06](#ref111-06).

\[ Any queries relating to offences under this section should be
referred to Patents Legal Section. \]

### 111.04

The maximum fine was converted to a level on the standard scale by the
Criminal Justice Act 1982.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 111(2)**
  Subsection (1)(b) above does not apply where the representation is made (or continues to be made) before the expiry of a period which commences with the refusal or withdrawal and which is reasonably sufficient to enable the accused to take steps to ensure that the representation is not made (or does not continue to be made).
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 111.05 {#ref111-05}

There is a period of grace following the refusal or withdrawal of the
relevant application during which representation that a patent has been
applied for does not constitute an offence under this section. That
period is of a length regarded as "reasonably sufficient" for the person
in question to prevent or cease such representation.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 111(3)**
  For the purposes of subsection (1) above a person who for value disposes of an article having stamped, engraved or impressed on it or otherwise applied to it the words "patent applied for" or "patent pending", or anything expressing or implying that a patent has been applied for in respect of the article, shall be taken to represent that a patent has been applied for in respect of it.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 111.06 {#ref111-06}

Marking of an article to the effect that a patent has been applied for
in respect of it, eg by use of the words "patent applied for" or "patent
pending", is taken (for the purposes of subsection(1)) to represent that
a patent has been applied for in respect of it.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 111(4)**
  In any proceedings for an offence under this section it shall be a defence for the accused to prove that he used due diligence to prevent the commission of such an offence.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 111.07 {#ref111-07}

It is a defence for a person accused of an offence under this section to
have used due diligence to prevent its commission, but the onus is on
that person so to prove.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
