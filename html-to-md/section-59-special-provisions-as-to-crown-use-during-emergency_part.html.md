::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 59: Special provisions as to Crown use during emergency {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 59.01

This is the sixth and last of the group of sections relating to use of
patented inventions for the services of the Crown. The powers for Crown
use under s.55 ([see
55.04](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-04))
are extended by s.59 during any period of emergency as defined by
s.59(3). The meanings of "the services of the Crown" and "use for the
services of the Crown" are extended beyond their normal meanings ([see
56.03](/guidance/manual-of-patent-practice-mopp/section-56-interpretation-etc-of-provisions-about-crown-use/#ref56-03))
by s.59(1) and (2). With regard to ss.55 to 59 in general, [see
55.01-03](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-01).

  -----------------------------------------------------------------------
   

  **Section 59(1)**

  During any period of emergency within the meaning of this section the
  powers exercisable in relation to an invention by a government
  department or a person authorised by a government department under
  section 55 above shall include power to use the invention for any
  purpose which appears to the department necessary or expedient ­\
  (a) for the efficient prosecution of any war in which His Majesty may
  be engaged;\
  (b) for the maintenance of supplies and services essential to the life
  of the community;\
  (c) for securing a sufficiency of supplies and services essential to
  the well-being of the community;\
  (d) for promoting the productivity of industry, commerce and
  agriculture;\
  (e) for fostering and directing exports and reducing imports, or
  imports of any classes, from all or any countries and for redressing
  the balance of trade;\
  (f) generally for ensuring that the whole resources of the community
  are available for use, and are used, in a manner best calculated to
  serve the interests of the community; or\
  (g) for assisting the relief of suffering and the restoration and
  distribution of essential supplies and services in any country or
  territory outside the United Kingdom which is in grave distress as the
  result of war; and any reference in this Act to the services of the
  Crown shall, as respects any period of emergency, include a reference
  to those purposes.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 59(2)**
  In this section the use of an invention includes, in addition to any act constituting such use by virtue of section 55 above, any act which would, apart from that section and this section, amount to an infringement of the patent concerned or, as the case may be, give rise to a right under section 69 below to bring proceedings in respect of the application concerned, and any reference in this Act to "use for the services of the Crown" shall, as respects any period of emergency, be construed accordingly.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 59(3)**
  In this section "period of emergency" means any period beginning with such date as may be declared by Order in Council to be the commencement, and ending with such date as may be so declared to be the termination, of a period of emergency for the purposes of this section.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 59(4)**
  A draft of an Order under this section shall not be submitted to His Majesty unless it has been laid before, and approved by resolution of, each House of Parliament.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
