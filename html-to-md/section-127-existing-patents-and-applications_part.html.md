::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 127: Existing patents and applications {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (127.01 - 127.10) last updated: April 2007
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 127.01

This section provided for the position of 1949 Act patents and
applications as a result of the advent of the 1977 Act. It authorises
Schedules 1 to 4 to the 1977 Act which respectively set out which
provisions of the 1949 Act and of the 1977 Act applied in relation to
1949 Act patents and applications, repealed certain provisions of the
1949 Act and made transitional provisions. All patents granted under the
1949 Act have now expired.

  -------------------------------------------------------------------------------------------
   
  **Section 127(1)**
  No application for a patent may be made under the 1949 Act on or after the appointed day.
  -------------------------------------------------------------------------------------------

### 127.02

s.130(1) is also relevant

The "appointed day" on which s.127 came into operation was 1 June 1978.
From that day onwards, it has not been possible to make an application
for a patent under the Patents Act 1949.

  -----------------------------------------------------------------------
   

  **Section 127(2)**

  Schedule 1 to this Act shall have effect for securing that certain
  provisions of the 1949 Act shall continue to apply on and after the
  appointed day to\
  (a) a patent granted before that day:\
  (b) an application for a patent which is filed before that day, and
  which is accompanied by a complete specification or in respect of which
  a complete specification is filed before that day;\
  (c) a patent granted in pursuance of such an application.
  -----------------------------------------------------------------------

### 127.03 {#ref127-03}

Certain provisions of the 1949 Act as set out in Schedule 1 applied to
1949 Act applications for which a complete specification was filed
before 1 June 1978 and to patents granted before 1 June 1978 or in
pursuance of such an application. Those 1949 Act provisions applied
subject to various qualifications given in Schedules 1, 3 and 4.

### 127.04 {#section-2}

\[deleted\]

### 127.05 {#section-3}

\[deleted\]

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 127(3)**
  Schedule 2 to this Act shall have effect for securing that (subject to the provisions of that Schedule) certain provisions of this Act shall apply on and after the appointed day to any patent and application to which subsection (2) above relates, but, except as provided by the following provisions of this Act, this Act shall not apply to any such patent or application.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 127.06 {#ref127-06}

Certain provisions of the 1977 Act as set out in Schedule 2 have, from 1
June 1978, applied to the 1949 Act applications and patents mentioned in
[127.03](#ref127-03). Those 1977 Act provisions applied subject to
various qualifications given in Schedules 2 and 4.

### 127.07 {#section-4}

\[deleted\]

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 127(4)**
  An application for a patent which is made before the appointed day, but which does not comply with subsection (2)(b) above, shall be taken to have been abandoned immediately before that day, but, notwithstanding anything in section 5(3) above, the application may nevertheless serve to establish a priority date in relation to a later application for a patent under this Act if the date of filing the abandoned application falls within the period of fifteen months immediately preceding the filing of the later application.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 127.08 {#section-5}

1949 Act applications for which a complete specification was not filed
before 1 June 1978 were treated as abandoned. However, such an
application could provide priority for a subsequent 1977 Act application
filed within fifteen months of the date of filing of the abandoned
application.

  --------------------------------------------------------------------------------------------
   
  **Section 127(5)**
  Schedule 3 to this Act shall have effect for repealing certain provisions of the 1949 Act.
  --------------------------------------------------------------------------------------------

### 127.09 {#ref127-09}

Certain provisions of the 1949 Act which have no counterpart in the 1977
Act were repealed, as set out in Schedule 3 but subject to the
transitional provisions of Schedule 4.

  --------------------------------------------------------------------------------------
   
  **Section 127(6)**
  The transitional provisions and savings in Schedule 4 to this Act shall have effect.
  --------------------------------------------------------------------------------------

### 127.10 {#ref127-10}

Sch. 4, para 1 is also relevant

Schedule 4 makes provision for the transition from the 1949 Act to the
1977 Act. It provides that anything done under a 1949 Act provision
(since repealed by the 1977 Act) which could have been done under a
corresponding 1977 Act provision has effect as if done under the latter.
It also makes specific provision with regard to the use of patented
inventions for services of the Crown, infringement, notices of
opposition to the grant of 1949 Act patents, secrecy directions,
revocation (including appeals from the court), licences of right and
compulsory licences, convention countries, appeals from the comptroller
under continuing or repealed provisions of the 1949 Act, appeals from
the Patents Appeal Tribunal to the Court of Appeal and the power to make
rules.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 127(7)**
  In Schedules 1 to 4 to this Act "existing patent" means a patent mentioned in subsection (2)(a) and (c) above, "existing application" means an application mentioned in subsection (2)(b) above, and expressions used in the 1949 Act and those Schedules have the same meanings in those Schedules as in that Act.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
