::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 290: Limitation of costs where pecuniary claim could have been brought in patents county court \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (290.01 - 290.02) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 290.01

s.290(6) is also relevant

This section imposed a costs sanction on any claimant who without good
reason brought a case in the Patents Court which they could have brought
in the patents county court, if they recovered damages of less than a
certain figure.

### 290.02

Section 290 has been repealed by the Courts and Legal Services Act 1990.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
