::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Standard titles for online database names {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated January 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
Within the scope of the investigations made under Section 17(4) of the
Patents Act 1977 examiners use their discretion to perform searches of
selected online databases. Therefore, most reports issued under Section
17(5) contain references to online databases. It is Office practice for
each database to be identified by a short standard title, this overcomes
inconsistencies in the use of online database names and ensures the
names will fit into the space provided for the Field of Search on the
front page of published documents. The standard titles and their
definitions are listed below.

An online database may be present on more than one online host and may
be divided into a number of individually-named component files. In
general only the standard title will be used irrespective of the host
accessed for the search or the particular component files searched. It
should be noted that the presence of a standard database title does not
imply that all component files of that database, or indeed the whole of
any one component file, was searched.

It should be noted that the following names are registered trademarks:
EMBASE, INSPEC and IP.COM.

  ----------------------------------- ------------------------------------------------------------------------------------------
                                       

  **Standard title**                  **Corresponding Database and (Host)**

  AGRICOLA                            \- AGRICOLA (DIALOG, STN)

  ALLOYS                              \- ALLOYS (EPOQUE)

  BIOSIS                              \- BIOSIS Previews (DIALOG)\
                                      - BIOSIS (EPOQUE, STN)

  BIOABS                              \- BIOTECHABS (STN)

  BIOTECHNO                           \- BIOTECHNO (STN)

  BLASTN                              \- Nucleotide Blast search (STN)

  BLASTP                              \- Protein Blast search (STN)

  CABA                                \- CAB ABSTRACTS \[Commonwealth Agricultural Bureau Abstracts\] (DIALOG, STN)

  CAS ONLINE                          \- CAS REGISTRY File; graphical structure searching CA, CAOLD (STN)

  CBA                                 \- Current Biotechnology Abstracts (DIALOG)

  CHABS                               \- CAS (QUESTEL.ORBIT)\
                                      CA SEARCH, CHEMNAME, CHEMSEARCH (DIALOG)

  CLAIMS                              \- CLAIMS/CITATION, CLAIMS/REASSIGNMENT & REEXAMINATION, CLAIMS/REFERENCE, CLAIMS/US
                                      PATENT ABSTRACTS, CLAIMS/US PATENT ABSTRACTS WEEKLY,\
                                      - CLAIMS/UNITERM (DIALOG) IFIPAT, IFIUDB (QUESTEL.ORBIT, STN)

  CMP                                 \- COMPUTER FULLTEXT (DIALOG)

  COMPUTER NEWS                       \- COMPUTER NEWS FULLTEXT (DIALOG)

  COMPENDEX                           \- EiCOMPENDEX (DIALOG)\
                                      COMPENDEX (STN, QUESTEL.ORBIT)

  DGENE                               \- DGENE (STN)

  DOEE                                \- ENERGY SCIENCE AND TECHNOLOGY \[formerly DOE Energy\] (DIALOG)\
                                      - ENERGY (STN)

  EPODOC                              \- EPODOC; Documentation of the European Patent Office \[includes patent and technical
                                      literature, and since January 2005 PAJ data\] (EPOQUE)

  EMBASE                              \- EMBASE (DIALOG, STN)

  FSTA                                \- Food Science and Technology Abstracts (DIALOG)\
                                      - FSTA (QUESTEL.ORBIT, STN)

  GENEBANK                            \- GENEBANK (STN)

  GENEN                               \- Nucleotide algorithm search through a selection of EMBL databases and the GENESEQ
                                      databases (GENOMEQUEST)

  GENEP                               \- Protein algorithm search through a selection of EPO, JPO and USPTO data and the UniProt
                                      and GENESEQ databases (GENOMEQUEST)

  GEOPHYSICS                          \- Society of Exploration Geophysicists journal archive (Internet)

  IEL                                 \- IEEE Xplore (Internet)

  INPADOC                             \- INPADOC/FAMILY AND LEGAL STATUS (DIALOG)\
                                      - INPADOC (STN)

  INTERNET                            \- General Internet search

  INSPEC                              \- INSPEC (DIALOG, EPOQUE, QUESTEL.ORBIT, STN)

  IP.COM                              \- IP.COM (Internet)\
                                      - XPIPCOM (EPOQUE)

  JANE'S                              \- Jane's Defense & Aerospace News/Analysis (DIALOG)

  JAPIO                               \- Patent Abstracts of Japan \[see also PAJ\] (DIALOG, QUESTEL.ORBIT, STN)

  LFSCICOL                            \- Life Science Collection (DIALOG)

  MEDLINE                             \- MEDLINE (DIALOG, EPOQUE, QUESTEL.ORBIT, STN)

  MOBILITY                            \- 1MOBILITY, 2MOBILITY (STN)

  MARPAT                              \- MARPAT (STN)

  METADEX                             \- METADEX (DIALOG, STN)

  NAPRALERT                           \- NAPRALERT (STN)

  NPL                                 \- NPL (EPOQUE)

  OAC                                 \- Open Access Central Journal Articles from BioMed Central, Chemistry Central,
                                      PhysMathCentral (Internet)\
                                      - XPOAC (EPOQUE)

  PAJ                                 \- Patent Abstracts of Japan \[PAJ was merged into EPODOC from January 2005 and ceased to
                                      exist as a separate database on EPOQUE from November 2006\] (EPOQUE)

  PASCAL                              \- PASCAL (DIALOG, STN)

  PCI                                 \- DERWENT PATENT CITATION INDEX (DIALOG)\
                                      - DPCI (STN)

  PIRA                                \- Paper, Printing & Packaging Abstracts (DIALOG, QUESTEL.ORBIT, STN)

  RAPRA                               \- RAPRA Abstracts (DIALOG, QUESTEL.ORBIT, STN)

  RM25                                \- RM25 laminates (EPOQUE)

  RM26                                \- RM26 laminates (EPOQUE)

  SADIQ                               \- Glass compositions (EPOQUE)

  SCISEARCH                           \- SCISEARCH (DIALOG, STN)

  SPRINGER                            \- Springer Verlag Journals (EPOQUE, Internet)

  SEARCH - NPL\                       \- Non-patent literature database of the European Patent Office (subject to UK IPO active
  (CST)                               subscriptions), specific databases may also be listed

  SEARCH - PATENT\                    Patent documentation of the European Patent Office including patent and technical
  (CST)                               literature

  TCM                                 \- Traditional Chinese Medicine (EPOQUE)

  TEXTECH                             \- Textile Technology Digest (DIALOG, STN)

  TDB                                 \- IBM Technical Disclosure Bulletin (EPOQUE)

  TKDL                                \- [Indian Traditional Knowledge Digital
                                      Library](http://www.tkdl.res.in/tkdl/langdefault/common/Home.asp?GL=Eng){rel="external"}

  TXTA                                \- English language full text patent collection \[Translated and native\] (EPOQUE) - used
                                      prior to April 2017

  TXTE                                \- Native English language full text patent collection (EPOQUE) - used prior to April 2017

  TXTEN                               \- Native English language full text patent collection (EPOQUE) - used prior to April 2017

  TXTT                                \- Translated English full text patent collection (EPOQUE) - used prior to April 2017

  Patent Fulltext                     \- Selection of full-text patent databases (EPOQUE) - used from April 2017

  WPI                                 \- DERWENT WORLD PATENT INDEX (DIALOG, EPOQUE, QUESTEL.ORBIT, STN)

  WTEXT                               \- World Textiles (DIALOG)

  XP3GPP                              \- XP3GPP 3rd Generation Partnership Project (EPOQUE)

  XPAIP                               \- XPAIP American Institute of Physics (EPOQUE)

  XPESP                               \- XPESP/Science Direct, Elsevier Publishing Group (EPOQUE)

  XPIEE                               \- XPIEE Institution of Electrical Engineers (EPOQUE)

  XPIETF                              \- XPIETF Internet Engineering Taskforce (EPOQUE)

  XPIOP                               \- XPIOP Institute of Physics (EPOQUE)

  XPI3E                               \- XPI3E Institution of Electrical and Electronic Engineers (EPOQUE)

  XPJPEG                              \- XPJPEG Joint Photographic, and Joint Bi-level Image Experts Groups (EPOQUE)

  XPMISC                              \- XPMISC non-patent literature full-text from miscellaneous providers \[inc Alcatel
                                      Telecommunications Review, Siemens Technical Reports and Speech recognition Conference
                                      Proceedings\] (EPOQUE)

  XPRD                                \- XPRD Research Disclosure (EPOQUE)

  XPTK                                \- XPTK Traditional Knowledge (EPOQUE)
  ----------------------------------- ------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
