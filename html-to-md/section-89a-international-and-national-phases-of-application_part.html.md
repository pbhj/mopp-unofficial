::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 89A: International and national phases of application {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (89A.01 - 89A.26) last updated: January 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 89A.01 {#ref89A-01}

This is the second of the three sections concerned specifically with
international applications for patents. Section 89 makes provision for
an international application for a patent (UK) to be treated as an
application under the 1977 Act, subject to sections 89A and 89B ([see
89.01-05](/guidance/manual-of-patent-practice-mopp/section-89-effect-of-international-application-for-patent/#ref89-01)).
Section 89A was introduced by the CDP Act and clarifies the relationship
between the international phase, initiated by filing under the PCT, and
the UK national phase. In particular, it relates to what is often known
as "entry" into the latter phase.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 89A(1)**
  The provisions of the Patent Co-operation Treaty relating to publication, search, examination and amendment, and not those of this Act, apply to an international application for a patent (UK) during the international phase of the application.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 89A.02 {#a02}

PCT provisions relating to publication, search, examination and
amendment, rather than those of the 1977 Act, apply to the application
during the international phase.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 89A(2)**
  The international phase of the application means the period from the filing of the application in accordance with the Treaty until the national phase of the application begins.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 89A.03 {#a03}

The international phase runs from filing under the PCT until the
national phase begins, the beginning of the national phase being defined
in subsection (3).

  -----------------------------------------------------------------------
   

  **Section 89A(3)**

  The national phase of the application begins-\
  (a) when the prescribed period expires, provided any necessary
  translation of the application into English has been filed at the
  Patent Office and the prescribed fee has been paid by the applicant;
  or\
  (b) on the applicant expressly requesting the comptroller to proceed
  earlier with the national phase of the application, filing at the
  Patent Office\
  (i) a copy of the application, if none has yet been sent to the Patent
  Office in accordance with the Treaty, and\
  (ii) any necessary translation of the application into English,\
  \
  and paying the prescribed fee.
  -----------------------------------------------------------------------

### 89A.03.1 {#a031}

PCT a.3(2) is also relevant

For this purpose a "copy of the application" includes a copy published
in accordance with the Treaty in a language other than that in which it
was originally filed. Where the application has not been published in
accordance with the Treaty, the "copy of the application" must include a
copy of the PCT request form (Form PCT/RO/101), the specification and
the abstract.

### Entering the National Phase

### 89A.04 {#ref89A-04}

PCT r.49.4 is also relevant

Applicants are strongly recommended to enter the national phase by
filing Form NP1, which has spaces for details of the international
application, details of the applicant and the required address for
service. However, the use of this form is not obligatory.

### 89A.04.1 {#a041}

If, as in the vast majority of cases, there is no request for early
entry into the national phase, the application enters the national phase
at the expiry of the prescribed period in accordance with s.89A(3)(a)
provided that the requirements of this sub-section are satisfied.

\[ On receipt, the application is allocated a UK application number and
the appropriate label is applied to the cover of the PDAX dossier. A
check is made as to whether early entry into the national phase under
s.89A(3)(b) has been expressly requested ([see also
89A.21](#ref89A-21)). If it has, the procedure outlined in
[89A.16-21](#ref89A-16) should be followed. If there is any uncertainty
in the matter, the case should be referred to the appropriate legal
adviser in the Registered Rights Legal Team for instructions on the
subsequent procedure. \]

### 89A.05 {#ref89A-05}

r.66(1) and PCT a.22 is also relevant

The prescribed period for entry into the national phase is (since 1
April 2002) thirty- one months from the priority date declared under the
PCT or from the date of filing of the international application if there
is no declared priority date. This period is extensible in two month
tranches, but not after the expiry of the period as extended, in
accordance with r.108(2) or (3) and (4) to (7) - [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34)
and in particular see the discussion of Meunier's International
Application (BL O/013/01) in
[123.37](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-37).

### Entry into the national phase at the expiry of the prescribed period

### 89A.06 {#a06}

r.65(1) and r.66(1) is also relevant

Where there is no express request for early entry into the national
phase, the application is examined by a formalities examiner to
determine whether it complies with the requirements of s.89A(3)(a). When
the international application is in English or Welsh, the only
requirement for entry into the national phase at the end of the
prescribed period under s.89A(3)(a) is the payment of the prescribed fee
before the expiry of the prescribed period. However, if the
international application is not in English or Welsh, the applicant must
also file any necessary translations ([see 89A.08](#ref89A-08)) before
the expiry of the prescribed period. As noted in [89A.05](#ref89A-05)
this period may be extended.

### 89A.06.1 {#a061}

Between 1 October 1998 and 3 May 1999 inclusive, the national fee
prescribed under s.89A(3) was zero (The Patents (Fees) Rules 1998,
amended by The Patents (Fees) (Amendment) Rules 1999). As a consequence,
any international application in the English language which designated
the UK whose s.89A(3)(a) prescribed period ended within these dates will
have entered the national phase in the UK, and thus will form part of
the state of the art for the purposes of s.2(3), without further action
by the applicant, unless the application or the designation of the UK in
it was withdrawn before the expiry of the prescribed period. The Office
allotted application numbers to such applications only if the applicant
filed Form NP1 ([see 89A.04](#ref89A-04)) or indicated in writing in
some other way that they intended to enter the UK national phase.

### 89A.07 {#ref89A-07}

PCT a.20 and PCT a.36(3)(a) is also relevant

The applicant is not required to file a copy of the international
application or of the International Search Report as these are
communicated to designated Offices by making them available on WIPO's
[Patentscope
database](https://patentscope.wipo.int/search/en/search.jsf){rel="external"}
database, normally in the form of a WO pamphlet. The documents made
available on Patentscope will also include the International Preliminary
Report on Patentability (IPRP) and annexes thereto; any amendments to
the claims filed with WIPO under PCT a.19 and a copy of any statement
under a.19 explaining the amendments and indicating any impact on the
description and drawings (these documents are included in the WO
pamphlet); any Supplementary International Search Reports; and any third
party observations submitted in the international phase. If the WO
pamphlet is not available when national processing is due to commence,
the National Phase Officer will establish with WIPO when it can be
expected.

### 89A.08 {#ref89A-08}

r.69, r.70 ,PCT r.48.3, PCT r.47.1(c), PCT r.49.5(d) is also relevant

Where the international application has been filed in a foreign language
other than Arabic, Chinese, French, German, Japanese, Korean,
Portuguese, Russian or Spanish, and has been published by WIPO as a WO
pamphlet in English translation and the WO pamphlet has been
communicated to the Office under PCT a.20 before the expiry of the
thirty-one month period for entry into the national phase, then no
further translation of the application or of any amendments under PCT
a.19 need be filed. A notice under PCT r.47 is sent by WIPO to the
applicant indicating the designated Offices to which the communication
under PCT a.20 has been sent and the date of the communication. This
notice must be accepted as conclusive evidence that the communication
was sent on the date specified. In other instances where the application
is not published in English, or exceptionally the WO pamphlet has not
been communicated, the applicant must provide a translation into English
of the application as published under the PCT (including any textual
matter in the drawings) and of any amendments before the expiry of the
prescribed period in order to gain entry into the national phase,
subject to the exceptions set out in [89A.09](#ref89A-09).

### 89A.09 {#ref89A-09}

Exceptions to the requirement to file a translation of the application
as originally filed and of any amendments are:\
\
r. 69(2), (3) is also relevant\
\
(a) the request for the international application on Form PCT/RO/101 and
the abstract. A translation of these must be included when the applicant
expressly requests the comptroller to proceed with the national phase
before the end of the period prescribed by rule 66(1) but not where a
copy of the application, as published in accordance with the PCT is
available to the comptroller (since the request and abstract are always
published in English in the WO pamphlet).\
\
r.70(4) is also relevant\
\
(b) the original title if this has since been amended by the
international searching authority; however, a translation of the amended
title must be supplied instead.\
\
r.70(5), PCTr.49.5(a-bis) is also relevant\
\
(c) any sequence listing complying with the relevant requirements of the
PCT (PCT r.12.1(d)), provided any free text in the sequence listing also
appears in the main part of the description in accordance with PCT
r.5.2(b).

### 89A.09.1 {#a091}

r.69(5), r.66(2) is also relevant

Where the application has been amended and the applicant has filed a
translation of the application as originally filed but not of the
amendment, and the prescribed fee has been paid, notice is given to the
applicant that a necessary translation is missing and requiring the
necessary translation to be filed within three months of the date of
such notice, in accordance with rule 66(2). ([See also
89A.25](#ref89A-25))

\[ The formalities examiner should notify the applicant that the
necessary translation should be filed whenever the application has been
amended and the applicant has not filed a translation of the amendment.
\]

### 89A.10 {#ref89A-10}

PR Sch.1, para 3(2) r.69(1) r.66(2) is also relevant.

In addition, where information in a language other than English
regarding the deposit of biological material has been provided and a
translation thereof has not been filed within the period prescribed by
rule 66(1), the comptroller must notify the applicant that this
translation is missing and should be filed within three months of the
date the notification is sent. The information in question is the name
of the depositary institution and the accession number of the deposit,
and (where the biological material has been deposited by a person other
than the applicant) the name and address of the depositor and the
statement referred to in paragraph 3(2)(b)(ii) of Schedule 1 to the
Patents Rules 2007.

\[ When information concerning the deposit of biological material is
filed after the international filing date, the date this information was
received is published on the front page of the PCT pamphlet. The
formalities examiner will check the PCT pamphlet to see whether such
information has been received after the international filing date. If it
has and this information is not in English, the formalities examiner
will notify the applicant that a translation of that information must be
filed, in accordance with rule 69(1), within three months of the date on
which the notice is sent. \]

### 89A.11 {#a11}

If the formalities examiner is of the opinion that at the end of the
prescribed period (including any extension under r.107 or r.108) the
requirements of s.89A(3)(a) are not satisfied, the applicant should be
notified accordingly and informed that, subject to any comments received
within one month of the notification, it is proposed to treat the
application as withdrawn.

### 89A.12 {#ref89A-12}

r.68(3), r.68(2) is also relevant.

In order for the application to proceed, the following should be
provided:

\(i\) a request for search on Patents Form 9A accompanied by the
appropriate fee, including any excess claims fees, -- these should be
filed before the end of the period prescribed by rule 22(2) and (7) or,
if it expires later, the period of two months after the national phase
begins;

\(ii\) a statement of inventorship on Patents Form 7 (no fee), if the
name(s) and address(es) of the inventor(s) have not been disclosed in
the request for international processing made in accordance with Article
4 of the PCT. The statement of inventorship should be filed before the
end of the period prescribed by rule 10(3) or, if it expires later, the
period of two months after the national phase begins ([see
13.12](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-12));

### 89A.12.1 {#a121}

r.5, r.67 is also relevant

Where the invention has been displayed at an international exhibition
within the period of six months immediately preceding the filing date of
the application, a certificate and statement identifying the invention
should be filed in accordance with rule 5. The certificate and statement
should be filed within four months of the date of filing, except where
the applicant, on filing the application, informed the receiving office
under the PCT in writing of the display at an international exhibition,
in which case the certificate and statement should be filed within two
months of the date the national phase begins. This time period may be
extended at the comptroller's discretion under r.108(1) - [see
123.34-42](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).

### 89A.12.2 {#a122}

r.9 is also relevant

A translation of any priority document in a language other than English
or Welsh may also be required within the normal period ([see 5.11 to
5.13.2](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-11)).

### 89A.13 {#a13}

\[deleted\]

### 89A.14 {#ref89A-14}

When the formalities examiner is satisfied that the requirements for
entry into the national phase have been met and Patents Form 9A has been
filed, the application is allocated to an examiner assistants for CPC
classification to be applied, the IPC to be checked and necessary data
capture. However, if there is no CPC data available or the application
is accepted for acceleration, then reclassification is instead carried
out by an examiner.

\[If there is no CPC data available or where an application is accepted
for accelerated examination (see 18.07-18.07.2 and 17.05.1), the
application will be forwarded by the examiner assistant to an
examination group. In this case, CPC classifications will be handled by
an examiner.\]

### 89A.14.1 {#ref89A-14-1}

The examiner assistant should check
[Patentscope](https://patentscope.wipo.int/search/en/search.jsf){rel="external"}
for the presence of an International Search Report and International
Preliminary Report on Patentability (IPRP), where these have not already
been received from the International Bureau and imported onto the PDAX
dossier. A check should also be made for any Supplementary International
Search Reports, third party observations and applicant comments in
response to such observations. If such documents are found, the examiner
assistant should import these documents onto the dossier

\[The EA should apply the Field of Search, Citations & IPC symbols from
the WIPO front page to the GB application in PROSE. If there is CPC data
on Espacenet but there is not a CPC symbol in the primary IPC subclass,
a PDAX message should be sent to the ESO "REALLOCATE TO \[GROUP\] FOR
\[FIRST CPC SUBCLASS\]". The ESO will then reallocate the case on PAFS
and send a message back to the EA PCT mailbox. If there isn't any CPC
data on Espacenet, a minute should be added to the PDAX dossier and an
urgent PDAX message sent to the subclass mailbox "URGENT \[SUBCLASS\]
PCT RECLASS REQUIRED + PSM" for examiner action.\]

### 89A.14.2 {#ref89A-14-2}

The application is then forwarded for re-publication under a UK
Adocument publication number with the re-published document bearing
relevant bibliographic information from the WO pamphlet including IPC
(revised if necessary), field of search and citations as reported by the
international searching authority. If the PCT application is in English
and the national phase has started at the end of the prescribed period
the re-published A-document consists of a front page only. If it is not
in English, the A-document includes the translation of the specification
together with any translation filed under s.89A(5) of amendments
included in the WO pamphlet, the front page and the ISR in English.
However, any amendments to the claims which are not included in the WO
pamphlet (including claims filed at the Office in its capacity as
designated office under PCT a.28 or elected office under PCT a.41)
should not be included in the re-published document. This re-publication
is an administrative act, ie it is not required by the 1977 Act or the
Rules or the PCT. It thus does not constitute publication under s.16.
Publication under s.16 is deemed to have occurred by virtue of
publication of the WO pamphlet by WIPO and once the conditions for
entering the national phase are met - [see
89B.04](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-04).

\[ In general, applications should not be retained in an examining group
longer than 2 weeks for classification. Classification and
re-publication should not be delayed if priority documents or an
International Preliminary Report on Patentability (IPRP) are not on
file. Formalities should issue any necessary reminders to WIPO in
respect of outstanding documents. On receipt the documents should be
sent to Index & Scan to be scanned onto the dossier. \]

\[ Where the WIPO specification is in the form of an 'A2' document (that
is, published without a search report) then classification and
re-publication should not be delayed to await the publication of the
international search report as an 'A3' document ('A3' documents are not
normally sent to examining groups). Nevertheless, the examiner assistant
should check on EPOQUE to see if an International Search Report has
become available, and if so should enter the data from it in PROSE. If
it is not available, "Not yet advised" should be entered for 'Field of
Search' and 'Citations' on PROSE. In such cases, it will be necessary to
check for a search report at the examination stage and to enter the
field of search and citation information into COPS as data for
B-publication. The examiner assistant should therefore create a minute
reminding the substantive examiner to enter the necessary data. After
checking for CPC data on Espacenet, if classification to IPC and CPC is
not possible based on available information, the case should be sent to
an examiner for reclassification.\]

\[If a "Declaration of Non-establishment of International Search Report"
has been made by the International Search Authority, this should be
reflected in the 'Field of search' entry on PROSE with reference to the
relevant PCT Article indicated in the Declaration; e.g. "No search
performed: PCT Article 17(2)(a)". For the 'citations' field, "None"
should be entered. \]

\[ If only a partial search has been performed by the International
Search Authority (ISA) then details of the fields searched should still
be recorded on PROSE. If the ISA has performed only a partial search or
no search at all and the substantive examiner considers that the claims
may relate to a patentable invention, then it is likely that a complete
search will be required at the substantive examination stage.\]

\[Where there is a Supplementary International Search Report on file,
the fields of search and citations indicated in that report should also
be recorded on PROSE.\]

\[ The abstract title will already have been input into OPTICS by New
Applications Section, and should not be amended. If the title has been
incorrectly input, the matter should be referred to Index & Scan for
correction. \]

\[ If any drawing accompanying the abstract on the front page of a WIPO
specification includes text in a foreign language, the formalities
examiner should (a) ensure that translated drawings are on file, (b)
identify the translated drawing corresponding to that shown on the front
page of the WIPO specification, (c) flag the relevant drawing and (d)
minute the case examiner for confirmation that the flagged drawing is
the one that should accompany the abstract for re-publication. The case
examiner should confirm that the correct drawing has been identified or
indicate which is the correct drawing. On return to the formalities
examiner the application should be forwarded for re-publication with the
correct drawing flagged for the attention of Publication Section. \]

\[ If the wrong abstract has been used on the front page of the WIPO
specification (i.e. where the abstract does not relate to invention
described in the specification), the examiner should notify formalities.
The formalities examiner should contact WIPO to inform them of the error
and to request that the correct abstract be made available on
[Patentscope](https://patentscope.wipo.int/search/en/search.jsf){rel="external"}
(contact details for the relevant processing team at WIPO can be
obtained from
[www.wipo.int/pctdb/en/iateamlookup.jsp](www.wipo.int/pctdb/en/iateamlookup.jsp)).
Once the correct abstract is available the formalities examiner should
import a copy into the dossier from
[Patentscope](https://patentscope.wipo.int/search/en/search.jsf){rel="external"},
giving it a document code WIPOFP so that the correct abstract is used
for republication. The examiner should be consulted if the formalities
examiner is in any doubt about whether the abstract on
[Patentscope](https://patentscope.wipo.int/search/en/search.jsf){rel="external"}
is the correct one. \]

\[ The full data for OPTICS that has to be entered on PROSE to allow the
case to proceed to republication in the national phase is:

-   Field of Search
-   Citations
-   IPC classification
-   Processing status

CPC classification should also be entered. ClassTool should be used to
enter IPC and CPC classification data. Classification to the UKC is no
longer required.

Field of search and citation data should be entered using the PROSE
entry screens. Any IPC field of search information on the International
Search Report (and any Supplementary International Search Reports)
should be entered after selecting the IPC tab. All other field of search
data including US classification terms and databases (but not search
words used) should be entered under the Other tab. [See
17.75](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-75)
for the formats which should be used for different types of citation.

The examiner assistant should ensure that the 5 data markers have been
activated at the bottom right of the PROSE screen.

A REPUBP checklist should be created on PROSE and signed off to set the
processing status for republication.\]

\[ Although Form 9A has been filed, no search is made at this stage
since the results of the international search (updated at the
substantive examination stage) are utilised during substantive
examination. Nevertheless, the examiner assistant should record a search
as having been done. \]

\[ It is no longer possible to issue an abbreviated examination report
(AER) based upon an international preliminary report on patentability at
PCT reclassification stage.\]

### 89A.15 {#a15}

If the formalities examiner is of the opinion that any of the
requirements listed in [89A.12 or 89A.12.1](#ref89A-12) have not been
complied with, the applicant should be informed as soon as possible of
the need for compliance within the prescribed period. Any request for
extension under r.108(1) in respect of requirement to file an
international exhibition certificate should be considered on its merits
([see
123.36-123.39](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36)).

### Early entry into the national phase

### 89A.16 {#ref89A-16}

s.89A(3) is also relevant

Where the applicant has expressly requested early entry into the
national phase, the formalities examiner should establish whether the
requirements prescribed in s.89A(3)(b) have been satisfied, namely
whether

-   the prescribed fee has been paid

-   a copy of the application has either been communicated to the Office
    by WIPO ([see 89A.07](#ref89A-07)) or has been filed directly with
    the Office by the applicant; and

-   any necessary translation of the application into English has been
    filed at the Office.

PCT a.3(2) is also relevant

The copy of the application may be a copy published in accordance with
the PCT (ie a WO pamphlet) in a language other than that in which it was
originally filed. Where the application has not been published in
accordance with the Treaty, the "copy of the application" must include a
copy of the PCT request form (Form PCT/RO/101), the specification and
the abstract. A translation of the application into English is required
where the application is in Arabic, Chinese, French, German, Japanese,
Korean, Portuguese, Russian or Spanish or where the WO pamphlet
containing the English translation referred to in [89A.08](#ref89A-08)
has not been published or has not been communicated to the Office under
PCT a.20 ([see 89A.09-89A.10](#ref89A-09)).

### 89A.17 {#a17}

Where any of these requirements have not been met, the applicant should
be notified accordingly and informed that early entry into the national
phase cannot occur until they are met. The applicant should also be
informed that the application will be treated as withdrawn under
s.89A(4) if the requirements are not met before the expiry of the
prescribed period of Section 89A(3)(a), including any extension ([see
89A.05](#ref89A-05)).

### 89A.18 {#ref89A-18}

r.68(1)-(3) is also relevant

In order for the application to proceed once it has gained early entry
into the national phase, it is also necessary for the relevant
requirements listed in [89A.12 and 89A.12.1](#ref89A-12) to be
satisfied. However, the periods for paying the application fee and
filing Patents Form 7 and 9A are determined by r.68. Form 7 should be
filed within the last to expire of the period prescribed by r.10(3) or
two months from the date on which the national phase begins. Similarly,
the application fee (currently set at zero for international
applications) and Form 9A should be filed within the last to expire of
the period prescribed by r.22(2) and (7) or two months from the date on
which the national phase begins. These periods are extensible in two
month tranches, but any extension is not available after the end of the
period of two months beginning immediately after the expiry of the
period to be extended, in accordance with r.108(2) or (3) and (4) to
(7) - [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).

### 89A.19 {#ref89A-19}

S.89A(1) is also relevant

If the international application has been published with an
international search report by the International Bureau by the date of
early entry into the national phase, the application is re-published in
the normal way ([see 89A.14-89A14.2](#ref89A14)). If however the
international search report is not available, either from the
International Bureau or from the applicant, and the applicant has not
requested a full search under s.17, classification and re-publication
should not be delayed to await publication of the international search
report.

\[If the international search report is not available and the applicant
has not requested a full search under s.17, "Not yet advised" should be
entered for 'Field of Search' and 'Citations' on PROSE. In such cases,
it will be necessary to check for a search report at the examination
stage and to enter the field of search and citation information into
COPS as data for B-publication. The examiner should therefore create a
minute reminding the substantive examiner to enter the necessary data.\]

### 89A.19.1 {#a191}

Where the international search report is not available, the applicant
has the option of requesting that a full search be done under s.17, as
for a domestic UK application. The full search fee will be payable (see
89B.08). Re-publication of the whole WO specification together with the
UK external search report will then take place. This practice remains
the same even if the international search report has become available
after the s.17 search was commenced but before preparations for
publication are complete. However, a request for a full search under
s.17 should not be accepted if the international search report is
available; in such cases the examiner should arrange for the excess fee
to be refunded. Similarly, if the international search report becomes
available after the request for a full search under s.17, but before the
examiner commences the search, the excess fee should be refunded - [see
89B.08](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-08).

\[In cases where a full UK search under s.17 is carried out on a PCT
application without an ISR, the search, citation and classification data
reported by the UK search examiner should be entered via PROSE in the
usual way for inclusion on the front page of the specification.\]

### 89A.20 {#a20}

If the international application has not been published by the
International Bureau by the date of early entry into the national phase,
then, in the absence of a request for accelerated publication, the usual
re-publication of the application will have to wait until the WO
pamphlet becomes available. On no account should it be assumed that
accelerated publication is required unless an explicit request has been
made. In accelerated cases where it is not clear whether earlier
publication is desired in order to secure early grant, the applicant
should be contacted by telephone to clarify their intentions (see also
[18.07](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07)).

### 89A.20.1 {#ref89A-20-1}

Where the applicant does not wish to await publication by the
International Bureau they may explicitly request accelerated publication
of the application. If the international search report is available,
then the application should have accelerated publication under s.16 in
the same way as a domestic UK application, but including the
international search report instead of a domestic external search report
(even if the international search report has not yet been published by
the International Bureau). No subsequent re-publication of the WO
pamphlet is required. If the international search report has been
established in a language other than English and no translation is
available at the time that accelerated publication is requested, the
applicant should be asked under rule 113(1) to provide such a
translation for publication. Otherwise, publication must wait until the
English translation is available from WIPO. If the international search
report is not available, then accelerated publication cannot take place
until the international search report becomes available, unless the
applicant also makes a request for a full search under s.17. The full
search fee will then be payable ([see
89B.08](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-08)),
and the application will be published under s.16 in the usual way for
domestic UK applications, including the UK external search report.
Again, no subsequent re-publication of the WO pamphlet is required.
However, see
[89B.08](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-08)
for the situation where the international search report becomes
available before the examiner commences the search. Where the applicant
has requested a full search, the application may be processed as a CS&E
if Forms 9A and 10 have been filed on the same date or a specific
request has been made when filing Form 10 ([see
18.03](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-03))
For restrictions on the earliest date a s.18(4) report may be issued,
including allowing time for third party observations, see
[18.07.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07-2).

\[In cases where accelerated publication of an unpublished PCT
application is requested and the international search report is
available, classification data (revised if necessary), field of search
and citations as reported by the international search authority will be
entered via PROSE by the examiner assistant and the abstract should be
reframed by the examiner ([see
14.170-191](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-170)).
If the international search report is not available and a full UK search
under s.17 is carried out, the search, citation and classification data
should be entered via PROSE in the normal way, and again the abstract
should be reframed (if necessary).\]

### 89A.21 {#ref89A-21}

A specific request for accelerated examination made before the expiry of
the prescribed period is considered to be an express request for early
entry into the national phase. However, accelerated processing does not
follow early entry into the national phase unless a specific request is
made and allowed for accelerated search ([see
17.05-12](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-05)),
accelerated publication ([see
16.04](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-04))
or accelerated examination ([see
18.07-18.07.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07)).
Where a request for accelerated examination has been accepted then the
examination may be carried out at the same time as the search (or
classification). Where an acceleration request is accepted on a PCT
application, the substantive examiner (rather than the examiner
assistant) is required to carry out reclassification procedures (see
[89A.14](#ref89A-14) and [89A.20.1](#ref89A-20-1)). For accelerated
processing under the PCT(UK) Fast Track, [see
89B.17](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-17).
For restrictions on the earliest date a s.18(4) report may be issued,
including allowing time for third party observations, [see
18.07.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07-2).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 89A(4)**
  If the prescribed period expires without the conditions mentioned in subsection (3)(a) being satisfied, the application shall be taken to be withdrawn.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------

### 89A.22 {#a22}

The application for a UK national patent is taken to be withdrawn if the
conditions in subsection (3)(a) are not met within the prescribed period
to which [89A.05](#ref89A-05) refers.

  -----------------------------------------------------------------------
   

  **Section 89A(5)**

  Where during the international phase the application is amended in
  accordance with the Treaty, the amendment shall be treated as made
  under this Act if\
  (a) when the prescribed period expires, any necessary translation of
  the amendment into English has been filed at the Patent Office, or\
  (b) where the applicant expressly requests the comptroller to proceed
  earlier with the national phase of the application, there is then filed
  at the Patent Office\
  (i) a copy of the amendment, if none has yet been sent to the Patent
  Office in accordance with the Treaty, and\
  (ii) any necessary translation of the amendment into English;\
  \
  otherwise the amendment shall be disregarded.
  -----------------------------------------------------------------------

### 89A.23 {#a23}

PCT aa.19 & 34 is also relevant

Any amendments made during the international phase should be taken into
account during examination in the national phase, providing the
amendments and any necessary translations have either been communicated
to the Office by the International Bureau, or filed at the Office by the
applicant. If the application has been amended during the international
phase but the amendments and any necessary translations have neither
been communicated to the Office by the International Bureau, nor filed
at the Office by the applicant, the application is allowed to proceed in
its unamended form into the national phase -- [see 89A.25](#ref89A-25).

### 89A.24 {#a24}

r.66(2) is also relevant

Where an applicant is required to file a translation into English both
of an application as originally filed and of the amendment to it and
where at the expiry of the 31 month period for entry into the national
phase ([see 89A.05](#ref89A-05)) the prescribed fee has been paid and a
translation of the application has been filed but a translation of the
amendment has not been filed, the comptroller gives notice to the
applicant requiring that the required translation be filed within three
months of the date on which the notice is sent, and the 31 month period
is treated in respect of that translation as not expiring until the end
of the period specified in the notice. The period for filing a missing
translation is extensible in two month tranches, but any extension is
not available after the end of the period of two months beginning
immediately after the expiry of the period to be extended, in accordance
with r.108(2) or (3) and (4) to (7) - [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).

### 89A.25 {#ref89A-25}

r.69, 70 is also relevant

The amendments are disregarded unless the requirements of subsection (5)
are met; if they are met, such amendments are treated as if made under
the 1977 Act. The requirements in question are set out under (a) or (b)
in the subsection and are concerned in effect with the amendment and any
necessary translation being available at the time of entry into the
national phase. Amendments other than in English are marked "to be
disregarded" if a translation has not been received in the Office when
the national phase starts. If an amendment is to be disregarded the
applicant should be notified accordingly and invited to re-submit it (in
duplicate for applications filed before 26 June 2006), under s.19 and
r.66A.

\[ Where a translation of an amendment is not filed by the end of the
prescribed period, the application should be referred to the appropriate
Formalities Manager who may decide to consult the appropriate heading
examiner before marking the amendment as disregarded. \]

### 89A.25.1 {#a251}

For details of when an international application may be amended upon
entry to or during the national phase, [see
19.15.1](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-15-1).

  ----------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 89A(6)**
  The comptroller shall on payment of the prescribed fee publish any translation filed at the Patent Office under subsection (3) or (5) above.
  ----------------------------------------------------------------------------------------------------------------------------------------------

### 89A.26 {#ref89A-26}

In order to secure certain rights as set out in subsection (3) of s.89B,
the applicant may under subsection (6) of s.89A pay a prescribed fee for
publication of a translation filed under section 89A(3) or (5). The
Office automatically publishes translations supplied in accordance with
s.89A(3) or (5) ([see 89A.14.2 above](#ref89A-14-2)) but, in order to
obtain the protection to which s.89B(3) refers when the international
application was not in English, the applicant should file a written
request, accompanied by the appropriate fee, that publication for the
purposes of s.89A(6) is sought.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
