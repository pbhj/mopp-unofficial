::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 102A: Right of audience, etc., in proceedings on appeal from the comptroller \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (102A.01 - 102A.02) last updated: April 2011.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 102A.01 {#a01}

This section provided the rights of audience and communications in
proceedings on appeal from the Comptroller. This section was added to
the 1977 Act by the CDP Act but derives from the previous s.102, see
102.01.

### 102A.02 {#a02}

Section 102A has been repealed by the Legal Services Act 2007, section
210 and Schedule 23. The new regime established by the Legal Services
Act 2007 now gives the definitions and specific authorisation for
parties to take part in reserved legal activities such as the right of
audience in proceedings.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
