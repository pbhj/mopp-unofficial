::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Glossary of terms and abbreviations used in this Manual {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated January 2019.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  --------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Term                              Definition
  1949 Act                          The Patents Act 1949 (c.87). The authority under which patents were granted prior to the 1977 Act
  1977 Act                          The Patents Act 1977 (c.37). The authority under which patents are presently granted and enforced in the UK
  a\.                               Article. Part of a treaty or convention (eg EPC, PCT), which will usually be specified
  All ER                            All England Reports. A series of law reports
  BL number                         Decisions of the Intellectual Property Office and Patents Court are indexed according to "BL numbers" in the format "BL O/nnn/yy" and "BL C/nnn/yy" respectively. BL refers to the British Library Science Technology and Business section, which houses the UK national patent library. This was previously called the Science Reference and Information Service, and BL numbers used to be called "SRIS numbers" for this reason
  CDP Act                           The Copyright, Designs and Patents Act 1988 (c.48)
  CIPA                              The Chartered Institute of Patent Attorneys
  CMLR                              Common Market Law Reports
  COPS                              An Intellectual Property Office computer system.
  CPC                               Community Patent Convention. A convention to establish a unitary patent covering the entire EC, which has never come into force
  CPR                               Civil Procedure Rules. A procedural code for the courts, with the overriding objective of enabling the court to deal with cases justly
  EPC                               European Patent Convention. A convention allowing the grant of patents covering one or more countries within the convention by the European Patent Office. A single application may lead to grant of a patent in each country designated, but once granted the patents are treated in the same way as a set of national applications
  EPC \[1973\]                      The European Patent Convention of 1973 which was the European Patent Convention which was in force prior to 13 December 2007
  EPO                               European Patent Office. An intergovernmental organisation (not part of the EU) set up to administer the EPC and grant European Patents
  EPOQUE                            A portal for online database searching provided by the EPO
  EPOR                              European Patent Office Reports. A series of law reports published by Sweet and Maxwell covering cases heard by the European Patent Office Boards of Appeal
  European Patent Bulletin          A journal published by the European Patent Office giving information relating to European patents and applications
  EWCA Civ                          Neutral citation assigned to judgments of the Civil Division of the Court of Appeal
  EWHC number (Pat)                 Neutral citation assigned to judgments of the Patents Court
  Ex parte hearing                  A hearing on an issue solely between an applicant and the comptroller. Also sometimes referred to as a "without-notice" hearing
  FSR                               Fleet Street Reports. A series of law reports relating to intellectual property decisions published by Sweet and Maxwell
  G nn/yy                           A decision of the EPO Enlarged Board of Appeal
  Intellectual Property Office      An operating name of the Patent Office
  Inter partes hearing              A hearing by the comptroller of an issue between two parties. Also sometimes referred to as a "with-notice" hearing
  Ipsum                             The Intellectual Property Office's online patent information and document inspection service
  J nn/yy                           A decision of the EPO Legal Board of Appeal
  OJEPO                             Official Journal of the European Patent Office. A journal giving official information from the European Patent Office
  OJ(P)                             Official Journal (Patents). The original name for the Patents Journal (later called the Patents and Designs Journal (PDJ))
  PA                                Private applicant - an individual or small business applying for a patent personally rather than through the services of a patent agent
  PA 1977                           The Patents Act 1977
  Paris Convention                  The Paris Convention for the Protection of Industrial Property. An agreement concluded in 1883 and updated several times since, providing for national treatment, right of priority and common rules between states for patents and other forms of intellectual property
  PCT                               Patent Cooperation Treaty. A system to aid filing in many different states by initially filing a single "international" application, which after search, publication and optionally examination may be converted into a series of national applications
  PDAX                              An Intellectual Property Office computer system
  PDJ                               Patents and Designs Journal. Now published separately as the Designs Journal and the Patents Journal: a journal published by the Intellectual Property Office giving notices and information concerning UK patents and applications
  PLT                               Patent Law Treaty. A treaty agreed in 2000 to harmonise the formal requirements for filing patent applications
  PROSE                             An Intellectual Property Office computer system
  r\.                               Rule. Part of the Patents Rules 2007 unless otherwise specified
  RPC                               Reports of Patent, Design and Trade Mark Cases - a series of law reports relating to intellectual property cases, published on behalf of the Intellectual Property Office, currently by Oxford University Press
  RSC                               Rules of the Supreme Court. Now substantially replaced by the Civil Procedure Rules (CPR)
  s\.                               Section. Part of the Patents Act 1977 unless otherwise specified
  SHB                               Secretariat Hearing Box. A system for numbering records of Office hearings. Now discontinued and replaced by BL number
  SPC                               Supplementary Protection Certificate. Additional protection taking effect after the expiry of a patent for medicinal or plant protection products to compensate the patentee for loss of effective protection due to the time taken to gain regulatory approval
  SRIS number                       See BL number
  TRIPS Agreement                   The Agreement on Trade Related Aspects of Intellectual Property Rights, originating from the Uruguay Round of trade negotiations setting up the WTO and completed in 1994. The TRIPS Agreement requires WTO Members to provide minimum standards of protection for a wide range of intellectual property rights
  T nn/yy                           A decision of the EPO Technical Board of Appeal
  UK Intellectual Property Office   A former operating name of the Patent Office, now known as the Intellectual Property Office
  V nn/yy                           A decision of the EPO Opposition Division
  WIPO                              World Intellectual Property Organisation. An intergovernmental organization with headquarters in Geneva, Switzerland, responsible for the promotion of the protection of intellectual property throughout the world through cooperation among States, and for the administration of various multilateral treaties dealing with the legal and administrative aspects of intellectual property
  WTO                               World Trade Organisation. An international organisation created by the Uruguay Round of trade negotiations completed in 1994, dealing with rules of trade between nations
  WLR                               Weekly Law Reports
  --------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
