::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 35: Evidence of register, documents, etc \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (35.01) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 35.01

The Patents, Designs and Marks Act 1986 (c.39) repealed s.35 of the 1977
Act and amended s.32 in such a way as to incorporate therein the content
of both the former sections 32 and 35, and provide for computerisation
of the register. These measures were brought into force on 1 January
1989 by the Patents, Designs and Marks Act 1986 (Commencement No 2)
Order 1989.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
