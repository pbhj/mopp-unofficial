::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 66: Proceedings for infringement by a co-owner {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (66.01-66.04) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 66.01

This section lays down the way in which references to the proprietor of
a patent should be construed in applying s.60 where there are two or
more joint proprietors. It also provides, in subsection (2), for the
bringing of infringement proceedings by one of the joint proprietors.

### 66.02

In accordance with s.69, references in inter alia ss.60 and 66 to a
patent and the proprietor of a patent are to be respectively construed
as including references to an application which has been published but
not yet granted and the applicant. Section 66 thus also relates mutatis
mutandis to the position of joint applicants with regard to
infringement.

### 66.03

For the applicability of s.66 in relation to European patents, [see
60.02](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-02).

  -----------------------------------------------------------------------
   

  **Section 66(1)**

  In the application of section 60 above to a patent of which there are
  two or more joint proprietors the reference to the proprietor shall be
  construed -\
  \
  (a) in relation to any act, as a reference to that proprietor or those
  proprietors who, by virtue of section 36 above or any agreement
  referred to in that section, is or are entitled to do that act without
  its amounting to an infringement; and\
  \
  (b) in relation to any consent, as a reference to that proprietor or
  those proprietors who, by virtue of section 36 above or any such
  agreement, is or are the proper person.
  -----------------------------------------------------------------------

### 66.04

s.36(1), s.36(2) and s.36(3) are also available.

Section 36 provides that joint proprietors of a patent are each (subject
to any agreement to the contrary) entitled to an equal undivided share
in the patent and also entitled to independently do an act which would
otherwise amount to an infringement, but not to grant a licence or
assignment etc without the consent of the others. In applying s.60, the
proprietor means that proprietor or those proprietors entitled under
s.36 or under any such agreement to do such an act or give such a
consent, as the case may be.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 66(2)**
  One of two or more joint proprietors of a patent may without the concurrence of the others bring proceedings in respect of an act alleged to infringe the patent, but shall not do so unless the others are made parties to the proceedings; but any of the others made a defendant or defender shall not be liable for any costs or expenses unless he enters an appearance and takes part in the proceedings.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
