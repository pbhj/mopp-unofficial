::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 105: Extension of privilege in Scotland for communications relating to patent proceedings {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 105.01

This section provides for certain documents relating to patent
proceedings to be privileged from disclosure in Scotland. It was amended
by section 303(1) and schedule 7, paragraph 21, of the CDP Act.

### 105.02

In addition, section 280(4) of the CDP Act declares that in Scotland the
rules of law which confer privilege from disclosure in legal proceedings
in respect of communications extend to such communications as are
mentioned in section 280. That section is concerned with communications
with, or seeking information for the purpose of instructing, one's
patent agent. It is to be noted that s.105 does not contain any such
restriction to patent agents.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 105(1)**
  It is hereby declared that in Scotland the rules of law which confer privilege from disclosure in legal proceedings in respect of communications, reports or other documents (by whomsoever made) made for the purpose of any pending or contemplated proceedings in a court in the United Kingdom extend to communications, reports or other documents made for the purpose of patent proceedings.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 105.03

Section 105(1) declares that in Scotland the privilege from disclosure
afforded to documents made for the purpose of any pending or
contemplated proceedings in a court in the UK extends to those made for
the purpose of patent proceedings.

  -----------------------------------------------------------------------
   

  **Section 105(2)**

  In this section\
  \
  "patent proceedings" means proceedings under this Act or any of the
  relevant conventions, before the court, the comptroller or the relevant
  convention court, whether contested or uncontested and including an
  application for a patent; and\
  \
  "the relevant conventions" means the European Patent Convention and the
  Patent Co-operation Treaty.
  -----------------------------------------------------------------------

### 105.04

\[deleted\]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
