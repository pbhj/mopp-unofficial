::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 78: Effect of filing an application for a European patent (UK) {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (78.01 - 78.15) last updated October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 78.01

EPC applications for European patents (UK), although searched, published
and processed to grant by the EPO, are treated as applications under the
1977 Act in certain respects, as laid down in this section. The section
also includes a provision whereby English translations of the claims of
applications for European patents (UK) published in French or German are
required in order to establish certain rights.

### 78.02

s.130(1) is also relevant.

An application for a European patent (UK) is an application which has
been filed under the EPC and which, on its date of filing, designated
the United Kingdom as a country in which protection is sought for the
invention which is the subject of the application.

### 78.03

a.75 EPC, s.23(1), s.23(1A), s.22(3)(b), a.77(5), EPC a.135, EPC a.76
and EPC are also relevant.

A European patent application (other than a European divisional
application) may be filed at the EPO (Munich or The Hague) or, if the
law of a contracting state so permits, at the central industrial
property office or other competent authority of that state. The UK
Office is just such a central industrial property office and may thus
receive any European patent application. However, in the case of
applications which contain information relating to military technology
or other information whose publication might be prejudicial to national
security or the safety of the public by residents of the UK, they must
be first filed with the UK Office unless either, written authority for
filing elsewhere has been previously given by the comptroller, or an
application for the same invention has been filed in the UK not less
than six weeks earlier and no prohibition directions under s.22 are in
force. While any such directions are in force with respect to a European
patent application, it is not forwarded to the EPO. If this prevents it
reaching the EPO before the end of the fourteenth month after filing or,
if priority has been claimed, after the date of priority, it is deemed
to be withdrawn. The applicant may then apply (under s.81) for the
application to be converted to one for a patent under the Act. European
divisional applications must be filed directly with the EPO.

### 78.04

Section 79 provides for the operation of s.78 in relation to
applications for a European patent (UK) initiated by an international
application under the PCT.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 78(1)**
  Subject to the provisions of this Act, an application for a European patent (UK) having a date of filing under the European Patent Convention shall be treated for the purposes of the provisions of this Act to which this section applies as an application for a patent under this Act having that date as its date of filing and having the other incidents listed in subsection (3) below, but subject to the modifications mentioned in the following provisions of this section.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 78(2)**

  This section applies to the following provisions of this Act -\
  \
  section 2(3) and so much of section 14(7) as relates to section 2(3);\
  \
  section 5;\
  \
  section 6;\
  \
  so much of section 13(3) as relates to an application for and issue of
  a certificate under that subsection;\
  \
  sections 30 to 33;\
  \
  section 36;\
  \
  sections 55 to 69;\
  \
  sections 70 to 70F;\
  \
  section 74, so far as relevant to any of the provisions mentioned
  above;\
  \
  section 111; and\
  \
  section 125.
  -----------------------------------------------------------------------

### 78.05 {#ref78-05}

An application for a European patent (UK) made under the EPC is treated
as an application under the Act for the purposes of the provisions set
out in sub-section (2). These purposes relate to the extent of the prior
art under s.2(3) (and exclusion of the abstract therefrom); priority
dates under s.5; disclosure in earlier and later applications, under
s.6; certificates under s.13(3) to the effect that a person ought not to
have been mentioned as sole or joint inventor; transactions and property
in patents and applications under ss.30 and 31; registration of patents
and published applications and transactions etc under ss.32 and 33;
co-ownership of patents and applications under s.36; Crown use and
infringement under ss.55 to 69; proceedings relevant to any of the
preceding sections where validity can be put in issue under s.74;
unauthorised claims that a patent has been applied for, under s.111; and
the extent of an invention under s.125.

### 78.05.1 {#section-4}

Sections 70 to 70F were added to section 78(2) by the Intellectual
Property (Unjustified Threats) Act 2017. This ensures that threats
provisions apply to European Patent applications in the same way that
they apply to UK patent applications.

  -----------------------------------------------------------------------
   

  **Section 78(3)**

  The incidents referred to in subsection (1) above in relation to an
  application for a European patent (UK) are as follows -\
  \
  (a) any declaration of priority made in connection with the application
  under the European Patent Convention shall be treated for the purposes
  of this Act as a declaration made under section 5(2) above;\
  \
  (b) where a period of time relevant to priority is extended under that
  convention, the period of twelve months allowed under section 5(2A)(a)
  above shall be so treated as altered correspondingly;\
  \
  (c) where the date of filing an application is re-dated under that
  convention to a later date, that date shall be so treated as the date
  of filing the application;\
  \
  (d) the application, if published in accordance with that convention,
  shall, subject to subsection (7) and section 79 below, be so treated as
  published under section 16 above;\
  \
  (e) any designation of the inventor under that convention or any
  statement under it indicating the origin of the right to a European
  patent shall be treated for the purposes of section 13(3) above as a
  statement filed under section 13(2) above;\
  \
  (f) registration of the application in the register of European patents
  shall be treated as registration under this Act.
  -----------------------------------------------------------------------

### 78.06 {#ref78-06}

s.78(1) is also relevant.

The date of filing accorded to the application for a European patent
(UK) under EPC Article 80 is taken as if it were the date of filing
under the Act. Various other acts (concerning priority, filing,
publication, indicating the name of the inventor or derivation of the
right to apply, and registration all as set out in sub-section (3)) done
under the EPC can be treated as having been done under the Act.
Treatment of publication under the EPC as publication under s.16 of the
Act is subject to compliance with the requirements as to the language of
the published application set out in s.78(7) (since this sub-section was
brought into force, [see 78.11-12](#ref78-11)) and s.79(2) and (3) (if
the European patent application was initiated by a PCT international
application).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 78(4)**
  Rules under section 32 above may not impose any requirements as to the registration of applications for European patents (UK) but may provide for the registration of copies of entries relating to such applications in the European register of patents.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 78.07 {#ref78-07}

Rule 79 of the Patents Rules 1995 provided for keeping entries in the
register relating to published applications for European patents (UK),
but was revoked by the Patents (Amendments) Rules 1999. [See
32.07](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-07).

  -----------------------------------------------------------------------
   

  **Section 78(5)**

  Subsections (1) to (3) above shall cease to apply to an application for
  a European patent (UK), except as mentioned in subsection (5A) below,
  if -\
  \
  (a) the application is refused or withdrawn or deemed to be withdrawn,
  or\
  \
  (b) the designation of the United Kingdom in the application is
  withdrawn or deemed to be withdrawn,\
  \
  but shall apply again if the rights of the applicant are re-established
  under the European Patent Convention, as from their re-establishment.
  -----------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 78(5)**
  The occurrence of any of the events mentioned in subsection (5)(a) or (b) shall not affect the continued operation of section 2(3) above in relation to matter contained in an application for a European patent (UK) which by virtue of that provision has become part of the state of the art as regards other inventions; and the occurrence of any event mentioned in subsection (5)(b) shall not prevent matter contained in an application for a European patent (UK) becoming part of the state of the art by virtue of section 2(3) above as regards other inventions where the event occurs before the publication of that application.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 78.08 {#ref78-08}

Under EPC, every European patent application will designate the UK on
filing and will be treated as an application for a European patent (UK).
Upon the refusal or the actual or deemed withdrawal of an application
for a European patent (UK), or of its designation of the UK, it is no
longer treated as an application under the Act except as provided for by
subsection (5A). This subsection means that any document which forms
part of the state of the art under s.2(3) will not cease to do so if the
application is withdrawn or refused. Furthermore, under section 78(5A)
removal of the designation prior to publication will not have an effect
on the prior art status under section 2(3). Every European patent
application will therefore have prior art effect under section 2(3) on
publication regardless of whether the UK remains designated at the time
of publication. If the rights of the applicant in relation to the
application are subsequently re-established under the EPC, the
application at the same time resumes its status under the Act as set out
in s.78(1)-(3). However, the provisions of s.78(6) apply to infringers
in the intervening period.

### 78.09 {#section-5}

\[deleted\]

  -----------------------------------------------------------------------
   

  **Section 78(6)**

  Where, between subsections (1) to (3) above ceasing to apply to an
  application for a European patent (UK) and the re-establishment of the
  rights of the applicant, a person --\
  \
  (a) begins in good faith to do an act which would constitute an
  infringement of the rights conferred by publication of the application
  if those subsections then applied, or\
  \
  (b) makes in good faith effective and serious preparations to do such
  an act,\
  \
  he shall have the right to continue to do the act, or as the case may
  be, to do the act, notwithstanding subsections (1) to (3) applying
  again and notwithstanding the grant of the patent.
  -----------------------------------------------------------------------

### 78.10 {#ref78-10}

The exemption of third parties from infringement proceedings provided by
s.28A(4) to (7) applies to acts or preparations in the period between
cessation and reestablishment [see 78.08](#ref78-08). This includes acts
of Crown use which, but for s.55, would constitute infringements.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 78(6A)**
  Subsections (5) and (6) of section 20B above have effect for the purposes of subsection (6) above as they have effect for the purposes of that section and as if the references to subsection (4) of that section were references to subsection (6) above.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 78(6B)**
  Subject to subsection (6A) above, the right conferred by subsection (6) above does not extend to granting a licence to another person to do the act in question.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 78(6C)**
  Subsections (6) to (6B) above apply in relation to the use of a patented invention for the services of the Crown as they apply in relation to an infringement of the rights conferred by publication of the application (or, as the case may be, infringement of the patent). "Patented invention" has the same meaning as in section 55 above.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 78.10.1 {#section-6}

Section 78(6) prior to implementation of the revised EPC on 13 December
2007 provided protection for third parties when a European patent
application was terminated or refused. Sections 78(6) to 78 (6C) clarify
the protection afforded to third parties when a European patent
application has been refused and then reinstated. This includes
reinstatement by the Enlarged Board of Appeal. The provision as amended
is consistent with sections 20B and 177A on reinstatement and
resuscitation of national patent applications.

  -----------------------------------------------------------------------
   

  **Section 78(7)**

  While this subsection is in force, an application for a European patent
  (UK) published by the European Patent Office under the European Patent
  Convention in French or German shall be treated for the purposes of
  sections 55 and 69 above as published under section 16 above when a
  translation into English of the claims of the specification of the
  application has been filed at and published by the Patent Office and
  the prescribed fee has been paid, but an applicant -\
  \
  (a) may recover a payment by virtue of section 55(5) above in respect
  of the use of the invention in question before publication of that
  translation; or\
  \
  (b) may bring proceedings by virtue of section 69 above in respect of
  an act mentioned in that section which is done before publication of
  that translation;\
  \
  if before that use or the doing of that act he has sent by post or
  delivered to the government department who made use or authorised the
  use of the invention, or, as the case may be, to the person alleged to
  have done the act, a Translation into English of those claims.
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 78(8)**
  Subsection (7) above shall come into force on a day appointed for the purpose by rules and shall cease to have effect on a day so appointed, without prejudice, however, to the power to bring it into force again.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Translation of claims from French or German

### 78.11 {#ref78-11}

s.79(1), r.56(1)(b) and s.80(2) are also relevant

Since s.78(7) was brought into force, applications for European patents
(UK) published by the EPO (and international applications for a European
patent (UK) the subject of proceedings before the EPO under EPC a.150
and published under PCT a.21) in French or German are treated as
published for Crown use or infringement purposes under the Act when an
English language translation of the claims thereof has been filed at and
published by the UK Office. Rights are also obtainable by the applicant
in relation to use or acts done before publication of that translation
from the date on which the applicant sends a translation to the
government department or potential infringer in question under s.55 or
69 respectively. Such a translation may in some circumstances be treated
as the authentic text for the purposes of proceedings under the Act,
[see
80.02](/guidance/manual-of-patent-practice-mopp/section-80-authentic-text-of-european-patents-and-patent-applications/#ref80-02).

### 78.12 {#ref78-12}

The Patents (Amendment) Rules 1987 appointed 1 September 1987 as the day
on which s.78(7) came into force but only in respect of applications for
a European patent (UK) which were published by the EPO on or after that
day.

### 78.13 {#section-7}

It is not obligatory to file a translation of the claims; it is entirely
at the option of the applicant to do so if they wish to secure the
rights referred to in [78.11](#ref78-11). There is no time limit for
filing it.

### 78.14 {#section-8}

r.56 and r.115 are also relevant.

The translation should be filed in duplicate together with Patents Form
54 in duplicate and the appropriate fee. The translation should comply
with certain formal requirements, as set out in Parts 1 to 3 of Schedule
2. These requirements are described in
[14.27-30](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-27)
and
[14.33-36](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-33).
There is no requirement for a translation to be verified. However, if
there are reasonable doubts about the accuracy of the translation, the
comptroller should notify the person of the reasons for their doubts and
may require evidence to be filed to establish that the translation is
accurate. The comptroller may, if they think fit, take no further action
in relation to the document where the person fails to furnish evidence.

### 78.15 {#section-9}

s.118(1) is also relevant.

If the requirements are met, the fact that the translation has been
filed is recorded in the register and announced in the Journal. It is
made available for sale and a copy is placed on file and thus open to
public inspection at the Office. Requests for inspection of the file
containing the translation should be accompanied by the prescribed fee,
if any.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
