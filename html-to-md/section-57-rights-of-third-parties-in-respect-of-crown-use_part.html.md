::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 57: Rights of third parties in respect of Crown use {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (57.01 - 57.02) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 57.01

This is the third of the group of sections relating to use of patented
inventions for the services of the Crown. It concerns the rights of
third parties, particularly licensees and parties to assignments
(assignations in Scotland). With regard to ss.55 to 59 in general, [see
55.01-03](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-01).

### 57.02

Subsections (1) and (2) render ineffective any provisions of a licence,
assignment or agreement which would otherwise inhibit Crown use; the use
of models, documents or information relating to an invention affected by
a licence, assignment or agreement is also subject to subsections (9)
and (10). Exclusive licences not providing for royalties or other
benefits determined by reference to the working of the invention are
dealt with by subsection (3); other exclusive licences by subsections
(5) to (8). Where patent rights were assigned to the proprietor of the
patent or application, subsection (4) is applicable. Subsection (1) also
renders ineffective copyright or Design Right which would otherwise
inhibit Crown use, the reference to Design Right having been added by
paragraph 20 of Schedule 7 to the CDP Act.

  -----------------------------------------------------------------------
   

  **Section 57(1)**

  In relation to\
  (a) any use made for the services of the Crown of an invention by a
  government department, or a person authorised by a government
  department, by virtue of section 55 above, or\
  (b) anything done for the services of the Crown to the order of a
  government department by the proprietor of a patent in respect of a
  patented invention or by the proprietor of an application in respect of
  an invention for which an application for a patent has been filed and
  is still pending,\
  \
  the provisions of any licence, assignment, assignation or agreement to
  which this subsection applies shall be of no effect so far as those
  provisions restrict or regulate the working of the invention, or the
  use of any model, document or information relating to it, or provide
  for the making of payments in respect of, or calculated by reference
  to, such working or use; and the reproduction or publication of any
  model or document in connection with the said working or use shall not
  be deemed to be an infringement of any copyright or design right
  subsisting in the model or document.
  -----------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57(2)**
  Subsection (1) above applies to a licence, assignment, assignation or agreement which is made, whether before or after the appointed day, between (on the one hand) any person who is a proprietor of or an applicant for the patent, or anyone who derives title from any such person or from whom such person derives title, and (on the other hand) any person whatever other than a government department.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 57(3)**

  Where an exclusive licence granted otherwise than for royalties or
  other benefits determined by reference to the working of the invention
  is in force under the patent or application concerned, then -\
  (a) in relation to anything done in respect of the invention which, but
  for the provisions of this section and section 55 above, would
  constitute an infringement of the rights of the licensee, subsection
  (4) of that section shall have effect as if for the reference to the
  proprietor of the patent there were substituted a reference to the
  licensee; and\
  (b) in relation to anything done in respect of the invention by virtue
  of an authority given under that section, that section shall have
  effect as if the said subsection (4) were omitted.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 57(4)**

  Subject to the provisions of subsection (3) above, where the patent, or
  the right to the grant of the patent, has been assigned to the
  proprietor of the patent or application in consideration of royalties
  or other benefits determined by reference to the working of the
  invention, then\
  (a) in relation to any use of the invention by virtue of section 55
  above, subsection (4) of that section shall have effect as if the
  reference to the proprietor of the patent included a reference to the
  assignor, and any sum payable by virtue of that subsection shall be
  divided between the proprietor of the patent or application and the
  assignor in such proportion as may be agreed on by them or as may in
  default of agreement be determined by the court on a reference under
  section 58 below; and\
  (b) in relation to any act done in respect of the invention for the
  services of the Crown by the proprietor of the patent or application to
  the order of a government department, section 55(4) above shall have
  effect as if that act were use made by virtue of an authority given
  under that section.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57(5)**
  Where section 55(4) above applies to any use of an invention and a person holds an exclusive licence under the patent or application concerned (other than such a licence as is mentioned in subsection (3) above) authorising him to work the invention, then subsections (7) and (8) below shall apply.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57(6)**
  In those subsections "the section 55(4)" payment means such payment (if any) as the proprietor of the patent or application and the department agree under section 55 above, or the court determines under section 58 below, should be made by the department to the proprietor in respect of the use of the invention.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 57(7)**

  The licensee shall be entitled to recover from the proprietor of the
  patent or application such part (if any) of the section 55(4) payment
  as may be agreed on by them or as may in default of agreement be
  determined by the court under section 58 below to be just having regard
  to any expenditure incurred by the licensee -\
  (a) in developing the invention, or\
  (b) in making payments to the proprietor in consideration of the
  licence, other than royalties or other payments determined by reference
  to the use of the invention.
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57(8)**
  Any agreement by the proprietor of the patent or application and the department under section 55(4) above as to the amount of the section 55(4) payment shall be of no effect unless the licensee consents to the agreement; and any determination by the court under section 55(4) above as to the amount of that payment shall be of no effect unless the licensee has been informed of the reference to the court and is given an opportunity to be heard.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57(9)**
  Where any models, documents or information relating to an invention are used in connection with any use of the invention which falls within subsection (1)(a) above, or with anything done in respect of the invention which falls within subsection (1)(b) above, subsection (4) of section 55 above shall (whether or not it applies to any such use of the invention) apply to the use of the models, documents or information as if for the reference in it to the proprietor of the patent there were substituted a reference to the person entitled to the benefit of any provision of an agreement which is rendered inoperative by this section in relation to that use; and in section 58 below the references to terms for the use of an invention shall be construed accordingly.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57(10)**
  Nothing in this section shall be construed as authorising the disclosure to a government department or any other person of any model, document or information to the use of which this section applies in contravention of any such licence, assignment, assignation or agreement as is mentioned in this section.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
