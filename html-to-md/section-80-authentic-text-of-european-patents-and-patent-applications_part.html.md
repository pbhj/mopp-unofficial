::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 80: Authentic text of European patents and patent applications {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (80.01-80.07) last updated April 2009.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 80(1)

Subject to subsection (2) below, the text of a European patent or
application for such a patent in the language of the proceedings, that
is to say, the language in which proceedings relating to the patent or
the application are to be conducted before the European Patent Office,
shall be the authentic text for the purposes of any domestic
proceedings, that is to say, any proceedings relating to the patent or
application before the comptroller or the court.

### 80.01 {#ref80-01}

r.114 is also relevant.

The authentic text of a European patent or application therefor in
proceedings before the comptroller or the court is the text in the
language used in the EPO proceedings, except as provided for below [see
80.02-07](#ref80-02-07). (Where proceedings are instituted before the
comptroller in relation to a European patent (UK) the specification of
which was published in French or German, the party who institutes those
proceedings is normally required to furnish a verified translation into
English of the specification unless such a translation has already been
filed under s.77(6). This also applies to the making a request of an
opinion under section 74A. A party given leave to amend the
specification during such proceedings must furnish a verified
translation of the amendment into the language in which the
specification was published).

### 80.01.1 {#section}

a.70(1) is also relevant.

Since the authentic text of a European patent published in French or
German is that in the language used in the EPO proceedings, the
authentic text for the claims is the text of the claims in the language
of the EPO proceedings. The translated claims of a European patent
published in French or German which are filed under Article 14(6) are
for information only.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 80(2)**
  Where the language of the proceedings is French or German, a translation into English of the specification of the patent under section 77 above or of the claims of the application under section 78 above shall be treated as the authentic text for the purpose of any domestic proceedings, other than proceedings for the revocation of the patent, if the patent or application as translated into English confers protection which is narrower than that conferred by it in French or German.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 80.02 {#ref80-02}

This sub-section became effective only when s.77(6) and s.78(7) were
brought into force, [see
77.13-14](/guidance/manual-of-patent-practice-mopp/section-77-effect-of-european-patent-uk/#ref77-13)
and
[78.11-12](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-11)
respectively. Where the language of the EPO proceedings is French or
German, an English translation of the specification under s.77(6) or of
the claims under s.78(7) is treated as the authentic text if it confers
protection narrower than that conferred by the French or German text.
This provision does not apply to proceedings for the revocation of the
patent.

  -----------------------------------------------------------------------
   

  **Section 80(3)**

  If any such translation results in a European patent or application
  conferring the narrower protection, the proprietor of or applicant for
  the patent may file a corrected translation with the Office and, if he
  pays the prescribed fee within the prescribed period, the Office shall
  publish it, but -\
  \
  (a) any payment for any use of the invention which (apart from section
  55 above) would have infringed the patent as correctly translated, but
  not as originally translated, or in the case of an application would
  have infringed it as aforesaid if the patent had been granted, shall
  not be recoverable under that section,\
  \
  (b) the proprietor or applicant shall not be entitled to bring
  proceedings in respect of an act which infringed the patent as
  correctly translated, but not as originally translated, or in the case
  of an application would have infringed it as aforesaid if the patent
  had been granted,\
  \
  unless before that use or the doing of the act the corrected
  translation has been published by the Office or the proprietor or
  applicant has sent the corrected translation by post or delivered it to
  the government department who made use or authorised the use of the
  invention or, as the case may be, to the person alleged to have done
  that act.
  -----------------------------------------------------------------------

### 80.03 {#ref80-03}

If a translation as referred to in [80.02](#ref80-02) results in
narrower protection being conferred by the European patent or
application, the proprietor or applicant may file a corrected
translation. In such a case it is the specific provisions of s.80(3) and
r.57 that apply rather than the general provisions of s.117
[Rhône-Poulenc Santé's European Patent (UK) \[1996\] RPC
125](http://rpc.oxfordjournals.org/content/113/4/125.full.pdf+html){rel="external"}.
However, rights of the proprietor or applicant with regard to acts or
use (including Crown use which, but for s.55, would constitute
infringement) which would infringe in relation to the corrected
translation but not in relation to the original translation apply only
to such acts or use done after the date on which the corrected
translation is published by the Office or sent to the potential
infringer or government department in question by the proprietor or
applicant.

### 80.04 {#section-1}

r.57(2) & (6) are also relevant.

The corrected translation should be a corrected version, in duplicate,
of the (4) whole of the specification or of the claims as the case may
be. It should comply with the requirements as to presentation set out in
of Schedule 2 of the Patents Rules 2007 ([see
78.14](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-14)).
Where the translation includes drawings, they should correspond exactly
in content and presentation to those published by the EPO except that
any textual matter should be replaced by an English translation and each
sheet of drawings should be numbered consecutively in arabic numerals,
as a separate series from that used for the other sheets of the
translation, if not so numbered when published by the EPO. Verification
is necessary in the same way as referred to in
[78.14](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-14).

### 80.05 {#section-2}

r.57(4) & (6) are also relevant.

Patents Form 54 in duplicate should accompany the corrected translation.
(6) The appropriate fee should be paid within fourteen days of the
filing of the corrected translation. This period may be extended at the
comptroller's discretion under rule 108(1).

### 80.06 {#section-3}

s.118(1) is also relevant.

If the requirements are met, the fact that the corrected translation has
been filed is recorded in the register and announced in the Journal.
Copies of the translation are made available for public inspection or
sale. Requests for inspection of such translations should be accompanied
by the prescribed fee, if any.

  -----------------------------------------------------------------------
   

  **Section 80(4)**

  Where a correction of a translation is published under subsection (3)
  above and before it is so published a person --\
  \
  begins in good faith to do an act which would not constitute an
  infringement of the patent as originally translated, or of the rights
  conferred by publication of the application as originally translated,
  but would do so under the amended translation, or\
  \
  makes in good faith effective and serious preparations to do such an
  act,\
  \
  he shall have the right to continue to do the act or, as the case may
  be, to do the act, notwithstanding the publication of the corrected
  translation and notwithstanding the grant of the patent.
  -----------------------------------------------------------------------

### 80.07 {#ref80-02-07}

The exemption of third parties from infringement proceedings provided by
s.28A(4) to (7) applies to acts or preparations for acts, which would
infringe in relation to the corrected translation but not in relation to
the original translation, done or made before the corrected translation
is published by the Office. This includes acts of Crown use which, but
for s.55, would constitute infringement.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 80(5)**
  Subsections (5) and (6) of section 28A above have effect for the purposes of subsection (4) above as they have effect for the purposes of that section and as if -- the references to subsection (4) of that section were references to subsection (4) above; the reference to the registered proprietor of the patent included a reference to the applicant.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 80(6)**
  Subject to subsection (5) above, the right conferred by subsection (4) above does not extend to granting a licence to another person to do the act in question.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 80(7)**

  Subsections 4) to (6) above apply in relation to the use of a patented
  invention for the services of the Crown as they apply in relation to an
  infringement of the patent or of the rights conferred by the
  publication of the application.\
  \
  "Patented invention" has the same meaning as in section 55 above.
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
