::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 285: Offences committed by partnerships and bodies corporate {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (285.01 - 285-05) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 285.01

This contains standard procedural provisions relating to criminal
proceedings against bodies corporate and partnerships. It provides inter
alia that where offences are committed with the consent or connivance of
a director or partner those persons may in addition be proceeded against
in their own personal capacities. It also provides that for some
purposes, partnerships should be treated in the same way as bodies
corporate.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 285(1)**
  Proceedings for an offence under this Part alleged to have been committed by a partnership shall be brought in the name of the partnership and not in that of the partners; but without prejudice to any liability of theirs under subsection (4) below.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 285.02

Where proceedings are brought against a partnership, they are to be
brought in the name of the partnership as a whole. This applies to
proceedings for an offence under Part V of the CDP Act (patent agents
and trade mark agents). Individual partners may still be held liable,
however, under subsection (4) of this section.

  -----------------------------------------------------------------------
   

  **Section 285(2)**

  The following provisions apply for the purposes of such proceedings as
  in relation to a body corporate -\
  \
  (a) any rules of court relating to the service of documents;\
  \
  (b) in England, Wales or Northern Ireland, Schedule 3 to the
  Magistrates' Courts Act 1980 or Schedule 4 to the Magistrates' Courts
  (Northern Ireland) Order 1981 (procedure on charge of offence).
  -----------------------------------------------------------------------

### 285.03

Certain provisions applicable to proceedings against bodies corporate
also apply to partnerships.

  --------------------------------------------------------------------------------------------------------------------
   
  **Section 285(3)**
  A fine imposed on a partnership on its conviction in such proceedings shall be paid out of the partnership assets.
  --------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 285(4)**
  Where a partnership is guilty of an offence under this Part, every partner, other than a partner who is proved to have been ignorant of or to have attempted to prevent the commission of the offence, is also guilty of the offence and liable to be proceeded against and punished accordingly.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 285.04

Should a partnership be guilty of an offence under this Part, any fine
imposed on the partnership should be paid out of the partnership assets;
but every partner is also guilty and liable to be proceeded against.
However, this does not apply to any partner who was ignorant of or
attempted to prevent the commission of the offence, but the onus is on
such a partner so to prove.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 285(4)**
  Where an offence under this Part committed by a body corporate is proved to have been committed with the consent or connivance of a director, manager, secretary or other similar officer of the body, or a person purporting to act in any such capacity, he as well as the body corporate is guilty of the offence and liable to be proceeded against and punished accordingly.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 285.05

Where an offence has been committed by a body corporate with the consent
or connivance of a director or other officer of the company, that person
is also guilty and is liable to be proceeded against.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
