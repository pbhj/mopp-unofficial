::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 128A: Compulsory pharmaceutical licences {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (128A.01 - 128A.08) last updated: January 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 128A.01 {#a01}

This section was introduced by regulation 2 of the [Patents (Compulsory
Licensing and Supplementary Protection Certificates) Regulations
2007](https://www.legislation.gov.uk/uksi/2007/3293/contents/made){rel="external"}
(SI 2007/3293), on 17 December 2007. It sets out how certain provisions
of the Act apply to compulsory pharmaceuticals licences (originally, "EU
compulsory licences"), and applications for such licences, under the
Compulsory Licensing Regulation (Regulation (EC) No 816/2006 of the
European Parliament and of the Council of 17 May 2006 on compulsory
licensing of patents relating to the manufacture of pharmaceutical
products for export to countries with public health problems). The
Compulsory Licensing Regulation provides for the availability of a
compulsory licence for anyone who wishes to make a specific patented
pharmaceutical product solely for the purpose of exporting it to a
developing country with a particular public health problem. Section 128A
does not implement those provisions of the Regulation that were directly
applicable in relation to UK patents under the provisions of section
2(1) of the European Communities Act 1972 and are now incorporated into
domestic law by the European Union (Withdrawal) Act 2018.

### 128A.01.1 {#ref128a-01-1}

Under the provisions of the European Union (Withdrawal) Act 2018,
EU-derived domestic legislation and direct EU legislation in force in
the UK at 11pm on 31 December 2020 was saved or incorporated into
domestic law. This included the Compulsory Licensing Regulation as well
as its implementation through the 2007 Regulations. These remain in
effect as assimilated law. The Compulsory Licensing Regulation was
amended by the [Patents (Amendment) (EU Exit) Regulations
2019](https://www.legislation.gov.uk/uksi/2019/801/contents/made){rel="external"}
(SI 2019/801), which established that the comptroller is the competent
authority for the grant and maintenance of compulsory pharmaceutical
licences.

### 128A.01.2 {#ref128a-01-2}

This section was also amended by the Patents (Amendment) (EU Exit)
Regulations 2019 (as modified by the [Intellectual Property (Amendment
etc.) (EU Exit) Regulations
2020](https://www.legislation.gov.uk/uksi/2020/1050/contents/made){rel="external"}
(SI 2020/1050)), to replace references to "EU compulsory licences" with
"compulsory pharmaceutical licences", including in the title.

### 128A.01.3 {#ref128a-01-3}

Under the provisions of the Protocol on Ireland/Northern Ireland in the
Agreement on the Withdrawal of the United Kingdom from the European
Union, the EU version of the Compulsory Licensing Regulation continues
to apply in Northern Ireland for as long as the Protocol has effect. In
terms of the grant and maintenance of compulsory pharmaceutical
licences, there is no substantive difference in how the two versions
operate in relation to Office practice.

### 128A.02 {#ref128A-02}

PR rr.73-88 is also relevant

The Compulsory Licensing Regulation sets out a number of proceedings
which may take place before the comptroller in relation to compulsory
pharmaceutical licences. These include proceedings to apply for, modify
or revoke such a licence. All proceedings in relation to such licences
fall within the scope of Part 7 of the Patents Rules 2007, which governs
procedures in relation to all proceedings before the comptroller in
relation to patents. Reference should be made to paragraphs [123.05 to
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05),
which set out in general terms how proceedings (such as an application
for a compulsory pharmaceutical licence) are launched under Part 7 of
the Rules, and how such proceedings would then progress.

### 128A.03 {#a03}

\[Deleted\]

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 128A(1)**
  In this Act a "compulsory pharmaceutical licence" means a compulsory licence granted under Regulation (EC) No 816/2006 of the European Parliament and of the Council of 17 May 2006 on compulsory licensing of patents relating to the manufacture of pharmaceutical products for export to countries with public health problems (referred to in this Act as "the Compulsory Licensing Regulation").
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 128A.04 {#ref128A-04}

Subsection (1) defines the terms "compulsory pharmaceutical licence" and
"the Compulsory Licensing Regulation". As per paragraphs 1 and 1A of
schedule 8 to the European Union (Withdrawal) Act 2018, the reference to
Regulation (EC) No 816/2006 is to be interpreted as a reference to the
version as incorporated into UK domestic law, or as the EU version which
has effect under the Protocol, depending on context.

  -----------------------------------------------------------------------
   

  **Section 128A(2)**

  In the application to compulsory pharmaceutical licences of the
  provisions of this Act listed in subsection (3)\
  (a) references to a licence under a patent,\
  (b) references to a right under a patent, and\
  (c) references to a proprietary interest under a patent, include an
  compulsory pharmaceutical licence.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 128A(3)**

  The provisions referred to in subsection (2) are --\
  sections 32 and 33 registration of patents etc);\
  section 37 (determination of right to patent after grant);\
  section 38 (effect of transfer etc of patent under section 37), apart
  from subsection (2) and subsections (3) to (5) so far as relating to
  subsection (2);\
  section 41 (amount of compensation);\
  section 46 (2) (notice of application for entry that licences are
  available as of right);\
  section 57(1) and (2) (rights of third parties in respect of Crown
  use).
  -----------------------------------------------------------------------

### 128A.05 {#ref128A-05}

Subsections (2) and (3) together make clear that certain references in
the Act to a licence or a right or a proprietary interest under a patent
include within their meaning a compulsory pharmaceutical licence granted
under the Compulsory Licensing Regulation. In particular, subsection (3)
identifies the sections of the Act to which the gloss set out in
subsection (2) applies. Thus, for example, references in section 38(1)
to the continuation in force of licences (in certain circumstances
following an entitlement dispute) include not only licences granted
under the Act but also compulsory pharmaceutical licences granted under
the Compulsory Licensing Regulation. It should be noted that references
to licences in section 38(2) do not include compulsory pharmaceutical
licences. This is because section 38(2) sets out that in certain
circumstances a licence under a patent may lapse when a person becomes
the new proprietor of the patent following entitlement proceedings --
but the Compulsory Licensing Regulation does not envisage that a
compulsory pharmaceutical licence could lapse in such circumstances.

  -----------------------------------------------------------------------
   

  **Section 128A(4)**

  In the following provisions references to this Act include the
  Compulsory Licensing Regulation --\
  sections 97 to 99B, 101 to 103, 105 and 107 (legal proceedings);\
  section 119 (service by post);\
  section 120 (hours of business and excluded days);\
  section 121 (comptroller's annual report);\
  section 123 (rules);\
  section 124A (use of electronic communications);\
  section 130(8) (disapplication of Part 1 of Arbitration Act 1996).
  -----------------------------------------------------------------------

### 128A.06 {#a06}

Subsection (4) makes clear that certain references in the Act to the Act
itself include within their meaning the Compulsory Licensing Regulation.
For example, the reference in [section
120](/guidance/manual-of-patent-practice-mopp/section-120-hours-of-business-and-excluded-days)
to the Office's opening hours and business done "under this Act"
includes business done by the Office under the provisions of the
Compulsory Licensing Regulation.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 128A(5)**
  In [section 108](/guidance/manual-of-patent-practice-mopp/section-108-licences-granted-by-order-of-comptroller) (licences granted by order of comptroller) the reference to a licence under section 11, 38, 48 or 49 includes a compulsory pharmaceutical licence.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 128A.07 {#ref128A-07}

Subsection (5) makes clear that the reference to "a licence under
section 11, 38, 48 or 49" includes within its meaning a licence under
the Compulsory Licensing Regulation. Thus any order for the grant of a
compulsory pharmaceutical licence shall, without prejudice to any other
method of enforcement, have effect as if it were a deed (executed by the
patent proprietor and all other necessary parties) granting a licence in
accordance with the order. This ensures that such an order under the
Compulsory Licensing Regulation will be effective even if the parties
affected take no action in response to it.

  --------------------------------------------------------------------------------------------------------------------
   
  **Section 128A(6)**
  References in this Act to the Compulsory Licensing Regulation are to that Regulation as amended from time to time.
  --------------------------------------------------------------------------------------------------------------------

### 128A.08 {#ref128A-08}

Subsection (6) is an "ambulatory reference". It ensures that, if the
Compulsory Licensing Regulation is amended in the future by UK domestic
legislation, the references to it in the Act will continue to apply
without further amendment being needed. This is supported by paragraph 1
of schedule 8 to the European Union (Withdrawal) Act 2018. Similarly,
paragraph 1A of the same schedule will mean that any changes affecting
the EU version that has effect in Northern Ireland will be recognised
without further amendment.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
