::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 131: Northern Ireland {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (131.01 - 131.03) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 131

In the application of this Act to Northern Ireland -

\(a\) "enactment" includes an enactment of the Parliament of Northern
Ireland and a Measure of the Northern Ireland Assembly;

\(b\) any reference to a government department includes a reference to a
Department of the Government of Northern Ireland;

\(c\) any reference to the Crown includes a reference to the Crown in
right of His Majesty's Government in Northern Ireland;

\(d\) any reference to the Companies Act 1985 includes a reference to
the corresponding enactments in force in Northern Ireland; and

\(e\) \[repealed\]

\(f\) any reference to a claimant includes reference to a plaintiff.

### 131.01

The Act extends to Northern Ireland but, in its application thereto, is
interpreted in accordance with this section.

### 131.02

s.130(1) is also relevant

In the Act, various powers are specifically given to the "court" which,
as respects Northern Ireland, means the High Court in Northern Ireland
(unless the context otherwise requires).

### 131.03

The reference to the Companies Act 1985 in (d) was substituted by the
Companies Consolidation (Consequential Provisions) Act 1985 for the
original reference to the Companies Act 1948. Part (e) was repealed by
the Arbitration Act 1996. Part (f) was added by the Patents Act 2004 to
clarify that the term "claimant" used in the Act includes "plaintiff"
when the Act is applied in Northern Ireland.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
