::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 74B: Reviews of opinions under section 74A {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (74B.01 - 74B.10) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 74B.01 {#b01}

Section 74B was introduced by the Patents Act 2004 and came into force
on 1 October 2005. The section enables provision to be made for a review
of opinions made under section 74A.\
\
The Patents Opinions Service Procedures Manual should be consulted for
full details of practice concerning opinions under section 74A and
reviews of opinions under section 74B.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 74B(1)**
  Rules may make provision for a review before the comptroller, on an application by the proprietor or an exclusive licensee of the patent in question, or an opinion under section 74A above.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 74B.02 {#b02}

The comptroller may undertake a review of an opinion made under section
74A. Only the patent proprietor or exclusive licensee ("the patent
holder") may apply for a review, which is considered in full proceedings
before the comptroller.

  -----------------------------------------------------------------------
   

  **Section 74B(2)**

  The rules may, in particular\
  (a) prescribe the circumstances in which, and the period within which,
  an application may be made;\
  (b) provide that, in prescribed circumstances, proceedings for a review
  may not be brought or continued where other proceedings have been
  brought;\
  (c) \[Repealed\]\
  (d) provide for there to be a right of appeal against a decision made
  on a review only in prescribed cases.
  -----------------------------------------------------------------------

### Applying for a review of an opinion

### 74B.03 {#b03}

r.98(1)-(4) is also relevant.

The patent proprietor or exclusive licensee may apply to the comptroller
for a review of an opinion within three months of the date of issue of
the opinion (extendable under rule 108(1)). The application should be
made on Patents Form 2 filed in duplicate. The form should be
accompanied by a statement in duplicate setting out fully the grounds on
which the review is sought. This statement should contain details of any
proceedings of which the applicant is aware which may be relevant to the
question whether the review proceedings can be brought or continued.
Proceedings for a review may not be brought or continued if the issue
raised by the review has been decided in other proceedings.

### 74B.04 {#b04}

r.98(5) is also relevant.

The application for a review of an opinion may only be brought on two
grounds. Firstly, the patent proprietor or exclusive licensee may apply
to have an opinion set aside on the grounds that the opinion wrongly
concluded that the patent was invalid, or was invalid to a limited
extent. Secondly, they may apply for a review of an opinion which
concluded that a particular act did not or would not constitute an
infringement of the patent. In this case, an application for a review
can only be made where this conclusion was reached by what is believed
to be an erroneous construction of the patent specification.

### Procedure on review

### 74B.05 {#b05}

r.99(1) is also relevant.

Upon receipt of the application, a copy of the form and statement is
sent to the original requester of the opinion (if different from the
applicant for the review) and to anyone who submitted observations
during the original opinions procedure ([see
74A.08](/guidance/manual-of-patent-practice-mopp/section-74a-opinions-on-matters-prescribed-in-the-rules/#ref74A-08)).

### 74B.06 {#b06}

r.99(2) is also relevant

An [application for a review of an
opinion](https://www.gov.uk/government/publications/applications-for-reviews-on-patent-opinions)
is advertised on the Office website.

### 74B.07 {#b07}

r.99(3)-(6) is also relevant.

Following advertisement of the review, any person may file a statement
in support of the application for review, or a counter-statement
contesting the application. In doing so, the person becomes party to the
proceedings. Any statement or counter-statement must be filed within
four weeks from publication of the advertisement of the review, or
within two months of the date of issue of the opinion under s.74A,
whichever period is the later to expire. Copies of statements or
counter-statements are sent to the other parties in the review. The
comptroller may give directions as they think fit with regard to
subsequent procedure.

### Outcome of review

### 74B.08 {#b08}

r.100(1)-(2) is also relevant.

As with other proceedings before the comptroller, the parties to the
proceedings have a right to be heard before the comptroller issues a
decision upon completion of the review. The decision shall either set
aside the opinion in whole or in part, or decide that no reason has been
shown for the opinion to be set aside. The decision does not estop any
of the parties from raising an issue regarding the validity or the
infringement of the patent.

### Appeals against a decision on review

### 74B.09 {#b09}

r.100 is also relevant.

There is no appeal to the courts against a decision by the comptroller
to set aside an opinion, except where the appeal relates to a part of
the opinion that is not set aside. Therefore, it is not possible for the
courts to consider the issue of whether to reinstate a nonbinding
opinion.

### Revocation action following a review

### 74B.10 {#b10}

[See
73.02](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-02)
and
[73.04.1](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-04-1)
for details of when action may be taken to commence revocation
proceedings under s.73 after an opinion concludes that a granted UK
patent or European patent (UK) lacks novelty or an inventive step, and a
decision has been issued that no reason has been shown for the opinion
to be set aside.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
