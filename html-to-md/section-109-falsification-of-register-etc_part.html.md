::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 109: Falsification of register etc {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (109.01 - 109.03) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------
   

  **Section 109**

  If a person makes or causes to be made a false entry in any register
  kept under this Act, or a writing falsely purporting to be a copy or
  reproduction of an entry in any such register, or produces or tenders
  or causes to be produced or tendered in evidence any such writing,
  knowing the entry or writing to be false, he shall be liable\
  (a) on summary conviction, to a fine not exceeding the prescribed sum,\
  (b) on conviction on indictment, to imprisonment for a term not
  exceeding two years or a fine, or both.
  -----------------------------------------------------------------------

### 109.01

This section lays down penalties for certain offences relating to
entries in any register kept under the 1977 Act. It thus applies to the
register of patents kept under s.32 of the 1977 Act. The offences in
question are, in essence, to procure a false entry or falsely represent
an entry.

### 109.02

The maximum fine on summary conviction corresponds to that applicable
under s.22(9) [see
22.26.1](/guidance/manual-of-patent-practice-mopp/section-22-information-prejudicial-to-national-security-or-safety-of-public/#ref22-26-1).

### 109.03

The reference to "indictment" in s.109(b) is treated as a reference to
"information" for the Isle of Man only (SI 2003 No 1249).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
