::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 112: Misuse of title \"Patent Office\" {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (112.01 - 112.02) last updated: July 2010.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 112**
  If any person uses on his place of business, or on any document issued by him, or otherwise, the words "Patent Office" or any other words suggesting that his place of business is, or is officially connected with, the Patent Office, he shall be liable on summary conviction to a fine not exceeding level 4 on the standard scale.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 112.01

It is an offence under s.112 to suggest that a place of business (other
than the Patent Office) is, or is officially connected with, the Patent
Office.

\[ Any queries relating to offences under this section should be
referred to Patents Legal Section. \]

### 112.02

The maximum fine was converted to a level on the standard scale by the
Criminal Justice Act 1982.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
