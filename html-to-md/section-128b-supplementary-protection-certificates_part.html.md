::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 128B: Supplementary Protection Certificates {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (128B.0 - 128B.12) last updated: January 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 128B.01 {#b01}

This section was introduced by regulation 2 of the Patents (Compulsory
Licensing and Supplementary Protection Certificates) Regulations 2007
(SI 2007/3293), on 17 December 2007. In combination with Schedule 4A to
the Act, it sets out how certain provisions of the Act apply to
supplementary protection certificates, and applications for such
certificates. These certificates exist under the two supplementary
protection certificates Regulations -- namely Regulation (EC) No
469/2009 of 6 May 2009 (which superseded Council Regulation (EEC) No
1768/92 of 18 June 1992) concerning the supplementary protection
certificate for medicinal products, and Regulation (EC) No 1610/96 of
the European Parliament and of the Council of 23 July 1996 concerning
the creation of a supplementary protection certificate for plant
protection products. These two Regulations are referred to in the Act as
"the Medicinal Products Regulation" and "the Plant Protection Products
Regulation". Section 128B and Schedule 4A do not implement those
provisions of the Regulations that were directly applicable under the
provisions of section 2(1) of the European Communities Act 1972 and are
now incorporated into domestic law by the European Union (Withdrawal)
Act 2018.

### 128B.02 {#b02}

The Medicinal Products and Plant Protection Products Regulations provide
for the availability of a supplementary protection certificate in order
to compensate a patentee for the loss of effective protection arising
out of the time taken to obtain regulatory approval for a medicinal or
plant protection product which is protected by a patent. The Medicinal
Products Regulation also provides for the possibility of a six month
extension to a certificate if the product in question has undergone an
approved investigation plan for use on children.

### 128B.03 {#ref128B-03}

The 1992 Medicinal Products Regulation and 1996 Plant Protection
Products Regulation were originally implemented in the UK by the Patents
(Supplementary Protection Certificate for Medicinal Products)
Regulations 1992 (SI 1992/3091) and the Patents (Supplementary
Protection Certificate for Plant Protection Products) Regulations 1996
(SI 1996/3120). On the introduction of section 128B and Schedule 4A,
these original implementing Regulations were revoked. The general
application of the Act to supplementary protection certificates that
they set out was replaced, in Schedule 4A, with a more specific list of
provisions in the Act which apply (with the necessary glosses) to
supplementary protection certificates in the UK. Legislation equivalent
to the 1992 and 1996 implementing Regulations continued to apply in the
Isle of Man until the [Patents (Isle of Man) Order 2013 (SI
2013/2602)](http://www.legislation.gov.uk/uksi/2013/2602/made){rel="external"},
which modified the Patents Act as it applies to the Isle of Man to
introduce section 128B and Schedule 4A ([see paragraph
SP0.05](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSP0-05)).

### 128B.04 {#b04}

PR rr.73-88 is also relevant

The Medicinal Products and Plant Protection Products Regulations set out
a number of proceedings which may take place before the comptroller in
relation to supplementary protection certificates. These include
proceedings to apply for a declaration of invalidity of a certificate,
or to apply for revocation of a "paediatric" extension to a certificate.
All proceedings before the comptroller in relation to certificates fall
within the scope of Part 7 of the Patents Rules 2007, which also governs
procedures in relation to all proceedings before the comptroller in
relation to patents. Reference should be made to paragraphs [123.05 to
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05),
which set out in general terms how proceedings are launched under Part 7
of the Rules, and how such proceedings would then progress.

### 128B.05 {#b05}

Detailed commentary on the Medicinal Products and Plant Protection
Products Regulations, and on UK implementation and practice, is provided
in the separate part of this Manual which is dedicated to supplementary
protection certificates.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 128B(1)**
  Schedule 4A contains provision about the application of this Act in relation to supplementary protection certificates and other provision about such certificates.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 128B.06 {#b06}

Paragraph 1(1) of Schedule 4A makes clear that certain references in the
Act which relate to patents should, in relation to supplementary
protection certificates, be construed differently. Thus, for example,
references to a patent proprietor are construed as references to the
holder of a certificate, and references to a patented product or
invention are construed as references to a product for which a
certificate has effect. Paragraph 1(2) of the Schedule identifies the
sections of the Act to which these various glosses set out in paragraph
1(1) apply. Thus, for example, section 19(1) applies to an application
for a certificate, but the references in section 19(1) to the ability of
a patent applicant to amend their application before grant are construed
as references to the ability of an applicant for a certificate to amend
their application for a certificate before that certificate is granted.

### 128B.07 {#b07}

Paragraph 2 of the Schedule makes clear that certain provisions of the
Act which refer to a patent application are only construed as referring
to an application for a certificate in the circumstances where the
patent has expired while the application for the certificate is still
pending. This ensures that certain rights associated with a patent
application (notably, the provisional protection provided by section 69)
only become rights associated with an application for a certificate in
the circumstances where there are no patent rights still in force.

### 128B.08 {#b08}

Paragraph 3 of the Schedule makes clear that certain references in the
Act to the Act itself include within their meaning the Medicinal
Products and Plant Protection Products Regulations, and similarly that
certain references in the Act to a section of the Act include within
their meaning the equivalent provision of those Regulations. For
example, the references in section 124A to "requirements of the Act" are
taken to include requirements set out within the provisions of the
Medicinal Products or Plant Protection Products Regulations.

### 128B.09 {#b09}

Paragraph 4 of the Schedule deals with two specific glosses which
require more detailed explanation than those set out in the list in
paragraph 1(1) of the Schedule. In accordance with paragraph 4, the
reference in section 21(1) to the question of whether the invention is a
patentable one is to be construed, for the purposes of supplementary
protection certificates, as a question of whether the product is one for
which a certificate may have effect. Also, the condition in section
69(2)(b) is to be construed as being that the act in question would, if
the certificate had been granted on the date of publication of the
application, have infringed not only the certificate as granted but also
the certificate for which the application was made.

### 128B.10 {#ref128B-10}

Paragraph 5 of the Schedule states that a certificate does not take
effect unless the prescribed fee is paid before the end of the
prescribed period, or the prescribed fee and any prescribed additional
fee are paid before the end of the period of six months beginning
immediately after the prescribed period. See paragraphs [SPM12.01 to
12.15](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refspm12-01)
for a detailed discussion of fees in relation to certificates.

### 128B.11 {#b11}

Paragraph 6 of the Schedule states that expressions used in the Act that
are defined in the Medicinal Products or Plant Protection Products
Regulations have the same meaning as in those Regulations. Furthermore,
paragraph 6(2) ensures that, if those Regulations are amended in the
future, the references to them in the Act will continue to apply without
further amendment being needed. Paragraph 7 of the Schedule defines the
terms "Medicinal Products Regulation" and "Plant Protection Products
Regulation". This paragraph was amended by the Patents (Supplementary
Protection Certificate) Regulations 2014 (SI 2014/2411) in order to
update the definition of the Medicinal Product Regulation to refer to
the 2009 EC Regulation. Paragraph 8 of the Schedule sets out
transitional provisions which ensure that a reference in the Act to the
2009 EC Regulation is read as being, or including, a reference to the
superseded 1992 EEC Regulation for all relevant purposes.

  -----------------------------------------------------------------------
   

  **Section 128B(2)**

  In this Act a "supplementary protection certificate" means a
  certificate issued under---\
  (a) Regulation (EC) No 469/2009 of the European Parliament and of the
  Council of 6th May 2009 concerning the supplementary protection
  certificate for medicinal products, or\
  (b) Regulation (EC) No 1610/96 of the European Parliament and of the
  Council of 23 July 1996 concerning the creation of a supplementary
  protection certificate for plant protection products.
  -----------------------------------------------------------------------

### 128B.12 {#b12}

Subsection (2) defines the term "supplementary protection certificate"
for the purposes of the Act (and rules made under it). It was amended by
the Patents (Supplementary Protection Certificate) Regulations 2014 (SI
2014/2411) in order to update the reference to the Medicinal Product
Regulation. Under the European Union (Withdrawal) Act 2018, these
definitions are to be interpreted as references to the Regulations as
incorporated into domestic law ([see
SP0.13-0.13.2](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refsp0-13)).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
