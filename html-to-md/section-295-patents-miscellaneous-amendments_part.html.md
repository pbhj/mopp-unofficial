::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 295: Patents: miscellaneous amendments {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (295.01) last updated April 2007
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
The Patents Act 1949 and the Patents Act 1977 are amended in accordance
with Schedule 5.

### 295.01

This section makes various amendments to the Patents Acts 1949 and 1977
mainly to remove anomalies and simplify procedures. The amendments are
specified in Schedule 5 to the CDP Act, the first paragraph of which
relates to the 1949 Act. The remaining twenty-nine paragraphs relate to
the 1977 Act and their effects are discussed in the chapters about the
particular provisions in question.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
