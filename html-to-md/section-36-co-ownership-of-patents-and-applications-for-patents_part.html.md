::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 36: Co-ownership of patents and applications for patents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (36.01 - 36.09) last updated: April 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 36.01

s.36(7) is also relevant

The rights of and relating to co-owners of patents and applications are
laid down by this section. Although sub-sections (1) to (6) do not refer
to applications, their provisions have the same effect in relation to a
filed application for a patent as they have in relation to a granted
patent.

### 36.02

s.77(1) and s.78(2) is also relevant

This section also applies to granted European patents (UK) and
applications for European patents (UK).

### 36.03 {#ref36-03}

The provisions of sub-section (1) (that each co-owner is entitled to an
equal undivided share), sub-section (2) (regarding the right of a
co-owner to perform an act which would otherwise amount to an
infringement) and sub-section (3) (regarding the need for the consent of
all co-owners to amend the specification of the patent, apply for the
patent to be revoked, or to the grant of a licence or the assignment or
mortgage of a share etc) do not apply if there is any agreement to the
contrary.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 36(1)**
  Where a patent is granted to two or more persons, each of them shall, subject to any agreement to the contrary, be entitled to an equal undivided share in the patent.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 36.04 {#ref36-04}

In Florey & Others' Patent \[1962\] RPC 186, the hearing officer (when
considering the Patents Act 1949 s.54(1) which contained an almost
identical provision to s.36(1) regarding the entitlement of co-owners to
an equal undivided share in a patent) held that, if the existence of an
agreement to the contrary is not established beyond reasonable doubt,
the patentees are entitled to equal shares in the patent no matter what
their individual efforts amounted to in relation to the whole; and that
each co-patentee is therefore entitled to an equal share in any benefits
accruing from assignment of the patent. He further observed as a general
proposition that "whatever their several contributions may have been,
the members of a team pursuing different aspects of a research project
under the direction of a team leader should, in any event, be entitled
to an equal share in any benefit resulting from what must inevitably be
regarded as a joint effort".

### 36.05 {#section-2}

In Patchett's Patent \[1963\] RPC 90, the judge held (under the Patents
Act 1949) that unless there is some subsisting agreement to the
contrary, a co-patentee is entitled to receive some part of any payment
made in respect of Crown use of a patented invention, and has a prima
facie right to initiate a reference to the court of a dispute as to
Crown use; and that so long as the rest of the co-patentees were brought
into such reference, for which purpose their status as co-applicant or
respondent would appear to be immaterial, the issues between the
patentees and the Crown would be susceptible of complete resolution.

  -----------------------------------------------------------------------
   

  **Section 36(2)**

  Where two or more persons are proprietors of a patent, then, subject to
  the provisions of this section and subject to any agreement to the
  contrary-\
  (a) each of them shall be entitled, by himself or his agents, to do in
  respect of the invention concerned, for his own benefit and without the
  consent of or the need to account to the other or others, any act which
  would apart from this subsection and section 55 below, amount to an
  infringement of the patent concerned; and\
  (b) any such act shall not amount to an infringement of the patent
  concerned.
  -----------------------------------------------------------------------

### 36.06 {#ref36-06}

Section 55 referred to in s.36(2) provides that use of patented
inventions for the services of the Crown does not amount to infringement
([see also 36.03](#ref36-03)). In Henry Brothers (Magherafelt) Ltd. v
The Ministry of Defence and the Northern Ireland Office \[1997\] RPC
693, upheld by the Court of Appeal \[1999\] RPC 442, Jacob J held that
the term "agents" was not used in a strict sense. The patentee was
entitled to exploit his invention through others. Where an independent
contractor is used by one of the joint proprietors to perform an act
which would be an infringement of a patent if this section did not
apply, it must be considered whether the act is in substance licensing
or use by the joint proprietor for their own benefit.

  -----------------------------------------------------------------------
   

  **Section 36(3)**

  Subject to the provisions of sections 8 and 12 above and section 37
  below and to any agreement for the time being in force, where two or
  more persons are proprietors of a patent one of them shall not without
  the consent of the other or others -\
  (a) amend the specification of the patent or apply for such an
  amendment to be allowed or for the patent to be revoked, or\
  (b) grant a licence under the patent or assign or mortgage a share in
  the patent or in Scotland cause or permit security to be granted over
  it.
  -----------------------------------------------------------------------

### 36.07 {#ref36-07}

[See 36.03](#ref36-03). Amendment or applying for revocation of a
patent, and assignments, mortgages and the grant of licences etc under
s.30(2) or (4) or, in Scotland, s.31(2), are specifically subject to the
s.36(3) requirement for the consent of all co-owners. Sections 8, 12 and
37 referred to in s.36(3) relate to the determination of questions about
entitlement to applications and patents and provide in s.8(1)(b),
s.12(1)(b) and s.37(1)(c) for referral by a co-owner of the question
whether any right in or under an application or patent should be
transferred or granted to any other person. Thus the comptroller may
make an order under s.37(1) which over-rides the specific provisions of
s.36(3) -- this was confirmed in Hughes v Paxman \[2006\] EWCA Civ 818;
\[2007\] RPC 2. In this Court of Appeal judgment it was held that the
comptroller can order that licences under the patent be granted if they
consider that there is a deadlock situation between the co-proprietors.
The comptroller has a wide discretion in such circumstances but must act
rationally, fairly and proportionately having regard to all the
circumstances of the case, the aim being to produce a fair commercial
solution when co-owners cannot agree. On the facts of this particular
case the comptroller held that there was not a deadlock situation and
therefore gave no order that licences should be granted (see BL
O/217/08).

The hearing officer came to a similar conclusion in Taylor v Lanarkshire
Health Board (BL O/864/21 and BL O/157/22). It was determined that the
comptroller did not have discretion to order a royalty payment in
respect of revenue received by the defendant from the commercial
exploitation of the invention before the later finding that the claimant
was entitled to co-ownership of the patent. Despite the wide powers
available to the comptroller to make orders under section 37 (and, by
extension, under sections 8 and 12), the hearing officer declined to
determine an appropriate royalty rate that the claimant was entitled to
or to make an order giving effect to such a determination on the basis
that there was no deadlock situation needing to be resolved in regard to
the successful exploitation of the invention.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 36(4)**
  Subject to the provisions of those sections, where two or more persons are proprietors of a patent, anyone else may supply one of those persons with the means, relating to an essential element of the invention, for putting the invention into effect, and the supply of those means by virtue of this subsection shall not amount to an infringement of the patent.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 36.08 {#section-3}

Under s.60(2), a person who supplies any of the means, relating to an
essential element of an invention, for putting the invention into effect
may thus infringe a patent for the invention, ie contributory
infringement. Section 36(4) provides that such supply to a co-owner of a
patent does not constitute such infringement.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 36(5)**
  Where a patented product is disposed of by any of two or more proprietors to any person, that person and any other person claiming through him shall be entitled to deal with the product in the same way as if it had been disposed of by a sole registered proprietor.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 36.09 {#section-4}

s.130(7) and s.36(7) is also relevant.

The term "patented product" means a product which is a patented
invention or, in relation to a patented process, a product obtained
directly by means of the process or to which the process has been
applied. In that definition, "patented invention" means an invention for
which a patent is granted and "patented process" should be construed
accordingly. Section 36(5) has the same effect in relation to a filed
application for a patent as in relation to a granted patent and the
reference to a patented product should be construed accordingly. Any
such product disposed of by a co-owner can under s.36(5) be dealt with
by a recipient in the same way as if it had been disposed of by a sole
registered proprietor. The recipient is thus protected against any
infringement proceedings which might otherwise have arisen.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 36(6)**
  Nothing in subsection (1) or (2) above shall affect the mutual rights or obligations of trustees or of the personal representatives of a deceased person, or their rights or obligations as such.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 36(7)**

  The foregoing provisions of this section shall have effect in relation
  to an application for a patent which is filed as they have effect in
  relation to a patent and -\
  (a) references to a patent and a patent being granted shall accordingly
  include references respectively to any such application and to the
  application being filed; and\
  (b) the reference in subsection (5) above to a patented product shall
  be construed accordingly.
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
