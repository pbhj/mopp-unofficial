::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 39: Right to employees\' inventions {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (39.01 - 39.13) last updated: April 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 39.01

s.130(1) is also relevant.

This is the first of a group of sections (39 to 43) relating to
inventions made by employees. In the Act, unless the context otherwise
requires, "employee" means a person who works or (where the employment
has ceased) worked under a contract of employment or in employment under
or for the purposes of a government department or a person who serves
(or served) in the naval, military or air forces of the Crown. In
relation to an employee, "employer" means the person by whom the
employee is or was employed.

### 39.02

s.43(1), s.43(2) is also relevant.

Sections 39 to 42 do not apply to an invention made before the appointed
day (1 June 1978). Moreover, those sections do not apply to an invention
made by an employee unless at the time they made it either\
­(a) they were mainly employed in the UK; or\
(b) they were not mainly employed anywhere or their place of employment
could not be determined, but their employer had a place of business in
the UK to which the employee was attached, whether or not they were also
attached elsewhere.

### 39.03

s.43(3) is also relevant.

In sections 39 to 43, unless the context otherwise requires, references
to the making of an invention by an employee are references to their
making it alone or jointly with any other person, but do not include
references to the employee merely contributing advice or other
assistance in the making of an invention by another employee.

### 39.04

s.77(1), s.78(2) is also relevant.

Sections 39 to 43 apply in relation not only to 1977 Act patents and
applications but also to granted European Patents (UK) by virtue of
s.43(4). According to s.43(4) references in ss.39 to 42 to a patent and
to a patent being granted are respectively references to a patent or
other protection and to its being granted under any national or
international law, i.e. UK law, the law in force in any other country or
any treaty or international convention. A.60(1) EPC in association with
aa.4 and 5 of the Protocol on Jurisdiction and the Recognition of
Decisions in Respect of the Right to the Grant of a European Patent
under the EPC also permits the application of s.39 to applications for
European Patents (UK) where the employee is employed in the UK, subject
to any agreement to the contrary insofar as the national law governing
the contract allows the agreement in question.

  -----------------------------------------------------------------------
   

  **Section 39(1)**

  Notwithstanding anything in any rule of law, an invention made by an
  employee shall, as between him and his employer, be taken to belong to
  his employer for the purposes of this Act and all other purposes if ­\
  (a) it was made in the course of the normal duties of the employee or
  in the course of duties falling outside his normal duties, but
  specifically assigned to him, and the circumstances in either case were
  such that an invention might reasonably be expected to result from the
  carrying out of his duties; or\
  (b) the invention was made in the course of the duties of the employee
  and, at the time of making the invention, because of the nature of his
  duties and the particular responsibilities arising from the nature of
  his duties he had a special obligation to further the interests of the
  employer's undertaking.
  -----------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 39(2)**
  Any other invention made by an employee shall, as between him and his employer, be taken for those purposes to belong to the employee.
  ----------------------------------------------------------------------------------------------------------------------------------------

### 39.05

Section 39 gives criteria for determining whether an invention made by
an employee belongs to them or to their employer. Where rights to such
an invention are disputed by the inventor and their employer, subsection
(1) lays down the circumstances in which it belongs to the employer; it
otherwise belongs to the employee by virtue of subsection (2).

### 39.06

Since s.39 refers to an invention without any reference to a patent or
an application for a patent, the s.125 definition of an invention as
that specified in a claim cannot apply to s.39. The term "invention" in
s.39 should apparently instead be construed in its broadest sense of
that which has been invented, in much the same way as when considering
questions about entitlement under s.8, [see
8.06-8.09](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-06)

### 39.07

s.39(1) is also relevant.

In order for the invention to belong to the employer it must, firstly,
have been made in the course of the duties of the employee (which may,
in subsection (1)(a), be either their "normal" duties or other duties
"specifically assigned to them"). In addition, either the circumstances
must have been such that an invention might reasonably be expected to
result from the carrying out of the employee's duties (subsection
(1)(a), [see 39.10](#ref39-10)) or the employee must have had as a
result of their duties a special obligation to further the interests of
the employer's undertaking (subsection (1)(b)).

### 39.08 {#ref39-08}

s.42(2) is also relevant.

The provisions of subsection (1) apply notwithstanding anything in any
rule of law. They thus override the provisions of all other enactments
except any relevant enactment of later date. Moreover, any term in a
contract entered into by the employee with or at the request of the
employer which diminishes the employee's rights in inventions made by
them after the appointed day (1 June 1978) and the date of the contract
is unenforceable, [see
42.03­-04](/guidance/manual-of-patent-practice-mopp/section-42-enforceability-of-contracts-relating-to-employees-inventions/#ref42-03).

### 39.09 {#ref39-09}

Section 39 lays down for the first time statutory criteria for
determining rights in employee's inventions; this was previously a
matter of common law. The Patents Court, in Harris' Patent \[1985\] RPC
19, entertained some doubt as to whether s.39 is declaratory of the
previous common law position. The Court held that, although guidance may
be obtained from earlier cases as to how courts assessed the duties of
the employee in a particular case and particular circumstances, and the
extent and nature of an employee's obligation to further the interests
of the employer's undertaking, it is the provisions of s.39 alone to
which regard must be had for the law governing any employee's invention
made after 1 June 1978. The Court of Appeal, in LIFFE Administration and
Management v Pavel Pinkava \[2007\] RPC 30, confirmed that it is the
provisions of s.39 alone to which regard must be had for the law
governing an employee's invention and noted that sections 39 to 43 of
the Patents Act 1977 is more favourable to the employee than the
previous common law rules. There is no reason to interpret s.39 by
reference to any previous law and there is no reason to imply any
further condition or qualification to the requirements of s.39.

### 39.10 {#ref39-10}

The Patents Court in Harris' Patent also held that the expression "an
invention" in s.39(1)(a) cannot mean any invention whatsoever. It is
governed by the qualification that it has to be an invention that "might
reasonably be expected to result from the carrying out of his duties" by
the employee and, therefore, must be referring to an invention which
achieves, or contributes to achieving, whatever was the aim or object to
which the employee's efforts in carrying out their (normal or
specifically assigned) duties were directed, i.e. such an invention as
that made, though not necessarily the precise invention actually made
and in question. Kitchin J in LIFFE Administration and Management v
Pavel Pinkava and De Novo Markets Limited \[2006\] EWHC 595 (Pat)
however cautioned against using this as a substitute for the statutory
test in s.39(1)(a). If that test was satisfied, it was not relevant to
say that the invention in question did not achieve the particular aim or
object to which the inventor's efforts were directed. Moreover, the
"circumstances" referred to in s.39(1)(a) are the circumstances in which
the invention in question was made. The Court of Appeal, in LIFFE
Administration and Management v Pavel Pinkava \[2007\] RPC 30, agreed
with the conclusion of Kitchin J and rejected the submission that the
words "an invention" in s.39(1)(a) should be read as "the invention" or
"a similar invention". Furthermore, the invention does not need to
provide a solution to a pre-identified problem.

### 39.11 {#ref39-11}

When considering the normal duties of an employee, the Court in Harris'
Patent further held that their duty of fidelity to their employer is to
carry out faithfully the work they are employed to do to the best of
their ability and does not assist in the formulation of the actual
duties they are employed to do. In LIFFE Administration and Management v
Pavel Pinkava \[2007\] RPC 30, the Court of Appeal held that although
the source of an employee's duty is primarily contractual, the contract
evolves in the course of time such that it is unsafe to have regard only
to the terms contained in an initial written contract of employment.
Furthermore, some of the terms of an employee's duties may be implied by
law.

In Prosyscor Ltd v Netsweeper Inc & Ors \[2019\] EWHC 1302 (IPEC) the
court notes that the time and place of devising an inventive concept may
be relevant but that they are a secondary matter to be considered when
there is doubt as to the duties expected of an employee. In this case
the devising of the inventive concept fell squarely within the
employee's named duties; the fact that the work was carried out in the
employee's home and using their own equipment makes no difference, the
employer was still entitled to the invention.

### 39.12 {#ref39-12}

The special obligation of an employee to further the interests of the
employer's undertaking was decisive in Unitec Systems' Application (BL
O/143/94) where the invention lay in the field of business of a company,
whose joint managing directors at the time of the invention were the
patent applicants. The hearing officer held that the patent applicants
were employees, even though they had not signed a contract of employment
until after the invention had been made, and that therefore s.39
applied. Moreover, although he found that the invention had not been
made in the course of the patent applicants' "normal" duties as required
by s.39(1)(a), he held, following the guidelines set out in Harris'
Patent, that the s.8 reference succeeded since the invention had been
made in the course of the duties of the patent applicants, who were not
relieved to any degree of the "special obligation" referred to in
s.39(1)(b) and implicit in their titles.

### 39.12.1 {#ref39-12-1}

In Ultraframe UK Ltd v Fielding \[2004\] RPC 24, which concerned the
ownership of design rights, the Court of Appeal held that a contract of
service existed if three conditions were fulfilled: 1) the servant
agreed that, in consideration of a wage or other remuneration, they
would provide their own work and skill in the performance of some
service for their master; 2) they agreed, expressly or impliedly, that
in the performance of that service, they would be subject to the other's
control in a sufficient degree to make that other master; and 3) the
provisions of the contract were consistent with its being a contract of
service. A 100% shareholder and director who was not under obligation to
be at work certain hours or to produce designs in return for a wage was
not subject to a contract of service and was therefore not employed by
the companies they had operated their business through. However, the
Court held that they held any design rights in trust for the company
through whom they were operating the business as at the time they
created those design rights.

  -----------------------------------------------------------------------
   

  **Section 39(3)**

  Where by virtue of this section an invention belongs, as between him
  and his employer, to an employee, nothing done\
  (a) by or on behalf of the employee or any person claiming under him
  for the purposes of pursuing an application for a patent, or\
  (b) by any person for the purpose of performing or working the
  invention,\
  shall be taken to infringe any copyright or design right to which, as
  between him and his employer, his employer is entitled in any model or
  document relating to the invention.
  -----------------------------------------------------------------------

### 39.13 {#section-7}

Subsection (3) was added by the CDP Act. This provides that acts done
for the purposes of patenting, performing or working the invention shall
not be taken to infringe any copyright or Design Right pertaining to the
invention and belonging to the employer. This prevents an employer using
copyright or Design Right to frustrate an employee who tries to patent
or exploit an invention which rightly belongs to the employee.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
