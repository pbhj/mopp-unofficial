::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 280: Privilege for communications with patent agents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (280-01 280.05) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 280.01 {#ref280-01}

This replaced section 104 of the Patents Act 1977. It provides that
communications between a person and their patent agent (who may be a
registered patent attorney, a person on the European list or a
partnership or company entitled to describe itself as one of these) are
privileged from disclosure in legal proceedings. The scope of the
privilege is extended beyond that conferred under the 1977 Act (which
was limited to proceedings under that Act) to cover other industrial
property topics. Following amendments to the section on 1 January 2010
by the Legal Services Act 2007, the application of privilege is widened
to encompass documents, materials and information as detailed in
paragraph 77 of Schedule 21 to the Legal Services Act 2007.

  -----------------------------------------------------------------------
   

  **Section 280(1)**

  This section applies to--\
  (a) communications as to any matter relating to the protection of any
  invention, design, technical information, or trade mark, or as to any
  matter involving passing off, and\
  (b) documents, material or information relating to any matter mentioned
  in paragraph (a).
  -----------------------------------------------------------------------

### 280.02 {#section}

Subsection (1) defines the scope of the privilege with regard to the
topics and format to which communications may relate. They are the
protection of any invention, design, technical information, or trade
mark; and passing off. Any matter including documents, materials or
information relating to these is covered, with the intention of
affording privilege to all advice that patent agents are competent to
give, sometimes going beyond the strict boundaries of the listed
subjects, eg to include copyright issues relating to the 'get-up' of a
product.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 280(2)**
  Where a patent attorney acts for a client in relation to a matter mentioned in subsection (1), any communication, document, material or information to which this section applies is privileged from disclosure in like manner as if the patent attorney had at all times been acting as the client's solicitor.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 280(3)**

  In subsection (2) "patent attorneys" means ­\
  (a) a registered patent attorney or a person who is on the European
  list,\
  (b) a partnership entitled to describe itself as a firm of patent
  attorneys or as a firm carrying on the business of a European patent
  attorney, or (ba) an unincorporated body (other than a partnership)
  entitled to describe itself as a patent attorney, or\
  (c) a body corporate entitled to describe itself as a patent attorney
  or as a company carrying on the business of a European patent attorney.
  -----------------------------------------------------------------------

### 280.03 {#section-1}

Such communications and information are privileged from disclosure in
legal proceedings in England, Wales, Scotland and Northern Ireland in
the same way as communications with a solicitor. The information is
treated as if the patent attorney had been acting as a solicitor.

### 280.04 {#ref280-04}

In Sonic Tape PLC's Patent \[1987\] RPC 251, the hearing officer found
the parties to be joint holders of privilege in one letter. This
privilege could be claimed vis-a-vis a third party but not against the
other joint holder; the letter could therefore be used in evidence.
Aldous J in Lubrizol Corpn v Esso Petroleum \[1993\] FSR 64 found that
privilege did not attach to documents in the possession of both parties
unless they would indicate either the advice that might be sought or the
advice that was given. In the same case Aldous J observed that it was
incredible that any distinction concerning privilege could be drawn
between an original document and a copy of it.

### \[Section 280(4) Repealed\]

### 280.05 {#section-2}

Section 280(4) extended with equivalent effect the privilege from
disclosure in legal proceedings to legal proceedings in Scotland. The
section has now been omitted under s.208 and paragraph 77(e) of schedule
21 of the Legal Services Act 2007.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
