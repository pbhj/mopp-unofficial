::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 70D: Professional advisers {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (70D.01-70D.05) last published: 2017.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 70D(1)

Proceedings in respect of an actionable threat may not be brought
against a professional adviser (or any person vicariously liable for the
actions of that professional adviser) if the conditions in subsection
(3) are met.

### 70D.01 {#d01}

Section 70D(1) sets out that a professional adviser cannot be sued for a
threat made on behalf of a client if certain conditions are met ([see
70D.03](#ref70D-03)).

### Section 70D(2)

In this section "professional adviser" means a person who, in relation
to the making of the communication containing the threat---\
(a) is acting in a professional capacity in providing legal services or
the services of a trade mark attorney or a patent attorney, and\
(b) is regulated in the provision of legal services, or the services of
a trade mark attorney or a patent attorney, by one or more regulatory
bodies (whether through membership of a regulatory body, the issue of a
licence to practise or any other means).

### 70D.02 {#d02}

Section 70D(2) defines a "professional adviser" as someone who is
providing legal services and is regulated by one or more regulatory
bodies, such as the Intellectual Property Regulation Board (IPReg). This
provision does not affect the principle that anyone, not just the patent
holder, may be liable for making a threat.

### Section 70D(3)

The conditions are that---\
(a) in making the communication the professional adviser is acting on
the instructions of another person, and\
(b) when the communication is made the professional adviser identifies
the person on whose instructions the adviser is acting.

### 70D.03 {#ref70D-03}

Section 70D(3) sets out the conditions that must be met for the
professional adviser to be protected by section 70D. These conditions
apply to both a single instruction to send a specific communication
given to a professional adviser and an in-house adviser with a general
instruction to protect the intellectual property rights of a specific
company. In both cases, the professional adviser is acting on the
instructions of another and not of their own volition. This protection
applies equally to non-UK advisers if the above conditions are met.

### Section 70D(4)

This section does not affect any liability of the person on whose
instructions the professional adviser is acting.

### 70D.04 {#d04}

Section 70D(4) states that this protection only relates to the
professional adviser. Any liability incurred by the client (patent
holder) for making threats is unaffected and a threats action may be
brought against the client.

### Section 70D(5)

It is for a person asserting that subsection (1) applies to prove (if
required) that at the material time-\
(a) the person concerned was acting as a professional adviser, and\
(b) the conditions in subsection (3) were met.

### 70D.05 {#d05}

The onus is on the professional adviser to prove that they were acting
as a professional adviser and that the conditions set out in section
70D(3) ([see 70D.03](#ref70D-03)) were met at the time the threat was
sent.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
