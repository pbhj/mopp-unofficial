::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 89: Effect of international application for patent {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (89.01 - 89.10) last updated October 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 89: Effect of international application for patent {#section-89-effect-of-international-application-for-patent}

### 89.01 {#ref89-01}

s.130(1) is also relevant

Sections 89, 89A and 89B make provision for an international application
under the Patent Cooperation Treaty to be treated as an application for
a patent under the Patents Act 1977. An international application for a
patent (UK) is an application which has been filed under the PCT and
which, on its date of filing, separately designated the United Kingdom
as a country in which protection is sought. [See 89.08](#ref:89-08). In
Archibald Kenrick & Sons Ltd's International Application \[1994\] RPC
635, which related to a postal delay affecting an international
application filed at the UK Office as receiving Office, Aldous J
described the PCT as "a complete code". This comment was re-iterated in
[Abaco Machines (Australasia) Pty Ltd's Application \[2007\] EWHC 347
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2007/347.html){rel="external"}
where Lewison J commented that a PCT application cannot be an
application under the 1977 Act except to the extent that the Act says it
is. This case concerned a request to make a late declaration of priority
under section 5(2B) for a national application when the applicants
intention had been to file a PCT application within the twelve month
priority period. He said:

> In my judgment Mr Mitcheson's submission, if accepted, would amount to
> a major breach in the complete code of the PCT. The PCT undoubtedly
> has advantages to those who make international applications under it.
> As Mr Mitcheson said, these advantages include savings in cost and the
> flexibility given to applicants to decide whether to enter the
> national phase in all or only some of the territories designated in
> the international phase. But the PCT is a package. Part of the package
> is, for the moment, a rigid timetable. As Mr Birss submitted, having
> chosen to use the PCT route, Abaco must take the PCT system as they
> find it.

\[Deleted\]

### 89.02 {#section}

The international application must be filed at a receiving office under
the PCT. Rule 65 relates to the role of this Office as such a receiving
office. The Office ceased to receive new demands for international
preliminary examination after 28 May 1993 in accordance with the EPC
Protocol on Centralisation.

### 89.03 {#ref89-03}

The application is searched by an International Searching Authority
(ISA), such as the EPO in the case of applications received at the UK
Office. The ISA also produces a written opinion on patentability based
on the results of its search. Applicants also have the option of
requesting one or more additional "supplementary international searches
(SIS)" which are each carried out by an ISA other than the one that
performed the main international search. A SIS usually covers the same
claims as the main international search, but where plurality has been
reported by the main ISA the applicant may choose which invention is to
be the subject of the SIS. The international application is published by
the International Bureau of WIPO, together with the main international
search report (if completed); the written opinion and any supplementary
international search reports are not included in the published
specification. The procedures for search, issue of a written opinion and
publication of international applications are governed by PCT Chapter I.
After receipt of the international search report and written opinion,
the applicant may then request international preliminary examination
under PCT Chapter II. The International Bureau issue an international
preliminary report on patentability (IPRP) on all international
applications, regardless of whether international preliminary
examination is requested under Chapter II. For applications undergoing
Chapter I processing only, the IPRP is established on the basis of the
written opinion of the ISA. Where the applicant elects to have
international preliminary examination under Chapter II, the
International Preliminary Examination Report (IPER) is issued as the
IPRP. The IPRP provides a non-binding opinion on (i) unity of invention,
(ii) whether each individual claim meets the criteria of novelty,
inventive step and industrial applicability, (iii) any lack of clarity
in the claims or description or lack of support for the claims in the
description and (iv) whether any amendments add subject matter.

### 89.03.1 {#section-1}

PCT a.30(1), PCT r.94.1, PCT r.17.2(c), PCT a.38, s.89A(3) and r.66(1)
are also relevant

In accordance with PCT Article 30(1), the international application is
confidential prior to international publication and may not be accessed
by any person or authority, unless requested or authorised by the
applicant, except for the transmission of information specifically
required under the PCT for the purposes of processing the application.
After international publication, documents held in the International
Bureau's files are made publicly available online via WIPO's
[Patentscope](https://patentscope.wipo.int/search/en/search.jsf){rel="external"}
database. The available documents include the international application
itself, any amendments under PCT Article 19, the international search
report (these elements are included in the published pamphlet), the
priority document(s) (unless the relevant priority claim was withdrawn
or considered not to have been made prior to international publication),
the written opinion of the ISA, any supplementary international search
reports, the IPRP, once it has been established, and any third party
observations submitted during the international phase. However,
documents relating to international preliminary examination are not made
available as PCT Article 38 prohibits access to these unless requested
or authorised by the applicant.

### 89.03.2 {#section-2}

The applicant has a period of 31 months from the (priority) date of the
international application in which to enter the UK national phase in
accordance with ss.89, 89A and 89B ([see
89A.05](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-05)).
This period is not affected by whether the application is elected under
PCT Chapter II.

### 89.04 {#section-3}

\[deleted\]

\[ Code of practice for applicants and agents \]

International applications for patents

The legal requirements of the Patent Cooperation Treaty (PCT) differ
from those of the Patents Act in important ways and those filing
international applications with the UK Office as PCT Receiving Office
need to take particular care to follow PCT requirements. It may not be
possible to remedy some problems if they are not spotted in time. The
more important points are summarised in a checklist below. The
International Unit of the Patents Directorate can provide further
guidance and queries may be made by email <pct@ipo.gov.uk> or by
telephone on +44 0)1633 814586.

### Point 10: International applications

-   a the provisions of the Patent Cooperation Treaty, eg as to forms
    and formalities, should be observed when filing international
    applications with the UK Office as PCT Receiving Office

-   b a power of attorney and/or a copy of a previously-filed general
    power of attorney is needed: (i) for all withdrawal requests;
    and (ii) where it is unclear that an agent or common representative
    has power to act on behalf of the applicant

-   c withdrawals: Notices of withdrawal should be sent to the
    International Bureau in Geneva, with a copy sent to the Intellectual
    Property Office (see also point b above)

### PCT filing checklist

-   claims - It is vital for claims to be included in the international
    application if it is to be awarded an international filing date

-   timing - If it is critical that an international application be
    filed within a certain time window such as the twelve months of the
    Paris Convention, the application should be filed with sufficient
    time remaining for any fatal defects to be detected and put right

-   application form - It is necessary to complete a Request Form (Form
    PCT/RO/101), and it is important that the current version be used.
    British nationality and residency should be shown as either "GB" or
    "United Kingdom". Applicants should be identified by family name
    followed by forename(s). It is possible to request correction of a
    PCT form; in some circumstances we may request a replacement form be
    provided

-   fees and copies - A PCT fee sheet should be completed if fees are
    being paid. Further details can be found of fees payable for [PCT
    applications](https://www.gov.uk/government/collections/patent-cooperation-treaty-pct-fees)
    on the website. If there is doubt over what fee is due the Receiving
    Office will clarify this by issuing a Form PCT/RO/102

-   Filing PCT applications electronically - PCT applications can be
    filed electronically using EPO Online Services or
    [ePCT](https://www.gov.uk/government/publications/how-to-file-documents-with-the-intellectual-property-office/how-to-file-documents-with-the-intellectual-property-office).
    When using EPO Online Services, it is important that the latest
    version of the software is used to ensure that the request form and
    fees are current\]

  -----------------------------------------------------------------------
   

  **Section 89(1)**

  An international application for a patent (UK) for which a date of
  filing has been accorded under the Patent Co-operation Treaty shall,
  subject to section -\
  \
  89A (international and national phases of application), and\
  \
  section 89B (adaptation of provisions in relation to international
  application),\
  \
  be treated for the purposes of Parts I and III of this Act as an
  application for a patent under this Act.
  -----------------------------------------------------------------------

### 89.05 {#ref89-05}

Section 89 provides for an international application for a patent (UK)
filed under the PCT to be treated from the date of filing as an
application under the 1977 Act, subject to sections 89A and 89B.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 89(2)**
  If the application, or the designation of the United Kingdom in it, is withdrawn or (except as mentioned in subsection (3)) deemed to be withdrawn under the Treaty, it shall be treated as withdrawn under this Act.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 89(3)**

  An application shall not be treated as withdrawn under this Act if it,
  or the designation of the United Kingdom in it, is deemed to be
  withdrawn under the Treaty (a) because of an error or omission in an
  institution having functions under the Treaty, or\
  \
  (b) because, owing to circumstances outside the applicant's control, a
  copy of the application was not received by the International Bureau
  before the end of the time limited for that purpose under the Treaty,
  or in such other circumstances as may be prescribed.
  -----------------------------------------------------------------------

### 89.06 {#ref89-06}

r.71(2), r.71(7), PCT a.25(1), PCT r.51.1, r.72 PCT and a.48(2)(a) are
also relevant

Subsections (2) and (3) deal with the effects under the 1977 Act of
actual or deemed withdrawal under the PCT. The application is treated as
withdrawn under the Act unless it was deemed to be withdrawn under the
PCT in circumstances referred to in subsection (3), in which case the
applicant may apply in writing for the international application to be
or continue to be treated as an application under the 1977 Act. This
should be accompanied by a statement of facts on which the applicant
relies. A translation of any document which is not in the English or
Welsh language should be filed in accordance with r.113(1) and (2).
Where section 89(3) applies such that the application is not to be
treated as withdrawn, the comptroller may amend any document kept at the
Office in relation to the application and alter any period of time
(whether it has already expired or not) specified in the Act or listed
in Parts 1 to 3 of Schedule 4 to the Patents Rules 2007 subject to such
conditions as the comptroller may direct (including payment of any
appropriate prescribed fee (including extension fees) but not that under
s.89A(3)). The time within which an application under s.89(3) can be
made is not in itself limited by the Regulations under the PCT
[Fletcher's Application BL
O/235/98](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/235/98){rel="external"}.
However it will normally be necessary to request the International
Bureau to send copies of the documents from the file to the Office and
such a request must be made within two months of the notification sent
to the applicant that their application has been treated as withdrawn.

\[ An application filed with the Office under s.89(3) should be referred
to the appropriate legal adviser in the Registered Rights Legal Team. \]

### 89.07 {#section-4}

The comptroller's power to reinstate applications deemed to be withdrawn
has been extended in subsection (3) to cover such circumstances as may
be prescribed, in addition to the particular circumstances (a) and (b)
above. The other circumstance prescribed for the purposes of section
89(3) by rule 72 is where the comptroller determines that, in comparable
circumstances in relation to an application under the Act (other than an
international application for a patent (UK)), they would have exercised
their powers under rule 107 or 108 to prevent the application being
treated as withdrawn.

The comptroller's powers to amend documents and alter periods of time in
this context thus apply in the same way as for equivalent situations
relating to a domestic application. This accords with the PCT
requirement that any Contracting State shall, as far as that State is
concerned, excuse, for reasons admitted under its national law, any
delay in meeting any time limit.

### \[Section 89(4) repealed\]

### 89.08 {#ref:89-08}

If an international application should not to be treated as an
international application for a patent (UK) by virtue of designating
EP(UK) and not separately designating the UK. This subsection was
replaced by section 130(4A), which reworded this subsection to bring it
into consistency with the PCT, where all contracting states and regions
are automatically designated on filing ([see
130.27.1](/guidance/manual-of-patent-practice-mopp/section-130-interpretation/#ref130-27-1)).
(Applications for a European patent (UK) initiated by an international
application under the PCT are dealt with by s.79.)

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 89(5)**
  If an international application for a patent which designates the United Kingdom is refused a filing date under the Treaty and the comptroller determines that the refusal was caused by an error or omission in an institution having functions under the Treaty, he may direct that the application shall be treated as an application under this Act, having such date of filing as he may direct.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 89.09 {#ref89-09}

r.71 PCT a.25(2)(a) PCT r.51(3) PCT r.51(1) and PCT r.51(2) are also
relevant

Subsection (5) provides for the comptroller to direct that an
application is to be treated as one under the 1977 Act (having such date
of filing as the comptroller may direct) when it has been refused a
filing date as an international one because of an error in an
institution having functions under the PCT. A written request for such a
direction should be accompanied by a statement of the reasons for the
request and the fee prescribed for the purposes of section 89A(3). A
translation of any document which is not in the English language should
be filed in accordance with r.113(1) and (2). If the application is to
be treated as one under the 1977 Act, the comptroller may amend any
document kept at the Office in relation to the application and alter any
period of time which is specified in the Act or listed in Parts 1 to 3
of Schedule 4 to the Patents Rules 2007 (whether it has already expired
or not), subject to such conditions as the comptroller may direct. The
time within which an application under s.89(5) can be made is not in
itself limited by the Regulations under the PCT [cf Fletcher's
Application BL
O/235/98](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/235/98){rel="external"}.
However it will normally be necessary to request the International
Bureau to send copies of the documents from the file to the Office and
such a request must be made within two months of the notification sent
to the applicant that their application has been refused a filing date.

\[ An application for directions under s.89(5) should be referred to the
appropriate legal adviser in the Registered Rights Legal Team. \]

### 89.10 {#ref89-10}

Following refusal of the United States Patent and Trademark Office
(USPTO) to treat a purported international application deposited with
them as an international application because the applicants had declared
themselves as residents of a country which was not a PCT member country,
an application was made under s.89(5) for a direction that the
application shall be treated as an application under the Act. In
refusing to treat the purported international application in this way,
the hearing officer decided that the USPTO had not erred both in
refusing a filing date under the PCT and failing that in not
transmitting the application to the International Bureau under PCT Rule
19.4 (Unique Products & Design Co Ltd's Application BL O/147/95).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
