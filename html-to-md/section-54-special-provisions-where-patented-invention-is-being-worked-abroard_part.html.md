::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 54: Special provisions where patented invention is being worked abroad {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (54.01 - 54.04) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 54.01 {#ref54-01}

s.54(2) is also relevant

This section provides for Orders in Council to preclude orders regarding
licences and "licences of right" entries in the register on applications
under ss. 48 to 51. Any such Order in Council would specify one or more
non-EU countries having reciprocal arrangements with the UK and would
preclude such orders or entries if the invention concerned is being
commercially worked in any such country and demand in the UK is being
met by importation from that country. Orders or entries can however be
made for purposes of the public interest. No Order in Council under s.54
has yet been made. Subsection (2) was amended by the Patents and Trade
Marks (World Trade Organisation) Regulations 1999 to include the words
"or a member of the World Trade Organisation".

### 54.02 {#section}

Similar provision in respect of EU countries is made by s.53(1), [see
53.03](/guidance/manual-of-patent-practice-mopp/section-53-compulsory-licences-supplementary-provisions/#ref53-03).

### 54.03 {#section-1}

Section 54 is the last of a group of sections (46 to 54) relating to
licences of right, compulsory licences etc. [See
46.02-07](/guidance/manual-of-patent-practice-mopp/section-46-patentee-s-application-for-entry-in-register-that-licences-are-available-as-of-right/#ref46-02)
for general discussion of this group.

### 54.04 {#section-2}

Section 54 is one of the sections mentioned in s.130(7) as being so
framed as to have, as nearly as practicable, the same effects as the
corresponding provisions of the EPC, CPC and PCT. Articles 47 and 82 of
the CPC (renumbered as Articles 46 and 77 \[1989\]) correspond except
that they apply to CPC contracting states [see
53.03](/guidance/manual-of-patent-practice-mopp/section-53-compulsory-licences-supplementary-provisions/#ref53-03).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 54(1)**
  His Majesty may by Order in Council provide that the comptroller may not (otherwise than for purposes of the public interest) make an order or entry in respect of a patent in pursuance of an application under sections 48 to 51 above if the invention concerned is being commercially worked in any relevant country specified in the Order and demand in the United Kingdom for any patented product resulting from that working is being met by importation from that country.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 54(2)**
  In subsection (1) above "relevant country" means a country other than a member state or a member of the World Trade Organisation whose law in the opinion of His Majesty in Council incorporates or will incorporate provisions treating the working of an invention in, and importation from, the United Kingdom in a similar way to that in which the Order in Council would (if made) treat the working of an invention in, and importation from, that country.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
