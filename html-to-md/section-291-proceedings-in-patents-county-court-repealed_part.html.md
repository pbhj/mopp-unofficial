::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 291: Proceedings in patents county court \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (291.01 - 291.03) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 291.01

This section covered two distinct areas affecting proceedings in the
patents county court: the first the nomination and duties of a judge,
and secondly the appointment and remuneration of advisers or assessors
to assist the Court, and the provision of reports by the Office.

### 291.02

Section 291 required county court rules to make appropriate provision in
the above respects, and such provision was made in the Civil Procedure
Rules, particularly Part 63. This section was amended by the
Constitutional Reform Act 2005 to provide a role for the Lord Chief
Justice of England and Wales or a judicial office holder appointed by
them to nominate a judge of the patents county court.

### 291.03

Section 291 has been repealed by Schedule 9, paragraph 30 of the Crimes
and Courts Act 2013
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
