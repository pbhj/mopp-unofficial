::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 101: Exercise of comptroller\'s discretionary powers {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (101.01 - 101.45) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 101.01

This section applies to applications and patents under the 1977 Act.
Some requirements relating to hearings are prescribed in Part 7 of the
Patents Rules 2007 (Proceedings heard before the comptroller),
particularly at rr. 80, 82 and 84 ([see
123.05.02](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-02)
and [123.05.10 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-10)).

\[ The Patent Hearings Manual should be consulted for guidance on the
conduct of hearings and procedures for appointing and conducting
hearings and the giving and issuing of decisions. Some of the relevant
parts of that Manual are referred to below. Further information is also
given in the Tribunal Patents Manual. \]

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 101**
  Without prejudice to any rule of law, the comptroller shall give any party to a proceeding before him an opportunity of being heard before exercising adversely to that party any discretion vested in the comptroller by this Act or rules.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 101.02 {#ref101-02}

This section applies to both ex parte proceedings (that is, where the
proceedings involve no other party than the applicant or proprietor) and
inter partes disputes. In particular, before an application for a patent
is refused the applicant must be offered an opportunity to be heard;
following a hearing (whether attended by the applicant or not) refusal
would normally only be made by a formal decision. ([See also
18.79-18.80](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-79)).
If a party to any proceedings wishes to be heard, they must be given
notice of a date for the hearing.

### 101.02.1 {#section-1}

Anyone is entitled under s.22 of the Welsh Language Act 1993 to have
legal proceedings (such as hearings under s.101) which are held in Wales
conducted in Welsh without prior notice. Also, if the proceedings are
held in Wales then any oath or affirmation can be given in Welsh. S.24
of the Welsh Language Act provides for the use of interpreters in any
such proceedings conducted in Welsh.

### 101.02.2 {#section-2}

In the event that a party requests that a hearing should be conducted in
Welsh, the hearing officer should ascertain that the party is unwilling
to continue in English. If the request is maintained, the hearing
officer should adjourn the proceedings until an interpreter can be
obtained.

\[ There is a list of Office personnel able and prepared to act in this
capacity available on Circle. If an interpreter cannot be obtained
through ARD, PD/DL should be consulted. \]

### 101.03 {#section-3}

A hearing must be before a hearing officer who is authorised to act for
the comptroller ([see
130.05.1](/guidance/manual-of-patent-practice-mopp/section-130-interpretation/#ref130-05-01)).
\[ See chapters 4 and 6 of the Patent Hearings Manual regarding the
appointment of the hearing officer.\]

### 101.04 {#section-4}

\[deleted\]

### 101.05 {#section-5}

r.118 is also relevant

A report of a decision of a hearing officer may be published in the
Reports of Patent, etc. Cases (RPC) if this is considered to be
generally useful or important.

\[ A hearing officer may recommend that a particular case should be
reported - see chapter 5 of the Patent Hearings Manual. \]

### 101.06 {#ref101-06}

r.107, s.97(1) is also relevant

Once a hearing officer has given a decision it cannot normally be
reviewed or reversed within the Office, although exceptionally it may be
possible for the hearing officer to re-open issues after judgment, as
explained in Interfilta (UK) Ltd's Patent \[2003\] RPC 22. The decision
can only be referred to the Patents Court on appeal (except where that
is precluded - see s.97) or to the Divisional Court.

### 101.07 {#section-6}

The way in which hearing officers conduct proceedings before them is
subject to the general surveillance of the Administrative Justice and
Tribunals Council (previously the Council of Tribunals), who investigate
complaints from the public and publish an annual report of their
investigations. A member of the Council may attend hearings.

### 101.08 to 101.45 {#ref101-20}

\[deleted\]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
