::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 91: Evidence of conventions and instruments under conventions {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (91.01 - 91.03) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 91.01

s.91(6) is also relevant

This section is concerned with the status of the EPC, CPC and PCT, and
instruments, publications and decisions thereunder, in legal proceedings
(including those before the comptroller).

  -----------------------------------------------------------------------
   

  **Section 91(1)**

  Judicial notice shall be taken of the following, that is to say\
  (a) the European Patent Convention, the Community Patent Convention and
  the Patent Co-operation Treaty (each of which is hereafter in this
  section referred to as the relevant convention);\
  (b) any bulletin, journal or gazette published under the relevant
  convention and the register of European patents kept under the European
  Patent Convention; and\
  (c) any decision of, or expression of opinion by, the relevant
  convention court on any question arising under or in connection with
  the relevant convention.
  -----------------------------------------------------------------------

### 91.02 {#ref91-02}

s.130(1), s.91(6) is also relevant

Judicial notice should be taken of those conventions and that treaty,
publications thereunder including the EPO register of European patents,
and decisions by the relevant convention courts. This means that these
matters are recognised without need for formal evidence of their
existence. The relevant convention court means that court or other body
which under the relevant convention or treaty has jurisdiction over the
proceedings in question, including (where it has such jurisdiction) any
department of the EPO but excluding the national courts of the UK and
other contracting states. In his judgment in the Court of Appeal in
Genentech Inc's Patent \[1989\] RPC 147, Mustill L J said "The
requirement that the court shall take judicial notice of the decision of
the relevant convention court is directed (like the remainder of the
section) to evidentiary matters; in this case the mode of proof of
matters which might otherwise have to be proved as foreign law. The
subsection does not give to rulings of other courts any greater status
than they would otherwise possess, although of course, the desirability
for a uniform course of decision in matters touching the Convention is
manifest (see, for example, section 130(7)), and the Board of Appeal as
the central decision making body of the European patent system must be
hearkened to with particular attention."

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 91(2)**
  Any document mentioned in subsection (1)(b) above shall be admissible as evidence of any instrument or other act thereby communicated of any convention institution.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 91(3)**
  Evidence of any instrument issued under the relevant convention by any such institution, including any judgment or order of the relevant convention court, or of any document in the custody of any such institution or reproducing in legible form any information in such custody otherwise than in legible form, or any entry in or extract from such a document, may be given in any legal proceedings by production of a copy certified as a true copy by an official of that institution; and any document purporting to be such a copy shall be received in evidence without proof of the official position or handwriting of the person signing the certificate.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 91(4)**

  Evidence of any such instrument may also be given in any legal
  proceedings\
  (a) by production of a copy purporting to be printed by the Queen's
  Printer;\
  (b) where the instrument is in the custody of a government department,
  by production of a copy certified on behalf of the department to be a
  true copy by an officer of the department generally or specially
  authorised to do so;\
  and any document purporting to be such a copy as is mentioned in
  paragraph (b) above of an instrument in the custody of a department
  shall be received in evidence without proof of the official position or
  handwriting of the person signing the certificate, or of his authority
  to do so, or of the document being in the custody of the department.
  -----------------------------------------------------------------------

### 91.03 {#section-1}

s.91(6) is also relevant

Subsections (2), (3) and (4) lay down the forms of documentation which
constitute admissible evidence of instruments, decisions etc. under the
relevant convention or treaty. This includes certified copies of
documents in the custody of an institution established by or having
functions under the relevant convention or treaty, or in the custody of
a government department.

  ----------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 91(5)**
  In any legal proceedings in Scotland evidence of any matter given in a manner authorised by this section shall be sufficient evidence of it.
  ----------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 91(6)**

  In this section-\
  "convention institution" means an institution established by or having
  functions under the relevant convention;\
  \
  "relevant convention court" does not include a court of the United
  Kingdom or of any other country which is a party to the relevant
  convention; and\
  \
  "legal proceedings", in relation to the United Kingdom, includes
  proceedings before the comptroller.
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
