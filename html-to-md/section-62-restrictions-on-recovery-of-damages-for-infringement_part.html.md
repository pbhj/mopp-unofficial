::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 62: Restrictions on recovery of damages for infringement {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (62.01 - 62.13) last updated July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 62.01

This section relates to certain circumstances in which the remedies for
infringement of damages and of an account of profits are not available
or may be refused by the court or comptroller, in proceedings under s.61
(or s.69).

### 62.02

s.69(3) is relevant.

Section 62(2) and (3) do not apply to infringement of the rights
conferred by s.69 in respect of published applications under the 1977
Act. For the applicability of s.62 in relation to European patents, [see
60.02](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-02).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 62(1)**
  In proceedings for infringement of a patent damages shall not be awarded, and no order shall be made for an account of profits, against a defendant or defender who proves that at the date of the infringement he was not aware, and had no reasonable grounds for supposing, that the patent existed; and a person shall not be taken to have been so aware or to have had reasonable grounds for so supposing by reason only of the application to a product of the word "patent" or "patented", or any word or words expressing or implying that a patent has been obtained for the product, unless the number of the patent or a relevant internet link accompanied the word or words in question.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------- -----------------------------------
                                       

  **Section 62(1A)**                   

  The reference in subsection (1) to  \
  a relevant internet link is a       
  reference to an address of a        
  posting on the internet---\         
  \                                   
  (a) which is accessible to the      
  public free of charge, and\         
  \                                   
  (b) which clearly associates the    
  product with the number of the      
  patent.                             
  ----------------------------------- -----------------------------------

### Ignorance of existence of patent

### 62.03 {#ref62-03}

s.29 is relevant.

The court or the comptroller cannot award damages or make an order for
an account of profits against an innocent infringer. However, the onus
is on the infringer to prove their innocence, i.e. that at the date of
the infringing act they were not aware, and had no reasonable grounds
for supposing, that the patent (or published application for a patent)
existed. The test thus concerns only ignorance of the existence of the
patent and not failure to appreciate that an act committed by them might
constitute an infringement. If the infringer had no actual knowledge,
the existence of reasonable grounds must be judged in the light of all
the circumstances at the time of the infringement; the test is objective
(Lancer Boss v Henley Forklift \[1975\] RPC 307). In Texas Iron Works v
Nodeco \[2000\] RPC 207 for example, the applicant was held not to be
deemed to be aware of the patent until it had been sent a warning
letter. The infringing act in question may have been committed either
after grant of the patent or between publication of the application and
grant.

### 62.04 {#ref62-04}

s.110 is relevant.

The fact that a product is marked with wording to the effect that it is
patented is insufficient to establish that other persons should be taken
to be aware of the patent, unless either the patent number is quoted in
the marking, or (from 1st October 2014) a relevant web address is quoted
in the marking. Marking a patented product in either of these ways is
sufficient to establish that other persons should be taken to be aware
of the patent. The web address must be of a webpage that is freely
accessible to the public and clearly associates the patent number with
that product. The product must be clearly identified on the webpage,
e.g. by including any relevant model numbers and variants that exist.
Where a dispute arises, it will be for the courts to decide whether
sufficient notice had been provided. It is an offence to dispose of for
value a product marked with wording claiming that it is patented if the
claim is false.

### Section 62(2)

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 62(2)**
  In proceedings for infringement of a patent the court or the comptroller may, if it or he thinks fit, refuse to award any damages or make any such order in respect of an infringement committed during the further period specified in section 25(4) above, but before the payment of the renewal fee and any additional fee prescribed for the purposes of that subsection.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Late renewal of patent

### 62.05 {#ref62-05}

Under s.25(4), if a patent lapses due to non-payment of a renewal fee by
the renewal date but that fee together with an additional fee is paid by
the end of the sixth month after that date, the patent is treated as if
it had never lapsed ([see 25.12 to
25.14](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-12)).
However, the court or comptroller has discretion to refuse to award
damages or to order an account of profits in respect of an infringing
act committed in the intervening period.

### Section 62(3)

  -----------------------------------------------------------------------
   

  **Section 63(3)**

  Where an amendment of the specification of a patent has been allowed
  under any of the provisions of this Act, the court or the comptroller
  shall, when awarding damages or making an order for an account of
  profits in proceedings for an infringement of the patent committed
  before the decision to allow the amendment, take into account the
  following -\
  \
  (a) whether at the date of infringement the defendant or defender knew,
  or had reasonable grounds to know, that he was infringing the patent;\
  \
  (b) whether the specification of the patent as published was framed in
  good faith and with reasonable skill and knowledge;\
  \
  (c) whether the proceedings are brought in good faith.
  -----------------------------------------------------------------------

### Pre-amendment infringement

### 62.06 {#ref62-06}

Section 62(3) applies where the infringing act is committed before a
decision to allow amendment of the specification of the patent in
question. When awarding damages or making an order for an account of
profits in such circumstances, the court or comptroller is required to
take into account the various factors set out in paragraphs (a) to (c)
of that provision. These factors were modified and expanded upon by the
Intellectual Property (Enforcement, etc.) Regulations 2006 (S.I. 2006
No. 1028), which came into force on 29 April 2006, in order to implement
Directive 2004/48/EC on the enforcement of intellectual property rights.
([See also paragraphs 63.04 to
63.06](/guidance/manual-of-patent-practice-mopp/section-63-relief-for-infringement-of-partially-valid-patent/#ref63-04)).

### 62.07 {#section-2}

Paragraph (a) requires the court or comptroller to take account of the
defendant's actual or constructive knowledge that they were infringing
the patent. This reflects the requirement in article 13.1 of the
Directive that, in such circumstances, the proprietor is entitled to
damages appropriate to the actual prejudice suffered. For this reason,
the absolute bar on damages that was imposed in section 62(3)
previously, in the circumstances where the specification was not framed
in good faith and with reasonable skill and knowledge, was not
compatible with the Directive. Thus no absolute bar on damages is
maintained, but paragraph (b) requires account to be taken of the
framing of the specification when determining what level of award is
appropriate.

### 62.08 {#ref62-08}

Paragraph (c) requires the court or comptroller to take into account
whether the infringement proceedings are brought in good faith. Where it
is shown that the proprietor knew that the infringed claim, before
amendment, was invalid, the court or comptroller can take that matter
into account when determining the appropriate level of damages.

### Good faith; reasonable skill and knowledge

### 62.09 {#ref62-09}

Aldous J considered in Hallen Co v Brabantia (UK) Ltd \[1990\] FSR 134
that good faith meant that the specification was framed honestly with a
view to obtaining a monopoly to which, on the material known to the
drafter, he believed the applicant was entitled. The words "skill and
knowledge" were observed to be a composite phrase relating to the
competence employed in framing the specification, which should be in the
form which a person with reasonable skill in drafting patent
specifications and a knowledge of the law and practice relating thereto
would produce.

### 62.10 {#ref62-10}

In Ronson Products Ltd v A Lewis & Co (Westminster) Ltd \[1963\] RPC 103
at page 138, it was observed that where the drafting of the
specification departs in a material respect from the intention of the
applicant, and this despite the transmission by the applicant to their
patent agent of all relevant information, an acknowledgement by such
agent that the way they expressed themselves in the passage in question
was wrong in view of the information they had received must establish an
absence of reasonable skill and knowledge. On the other hand in
Molnlycke AB v Procter & Gamble Ltd \[1994\] RPC 49 Morritt J observed
(at p.106) that even if the reasonable knowledge required includes
details of the invention, it is irrelevant that the drafter was unaware
of the best or optimum method for its performance so long as the
description is sufficient.

### 62.11 {#section-3}

In Page v Brent Toy Products Ltd (1950) 67 RPC 4 at page 21, the agent
who drafted the specification in suit said that they had initially put
in a "fairly broad claim" to "draw the Patent Office search report" and
had modified the claim in accordance with what the report disclosed; and
that this was the practice usually adopted by agents. The claim in
question was held (although invalid) to have been framed in good faith
and with reasonable skill and knowledge.

### 62.12 {#ref62-12}

The patent in Nutrinova Nutrition v Scanchem UK (No.2) \[2001\] FSR 43
concerned a process for making a particular compound, but also included
claims to the production of an intermediate compound. It was held that
it was not bad faith to file the specification knowing of an arguable
case of obviousness in respect of the claims to the intermediate
compound. Although these claims were subsequently found to be obvious,
the patent agents would have been failing in their duty had they not
tried to cover what was a novel intermediate compound.

### 62.13 {#ref62-13}

The Court of Appeal held, in Kirin-Amgen Inc. v Transkaryotic Therapies
Inc. \[2003\] RPC 3, that any mistake in the specification on the part
of the drafter must be considered in the context of the whole
specification, making due allowance for any difficulty that the drafter
had and for the importance of the passage said to be mistaken. If a
specific passage has no importance, then, as held by the Court of Appeal
in Unilin Beheer BV v Berry Floor NV (No. 2) \[2006\] FSR 26, even if it
is negligently wrong, the specification is still drafted with reasonable
skill and knowledge. Certain passages of the description not amended in
conformity with amended claims could not in this case mislead the
skilled reader and did not make the claims unclear; and if a
specification retains such irrelevant and harmless material, however
much there may be, it is still drafted with the relevant skill and
knowledge for complying with the law and providing accurate technical
information. That would not be the case where the unamended passages of
the description made it difficult to decide on the meaning of the claim
and had been left in through negligence.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
