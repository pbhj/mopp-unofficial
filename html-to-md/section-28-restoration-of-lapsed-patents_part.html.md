::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 28: Restoration of lapsed patents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (28.01 - 28.18) last updated April 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 28.01

This section specifies the circumstances under which a patent which has
lapsed through failure to pay renewal fees may be restored. It governs
patents granted under the 1977 Act and European patents (UK) which have
lapsed due to failure to pay renewal fees due to the Office [see
25.08--25.08.1](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-08)
and
[25.13-25.13.1](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-13).
Relevant procedures are prescribed in r.40. The Regulatory Reform
(Patents) Order 2004, which entered into force on 1 January 2005 amended
this section by replacing the previous condition for restoration, namely
"reasonable care" with "unintentional". The latter standard applies to
patents that ceased (as defined by section 25(3)) on or after 1 January
2005. For patents that ceased before 1 January 2005, the comptroller
will continue to apply the standard of "reasonable care".

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 28(1)**
  Where a patent has ceased to have effect by reason of a failure to pay any renewal fee, an application for the restoration of the patent may be made to the comptroller within the prescribed period.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 28(1A)**
  Rules prescribing that period may contain such transitional provisions and savings as appear to the Secretary of State to be necessary or expedient.
  ------------------------------------------------------------------------------------------------------------------------------------------------------

### 28.02 {#ref28-02}

r.40(2), (4),(5), r.81, r.108(1) is also relevant.

An application for restoration should be made on Form 16, on which
should be stated the reasons for the application. These must be
supported by evidence, and if that evidence does not accompany the
application then the comptroller will specify a period within which it
must be filed.

### 28.03 {#ref28-03}

A mere attempt to pay the renewal fees does not constitute an
application for restoration (Dynamics Research and Manufacturing Inc's
Patent, \[1980\] RPC 179); Electricité de France (EDF)'s Patents,
\[1992\] RPC 205).

### 28.04 {#ref28-04}

r.41 is also relevant.

Once the extended period for paying renewal fees has expired the Office
is obliged to draw the attention of the proprietor to the provisions of
s.28 [see
25.15](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-15).
In Daido Kogyo KK's Patent, \[1984\] RPC 97 discretion was exercised
under r.100 of the Patents Rules 1995 to allow an application for
restoration to be filed out of time since this reminder had not been
issued at the proper time. In view of this, the Court of Appeal
recommended that all applications for restoration, even though
apparently too late to have effect, should be entered in the Register
([see 28.05.1](#ref28-05)).

### 28.04.1 {#section-1}

Sch 4, Part 1 is also relevant.

The prescribed period under s.28(1) is thirteen months after the end of
the period specified in section 25(4) and cannot be altered. For
example, if the anniversary of the filing date of the patent falls on 10
May, the renewal fee will be due by 31 May each year. If the renewal fee
for 2006 is not paid by 30 November 2006 (i.e. the end of the six month
period specified in section 25(4)), the patent will be treated as ceased
on 10 May 2006. The thirteen month period allowed to make an application
for restoration will begin on 1 December 2006 and expire on 31 December
2007.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 28(2)**
  In application under this section may be made by the person who was the proprietor of the patent or by any other person who would have been entitled to the patent if it had not ceased to have effect; and where the patent was held by two or more persons jointly, the application may, with the leave of the comptroller, be made by one or more of them without joining the others.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 28.05 {#ref28-05}

By virtue of the reference to "any other person who would have been
entitled", an application for restoration may be made by a person who
has acquired the patent after it has lapsed, for example by assignment
(Border's Patent BL O/157/79). If the assignment has not been entered in
the Register, proof of ownership must be provided as part of the
evidence to which [28.02 refers](#ref28-02). A potential proprietor, to
whom the patent would later be assigned if it were to be successfully
restored, is not "any other person who would have been entitled"
(Vause's European Patent [BL
O/278/00](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F278%2F00&submit=Go+%BB){rel="external"}).

  -------------------------------------------------------------------------------------------
   
  **Section 28(2A)**
  Notice of the application shall be published by the comptroller in the prescribed manner.
  -------------------------------------------------------------------------------------------

### 28.05.1 {#section-2}

r.40(1), (2) is also relevant.

An application for restoration is advertised in the Journal and noted in
the register. The advertisement constitutes the notice required by
subsection (2A) and concludes the period referred to in s.28A(4), [see
28A.03](/guidance/manual-of-patent-practice-mopp/section-28a-effect-of-order-for-restoration-of-patent/#ref28A-03)).

  -----------------------------------------------------------------------
   

  **Section 28(3)**

  If the comptroller is satisfied that the failure of the proprietor of
  the patent-\
  (a) to pay the renewal fee within the prescribed period; or\
  (b) to pay that fee and any prescribed additional fee within the period
  ending with the sixth month after the month in which the prescribed
  period ended, was unintentional, the comptroller shall by order restore
  the patent on payment of any unpaid renewal fee and any prescribed
  additional fee.
  -----------------------------------------------------------------------

### 28.06 {#section-3}

r.40(6)-(8) is also relevant.

If, on consideration of the statements in the application for
restoration and of the supporting evidence, the Office comes to the
conclusion that a case has not been made out, then the applicant is sent
a "minded to refuse" letter containing reasons for the conclusions
drawn. The applicant is given one month to respond to this letter and to
provide further evidence if applicable. If no further evidence is
supplied, or if the evidence does not convince the Office to change its
opinion, a further letter is issued advising the applicant that, unless
within a further one month they request to be heard, the application for
restoration will be formally refused. If no request for a hearing is
received within this time, a formal decision is issued refusing to order
restoration of the patent unless the Office has been formally notified
in writing that the application has been withdrawn, in which case only
an acknowledgement of receipt of the notification is issued. If the
applicant asks for a hearing within the time allowed, then they must be
given an opportunity to be heard, following which either a decision
refusing the application is given or a conditional offer of restoration
is made. An order restoring the patent is issued when the conditions,
which include the payment of fees due (see 28.07), have been met. There
is no provision for an application for restoration to be opposed.

### 28.07 {#section-4}

r.36(4) r.109 is also relevant.

If it is decided, with or without a hearing, that the patent may be
restored, the applicant must first file Form 12, together with the
unpaid renewal fees. The comptroller will specify a period within which
the form and fees must be filed (usually two months of notification of
the decision being sent to the applicant). This specified period may be
extended under section 117B and rule 109, [see
117B.01-0.5](/guidance/manual-of-patent-practice-mopp/section-117b-extension-of-time-limits-specified-by-comptroller/#ref117B-01).
A formal order is then issued restoring the patent.

### 28.08 {#section-5}

r.40(9) is also relevant.

The final decision allowing or refusing restoration is advertised in the
Journal and noted in the Register. Similar action is taken when
applications are formally withdrawn. If there is some doubt as to
whether a notification is intended to be a withdrawal, the Office
informs the applicant that it intends treating the notification as a
withdrawal and, unless within one month the Office hears to the
contrary, the Register will be noted accordingly.

### Meaning of unintentional

### 28.09 {#ref28-09}

As with the more stringent standard of "reasonable care" [see 28.10 to
28.16](/guidance/manual-of-patent-practice-mopp/section-28-restoration-of-lapsed-patents/#ref28-10),
there is no definition in the Act or rules as to what is meant by
"unintentional" as it applies for determining whether to allow a request
for restoration. In Sirna Therapeutics Inc's Application \[2006\] RPC
12, which related to a request to make a late declaration of priority
under section 5(2B), the hearing officer observed that the requirement
to show an intention to file an application in time differed from the
test of "continual underlying intention to proceed" that was applied in
Heatex Group Ltd's Application (\[1995\] RPC 546) in deciding whether to
exercise discretion favourably to allow a period of time to be extended
under rule 108 [see
123.37](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-37).
However, case law under rule 108 may be of relevance in analysing the
evidence to establish the applicant's intentions. In Anning's
Application ([BL
O/374/06](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O374%2F06&submit=Go+%BB){rel="external"}),
which related to a request for reinstatement under section 20A, the
hearing officer took a similar approach and warned against the danger of
going beyond the clear meaning of the statute. He interpreted
"unintentional" according to its normal English meaning. In this case
the hearing officer held that although there was a continual underlying
intention to proceed it did not follow that the failure to reply to an
examination report was unintentional.

### 28.9.1 {#ref28-09-1}

Sirna Therapeutics Inc's Application \[2006\] RPC 12 and Anning's
Application ([BL
O/374/06](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O374%2F06&submit=Go+%BB){rel="external"})
established that the "continual underlying intention" test in Heatex is
not applicable in determining the meaning of the word "unintentional"
(in section 5(2B) or section 20A and it follows, in section 28(3)
either).

### 28.9.2 {#ref28-09-2}

In Matsushita Electric Industrial Co. v Comptroller General of Patents
\[2008\] EWHC 2071 (Pat), \[2008\] RPC 35, Mr. Justice Mann gave some
guidance on the level of evidential burden required to "satisfy" the
Comptroller that the failure in section 28(3) was "unintentional". The
applicant in that case chose not to file any evidence beyond a bald
assertion of the statute that the failure to pay the renewal fee on time
was unintentional. It argued that that was all the statute required to
satisfy the comptroller. It was held by the Judge that a mere assertion
that the failure to pay the renewal fee was unintentional is not
sufficient to enable the Comptroller to determine that the requirements
of s.28(3) are fulfilled. He said :

> the Act requires a judgment to be formed by the Comptroller so that he
> can be satisfied of the relevant matters. A judgment usually has to be
> made on the basis of evidence... The evidence required in any
> particular case where satisfaction is required depends on the nature
> of the enquiry and the nature and purpose of the decision to be
> made... A significant matter requires significant proof. I repeat, the
> Act does not require a statement that the failure to pay fees was
> unintentional. It requires the Comptroller to be satisfied of that
> fact.

### 28.9.3 {#section-6}

It is clear from this judgment that while there is no universal rule as
to what level of evidence has to be provided to satisfy the comptroller
of the unintentional lapse in section 28(3) (and by implication in
sections 5(2B) and 20A), some evidence above and beyond a bald assertion
of the law is required.

### Meaning of reasonable care

### 28.10 {#ref28-10}

For patents that lapsed before 1 January 2005, the standard of
"reasonable care" applies when determining whether a patent should be
restored, i.e. the comptroller will need to be satisfied that the
proprietor took reasonable care to see that the renewal fee was paid
within the prescribed period or that the renewal fee and any prescribed
additional fee were paid within the six months immediately following the
end of that period. In deciding whether or not a proprietor took
reasonable care it is appropriate to bear in mind the direction given by
the judge in Continental Manufacturing & Sales Inc's Patent \[1994\] RPC
535:

> The words 'reasonable care' do not need explanation. The standard is
> that required of the particular patentee acting reasonably in ensuring
> that the fee is paid.

### 28.11 {#ref28-11}

The proprietor is reminded at the time of grant of the need for setting
up effective renewal arrangements [see
24.03](/guidance/manual-of-patent-practice-mopp/section-24-publication-and-certificate-of-grant/#ref24-03),
and the reminder issued under r.39 [see
25.12](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-12)
is not intended to be a substitute for such a system but to alert the
proprietor to a breakdown in their own system. However in Ling's Patent
and Wilson's and Pearce's Patent, \[1981\] RPC 85, it was held that it
was reasonable for an individual in a small way of business who has
taken upon themselves to pay renewal fees without professional
assistance to rely on these reminders. Similarly, in Frazer's Patent,
\[1981\] RPC 53, it was held that a "reasonable lone patentee" who had
put their patent affairs in the hands of their solicitor had exercised
due care, and the failure of the solicitor to pay renewal fees was a
circumstance beyond the proprietor's control; for a person in this
situation the normal rule that a principal stands in the shoes of their
agent did not apply. However, a company of some size owning several
patents might be expected to have on its staff a person responsible for
dealing with patent matters, and the failure to do so indicates a lack
of reasonable care (Societe Minerve SA's Patent, BL O/55/82). In
Marbourn's Patent ([BL
O/376/99](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F376%2F99&submit=Go+%BB){rel="external"})
restoration was allowed where a previously effective system within a
company broke down following restructuring of the company as a result of
decisions which could not reasonably have been foreseen by the director
responsible for ensuring that the patent was renewed.

### 28.12 {#ref28-12}

While the placing of responsibility for renewal fees in the hands of a
professional adviser may be considered to amount to reasonable care
(Frazer's Patent - [see 28.11](#ref28-11), the Patents Court agreed that
a proprietor who had entrusted the payment of renewal fees to a person
holding a licence under the patent had not shown sufficient care to
justify restoration (Lichtenstein's Patent, [BL
O/152/83](https://www.gov.uk/government/publications/patent-decision-015283),
BL C/34/84) (although this was decided on the facts of the case and may
not always be so); the same is true where the task is left to staff
employed by the proprietor who are inadequately instructed or supervised
(Zarach's Patent, not reported; Tekdata Ltd's Patent, \[1985\] RPC 201
and Luthy's Patent, not reported) or to one of the co-proprietors (Ho
and Wang's Patent, [BL
O/152/87](https://www.gov.uk/government/publications/patent-decision-o15287)).
In Textron \[1989\] RPC 441, the House of Lords held that the patentee
must, in each case, take reasonable care in the selection of an agent or
servant and in the instructions and arrangements for payment. When
placing the responsibility for payment of renewal fees in the hands of
others, the proprietor is expected to have checked that an effective
renewal system is in place and that a dependable address is provided to
which reminders can be forwarded (University of Chicago's Patent, [BL
O/44/91](https://www.gov.uk/government/publications/patent-decision-o04491)).
Nevertheless, in Pritchard's Patent ([BL
O/104/96](https://www.gov.uk/government/publications/patent-decision-o10496))
a proprietor who paid insufficient attention to reminders from his agent
and failed to pay a renewal fee in time was held not to have exercised
reasonable care. In Gram's Patent ([BL
O/412/99](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F412%2F99&submit=Go+%BB){rel="external"})
a proprietor acting for himself in renewals matters following a dispute
with his representatives was held to have taken reasonable care, despite
his mistaken belief that he could pay the renewal fee up to six months
after the end of the month in which the anniversary of filing fell,
because of a unique set of circumstances which led him towards that
belief and the fact that the reminder notice which would have disabused
him of this notion was not forwarded to him by his representatives. The
allocation to a trusted and hitherto reliable employee of the task of
checking the payment of renewal fees was held to be a reasonable system
for ensuring their payment. Following Textron, the Patents Court ordered
restoration of Sony Corporation's Patent (\[1990\] RPC 152), holding
that Sony exercised reasonable care by operating a system whereby
competent agents were responsible for paying renewal fees and in
extraordinary cases the matter came before a competent employee part of
whose duty it was to take the appropriate steps. That employee made a
mistake which was contrary to his duties and therefore outside the
control of Sony; they could not be reasonably expected to ensure that it
would not happen. In the case of a corporate proprietor regard should be
given to the words of Lord Oliver in Textron at page 453, lines 31-44,
when deciding who should be regarded as the "proprietor by his directing
mind" for the purposes of s.28(3). However, a failure by a senior
employee responsible for patent matters (and serving for that purpose as
the "directing mind" of the proprietor) was held, following Textron, to
constitute lack of reasonable care (British Broadcasting Corporation's
Patent, [BL
O/49/89](https://www.gov.uk/government/publications/patent-decision-o04989)).

### 28.13 {#ref28-13}

In Ling's Patent and Wilson's and Pearce's Patent [see 28.11](#ref28-11)
it was observed that if a proprietor were to fail to notify the Office
of a change of address, then failure to receive the statutory notice
would undoubtedly be the proprietor's fault. Likewise a proprietor who
takes their patent affairs out of the hands of the agent who has
prosecuted the application but fails to provide the Office with an
address for service or an address for renewal reminders [see
25.12](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-12)
will be regarded as not having exercised reasonable care (Convex Ltd's
Patent, \[1980\] RPC 423; Francis' Patent, \[BL O/154/82\]). The same is
true of a person who does not inform an agent acting for them of a
change of address (Nakumura's Patent, [BL
O/121/84](https://www.gov.uk/government/publications/patent-decision-012184);
Samuels' Patent, not reported) or of a temporary absence (Whiteside's
Patent, [BL
O/44/84](https://www.gov.uk/government/publications/patent-decision-004484);
Singleton's Patent, not reported).

### 28.14 {#ref28-14}

Restoration may be allowed where a proprietor who has set up a
reasonable system for paying renewal fees is prevented by ill health
from playing their part in the system (Mead's Patent, \[1980\] RPC 146)
although being stressed and run-down has been found inadequate to
justify restoration (Linkrose Ltd's Patent, [BL
O/117/90](https://www.gov.uk/government/publications/patent-decision-o11790)).
The Office would expect claims that ill health was the reason for not
paying a renewal fee to be supported by medical evidence. However, an
application will not be successful even if it is demonstrated that
failure to pay in time was due to difficult personal circumstances if
the proprietor has not shown reasonable care in the first place by
setting up a system (Warwick's Patent, BL O/150/82). Similarly, in
Marcel J Paulus's Patent ([BL
O/73/95](https://www.gov.uk/government/publications/patent-decision-o07395))
(upheld on appeal) restoration was refused because the renewal system
put in place by the proprietor collapsed completely when he died and no
alternative arrangements for maintaining the patent were introduced even
though at least one member of the family knew of the existence of the
patent.

### 28.14.1 {#section-7}

The fact that a proprietor has set up a system which is reasonable for
the renewal of patents in general will not necessarily justify
restoration. In The Cement and Concrete Association's Patent, \[1984\]
RPC 131 a system which had worked adequately for other patents failed to
alert the person responsible for deciding whether to pay renewal fees to
the fact that the proprietor was under an obligation to a licensee to
keep the patent in force; the Patents Court held that, whatever the
merits of the system in general, there had not been reasonable care in
the case of the particular patent in suit. The failure to pay renewal
fees was the result of a conscious decision, albeit one made in
ignorance of important facts. Similarly, employing a computer system
which did not cater for a date of grant close to the renewal date of
Halcon's Patent (BL O/94/85) did not amount to reasonable care.

### 28.15 {#ref28-15}

In Atlas Powder Co's Patent \[1995\] RPC 357 (upheld on Appeal) Aldous J
concluded that s.28 is not there to alleviate proprietors from decisions
not to pay the fees, even though such proprietors may have taken
reasonable care to come to a correct decision. Thus a proprietor who
decides not to pay a fee cannot have their patent restored. They will
not have taken any care to see that the fee was paid even though they
may have taken reasonable care to decide whether to pay the fee. It
makes no difference if the decision not to pay a renewal fee was based
on facts subsequently shown to be incomplete or inaccurate (also
Walters' Patent, [BL
O/105/91](https://www.gov.uk/government/publications/patent-decision-o10591)).
This is in line with the intention of the 1977 Act to impose on the
proprietor the need to take a greater degree of care to see that the fee
was paid than was required under the 1949 Act where all the proprietor
had to show was that failure to pay the fee was unintentional. In Lermer
Gmbh's Patent ([BL
O/14/96](https://www.gov.uk/government/publications/patent-decision-o01496))
it was alleged that the person who had ordered that the patent should
not be renewed had exceeded his authority. Restoration was refused
because the applicant for restoration failed to establish whose
responsibility it had been to decide whether to renew and so had not
shown that there had been an effective system in place to ensure payment
of the renewal fee.

### 28.15.1 {#ref28-15-1}

In Ament's Application \[1994\] RPC 647 the Patents Court rejected a
long- standing practice of the Office in regarding the inability of a
proprietor to pay a renewal fee because of lack of funds and the
attendant absence of any attempt to pay the fee as amounting to lack of
reasonable care to see that the fee was paid. It was held that comments
made by the Appeal Board in Radakovic (J/22/88 OJEPO 5/90 applied with
equal force to section 28(3) as they did to Article 122 EPC \[1973\].
These comments were to the effect that for "all due care" to be proven
it must be clear that the financial difficulties were genuine and were
due to circumstances beyond the reasonable control of the applicant, and
it was also necessary for the applicant to have shown diligence in
seeking financial assistance. The onus is on the applicants for
restoration to establish that they wanted to pay the fee and had
exercised reasonable care to ensure that they were in a position to pay
(in Ament's Application sufficient evidence was not provided). That may
require seeking financial assistance and in appropriate cases taking
reasonable care to avoid impecuniousity. The comptroller has no
discretion to waive fees or to provide periods for their payment other
than those provided in the Rules (Halpern & Ward's Patent, BL C/14/93;
EPS Research's Patent, [BL
O/53/92](https://www.gov.uk/government/publications/patent-decision-o05392)).

### 28.16 {#ref28-16}

The responsibility for taking reasonable care to see that renewal fees
are paid falls to the actual proprietor (at the time the fees could have
been paid), and not to the registered proprietor if different. Thus if
the patent is assigned, the obligation passes to the new proprietor,
irrespective of whether or not the assignment is registered (Whiteside's
Patent, [BL
O/44/84](https://www.gov.uk/government/publications/patent-decision-004484)).
It is the responsibility of a person acquiring a patent to take steps to
discover the true position regarding renewals (Advocat Giovanni Gozzo
AB's Patent ([BL
O/150/95](https://www.gov.uk/government/publications/patent-decision-o15095)),
Uniworld Trade and Finance Establishment's Patent, not reported). In
Latchworth Ltd's Patent ([BL
O/112/96](https://www.gov.uk/government/publications/patent-decision-o11296))
where the applicant company had been struck off the Companies Register
and dissolved, causing ownership of the patent to pass to the Crown a
few days before it was granted and to remain so for the entire period
that the renewal fee could have been paid, restoration was refused
because there was no evidence to demonstrate that the Crown took
reasonable care to see that the renewal fee was paid and the indications
were that the Crown would not take any such care in the circumstances.
Failure to make adequate arrangements at change of patent proprietorship
on takeover or transfer of assets was also found to involve lack of
reasonable care in the unreported decisions on Reiss Engineering's
Patents ([BL
O/180/86](https://www.gov.uk/government/publications/patent-decision-o18086)),
Dytap Revetments' Patent ([BL
O/76/87](https://www.gov.uk/government/publications/patent-decision-o07687)),
Triten Corp's Patent ([BL
O/65/88](https://www.gov.uk/government/publications/patent-decision-o06588)),
BWS Management's Patent (BL O/113/90) and Fisher Westmoreland's Patent
([BL
O/151/90](https://www.gov.uk/government/publications/patent-decision-o15190)).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 28(4)**
  An order under this section may be made subject to such conditions as the comptroller thinks fit (including a condition requiring compliance with any provisions of the rules relating to registration which have not been complied with), and if the proprietor of the patent does not comply with any condition of such an order the comptroller may revoke the order and give such directions consequential on the revocation as he thinks fit.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 28.17 {#ref28-17}

If it is considered that, in the circumstances of a particular case, the
protection given to third parties by s.28A(4) to (6) ([see
28A.03](/guidance/manual-of-patent-practice-mopp/section-28a-effect-of-order-for-restoration-of-patent/#ref28A-03))
is not adequate, an order for restoration may vary the provisions of
those subsections. In Daido Kogyo KK's Patent \[1984\] RPC 97, where the
Court allowed an application for restoration which had not been entered
on the Register since it had apparently been lodged out of time ([see
28.04](#ref28-04)), the Court of Appeal upheld an order making it a
condition of restoration that the protection given to anyone who takes
steps to begin to work the invention should be extended until the time
when the order was actually made.

### 28.18 {#section-8}

Subsections (5) to (9) concerning the effect of restoration were
repealed, and replaced by section 28A, by the CDP Act.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
