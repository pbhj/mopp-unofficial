::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 294: When application may be made for settlement of terms of licence \[spent\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (294.01) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 294.01

Section 294 inserted paragraph 4B into Schedule 1 to the Patents Act
1977. It gives statutory force to the existing practice of the
comptroller not to deal with applications for the settlement of terms
for licences of right available under the transitional provisions of the
Patents Act 1977 more than one year before the relevant patent becomes
subject to the licence of right provisions. This section can now be
considered to be spent.

### 294.02-04

\[deleted\]

  -----------------------------------------------------------------------
   

  **Section 294**

  In Schedule 1 to the Patents Act 1977, after the paragraph inserted by
  section 293 above, insert "\
  \
  4B (1) An application under section 46(3)(a) or (b) above for the
  settlement by the comptroller of the terms on which a person is
  entitled to a licence by virtue of paragraph 4(2)(c) above is
  ineffective if made before the beginning of the sixteenth year of the
  patent.\
  \
  4B (2) This paragraph applies to applications made after the
  commencement of section 294 of the Copyright, Designs and Patents Act
  1988 and to any application made before the commencement of that
  section in respect of a patent which has not at the commencement of
  that section passed the end of its fifteenth year.".
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
