::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 130: Interpretation {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (130.01 - 130.35.1) last updated: October 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 130.01

This section defines certain terms used in the Act and, in subsections
(7) and (9), is concerned with the interpretation and construction of
certain provisions and references. It should be noted that other terms
are defined in other sections of the Act, for example "relevant
application" is defined in s.5(5) [see
5.30](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-30),
although some of those definitions apply only in specified section(s).

### Section 130(1)

  -----------------------------------------------------------------------
   

  **Section 130(1)**

  In this Act, except so far as the context otherwise requires "\
  \
  application fee" means the fee prescribed for the purposes of section
  14(1A) above;\
  \
  "application for a European patent (UK)" and (subject to subsection
  (4A) below) "international application for a patent (UK)" each mean an
  application of the relevant description which, on its date of filing,
  designates the United Kingdom;
  -----------------------------------------------------------------------

### 130.02

The definitions in subsection (1) thus apply throughout the Act unless
the context otherwise requires.

### 130.03

The terms "date of filing", "designate", "European patent (UK)" and
"international application for a patent" (used in the above definitions)
are defined later in subsection (1), see below.

### Section 130(1)

  -------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 130(1)**
  "appointed day", in any provision of this Act, means the day appointed under section 132 below for the coming into operation of that provision;
  -------------------------------------------------------------------------------------------------------------------------------------------------

### 130.04

[See 132.06 to
132.08](/guidance/manual-of-patent-practice-mopp/section-132-short-title-extent-commencement-consequential-amendments-and-repeals/#ref132-06).

### Section 130(1)

  -----------------------------------------------------------------------
   

  **Section 130(1)**

  "biological material" means any material containing genetic information
  and capable of reproducing itself or being reproduced in a biological
  system;\
  \
  "biotechnological invention" means an invention which concerns a
  product consisting of or containing biological material or a process by
  means of which biological material is

  produced, processed or used;
  -----------------------------------------------------------------------

### 130.04.1 {#ref130-04-1}

These definitions were introduced into s.130(1) by the Patents
Regulations 2000 (SI 2000 No.2037). Schedule A2 to the Act (also
introduced by the Patents Regulations 2000 under s.76A) makes certain
specific provisions concerning "biotechnological inventions", as defined
above. References to "biological material" in that Schedule and
elsewhere in the Act are also to be interpreted in the light of the
above definition.

#### Section 130(1)

  ---------------------------------------------------------------------------------------------------
   
  **Section 130(1)**
  "Community Patent Convention" means the Convention for the European Patent for the Common Market;
  ---------------------------------------------------------------------------------------------------

### Section 130(1)

  ----------------------------------------------------------------------------------
   
  **Section 130(1)**
  "comptroller" means the Comptroller-General of Patents, Designs and Trade Marks;
  ----------------------------------------------------------------------------------

### 130.05 {#section-4}

It is a requirement of the Patents and Designs Act 1907 that there is a
"Comptroller General of Patents, Designs, and Trade Marks"; the
definition in s.130(1) is therefore intended to allow for brevity in the
Patents Act 1977 whilst ensuring consistency with the 1907 Act. The
comptroller is appointed by the Secretary of State under s.63(1) of the
1907 Act.

### 130.05.1 {#ref130-05-01}

Any act or thing directed to be done by or to the comptroller may be
done by or to any officer authorised by the Secretary of State (see
s.62(3) of the 1907 Act) or any officer authorised by the comptroller
themselves (see s.74 of the Deregulation and Contracting Out Act 1994).
The current authorisation, dated 22 November 2023, authorises various
officers of the Office to perform functions of the comptroller in
accordance with the following schedule. Like previous authorisations, it
is drawn in broader terms than will normally be applied in practice.

### 130.05.2 {#ref130-05-02}

The comptroller's tribunal function in respect of patent disputes under
the Patents Act 1977 was considered to be a court for the purposes of
Article 24 of the Council Regulation (EC) 44/2001 (now Article 26 of
Regulation (EU) No. 1215/2012) in [Future New Developments Ltd v B & S
Patente Und Marken GmbH \[2014\] EWHC 1874
(IPEC)](http://www.bailii.org/ew/cases/EWHC/IPEC/2014/1874.html){rel="external"}.

### Schedule

  -----------------------------------------------------------------------
  Column 1                            Column 2
  ----------------------------------- -----------------------------------
  Deputy Chief Executive and Director 1\. All the provisions of:\
  of Services, Chief Hearing Officer, \
  Divisional Directors, Deputy        (i) the Patents Acts 1949 and
  Directors (Tribunals, Patents,      1977,\
  Trade Marks, Designs, Legal and     \
  Practice)                           (ii) the Registered Designs Act
                                      1949,\
                                      \
                                      (iii) the Copyright, Designs and
                                      Patents Act 1988, and\
                                      \
                                      (iv) the Trade Marks Acts 1938 and
                                      1994.\
                                      \
                                      2. The relevant Rules, Orders and
                                      Regulations made under the above
                                      Acts.\
                                      \
                                      3. All the provisions of the
                                      Patents and Plant Variety Rights
                                      (Compulsory Licensing) Regulations
                                      2002.\
                                      \
                                      4. All provisions of Regulation
                                      (EC) No 1610/96 and Regulation (EC)
                                      No 469/2009 concerning
                                      supplementary protection
                                      certificates for plant protection
                                      and medicinal products
                                      respectively.

  Officers of span D1 (patents),      1\. All the provisions of the
  senior patent examiners             Patents Acts 1949 and 1977 except
                                      insofar as they involve:\
                                      \
                                      (i) opposed requests, references
                                      and applications, or\
                                      \
                                      (ii) the refusal of any application
                                      or request where the refusal is
                                      disputed by the applicant or
                                      requester and where the technical
                                      content of specifications is
                                      involved.\
                                      \
                                      2. The relevant Rules, Orders and
                                      Regulations made under the Patents
                                      Act 1977.\
                                      \
                                      3. All the provisions of the
                                      Registered Designs Act 1949.\
                                      \
                                      4. Granting and rectification of
                                      certificates under the provisions
                                      of Article 10 of the Regulation
                                      (EC) No 1610/96 and Article 10 of
                                      Regulation (EC) No 469/2009.

  Patent examiners                    1\. All the provisions of the
                                      Patents Act 1977 except insofar as
                                      they involve:\
                                      \
                                      (i) opposed requests, references
                                      and applications, or\
                                      \
                                      (ii) the refusal of any application
                                      or request where the refusal is
                                      disputed by the applicant or
                                      requester.\
                                      \
                                      2. The relevant Rules, Orders and
                                      Regulations made under the Patents
                                      Act 1977\
                                      \
                                      3. All the provisions of the
                                      Registered Designs Act 1949\
                                      \
                                      4. Granting and rectification of
                                      certificates under Article 10 of
                                      Regulation (EC) No 1610/96 and
                                      Article 10 of Regulation (EC) No
                                      469/2009.

  Associate patent examiners and      1\. All the provisions of the
  private applicant unit examiners    Patents Act 1977 except insofar as
                                      they involve:\
                                      \
                                      (i) opposed requests, references
                                      and applications, or\
                                      \
                                      (ii) the refusal of any application
                                      or request where the refusal is
                                      disputed by the applicant or
                                      requester.\
                                      \
                                      2. The relevant Rules, Orders and
                                      Regulations made under the above
                                      Act.

  Director (finance), where not       All the provisions of:\
  already mentioned officers of span  \
  C2 or higher                        (i) the Patents Acts 1949 and 1977,
                                      including section 123(2A), and
                                      opposed requests, references and
                                      applications insofar as these
                                      relate to preliminary or procedural
                                      matters, except insofar as these
                                      involve the technical content of
                                      specifications,\
                                      \
                                      (ii)the Registered Designs Act
                                      1949, and\
                                      \
                                      (iii) the Trade Marks Acts 1938 and
                                      1994.\
                                      \
                                      2.The relevant Rules, Orders and
                                      Regulations made under the above
                                      Acts.

  Where not already mentioned         1\. All the provisions of:\
  Officers of span B3 or higher,      \
  Assistant heads of administration   (i) the Patents Acts 1949 and 1977
  (patents)                           except insofar as opposed requests,
                                      references and applications made
                                      thereunder or the technical content
                                      of specifications are involved,\
                                      \
                                      (ii) the Registered Designs Act
                                      1949, and\
                                      \
                                      (iii) the Trade Marks Acts 1938 and
                                      1994.\
                                      \
                                      2. The relevant Rules, Orders and
                                      Regulations made under the above
                                      Acts.

  All officers of span B1 or B2       1\. The provisions of:\
                                      \
                                      (i) Sections 25, 27(3), 34, 37, 38,
                                      39, 40, 43, 44, 46, 47, 63, 64, 65
                                      and 67 of the Trade Marks Act
                                      1994,\
                                      \
                                      (ii) Sections 25(3) and 28 of the
                                      Trade Marks Act 1938,\
                                      \
                                      (iii) Section 5(2C), 73(2) and
                                      117B(4) of the Patents Act 1977,\
                                      \
                                      (iv) Sections 28, 47 and 117 of the
                                      Patents Act 1977 except insofar as
                                      opposed requests or the technical
                                      content of specifications are
                                      involved,\
                                      \
                                      (v) the relevant related Rules made
                                      under the above Acts,\
                                      \
                                      (vi) Orders made under Sections 8
                                      and 54 the Trade Marks Act 1994,\
                                      \
                                      (vii) Regulations made under the
                                      Trade Marks Act 1994,\
                                      \
                                      (viii) Rules made under Sections
                                      41, 52, 68, 69, 78 and 81 of the
                                      Trade Marks Act, and\
                                      \
                                      (ix) the following Rules:\
                                      \
                                      (a) Rules 53, 106, 108, 111(1) and
                                      113(4) of the Patents Rules 2007,\
                                      \
                                      (b) Schedule 1 to the Patents Rules
                                      2007 in connection with
                                      certificates authorising the
                                      release of biological material
                                      except insofar as an objection to
                                      release to a named expert is
                                      raised,\
                                      \
                                      (c) Rule 39 of the Registered
                                      Designs Rules 2006, and\
                                      \
                                      (d) Rule 20(1) of the Design Right
                                      (Proceedings before Comptroller)
                                      Rules 1989.

  All officers of span A2 or A3 or    1\. The provisions of:\
  higher                              \
                                      (i) Sections 117B(2), 118(1) and
                                      118(5) of the Patents Act 1977 and
                                      the relevant related Rules made
                                      under the Act, except insofar as
                                      opposed requests or the technical
                                      content of specifications are
                                      involved,\
                                      \
                                      (ii) Rules 11(3), 35(3), 35(5),
                                      35(5), 47(2), 104(1) and 108(2) of
                                      the Patents Rules 2007,\
                                      \
                                      (iii) the Registered Designs Act
                                      1949 and the Rules made under the
                                      Act insofar as the examination of
                                      design applications and the
                                      registration, renewal, restoration,
                                      assignment, licensing, cancellation
                                      and invalidation of registered
                                      designs is concerned,\
                                      \
                                      (iv) Section 21 of the Registered
                                      Designs Act 1949, and\
                                      \
                                      (v) Rules made under Section 30 of
                                      the Registered Designs Act 1949.\
                                      \
                                      2. Certifying certificates for the
                                      purposes of Section 32(10) of the
                                      Patents Act 1977 and copies or
                                      extracts falling within Section
                                      32(11) and (13) of the Patents Act
                                      1977 in accordance with the
                                      relevant Rules made under the Act.\
                                      \
                                      3. Certifying copies of an entry in
                                      the register or an extract from the
                                      register under the provisions of
                                      Section 63(3) of the Trade Marks
                                      Act 1994 in accordance with the
                                      relevant Rules and Regulations made
                                      under the Act.\
                                      \
                                      4. Certifying certificates for the
                                      purposes of Section 17(9) of the
                                      Registered Designs Act 1949 and
                                      copies or extracts falling within
                                      Section 17(10) and (12) of the
                                      Registered Designs Act 1949 in
                                      accordance with the relevant Rules
                                      made under the Act.\
                                      \
                                      5. All ordinary administrative
                                      functions under the Acts, Rules,
                                      Regulations and Orders mentioned in
                                      this Schedule, such as where the
                                      comptroller is required to send or
                                      issue any document, notify or give
                                      notice of any event or requirement,
                                      request information or payment of a
                                      fee, specify a period, publish or
                                      advertise any information, or enter
                                      or amend information in the
                                      register.
  -----------------------------------------------------------------------

### Section 130(1)

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 130(1)**
  "Convention on International Exhibitions" means the Convention relating to International Exhibitions signed in Paris on 22 November 1928, as amended or supplemented by any protocol to that convention which is for the time being in force;
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Section 130(1)

  -----------------------------------------------------------------------
   

  **Section 130(1)**

  "court" means\
  \
  (a) as respects England and Wales, the High Court;\
  \
  (b) as respects Scotland, the Court of Session;\
  \
  (c) as respects Northern Ireland, the High Court in Northern Ireland;\
  \
  (d) as respects the Isle of Man, His Majesty's High Court of Justice of
  the Isle of Man;
  -----------------------------------------------------------------------

### 130.06 {#section-5}

Part (d) of the definition of "court" was added by the Patents (Isle of
Man) Order 2013 (SI 2013/2602), which is the current Order in force.
[See
132.03](/guidance/manual-of-patent-practice-mopp/section-132-short-title-extent-commencement-consequential-amendments-and-repeals/#ref132-03)
for information on previous Orders. The reference to patents county
courts added to part (a) by the CDP Act 1988 was repealed by the Crimes
and Courts Act 2013.

### Section 130(1)

  -----------------------------------------------------------------------
   

  **Section 130(1)**

  "date of filing" means\
  \
  (a) in relation to an application for a patent made under this Act, the
  date which is the date of filing that application by virtue of section
  15 above; and\
  \
  (b) in relation to any other application, the date which, under the law
  of the country where the application was made or in accordance with the
  terms of a treaty or convention to which that country is a party, is to
  be treated as the date of filing that application or is equivalent to
  the date of filing an application in that country (whatever the outcome
  of the application);
  -----------------------------------------------------------------------

### 130.07 {#ref130-07}

The date of filing of an application under the Act is thus the date
accorded under section 15.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 130(1)**
  "designate" in relation to an application or a patent, means designate the country or countries (in pursuance of the European Patent Convention or the Patent Co-operation Treaty) in which protection is sought for the invention which is the subject of the application or patent and includes a reference to a country being treated as designated in pursuance of the convention or treaty;
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 130.08 {#section-6}

EPC r.41 PCT rr. 3 and 4 are also relevant

"European Patent Convention" (EPC) and "Patent Co-operation Treaty"
(PCT) are defined later in subsection (1), see below. Designations are
made on the relevant EPC or PCT request form -- although in both cases
it is now the case that all states and regions are by default treated as
being designated on filing.

  -----------------------------------------------------------------------------------------------
   
  **Section 130(1)**
  "electronic communication" has the same meaning as in the Electronic Communications Act 2000;
  -----------------------------------------------------------------------------------------------

### 130.08.1 {#section-7}

This definition was inserted into s.130(1) by the Patents Act 1977
(Electronic Communications) Order 2003 (S.I. 2003 No. 512), which
inserted s.124A into the Act.

### Section 130(1)

"employee" means a person who works or (where the employment has ceased)
worked under a contract of employment or in employment under or for the
purposes of a government department or a person who serves (or served)
in the naval, military or air forces of the Crown;

### 130.09 {#section-8}

The part of the definition of "employee" following the word "department"
was added by the Armed Forces Act 1981 (c.55), section 22.

  --------------------------------------------------------------------------------------------------
   
  **Section 130(1)**
  "employer" in relation to an employee, means the person by whom the employee is or was employed;
  --------------------------------------------------------------------------------------------------

  -----------------------------------------
   
  **Section 130(1)**
  "enactment" includes an Act of Tynwald;
  -----------------------------------------

### 130.09.1 {#section-9}

The definition of "enactment" was added by the Patents Act 1977 (Isle of
Man) (Variation) Order 1990. This Order was revoked by the Patents Act
1977 (Isle of Man) Order 2003 (S.I. 2003 No. 1249), which re-inserted
this definition into the Patents Act.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                                                                                                                                                                                              
  **Section 130(1)**                                                                                                                                                                                                                                                                                                                                                                                                          
  "European Patent Convention" means the Convention on the Grant of European Patents, "European patent" means a patent granted under that convention, "European patent (UK)" means a European patent designating the United Kingdom, "European Patent Bulletin" means the bulletin of that name published under the convention, and "European Patent Office" means the office of that name established by that convention;    
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                                                                                                                                                                                     
  **Section 130(1)**                                                                                                                                                                                                                                                                                                                                                                                                 
  "exclusive licence" means a licence from the proprietor of or applicant for a patent conferring on the licensee, or on him and persons authorised by him, to the exclusion of all other persons (including the proprietor or applicant), any right in respect of the invention to which the patent or application relates, and "exclusive licensee" and "non-exclusive licence" shall be construed accordingly;    
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.09.2 {#section-10}

[See
67.04](/guidance/manual-of-patent-practice-mopp/section-67-proceedings-for-infringement-by-exclusive-licensee/#ref67-04).

  ---------------------------------------------------------------------- ---
                                                                          
  **Section 130(1)**                                                      
  "filing fee" means the fee prescribed for the purposes of section 14    
  ---------------------------------------------------------------------- ---

### 130.10 {#section-11}

s.14(1)(b) s.89A(3) are also relevant

A filing fee may be payable when an application under the Act is filed
([see
15.06](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-06))
or when (or before) an international application enters the national
phase ([see
89A.04-21](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-04)).

  ------------------------------------------------------------------------------------------------------------------------ ---
                                                                                                                            
  **Section 130(1)**                                                                                                        
  "formal requirements" means those requirements designated as such by rules made for the purposes of section 15A above;    
  ------------------------------------------------------------------------------------------------------------------------ ---

### 130.11 {#section-12}

The formal requirements are set out in r.25 ([see
17.07](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-07)).

  ----------------------------------------------------------------------------------------------------------- ---
                                                                                                               
  **Section 130(1)**                                                                                           
  "international application for a patent" means an application made under the Patent Co­ operation Treaty;    
  ----------------------------------------------------------------------------------------------------------- ---

### 130.12 {#section-13}

"Patent Co-operation Treaty" is defined later in subsection (1), see
below.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                   
  **Section 130(1)**                                                                                                                                               
  "International Bureau" means the secretariat of the World Intellectual Property Organization established by a convention signed at Stockholm on 14 July 1967;    
  --------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                                          
  **Section 130(1)**                                                                                                                                                                                                                                                      
  "international exhibition" means an official or officially recognised international exhibition falling within the terms of the Convention on International Exhibitions or falling within the terms of any subsequent treaty or convention replacing that convention;    
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.13 {#ref130-13}

"Convention on International Exhibitions" is defined earlier in
subsection (1),

see above. See also
[2.40-41](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-40).

  --------------------------------------------------------------- ---
                                                                   
  **Section 130(1)**                                               
  "inventor" has the meaning assigned to it by section 7 above;    
  --------------------------------------------------------------- ---

### 130.14 {#section-14}

[See
7.12](/guidance/manual-of-patent-practice-mopp/section-7-right-to-apply-for-and-obtain-a-patent/#ref7-12).

  -------------------------------------------------------------------- ---
                                                                        
  **Section 130(1)**                                                    
  "journal" has the meaning assigned to it by section 123 (6) above;    
  -------------------------------------------------------------------- ---

### 130.15 {#section-15}

[See
123.72](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-72).

  -------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                      
  **Section 130(1)**                                                                                                                                  
  "mortgage", when used as a noun, includes a charge for securing money or money's worth and, when used as a verb, shall be construed accordingly;    
  -------------------------------------------------------------------------------------------------------------------------------------------------- ---

  ---------------------------------------- ---
                                            
  **Section 130(1)**                        
  "1949 Act" means the Patents Act 1949;    
  ---------------------------------------- ---

  ----------------------------------------- ---
                                             
  **Section 130(1)**                         
  "patent" means a patent under this Act;    
  ----------------------------------------- ---

### 130.16 {#section-16}

s.86(4) is also relevant

Thus the provisions of the Act apply only to "patents" granted, or
applications for "patents" filed, under the Act except where there is
provision to the contrary. There are such exceptions making parts of the
Act applicable in the cases of 1949 Act patents and applications, in
accordance with s.127 and Schedules 2 and 4 ([see
127.06-07](/guidance/manual-of-patent-practice-mopp/section-127-existing-patents-and-applications/#ref127-06)
and
[127.10](/guidance/manual-of-patent-practice-mopp/section-127-existing-patents-and-applications/#ref127-10));
European patents (UK), in accordance with s.77 ([see
77.03](/guidance/manual-of-patent-practice-mopp/section-77-effect-of-european-patent-uk/#ref77-03)
et seq); applications for European patents (UK), in accordance with s.78
([see
78.05](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-05));
and international applications for patents (UK), in accordance with s.89
([see
89.05](/guidance/manual-of-patent-practice-mopp/section-89-effect-of-international-application-for-patent/#ref89-05)
et seq). However, the provisions of the Act are not applicable to
Community patents.

### 130.17 {#section-17}

Deleted

  -------------------------------------------------------------------------------------------------- ---
                                                                                                      
  **Section 130(1)**                                                                                  
  "Patent Co-operation Treaty" means the treaty of that name signed at Washington on 19 June 1970;    
  -------------------------------------------------------------------------------------------------- ---

  ------------------------------------------------------------------------------------------------------------------------------ ---
                                                                                                                                  
  **Section 130(1)**                                                                                                              
  "patented invention" means an invention for which a patent is granted and "patented process" shall be construed accordingly;    
  ------------------------------------------------------------------------------------------------------------------------------ ---

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                           
  **Section 130(1)**                                                                                                                                                                                       
  "patented product" means a product which is a patented invention or, in relation to a patented process, a product obtained directly by means of the process or to which the process has been applied;    
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

  ----------------------------------------------------------------------------------- ---
                                                                                       
  **Section 130(1)**                                                                   
  "prescribed" and "rules" have the meanings assigned to them by section 123 above;    
  ----------------------------------------------------------------------------------- ---

### 130.18 {#section-18}

[See
123.01](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-01).

  -------------------------------------------------------------------------- ---
                                                                              
  **Section 130(1)**                                                          
  "priority date" means the date determined as such under section 5 above;    
  -------------------------------------------------------------------------- ---

### 130.19 {#section-19}

[See
5.03](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-03)
et seq.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                                                                                                                                     
  **Section 130(1)**                                                                                                                                                                                                                                                                                                                                                 
  "published" means made available to the public (whether in the United Kingdom or elsewhere) and a document shall be taken to be published under any provision of this Act if it can be inspected as of right at any place in the United Kingdom by members of the public, whether on payment of a fee or not; and "republished" shall be construed accordingly;    
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.20 {#section-20}

Thus an application laid open to public inspection in the Office ([see
16.03](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-03))
has been "published". Similarly, documents relating to an application
and available to inspection under s.118(1) are therefore "published" and
thus form part of s.2(2) art.

  -------------------------------------------------------------------------------------------- ---
                                                                                                
  **Section 130(1)**                                                                            
  "register" and cognate expressions have the meanings assigned to them by section 32 above;    
  -------------------------------------------------------------------------------------------- ---

### 130.21 {#section-21}

See the chapter on s.32.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                                                                                                                 
  **Section 130(1)**                                                                                                                                                                                                                                                                                                                             
  "relevant convention court", in relation to any proceedings under the European Patent Convention, or the Patent Co-operation Treaty, means that court or other body which under that convention or treaty has jurisdiction over those proceedings, including (where it has such jurisdiction) any department of the European Patent Office;    
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.22 {#ref130-22}

"European Patent Convention" "European Patent Office", and "Patent Co­
operation Treaty" are defined earlier in subsection (1), see above. The
"relevant convention court" appears to include a national office acting
in its capacity as a receiving office under the appropriate convention
or treaty. However, it does not include a national office, e.g. that of
the USA, processing an international application which was made under
the PCT but has entered its national phase before that office and is
proceeding under that national law instead of under the treaty [(Sonic
Tape PLC's Patent \[1987\] RPC
251)](http://rpc.oxfordjournals.org/content/104/12/251.abstract){rel="external"}.

### Section 130(1)

"right", in relation to any patent or application, includes an interest
in the patent or application and, without prejudice to the foregoing,
any reference to a right in a patent includes a reference to a share in
the patent;

### 130.22.1 {#ref130-22-1}

It was held in [Hartington Conway Ltd's Patent Applications \[2004\] RPC
6](http://rpc.oxfordjournals.org/content/121/4/161.full.pdf+html){rel="external"}
that the term "right", in relation to an application, encompassed not
only a right in relation to an existing application, but also a right to
file an application for grant of a patent ([see 30.05 and
30.08](/guidance/manual-of-patent-practice-mopp/section-30-nature-of-and-transactions-in-patents-and-applications-for-patents/#ref30-05)).

### Section 130(1)

"search fee" means the fee prescribed for the purposes of section 17(1)
above;

### 130.23 {#section-22}

[See
17.02-06](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-02).
The definition of "search fee" was amended by the CDP Act to make it
clear that this term alludes to the fee for a preliminary examination
and search, and not to the fee for a supplementary search under s.17(8)
and s.18(1A).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                                             
  **Section 130(1)**                                                                                                                                                                                                                                                         
  "services of the Crown" and "use for the services of the Crown" have the meanings assigned to them by section 56(2) above, including, as respects any period of emergency within the meaning of section 59 above, the meanings assigned to them by the said section 59.    
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.24 {#section-23}

See the chapters on ss.56 and 59.

### Section 130(2)

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                           
  **Section 130(2)**                                                                                                                                                                                                                                       
  Rules may provide for stating in the journal that an exhibition falls within the definition of international exhibition in subsection (1) above and any such statement shall be conclusive evidence that the exhibition falls within that definition.    
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.25 {#section-24}

Rule 5 is made under this subsection ([see
2.40-41](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-40)).
[See also 130.13](#ref130-13).

### Section 130(3)

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                                                                                                      
  **Section 130(3)**                                                                                                                                                                                                                                                                                                                  
  For the purposes of this Act matter shall be taken to have been disclosed in any relevant application within the meaning of section 5 above or in the specification of a patent if it was either claimed or disclosed (otherwise than by way of disclaimer or acknowledgment of prior art) in that application or specification.    
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.26 {#ref130-26}

[See
5.23-5.25](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-23)
with regard to disclosure in an earlier relevant application from which
priority is claimed. For the meaning of "relevant application", [see
5.30](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-30).
The term "disclaimer" was held by the Patents Court in Secretary of
State for Education and Skills v Frontline Technology \[2005\] EWHC 37
(Ch) to relate to disclaimers relating to accidental anticipations. In
joined cases G1/03 and G2/03 of the EPO Enlarged Board of Appeal,
followed in M-Systems Flash Disk Pioneers Ltd v Trek Technology
(Singapore) Pte Ltd [BL
O/318/06](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/318/06){rel="external"},
the term "disclaimer" was further held to relate to delimiting a claim
against anticipations under s2(3) and subject matter excluded from
patentability for non-technical reasons.

### Section 130(4)

  --------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                       
  **Section 130(4)**                                                                                                                                   
  References in this Act to an application for a patent, as filed, are references to such an application in the state it was on the date of filing.    
  --------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.27 {#section-25}

An application as filed does not include an abstract or claims filed
after the date of filing ([see 130.07](#ref130-07)) but does incorporate
any alterations filed on that date ([see
16.08­-14](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-08),
especially
[16.10](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-10)).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                  
  **Section 130(4A)**                                                                                                                                                                                                                             
  An international application for a patent is not, by reason of being treated by virtue of the European Patent Convention as an application for a European patent (UK), to be treated also as an international application for a patent (UK).    
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.27.1 {#ref130-27-1}

PCT, r.4.9(a) are also relevant

Subsection (4A) was inserted by the Patents Act 2004 and replaced
section 89(4). An international application under the PCT automatically
designates all contracting states and regions of the PCT on filing.
However, if the UK designation on an international application is later
withdrawn, that application cannot be considered to be an international
application for a patent (UK) on the basis that the EP(UK) designation
remains.

  ------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                         
  **Section 130(5)**                                                                                                                     
  References in this Act to an application for a patent being published are references to its being published under section 16 above.    
  ------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.28 {#section-26}

[See
16.01](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-01).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ---
                                                                                                                                                                                                                                                                                        
  **Section 130(5A)**                                                                                                                                                                                                                                                                   
  References in this Act to the amendment of a patent or its specification (whether under this Act or by the European Patent Office) include, in particular, limitation of the claims (as interpreted by the description and any drawings referred to in the description or claims).    
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ ---

### 130.28.1 {#section-27}

This section glosses references to amendment wherever they appear in the
1977 Act. However, this only applies to amendment of a patent, and so
does not include references to amendment of a patent application. The
paragraph makes it clear that amendment of a patent includes an
amendment which limits the scope of the claims -- and so limits the
protection afforded by the patent. A limitation of the claims may be
made under the provisions of this Act or, in the case of a European
patent (UK), the claims may be limited at the European Patent Office
(under the new procedure introduced by Article 138(3) EPC).

  ----------------------------------- -----------------------------------
                                       

  **Section 130(6)**                   

  References in this Act to any of     
  the following conventions, that is  
  to say\                             
  \                                   
  (a) The European Patent             
  Convention;\                        
  \                                   
  (b) The Community Patent            
  Convention;\                        
  \                                   
  (c) The Patent Co-operation         
  Treaty;\                            
  \                                   
  are references to that convention   
  or any other international          
  convention or agreement replacing   
  it, as amended or supplemented by   
  any convention or international     
  agreement (including in either case 
  any protocol or annex), or in       
  accordance with the terms of any    
  such convention or agreement, and   
  include references to any           
  instrument made under any such      
  convention or agreement.            
  ----------------------------------- -----------------------------------

### 130.29 {#section-28}

EPC a.33 is also relevant

The Administrative Council of the EPO has from time to time amended the
Implementing Regulations and the Rules relating to Fees under the EPC.
The PCT was amended on 28 September 1979 and modified on 3 February
1984, and the Regulations including the Schedule of Fees under the
treaty have been amended on several occasions.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
  **Section 130(7)**                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  Whereas by a resolution made on the signature of the Community Patent Convention the governments of                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
  the member states of the European Economic Community resolved to adjust their laws relating to patents so as (among other things) to bring those laws into conformity with the corresponding provisions of the European Patent Convention, the Community Patent Convention and the Patent Co-operation Treaty, it is hereby declared that the following provisions of this Act, that is to say, sections 1(1) to (4), 2 to 6, 14(3), (5) and (6),37(5), 54, 60, 69, 72(1) and (2), 74(4), 82, 83, 100 and 125, are so framed as to have, as nearly as practicable, the same effects in the United Kingdom as the corresponding provisions of the European Patent Convention, the Community Patent Convention and the Patent Co-operation Treaty have in the territories to which those Conventions apply.    
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---

### 130.30 {#ref130-30}

In the resolution, the governmental signatories recognised that
differences between national laws and the provisions of the CPC would
entail a duality of standards in patent laws in the contracting states
and decided to adjust their national laws to permit ratification of the
Strasbourg Convention on the unification of certain points of
substantive law on patents for invention and to bring their laws into
conformity as far as practical with corresponding provisions of the EPC,
CPC and PCT. It would also be anomalous if the provisions of
patentability and interpretation differed between the EPC which is
applicable to applications for European patents (UK) and to European
patents (UK) during the opposition period and our national law which
applies to European patents (UK) after grant. Similarly conformity with
many other aspects of the EPC and with the PCT is desirable.

### 130.31 {#ref130-31}

The particular provisions of the EPC, CPC and PCT corresponding to the
sections of the Act specified in s.130(7) are identified in the chapters
relating to those sections. When applying s.130(7) to those sections,
the principle of construction quoted by Lord Diplock in The Jade
\[1976\] 1 All ER 920, 924 (not a patent case) should be followed. He
stated that "As the Act was passed to enable His Majesty's Government to
give effect to the obligations in international law which it would
assume on ratifying the convention .... if there be any difference
between the language of the statutory provision and that of the
corresponding provisions of the Convention, the Statutory language
should be construed in the same sense as that of the Convention if the
words of the Statute are reasonably capable of bearing that meaning".
Furthermore, in [Merrell Dow Pharmaceuticals Inc. v H.N. Norton & Co Ltd
\[1996\] RPC
76](http://rpc.oxfordjournals.org/content/113/3/76.full.pdf+html){rel="external"},
the House of Lords held that in construing a section of the Patents Act
1977 said by section 130(7) to have, as nearly as practicable, the same
effects as the corresponding provisions of the European Patent
Convention, the United Kingdom courts must have regard to the decisions
of the EPO. Lord Hoffmann said (at page 82): "These decisions are not
strictly binding upon the courts in the United Kingdom but they are of
great persuasive authority; first, because they are decisions of expert
courts (the Boards of Appeal and Enlarged Board of Appeal of the EPO)
involved daily in the administration of the EPC, and secondly, because
it would be highly undesirable for the provisions for the EPC to be
construed differently in the EPO from the way they are interpreted in
the national courts of a contracting state". In the judgment given by
Jacob LJ in Actavis UK Ltd v Merck \[2008\] EWCA Civ 444, it was held
that the Court of Appeal can (but is not bound to) depart from its own
precedent if it is satisfied that the EPO Boards of Appeal have formed a
settled view of European Patent law which is inconsistent with the Court
of Appeal earlier decision. Generally the Court of Appeal will follow
settled view of the EPO (see para 107 of the decision).

See also the discussion in
[0.08-09](/guidance/manual-of-patent-practice-mopp/manual-of-patent-practice-introduction/#ref0-08).

### 130.32 {#ref130-32}

However Lord Diplock himself stated in [E's Applications \[1983\]
RPC](http://rpc.oxfordjournals.org/content/100/14/231.full.pdf+html?sid=0adfebca-b4c4-4d6a-92b9-0b1b1d291ab8){rel="external"}
at page 251 that resort to the PCT was not permitted under the principle
of construction enunciated in The Jade if the wording of the Act is
incapable of any other meaning.

### 130.33 {#ref130-33}

When construing the EPC, CPC or PCT it is legitimate to consider the
intention behind any provision as indicated in the papers of the
conference resulting in the Convention or Treaty. Following the judgment
of the House of Lords in Pepper v Hart \[1992\] 3 WLR 1032, the rule
excluding reference to Parliamentary material as an aid to statutory
construction has been relaxed so as to permit such references where (a)
legislation was ambiguous or obscure or led to absurdity; (b) the
material relied upon consisted of one or more statements by a minister
or other promoter of the Bill together if necessary with such other
Parliamentary material as was necessary to understand such statements
and their effect and (c) the statements were clear.

### 130.34 {#ref130-34}

The Patent Law Treaty (PLT) was signed by the United Kingdom in 2000 and
entered into force for the United Kingdom on 22 March 2006. It is by
nature a different type of treaty than those listed in section 130(7),
being essentially a minimum standards treaty setting out provisions
aimed at providing patent applicants with certain flexibilities. However
in [Abaco Machines (Australasia) Pty Ltd's Application \[2007\] EWHC 347
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2007/347.html){rel="external"},
which concerned a request to make a late declaration of priority under
section 5(2B), Lewison J commented that the PLT is at least part of the
background against which section 5 should be viewed. He agreed in
principle with the submission put to him that section 5 of the Act
should be interpreted in the light of the PLT itself. He then went on to
assess the facts of that case using the language of the PLT, although he
did also confirm that this was consistent with the language of section
5. Accordingly the Patent Law Treaty may be of relevance when
interpreting provisions in the Act or rules that were inspired by this
Treaty.

### 130.35 {#section-29}

\[deleted\]

### Section 130(8)

  -------------------------------------------------------------------------------------------------------------- ---
                                                                                                                  
  **Section 130(8)**                                                                                              
  Part I of the Arbitration Act 1996 shall not apply to any proceedings before the comptroller under this Act.    
  -------------------------------------------------------------------------------------------------------------- ---

### 130.35.1 {#section-30}

The words "Part I of the Arbitration Act 1996" were substituted for "The
Arbitration Act 1950" by Schedule III of the Arbitration Act 1996.

### Section 130(9)

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
                                                                                                                                                                                                                                 
  **Section 130(9)**                                                                                                                                                                                                             
  Except so far as the context otherwise requires, any reference in this Act to any enactment shall be construed as a reference to that enactment as amended or extended by or under any other enactment, including this Act.    
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
