::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 20B: Effect of reinstatement under section 20A {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (20B.01 - 20B.02) last updated: April 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 20B.01 {#b01}

This section sets out the rights that are accorded to third parties upon
reinstatement of a patent application under section 20A. The section is
analogous to section 28A which prescribes similar rights on restoration
of a patent.

  --------------------------------------------------------------------------------
   
  **Section 20B(1)**
  The effect of reinstatement under section 20A of an application is as follows.
  --------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 20B(2)**
  Anything done under or in relation to the application during the period between termination and reinstatement shall be treated as valid.
  ------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 20B(3)**

  If the application has been published under section 16 above before its
  termination anything done during that period which would have
  constituted an infringement of the rights conferred by publication of
  the application if the termination had not occurred shall be treated as
  an infringement of those rights\
  ­ (a) if done at a time when it was possible for the period referred to
  in section 20A(1) above to be extended, or\
  (b) if it was a continuation or repetition of an earlier act infringing
  those rights.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 20B(4)**

  If the application has been published under section 16 above before its
  termination and, after the termination and before publication of notice
  of the request for its reinstatement, a person\
  (a) began in good faith to do an act which would have constituted an
  infringement of the rights conferred by publication of the application
  if the termination had not taken place, or\
  (b) made in good faith effective and serious preparation to do such an
  act, he has the right to continue to do the act or, as the case may be,
  to do the act, notwithstanding the reinstatement of the application and
  the grant of the patent; but this right does not extend to granting a
  licence to another person to do the act.
  -----------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 20B(4A)**
  The right conferred by subsection (4) does not become exercisable until the end of the period during which a request may be made under this Act, or under the rules, for an extension of the period referred to in section 20A(1).
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 20B(5)**

  If the act was done, or the preparations were made, in the course of a
  business, the person entitled to the right conferred by subsection (4)
  above may­\
  (a) authorise the doing of that act by any partners of his for the time
  being in that business, and\
  (b) assign that right, or transmit it on death (or in the case of a
  body corporate on its dissolution), to any person who acquires that
  part of the business in the course of which the act was done or the
  preparations were made.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 20B(6)**
  Where a product is disposed of to another in exercise of a right conferred by subsection (4) or (5) above, that other and any person claiming through him may deal with the product in the same way as if it had been disposed of by the applicant.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 20B(6A)**

  The above provisions apply in relation to the use of a patented
  invention for the services of the Crown as they apply in relation to
  infringement of the rights conferred by publication of the application
  for a patent (or, as the case may be, infringement of the patent).\
  "Patented invention" has the same meaning as in section 55 below.
  -----------------------------------------------------------------------

### 20B.02 {#b02}

Subsections (3) to (6) give protection to persons who take steps to work
an invention which is the subject of a terminated patent application
before notice of a request to reinstate the application is published
([see
20A.08](/guidance/manual-of-patent-practice-mopp/section-20a-reinstatement-of-applications/#ref20A-08)).
If they take these steps after the end of the period during which the
patent applicant can request an extension of time, then they are free
not only to continue what they have started without infringing the
reinstated application, but also to pass their rights to work the
invention to others (but not license others to work the invention).
Subsection (4A) was inserted by the Intellectual Property Act 2014 on 1
October 2014 to make clear that these "third party rights" do not begin
until the end of the period during which the patent applicant can
request an extension of time to the period they had failed to meet.
Subsection (6A) ensures that the Crown is not liable for payment of
compensation to the patent proprietor for Crown use if it takes steps to
work an invention which is the subject of a terminated patent
application before notice of a request to reinstate the application is
published.

  -----------------------------------------------------------------------
   

  **Section 20B(7)**

  In this section "termination", in relation to an application, means\
  (a) the refusal of an application, or\
  (b) the application being treated as having been refused or withdrawn.
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
