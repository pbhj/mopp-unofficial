::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 68: Effect of non-registration on infringement proceedings {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (68.01 - 68.08) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 68.01

This section inhibits the award of costs or expenses by the court or the
comptroller, in respect of infringement of a patent, to a person who has
become a proprietor or exclusive licensee of the patent by virtue of a
transaction, instrument or event which was not, or not sufficiently
promptly, registered in the register of patents. The transactions,
instruments or events in question are those to which s.33 applies; these
are listed in s.33(3). This section was amended by The Intellectual
Property (Enforcement, etc.) Regulations 2006 (SI 2006 No. 1028) which
came into force on 29 April 2006. This removed the restriction on grant
of damages or an account of profits to a proprietor or exclusive
licensee who had not sufficiently promptly registered a transaction,
instrument or event. Such a restriction was not compatible with article
13.1 of Directive 2004/48/EC on the enforcement of intellectual property
rights.

### 68.02

In accordance with s.69, references in this section to a patent and the
proprietor of a patent are to be respectively construed as including
references to an application which has been published but not yet
granted and the applicant. By an exclusive licensee is meant the holder
of an exclusive licence as defined in
[67.04](/guidance/manual-of-patent-practice-mopp/section-67-proceedings-for-infringement-by-exclusive-licensee/#ref67-04).

### 68.03

For the applicability of s.68 in relation to European patents, [see
60.02](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-02).
With regard to applications for European patents, see also
[68.05](#ref68-05).

  -----------------------------------------------------------------------
   

  **Section 68**

  Where by virtue of a transaction, instrument or event to which section
  33 above applies a person becomes the proprietor or one of the
  proprietors or an exclusive licensee of a patent and the patent is
  subsequently infringed before the transaction, instrument or event is
  registered, in proceedings for such an infringement, the court or
  comptroller shall not award him costs or expenses unless\
  (a) the transaction, instrument or event is registered within the
  period of six months beginning with its date; or\
  (b) the court or the comptroller is satisfied that it was not
  practicable to register the transaction, instrument or event before the
  end of that period and that it was registered as soon as practicable
  thereafter.
  -----------------------------------------------------------------------

### 68.04 {#ref68-04}

If a person became a proprietor or exclusive licensee before a
particular infringement occurred, they cannot be awarded costs or
expenses if the relevant transaction, instrument or event was not
registered before the date of the infringement, unless either of the two
conditions in section 68 is met. The conditions are that registration
occurs within a period of six months of the date of the transaction,
instrument or event, or it is established that registration within that
period was not practicable but occurred as soon as practicable
thereafter.

### 68.05 {#ref68-05}

The appropriate registration procedure is prescribed by r.47 ([see 32.07
to
32.09](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-07)),
except in the case of applications for European patents (UK) which have
not yet been granted ([see
32.06](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-06),
[78.06 and
78.07](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-06)).

### 68.06 {#ref68-06}

The defendant argued in [H.Lundbeck A/S v Norpharma SpA \[2011\] EWHC
907 (Pat), \[2011\] RPC
23](http://rpc.oxfordjournals.org/content/128/9/657.abstract){rel="external"},
which related to a granted European patent (UK), that registration of an
assignment at the EPO counts as registration for the purposes of s.68.
However, this argument was rejected by the court. There is no provision
which deems registration of a transaction, instrument or event on a
granted European patent (UK) at the EPO to be registration under the Act
(unlike section 78(3)(f) which allows the registration of an assignment
of an application for a European patent (UK) at the EPO to be treated as
a registration under the Act). The transaction, instrument or event
therefore has to be registered in the UK to be considered for the
purposes of s.68.

### 68.07 {#ref68-07}

In considering whether it was not practicable to register the
transaction within six months of the date of the transaction, the
actions of both the applicant and their agent should be considered. In
[H.Lundbeck A/S v Norpharma SpA \[2011\] EWHC 907 (Pat), \[2011\] RPC
23](http://rpc.oxfordjournals.org/content/128/9/657.abstract){rel="external"}
the applicant instructed their agent to register the assignment, but the
agent subsequently failed to do so. The court held that these actions
were not sufficient to show that it was not practicable to register the
assignment. Considerations under section 68(b) should take account of
what was done by the applicant and their agent and not just the
applicant alone.

### 68.08 {#ref68-08}

In [Schütz (UK) Ltd v Werit UK Ltd & Anr \[2011\] EWCA Civ
927](http://www.bailii.org/ew/cases/EWCA/Civ/2011/927.html){rel="external"},
Sir Robin Jacob explained that this provision was not intended to say
that "once a party had failed to register a document in time it could
never recover costs even if all the infringements complained of only
occurred after registration." He explained that "if and in so far as a
claim covers a period for which a relevant transaction was not
registered when it should have been (a "non-registration period") then
any costs incurred during that period cannot be recovered. Costs for
periods outside a non-registration period are recoverable in the usual
way".
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
