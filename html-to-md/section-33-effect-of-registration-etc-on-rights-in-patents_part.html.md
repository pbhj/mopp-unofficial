::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 33: Effect of registration, etc on rights in patents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (33.01 - 33.06) last updated: January 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 33.01

Assignments and other transactions, instruments or events affecting
rights in or under patents and applications may be entered in the
register of patents once the patent application in question has been
published. Also, where the comptroller is notified of such a
transaction, instrument or event prior to the application being
published, the comptroller may, in accordance with rule 55(g), make that
information public. An application to register, or to give notice to the
comptroller of, any such transaction, instrument or event should follow
the procedure prescribed by r.47, [see
32.08-10](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-08)
Section 33 concerns the effect of such registration or giving of notice
on the rights in question.

### 33.02

(s.77(1), s.78(2) s.78(3)(f) is also relevant)

Section 33 applies in relation to not only 1977 Act patents and
applications but also granted European patents (UK) and applications for
European patents (UK). Registration of an application for a European
patent (UK) in the Register of European Patents kept by the EPO is
treated as registration under the 1977 Act, [see
32.07](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-07)
,
[78.06](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-06)
and
[78.07](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-07).

  -----------------------------------------------------------------------
   

  **Section 33(1)**

  Any person who claims to have acquired the property in a patent or
  application for a patent by virtue of any transaction, instrument or
  event to which this section applies shall be entitled as against any
  other person who claims to have acquired that property by virtue of an
  earlier transaction, instrument or event to which this section applies
  if, at the time of the later transaction, instrument or event -\
  (a) the earlier transaction, instrument or event was not registered,
  or\
  (b) n the case of any application which has not been published, notice
  of the earlier transaction, instrument or event had not been given to
  the comptroller, and\
  (c) in any case, the person claiming under the later transaction,
  instrument or event, did not know of the earlier transaction,
  instrument or event.
  -----------------------------------------------------------------------

### 33.03

The transactions, instruments or events (hereinafter termed
"transactions etc") to which this section applies are defined by
s.33(3). A person acquiring the property in a patent or application by
virtue of any such transaction etc is entitled as against any other
person who claims to have acquired that property by virtue of an earlier
such transaction etc if two conditions are met at the time of the later
transaction etc. One condition is that the earlier transaction etc was
not registered or (if it relates to an unpublished application) notice
of the earlier transaction etc had not been given to the comptroller.
The other condition is that the person claiming under the later
transaction etc did not know of the earlier one.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 33(2)**
  Subsection (1) above shall apply equally to the case where any person claims to have acquired any right in or under a patent or application for a patent, by virtue of a transaction, instrument or event to which this section applies, and that right is incompatible with any such right acquired by virtue of an earlier transaction, instrument or event to which this section applies .
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 33.04

The entitlement to which s.33(1) refers applies even if the right
claimed under a later such transaction etc is incompatible with a right
acquired under an earlier one.

  -----------------------------------------------------------------------
   

  **Section 33(3)**

  This section applies to the following transactions, instruments and
  events -\
  (a) the assignment or assignation of a patent or application for a
  patent, or a right in it;\
  (b) the mortgage of a patent or application or the granting of security
  over it;\
  (c) the grant, assignment or assignation of a licence or sub-licence,
  or mortgage of a licence or sub-licence, under a patent or
  application;\
  (d) the death of the proprietor or one of the proprietors of any such
  patent or application or any person having a right in or under a patent
  or application and the vesting by an assent of personal representatives
  of a patent, application or any such right; and\
  (e) any order or directions of a court or other competent authority\
  (i) transferring a patent or application or any right in or under it to
  any person; or\
  (ii) that an application should proceed in the name of any person;\
  and in either case the event by virtue of which the court or authority
  had power to make any such order or give any such directions.
  -----------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 33(4)**
  Where an application for the registration of a transaction, instrument or event has been made, but the transaction, instrument or event has not been registered, then, for the purposes of subsection (1)(a) above, registration of the application shall be treated as registration of the transaction, instrument or event.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 33.05

If the making of an application for registration of a transaction etc
has been recorded in the register ([see
32.09](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-09)),
it is immaterial for the purposes of s.33(1)(a) whether or not the
transaction etc has actually been entered in the register at the time of
a later transaction etc. Thus for determining the entitlement to which
s.33(1) refers, registration of the making of the application is deemed
to constitute registration of the transaction etc.

### 33.05.1 {#ref33-05-1}

Following the end of the transition period provided by the Agreement on
the Withdrawal of the United Kingdom from the European Union, Article 36
of Regulation (EU) No. 1215/2012 of 12 December 2012 on jurisdiction and
the recognition and enforcement of judgments in civil and commercial
matters (recast) ("the recast Brussels I Regulation"), which requires EU
member states to recognise judgments given in other member states
without further proceedings, ceased to apply in the UK. The UK has
applied to accede in its own right to the Lugano Convention on
Jurisdiction and Enforcement of Judgments in Civil and Commercial
Matters, which makes similar provisions across the EEA states. Reference
should be made to the Ministry of Justice guidance on the rules
governing recognition and enforcement of foreign judgments in
cross-border disputes following the end of the transition period.

### 33.05.2 {#ref33-05-2}

\[Deleted\]

### 33.05.3 {#ref33-05-3}

Under Article 67 of the Agreement on the Withdrawal of the United
Kingdom from the European Union, the recast Brussels I Regulation will
continue to apply to judgments issued in legal proceedings that were
initiated before the end of the transition period, and orders concluded
before that point. Therefore, an application to enter a court order on
the register transferring a patent or application, or stating that the
application is to proceed in the name of another person, made by a court
in the European Economic Area before that date, should be accepted
unless it cannot be reconciled with an earlier court order

### 33.06 {#section-5}

In Molnlycke AB and another v Procter & Gamble Ltd and others \[1994\]
RPC 49 at page 138 the Court of Appeal rejected the argument that entry
in register of a notice that a s.32 application had been filed was
equivalent to registration. The purpose of subsection (4) is to secure
priority for a person who claims an interest in a patent and seeks
registration. It is not to substitute for the requirement of
registration of the particular transaction or instrument a lesser
requirement that an application has been made for registration of an
unspecified transaction or instrument.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
