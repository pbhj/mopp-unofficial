::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 275A: Regulation of patent attorneys {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (275A.01 - 275A.04) last updated: July 2012
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 275A.01 {#a01}

This section provides for the regulation of patent attorneys and is
introduced into the CDP Act by section 185(3) of the Legal Services Act
2007.

Section 275A replaces the rules (The Register of Patent Agents Rules (SI
1990/1457)) previously made under section 275(2) and 275(3) of the CDP
Act 1988.

  -----------------------------------------------------------------------
   

  **Section 275A(1)**

  The person who keeps the register under section 275 may make
  regulations which regulate---\
  (a) the keeping of the register and the registration of persons;\
  (b) the carrying on of patent attorney work by registered persons.
  -----------------------------------------------------------------------

### 275A.02 {#a02}

This subsection provides for regulations to be made concerning the
registration and maintenance of the register of patent attorneys and
concerning the carrying on of patent attorney work by those who have
been recorded on the register.

  -----------------------------------------------------------------------
   

  **Section 275A(2)**

  Those regulations may, amongst other things, make---\
  (a) provision as to the educational and training qualifications, and
  other requirements, which must be satisfied before an individual may be
  registered or for an individual to remain registered;\
  (b) provision as to the requirements which must be met by a body
  (corporate or unincorporate) before it may be registered, or for it to
  remain registered, including provision as to the management and control
  of the body;\
  (c) provision as to the educational, training and other requirements to
  be met by regulated persons;\
  (d) provision regulating the practice, conduct and discipline of
  registered persons or regulated persons;\
  (e) provision authorising in such cases as may be specified in the
  regulations the erasure from the register of the name of any person
  registered in it, or the suspension of a person's registration;\
  (f) provision requiring the payment of such fees as may be specified in
  or determined in accordance with the regulations;\
  (g) provision about the provision to be made by registered persons in
  respect of complaints made against them;\
  (h) provision about the keeping by registered persons or regulated
  persons of records and accounts;\
  (i) provision for reviews of or appeals against decisions made under
  the regulations;\
  (j) provision as to the indemnification of registered persons or
  regulated persons against losses arising from claims in respect of
  civil liability incurred by them.
  -----------------------------------------------------------------------

### 275A.03 {#a03}

Subsection (2) sets out the areas which may be covered by the
regulations. These include provisions for regulating the educational and
training qualifications, the requirements which must be met by corporate
or incorporate bodies and the payment of fees by those wishing to be
registered as patent attorneys.

### 275A.04 {#a04}

The regulations may include provisions on the practice, conduct and
discipline of registered persons, including provisions in respect of
complaints made against patent attorneys, the right to reviews and
appeals of any decisions made under the regulations and the provisions
for the removal of the names of patent attorneys from the register.

  -------------------------------------------------------------------------------------
   
  **Section 275A(3)**
  Regulations under this section may make different provision for different purposes.
  -------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 275A(4)**
  Regulations under this section which are not regulatory arrangements within the meaning of the Legal Services Act 2007 are to be treated as such arrangements for the purposes of that Act.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 275A.05 {#a05}

The provisions in the regulations can differ according to their purpose
and can encompass aspects which are not regulatory arrangements within
the meaning of the Legal Services Act 2007.

  ------------------------------------------------------------------------------------------------------------------------
   
  **Section 275A(5)**
  Before the appointed day, regulations under this section may be made only with the approval of the Secretary of State.
  ------------------------------------------------------------------------------------------------------------------------

### 275A.06 {#a06}

The regulations made under this section come into force on the
"appointed day" as defined in subsection (7) below and which is 1
January 2010. Any regulations made under this section prior to the
appointed day (none have been made) may only be made with the approval
of the Secretary of State.

  -----------------------------------------------------------------------
   

  **Section 275A(6)**

  The powers conferred to make regulations under this section are not to
  be taken to prejudice---\
  (a) any other power which the person who keeps the register may have to
  make rules or regulations (however they may be described and whether
  they are made under an enactment or otherwise);\
  (b) any rules or regulations made by that person under any such power.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 275A(7)**

  In this section---\
  "appointed day" means the day appointed for the coming into force of
  paragraph 1 of Schedule 4 to the Legal Services Act 2007;\
  "manager", in relation to a body, has the same meaning as in the Legal
  Services Act 2007 (see section 207);\
  "patent attorney work" means work done in the course of carrying on the
  business of\
  acting as agent for others for the purpose of---\
  (a) applying for or obtaining patents, in the United Kingdom or
  elsewhere, or\
  (b) conducting proceedings before the comptroller relating to
  applications for, or otherwise in connection with, patents; "registered
  person" means---\
  (a) a registered patent attorney, or\
  (b) a body (corporate or unincorporate) registered in the register kept
  under section 275;\
  "regulated person" means a person who is not a registered person but is
  a manager or employee of a body which is a registered person.
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
