::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 71: Declaration or declarator as to non-infringement {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (71.01 - 71.10) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 71.01

This section provides for the making of a declaration or declarator that
an act does not, or a proposed act would not, constitute an infringement
of a patent. Hereinafter the word 'declaration' will generally just be
used, the word 'declarator' being the Scottish term. The procedure
within the Office is prescribed by Part 7 of the Patents Rules 2007.

  -----------------------------------------------------------------------
   

  **Section 71(1)**

  Without prejudice to the court's jurisdiction to make a declaration or
  declarator apart from this section, a declaration or declarator that an
  act does not, or a proposed act would not, constitute an infringement
  of a patent may be made by the court or the comptroller in proceedings
  between the person doing or proposing to do the act and the proprietor
  of the patent, notwithstanding that no assertion to the contrary has
  been made by the proprietor, if it is shown\
  (a) that that person has applied in writing to the proprietor for a
  written acknowledgment to the effect of the declaration or declarator
  claimed, and has furnished him with full particulars in writing of the
  act in question; and\
  (b) that the proprietor has refused or failed to give any such
  acknowledgment.
  -----------------------------------------------------------------------

### 71.02 {#ref71-02}

s\. 60(5) is also relevant.

Section 71 provides for the court or the comptroller to make a
declaration that an act or proposed act does not constitute an
infringement of the patent. The validity of the patent may be put in
issue in s.71 proceedings by virtue of s.74(1)(c), but this should be
specifically pleaded in the statement ([see 71.05](#ref71-05)). However,
in Johnson Matthey PLC v Sumitomo Special Metals Co Ltd BL O/168/92 and
[O/145/94](https://www.gov.uk/government/publications/patent-decision-o14592)
s.71 proceedings were stayed at the applicant's request pending the
outcome of opposition proceedings under the EPC where the validity of
the patent was at issue. In Glaxo Group Ltd v Genentech Inc \[2008\]
EWCA Civ 23, \[2008\] FSR 18, the Court of Appeal set out various
factors (which were updated in [IPcom GmbH & Co Kg v HTC Europe Co Ltd &
Ors \[2013\] EWCA Civ
1496](http://www.bailii.org/ew/cases/EWCA/Civ/2013/1496.html){rel="external"}
and applied in [Actavis Group PTC EHF v Pharmacia LLC \[2014\] EWHC 2265
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2014/2265.html){rel="external"}
which might be useful to consider in deciding whether to stay legal
proceedings on the ground that there are parallel proceedings pending in
the EPO contesting the validity of the patent. For further discussion of
staying proceedings, see the Patents Hearings Manual (paragraphs
2.73-2.76). A declaration can only be granted if the act or proposed act
(i) falls outside the scope of the claim, (ii) only falls within the
scope of claims held to be invalid (since an invalid claim cannot be
infringed), or (iii) is exempted from being an infringement under s.
60(5) [see
60.23-60.28](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-23).
Such a declaration is the only form of relief available under s.71; a
finding of invalidity in such proceedings does not itself cause
revocation (Zeigler's Patent ([BL
O/64/87](https://www.gov.uk/government/publications/patent-decision-o06487))).

### 71.03 {#section-1}

s.74(7) is also relevant.

Where proceedings are pending in the court under any of sections 58, 61,
69, 70-70F, 71 and 72, no proceedings may be instituted before the
comptroller under s.71 without the leave of the court ([see also
71.10](#ref71-10) and
[74.07](/guidance/manual-of-patent-practice-mopp/section-74-proceedings-in-which-validity-of-patent-may-be-put-in-issue/#ref74-07)).

### 71.04 {#ref71-04}

Before an application can be properly launched under s.71, the person
seeking the declaration must apply in writing to the proprietor for a
written acknowledgement to the effect of the declaration claimed and
must furnish them with full particulars in writing of the act in
question. Scarman L J, when considering the need for full particulars in
Mallory Metallurgical Products Limited v Black Sivalls and Bryson
incorporated \[1977\] RPC 321, stated at page 345 that, ".... the
description must be sufficiently clear and precise to enable the court
to declare that an article corresponding with the description would not
constitute an infringement. The burden of proving the absence of
infringement rests, in my judgment, upon the plaintiff. If there be lack
of clarity or precision, the court is not in a position to grant the
declaration sought". As pointed out in British Railways Board's Patent
([BL
O/41/85](https://www.gov.uk/government/publications/patent-decision-o04185)),
it is also essential for any difference between the act and the
invention claimed in the patent to be brought out whether the difference
relates to the absence, or to the replacement, of a feature claimed. In
Minnesota Mining and Manufacturing Co's (Suspension Aerosol Formulation)
Patent \[1999\] RPC 135, Pumfrey J suggested that it would always be
desirable to provide the patentee with a sample or drawings to aid the
description.

### 71.04.1 {#ref71-04-1}

In MMD Design & Consultancy Ltd's Patent \[1989\] RPC 131, a written
description which was inadequate under s.71(1) by itself was held to
suffice when taken together with a drawing. Although the drawing was
only made available to the patentees on, rather than before, the day the
application under s.71 was filed, the hearing officer refused to dismiss
the application as not properly launched, since the practical
consequence of doing so would simply have been the filing of another
application. Following this, in Johnson Matthey PLC v Sumitomo Special
Metals Co Ltd (BL O/168/92) the hearing officer decided that s.71
proceedings had not been properly launched because the claimant had
provided insufficient particulars of a range of alloys, on which a
declaration of non-infringement was sought, for the proprietors to argue
against the declaration and also for the comptroller to determine
whether or not the alloys fell within the scope of the patent. However,
an opportunity was given to clarify the act on which a declaration was
sought. These decisions should now be viewed in the light of Melkris Ltd
v Denman ([BL
O/369/01](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F369%2F01&submit=Go+%BB){rel="external"}),
in which the applicant provided particulars to the proprietors as
required, but then filed an application for declaration of
non-infringement the next working day. The hearing officer made clear
that "this approach is completely at odds with one of the key elements
of the reform of civil justice initiated by the Woolf report....under
which parties are expected to try and settle their disputes first before
resorting to litigation". Because the MMD Design decision (and, on that
basis, Office publications and other guidance) had encouraged parties to
believe that it was perfectly acceptable to defer seeking an
acknowledgement from the proprietor until an application under s.71 was
made, the hearing officer declined to dismiss the application in this
case. However, they indicated that the comptroller may well in the
future dismiss with an appropriate award of costs any application where
the applicant's actions had failed to give adequate opportunity for the
proprietor to give the requested acknowledgement.

### 71.05 {#ref71-05}

PR part 7 is also relevant.

If the proprietor refuses or does not give the acknowledgement sought,
then an application for a declaration may be made to the comptroller
(after grant of the patent) on Patents Form 2 accompanied by a copy
thereof and by a statement of grounds (in duplicate). This starts
proceedings before the comptroller, the procedure for which is discussed
at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).

### 71.06 {#section-2}

r.77(5), r.77(6) and r.78 is also relevant.

The counter-statement in the proceedings may include an application to
amend the specification under s.75 ([see
75.04-75.08](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-04)).

### 71.07 {#ref71-07}

In determining whether an act does not constitute or would not
constitute infringement of the patent regard is had to the provisions of
s.60 and s.64 as to what constitutes infringement. Because s.60 makes
clear that infringement has only occurred if a person has done an act
within the United Kingdom without the consent of the patent proprietor,
the power to make a declaration under s.71 is confined to acts within
the United Kingdom (Plastus Kreativ AB v Minnesota Mining and
Manufacturing Co and anr. \[1995\] RPC 438). In determining whether
there would be infringement in MMD ([see 71.04](#ref71-04)), the hearing
officer took into account a point which was not raised by the applicants
but which emerged directly out of and could be decided solely upon the
evidence.

### 71.08 {#section-3}

s.77(1) is also relevant.

It is also possible for a declaration to be made under s.71 in respect
of a European patent (UK).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 71(2)**
  Subject to section 72(5) below, a declaration made by the comptroller under this section shall have the same effect as a declaration or declarator by the court.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 71.09 {#section-4}

S.72(5) provides that a decision of the comptroller or on appeal from
the comptroller shall not estop any party to civil proceedings in which
infringement of a patent is in issue from alleging invalidity of the
patent on any of the grounds referred to in s.72(1), whether or not any
of the issues involved were decided in the said decision.

### 71.10 {#ref71-10}

However court proceedings for infringement of a patent may be stayed
pending the outcome of proceedings under sections 71 and 72 before the
comptroller (Hawker Siddeley Dynamics Engineering Ltd v Real Time
Developments Ltd \[1983\] RPC 395).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
