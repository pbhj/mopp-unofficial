::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 74A: Opinions on matters prescribed in the rules {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (74A -74A)(6) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 74A.01 {#a01}

Section 74A was introduced by the Patents Act 2004 and came into force
on 1 October 2005. The section provides for a procedure where the
comptroller can issue, on request, non-binding opinions. Initially such
opinions were restricted to questions of validity relating to novelty or
inventive step, and questions of infringement. On 1 October 2014,
section 74A was amended by the Intellectual Property Act 2014, such that
from this date onwards the matters on which an opinion can be requested
are set out in rule 93(6).

\[The Patents Opinions Service Procedures Manual should be consulted for
full details of practice concerning opinions under section 74A and
reviews of opinions under section 74B.\]

  --------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 74A(1)**
  The proprietor of a patent or any other person may request the comptroller to issue an opinion on a prescribed matter in relation to the patent.
  --------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------
   
  **Section 74A(2)**
  Subsection (1) above applies even if the patent has expired or has been surrendered.
  --------------------------------------------------------------------------------------

### Making a request for an opinion

### 74A.02 {#a02}

(r.92, r.93, sch 4A, para 1(2) are also relevant)

Anyone may ask the comptroller for an opinion on infringement or
validity in relation to a UK patent, European patent (UK), or
supplementary protection certificate (SPC), whether in force or not,
except when the patent has been revoked or the SPC declaredinvalid.
However, opinions on infringement or validity are limited to the matters
set out in rule 93(6):

\(a\) whether a particular act constitutes, or (if done) would
constitute, an infringement of the patent;

\(b\) whether, or to what extent, an invention for which the patent has
been granted is not a patentable invention (as set out in s.1(1));

\(c\) whether the specification of the patent discloses the invention
clearly enough and completely enough for it to be performed by a person
skilled in the art;

\(d\) whether the matter disclosed in the specification of the patent
extends beyond that disclosed in the application for the patent as filed
or, if the patent was granted on a a divisional application under
s.15(9) or a new application filed under s.8(3), s.12(6) or s.37(4), in
the earlier application as filed;

\(e\) whether the protection conferred by the patent has been extended
by an amendment which should not have been allowed;

\(f\) whether a supplementary protection certificate is invalid under
Article 15 of the Medicinal Products Regulation; and

\(g\) whether a supplementary protection certificate is invalid under
Article 15 of the Plant Protection Products Regulation.

In the following paragraphs, the term "patent holder" means the
proprietor of the patent and any exclusive licensee.

### 74A.03 {#a03}

(r.93, r.53(2)(b) and r.51 are also relevant)

A request for an opinion should be made on Patents Form 17 filed in
duplicate. This shall be accompanied by a statement setting out the
question upon which an opinion is r.51 sought, submissions from the
requester on that question, and any matters of fact which are requested
to be taken into account. The statement shall be accompanied by the name
and address of anyone, of whom the requester is aware, having an
interest in that question, particulars of any proceedings of which the
requester is aware which relate to the patent and which may be relevant
to that question, and a copy of any evidence or other document which is
referred to in the statement, but there is no need to provide copies of
published UK granted patents or applications. However, when the
requester is acting as an agent in making the request, there is no
requirement for the agent to disclose the person for whom they are
acting. The statement, evidence and any other documents must be provided
in duplicate. Any document filed in connection with a request for an
opinion cannot be kept confidential ([see
118.10](/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-10)).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 74A(3)**
  The comptroller shall issue an opinion if requested to do so under subsection (1) above, but shall not do so (a) in such circumstances as may be prescribed, or (b) if for any reason he considers it inappropriate in all the circumstances to do so.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Refusal or withdrawal of request

r.94, r.106(3) and r.106(4) are also relevant

### 74A-04 {#ref74A-04}

An opinion will not be issued if the request for an opinion appears to
the comptroller to be frivolous or vexatious, or the question upon which
an opinion is sought appears to have been sufficiently considered in any
proceedings. In addition, an opinion is not issued if the requester
withdraws the request by giving notice to the comptroller in writing. In
this situation, the comptroller may remit the whole or part of the fee
paid to request the opinion.

The requester is notified by the comptroller where a request for an
opinion is refused, and has a right to be heard [(see
74A.10)](#ref74A-10). Where the request is refused, the comptroller has
discretion to remit the whole or part of the fee paid to request the
opinion. Under the Patents Rules 2007 there is no longer a requirement
to apply in writing for remission of the whole or part of the fee.

### Notification and advertisement of request

### 74A.05 {#a05}

r.95(1), r.95(2), r.92, r.54(1) and r.54(6)(a) are also relevant.

Various parties are notified of the request for an opinion by being sent
a copy of the statement and Patents Form 17 filed by the requester,
along with any other documents filed by the requester that the
comptroller sees fit to send. The parties notified are (unless they are
also the requester for an opinion): the patent holder (i.e. the patent
proprietor and any exclusive licensee of the patent), any holder of a
licence or sub-licence which has been registered under r.47, any person
who has made a request for information as to when an opinion has been
filed on a patent, any person who is specified in the statement filed by
the requester as having an interest in the question, and any other
person who the comptroller considers to have an interest in the question
upon which the opinion is sought.

### 74A.06 {#a06}

r.95(4) is also relevant.

The request for an opinion is advertised on the Office website. The URL
index for opinions is available in [requests for
opinions](https://www.gov.uk/government/collections/requests-for-opinions).

### 74A.07 {#a07}

r.95(5) is also relevant.

If a request is refused or withdrawn before parties are notified that
the request for an opinion has been filed, only the patent holder is
notified of the request (if they are not also the requester). In this
event, the request for an opinion is not advertised.

### Submission of observations and observations in reply

### 74A.08 {#ref74A-08}

r.96(1)-(3) and r.96(7) are also relevant.

Unless the request for an opinion has been refused or withdrawn, any
person may submit written observations within four weeks from the date
of advertisement of the request on the Office website. This time period
is extendable at the comptroller's discretion under rule 108(1). Within
this period, the person submitting the observations is required to
supply a copy of these observations to the patent holder (except where
the patent holder is the person submitting the observations) and the
requester of the opinion. The observations must be confined to the
issues raised in the request for an opinion, but may include reasons why
the comptroller should refuse the request for an opinion.

### 74A.09 {#a09}

r.96(4)-(5) is also relevant.

Once the period for submitting observations has expired, the requester
and the patent holder (if the patent holder is not the person submitting
the observations in question) have two weeks to submit observations in
reply. This period is extendable at the comptroller's discretion under
rule 108(1). This response must be strictly confined to issues raised in
the observations. Within this period, any observations in reply from the
requester must be sent by them to the patent holder. Similarly, the
patent holder must send any observations in reply to the requester.

\[Any request for an extension of time to submit observations or
observations in reply should be referred to a Deputy Director with
responsibility for the opinions procedure. As the procedure of issuing
an opinion is intended to be quick, only very short extensions will
normally be granted, and only with very good reasons\]

### 74A.10 {#ref74A-10}

r.96(6) is also relevant.

If it is reasonably possible, the observations filed and copies of such
observations should be delivered only in electronic form or using
electronic communications. Directions made under section 124A
prescribing the form and manner for making these observations using
electronic media were published in PDJ No. 6071 on 28 September 2005 and
are reprinted in full in the "Relevant Official Notices and Directions"
section of this Manual. The irections require observations to be sent by
email to address <opinions@ipo.gov.uk> or to be delivered on digital
media.

  -----------------------------------------------------------------------
   
  **Section 74(A)(4)**
  Any opinion under this section shall not be binding for any purposes.
  -----------------------------------------------------------------------

### 74A.11 {#a11}

The opinion does not have binding legal effect on the requester,
observers or anyone else. However, that does not prevent the fact that
an opinion has been given from being referred to in subsequent
proceedings.

### 74A.11.1 {#ref74a-11-1}

[See
73.02](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-02)
and
[73.04.1](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-04-1)
for details of when action may be taken to commence revocation
proceedings under s.73 after an opinion concludes that a granted UK
patent or European patent (UK) lacks novelty or an inventive step.

  -----------------------------------------------------------------
   
  **Section 74(A)(5)**
  An opinion under this section shall be prepared by an examiner.
  -----------------------------------------------------------------

### Issue of the opinion

### 74A.12 {#a12}

r.97 is also relevant.

After the end of the procedure for submission of observations, the
request is referred to an examiner for preparation of the opinion. A
copy of the opinion is sent to the requester, the patent holder, and any
other person who filed observations. The opinion is also made available
in our website guide [requests for
opinions](https://www.gov.uk/government/collections/requests-for-opinions).

\[A senior patent examiner should consider the observations and prepare
the written opinion. The examiner will first see the file about 2 weeks
after the request for an opinion is received and may study the patent
and any observations during the submission period. Further details on
preparing the opinion are given in chapter 5 of the Patents Opinions
Service Procedures Manual. \]

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 74(A)(6)**
  In relation to a decision of the comptroller whether to issue an opinion under this section (a) for the purposes of section 101 below, only the person making the request under subsection (1) above shall be regarded as a party to a proceeding before the comptroller; and (b) no appeal shall lie at the instance of any other person.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 74A.13 {#a13}

The requester of an opinion has the right to be heard before the request
is refused by the comptroller. No other person has the right to be heard
on the question whether the request should be granted. Similarly, no
person other than the requester has the right to appeal to the courts
against that decision (under the general right of appeal given by
section 97).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
