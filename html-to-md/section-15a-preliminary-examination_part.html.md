::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 15A: Preliminary examination {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (15A.01 - 15A.26) last updated July 2022.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 15A: Preliminary examination {#section-15a-preliminary-examination}

### 15A.01 {#a01}

This section sets out the conditions necessary for an application to
proceed to preliminary examination, makes provision for carrying out the
preliminary examination and for reporting the results. Rules 23, 24 and
25 are particularly relevant to this section.

### Information security: a reminder

### 15A.01.1 {#a011}

(s.118(2) is also relevant).

The preliminary examination of UK patent applications is carried out
before A publication, and so most of the guidance in this section
relates to pre-publication actions. Before A publication, the
application, the preliminary examination report, and any other documents
or information concerning the content of the application (other than
that prescribed under s.118(3)) must be protected and must not be
communicated to anyone outside the Office other than the applicant or
their designated representative -- see 118.16- 118.16.1 for further
guidance on these issues.

### 15A.01.2 {#a012}

More generally, in all cases, whether pre-or post-publication,
formalities examiners should ensure that any communications (including
telephone conversations) are directed to the intended recipient.

\[Where correspondence from the Office is reported as never having
arrived at its intended destination, or is reported as being misdirected
or delayed, this fact should be recorded by sending a minute to the
relevant formalities group with any relevant details.\]

  -----------------------------------------------------------------------
   

  **Section 15A(1)**

  The comptroller shall refer an application for a patent to an examiner
  for a preliminary examination if -\
  \
  (a) the application has a date of filing,\
  (b) the application has not been withdrawn or treated as withdrawn;
  and\
  (c) the application fee is paid.
  -----------------------------------------------------------------------

### 15A.02 {#a02}

s.15(10)(c) is also relevant

The payment of the application fee will initiate preliminary
examination. There is provision to pay the application fee on Form 1 and
on Form 9A although it is not a requirement for the fee to be filed with
these forms. If the application fee is paid independently of these forms
it is suggested that it be accompanied by non-statutory form AF1.

### 15A.03 {#ref15A-03}

The time allowed for filing the application fee is prescribed in
r.22(7), (see 15.51 - 15.53). The preliminary examination should be
carried out by the formalities examiner as soon as possible after the
application fee has been paid and a letter should be issued [(see
15A.20)](#ref15A-20) if there are any outstanding objections.

\[ The Formalities Group Manager should allocate cases on which a search
fee has been filed to the appropriate classification heading and forward
these cases to the formalities group associated with that heading. \]

\[ The formalities examiner making the preliminary examination should
complete the appropriate parts of the formalities checklist and enter
any appropriate amplifying comments in a minute added to the dossier. If
a procedural requirement is outstanding, a letter should be sent to the
applicant. A report in a minute added to the dossier may also be
addressed to a specific person, for example the search examiner, and may
be by way of explanation or reminder. \]

\[ When, before publication, all the formal requirements have been met,
the formalities examiner should add the appropriate action to the
dossier. \]

### \[ Private applicants' cases \] {#private-applicants-cases-}

\[Any application filed without the services of an agent should be
identified by addition of the appropriate label to the cover of the
dossier. The formalities examiner should normally issue an LFEPA (if the
application fee has been filed) letter immediately, but should consult
the Private Applicant Unit (PAU) first if the application is considered
to be a 'no case' (i.e. an application which cannot be given a filing
date due to lack of technical description).

\[Where an application is identified as a 'no case', the PAU examiner
will issue a 'No Case Letter', informing the applicant of the
requirement to file a technical description in order to establish a
filing date. Where fees have been filed with such an application, the
unspent fees should be refunded and the applicant informed either by
phone or in the 'No Case Letter'. PAU desk notes provide further
guidance on the process.

[More information on preliminary examination processes for private
applicants is provided in Chapter 15 of the Formalities
Manual.](https://www.gov.uk/government/publications/patents-formalities-manual)\]

  -----------------------------------------------------------------------
   

  **Section 15A(2)**

  On a preliminary examination of an application the examiner shall -\
  \
  (a) determine whether the application complies with those requirements
  of this Act and the rules which are designated by the rules as formal
  requirements for the purposes of this Act; and\
  \
  (b) determine whether any requirements under section 13(2) or 15(10)
  above remain to be complied with.
  -----------------------------------------------------------------------

### 15A.04 {#ref15A-04}

(r.23(1) is also relevant).

The formal requirements are those designated by r.25(1), namely the
requirements of r.12(1) and r.14(1)-(3). The formalities examiner should
therefore establish whether a properly-completed Form 1 has been filed
(see 14.04 - 14.25) indicating an address for service (see 14.04.13),
and whether or not the drawings and other documents comply with the
formal requirements of r.14 (see 14.26-14.57). The preliminary
examination should also establish whether action under s.15(3) is
necessary, and whether the requirements of rr.6-9 [(see 15A.13
-15A.17)](#ref15A13-15A17) and rr.10(1) and 10(2) [(see 15A.11 -
15A.12)](#ref15A11-15A12) have been met. If any requirements in relation
to s.13(2) or 15(10) are outstanding these should also be determined. If
it is established as a result of this preliminary examination that there
is no outstanding objection, the application should be reported
"Formalities complied with for A-publication".

(r.13(2)-(7) is also relevant).

\[ Where the specification of a patent application concerning biological
material discloses a sequence, the formalities examiner should also
check that it includes a sequence listing, in accordance with rule
13(2). If a sequence listing has not been provided then the preliminary
examination report should specify a period of 2 months from the date of
the report within which the applicant must provide the sequence listing.

If a sequence listing has been filed, but is found to be non-compliant
with WIPO standard ST26, then the preliminary examination report should
specify a period of 15 months from the earliest date of the application
within which the applicant must provide a replacement sequence listing.
Although neither of these are formal requirements designated by r.25(1),
the comptroller may refuse the application if the sequence listing is
not provided within the period specified.

If there is any doubt as to whether an application falls under these
circumstances then the formalities examiner should consult the
examiner.\]

### The specification

### 15A.05 {#ref15A-05}

If the pages of text of the specification do not conform with the formal
requirements of r.14, the formalities examiner should itemise the
outstanding requirements [(see 15A.20)](#ref15A-20).

\[ In view of the propensity of some private applicants to add matter or
even totally redraft a specification, objection under r.14 against a
private applicant's specification should not normally be raised. If
unavoidable, any necessary typing or other essential remedial action for
the purposes of s.16 publication may be undertaken in the Office unless
this is clearly impossible (see 14.30). Where such remedial action has
been taken, the appropriate notice should be inserted on the front page
(see 16.29). The applicant should be advised of the retype and a copy of
the typed pages included.\]

### 15A.06 {#ref15A-06}

(r.14(3) is also relevant).

If the drawings are formal, that is to say, they comply with the formal
requirements, then, provided they were present on the filing date of the
application, they should be annotated appropriately in the TOC. Any
additional copies should also be annotated appropriately. (For the
procedure when formal drawings are filed late following the initial
filing of informal drawings, see [15A.22](#ref15A-22); for the case
where a drawing not present in any form on the filing date is
subsequently filed, see [15A.23](#ref15A-23)).

### 15A.07 {#ref15-07}

If the drawings are informal, that is, if they fail to comply with the
formal requirements of r.14, a report to this effect should be made,
detailing the deficiency unless it is clear that the drawings filed were
intended only as an informal version. Informal drawings should be
annotated appropriately in the TOC.

### 15A.08 {#a08}

\[deleted\]

### 15A.09 {#ref15A-09}

A drawing containing formulae which appear in the text of the
specification is rarely needed. If such a drawing has been filed but is
not to be used in publishing the A document, no action is necessary and
it should be left in the correspondence part of the file. If the drawing
is required, if it complies with the Rules it should be annotated
appropriately. Such drawings do not need to be in duplicate, and any
further copies should also be annotated appropriately. Any discrepancy
between the drawing and the formulae in the text should be drawn to the
attention of the search examiner. If, in the search examiner's opinion
the text is wrong, and the drawing correct, and the drawing was present
on the date of filing the application, no action should be taken before
substantive examination; the drawing is used in preparing the published
application and may be used for the abstract. If the search examiner
considers that the text is correct and the drawing wrong, or cannot form
an opinion, the applicant should be asked to provide a drawing agreeing
with the text. (If formula drawings were supplied later than the filing
date of the application, see [15A.22](#ref15A-22)).

\[ If the search examiner is sure that, where there is a discrepancy,
the text is correct and the drawing wrong, they may correct the drawing
and so inform the applicant. Publishing Section should be given suitable
instructions where a formula drawing is to be used in producing the A
document.\]

### 15A.10 {#ref15A-10}

(r.25(3) is also relevant).

If the application is an international application (UK) the formal
requirements of rr.12(1) and 14 are regarded as having been met if the
requirements of the corresponding rules of the PCT have been met. Rule
12(1) is equivalent to r.53.1 PCT and r.14 is equivalent to r.11 PCT.
According to section 81(3)(c), any document filed with the European
Patent Office under provisions of the EPC corresponding to sections
2(4)(c), 5, 13(2) or 14 shall be treated as having met the requirements
of those sections. Rule 12(1) is equivalent to r.41(1) EPC and r.14 is
equivalent to r.45(1),(2),(3) and r.49 EPC.

### Form 7

### 15A.11 {#ref15A11-15A12}

(r.10(4) is also relevant).

The formalities examiner should determine whether the questions in part
7 of Form 1 (see 14.04.16) have been properly completed, and, if
correctly completed should report whether Form 7 has been filed,
properly completed and with any necessary further copies (see 13.15).

### 15A.12 {#ref15A-12}

(r.10(3) is also relevant).

If it is found that Form 7 is outstanding or if there appears to be a
defect in the Form, and the end of the 16 month period (see 13.11) is
near, the applicant or agent should be notified at once. If an applicant
is required to file a Form 7 on the basis of the answers to the
questions in part 7 of the form 1 and the applicant fails to file Form 7
within the prescribed period the application is taken to be withdrawn
(see 13.13). The prescribed period in the case of a divisional
application is determined by r.21 - see 15.26).

### Priority documents

### 15A.13 {#ref15A-13}

(r.24(1), s.120(2), r.7, r.24(2)-(3) are also relevant).

If a declaration of priority has been made at part 5 of Form 1 (see
14.04.14) the formalities examiner should determine whether the filing
date of the application is within the period of 12 months from any
priority date so declared (always remembering that if the period of 12
months expired on an excluded day or on a day which is certified as one
on which there was an interruption under r.110(1), see 123.43, the
period is extended to include the first following day which is not
excluded or certified, and the period may also be extended under r.111,
see 123.46-47). If it is not, the applicant should be informed that the
claim to priority is not valid and be advised if a request for late
declaration of priority under s.5(2B) may be applied for. Where it is
not possible to claim the priority date declared, the applicant must
provide a corrected date for which a valid priority claim is possible
within two months of being notified by the formalities examiner,
otherwise the declaration will be disregarded.

### 15A.14 {#a14}

(r.112 r.8(2), PR Sch. 4, Part 2 are also relevant).

The formalities examiner should determine whether the file number of the
or each application from which priority is claimed has been supplied,
either on Form 1 or subsequently. If a copy of the priority claim
application is available to the office the formalities examiner should
obtain a copy of the application for the dossier [(see
15A.18-15A.19)](#ref15A-18-15A-19). Where the priority claim application
is not available the formalities officer should determine if a certified
copy of the application has been filed within the prescribed period of
16 months (extendible in accordance with r.108(2) or (3) and (4) to (6),
see 123.34-41) from the earliest declared priority date. Where priority
is claimed from more than one application, all the certified copies must
be filed within this period. Where no certified copies of the
application have been filed the formalities examiner should report this
and should also report if an application from which priority is claimed
is not an application for a patent under the Act, an international
application filed at the Office or any other application supplied to the
Office in support of a declaration of priority on another application
made under the Act \[(see 15A.20)\]. (For the periods allowed for filing
priority documents when the application in suit is one for a European
Patent (UK) converted under s.81, or is an international application see
5.10; for the periods in respect of a divisional application see
\[15.23\].

### 15A.15 {#a15}

(r.8(1) is also relevant).

When such a copy is filed, it must be established that it is of a
"relevant application" (see 5.30) from which priority can legitimately
be claimed, and that the document is consistent with the details given
on Form 1. If there is a discrepancy an explanation should be sought
from the applicant. If there is an error in the declaration of priority
on Form 1, an application to correct it should normally be made in
writing. If the wrong priority document has been filed, or if details on
it are clearly incorrect, a replacement (duly certified) should be
requested and must be filed before the end of the prescribed period; if
it is filed later, a written request for its admission as a correction
under s.117 should be filed.

\[ If there is a discrepancy between the declaration of priority on Form
1 and the priority documents the agent should be contacted by telephone
(followed by written confirmation) to establish where the error lies. If
only the file number on Form 1 is wrong then it may be corrected in the
office, provided that the error is notified within 16 months of the
earliest declared priority date, since the applicant is allowed to
supply the file number at any time within this period. If the fault lies
in the priority document and the prescribed period has already expired,
the matter must be referred via the Formalities Manager to the
divisional Head of Admin.\]

### 15A.16 {#ref15A-16}

If a page of text or drawing is apparently missing from the priority
document the formalities examiner should ask the applicant whether or
not the document is in agreement with the application, of which it
purports to be a copy, as filed. If it is, no further action is
necessary. If it is not, a replacement (duly certified) should be
requested if the prescribed period has not expired. If that period has
expired, the applicant can seek the admission of such a replacement as a
correction under s .117.

\[ If the priority document is deficient and the prescribed period has
already expired, the matter must be referred via the Formalities Manager
to the divisional Head of Administration. \]

### 15A.17 {#a17}

s.5(2), r.3 r.26(1) are also relevant

If, after 18 months from the earliest declared priority date, any
required certified copy of a priority application has not been filed,
the claim to priority in respect of that application is relinquished and
the application will then proceed with its own filing date as priority
date, or, where not all of several declared dates have been
relinquished, with the earliest remaining priority date. Time limits and
other dates, including the date on which publication is due, are
reckoned from the new priority date. The applicant must be notified of
the loss of any declared claim to priority and of the reason for this.
Even if a claim to priority is relinquished, any related priority
documents should be kept in the open part of the file so as to be
available for inspection under s.118.

\[ The appropriate standard letter should be issued warning of the
intention to cancel the declaration or date of priority, and giving two
weeks for any observations which might justify extension of the relevant
period under r.108(3). In the normal case, after the two weeks has
ended, the application should be referred with an explanatory minute to
the Formalities Manager. The Formalities Manager will authorise the
cancellation of the relevant priority details, annotate Form 1
appropriately and add an explanatory minute to the PDAX dossier and
carry out, or arrange for, the appropriate COPS action. On a PCT
application, the Formalities Manager will add an explanatory minute to
the PDAX dossier and amend the WIPO print and NP1 and carry out, or
arrange for, the appropriate COPS action.

The formalities examiner must also inform the applicant by issuing
LFH.\]

\[Deleted\]

\[ A minute on the PDAX dossier should direct the search examiner's
attention to the new priority date. \]

### Priority claimed from an application filed at the Office

### 15A.18 {#ref15A-18}

If priority is claimed from a UK application, an international
application filed at the Office or any other application supplied to the
Office in support of a declaration of priority in a UK application, and
the UK or international application has been withdrawn by the applicant
or has been treated as withdrawn because the period prescribed by
r.22(2), (together with a two month extension possible under r.108(2))
has expired without Form 9A and the search fee being filed, documents
may be transferred from the earlier application to serve as priority
documents for the application in suit. When priority is claimed from an
earlier UK application itself only documents present on the filing date
of the earlier application may be so transferred; in particular, later
filed claims or late-filed drawings may not be transferred. A full set
of these documents must remain on the file of the earlier application.

\[ In the case of a UK application the documents to be transferred are a
copy of the Form 1 (prepared by the formalities examiner) and a copy of
all other documents (and only those documents) submitted on the filing
date of the earlier application. Care must be taken to ensure that there
still remains on the file of the earlier application a complete set of
documents representing copies of those transferred. Thus, in accordance
with the above:-

the claims, abstract and drawings thus transferred must be those
submitted on the application date. If none was filed on the application
date no such transfer can occur; claims, abstract, or drawings filed
after the application date (including formal drawings following
initially-filed informal drawings) must not be transferred.

\[ When documents are transferred from one application to another, the
Dossier Action Log must be updated on each dossier. The TOC should be
annotated to advise the examiner as to whether the/each priority case
has been searched.\]

### 15A.19 {#a19}

\[deleted\]

  ---------------------------------------------------------------------------------------------
   
  **Section 15A(3)**
  The examiner shall report to the comptroller his determinations under subsection (2) above.
  ---------------------------------------------------------------------------------------------

### 15A.20 {#ref15A-20}

s.15A(2)(b) r.23(2) are also relevant

A letter should be issued following the preliminary examination setting
out any objections arising therefrom. If the specification does not
comply with r.14, or if any other formal requirement has not been met,
the report should set out the precise grounds of objection. If it is
clear that drawings which are informal are only intended as such, a
statement that formal drawings should be filed will suffice. The
applicant should be requested to rectify these deficiencies within 15
months of the priority date or, where there is none, of the filing date.
If this period has already or nearly expired a period of two months
should be allowed for compliance. If any other matters are outstanding,
for example, if a priority document has not been filed and the time for
filing it has not expired, or if Form 7 is not yet on file, a reminder
to this effect should be included in the report.

\[ It is usual for any objection arising out of the preliminary
examination to be conveyed to the applicant by standard paragraphs and
issued directly by the formalities examiner. If however the
circumstances demand it, any outstanding procedural requirements may be
communicated to the applicant by "first" letter or by telephone followed
by a confirmatory telephone conversation report. A first letter must be
authorised by the Formalities Manager and will issue under their name.
If, exceptionally, an outstanding formal requirement is conveyed for the
first time by a communication using other than standard paragraphs, that
communication must specify a period in which the applicant should comply
with the requirement: it must also state that unless the applicant
complies within the period specified, the comptroller may refuse the
application after giving the applicant an opportunity to be heard in the
matter. \]

\[ A copy of a standard letter, of a telephone conversation report or of
a first letter must be made available on the "open to public inspection"
part of the file. \]

### 15A.21 {#a21}

When replacement pages of text are filed, the formalities examiner
should check that they are in exact conformity with those originally
filed and that they comply with the formal requirements and should
incorporate them into the application. The published application should
carry a notice (see 16.29) stating that it takes account of replacement
documents filed later than the application. If, however, the original
text was illegible (as distinct from merely difficult to read) the
matter should be referred to the search examiner (see 16.09).

\[ If the replacement pages are acceptable, new versions of the relevant
documents should be assembled for the dossier. The latest versions of
the claims and description should be annotated appropriately. See also
[15A.22](#ref15A-22). \]

### 15A.22 {#ref15A-22}

When formal drawings are filed, whether or not in response to an
Official request, in a case where the drawings present on the filing
date were informal, they should be checked by the formalities examiner
for conformity with the informal drawings. Any discrepancy which appears
prima facie (that is, without requiring undue investigation) to be
material should be resolved before publication. Otherwise, the matter
can be deferred for resolution at the substantive examination stage (see
also 18.08). The consideration of discrepancies in formal drawings is a
matter for the search examiner. The drawings should be annotated as in
[15A.06](#ref15A-06) (the originally-filed informal drawings being
regarded as a third copy). The published application should bear a
notice (see 16.29) stating that the original drawings were informal and
that the published drawings have been prepared from a later-filed formal
copy. If formula drawings (see [15A.09)](#ref15A-09) have been filed
later than the filing date of the application and are to be used in
producing the published application the formalities examiner should
check that they agree with the formulae in the specification, any
discrepancy being reported to the search examiner, and instructions for
publication should be provided to include a notice that such drawings
were filed late.

\[ When replacement formal drawings are received the formalities
examiner should check that they are identical to the informal drawings
on file. Once the formalities examiner is satisfied that there is no
discrepancy between the drawings and that all other requirements are
met, the application should be sent for publication at the appropriate
time without further referral to the search examiner (assuming the
search report has issued). However, the search examiner should be
consulted if the formalities examiner is unsure as to whether the later
filed formal drawings are identical to those originally filed or has any
doubt that what purportedly has been filed are formal or replacement
drawings. Responsibility for deciding to raise the matter of any
discrepancy with the applicant or agent will then rest with the search
examiner, but it is the role of the formalities examiner to request
replacement drawings if the search examiner determines they are needed.
If the search examiner decides to defer until substantive examination
the question as to whether any such discrepancy is material, they should
add a minute to the dossier to alert the substantive examiner to the
situation. \]

(r.109 is also relevant).

\[ If formal drawings or replacement pages are filed outside the period
specified, an extension of time may be allowed. An automatic extension
of two months to the specified period can be allowed on receiving a
request in writing before the end of the period as extended. If the
period has already been extended by two months a further extension of
time may be allowed but this request must be accompanied by an
explanation of the cause of delay. Any such request should be referred
to the Formalities Manager for consideration. Any request made to extend
a specified period after the two month automatic extension period has
expired should also be referred to the Formalities Manager\]

  -----------------------------------------------------------------------
   

  **Section 15A(4)**

  If on the preliminary examination of an application it is found that-\
  \
  (a) any drawing referred to in the application, or\
  (b) part of the description of the invention for which a patent is
  sought\
  \
  is missing from the application, then the examiner shall include this
  finding in his report under subsection (3) above.
  -----------------------------------------------------------------------

### 15A.23 {#ref15A-23}

(r.18(2) s.15(6)(b) are also relevant).

The formalities examiner should check during the first preliminary
examination that all drawings referred to in the description or claims
were present (in either formal or informal versions) on the filing date
of the application and that all the pages of the description are
numbered consecutively. If such a drawing is missing or it appears that
a page is missing this should be reported in the preliminary examination
report. The report should state that the missing part should be filed
within two months of the date of notification. Subsequent filing of
missing drawings or pages will lead to the application being re-dated
unless there is a claim to priority and an application to avoid
re-dating is allowed see ([15.07](#ref15-07) to [15.16](#ref15-07)).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 15A(5)**
  Subsections (6) to (8) below apply if a report is made to the comptroller under subsection (3) above that not all the formal requirements have been complied with.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 15A(6)**

  The comptroller shall specify a period during which the applicant shall
  have the opportunity-\
  \
  (a) to make observations on the report, and\
  (b) to amend the application so as to comply with those requirements
  (subject to section 76 below)
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 15A(7)**
  The comptroller may refuse the application if the applicant fails to amend the application as mentioned in subsection (6)(b) above before the end of the period specified by the comptroller under that subsection.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 15A(8)**

  Subsection (7) above does not apply if-\
  \
  (a) the applicant makes observations as mention in subsection (6)(a)
  above before the end of the period specified by the comptroller under
  that subsection, and\
  (b) as a result of the observations, the comptroller is satisfied that
  the formal requirements have been complied with.
  -----------------------------------------------------------------------

### 15A.24 {#a24}

If an applicant has been notified at least once that a requirement
designated as formal by r.25(1) has not been complied with, and no
satisfactory response has been received within the period specified
[(see 15A.20)](#ref15A-20), a report should be issued stating that
unless the applicant submits observations or requests a hearing within
one month the application will be refused under s.15A(7). If no
satisfactory response is received within this period, or if, following a
hearing, the matter is decided against the applicant, a decision

should be issued formally refusing the application

\[ If the period specified in FL1 has elapsed without a satisfactory
reply having been received, a letter drafted in some such terms as the
following should be issued:-

> The .................... requested by .................... have not
> been filed. The Comptroller will refuse your patent application unless
> by .................... (i) you file the requested
> .................... with a full explanation why they were not filed
> on time; or (ii) you make observations; or (iii) you request the
> opportunity to present your case, in person if you choose, to a senior
> official at the Office.

Care must be taken when preparing the letter, using the above as a base,
to ensure that the correct requirement is inserted. The official letter
must be authorised by the Formalities Manager. Where the formal
requirement remains outstanding after expiry of the period specified in
the Official Letter issued as above, the application is refused. Where
the refusal is undisputed, a decision of refusal should be prepared and
submitted through the Formalities Manager to the divisional Head of
Admin for approval and signature. Cases where refusal is disputed should
be submitted through the Formalities Manager and divisional Head of
Administration to the appropriate Senior Legal Adviser in Legal Section
for a hearing. \]

  -----------------------------------------------------------------------
   

  **Section 15A(9)**

  If a report is made to the comptroller under subsection (3) above-\
  \
  (a) that any requirement of section 13(2) or 15(10) above has not been
  complied with; or\
  \
  (b) that a drawing or part of the invention has been found to be
  missing,\
  \
  then the comptroller shall notify the applicant accordingly.
  -----------------------------------------------------------------------

### 15A.25 {#a25}

r.10(3), r.22(7) are also available

Any requirements of section 13(2) or 15(10) which have not been complied
with within the prescribed periods (as extendible under r.108(2)) will
be reported to the applicant in warning letter WR4. The formalities
manager should issue the letter at the appropriate time.

### 15A.26 {#a26}

Failure to respond to any finding under s.15A(4) that a part of the
application appeared to be missing at the time of filing will result in
a letter being issued by the formalities manager stating that the
missing part has not been filed and that the application will proceed in
the form that it was originally filed.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
