::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 77: Effect of European patent (UK) {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (77.01 - 77.25) last updated October 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 77.01

This section provides for a granted European patent (UK) to be treated
like a granted domestic patent. It further relates to the effects on a
European patent (UK) of certain proceedings under the EPC (involving
amendment, revocation, restoration or determination of the validity of
the patent).

### 77.02

s.130(1) is also relevant.

European patent (UK) means a patent granted under the EPC designating
the United Kingdom as a country in which protection is sought for the
invention which is the subject of the patent. The EPC is the European
Patent Convention, ie the Convention on the Grant of European Patents.

  -----------------------------------------------------------------------
   

  **Section 77(1)**

  Subject to the provisions of this Act, a European patent (UK) shall, as
  from the publication of the mention of its grant in the European Patent
  Bulletin, be treated for the purposes of Parts I and III of this Act as
  if it were a patent under this Act granted in pursuance of an
  application made under this Act and as if notice of the grant of the
  patent had, on the date of that publication, been published under
  section 24 above in the journal; and\
  \
  (a) the proprietor of a European patent (UK) shall accordingly as
  respects the United Kingdom have the same rights and remedies, subject
  to the same conditions, as the proprietor of a patent under this Act;\
  \
  (b) references in Parts I and III of this Act to a patent shall be
  construed accordingly; and (c) any statement made and any certificate
  filed for the purposes of the provision of the convention corresponding
  to section 2(4)(c) above shall be respectively treated as a statement
  made and written evidence filed for the purposes of the said paragraph
  (c).
  -----------------------------------------------------------------------

### 77.03 {#ref77-03}

A European patent (UK) granted under the EPC is thus treated as if it
were a patent granted under the Patents Act 1977, and the proprietors of
the patent have the same rights and remedies as the proprietors of a
domestic patent. The effective date of grant from which it is so treated
is the date of mention of the grant in the Bulletin published under the
EPC.

### 77.03.1 {#ref77-03-1}

In [Virgin Atlantic Airways Ltd v Jet Airways (India) Ltd & Ors \[2013\]
EWCA Civ
1713](http://www.bailii.org/ew/cases/EWCA/Civ/2013/1713.html){rel="external"}
the Court of Appeal considered whether the UK had jurisdiction over the
decisions of the EPO for the purposes of Article 1 of the European
Convention on Human Rights (ECHR). The court held that the EPO is
factually and legally independent of the IPO and since s.77(1) makes
European patents directly effective in domestic law from the date of
publication in the Bulletin the Comptroller's function in the process is
purely administrative. It was considered that the recognition of the
validity and effect of European patents by virtue of s.77 is not
sufficient to create a jurisdictional link. Consequently the automatic
recognition of grant of an EP(UK) is not subject to qualification by the
ECHR, and the courts and the Comptroller do not have a general power to
review the validity of patents granted by the EPO on grounds other than
those specified in the EPC (in this case it was claimed that the EP(UK)
was not valid as the UK designation was the result of a procedural error
at the EPO - [see also
72.03](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-03)).
The court went on to say that to allow such a power would fatally
undermine the whole system for the grant of European patents.

### 77.04 {#section-2}

a.86 EPC, r.37(4) and r.38(1) are also relevant.

The term of a European patent (UK) is twenty years measured from the
date of filing accorded to it under Rule 40 of the EPC. Fees to keep it
in force in respect of years up to and including the year in which
mention of grant is published are paid direct to the EPO. Annual renewal
fees should be paid to the UK Office in order to maintain the patent
after the end of the year of grant or, if it is granted before the end
of the fourth year of its term, after the end of that fourth year (for
procedure, see
[25.06](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-06)
and
[25.08-25.11](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-08)).

### 77.05 {#ref77-05}

r.103 and r.39 are also relevant.

The proprietors of a European patent (UK) may provide an address for
service within the United Kingdom (including the Isle of Man), Gibraltar
or the Channel Islands. A convenient time for proprietors to notify the
Office of this address is as soon as it becomes apparent that a European
patent application is going to be granted, which will usually be on
receipt of the notice issued by the EPO under EPC rule 71(3). If,
however, the comptroller is not notified of an address for service for
the proprietor of a European patent (UK), then the proprietor's address
on the register will be treated as the address for service, even if that
address is outside the UK, Gibraltar or the Channel Islands ([see
77.05.1 below](#ref77-05-1). Where no other address is provided, this
address will be used for issuing any notice under rule 39(2) reminding
the proprietor of overdue renewal fees ([see
25.08](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-08))
unless an address specifically for this purpose has been given on Form
12 at a previous renewal ([see
25.12](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-12)).
Notification to the Office of an address for service can be made on Form
51 appointing an agent in the UK, or by letter. If this letter or form
is filed in duplicate, receipt is acknowledged by returning the copy.
The publication (serial) number of the European patent (UK) should be
clearly stated in the notification.

### 77.05.1 {#section-3}

r.103, r.104, PR Sch. 4 and Parts 2 & 3 are also relevant.

Any person, including the proprietor of a European patent (UK), who
makes any application, reference or request relating to any proceeding
under the Patents Act 1977 or Patents Rules 2007, or who gives any
notice of opposition under the Patents Act 1977, or who opposes such an
application, reference, request or notice must furnish the Office with
an address for service in the United Kingdom (including the Isle of
Man), Gibraltar or the Channel Islands. The payment of renewal fees is
not considered to amount to proceedings under the Patents Act 1977 (as
amended) or the Patents Rules 2007 (as amended). If a person fails to
provide such an address for service, and there is sufficient information
to contact the person, they should be contacted and be directed to file
an acceptable address for service within 2 months (this period can be
extended using rule 108(2) and 108(3) - [see
123.36.5](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36-5)).
If an acceptable address is not provided within this time period, or
there is insufficient information to make contact with anyone to provide
such an address, the person is taken either to have failed to initiate
or to have withdrawn from proceedings as appropriate.

### 77.06 {#section-4}

Where the invention to which a European patent (UK) relates was
disclosed at an international exhibition within the six months preceding
the filing of the European patent application, the statement and
certificate filed under EPC Article 55 and EPC rule 25 are treated as
the statement and evidence required under s.2(4)(c) and r.5, [see
2.40](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-40).

### 77.07 {#section-5}

r.31(1) and EPC are also relevant.

In accordance with paragraphs 2 and 3 of Schedule 1 of the Patents Rules
2007, if the invention to which a European patent (UK) relates involves
the use of or concerns biological material, the relevant disclosure is
regarded as clear enough and complete enough for the invention to be
performed by a person skilled in the art if the relevant provisions of
the Implementing Regulations to the EPC have been complied with, and the
biological material has been deposited, on or before the date of filing
of the application, in a depositary institution which is able to furnish
subsequently a sample of the biological material, and the specification
of the application as filed contains such relevant information as is
available to the applicant on the characteristics of the biological
material. See also
[125A.07](/guidance/manual-of-patent-practice-mopp/section-125a-disclosure-of-invention-by-specification-availability-of-samples-of-biological-material/#ref125A-07).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 77(2)**
  Subsection (1) above shall not affect the operation in relation to a European patent (UK) of any provisions of the European Patent Convention relating to the amendment or revocation of such a patent in proceedings before the European Patent Office.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 77.08 {#ref77-08}

a.99 EPC and a.101 EPC are also relevant.

Even though a European patent (UK) is treated as if it were a patent
under the 1977 Act, it is still subject to revocation, including
amendment, proceedings before the EPO. Notice of opposition to a
European patent may be given to the EPO within nine months of the
mention of its grant, and the opposition applies in all the contracting
states in which the patent has effect. This can lead to revocation of
the patent or its maintenance in amended or unamended form, whichever is
decided by the EPO.

\[ If proceedings before the comptroller are initiated in respect of a
European patent (UK) which is the subject of opposition proceedings
before the EPO, it is important to be aware of the progress of the
opposition. This can be monitored through the online file and register
inspection facilities available through the [European Patent
Register](https://register.epo.org/espacenet/regviewer){rel="external"}.
Queries about using this inspection facility should be raised with the
European Patent Filing Department on ext 4636.\]

### 77.09 {#section-6}

s.77(4) and(4A) are also relevant.

A European patent (UK) amended or revoked under the EPC is treated as if
it had been amended or revoked under the Act.

  -----------------------------------------------------------------------
   

  **Section 77(3)**

  Where in the case of a European patent (UK)\
  \
  (a) proceedings for infringement, or proceedings under section 58
  above, have been commenced before the court or the comptroller and have
  not been finally disposed of, and\
  \
  (b) it is established in proceedings before the European Patent Office
  that the patent is only partially valid,\
  \
  the provisions of section 63 or, as the case may be, of subsections (7)
  to (9) of section 58 apply as they apply to proceedings in which the
  validity of a patent is put in issue and in which it is found that the
  patent is only partially valid.
  -----------------------------------------------------------------------

### 77.10 {#ref77-10}

In proceedings under the Act relating to infringement of, or Crown use
of an invention protected by, a European patent (UK), the effect of a
finding of partial validity before the EPO is the same as that of a
finding of partial validity under the Act. The provisions of ss.58(7) to
(9) (relief for Crown use of invention of patent found to be only
partially valid) and 63 (relief for infringement of patent found to be
only partially valid) thus apply regardless of which of these ways
partial validity was found. Section 77(3) was amended by the CDP Act to
make it clear that its provisions apply to Crown use proceedings under
s.58 as well as to infringement proceedings.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 77(4)**
  Where a European patent (UK) is amended in accordance with the European Patent Convention, the amendment shall have effect for the purposes of Parts I and III of this Act as if the specification of the patent had been amended under this Act; but subject to subsection (6)(b) below.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 77(4A)**
  Where a European patent (UK) is revoked in accordance with the European Patent Convention, the patent shall be treated for the purposes of Parts I and III of this Act as having been revoked under this Act.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 77.11 {#section-7}

See [77.08-09](#ref77-08-09).

  -----------------------------------------------------------------------
   

  **Section 77(5)**

  Where\
  \
  (a) under the European Patent Convention a European patent (UK) is
  revoked for failure to observe a time limit and is subsequently
  restored or is revoked by the Board of Appeal and is subsequently
  restored by the Enlarged Board of Appeal; and\
  \
  (b) between the revocation and publication of the fact that it has been
  restored a person begins in good faith to do an act which would, apart
  from section 55 above, constitute an infringement of the patent or
  makes in good faith effective and serious preparations to do such an
  act;\
  \
  he shall have the rights conferred by section 28A(4) and (5) above, and
  subsections (6) and (7) of that section shall apply accordingly.
  -----------------------------------------------------------------------

### 77.12 {#ref77-12}

Where under the EPC a European patent (UK) is revoked for failure to
observe a time limit and subsequently restored or is revoked by the
Board of Appeal and is subsequently restored by the Enlarged Board of
Appeal, the exemption of third parties from infringement proceedings
provided by s.28A applies to acts or preparations in the intervening
period. This includes acts of Crown use which, but for s.55, would
constitute infringement.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 77(5A)**
  Where, under the European Patent Convention, a European patent (UK) is revoked and subsequently restored (including where it is revoked by the Board of Appeal and subsequently restored by the Enlarged Board of Appeal), any fee that would have been imposed in relation to the patent after the revocation but before the restoration is payable within the prescribed period following the restoration.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 77.12.1 {#ref77-12-1}

r.41A and r.36 are also relevant.

This subsection was introduced on 1 October 2014 by the Intellectual
Property Act 2014. It relates to the situation where a European patent
(UK) has been revoked by the EPO Board of Appeal and is subsequently
restored by the Enlarged Board of Appeal following a petition for review
under Art.112a EPC. Unlike other forms of appeal, where a petition for
review has been filed this does not have suspensive effect, meaning that
the patent remains revoked whilst the petition for review is being
considered. Renewal fees cannot be paid when a patent is revoked.
Section 77(5A) and rule 41A introduce a clear requirement for any
renewal fees which fell due whilst the patent was revoked to be paid
within a two-month period following the restoration. As with any other
renewal fees, Patents Form 12 should accompany any payment.

### 77.12.2 {#section-8}

s.25(5) and r.39 are also available.

[See
25.12-15](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-12)
for the actions taken should any outstanding renewal fees not be paid in
the two-month period following the restoration, and for the consequences
of nonpayment.

### Section 77(6) (Not in force from 1 May 2008)

  -----------------------------------------------------------------------
   

  **Section 77(6)**

  While this subsection is in force -\
  \
  (a) subsection (1) above shall not apply to a European patent (UK) the
  specification of which was published in French or German, unless a
  translation of the specification into English is filed at the Patent
  Office and the prescribed fee is paid before the end of the prescribed
  period;\
  \
  (b) subsection (4) above shall not apply to an amendment made in French
  or German unless a translation into English of the specification as
  amended is filed at the Patent Office and the prescribed fee is paid
  before the end of the prescribed period.
  -----------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------
   
  **Section 77(7)**
  Where such a translation is not filed, the patent shall be treated as always having been void.
  ------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------
   
  **Section 77(8)**
  The comptroller shall publish any translation filed at the Patent Office under subsection (6) above.
  ------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 77(9)**
  Subsection (6) above shall come into force on a day appointed for the purpose by rules and shall cease to have effect on a day so appointed, without prejudice, however, to the power to bring it into force again.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 77.12.3 {#section-9}

r.56(10) is also relevant

Rule 56(10) sets out that the appointed day is the day of coming into
force of the Agreement on the Application of Article 65 of the
convention on the grant of European patents made in London on 17 October
2000 ("the London Agreement"). The London Agreement came into force on 1
May 2008 from which date section 77(6) is no longer in force.

### Translations of European patents (UK) in French or German

### 77.13 {#ref77-13}

s.80(2) is also relevant

While s.77(6) was in force, European patents (UK) published or amended
in French or German were treated in the UK as ineffective and always
having been void unless an English translation of the specification or
amended specification was filed. Any translation filed of such a
specification or amended specification was published by the Office. Such
a translation of a specification may in some circumstances be treated as
the authentic text for the purposes of proceedings under the Act, [see
80.02](/guidance/manual-of-patent-practice-mopp/section-80-authentic-text-of-european-patents-and-patent-applications/#ref80-02).

### 77.14 {#section-10}

The Patents (Amendment) Rules 1987 (S.I. 1987 No. 288) appointed 1
September 1987 as the day on which s.77(6) came into force but only in
respect of European patents (UK) granted on or after that day. The date
of grant is that on which mention of the grant of the patent is
published in the European Patent Bulletin. Rule 56(9) and (10) of the
Patents Rules 2007 make provision for s. 77(6) and rule 56(1)(a) and (5)
to (8) to cease to have effect on the date that the Agreement on the
application of Article 65 of the Convention on the Grant of European
Patents made in London on 17 October 2000 ("the London Agreement") comes
into force. The coming into force date for the London Agreement was 1
May 2008 from which date translations are no longer required. Article 9
of the London Agreement sets out that that the agreement applies to
European patents in respect of which the mention of grant is published
in the European Patent Bulletin on or after the date the agreement comes
into force. However, there are no similar transitional arrangements for
rule 56 which has the effect that patent proprietors may take advantage
of the three month period prescribed for filing a translation. European
patents granted or amended from 1 February 2008 therefore do not require
a translation to be filed in order for the patent or amendment to enter
into force in the UK.

### 77.15 {#section-11}

The prescribed period may be extended in accordance with r.108(2) or (3)
and (4) to (6), [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34)
and therefore patents granted or amended prior to 1 February 2008 may
not require a translation to be filed.

### 77.16 {#section-12}

\[deleted\]

### 77.17 {#section-13}

\[deleted\]

### 77.18 {#section-14}

\[deleted\]

### 77.19 {#section-15}

\[deleted\]

### 77.20 {#section-16}

\[deleted\]

### 77.21 {#section-17}

\[deleted\]

### 77.22 {#section-18}

\[deleted\]

### 77.23 {#section-19}

\[deleted\]

### 77.24 {#section-20}

\[deleted\]

### 77.25 {#section-21}

Any renewal fees paid in respect of European patents (UK) which are
ultimately declared void because of failure to file a necessary
translation are not refunded.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
