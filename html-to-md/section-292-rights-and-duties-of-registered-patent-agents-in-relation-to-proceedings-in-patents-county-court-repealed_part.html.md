::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 292: Rights and duties of registered patent agents in relation to proceedings in patents county court \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (292.01 - 292.02) last updated April 2011.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 292.01

This section provided registered patent agents rights of audience before
the patents county court and the right to conduct the rest of
proceedings outside the courtroom.

### 292.02

Section 292 has been repealed by section 208(1), Schedule 21, paragraph
80 and Schedule 23 of the Legal Services Act 2007. The new regime
established by the Legal Services Act 2007 gives the definitions and
specific authorisation to take part in reserved legal activities such as
the rights and duties in the patents county court.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
