::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 58: References of disputes as to Crown use {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (58.01 - 58.15) last updated: April 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 58.01

This is the fifth of the group of sections relating to use of patented
inventions for the services of the Crown. It provides for the resolution
by the court of disputes concerning Crown use, including the settlement
of terms whereby the proprietor of a patent may be compensated for Crown
use. With regard to ss.55 to 59 in general, [see
55.01-03](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-01).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(1)**
  Any dispute as to (a) the exercise by a government department, or a person authorised by a government department, of the powers conferred by section 55 above, (b) terms for the use of an invention for the services of the Crown under that section, (c) the right of any person to receive any part of a payment made in pursuance of subsection (4) of that section, or (d) the right of any person to receive a payment under section 57A, may be referred to the court by either party to the dispute after a patent has been granted for the invention.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.02

s.57A(5) is also relevant.

Section 55 permits any government department or person authorised in
writing thereby to do certain acts for the services of the Crown, see
55.04-05. Section 55(4) provides that if terms for such use cannot be
agreed, they may be determined by the court on a reference under s.58,
[see
55.06-07](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-06).
All disputes concerning Crown use, as detailed in s.58(1), may be
referred to the court by either of the parties but the reference cannot
be made until after the grant of the relevant patent. Section 58(1) was
amended by the CDP Act to make disputes referable to the court regarding
the right to compensation under section 57A ([see
57A.01-02](/guidance/manual-of-patent-practice-mopp/section-57a-compensation-for-loss-of-profit/#ref57A-01))
The amount of such compensation may also be determined on such a
reference and is in addition to any other amount payable under s.55 or
57.

### 58.03

CPR 63.5 is also relevant

Such a reference to the Patents Court should be made by issue of a claim
s.130(1) form, the procedure being prescribed by the Civil Procedure
Rules. In England and Wales, s.58(12) a reference may alternatively be
made to the Patents County Court. In parts of the UK other than England
or Wales, the reference may be made to the High Court of that part or,
in Scotland, the Court of Session. Any such court may refer matters to a
Circuit judge or arbiter.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(2)**
  If in such proceedings any question arises whether an invention has been recorded or tried as mentioned in section 55 above, and the disclosure of any document recording the invention, or of any evidence of the trial thereof, would in the opinion of the department be prejudicial to the public interest, the disclosure may be made confidentially to the other party's legal representative or to an independent expert mutually agreed upon.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.04

Crown use may be made free of any royalty or other payment to the
proprietor of the patent if the invention in question was before its
priority date recorded or tried as set out in s.55(3). Documents or
evidence establishing such record or trial may be disclosed
confidentially to the other party's legal representative or an
independent expert if general disclosure would be prejudicial to the
public interest.

  -----------------------------------------------------------------------
   

  **Section 58(3)**

  In determining under this section any dispute between a government
  department and any person as to the terms for the use of an invention
  for the services of the Crown, the court shall have regard\
  (a) to any benefit or compensation which that person or any person from
  whom he derives title may have received or may be entitled to receive
  directly or indirectly from any government department in respect of the
  invention in question\
  (b) to whether that person or any person from whom he derives title has
  in the court's opinion without reasonable cause failed to comply with a
  request of the department to use the invention for the services of the
  Crown on reasonable terms.
  -----------------------------------------------------------------------

### 58.05

In settling terms for Crown use, the court has regard to the matters in
subsection (3)(a) and (b).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(4)**
  In determining whether or not to grant any relief under subsection (1)(a), (b) or (c) above and the nature and extent of the relief granted the court shall, subject to the following provisions of this section, apply the principles applied by the court immediately before the appointed day to the granting of relief under section 48 of the 1949 Act.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.06

Section 48 of the 1949 Act provided for the reference of Crown use
disputes to the court and thus corresponded to the present s.58. The
same principles are applied to the grant of relief under s.58 now as
were applied immediately before 1 June 1978 under the old s.48. The
provisions of s.58(4) with regard to Crown use correspond to those of
s.61(6) with regard to infringement. The CDP Act made a minor change in
the wording of s.58(4); this was consequential on amendment of s.58(1)
and did not alter the effect of s.58(4). Section 58(1)(d) is excluded
from comparability because there was no corresponding provision under
the 1949 Act.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(5)**
  On a reference under this section the court may refuse to grant relief by way of compensation in respect of the use of an invention for the services of the Crown during any further period specified under section 25(4) above, but before the payment of the renewal fee and any additional fee prescribed for the purposes of that section.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.07

The provisions of s.58(5) with regard to Crown use correspond to those
of s.62(2) with regard to infringement, [see
62.05](/guidance/manual-of-patent-practice-mopp/section-62-restrictions-on-recovery-of-damages-for-infringement/#ref62-05).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(6)**
  Where an amendment of the specification of a patent has been allowed under any of the provisions of this Act, the court shall not grant relief by way of compensation under this section in respect of any such use before the decision to allow the amendment unless the court is satisfied that (a) the specification of the patent as published was framed in good faith and with reasonable skill and knowledge; and (b) the relief is sought in good faith.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.08

The provisions of s.58(6) with regard to Crown use correspond to those
of s.62(3) with regard to infringement, [see
62.06](/guidance/manual-of-patent-practice-mopp/section-62-restrictions-on-recovery-of-damages-for-infringement/#ref62-06).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(7)**
  If the validity of a patent is put in issue in proceedings under this section and it is found that the patent is only partially valid, the court may, subject to subsection (8) below, grant relief to the proprietor of the patent in respect of that part of the patent which is found to be valid and to have been used for the services of the Crown.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.09

s.74(1) is also relevant

The validity of a patent may be put in issue in proceedings under s.58,
[see
74.03](/guidance/manual-of-patent-practice-mopp/section-74-proceedings-in-which-validity-of-patent-may-be-put-in-issue/#ref74-03).
The provisions of s.58(7) with regard to Crown use correspond to those
of s.63(1) with regard to infringement, [see
63.03](/guidance/manual-of-patent-practice-mopp/section-63-relief-for-infringement-of-partially-valid-patent/#ref63-03).

  -----------------------------------------------------------------------
   

  **Section 58(8)**

  Where in any such proceedings it is found that a patent is only
  partially valid the Court shall not grant relief by way of
  compensation, costs or expenses except where the proprietor of the
  patent proves that\
  (a) the specification of the patent was framed in good faith and with
  reasonable skill and knowledge, and\
  (b) the relief is sought in good faith,\
  and in that event the court may grant relief in respect of the part of
  the patent which is valid and has been so used, subject to the
  discretion of the court as to costs and expenses and as to the date
  from which compensation should be awarded.
  -----------------------------------------------------------------------

### 58.10

The provisions of s.58(8) with regard to Crown use correspond to those
of s.63(2) with regard to infringement, [see
63.04](/guidance/manual-of-patent-practice-mopp/section-63-relief-for-infringement-of-partially-valid-patent/#ref63-04).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(9)**
  As a condition of any such relief the court may direct that the specification of the patent shall be amended to its satisfaction upon an application made for that purpose under section 75 below, and an application may be so made accordingly, whether or not all other issues in the proceedings have been determined.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.11

The provisions of s.58(9) with regard to Crown use correspond to those
of s.63(3) with regard to infringement, [see
63.05](/guidance/manual-of-patent-practice-mopp/section-63-relief-for-infringement-of-partially-valid-patent/#ref63-05).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(9A)**
  The court may also grant such relief in the case of a European patent (UK) on condition that the claims of the patent are limited to its satisfaction by The European Patent Office at the request of the proprietor.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.11.1

Article 138(3) EPC provides a central amendment process for European
Patents. This is an alternative to the existing possibility of the
proprietor amending the patent under the 1977 Act. In the former case,
the amendments are effective in each Contracting State designated by the
patent whereas the latter would only affect the European patent (UK).
This section provides that relief may be granted on the condition that
the proprietor of a European patent (UK) limits the patent at the EPO.
The limitation would have to be done to the satisfaction of the court
for relief to be granted.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(10)**
  In considering the amount of any compensation for the use of an invention for the services of the Crown after publication of an application for a patent for the invention and before such a patent is granted, the court shall consider whether or not it would have been reasonable to expect, from a consideration of the application as published under section 16 above, that a patent would be granted conferring on the proprietor of the patent protection for an act of the same description as that found to constitute that use, and if the court finds that it would not have been reasonable, it shall reduce the compensation to such amount as it thinks just.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.12

The provisions of s.58(10) with regard to Crown use correspond to those
of s.69(3) with regard to infringement, [see
69.08](/guidance/manual-of-patent-practice-mopp/section-69-infringement-of-rights-conferred-by-publication-of-application/#ref69-08).

  -----------------------------------------------------------------------
   

  **Section 58(11)**

  Where by virtue of a transaction, instrument or event to which section
  33 above applies a person becomes the proprietor or one of the
  proprietors or an exclusive licensee of a patent (the new proprietor or
  licensee) and a government department or a person authorised by a
  government department subsequently makes use under section 55 above of
  the patented invention, the new proprietor or licensee shall not be
  entitled to any compensation under section 55(4) above (as it stands or
  as modified by section 57(3) above), or to any compensation under
  section 57A above, in respect of a subsequent use of the invention
  before the transaction, instrument or event is registered unless\
  (a) the transaction, instrument or event is registered within the
  period of six months beginning with its date; or\
  (b) the court is satisfied that it was not practicable to register the
  transaction, instrument or event before the end of that period and that
  it was registered as soon as practicable thereafter.
  -----------------------------------------------------------------------

### 58.13

The provisions of s.58(11) with regard to Crown use correspond to those
of s.68 with regard to infringement, [see
68.04-05](/guidance/manual-of-patent-practice-mopp/section-68-effect-of-non-registration-on-infringement-proceedings/#ref68-04).
The reference to compensation under s.57A was added the CDP Act, see
57A.01-02.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(12)**
  In any proceedings under this section the court may at any time order the whole proceedings or any question or issue of fact arising in them to be referred, on such terms as the court may direct, to a Circuit judge discharging the functions of an official referee or an arbitrator in England and Wales, the Isle of Man or Northern Ireland, or to an arbiter in Scotland; and references to the court in the foregoing provisions of this section shall be construed accordingly.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.14

The reference to the Isle of Man was added to s.58(12) by S.I. 1978 No.
621, which has since been replaced by S.I. 2003 No. 1249.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 58(13)**
  One of two or more joint proprietors of a patent or application for a patent may without the concurrence of the others refer a dispute to the court under this section, but shall not do so unless the others are made parties to the proceedings; but any of the others made a defendant or defender shall not be liable for any costs or expenses unless he enters an appearance and takes part in the proceedings.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 58.15

The provisions of s.58(13) with regard to Crown use correspond to those
of s.66(2) with regard to infringement.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
