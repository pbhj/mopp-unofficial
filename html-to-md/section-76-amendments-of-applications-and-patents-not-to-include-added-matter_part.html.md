::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 76: Amendments of applications and patents not to include added matter {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (76.01 - 76.28) last updated: April 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 76.01 {#ref76-01}

This section, which bars the inclusion of additional matter in a patent
application or specification, was rewritten by the CDP Act. The changes
then made had little effect with regard to the prohibition on the
introduction of new matter when amending an application or patent, but
changed the consequences of the presence of such matter in a new
application based on, and claiming the filing date of, an earlier
application or patent. Section 76(1A) was added to this section by the
Regulatory Reform (Patents) Order 2004 to prohibit the introduction of
new matter in a description when the initial filing includes a reference
under s.15(1)(c)(ii) to an earlier application.

  -----------------------------------------------------------------------
   

  **Section 76(1)**

  An application for a patent which\
  (a) is made in respect of matter disclosed in an earlier application,
  or in the specification of a patent which has been granted, and\
  (b) discloses additional matter, that is, matter extending beyond that
  disclosed in the earlier application, as filed, or the application for
  the patent, as filed,\
  \
  may be filed under section 8(3), 12 or 37(4) above, or as mentioned in
  section 15(9) above, but shall not be allowed to proceed unless it is
  amended so as to exclude the additional matter.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 76(1A)**

  Where, in relation to an application for a patent\
  (a) a reference to an earlier relevant application has been filed as
  mentioned in section 15(1)(c)(ii) above; and\
  (b) the description filed under section 15(10)(b)(i) above discloses
  additional matter, that is, matter extending beyond that disclosed in
  the earlier relevant application,\
  \
  the application shall not be allowed to proceed unless it is amended so
  as to exclude the additional matter.
  -----------------------------------------------------------------------

### 76.02 {#ref76-02}

Section 76(1) requires that an application which seeks divisional status
under s.15(9) (or to be treated as a new application under s.8(3), 12 or
37(4) following entitlement proceedings) and which discloses matter
extending beyond that disclosed in the relevant earlier application,
shall not be allowed to proceed unless it is amended to exclude that
matter (see procedure in
[15.35](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-35)
and
[15.45](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-45)).
Once amended, it should be published (provided all requirements are met)
under s.16(1) as filed (see
[15.38-39](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-38)
and
[16.08](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-08)).
Similarly, where an application contains a reference to an earlier
application under s.15(1)(c)(ii) and the description of the invention
sought required under s.15(10)(b)(i) discloses matter extending beyond
the earlier application, the application shall not be allowed to proceed
unless it is amended to exclude that matter ([see 15.06.3 to
15.06.5](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-06-03)),
but once amended, the description as first filed should be published
under s.16(1).

### 76.03 {#ref76-03}

The tests for deciding whether a later application discloses matter
which extends beyond that disclosed in an earlier application are the
same as the tests for determining whether amendment of an application
adds matter ([see 76.05 to 76.19](#ref76-05)).

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 76(2)**
  No amendment of an application for a patent shall be allowed under section 15A(6), 18(3) or 19(1) if it results in the application disclosing matter extending beyond that disclosed in the application as filed.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Before grant

### 76.04 {#ref76-04}

Section 76(2) disallows amendment of an application which results in it
disclosing matter extending beyond that which it disclosed when filed.
Its strictures apply to amendment of an application made either in
response to an objection made in the course of preliminary or
substantive examination or at the applicant's own volition.\
\
\[ RC9 may be used to object to a specification which contains added
matter as a result of amendment under s.18(3), r.31(3) or r.31(4). It is
not however appropriate for amendments requested under r.31(6) which
would add matter. (See further
[19.19](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-19),
[19.21](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-21))
\]

### 76.04.1 {#ref76-04-1}

In addition to the following UK precedents, Decisions of the EPO's
Boards of Appeal are also relevant. In Lowndes' Application ([BL
O/019/93](https://www.gov.uk/government/publications/patent-decision-o01993)),
the hearing officer noted that, although s.76 is not one of those
specified in s.130(7) as having been framed to have the same effect as a
corresponding provision of the EPC, there is an indirect link to the EPC
through s.72(1)(d), which concerns added matter as a ground for
revocation. This latter provision is worded in similar terms to s.76(1)
and (2) and is covered by s.130(7) (see also [76.22](#ref76-22) and
[91.02](/guidance/manual-of-patent-practice-mopp/section-91-evidence-of-conventions-and-instruments-under-conventions/#ref91-02)).
Moreover, in Merrell Dow Pharmaceuticals Inc v H N Norton & Co Ltd (BL
C/089/96) Jacob J came to the firm conclusion that s.76 is not intended
to have a different effect from a.123 EPC.

### 76.05 {#ref76-05}

The same general considerations arise under s.76(2), and s.72(1)(d),
regardless of whether an amendment relates to the description (including
any drawings) ([see paras 76.06-14](#ref76-06-14) or to the claims [see
paras 76.15 to 76.22](#ref76-15). Questions as to whether amendments
disclose additional matter can arise both pre- and post-grant in the
context of claim broadening. However, it is important to realise that
the stricture against claim broadening in s.76(3)(b) and the sanction
provided by s.72(1)(e) only apply to post-grant amendment ([see
76.24-27](#ref76-24-27)), as was confirmed by the Court of Appeal in
Texas Iron Works v Nodeco \[2000\] RPC 207. The only stricture governing
pre-grant amendment is that no additional matter should be disclosed,
and s.69(2) clearly contemplates allowability of claim broadening (or
"lateral shifting") so that acts which did not infringe the claims of
the published application could yet infringe those of the granted patent
([see 76.16](#ref76-16)). The same possibility is contemplated by
ss.17(8) and 18(1A).

### 76.06 {#ref76-06}

When considering in Bonzel and Schneider (Europe) AG v Intervention Ltd
\[1991\] RPC 553 whether an amendment to the description had the result
that a patent as granted disclosed matter which extended beyond that
disclosed in the application, Aldous J described his task as\
(1) to ascertain through the eyes of the skilled addressee what is
disclosed, both explicitly and implicitly in the application;\
(2) to do the same in respect of the patent as granted;\
(3) to compare the two disclosures and decide whether any subject matter
relevant to the invention has been added whether by deletion or
addition. The comparison is strict in the sense that subject matter will
be added unless such matter is clearly and unambiguously disclosed in
the application either explicitly or implicitly.\
\
As summarised by Jacob J. in Richardson-Vicks Inc.'s Patent \[1995\] RPC
568, "the test of added matter is whether a skilled man would, upon
looking at the amended specification, learn anything about the invention
which he could not learn from the unamended specification."\
\
(With regard to the narrowing of a claim to claim a sub-range not
specified before, [see
18.69-18.69.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-69)
and [76.27](#ref76-27)).

### 76.07 {#ref76-07}

In the Bonzel case, it was decided that additional matter was disclosed
by an amendment resulting in a guide wire lumen of a dilatation catheter
being described as "relatively short" (compared with the prior art),
rather than as "about as long as the (catheter) balloon" (the original
description of its length). The terms of the comparison at (3) above
conform broadly with those of the test for novelty adopted in some EPO
decisions as a basis for determining the allowability of amendments.
(i.e. no (new) subject matter may be disclosed by amendment which is not
derivable directly and unambiguously from the original application by a
person skilled in the art; see, eg, Technical Board of Appeal Decision
T201/83, OJEPO 10/84.) However, in A C Edwards Ltd v Acme Signs &
Displays Ltd \[1990\] RPC 621 at p.644 whilst acknowledging that the
novelty test could often prove useful, and would have given the same
result in that case, Aldous J observed nonetheless that it should be
applied with caution.

### 76.08 {#ref76-08}

r.15(2) and s.14(2) is also relevant.

In order to determine the original teaching of an application, the whole
of the description, any drawings and any claims which was or were
present on the filing date may be considered (for later-filed original
claims, see
[14.145](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-145)).
When viewed as a source of disclosure, the claims of a patent
specification are no different from any other source. The Court of
Appeal so held in A C Edwards Ltd v Acme Signs & Displays Ltd \[1992\]
RPC 131 at p.142, applying the decision of the House of Lords in Asahi
Kasei Kogyo KK \[1991\] RPC 485 (see also
[2.10.2](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-10-2)).

### 76.08.1 {#section}

A priority document does not form part of the application, and matter
disclosed in the priority document but omitted from the specification as
filed may not be subsequently added (as confirmed in VEB Kombinat
Walzlager \[1987\] RPC 405, [see
15.08](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-08)

### 76.08.2 {#ref76-08-2}

s.14(2) is also relevant.

Matter that is only disclosed in the abstract cannot be added to the
specification, regardless of whether the abstract was filed on or after
the filing date. The abstract is part of the application, though not
part of the specification, and so the hearing officer in ARMCO Inc's
Application ([BL
O/84/85](https://www.gov.uk/government/publications/patent-decision-o08485))
accepted that matter present in an abstract filed on the date of filing
could be considered to be part of the disclosure of the application when
determining under s.76(2) whether an amendment adds matter. However, the
Patents Court in Abbott Laboratories Ltd. v Medinol Ltd \[2010\] EWHC
2865 (Pat) held that the content of the abstract, filed on the date of
filing of the application, could not be used to determine the content of
the application as filed for the purpose of s.76 -- [see
14.171](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-171).
Examiners should therefore disregard the content of the abstract in
determining whether an amendment adds matter.

### 76.09 {#ref76-09}

It is allowable to add to the description or claims matter disclosed in
a drawing provided it does not go beyond what a skilled person would
judge to be disclosed in the drawing. If it is necessary to rely on the
drawings for support and the formal drawings were filed later than the
application date, then the informal drawings should be consulted. A
drawing treated as omitted under s.15(2) or (3) ([see
15.07-15.16](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-07))
may be reinstated by amendment, or a new drawing may be added, provided
that it shows nothing not originally disclosed in the application
(including any other drawings) on the date of filing. A similar
criterion applies to the amendment of a drawing.

### Implicit Disclosure

### 76.10 {#ref76-10}

Matter may be regarded as having been disclosed if the skilled reader
would realise that it was implicit in the original document (cf 18.22) -
see, for example, [DSM NV's Patent \[2001\] RPC 35 at paragraphs
197-200](http://rpc.oxfordjournals.org/content/118/19/675.full.pdf+html?sid=992bf004-43f5-40e6-bfd9-c6255f589f16){rel="external"}.
In this decision, Neuberger J gives the hypothetical illustration of a
description which refers to carrying out experiments at a certain
acidity, but which does not contain a reference to the fact that pH is a
measure of acidity. Since the skilled person would take this for
granted, it means that a claim which was amended to refer to this
acidity in terms of pH would not amount to added matter.

### 76.11 {#section-1}

Consider a further hypothetical example. If an applicant seeks to amend
a disclosure of a rubber composition comprising several ingredients to
specify that a further ingredient is present, this is prima facie not
allowable. However, the amendment may be allowable if the applicant can
show convincingly that the further ingredient is an additive normally
used in rubber compositions of that kind and that its omission would be
questioned by the skilled reader; such an amendment may be regarded as
clarification and as introducing nothing not already known to such a
reader, since the presence of the additive is implicit in the
description of the composition. If however the additive is merely common
but by no means universal, or if it is merely one additive selected from
several which are generally used, then the reference to its presence
constitutes added matter. A third example is an application originally
disclosing "resilient means" without disclosing any particular form of
such means. An amendment introducing the specific information that the
resilient means is, or could be, a helical spring, will not normally be
allowable. If however it can be convincingly argued that in the kind of
apparatus in question use of a helical spring is universal (for example
if the "resilient means" is for retracting the tip of a ball-point pen),
then the amendment may be allowable.

### 76.12 {#ref76-12}

Matter which is not disclosed, but which the skilled reader would find
it obvious to add, is not regarded as having been implicitly disclosed.
The specification of Flexible Direction Indicators Ltd's Application
\[1994\] RPC 207 concerned a traffic bollard characterised by its
flexibility and originally indicated that the bollard was made from a
compound of two polymers. The applicants produced evidence, suggesting
that a skilled reader would immediately see that one of the polymers
alone could provide the desired flexibility. They argued that removal of
the reference to a second polymer would be no more than rendering
explicit that which was already implicit. In refusing the amendment,
Aldous, J observed that s.76 "is concerned with what is disclosed, not
with that which the skilled reader might think could be substituted or
what had been omitted".

### 76.13 {#ref76-13}

If a generic term used in the document can be regarded as necessarily
disclosing a relatively small number of particular alternatives, then
restriction to one of these may be an allowable amendment. For example,
if a pump or valve is disclosed as for use with "fluid" then it is
reasonable to construe this as disclosing use with either liquid or gas,
so that a statement that use with only one of these is contemplated can
be regarded as a restriction of the disclosure rather than as added
subject-matter. However, in Noxell Ltd's Application ([BL
O/137/92](https://www.gov.uk/government/publications/patent-decision-o13792)),
the hearing officer refused to allow the applicants to specify that a
layer of plastic film was non-peelable, rejecting their submission that
the word "layer" disclosed two particular alternatives: peelable and
non-peelable layers. The hearing officer also rejected the applicant's
submission that s.76 does not apply to disclaiming amendments.

### 76.13.1 {#ref76-13-1}

In Protoned BV's Application, \[1983\] FSR 110, the invention as
originally described and claimed related to a mechanism for adjusting
the seat and back of a chair which used the co-operation of a gas spring
and a mechanical compression spring. The applicant sought to delete from
the claims the word "compression", and argued that a skilled reader
would at once realise that a tension spring could be used equally
effectively. This was rejected by Whitford J, who stated that even if
this were accepted to be the case, the amendment was not allowable,
since it added notionally to the body of the specification a whole range
of springs which were not originally in the application as filed.
However, the claim could well have been allowed if the approach taken
later in A C Edwards Ltd v Acme Signs & Displays Ltd and Southco Inc v
Dzus Fastener Europe Ltd ([see 76.15](#ref76-15) and [76.20](#ref76-20))
had been applied.

### Prior Art

### 76.14 {#ref76-14}

There may be no objection to an amendment introducing information
regarding prior art, provided it does not alter the construction of the
claims of the patent in suit (Cartonneries de Thulin SA v CTP White
Knight Ltd \[2001\] RPC 6). For example, while the originally filed
description of prior art may give the impression that the inventor has
developed the invention from a certain point, cited documents may show
that certain aspects of the alleged development were already known. The
subsequent inclusion of a reference to or a brief summary of the
relevant documents would not contravene s.76 (Merrell Dow Pharmaceutical
Inc v N H Norton & Co Ltd BL C/089/96). Likewise an amendment may be
allowable which sets out disadvantages of the prior art, and thus helps
to put the invention in its proper perspective in the art. If, however,
such an amendment implies an advantage of the invention, or if a
statement of such an advantage is sought to be introduced, for example
in order to distinguish the invention from the prior art, then this will
be allowable only if the advantage would have been apparent to a skilled
reader of the specification who was aware of the prior art (EPO Decision
T344/89; \[1993\] EPOR 209). In Palmaz's European Patents (UK) (\[1999\]
RPC 47, upheld on appeal \[2000\] RPC 631) Pumfrey J stated that the
practice, allowed by Advanced Semiconductor Products OJEPO 8/94 &
\[1995\] EPOR 97 (G 01/93) of adding an acknowledgement of prior art to
the body of the specification and limiting the claim by reference to the
prior art so acknowledged was too generally used in proceedings before
the EPO to be challenged, but noted that caution must be exercised where
the patentee themselves described the prior art in terms which they
propose to use in the limitation of their claim.

### Scope of the Claims vs. Disclosure {#scope-of-the-claims-vs-disclosure}

### 76.15 {#ref76-15}

In the case of [A C Edwards Ltd v Acme Signs & Displays Ltd \[1992\] RPC
131](http://rpc.oxfordjournals.org/content/109/7/131){rel="external"}
([see 76.08](#ref76-08)), the claim had been reduced in scope during
examination by the introduction of further features in generalised terms
which, the defendants contended, had the result that the claim covered,
and therefore disclosed, certain variations not disclosed in the
application as filed. The Court, in rejecting this argument,
distinguished between the ambit of the protection which the claim
identified and the matters which it disclosed. The Court decided that
although the claim covered certain variations, it contained no
disclosure of any of them; thus there was no added information and the
disclosure had not been extended. The Court also held that, in any
event, the variations in question had been implicitly disclosed to a
person skilled in the art by the contents of the application as filed.

### 76.15.0 {#ref76-15-0}

Similarly, [AP Racing Ltd v Alcon Components Ltd \[2014\] EWCA Civ
40](http://www.bailii.org/ew/cases/EWCA/Civ/2014/40.html){rel="external"}
concerned an appeal of a Patents County Court decision to revoke AP
Racing's patent due to added matter. The patent related to a disc brake
calliper with the claimed feature of asymmetrical peripheral stiffening
bands (PSBs); however the application as filed did not explicitly refer
to the PSBs as being asymmetrical and contained instead disclosure of
\[...one particular geometry of PSB in a "hockey stick" shape...\]. The
Patents County Court judge recognised that the PSBs disclosed in the
application were necessarily asymmetrical but said that a skilled
addressee would not have derived from the application "a concept at the
same level of generality as" the feature of claim 1. Thus he held matter
had been added in claiming the general feature of asymmetrical PSBs.
However, this was overturned by a decision of the Court of Appeal, in
which Floyd LJ stated:\

> Having correctly concluded that the description in the application of
> the hockey stick shaped PSBs was of something "necessarily
> asymmetrical" \[the judge\] should have gone on to ask himself whether
> there was any added disclosure in the granted specification. The
> description of the PSBs in claim 1 as "asymmetric" has to be read as
> part of the disclosure of the specification of the granted patent as a
> whole, taking account of the different function of the claims and the
> specification. When this is done the skilled person would understand
> that the patentee has drafted his claim so that it covers asymmetric
> PSBs generally. However I am not persuaded that the specification read
> as a whole discloses any configuration of PSB which is not disclosed
> in the application.

It is therefore possible to broaden the scope of a claim without
disclosing new information about an invention [also see
76.15](#ref76-15). That is, although broadening the scope of a claim may
result in the claim covering matter which was not previously disclosed,
this does not necessarily mean that the claim itself (and the
specification as a whole) actually discloses any additional matter. In
contrast, [Koninklijke Philips Electronics NV v Nintendo of Europe GmbH
\[2014\] EWHC 1959
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2014/1959.htm){rel="external"}
illustrates that a granted claim may cover new matter and also disclose
new matter over what was filed. A granted claim referring to "at least
one room localisation beacon" was held to add matter over an application
which disclosed only the use of plural beacons. The judge determined
that the granted claim clearly covered a system with only one beacon,
and also disclosed such a system, because that idea is conveyed by the
language of the claim. He stated

> The skilled addressee reading the granted patent would have the idea
> that one of the things they could build if they put the ideas in the
> document into practice was a system with a single beacon in it.

### 76.15.1 {#ref76-15-1}

The addition of what, in essence, was specific new disclosure was
considered in Van der Lely's Application \[1987\] RPC 61. In that case,
a divisional application claimed a baling machine comprising "at least
one swingable conveyor" whereas the main application only disclosed a
machine having three such conveyors. In the absence of any suggestion in
the main application that the baling machine could have less than three
swingable conveyors, and because the claim of the divisional application
embraced a machine with only one or only two conveyors of this type, the
disclosure of the divisional application was considered to extend beyond
that of the main application.

### 76.15.2 {#ref76-15-2}

As discussed in paragraph 4A.27.1, it does not add matter to amend a
"Swisstype" second medical use claim (e.g. "The use of substance X in
the manufacture of a medicament to treat disease Y"), or an unpatentable
method of treatment claim (e.g. "A method of treating disease Y by
administering substance X"), to the equivalent direct form of second
medical use claim ("Substance X for use in the treatment of disease Y").
Although such a change in claim format does not add to the technical
disclosure of a patent application, it would extend the scope of the
claims and is therefore contrary to s.76(3)(b) if made post grant (see
[4A.27.1](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-27-1)
and [76.26](#ref76-26)).

### Intermediate Generalisation

### 76.15.3 {#section-2}

Amendments which limit the scope of a claim by the introduction of one
or more features from the description or claims may in certain
circumstances add matter through what is known as "intermediate
generalisation". This concept was explained by Pumfrey J in Palmaz's
European Patents (UK) (\[1999\] RPC 47, upheld on appeal \[2000\] RPC
631):\

> If the specification discloses distinct sub-classes of the overall
> inventive concept, then it should be possible to amend down to one or
> other of those sub-classes, whether or not they are presented as
> inventively distinct in the specification before amendment. The
> difficulty comes when it is sought to take features which are only
> disclosed in a particular context and which are not disclosed as
> having any inventive significance and introduce them into the claim
> deprived of that context. This is a process sometimes called
> 'intermediate generalisation'.

### 76.15.4 {#ref76-15-4}

This definition has been endorsed in subsequent decisions of the courts,
such as Vector Corp v Glatt Air Technologies Ltd \[2007\] RPC 12. In
particular, if a feature is taken from only one, or a subset, of the
embodiments, stripped of the other related features of the
embodiment(s), and claimed as a defining feature of the invention, then
unless the application suggests that this feature has a particular
significance this is likely to constitute an intermediate
generalisation; as in Datacard Corp. v Eagle Technologies Ltd. \[2011\]
EWHC 244 (Pat), \[2011\] RPC 17.

### 76.15.5 {#ref76-15-5}

As discussed in Nokia Corporation v IPCOM GMBH & Co KG (No. 3) \[2013\]
R.P.C. 5 it is not permissible to introduce into a claim a feature taken
from a specific embodiment unless the skilled person would understand
that the other features of the embodiment are not necessary to carry out
the claimed invention. Put another way, it must be apparent to the
skilled person that the selected feature is generally applicable to the
claimed invention absent the other features of that embodiment. Teva UK
Limited & Anor v AstraZeneca AB \[2014\] EWHC 2873 (Pat) relates to a
patent for a therapy for asthma. In his decision Mr Justice Sales
applied the teaching of Nokia Corporation v IPCOM and found that a
specific example given in the patent specification could not be used to
generate generalisable patent claims. He held that the skilled addressee
would not be able to derive from the example that the dose amounts set
out are capable of abstraction from the details of the factual scenario
set out in that example. The proprietor was thus held to have added
matter to the application in attempting to generate patent claims based
on particular details from the example. In Starsight Telecast Inc & Anor
v Virgin Media Ltd & Ors \[2014\] EWHC 828 (Pat) the invention presented
in the application as filed was a detailed and specific method of using
parental controls to restrict access to program schedule information
displayed on a television. The judge however considered that the claim 1
in the granted patent was directed to a method of restricting access to
program schedule information based on parental control options per se
and was not restricted to the specific features set out in the
specification as filed. The granted claim 1 therefore presented the
skilled team with new information about the invention which was not
directly and unambiguously apparent from the original disclosure. It was
further held that claim 4, in combination with claims 1 and 2,
generalised the invention in a way that omitted important parts of the
disclosed method and thereby told the skilled team for the first time
that those parts were inessential. Consequently claim 1 and the
combination of claims 1, 2 and 4 were held to be intermediate
generalisations and therefore invalid on the grounds of added matter. In
[Philip Morris Products SA & Anor v Nicoventures Trading Ltd & Anor
\[2023\] EWHC 2616
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2023/2616.html){rel="external"}
the invention related to 'heat-not-burn' cigarettes where a cartridge of
smokable material is inserted into a device which can electrically heat
the smokable material. An amendment was filed which specified that the
heater "...extends to opposite longitudinal ends of the mass of smokable
material." This feature was only disclosed in a single embodiment of the
invention in which the heater was described as part of the cartridge of
smokable material, not part of the device itself. The judge held that
the disclosure did not clearly and unambiguously teach that this feature
could be generalised to the claimed embodiment in which the heater was
part of the device itself. Hacon J therefore concluded that the
amendment added matter.

### 76.15.6 {#ref76-15-6}

It is worth noting that the disclosure of the application includes all
the information that the skilled person may ascertain about the
invention. An intermediate generalisation is considered to add matter
(at least in part) because it results in the skilled addressee being
presented with information which they could not have derived from the
application as originally filed, concerning the importance of the newly
claimed feature. This new understanding is the 'matter' which is added.
In a similar vein, an amendment limiting the scope of a claim to a
single pill comprising 70 mg of alendronate by deleting other tablet
weights and dosage combinations was held by the Court of Appeal in Merck
& Co Inc's Patents \[2004\] FSR 16 to add to the teaching of the patent
by introducing the importance of the 70 mg dosage being in the form of a
single pill. For discussion of amendments which restrict a claim to a
sub-range of a range disclosed at filing [see
18.69.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-69-1).

### 76.16 {#ref76-16}

[see 76.20](#ref76-20)

### Omission of a feature

### 76.17 {#ref76-17}

Thus the omission from an amended claim of a feature specified in the
original claim may be allowable if it is apparent from the whole
document that its inclusion as a characterising feature was arbitrary
and unnecessary. On the other hand, if the specification gave the
impression that a feature was regarded as an essential element of the
invention then amendment to omit this feature is not allowable. In
Raychem Ltd's Application \[1986\] RPC 547, the applicants sought to
amend two divisional applications by omitting the final (cross-linking)
step from a process for producing a heat-recoverable polymeric sleeve
assembly. The hearing officer's refusal to allow the applications as so
amended to proceed under s.15(4) was upheld in the Patents Court where
it was held that the cross-linking feature was clearly disclosed as an
essential feature of the invention described; claims relating to the
"intermediate" product obtained without the final crosslinking were not
supported by that disclosure and would offend against s.76 as disclosing
matter extending beyond that disclosed in the applications (and parent)
as filed. In Decision T122/90, not published, concerning corresponding
European patent applications, the EPO Board of Appeal similarly
supported objection to claims "which did not specify that the bonded
parts could be cross-linked so that the sleeve assembly was presented
out of its original context", but allowed claims to the intermediate
product which required the relevant material to be cross-linkable so
that the bonded parts could be cross-linked, thus preserving the
cross-linking feature.

### 76.18 {#ref76-18}

Likewise, if a feature is necessary in order that the invention may
fulfil an originally-stated purpose, then its omission will not be
allowable. In International Playtex Corporation's Application, \[1969\]
RPC 362, the specification originally stated that an object of the
invention was "to design a brassiere with maximum resistance to riding
over derived from its built-in differential stretch patterns", and the
claim included "a triangular piece of stretchable fabric". Objection was
upheld to an amendment which sought to delete the statement of object
and to replace the reference in the claim by one to "a triangular
insert". The same outcome would appear likely under the current law.

### 76.18.1 {#ref76-18-1}

The Court of Appeal in \[Nokia Corporation v IPCOM GMBH & Co KG (No. 3)
\[2013\] R.P.C. 5\]( considered whether matter had been added by
omission with reference to the "Houdaille Test" set out by the EPO Board
of Appeal in [T331/87 Houdaille/Removal of feature \[1991\] E.P.O.R.
194](https://www.epo.org/en/boards-of-appeal/decisions/t870331ep1){rel="external"}.
The test was summarised by Kitchin L J:\
\

> The skilled person must be able to recognise directly and
> unambiguously that (1) the \[omitted\] feature is not explained as
> essential in the original disclosure, (2) it is not, as such,
> indispensible for the function of the invention in light of the
> technical problem it serves to solve, and (3) the replacement or
> removal requires no real modification of other features to compensate
> for the change.

### Claim Broadening

### 76.19 {#ref76-19}

If an invention has been claimed narrowly and the applicant subsequently
realises that they could have claimed it more broadly, this will not
generally be possible if the whole teaching of the original
specification was that the invention related only to the narrow aspect.
For example, Glatt's Application \[1983\] RPC 122 was for an article
(suitable for conditioning fabrics in a laundry dryer) comprising a
flexible woven or non-woven airpermeable web. It was held to be wrong to
allow claims to go forward omitting the requirement of air-permeability
"upon a description of the invention in the body of the specification
which only supports an article in which the use of an air-permeable base
fabric is an essential feature".

### 76.20 {#ref76-20}

Nevertheless, the fact that an amendment has the effect of broadening
the scope of the claims does not necessarily mean that it will be
regarded as adding matter ([see 76.05](#ref76-05)). Aldous J said in
Southco Inc v Dzus Fastener Europe Ltd (\[1990\] RPC 587; upheld on
appeal - see \[1992\] RPC 299) that "What the Act is seeking to prevent
is a patentee altering his claims in such a way that they claim a
different invention from that which is disclosed in the application.
Thus, provided the invention in the amended claim is disclosed in the
application when read as a whole, it will not offend against section
76", and that "section 76 is there to prevent the patentee disclosing
either by deletion or addition any inventive concept which was not
disclosed before but not to prevent a patentee claiming the same
invention in a different way". From a consideration of ss.125 and 130(3)
in that case, the judge concluded that although there was no definition
of the word "matter" it was wide enough to cover structural features of
the mechanism and inventive concepts, and that it was reasonable to look
at the claims construed as part of the whole document to see what was
the invention.

### 76.20.1 {#ref76-20-1}

Care must be taken when considering amendments to the description which
impact the construction of the claims. In [Ensygnia v Shell \[2023\]
EWHC 1495
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2023/1495.html){rel="external"},
a statement that embodiments comprising an electronic display were
"outside the scope of the claims" was added to the description by
amendment. However, the judge at paragraph 177 held that "without
hindsight, I do not think it would even occur to the skilled reader
reading this passage in the context of the application as a whole that
the invention could be implemented using a sign that is not electronic
or which cannot be changed." The patent was therefore found to be
invalid due to both added matter and for having extended the scope of
protection ([see 76.24-76.26](#ref76.24)).

### 76.21 {#ref76-21}

In many cases it will be possible to raise objection to unacceptable
claim broadening under either s.76 or s.14(5)(c); if both are
contravened, then in general objection should be raised under both. When
objecting under s.14(5)(c), a warning should be given against an attempt
to overcome the objection by amending the description in a way which
would contravene s.76(2). (For divisional applications, see
[15.30](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-30).

### 76.22 {#ref76-22}

[See 76.15.1](#ref76-15-1)

### 76.23 \[deleted\]

  -----------------------------------------------------------------------
   

  **Section 76(3)**

  No amendment of the specification of a patent shall be allowed under
  section 27(1), 73 or 75 if it\
  (a) results in the specification disclosing additional matter, or\
  (b) extends the protection conferred by the patent.
  -----------------------------------------------------------------------

### After grant

### 76.24 {#ref76-24}

Post-grant amendments are barred not only if they add matter ([see
76.04 - 76.23](#ref76-04)) but also if they extend the protection
conferred by the patent. These strictures apply to amendment of a
granted patent, whether in proceedings initiated by the proprietor of
the patent for that purpose or in the course of either proceedings
initiated by the comptroller to revoke the patent, or of other
proceedings in which the validity of the patent is put in issue.

### 76.25 {#ref76-25}

s.125(1) is also relevant.

There is no restriction in the form of a post grant amendment, provided
that it neither (a) extends the disclosure of the granted patent over
the application as filed -- [see 76.05-23](#ref76-05), nor (b) extends
the protection conferred by the patent. The protection conferred is
determined by the scope of the claims interpreted by the description and
any drawings, and the disclosure of the claims is similarly determined -
[see 76.16](#ref76-16). The term "matter" was held by Jacob J in Merrell
Dow Pharmaceuticals Inc v H N Norton & Co Ltd (BL C/089/96) to be
equivalent to "subject matter" in EPC a.123(2) and from the purposive
construction applied in that case it is presumed that "additional" means
compared to the application as filed, not as granted. This means that it
could be possible to re-introduce matter deleted before grant, provided
that it does not result in extending the protection conferred by the
patent. When interpreting s.76(3), due account may be taken of relevant
decisions of the EPO Boards of Appeal; [see 76.04.1](#ref76-04-1).

### 76.26 {#ref76-26}

To determine whether an amendment extends the protection conferred by
the patent, the question to be asked is, is it possible to conceive of
any act or apparatus which would infringe the amended claim but would
not infringe any claim of the patent as it stands, without the proposed
amendment? If the answer is negative then no objection arises under
s.76(3)(b). If the claims have already been amended post-grant, then the
proposed amendment must not extend the scope of the patent as currently
amended. So, for example, if the claims have already been limited by
amendment post-grant, the limitation cannot be removed in a further
amendment. In Siegfried Demel v C & H Jefferson \[1999\] FSR 204 it was
held that an amendment was permissible which broadened the scope of an
appendant claim on the grounds that any case covered by that claim as
amended would already have been covered by the earlier broader
independent claim. It would appear that it would also be permissible to
broaden the scope of an independent claim which is within the scope of
another independent claim, or to introduce a new claim, whether
appendant or independent providing the overall scope of the protection
conferred by the patent is not extended. However any such broadening or
intermediate generalisation ([see 76.15.2](#ref76-15-2)) must not be
contrary to the provisions of Sections 76(3)(a) and 14(5)(c) - ie must
not extend the matter disclosed and must be supported by the description
([see also
27.11](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-11)).
In patents with second medical use claims, post-grant amendment to
replace "Swiss-type" claims (e.g. "The use of substance X in the
manufacture of a medicament to treat disease Y") with the direct form of
second medical use claim ("Substance X for use in the treatment of
disease Y") is considered to extend the scope of protection and thus is
contrary to s.76(3)(b) -- [see
4A.27.1](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-27-1)).

### 76.27 {#ref76-27}

If an amendment allowed pre-grant is determined post-grant to have
disclosed additional matter, then s.76(3)(b) prevents amendment to
remove the additional matter if removal would have the result of
extending the protection conferred by the patent (see EPO Enlarged Board
of Appeal Decision G 01/93 Advanced Semiconductor Products, OJEPO 8/94).
This can have potentially fatal consequences for the validity of the
patent post-grant; the Enlarged Board described this as an "inescapable
trap".

  ----------------------------------------------------------------------------------------------
   
  **Section 76(4)**
  In subsection (1A) above "relevant application" has the meaning given by section 5(5) above.
  ----------------------------------------------------------------------------------------------

### 76.28 {#section-3}

See
[5.30](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-30).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
