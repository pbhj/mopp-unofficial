::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 84: Patent agents and other representatives \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (84.01 -84.02) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 84.01

This section formerly determined who was permitted to practise in the UK
as a representative of other persons in relation to European patents and
patent applications. This included taking part in proceedings relevant
thereto before the EPO or the comptroller as well as applying for or
obtaining European patents.

### 84.02

Section 84, together with other provisions of the 1977 Act concerning
patent agents, has been repealed by the CDP Act and replaced.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
