::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 121: Comptroller\'s annual report {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (121.01 - 121.02) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 121

Before 1st December in every financial year the comptroller shall cause
to be laid before both Houses of Parliament a report with respect to the
execution of this Act and the discharge of his functions under the
European Patent Convention and the Patent Cooperation Treaty, and every
such report shall include an account of all fees, salaries and
allowances, and other money received and paid by him under this Act,
that convention and that treaty during the previous financial year.

### 121.01

The comptroller's annual report is laid before Parliament and also
placed on sale to the public. It covers not only activities under the
1977 Patents Act, EPC and PCT (including trends of inventions in
published 1977 Act applications) but also those under the extant Acts
relating to registered designs and trade marks. Financial and other
statistics in respect of such activities, and accounts of other
activities in which The Office staff have been involved (e.g.
international conferences concerning intellectual property), are also
included.

### 121.02

The date of the report was changed by Article 6 of The Office Trading
Fund Order 1991 (SI 1991 No 1796) to require laying of the report (for
the previous financial year) before 30 November of the same year. This
article was revoked by the Patents Act 2004, which changed the date
required for the report in section 121 from 1 June in every year to 1
December in every financial year.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
