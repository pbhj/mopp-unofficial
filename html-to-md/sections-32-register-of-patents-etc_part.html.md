::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 32: Register of patents, etc {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (32.01 - 32.24) last updated: January 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 32.01

The maintenance of a register of patents is required by this section
which also provides for the making of rules relating to registration,
correction of errors in the register, public inspection and copying of
the register and other actions connected with the register. Rules 44-50
are the rules in question. Subsections (9) to (11) concern the status as
evidence of the register of patents, comptroller's certificates and
certified copies of entries in the register or of documents etc. Rules
46 and 48 prescribe the procedure for obtaining such certificates and
copies etc.

### 32.02

s.77(1), s78(4), S.89, Sch.2 is also relevant.

The section applies to applications for patents under the Act, including
international applications (UK) which have been published under the PCT
and entered the UK national phase (and are therefore treated as
published under s.16), and to patents granted under the Act or under the
EPC. Section 32 and the rules thereunder do not impose any requirements
as to the registration of applications for European patents (UK).

### 32.03 \[deleted\]

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 32(1)**
  The comptroller shall maintain the register of patents, which shall comply with rules made by virtue of this section and shall be kept in accordance with such rules.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 32(2)**

  Without prejudice to any other provision of this Act or rules, rules
  may make provision with respect to the following matters, including
  provision imposing requirements as to any of those matters ­\
  (a) the registration of patents and of published applications for
  patents;
  -----------------------------------------------------------------------

### Patents and published applications

### 32.04 {#ref32-04}

r.44(1), r.44(2) is also relevant.

Upon s.16 publication, the following are entered in the register:-

\(a\) the name and address of the applicant or applicants;\
(b) the name and address of the person or persons stated by the
applicant or applicants to be believed to be the inventor or inventors,
unless the comptroller has accepted an application under r.11(1) for the
inventor to waive their right to be mentioned (see 13.03);\
(c) the title of the invention;\
(d) the date of filing and the application number of the application for
the patent;\
(e) the date of filing and the application number of any application
declared for the purposes of s.5(2) and the country it was filed in or
in respect of;\
(f) the date on which the application was published;\
(g) the address for service of the applicant or applicants.

### 32.05 {#section-2}

r.44(3)-(6) is also relevant.

The following are also entered in the register, as soon as practicable
after the event to which they relate:­\
(a) the date of filing of the request for substantive examination;\
(b) the date on which the application is withdrawn, taken to be
withdrawn,\
treated as having been withdrawn, refused or treated as having been
refused;\
(c) the date on which the patent is granted;\
(d) the name and address of the person or persons to whom the patent is
granted if different from the entries made in accordance with item (a)
of [32.04](#ref32-04);\
(e) the address for service if different from the entry made in
accordance with item (g) of [32.04](#ref32-04);\
(f) certain matters in connection with a request for an opinion under
s.74A, see 74A.01-13\
(g) notice of any transaction, instrument or event referred to in
s.32(2)(b) or s.33(3), [see 32.08](#ref32-08).

Such other particulars as the comptroller may think fit may be entered
in the register at any r.46 time after s.16 publication. In addition to
the matters referred to above and in [32.07](#ref32-07),
[32.08](#ref32-08), [32.11](#ref32-11) and [32.12](#ref32-12), an entry
is made on cessation of a patent under s.25(3); on the reference to the
comptroller of a question or application under s.10 or 12(4) (disputes
between joint applicants), s.11(5) or 38(5) (justification for grant of
licence or its period or terms), s.13(1) and/or 13(3) (mention as
inventor), s.27 (amendment of specification after grant), s.28
(restoration of patent), s.29 (surrender of patent), s.46 or 47
(licences of right), s.48 (compulsory licence), s.61(3) (infringement of
patent), s.71 (declaration of non-infringement), s.72 (revocation of
patent) or s.117 (correction of specification after grant); and on
appeal to the Patents Court, Court of Appeal or House of Lords. Where
appropriate, the outcome of the above events is also recorded in the
register (but [see 32.12](#ref32-12) for proceedings in the courts).

A copy of an entry in or extract from the register should be requested
on Patents Form 23. r.49

### 32.06 {#ref32-06}

Rule 49(1) provides that any person may request that a correction or
change be entered in the register or made to any application or other
document filed at the Office in respect of their name or their address,
or that a correction be made to their address for service. The term
"correction" in rule 49 encompasses alterations to reflect changes that
have occurred in names or addresses (such as a change in the applicant's
name; see also
[19.06-07](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-06))
as well as corrections of typographical errors in names or addresses
([see
117.17](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-17)).
A request to correct or change a name should be made on Patents Form 20.
The correction or change of an address or the correction of an address
for service may be advised by means of any written notification; it is
not necessary to use a Patents Form, although Form 20 has been designed
with this use in mind. The register is altered accordingly if the
comptroller is satisfied that the request should be allowed. If the
comptroller has reasonable doubts about whether they should make the
correction or change, they must inform the person making the request of
the reason for their doubts and they may require that person to file
evidence in support of the request. The type of evidence required is
described in
[19.06](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-06).
There is no need to request correction of the register where a body
corporate has become a "public limited company" or "plc" (or the Welsh
equivalent of either) with its name otherwise unchanged. Under the
Patents (Companies Re-registration) Rules 1982, references to the name
in the register (and in any application to the comptroller and in any
other record kept at, or any document issued by, the Office and relating
to patents) are treated from the date of the change as references to the
new name. (If a request to amend the applicant's name on Form 1 is
allowed, the register is amended in the same respect, [see
19.07](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-07)).
Similar provisions do not apply for conversions under the Limited
Liability Partnership Act 2000 and its associated Regulations; transfer
to a limited liability partnership (LLP) would be regarded as an
assignment rather than a change of name, and would require recording in
the register ([see 32.08 to 32.12](#ref32-08)).

\[Form 20 and accompanying documents to correct or change a name should
be sent to Register Administration for action if it is indicated in part
4 of the form that correction under s.32(2)(d) is requested as a result
of a change in a name. However, if correction of a typographical error
is being requested under s.117, Form 20 and accompanying documents
should be sent to Formalities (for pre-grant cases) or Restoration and
Post Grant Section (RAPS) (for post-grant cases) for action.

\[ The Assignments Assistant should enter details of each patent or
patent application in the Section's Record Book, giving each a different
job number. The serial numbers of all patent or patent applications
should be entered on the Report Sheet, as should any discrepancy between
the existing name on the Register and the identification of that name in
the application. The fact that an application under s.30 or s.32 has
been filed should be entered on the COPS register for all patents or
applications executed under the Patents Act 1977 or at the EPO. The
files for ungranted cases are also endorsed. PDAX dossiers will have an
"ASSIGNMENT" section created.

\[ When a change of name is requested, the Register and Form 20 should
be checked to ensure that the original name is given consistently
throughout. Supporting evidence should not routinely be requested unless
some reasonable doubt exists as to the accuracy or veracity of what is
presented, and if evidence is requested, the applicant must be informed
what the doubt is. If proof is requested, the adequacy of the proof that
is supplied should be considered and, if it is inadequate, a stock
letter should be sent to the agent or applicant pointing out the
deficiency. The change of name should have taken place after the filing
date of the patent application, failing which the agent or applicant
should be advised to consider applying for a correction of a clerical
error, if that is applicable. If the change is allowed, the name in
question should be altered in the Register on OPTICS. The agent or
applicant should be informed by letter that alteration has been effected
and a report sheet should be completed. For paper cases, the change of
name folder should be placed at the back of the file in question or, if
the change relates to more than one patent, the documents should be
placed on the file of the highest publication number available. PDAX
dossiers have an "ASSIGNMENT" section for such changes.

\[ In respect of all ungranted 1977 Act cases, the old name on Form 1
should be struck out and replaced by the new name in red capital
letters. The alteration on Form 1 should be endorsed with "F20" and the
date of receipt of the form and initialled. Form 20 should be signed and
dated by the actioning officer. \]

### 32.07 {#ref32-07}

The comptroller is not required to keep entries in the register relating
to published but ungranted applications for European patents (UK). The
Register of European Patents, kept by the EPO under article 127 of the
EPC should be consulted for information on such applications.

  -------------------------------------------------------------------------------------------------------------------
   
  **Section 32(2)**
  \(b\) the registration of transactions, instruments or events affecting rights or under patents and applications;
  -------------------------------------------------------------------------------------------------------------------

### Transactions, instruments or events affecting rights

### 32.08 {#ref32-08}

r.44(6) is also relevant.

The register contains notice of any transaction, instrument or event
referred to in s.32(2)(b) or s.33(3). An agreement to assign, which
operates in English law to create and vest in the buyer an immediate
equitable interest, may thus be entered in the register as a transaction
affecting rights in a patent but this is not itself an assignment or any
of the other transactions, instruments or events specified in s.33
(Coflexip Stena Offshore Limited's Patent \[1997\] RPC 179).

### 32.09 {#ref32-09}

r.47, r.113(1), r.113(2) is also relevant.

An application to register, or to give notice to the comptroller of, any
such transaction, instrument or event should be made on Patents Form 21
accompanied by the appropriate fee. The fact that such an application
has been received is recorded in the register (when the application for
a patent has been published). Rule 47 requires that such an application
should include evidence establishing the transaction, instrument or
event. Thus the form should be signed by or on behalf of the person or
persons making the application, to confirm the changes to the rights
affected by the transaction, instrument or event and that any necessary
stamp duty has been paid (see below). If the Form is signed by or on
behalf of at least the assignor, mortgagor or grantor of a licence or
security, the application will normally be taken to include sufficient
evidence to register the transaction, instrument or event. In such cases
the comptroller will not normally require any additional evidence.
However, they may require further evidence if the particular
circumstances warrant it. In any case, further evidence sufficient to
establish the transaction, instrument or event should accompany the form
if (a) in the case of an assignment it is not signed by or on behalf of
the assignor, or (b) in the case of a mortgage or the grant of a licence
or security, where the mortgagor or grantor is not the applicant, it is
not also signed by or on behalf of the mortgagor or grantor. For any
documentary evidence not in English, a translation must be supplied.

For discussion of the registration of transactions by co-owners of a
patent, [see
36.07](/guidance/manual-of-patent-practice-mopp/section-36-co-ownership-of-patents-and-applications-for-patents/#ref36-07).

The requirement for stamp duty to be paid on an instrument exclusively
for the sale, transfer or other disposition of intellectual property (as
defined in section 129(2) of the Finance Act 2000) was removed with
effect from 28 March 2000 (by s.129 of the Finance Act 2000). Stamp duty
remains chargeable on instruments which deal in part with intellectual
property and in part with other property on which stamp duty is payable,
as set out in Schedule 34 to the Finance Act 2000. If the applicant or
other party enquires as to whether stamp duty is payable in relation to
a transaction relating in part to intellectual property and in part to
other property or in any other circumstances, e.g. in respect of
transactions outside the UK, it will normally be necessary to advise
that the enquiry should be referred to HM Revenue & Customs (HMRC).

In the case of a published application for a patent, details of a
transaction, instrument or event may be recorded even if the application
has been refused or withdrawn.

s.30(1),(2), r.55(g) is also relevant.

If Form 21 relates to an unpublished application for a patent, details
of the transaction, instrument or event concerned are published in the
journal. If there is a change of ownership of the application, that is
recorded on the Patents Form 1 in the application file. PDAX dossiers
should have the Form 1 annotated and a minute added to the dossier.

In the case of a granted patent, details of a transaction, instrument or
event may be entered on the register even if the patent has lapsed for
non-payment of fees. These details may not be registered in respect of a
revoked patent since revocation has effect ex tunc and the patent is
therefore deemed never to have been granted. However, any register
entries made prior to revocation remain on the register as a historical
record. Similarly if a patent has been deemed void ab initio no recordal
is possible.

When the Office is aware that there are proceedings before the court in
which the ownership of the patent is at issue, the applicant for
registration should be informed that the Office proposes to stay the
application on Form 21 pending the final outcome of those proceedings
unless the applicant provides evidence that the court is content for the
substitution of the parties (under the procedure governed by Rule 19.4
of the Civil Procedure Rules) or supplies evidence that the other party
consents to the registration. Where there are pending entitlement
proceedings before the Comptroller, the request to register an
assignment should be referred to the Hearing Officer. Unless special
circumstances apply, the Hearing Officer should contact the claimant to
determine if they are content with the registration. If the claimant is
not content, the registration should be stayed until proceedings have
been settled, but the fact that the request was made should be recorded
on the register. In the case where the Office is aware of any
proceedings before the court or the comptroller where ownership is not
in issue, registration will normally be effected, save that for inter
partes proceedings before the comptroller, the Hearing Officer should be
alerted. Here, registration will have an impact on the proceedings
because it is likely to change one of the parties, but this will have to
be dealt with by an application for substitution or addition of a party.

The same procedure applies in relation to granted European patents (UK).
Thus an application to register any transaction, instrument or event in
respect of a granted EP(UK)should be made on a Form 21. A copy of EPO
Form 2544 (notice of a change in the register issued by the EPO) is
sufficient as the documentary evidence required by rule 47 in certain
circumstances to support a Form 21. Although the EPO continues to record
assignments up to the end of the opposition period (under EPC rule 85),
this does not avoid the need for a Form 21 to be filed. For the purpose
of recording assignments and other transactions in the register, the
effective date of grant is the date on which notice of grant is
published in the Bulletin ([see
25.02](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-02)).

\[ Forms 21 and any accompanying documents to register assignments etc
should be referred to Assignment Section for action.

\[ In Assignments Section, an Assignments Assistant should check that
the correct form and fee have been presented and advise the agent or
applicant of any error. Cashier Section should hold the form (but not
any supporting documents) and a note of the error until it is rectified.
The Assignments Assistant should enter details of each application in
the Section's Record Book, giving each patent or patent application
affected a different job number. The serial numbers of all applications
or patents affected should be entered on the Report Sheet, as should any
discrepancy between the name and address of the assignor on the form or
any transaction document and in the Register. The fact that an
application under s.30 or s.32 has been filed should be entered on the
COPS register for all patents or patent applications executed under the
Patents Act 1977 or at the EPO. The files for ungranted cases are also
endorsed. PDAX dossiers not yet published will have the "ASSIGNMENTS"
section of the dossier completed.

\[ The contents of the Assignment Section (PDAX dossiers) or folder
(paper cases) should be examined to check that the assignor is empowered
to do so, that the patentee is the same as the one shown on the Register
sheet, that any documents are and signed, any documents have been
translated, any copies certified etc. If there is any deficiency, the
agent or applicant should be informed of it in the standard letter. A
suitable entry should be made in the Section's Record Book at this stage
and again when a reply is received. Cases where there is doubt as to
whether to effect registration while other proceedings are in train
should be referred up. When all requirements are met, the Register
should be altered accordingly. PDAX dossiers should have the
"ASSIGNMENTS" section of the dossier completed. For paper cases, the
assignment folder should be placed at the back of the file of the
application or patent in question or, if the change relates to more than
one patent, the documents should be placed on the file of the highest
publication number available.

\[ The above procedure is for assignments but procedure is generally the
same for mergers, licences, agreements and mortgages.\]

\[ The procedure in relation to a European patent (UK) is generally the
same as for a domestic patent. If there is found to be no Register entry
for the number quoted, a request should be made to the European Patent
Filing Department to obtain a printout. If that shows that the patent is
granted and designates the UK, the matter must be referred back to the
European Patent Filing Department to investigate the absence of the data
and arrange for the necessary register to be created. Once that is done
the request is entered in the Register and the normal procedure follows;
if it does not designate the UK, the documents should be returned to the
applicant giving the reason; or, if the patent is still in the
application stage but designates the UK, the applicant should be advised
that the matter should be referred to the EPO for registration. Prior to
grant, European patents (UK) do not have a file in the UK. Files are
raised as required. In the event of proceedings before the comptroller,
a request for the file should be made to ensure that a file is not
already in existence before a new file is raised. Suitable shells for
such files may be obtained from the European Patent Filing Department.
\]

### 32.10 {#ref32-10}

In the case of a deceased owner of a patent or application or right
therein, a certified copy of the probate or letters of administration is
required. The executor named therein should then complete an assignment
as though they were the owner but where the executor and beneficiary are
the same person or the named beneficiary is to be entered as the new
owner a copy of the will or a signed statement by the executor is
required. For overseas owners a personal representative who is resident
in the UK must be appointed.

### 32.11 {#ref32-11}

r.44(7) is also relevant.

Provided that the patent application in question (or its UK equivalent)
has been published under s.16, an entry is made in the register on the
reference to the comptroller of a question under s.8(1) (determination
before grant of questions about entitlement to a patent), s.12(1)
(determination before grant of questions about entitlement to a foreign
or convention patent) or s.37(1) (determination of right to patent after
grant). For a s.12(1) reference relating to an application for a patent
other than a European patent (UK), the entry is made under the UK
equivalent (ie application for a UK national patent or EP(UK)) if such
exists. Where a decision is issued in relation to any of the above, the
outcome is recorded in the register.

### 32.12 {#ref32-12}

Where any order or direction has been made or given by the court, for
example ­\
(a) transferring a patent or application or any right in or under it to
any person;\
(b) that an application should proceed in the name of any person; or\
(c) revoking a patent;\
the person in whose favour the order is made or the direction is given
should send the comptroller written notice thereof accompanied by an
office copy of such order or direction. The register is then rectified
or altered accordingly. (Where the order or direction refers to
amendment of a specification, [see
75.13](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-13)).

  --------------------------------------------------------------------------------------------------------------------
   
  **Section 32(2)**
  (ba) the entering on the register of notices concerning opinions issued, or to be issued, under section 74A below;
  --------------------------------------------------------------------------------------------------------------------

### 32.12.1 {#section-3}

r.44(5), (7) is also relevant.

An entry is made on the register when\
­(a) a request for an opinion under section 74A(1)(a) or (b) has been
received;\
(b) a request has been refused or withdrawn;\
(c) an opinion has been issued; or\
(d) the comptroller thinks other particulars concerning opinions or
requests should be entered.\

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 32(2)**
  \(c\) the furnishing to the comptroller of any prescribed documents or description of documents in connection with any matter which is required to be registered;
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Furnishing of documents in connection with registration

### 32.13 {#section-4}

Documents may be required in order to give grounds for a requested
insertion or amendment of an entry in the register, [see
32.06](#ref32-06), [32.07](#ref32-07), [32.09](#ref32-09),
[32.10](#ref32-10), [32.12](#ref32-12) and [32.14](#ref32-14).

  ---------------------------------------------------------------------------------------------------------------------------------
   
  **Section 32(2) **
  \(d\) the correction of errors in the register and in any documents filed at the Patent Office in connection with registration;
  ---------------------------------------------------------------------------------------------------------------------------------

### Correction of errors in register etc

### 32.14 {#ref32-14}

r.50 is also relevant.

Correction of an error in the register or in any document filed in
connection with registration must be requested in writing, accompanied
by sufficient information to identify the nature of the error and the
correction requested ([see 32.06](#ref32-06) if the requested correction
is due to a change in a name, address or address for service). Written
explanation of the reasons for the request or evidence in support of it
may be called for in order to satisfy the comptroller that there is an
error. If the comptroller is so satisfied, such correction as may be
agreed between the Comptroller and the proprietor of the patent or
applicant is made.

### 32.14.1 {#section-5}

The scope of the Comptroller's powers under r.50 was considered in
[Virgin Atlantic Airways Ltd v Jet Airways Ltd \[2013\] R.P.C.
10](http://rpc.oxfordjournals.org/content/130/4/247.abstract?sid=d97d4ace-d54d-4796-b60b-75caf9709700){rel="external"}
. A request had been made to the Office under r.50 to remove Virgin's
European Patent (UK) from the register because, it was claimed, the UK
designation was not valid on account of events that had taken place at
the EPO pre-grant. The request was dismissed by the hearing officer in
[BL O/281/11 (PDF,
82.6KB)](https://www.ipo.gov.uk/pro-types/pro-patent/pro-p-os/o28111.pdf){rel="external"}
. On appeal, the Patents Court held that the procedure under r.50 was
only suitable for corrections of the kind likely to be agreed with the
proprietor of (or applicant for) the patent. The Comptroller's power to
make corrections under r.50 therefore did not extend to corrections of
the kind sought in this case.

  ---------------------------------------------------------------------------------------------------------------
   
  **Section 32(2)**
  \(e\) the publication and advertisement of anything done under this Act or rules in relation to the register.
  ---------------------------------------------------------------------------------------------------------------

### Notices concerning register

### 32.15 {#section-6}

r.45 is also relevant.

The comptroller may arrange for the publication and advertisement of
such things done under the Act or Rules in relation to the register as
they may think fit. This is done by means of notices in the Journal.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 32(3)**
  Notwithstanding anything in subsection (2)(b) above, no notice of any trust, whether express, implied or constructive, shall be entered in the register and the comptroller shall not be affected by any such notice.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------
  **Section 32(4)**
  The register need not be kept in documentary form.
  ----------------------------------------------------

### Register in non-documentary form

### 32.16 {#section-7}

Subsection (4) (together with subsection (8) providing for inspection
and copying of material on the register in non-documentary form) enables
part(s) or all of the register to be kept in non-documentary form, in
particular on a computer.

  -----------------------------------------------------------------------------------------------------------------------
   
  **Section 32(5)**
  Subject to rules, the public shall have a right to inspect the register at the Patent Office at all convenient times.
  -----------------------------------------------------------------------------------------------------------------------

### Public inspection of register

### 32.17 {#section-8}

r.46, s.32(8)(a) is also relevant.

The register (or entries or reproductions of entries in it) is available
for inspection by the public at the Office in Newport or London between
the hours of 9am and 5pm on weekdays, other than Saturdays and days
which are specified as excluded days for the purposes of s.120. Register
details are also available from the [Online Patent Information and
Document Inspection Service
(Ipsum)](https://www.ipo.gov.uk/p-ipsum.htm){rel="external"}. A copy of
an entry in or extract from the register should be requested on Patents
Form 23 accompanied by the appropriate fee. In the case of a
non-documentary part of the register, the right of inspection is a right
to inspect the material on the register.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 32(6)**
  Any person who applies for a certified copy of an entry in the register or a certified extract from the register shall be entitled to obtain such a copy or extract on payment of a fee prescribed in relation to certified copies and extracts; and rules may provide that any person who applies for an uncertified copy or extract shall be entitled to such a copy or extract on payment of a fee prescribed in relation to uncertified copies and extracts.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 32(7)**
  Applications under subsection (6) above or rules made by virtue of that subsection shall be made in such manner as may be prescribed.
  ---------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 32(8)**

  In relation to any portion of the register kept otherwise than in
  documentary form­\
  (a) the right of inspection conferred by subsection (5) above is a
  right to inspect the material on the register; and\
  (b) the right to a copy or extract conferred by subsection (6) above or
  rules is a right to a copy or extract in a form in which it can be
  taken away and in which it is visible and legible\
  -----------------------------------------------------------------------

### Copies of entries; extracts

### 32.18 {#ref32-18}

s.32(8)(b), r.46 is also relevant.

Copies of entries in the register and extracts from the register are
obtainable, on application, in either certified or uncertified form. In
the case of a nondocumentary part of the register, both forms must be
made visible and legible and able to be taken away.

### Certified

### 32.19 {#ref32-19}

s.32(13), r.46,r48 is also relevant.

A certified copy or extract, i.e. one certified by the comptroller and
sealed with the seal of the Office, should be requested on Patents Form
23 accompanied by the appropriate fee ([see 32.20](#ref32-20)). It
constitutes evidence in accordance with subsections (11) and (12).
Section 32(6) makes mandatory the supply of certified copies of or
extracts from the register. The restrictions in r.51 ([see
118.07](/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-07))
apply to the supply of copies and extracts.

### 32.20 {#ref32-20}

In order to obtain a certificate ([see 32.22](#ref32-22)) or certified
copy or extract which will constitute evidence in accordance with
s.32(10) or (11), the greater of the two fees specified for Form 23
should accompany the form. A separate certificate (accompanied by the
relevant copy or extract if one has been requested) bearing the Office
seal and stating the matter certified is then provided.

### 32.20.1 {#section-9}

When requesting certified copies, a separate Form 23 should be used for
each patent or patent application.

### Uncertified

### 32.21 {#section-10}

An uncertified copy or extract should be requested on Patents Form 23 as
prescribed by r.46(3) and r.48, [see
118.08-09](/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-08).
The restrictions in r.51 ([see
118.07](/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-07))
apply to the supply of copies and extracts.

### 32.21.1 {#section-11}

A single Form 23 may be used to request uncertified copies relating to
more than one patent or patent application. However, certified and
uncertified copies should not be requested on the same form.

### 32.21.2 {#section-12}

An electronic uncertified copy of documents from a published UK patent
or patent application can be requested using the [online Form 23
service](http://www.ipo.gov.uk/p-apply-online-uk-uncertified-checklist.htm){rel="external"}.
However, it is not possible to use the online service to request a paper
uncertified copy, and it is not possible to request an electronic
uncertified copy by filing a paper Form 23.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 32(9)**
  The register shall be prima facie evidence of anything required or authorised by this Act or rules to be registered and in Scotland shall be sufficient evidence of any such thing.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 32(10)**
  A certificate purporting to be signed by the comptroller and certifying that any entry which he is authorised by this Act or rules to make has or has not been made, or that any other thing which he is so authorised to do has or has not been done, shall be prima facie evidence, and in Scotland shall be sufficient evidence, of the matters so certified.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Certificate of comptroller

### 32.22 {#ref32-22}

r.46(3) is also relevant.

Such a certificate certifying that an entry in the register has or has
not been made or that a certain thing has or has not been done should be
requested on Patents Form 23 accompanied by the appropriate fee ([see
32.20](#ref32-20)). The certificate bears the real or facsimile
signature of an authorised officer on behalf of the comptroller. The
restrictions in r.51 ([see
118.07](/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-07))
apply to the information which may be given.

  -----------------------------------------------------------------------
   

  **Section 32(11)**

  Each of the following, that is to say ­\
  (a) a copy of an entry in the register or an extract from the register
  which is supplied under subsection (6) above;\
  (b) a copy of any document kept in the Patent Office or an extract from
  any such document, any specification of a patent or any application for
  a patent which has been published, which purports to be a certified
  copy or a certified extract shall be admitted in evidence without
  further proof and without production of any original; and in Scotland
  such evidence shall be sufficient evidence.
  -----------------------------------------------------------------------

### Copies and extracts as evidence

### 32.23 {#section-13}

Subsection (11) lays down the status as evidence of not only a certified
copy or extract from the register ([see 32.18-20](#ref32-18)) but also a
certified copy of any document kept in the Office or extract therefrom,
any patent specification or any published patent application. Such
copies or extracts of documents etc, are obtainable as set out in
[32.19-20](#ref32-19). In the case of a document not open to public
inspection, a copy or extract thereof is not supplied unless the person
requesting it is entitled thereto, e.g. in the case of an unpublished
patent application they are the applicant or their agent. Where a
requested item cannot be supplied, the person is so informed, normally
by telephone, and the fee paid for that item may be refunded.

### \[Section 32(12) Repealed.\] {#section-3212-repealed}

### 32.24 {#section-14}

This subsection has been repealed by the Youth Justice and Criminal
Evidence Act 1999, with effect from 14 April 2000.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 32(13)**
  In this section "certified copy" and "certified extract" mean a copy and extract certified by the comptroller and sealed with the seal of the Patent Office.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 32(14)**

  In this Act, except so far as the context otherwise requires\
  "register", as a noun, means the register of patents;\
  "register", as a verb, means, in relation to any thing, to register or
  register particulars, or enter notice, of that thing in the register
  and, in relation to a person, means to enter his name in the register;\
  and cognate expressions shall be construed accordingly.
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
