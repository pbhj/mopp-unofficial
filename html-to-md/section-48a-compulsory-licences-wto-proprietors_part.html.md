::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 48A: Compulsory licences: WTO proprietors {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (48A.01-48A.09) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 48A.01 {#a01}

s.52(2) is also relevant.

This section provides the grounds for application under section 48 when
the proprietor of the patent is a WTO proprietor and conditions specific
to such applications. Section 48A was introduced by The Patents and
Trade Marks (World Trade Organisation) Regulations 1999 regulation 5
with effect from 29 July 1999. A WTO proprietor may, if an order or
entry is made, later apply to have that order or entry revoked or
cancelled if the circumstances which led to the making of the order or
entry have ceased to exist and are unlikely to recur [see
52.07](/guidance/manual-of-patent-practice-mopp/section-52-opposition-appeal-and-arbitration/#ref52-07).

  -----------------------------------------------------------------------
   

  **Section 48A(1)(a)**

  In the case of an application made under section 48 above in respect of
  a patent whose proprietor is a WTO proprietor, the relevant grounds
  are­\
  (a) where the patented invention is a product, that a demand in the
  United Kingdom for that product is not being met on reasonable terms;
  -----------------------------------------------------------------------

### Grounds for application (WTO proprietor)

### 48A.02 {#ref48a-02}

This ground applies where the patented invention is a "product", which
in this context apparently does not include a product obtained by means
of a patented process or to which a patented process has been applied.
The ground is concerned with meeting a demand for the product in the UK.
The demand must be an actual one and not merely one which an applicant
hopes and expects to create if and when they have obtained a licence and
commenced business (Cathro's Applications 51 RPC 75). Contrary to the
situation where the proprietor is a non-WTO proprietor, it is immaterial
whether the demand is met by production in the UK or by importation.

### 48A.03 {#ref48a-03}

The applicant needs to show that such a demand is not being met on
reasonable terms. What constitutes "reasonable terms" depends on a
careful consideration of all the surrounding circumstances in each case,
eg the nature of the invention, the terms of any licences under the
patent, the expenditure and liabilities of the patentee in respect of
the patent, and the requirements of the purchasing public. The price
charged by the patentee should be a bona fide one and not one adopted to
suppress or depress demand.

  -----------------------------------------------------------------------
   

  **Section 48A(1)(b)**

  that by reason of the refusal of the proprietor of the patent concerned
  to grant a licence or licences on reasonable terms­\
  (i) the exploitation in the United Kingdom of any other patented
  invention which involves an important technical advance of considerable
  economic significance in relation to the invention for which the patent
  concerned was granted is prevented or hindered, or\
  (ii) the establishment or development of commercial or industrial
  activities in theUnited Kingdom is unfairly prejudiced;
  -----------------------------------------------------------------------

### 48A.04 {#ref48A-04}

This ground concerns various consequences of the refusal of the
proprietor of the patent to grant a licence on "reasonable terms". This
may be a refusal to grant a licence at all or an offer to grant a
licence but on terms which are unreasonable despite discussion in an
effort to agree terms (Loewe Radio Co Ltd's Applications 46 RPC 479).
Under the provisions of the 1949 Act a similar ground was held not
applicable to the refusal of an existing exclusive licensee to grant a
licence on such terms (Colbourne Engineering Co Ltd's Application 72 RPC
169).

### 48A.05 {#ref48A-05}

With regard to the meaning of "reasonable terms", see
[48.18](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-18)
and [48A.03](#ref48a-03). In Brownie Wireless Co Ltd's Applications 46
RPC 457, the court considered the best test of whether a royalty is
reasonable is: How much are manufacturers who are anxious to make and
deal with the patented article on commercial lines ready and willing to
pay? The court also held that it can in certain circumstances be
reasonable to require licensees to take a licence under all patents
belonging to a group rather than under an individual patent from the
group, or to include terms requiring royalties to be paid on certain
non-patented articles. In Monsanto's CCP Patent \[1990\] FSR 93, it was
held that an offer of a licence covering a number of countries worldwide
in return for a fully paid-up royalty of 1 million US dollars did not
constitute a refusal to grant a licence on reasonable terms.

### 48A.06 {#a06}

s.48A(4) is also relevant.

The applicant must show that the refusal of the proprietor to grant a
licence on reasonable terms has caused one or other of two situations.
The first is that the working or efficient working in the UK of any
other patented invention which "involves an important technical advance
of considerable economic significance" is prevented or hindered.
However, the patentee of the other invention must be able and willing to
grant to the patentee and their licensees a licence in respect of the
other invention on reasonable terms.

### 48A.07 {#ref48A-07}

The alternative situation under s.48A(1)(b) is that the establishment or
development of commercial or industrial activities in the UK is unfairly
prejudiced. An increase in the size of a business is regarded as
sufficient to constitute such development (Kamborian's Patent \[1961\]
RPC 403).

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 48A(1)(c)**
  that by reason of conditions imposed by the proprietor of the patent concerned on the grant of licences under the patent, or on the disposal or use of the patented product or on the use of the patented process, the manufacture, use or disposal of materials not protected by the patent, or the establishment or development of commercial or industrial activities in the United Kingdom, is unfairly prejudiced.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 48A.08 {#a08}

s.49(1) is also relevant

This last ground concerns conditions imposed by the patentee which
unfairly prejudice (a) the manufacture, use or disposal of materials not
protected by the patent, or (b) the establishment or development of
commercial or industrial activities in the UK see 48A.22 re the latter.
Where (a) is held to be unfairly prejudiced, the comptroller may order
the grant of licences to customers of the applicant as well as to the
applicant.

  -----------------------------------------------------------------------
   

  **Section 48A(2)**

  No order or entry shall be made under section 48 above in respect of a
  patent whose proprietor is a WTO proprietor unless­\
  (a) the applicant has made efforts to obtain a licence from the
  proprietor on reasonable commercial terms and conditions; and\
  (b) his efforts have not been successful within a reasonable period.
  -----------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------
   
  **Section 48A(3)**
  No order or entry shall be so made if the patented invention is in the field of semi-conductor technology.
  ------------------------------------------------------------------------------------------------------------

### 48A.09 {#a09}

Article 31(c) of the TRIPS Agreement requires that use of the subject
matter of a patent should not be allowed without the authorization of
the right-holder in the case of semiconductor technology other than for
public non-commercial use or to remedy a practice determined after
judicial or administrative process to be anti-competitive. While no
order may be made for a compulsory licence or register entry in an
application under section 48 where the invention is in this field and
the proprietor is a WTO proprietor, a register entry that licences are
available as of right may be made on application by the appropriate
Minister under section 51 following a report of the Monopolies and
Mergers Commission.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 48A(4)**
  No order or entry shall be made under section 48 above in respect of a patent on the ground mentioned in subsection (1)(b)(i) above unless the comptroller is satisfied that the proprietor of the patent for the other invention is able and willing to grant the proprietor of the patent concerned and his licensees a licence under the patent for the other invention on reasonable terms.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 48A(5)**
  A licence granted in pursuance of an order or entry so made shall not be assigned except to a person to whom the patent for the other invention is also assigned.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 48A(6)**

  A licence granted in pursuance of an order or entry made under section
  48 above in respect of a patent whose proprietor is a WTO proprietor­\
  (a) shall not be exclusive;\
  (b) shall not be assigned except to a person to whom there is also
  assigned the part of the enterprise that enjoys the use of the patented
  invention, or the part of the goodwill that belongs to that part;\
  (c) shall be predominantly for the supply of the market in the United
  Kingdom;\
  (d) shall include conditions entitling the proprietor of the patent
  concerned to remuneration adequate in the circumstances of the case,
  taking into account the economic value of the licence; and\
  (e) shall be limited in scope and in duration to the purpose for which
  the licence was granted.
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
