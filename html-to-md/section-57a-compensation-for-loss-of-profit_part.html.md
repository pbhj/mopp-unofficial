::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 57A: Compensation for loss of profit {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (57A.01 - 57A.02) last updated: April 2007
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 57A.01 {#ref57A-01}

Section 57A was added by the CDP Act to make available an additional
form of compensation for Crown use of a patented invention, which made
consequential amendments to s.58. This supplements the provisions of
sections 55 to 59 regarding Crown use.

### 57A.02 {#a02}

Section 57A is effective for any Crown use after commencement (1 August
1989) even if terms for such use had been settled previously.

  -----------------------------------------------------------------------
   

  **Section 57A(1)**

  Where use is made of an invention for the services of the Crown, the
  government department concerned shall pay\
  (a) to the proprietor of the patent, or\
  (b) if there is an exclusive licence in force in respect of the patent,
  to the exclusive licensee,\
  \
  compensation for any loss resulting from his not being awarded a
  contract to supply the patented product or, as the case may be, to
  perform the patented process or supply a thing made by means of the
  patented process.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57A(2)**
  Compensation is payable only to the extent that such a contract could have been fulfilled from his existing manufacturing or other capacity; but is payable notwithstanding the existence of circumstances rendering him ineligible for the award of such a contract.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57A(3)**
  In determining the loss, regard shall be had to the profit which would have been made on such a contract and to the extent to which any manufacturing or other capacity was underused.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57A(4)**
  No compensation is payable in respect of any failure to secure contracts to supply the patented product or, as the case may be, to perform the patented process or supply a thing made by means of the patented process, otherwise than for the services of the Crown.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57A(5)**
  The amount payable shall, if not agreed between the proprietor or licensee and the government department concerned with the approval of the Treasury, be determined by the court on a reference under section 58, and is in addition to any amount payable under section 55 or 57.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57A(6)**
  In this section 'the government department concerned', in relation to any use of an invention for the services of the Crown, means the government department by whom or on whose authority the use was made.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 57A(7)**
  In the application of this section to Northern Ireland, the reference in subsection (5) above to the Treasury shall, where the government department concerned is a department of the Government of Northern Ireland, be construed as a reference to the Department of Finance and Personnel.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
