::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Changes to the Manual of Patent Practice {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
This section details the recent main changes to the manual.
:::

::: govuk-grid-column-two-thirds
::: {#default-id-c97e4e9e .gem-c-accordion .govuk-accordion .govuk-!-margin-bottom-6 module="govuk-accordion gem-accordion ga4-event-tracker" ga4-expandable="" anchor-navigation="true" show-text="Show" hide-text="Hide" show-all-text="Show all sections" hide-all-text="Hide all sections" this-section-visually-hidden=" this section"}
::: govuk-accordion__section
::: govuk-accordion__section-header
## [April 2024 changes]{#default-id-c97e4e9e-heading-1 .govuk-accordion__section-button} {#april-2024-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"April 2024 changes\",\"index_section\":1,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-1 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-1" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"April 2024 changes\",\"index_section\":1,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------
  Paragraph                                                                                                                                               Update

  [7.11.1](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-7-right-to-apply-for-and-obtain-a-patent/#ref7-11-1)\                       Updated to in light of [Thaler v Comptroller-General of Patents, Designs and
  [13.09](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-09)\                                           Trade Marks \[2023\] UKSC
  [13.10.1](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-10-1)                                        49](https://www.bailii.org/uk/cases/UKSC/2023/49.html){rel="external"}

  [SPM3.03.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-03-1)   Updated in light of [Newron Pharmaceuticals SpA v The Comptroller General of
                                                                                                                                                          Patents, Trademarks and Designs \[2024\] EWCA Civ
                                                                                                                                                          128](https://caselaw.nationalarchives.gov.uk/ewca/civ/2024/128){rel="external"}

  [SPM3.05.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-05-1)   Updated in light of [Merck Serono S.A. v The Comptroller-General of Patents,
                                                                                                                                                          Trade Marks and Designs \[2023\] EWHC 3240
                                                                                                                                                          (Ch)](https://www.bailii.org/ew/cases/EWHC/Ch/2023/3240.html){rel="external"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [January 2024 changes]{#default-id-c97e4e9e-heading-2 .govuk-accordion__section-button} {#january-2024-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"January 2024 changes\",\"index_section\":2,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-2 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-2" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"January 2024 changes\",\"index_section\":2,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                       Update
  [1.39.4](/guidance/manual-of-patent-practice-mopp/section-1-patentability/ref1-39.4)                                                            Added in light of [Emotional Perception AI Ltd v Comptroller-General of Patents, Designs and Trade Marks \[2023\] EWHC 2948 (Ch)](https://www.bailii.org/ew/cases/EWHC/Ch/2023/2948.html){rel="external"}
  [3.87.3](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-87-3)                                                          Updated in light of Teva Pharmaceutical Industries Ltd v Astellas Pharma Inc. \[2022\] EWHC 1316(Pat) and Teva Pharmaceutical Industries Ltd & Anor v Astellas Pharma Inc \[2023\] EWCA Civ 880
  [3.95](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-95)                                                              Updated in light of Vernacare Limited v Moulded Fibre Products Limited \[2023\] EWCA Civ 831
  [76.15.5](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-15-5)   Updated in light of Philip Morris Products SA & Anor v Nicoventures Trading Ltd & Anor \[2023\] EWHC 2616 (Pat)
  [76.20.1](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-20-1)   Added in light of Ensygnia v Shell \[2023\] EWHC 1495 (pat)
  SPC - Several paragraphs                                                                                                                        Updated to reflect changes in terminology arising from the Retained EU Law (Revocation and Reform) Act 2023
  ----------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [October 2023 changes]{#default-id-c97e4e9e-heading-3 .govuk-accordion__section-button} {#october-2023-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"October 2023 changes\",\"index_section\":3,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-3 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-3" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"October 2023 changes\",\"index_section\":3,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ---------------------------------------------------------------------------------------------- ---------------------------------------------------------------
  Paragraph                                                                                      Update
  [20.02](guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)   Updated in light of Walmart Apollo's application BL O/0704/23
  ---------------------------------------------------------------------------------------------- ---------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [July 2023 changes]{#default-id-c97e4e9e-heading-4 .govuk-accordion__section-button} {#july-2023-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"July 2023 changes\",\"index_section\":4,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-4 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-4" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"July 2023 changes\",\"index_section\":4,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                     Update
  [36.07](/guidance/manual-of-patent-practice-mopp/section-36-co-ownership-of-patents-and-applications-for-patents/#ref36-07)   Updated in light of Taylor v Lanarkshire Health Board ([BL O/864/21](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F864%2F21&submit=Go+%BB){rel="external"}) and ([BL O/157/22](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F157%2F22&submit=Go+%BB){rel="external"})
  [20.02](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)                                 Updated in light of Metabiotech's application BL O/359/23
  ----------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [April 2023 changes]{#default-id-c97e4e9e-heading-5 .govuk-accordion__section-button} {#april-2023-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"April 2023 changes\",\"index_section\":5,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-5 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-5" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"April 2023 changes\",\"index_section\":5,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                Update

  [1.20.2](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-20-2)                    Updated in light of Nokia v Oppo \[2023\] EHWC 23 (Pat)

  [3.26.1](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-26-1)                   Updated in light of Vernacare Ltd v Moulded Fibre Products (MFP) \[2022\] EWHC 2197 (IPEC)

  [5.30](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-30)                        Updated in light of Adamson Jones IP Limited v American Isostatic Presses Inc
                                                                                                           (BL[O/00992/23](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F0092%2F23&submit=Go+%BB){rel="external"})

  [15.20.1](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-20-1)\   Updated in light of forthcoming practice change on accordance of [compliance dates for divisional
  [20.02](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)            applications](https://www.gov.uk/government/publications/compliance-periods-on-divisional-applications).
  -------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [January 2023 changes]{#default-id-c97e4e9e-heading-6 .govuk-accordion__section-button} {#january-2023-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"January 2023 changes\",\"index_section\":6,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-6 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-6" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"January 2023 changes\",\"index_section\":6,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                 Update
  [20.02](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)                                                                                                             Updated in light of Xu's Application ([BL O/610/22](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/610/22){rel="external"})
  [125.17.8](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-17-8) and [125.18.8](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-18-8)   Updated in light of [Vernacare Ltd v Moulded Fibre Products (MFP) \[2022\] EWHC 2197 (IPEC)](https://caselaw.nationalarchives.gov.uk/ewhc/ipec/2022/2197){rel="external"}
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [October 2022 changes]{#default-id-c97e4e9e-heading-7 .govuk-accordion__section-button} {#october-2022-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"October 2022 changes\",\"index_section\":7,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-7 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-7" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"October 2022 changes\",\"index_section\":7,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                                                   Update
  [2.08.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-08-1)                                                                                                                                                             Updated in light of Zebra Technologies Corporation ([BL O/653/22(PDF,402 KB)](https://www.ipo.gov.uk/p-challenge-decision-results/o65322.pdf){rel="external"})
  Various paragraphs in [S.13](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor) and [S.89B](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application)   Updated in Light of Thaler's Application ([BL O/447/22(PDF, 226 KB)](https://www.ipo.gov.uk/p-challenge-decision-results/o44722.pdf){rel="external"})
  [SPM3.03.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-03-1)                                                                                       Updated in light of Roche Glycart AG ([BL O/711/22](226%20KB))
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [July 2022 changes]{#default-id-c97e4e9e-heading-8 .govuk-accordion__section-button} {#july-2022-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"July 2022 changes\",\"index_section\":8,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-8 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-8" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"July 2022 changes\",\"index_section\":8,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                Update
  [4A.29.4.1](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-29-4-1)                                        Updated in light of Sandoz Ltd v Teva Pharmaceutical Industries Ltd \[2022\] EWHC 822 (Pat)
  [67.04](/guidance/manual-of-patent-practice-mopp/section-67-proceedings-for-infringement-by-exclusive-licensee/#ref67-04)                                Updated in light of Neurim Pharmaceuticals (1991) Ltd v Generics (UK) Ltd \[2022\] EWCA Civ 359
  [SPM 1.04.2](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM1-04-2)   Updated in light of Ethicon, Inc., and Omrix Biopharmaceuticals, Inc. (BL O/136/22)
  -------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [April 2022 changes]{#default-id-c97e4e9e-heading-9 .govuk-accordion__section-button} {#april-2022-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"April 2022 changes\",\"index_section\":9,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-9 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-9" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"April 2022 changes\",\"index_section\":9,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                       Update
  [3.87.2](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-87-2)                                                                                                                          Updated in light of Teva Pharmaceutical Industries Ltd & Ors v Bayer Healthcare LLC \[2021\] EWHC 2690 (Pat)
  Various paragraphs in [s.17](/guidance/manual-of-patent-practice-mopp/section-17-search) and [18](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent)   Updated to take account of a standardised format for reporting plurality of invention to applicants
  [SPM 8.10.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products#refSPM8-10-1)                                                           Updated in light of Chiesi Farmaceutici S.P.A ([BL O/019/22](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O01922){rel="external"}
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [January 2022 changes]{#default-id-c97e4e9e-heading-10 .govuk-accordion__section-button} {#january-2022-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"January 2022 changes\",\"index_section\":10,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-10 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-10" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"January 2022 changes\",\"index_section\":10,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                           Update
  [7.11.1](/guidance/manual-of-patent-practice-mopp/section-7-right-to-apply-for-and-obtain-a-patent/#ref7-11-1) and [13.10.1](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-10-1)   Updated in light of Thaler v Comptroller-General of Patents \[2021\] EWCA Civ 1374
  [14.88.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-88)                                                                                                                            Updated in light of FibroGen Inc. v Akebia Therapeutics Inc \[2021\] EWCA Civ 1279.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [October 2021 changes]{#default-id-c97e4e9e-heading-11 .govuk-accordion__section-button} {#october-2021-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"October 2021 changes\",\"index_section\":11,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-11 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-11" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"October 2021 changes\",\"index_section\":11,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                                                                                                Update
  [125.17.8](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-17-8)                                                                                                                                                                                        Updated in light of Facebook Ireland Ltd v Voxer IP LLC \[2021\] EWHC 1377 (Pat)
  [SPM](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products) and [SPP](/guidance/manual-of-patent-practice-mopp/regulation-ec-no-1610-96-of-the-european-parliament-and-of-the-council-plant-protection-products)   Updated to take account of the regulatory effects of the Northern Ireland Protocol following the end of the transition period in December 2020
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [July 2021 changes]{#default-id-c97e4e9e-heading-12 .govuk-accordion__section-button} {#july-2021-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"July 2021 changes\",\"index_section\":12,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-12 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-12" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"July 2021 changes\",\"index_section\":12,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  --------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------
  Paragraph                                                                                                                         Update
  [1.29.1](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-29-1)                                             Updated in light of G 01/19 (Pedestrian simulation)
  [2.27.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-27-1)                                                   Updated in light of Claydon v Mzuri \[2021\] EWHC 1007 (IPEC)
  [55.05.1](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-05-1)   Updated in light of IPCom GmbH v Vodafone \[2021\] EWCA Civ 205
  [125.18.7](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-18-7)                                 Updated in light of Illumina Cambridge Ltd v Latvia MGI Tech SIA & Ors \[2021\] EWHC 57 (Pat)
  --------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [April 2021 changes]{#default-id-c97e4e9e-heading-13 .govuk-accordion__section-button} {#april-2021-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"April 2021 changes\",\"index_section\":13,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-13 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-13" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"April 2021 changes\",\"index_section\":13,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                               Update
  [14.82.2](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-82-2)                                                              Updated to include Illumina Cambridge Ltd v Latvia MGI Tech SIA & Ors \[2021\] EWHC 57 (Pat)
  [SPM2.01.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM2-01-1)   Updated to include Erber Aktiengesellschaft BL O/610/20
  ------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [January 2021 changes]{#default-id-c97e4e9e-heading-14 .govuk-accordion__section-button} {#january-2021-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"January 2021 changes\",\"index_section\":14,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-14 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-14" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"January 2021 changes\",\"index_section\":14,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                        Update
  [7.11.1](/guidance/manual-of-patent-practice-mopp/section-7-right-to-apply-for-and-obtain-a-patent/#ref7-11-1), [13.10.1](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-10-1)   Updated in light of Thaler v Comptroller-General of Patents \[2020\] EWHC 2412 (Pat)
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [October 2020 changes]{#default-id-c97e4e9e-heading-15 .govuk-accordion__section-button} {#october-2020-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"October 2020 changes\",\"index_section\":15,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-15 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-15" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"October 2020 changes\",\"index_section\":15,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                                                                                                                                                                                                                                                       Update
  [1.09.4](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-09-4), [1.20.3](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#red1-20-3), [1.21](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-21), [1.34.1](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-34-1), [1.38.1](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-38-1)   Updated to include Lenovo (Singapore) PTE Ltd v Comptroller General of Patents \[2020\] EWHC 1706 (Pat)
  [3.39.2](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-39-2), [3.79.1](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-79-1), [3.81.3](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-81-3)                                                                                                                                                                          Updated to include Emson v Hozelock \[2020\] EWCA Civ 871.
  [14.70.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-70-1), [14.82.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-82-1), [14.84](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-84)                                                                                                                                                                  Updated to include Regeneron Pharmaceuticals Inc v Kymab Ltd \[2020\] UKSC 27
  [76A.03.1](/guidance/manual-of-patent-practice-mopp/section-76a-biotechnological-inventions/#ref76A-03-1)                                                                                                                                                                                                                                                                                                                                       Updated to include G3/19 (Pepper)
  [SPM 8.13](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM8-13) and [SPM 13.01](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM13-01)                                                                                                                                     Updated to include Chugai Pharmaceutical Co. and Tadamitsu Kishimoto BL O/321/20
  [SPM 1.04.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM1-04-1) and [SPM 3.05.2](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-05-2)                                                                                                                               Updated to include Santen SAS v Directeur général de l'Institut national de la propriété industrielle C-673/18
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [July 2020 changes]{#default-id-c97e4e9e-heading-16 .govuk-accordion__section-button} {#july-2020-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"July 2020 changes\",\"index_section\":16,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-16 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-16" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"July 2020 changes\",\"index_section\":16,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  --------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------
  Paragraph                                                                                                                                                 Update

  [14.93.2](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-93-2),\                                                              Updated to include Akebia
  [125.17.9](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-17-9)\                                                        Therapeutics Inc v Fibrogen Inc
  [125.26.1](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-26-1)                                                         \[2020\] EWHC 866 (Pat)

  [117.06.1](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-06-1)\                           Updated to include Genentech Inc v
  [SPM12.03](/#refSPM12-03)\                                                                                                                                The Comptroller General of Patents
  [SPM12.10](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM12-10)\      \[2020\] EWCA Civ 475
  [SPM13.02](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM13-02)\      
  [SPM13.07](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM13-07)\      
  [SPM14.01.2](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM14-01-2)   

  [SPM3.02.5](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-02-5)     Updated practice as regards how the
                                                                                                                                                            Office will determine if a product
                                                                                                                                                            is protected by a Markush claim in
                                                                                                                                                            light of Royalty Pharma Collection
                                                                                                                                                            Trust v Deutsches Patent und
                                                                                                                                                            Markenamt C-650/17
  --------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [April 2020 changes]{#default-id-c97e4e9e-heading-17 .govuk-accordion__section-button} {#april-2020-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"April 2020 changes\",\"index_section\":17,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-17 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-17" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"April 2020 changes\",\"index_section\":17,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                       Update
  [Section 1](/guidance/manual-of-patent-practice-mopp/section-1-patentability)   Updated to clarify the guidance relating to artificial intelligence inventions and the excluded matter guidance more generally.
  ------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------
  [7.11.1](/guidance/manual-of-patent-practice-mopp/section-7-right-to-apply-for-and-obtain-a-patent/#ref7-11-1), [13.10.1](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-10-1)   Updated to include BL O/741/19 (Stephen L Thaler's application).
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Various paragraph in sections [14](/guidance/manual-of-patent-practice-mopp/section-14-the-application), [18](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent), [73](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative), [125](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention)   Updated in light of our changes in examination practice to ensure objections raised have a solid foundation in the law and that objections raised are necessary to the particular case.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------
  [55.04](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-04), [55.05.1](/guidance/manual-of-patent-practice-mopp/section-55-use-of-patented-inventions-for-services-of-the-crown/#ref55-05-1), [56.03](/guidance/manual-of-patent-practice-mopp/section-56-interpretation-etc-of-provisions-about-crown-use/#ref56-03)   Updated to include IPCom GmbH v Vodafone \[2020\] EWHC 132 (Pat)
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------

  ------------------ ----------------------------------------------------------------------------------------------------------------------------------------------
  Various sections   Various updates to reflect that the UK has left the EU with a transition period until December 2020, during which EU law continues to apply.
  ------------------ ----------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------
  [SPM3.02.6 to 3.02.6.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-02-6)   Updated SPC guidance in light of Court of Appeal decision in Teva & Ors v Gilead \[2019\] EWCA Civ 2272.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [January 2020 changes]{#default-id-c97e4e9e-heading-18 .govuk-accordion__section-button} {#january-2020-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"January 2020 changes\",\"index_section\":18,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-18 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-18" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"January 2020 changes\",\"index_section\":18,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                                                                                          Update
  [14.76](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-76), [14.80.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-80-1), [14.123.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-123-1)   Updated to include Anan Kasei v Neo Chemicals \[2019\] EWCA Civ 1646
  [15.07](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-07), [117.09.1](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-09-1)                                                  Updated to include BL O/769/18
  [40.04.2](/guidance/manual-of-patent-practice-mopp/section-40-compensation-of-employees-for-certain-inventions/#ref40-04-2), [41.04](/guidance/manual-of-patent-practice-mopp/section-41-amount-of-compensation/#ref41-04)                                                         Updated to include Shanks v Unilever \[2019\] UKSC 45
  [SPC](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products)                                                                                                                                                  Various parts of the SPC section have been updated to include provisions on the SPC manufacturing waiver introduced by EU Regulation 2019/933, which entered into force on 1 July 2019.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [October 2019 changes]{#default-id-c97e4e9e-heading-19 .govuk-accordion__section-button} {#october-2019-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"October 2019 changes\",\"index_section\":19,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-19 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-19" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"October 2019 changes\",\"index_section\":19,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                            Update
  [21](/guidance/manual-of-patent-practice-mopp/section-21-observations-by-third-party-on-patentability)                                                               A whole chapter review of 21 has been undertaken to more clearly set out office practice with respect to third party observations. Corresponding revisions have been made throughout the manual to ensure references are correct.
  [2.27.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-27-1)                                                                                      Updated to include Emson v Hozelock Ltd & Others \[2019\] EWHC 991 (Pat)
  [3.87-2](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-87-2)                                                                               Updated in light of Actavis Group PTC EHF v ICOS Corporation & Ors \[2019\] UKSC 15
  [17.106 - 108](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-106)                                                                                Updated to clarify office practice when there is doubt as to the presence of plurality in a claim set.
  [39.11](https://draft-origin.publishing.service.gov.uk/guidance/manual-of-patent-practice-mopp/section-39-right-to-employees-inventions/#ref39-11){rel="external"}   Updated to include Prosyscor Ltd v Netsweeper Inc & Ors \[2019\] EWHC 1302 (IPEC)
  [SPM3.04.2](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-04-2)                Updated in light of C-354/19 Novartis AG v Patent-och registreringsverket (PRV)
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [July 2019 changes]{#default-id-c97e4e9e-heading-20 .govuk-accordion__section-button} {#july-2019-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"July 2019 changes\",\"index_section\":20,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-20 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-20" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"July 2019 changes\",\"index_section\":20,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ --------
  Paragraph                                                                                                                                                                                        Update
  [14.127](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-127) Updated in light of EPO Board of Appeal decision T 1218/14 (Dohler/DuPont Nutrition Biosciences)         
  [15.21](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-21) Updated to clarify the extension regimes available when dealing with late filed divisionals.    
  [125.17.7](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-17-7) Updated in light of Regan Lab SA v Estar Medical Ltd & Ors \[2019\]EWHC 63 (Pat)                
  [SPM3.05.2](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#ref3-05-02) Updated in light of judgment C-443/17         
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ --------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [April 2019 changes]{#default-id-c97e4e9e-heading-21 .govuk-accordion__section-button} {#april-2019-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"April 2019 changes\",\"index_section\":21,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-21 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-21" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"April 2019 changes\",\"index_section\":21,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------- --------
  Paragraph   Update
  ----------- --------

  ---------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------
  [4A.](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis)\                                                       Updated in light of Warner-Lambert
  [14.72.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-72-1)\                                                          Company LLC v Generics (UK) Ltd
  [60.16](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-16)\                                                      (t.a. Mylan) & Anor. \[2018\] UKSC
  [60.19.2](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-19-2)\                                                  56.
  [125.14](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-14)                                                        

  [20.02.1](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02-1)                                                    Updated to include Akron Brass
                                                                                                                                                       Company's application BL O/012/19

  [125.17.8](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-17-8)                                                    Updated in light of Technetix B.V
                                                                                                                                                       and others v Teleste Ltd \[2019\]
                                                                                                                                                       EWHC 126 (IPEC)

  [SPM3.02.6](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#ref3-02-6)    
  ---------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [January 2019 changes]{#default-id-c97e4e9e-heading-22 .govuk-accordion__section-button} {#january-2019-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"January 2019 changes\",\"index_section\":22,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-22 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-22" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"January 2019 changes\",\"index_section\":22,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------- --------
  Paragraph   Update
  ----------- --------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  [17.03](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-03)                                                                                          Updated to note that the excess claims fee is part of the search fee so it follows that if the excess claims fee is not paid, the Patents Form 9A is considered not to have been filed.
  [17.107](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-107)                                                                                        Updated to note that if there is plurality and the first invention is excluded under s.1(2) but a later invention is not excluded, the examiner can search the first of the non-excluded inventions.
  [20.02.2](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02-2)                                                                      Updated to clarify that the compliance date does not roll over to the next working day if it falls on a non-working day (in light of Anning's Patent Application \[2007\] EWHC 2770 (Pat)).
  [125.17.6](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-17-6)                                                                      Updated in light of Icescape Ltd v Ice-World International BV & Ors \[2018\] EWCA Civ 2219.
  [SPM3.03.2](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-03-2)                  Updated in light of C-527/17.
  [SPP3.02.2](/guidance/manual-of-patent-practice-mopp/regulation-ec-no-1610-96-of-the-european-parliament-and-of-the-council-plant-protection-products/#refSPP3-02-2)   Updated to explain how authorisations under Regulations 1107/2009 are dealt with.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [October 2018 changes]{#default-id-c97e4e9e-heading-23 .govuk-accordion__section-button} {#october-2018-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"October 2018 changes\",\"index_section\":23,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-23 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-23" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"October 2018 changes\",\"index_section\":23,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------- --------
  Paragraph   Update
  ----------- --------

  --------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------
  [2.06.2](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-06-2)   Updated in light of Jushi Group Co. Ltd v OCV Intellectual Capital LLC \[2018\] EWCA Civ 1416.
  --------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------
  [14.123.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-123-1)   Updated in light of Anan Kasei Co Ltd & Rhodia Operations SAS v Molycorp Chemicals & Oxides Ltd \[2018\] EWHC 843 (Pat).
  ---------------------------------------------------------------------------------------------- --------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------
  [15.41](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-41)   Examining procedure where a divisional application is filed late such that it will be published after the end of the compliance period.
  --------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------
  [SPM3.02.6](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-02-6)   Updated in light of C-121/17
  ------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [July 2018 changes]{#default-id-c97e4e9e-heading-24 .govuk-accordion__section-button} {#july-2018-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"July 2018 changes\",\"index_section\":24,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-24 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-24" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"July 2018 changes\",\"index_section\":24,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  --------------------------------------------------------------------------------------------- -----------------------------------
  Paragraph                                                                                     Update

  [14.66.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-66-1)\   Updated in light of Regeneron
  [14.70.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-70-1)    Pharmaceuticals v Kymab Ltd
                                                                                                \[2018\] EWCA Civ 671
  --------------------------------------------------------------------------------------------- -----------------------------------

  --------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------
  [15.41](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-41)   Examining procedure where a divisional application is in order and the invention in suit was clearly claimed in the published parent application.
  --------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------
  [18](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent) and [19](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant)   Examining procedure on the intention to grant process and its interaction with the grant fee.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------ ------------------------------------------------------------
  [73.09](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-09)   Examining procedure updated with regard to s.73(2) action.
  ------------------------------------------------------------------------------------------------------------------------------------ ------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------
  [75.12](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-12)   Added judgments which discuss the use of the Comptroller's comments.
  -------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [March 2018 changes]{#default-id-c97e4e9e-heading-25 .govuk-accordion__section-button} {#march-2018-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"March 2018 changes\",\"index_section\":25,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-25 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-25" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"March 2018 changes\",\"index_section\":25,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ---------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                Update
  [3.87.2](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-87-2)   Updated to add more detail on obvious to try section, including adding a reference to Novartis AG v Generics (UK) Ltd (trading as Mylan) \[2012\] EWCA Civ 1623.
  ---------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------- --------------------------------------------------------------------------------
  [5.23.3](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-23-3)   Updated to include Unilin Beheer BV v Berry Floor NV \[2004\] EWCA (Civ) 1021.
  --------------------------------------------------------------------------------------- --------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------- ----------------------------------------------------------------------------
  [14.44](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-44)   Amended to note that drawings should not contain extensive textual matter.
  ---------------------------------------------------------------------------------------- ----------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------
  [125.06](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-06)   Updated in light of L'Oréal Ltd v RN Ventures Ltd \[2018\] EWHC 173 (Pat)
  ----------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------

  ------------ -----------------------------------------------------------------------------------------------------------------------------------------------------------------
  Throughout   The manual has been updated throughout in light of the changes to patent fees, including the introduction of a grant fee which come into force on 6 April 2018.
  ------------ -----------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [January 2018 changes]{#default-id-c97e4e9e-heading-26 .govuk-accordion__section-button} {#january-2018-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"January 2018 changes\",\"index_section\":26,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-26 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-26" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"January 2018 changes\",\"index_section\":26,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                        Update
  [2.03](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-03), [125.17.7](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-17-7)   Updated in light of Generics (U.K.) Limited and others v Yeda Research and Development Company Ltd and others \[2017\] EWHC 2629 (Pat).
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------
  [2.29.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-29-1)   Updated to note Memcor Australia Pty Ltd v Norit Membraan Technologie BV \[2003\] FSR 43.
  --------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------
  [3.87.2](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-87-2), [125.17.7](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-17-7)   Updated in light of Actavis Group PTC EHF v ICOS Corporation & Ors \[2017\] EWCA Civ 1671
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  [14.139.3](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-139-3), [125.18.6](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-18-6)   Updated to note that the test set out in Actavis v Eli Lilly \[2017\] UKSC 48 is for determining whether certain equivalents fall within the scope of protection of the claims. This test does not mean that all equivalents are protected by the claims. Therefore any general statement within the description stating that the scope of protection of the claims includes all equivalents is not allowable.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  [89B.08](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-08)   Updated to note that the only situations in which rule 106(2)(a) is used to provide a refund of the excess search fee are when an international search report becomes available before the examiner starts the search and if the larger search fee has been paid unnecessarily. Under no circumstances can rule 106(2)(a) be used to refund the reduced search fee.
  --------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [October 2017 changes]{#default-id-c97e4e9e-heading-27 .govuk-accordion__section-button} {#october-2017-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"October 2017 changes\",\"index_section\":27,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-27 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-27" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"October 2017 changes\",\"index_section\":27,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                               Update
  [15.20.1](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-20-1)                                                   Updated to note that when a divisional application is filed before the first examination report on the parent has been issued, the compliance date of the divisional is undefined until the first examination report is issued.
  [15.22](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-22)                                                       Updated to clarify that if a priority declaration on a divisional is not made or is determined to be invalid, there may be implications for the novelty of the divisional application.
  [70-70F](/guidance/manual-of-patent-practice-mopp/section-70-remedy-for-groundless-threats-of-infringement-proceedings)                                 New sections in light of the new chapters of The Patents Act added by the Intellectual Property (Unjustified Threats) Act 2017.
  [125](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention)                                                                         Updated in light of Actavis UK Limited and others v Eli Lilly and Company \[2017\] UKSC 48.
  [SPM3.02.5](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-02-5)   Updated in light of Sandoz Ltd & Anor v G.D. Searle & Anor \[2017\] EWHC 987 (Pat)
  ------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [July 2017 changes]{#default-id-c97e4e9e-heading-28 .govuk-accordion__section-button} {#july-2017-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"July 2017 changes\",\"index_section\":28,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-28 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-28" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"July 2017 changes\",\"index_section\":28,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                   Update
  [5.25.1-3](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-23)                                                                                       Updated to include Novartis AG v Johnson & Johnson \[2009\] EWHC (Pat) 1671 and to provide further guidance on partial and poisonous priority.
  [14.120.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-120-1)                                                                                A claim is a product by process claim if a product claim includes any method or process steps. Also, a product by process claim will result if a product claim incorporates a method or process step by reference to another claim.
  [14.137](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-137)                                                                                    Trademarks such as Bluetooth (RTM) and Wi-Fi (RTM) may be allowable in claims their use in certain claims may be regarded as unavoidable.
  [18.07.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07-1)                                           Updating the procedure for dealing with requests under the Patent Prosecution Highway.
  [18.07.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07-2)                                           When there is insufficient time remaining on the compliance date to allow for three months of publication, the issue of an intention to grant letter should be delayed to allow as much time as possible for section 21 observations to be filed without delaying beyond the compliance date.
  [19.21](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-21)                                                      Updating the procedure for processing voluntary amendments received after a report under s.18(4).
  [123.10.3](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-10-3), [123.47-47.2](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-47)   Updated to provide further guidance on the procedure for dealing with lost, misdirected or delayed correspondence.
  [SPM3.02.6](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-02-6)                       Updated in light of Teva UK & Ors v Gilead \[2017\] EWHC 13 (Pat).
  [SPM3.04](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-04)                           Updated to include C-577/13 Actavis Group PTC EHF, Actavis UK Ltd v Boehringer Ingelheim Pharma GmbH & Co. KG.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [March 2017 changes]{#default-id-c97e4e9e-heading-29 .govuk-accordion__section-button} {#march-2017-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"March 2017 changes\",\"index_section\":29,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-29 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-29" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"March 2017 changes\",\"index_section\":29,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------
  Paragraph                                                                                                                                                Update

  [4A.31](/guidance/manual-of-patent-practice-mopp/section-4-industrial-application/#ref4A-31)                                                             Updated in light of Generics (UK)
                                                                                                                                                           Ltd (t/a Mylan) v Warner-Lambert
                                                                                                                                                           Company LLC \[2016\] EWCA Civ 1006.

  [14.117.4](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-117-4)                                                             Updated in light of Napp
                                                                                                                                                           Pharmaceutical Holdings Ltd v Dr
                                                                                                                                                           Reddy's Laboratories (UK) Ltd
                                                                                                                                                           \[2016\] EWCA Civ 1053.

  [14.120](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-120)                                                                 Updated in light of Acumen Design
                                                                                                                                                           Associates Ltd BL O/031/17.

  [18.86.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86-2)                        Requests to forgo the intention to
                                                                                                                                                           grant period are only able to be
                                                                                                                                                           accepted after the notification of
                                                                                                                                                           intention to grant letter has been
                                                                                                                                                           issued.

  [18.86.4](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86-4)                        A notification of intention to
                                                                                                                                                           grant will be sent out on all
                                                                                                                                                           applications including applications
                                                                                                                                                           that are found in order for grant
                                                                                                                                                           near or after the compliance date.

  [18.86.5](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86-5)                        Rule 108(2) and rule 108(3) can be
                                                                                                                                                           used to extend the compliance
                                                                                                                                                           period after the notification of
                                                                                                                                                           intention to grant letter has been
                                                                                                                                                           sent out providing the application
                                                                                                                                                           has not been sent to grant.
                                                                                                                                                           Therefore it would be good practice
                                                                                                                                                           for the applicant to request any
                                                                                                                                                           extension to the compliance period
                                                                                                                                                           before the date set out in the
                                                                                                                                                           notification of intention to grant
                                                                                                                                                           letter.

  [19.21](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-21)\                                  Indicating the procedure for
  [19.22.1](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-22-1)                               processing voluntary amendments
                                                                                                                                                           received after a report under
                                                                                                                                                           s.18(4).

  [25.12](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-12)                                                                    Indicating that, from 6 April 2017,
                                                                                                                                                           a renewal notice is issued to the
                                                                                                                                                           last address specified for the
                                                                                                                                                           purpose of receiving renewal
                                                                                                                                                           notices by the proprietor.

  [27.09.1](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-09-1)                              Updated in light of MWUK Ltd v
                                                                                                                                                           Fashion at Work Ltd BL O/531/16.

  [60.16.1](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-16-1)\                                                      Updated in light of Generics (UK)
  [60.19.2](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-61-2)                                                       Ltd (t/a Mylan) v Warner-Lambert
                                                                                                                                                           Company LLC \[2016\] EWCA Civ 1006.

  [SPM1.04.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM1-04-1)\   Updated in light of Abraxis
  [SPM1.04.2](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#ref1-04-2)\      Bioscience LLC v the Comptroller
  [3.05](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-05)           General of Patents \[2017\] EWHC 14
                                                                                                                                                           (Pat).

  [SPM5.04](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#ref5-04)           To assist in determining if
                                                                                                                                                           particular compositions or acts
                                                                                                                                                           infringe an SPC it is possible to
                                                                                                                                                           request this Office gives a
                                                                                                                                                           non-binding opinion on
                                                                                                                                                           infringement.
  -------------------------------------------------------------------------------------------------------------------------------------------------------- -----------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [December 2016 changes]{#default-id-c97e4e9e-heading-30 .govuk-accordion__section-button} {#december-2016-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"December 2016 changes\",\"index_section\":30,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-30 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-30" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"December 2016 changes\",\"index_section\":30,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     Update
  [Section 1](/guidance/manual-of-patent-practice-mopp/section-1-patentability), [Section 2](/guidance/manual-of-patent-practice-mopp/section-2-novelty), [Section 3](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step), [Section 14](/guidance/manual-of-patent-practice-mopp/section-14-the-application), [Section 17](/guidance/manual-of-patent-practice-mopp/section-17-search), [Section 18](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent) and [Section 60](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement)   Each of these sections has been updated to have a contents section. On the [online version](/guidance/manual-of-patent-practice-mopp) of MoPP this appears as 'collapsible' headings, while on the PDF version there are headings which link to the relevant part of each section.
  Throughout                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    The manual has been updated throughout to remove references to the ambiguous term "early publication". This term has been replaced with either "A-publication" or "accelerated publication" as appropriate.
  [14.139.2](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-139-2) and [14.144](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-144)                                                                                                                                                                                                                                                                                                                                                                                                                                     Updated to provide additional examples of where support objections may arise.
  [14.148](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-148)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      To encourage the use of compact consistory clauses, where the statement of invention refers to the claims. This avoids repetition and often removes the necessity for redrafting the description following amendment of the claims.
  [15.21](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-21)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Discretion under r.108(3), in relation to extending the compliance period to allow a divisional application to be filed, will normally be exercised only if the applicant shows that the circumstances are exceptional and that they have been properly diligent.
  [15.41](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-41)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Examining procedure where a divisional application is in order and the invention in suit was clearly claimed in the published parent application.
  [18.03.6](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-03-6)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Examining procedure where an application is in order for grant directly after a supplementary search.
  [18.86.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86-2)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             Where the applicant would like an application to grant sooner than the date indicated in a notification of intention to grant letter, a written request must be submitted.
  [21.14](/guidance/manual-of-patent-practice-mopp/section-21-observations-by-third-party-on-patentability/#ref21-14)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           To clarify procedure where third party observations are received before the issue of a report under s.18(4) but too late to prevent its issue.
  [89B.12.2](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89b-12-2)                                                                                                                                                                                                                                                                                                                                                                                                                                                                               If the substantive examiner disagrees with the International Search Authority's assessment of plurality, and as a result determines that further searching is needed (bearing in mind 89B.12), this should be carried out without requiring an additional fee.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [October 2016 changes]{#default-id-c97e4e9e-heading-31 .govuk-accordion__section-button} {#october--2016-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"October  2016 changes\",\"index_section\":31,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-31 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-31" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"October  2016 changes\",\"index_section\":31,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   Update
  [14.04.13](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-04-13)                                                                                                                                                                                                                                                                                                                                                                                                                                                It is no longer necessary to file duplicate 51s when one agent replaces another
  [14.93-94](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-93)                                                                                                                                                                                                                                                                                                                                                                                                                                                   A reference in a patent specification to a document containing information which is not essential for sufficiency need not be considered by the examiner; such references are allowable
  [14.124](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-124)                                                                                                                                                                                                                                                                                                                                                                                                                                                    From 6 April 2017 it will no longer be possible to include omnibus claims in UK patent applications, unless this is the only way to define the technical features of the invention clearly and concisely
  [15.46](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-46)                                                                                                                                                                                                                                                                                                                                                                                                                                           To reflect that a notification of intention to grant will be issued before a patent is granted
  [17.55](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-55)                                                                                                                                                                                                                                                                                                                                                                                                                                                               To clarify that not all national document collections are classified to the CPC scheme, and a search should cover documents only classified to IPC
  [17.115](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-115)                                                                                                                                                                                                                                                                                                                                                                                                                                                             To indicate that where a priority claim is invalid or relinquished, a top-up search before 21 months from filing may not uncover all relevant s.2(3) prior art because some documents may not yet be published
  [18.81](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-81)                                                                                                                                                                                                                                                                                                                                                                                                               To reflect notification of intention to grant procedure when an application is in order at first examination
  [18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86), [18.86.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86-1), [18.86.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86-2) and [18.86.3](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86-3)   Details of the notification of intention to grant procedure
  [19.15.1](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-15-1)                                                                                                                                                                                                                                                                                                                                                                                                                  Where a PCT application has entered the UK national phase and an international search report was issued during the international phase, the applicant may amend the specification of his own volition from the date of national phase entry until the date the first examination report is issued
  [20A.03](/guidance/manual-of-patent-practice-mopp/section-20a-reinstatement-of-applications/#ref20A-03)                                                                                                                                                                                                                                                                                                                                                                                                                                     Reflecting simplified reinstatement deadline: a reinstatement request must be filed within 12 months beginning immediately after the date on which the application was terminated
  [27.11.1](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-11-1)                                                                                                                                                                                                                                                                                                                                                                                                                 Indicating that from 6 April 2017 it will not be possible to amend a granted patent to insert an omnibus claim
  [32.06](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-06)                                                                                                                                                                                                                                                                                                                                                                                                                                             Rule 49(1) provides that any person may request that a correction or change be entered in the register or made to any application or other document filed at the Office in respect of his name or his address, or that a correction be made to his address for service
  [75.07](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-07)                                                                                                                                                                                                                                                                                                                                                                                                        When amendments have been proposed during a hearing or following an interim decision the comptroller has discretion as to whether to advertise them in the Journal
  [123.10.3](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-10-3)                                                                                                                                                                                                                                                                                                                                                                                                                                                         Updated in light of Cypress Semiconductor Corporation's Application ([BL O/326/16(PDF, 88.5 KB)](https://www.ipo.gov.uk/p-challenge-decision-results/o32616.pdf){rel="external"})
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [July 2016 changes]{#default-id-c97e4e9e-heading-32 .govuk-accordion__section-button} {#july-2016-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"July 2016 changes\",\"index_section\":32,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-32 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-32" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"July 2016 changes\",\"index_section\":32,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------- --------
  Paragraph   Update
  ----------- --------

  --------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------
  [4A.08 to 4A.10](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-08)                      Expanded guidance on methods of surgery, based on Enlarged Board of Appeal decision G 01/07 and Virulite Ltd's Application [BL
                                                                                                                                          O/058/10](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F058%2F10&submit=Go+%BB){rel="external"}

  [4A.22](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-22)\                              Revised guidance on first and second medical use claims in relation to inactive carriers or excipients, non-medical uses and apparatus
  \                                                                                                                                       
  [4A.28.3](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-28-3)\                          
  \                                                                                                                                       
  [4A.31](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-31)                               

  [4A.31](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-31)                               Expanded guidance on novelty of second medical use claims

  [17.96.1 to 17.96.2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-96-1)                                            Updated guidance on how examiners should respond when search would serve no useful purpose -- indicating that issuing an examination report before search
                                                                                                                                          is very rarely appropriate

  [17.111](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-111)                                                         To indicate that the filing of a large number of further searches (under s.17(6)) is undesirable -- for efficiency and fairness the examiner should
                                                                                                                                          consider carrying out fewer searches than requested

  [17.113](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-113)                                                         Where multiple search requests are filed before the initial search report has issued, the search examiner will normally search only the first invention

  [17.122](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-122)                                                         Clarifying the circumstances in which a further search and fee (under s.17(8)) may be required

  [18.10.1 to                                                                                                                             Updated to clarify practice around family matching, equivalents, and online file inspection
  18.10.4](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-10-1)\       
  \                                                                                                                                       
  [18.64](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-64)\          
  \                                                                                                                                       
  [18.85](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-85)           

  [73.04.1 to                                                                                                                             Updated procedure for revocation process under s.73(1A)
  73.04.3](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-04-1)   
  --------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [April 2016 changes]{#default-id-c97e4e9e-heading-33 .govuk-accordion__section-button} {#april-2016-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"April 2016 changes\",\"index_section\":33,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-33 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-33" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"April 2016 changes\",\"index_section\":33,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ---------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                            Update

  [1.15.1](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-15-1)                                                                Updated in light of Astron Clinica Ltd & Ors v The Comptroller General of Patents, Designs and Trade Marks \[2008\]

  [2.12.3](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-12-3)\                                                                     Updated to clarify interpretation of "adapted to" and "constructed to"
  [2.12.3.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-12-3-1)                                                                  

  [2.22.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-22-1)                                                                      In light of Unwired Planet International Ltd v Huawei Technologies Co Ltd & Ors \[2015\] EWHC 3366

  [3.30.3](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-30-3)                                                               In light of Generics (UK) Ltd (t/a Mylan) v Warner-Lambert Company LLC \[2015\] EWHC 2548

  [17.120](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-120)                                                                      Updated to clarify the circumstances where request may be made under s.17(8) for a further search

  [27.11](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-11)                              To remove reference to Enlarged Board of Appeal decision G 3/14

  [100.04](/guidance/manual-of-patent-practice-mopp/section-100-burden-of-proof-in-certain-cases/#ref100-04)                                           In light of Elektron Ltd v Molycorp Chemicals & Oxides (Europe) Ltd & Anor \[2015\] EWHC 3596

  [SPM                                                                                                                                                 Updated in light of [BL
  3.02.6](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-02-6)\   O/117/16](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F117%2F16&submit=Go+%BB){rel="external"}
  [SPM                                                                                                                                                 
  3.03.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-03-1)    
  ---------------------------------------------------------------------------------------------------------------------------------------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [January 2016 changes]{#default-id-c97e4e9e-heading-34 .govuk-accordion__section-button} {#january-2016-changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"January 2016 changes\",\"index_section\":34,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-34 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-34" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"January 2016 changes\",\"index_section\":34,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ------------------------------------------------------------------------------------------------------------------------------------------------------ -----------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                                                                                                              Update

  [4A.29.1](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-29-1)\                                         In light of Hospira UK Ltd v Genentech Inc \[2014\] EWHC 1094, Warner-Lambert Company, LLC v Actavis Group Ptc EHF & Ors \[2015\] EWCA Civ 556, and
  \                                                                                                                                                      Generics (UK) Ltd (t/a Mylan) v Warner-Lambert Company LLC \[2015\] EWHC 2548
  [4A.31](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-31)                                              

  [5.23.2](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-23-2)                                                                  Hospira UK Ltd v Genentech Inc \[2014\] EWHC 1094

  [14.124](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-124)                                                               Updated in light of Jansen Betonwaren B.V. v Ian Robbie Christie ([BL
                                                                                                                                                         O/496/15](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F496%2F15&submit=Go+%BB){rel="external"}) on
                                                                                                                                                         the construction of omnibus claims

  [18.97](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-97)                          To indicate that assessment of conflict under sections 18(5) and 73(2) is unaffected by Koninklijke Philips Electronics n.v. v Nintendo of Europe GmbH
                                                                                                                                                         \[2014\] EWHC 1959 (Pat)

  [20.08](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-08)                                                          Updated in light of Aujla v Sanghera \[2004\] EWCA Civ 121

  [27.11](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-11)                                Relating to EPO Enlarged Board of Appeal decision G3/14 and the examination of post grant amendments unders.14(5)

  [60.16.1](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-16-1)\                                                    In light of Generics (UK) Ltd (t/a Mylan) v Warner-Lambert Company LLC \[2015\] EWHC 2548, Warner-Lambert Company, LLC v Actavis Group Ptc EHF & Ors
  \                                                                                                                                                      \[2015\] EWCA Civ 556, and Actavis UK Ltd & Ors v Eli Lilly & Company \[2015\] EWCA Civ 555
  [60.19.2](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-19-2)                                                     

  [75.05.1](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-05-1)               Indicating criteria for objecting to conflict resulting from post grant amendment, as given in Koninklijke Philips Electronics n.v. v Nintendo of Europe
                                                                                                                                                         GmbH \[2014\] EWHC 1959 (Pat)

  [123.47.1](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-47-1)                                                                    Providing further guidance on procedure for responding to reports of lost post

  [SPM sections:                                                                                                                                         SPC section updated in light of Angiotech Pharmaceuticals Inc. and University of British Columbia ([BL
  2.01](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM2-01)\         O/466/15](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F466%2F15&submit=Go+%BB){rel="external"}),
  \                                                                                                                                                      Leibniz-Institut für Neue Materialien Gemeinnützige GmbH ([BL
  [3.03.2](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-03-2)\    O/328/14](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F328%2F14&submit=Go+%BB){rel="external"})
  \                                                                                                                                                      and Seattle Genetics Inc. v Österreichisches Patentamt (C-471/14)
  [8.03.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM8-03-1)\    
  \                                                                                                                                                      
  [13.05.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM13-05-1)   
  ------------------------------------------------------------------------------------------------------------------------------------------------------ -----------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [2015 changes]{#default-id-c97e4e9e-heading-35 .govuk-accordion__section-button} {#changes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"2015 changes\",\"index_section\":35,\"index_section_count\":35}"}
:::

::: {#default-id-c97e4e9e-content-35 .govuk-accordion__section-content aria-labelledby="default-id-c97e4e9e-heading-35" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"2015 changes\",\"index_section\":35,\"index_section_count\":35}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### October 2015 changes

  ------------------------------ ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                      Update
  2.12.3                         Updated in light of Schenck Rotec GmbH v Universal Balancing Limited \[2012\] EWHC 1920 regarding construction of the claimed phrase "constructed to receive"
  14.117.2-3                     In light of Smith & Nephew Plc v Convatec Technologies Inc \[2015\] EWCA Civ 607
  17.102                         To indicate that the search examiner must classify applications even if an abstract has not been filed
  21.02, 21.02.1, 21.03, 21.05   To reflect that Examiner Assistants are taking over some administrative aspects of handling s.21 observations, and to indicate that requests for anonymity will be treated as requests for confidentiality
  29.03                          Updated in light of Genentech Inc's patent (BL O/360/14)
  73.07                          To indicate that the RC31 PROSE clause has been updated so that it no longer needs to be modified when an EP equivalent has already been granted
  123.47.1                       To indicate that r.111 cannot be used to extend a time period which has already expired, and to reflect procedure for handling reports of lost or misdirected correspondence
  ------------------------------ ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### July 2015 changes

  --------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                         Update
  14.41,Code of Practice: point 5   Indicating that drawings may include shading, that black and white photographs are allowable so long as they are clear enough to be reproduced, and that colour drawings and photographs are not allowable
  17.115                            Clarifying when databases are up to date for top-up searching purposes
  17.94.5                           Indicating that an Action Before Search (ABS) letter should not be issued as a response to a request for a further search under s.17(6)
  18.47.1                           Corrected to indicate that an Abbreviated Exam Report based on an IPRP should be booked as a completed examination in PAFS
  18.52 18.80                       Practice in relation to offering hearings clarified
  76.15.0                           Updated in light of Koninklijke Philips Electronics NV v Nintendo of Europe GmbH \[2014\] EWHC 1959 (Pat)
  SPM 8.10                          Updated in light of Otsuka Pharmaceuticals Company Limited
  Notices                           Updated to include recent Practice Notices
  Table of cases                    Updated accordingly
  --------------------------------- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### April 2015 changes

  ------------------------------------------------------------ -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph                                                    Update
  1.17.2, 1.21, 1.21.2, 1.38.5                                 Updated in light of Lantana Ltd v Comptroller General of Patents \[2014\] EWCA Civ 1463 to give further information on the relationship between excluded matter with novelty and inventive step, the AT&T signposts, and on determining 'the contribution'.
  14.117.2                                                     Updated in light of Smith & Nephew Plc v Convatec Technologies Inc & Anor \[2013\] EWHC 3955 to give further guidance on determining the limits of a numerical range.
  14.135                                                       Updated in view of Jarden Consumer Solutions (Europe) Ltd v SEB SA & Anor \[2014\] EWCA Civ 1629.
  14.171,15A.03, 17.03, 17.94.5                                To indicate the procedures of the Private Applicant Unit.
  18.47.1, 89B.15.1                                            Updated to indicate that IPRPs or IPERs may be reissued as an abbreviated examination report, when national phase examination takes place.
  61.04.3                                                      Updated in light of Les Laboratoires Servier & Anor v Apotex Inc & Ors (Rev 1) \[2014\] UKSC 55.
  75.15.1                                                      Updated in light of Ability International v Monkey Tower BL O/484/14.
  76.15.0                                                      Updated in light of AP Racing Ltd v Alcon Components Ltd \[2014\] EWCA Civ 40.
  76A.02.01 76A.02.02                                          Updated in light of International Stem Cell Corporation (Judgment) \[2014\] EUECJ C-364/13.
  89.03, 89.03.1, 89A.07, 89A.14.1, 89A.14.2, 89B.10-89B12.1   Giving information on Supplementary International Searches, and on the circumstances in which re-searching and top-up searching are performed in the national phase.
  89B.12.2                                                     To reflect procedure where the International Searching Authority has issued a declaration under a. 17(2) of the PCT that no International Search Report has been established.
  89B.16                                                       Indicating the procedure for considering third party observations received in the international phase.
  130.05                                                       Updated in light of Future New Developments Ltd v B & S Patente Und Marken GmbH \[2014\] EWHC 1874 (IPEC).
  SPM1.04.1                                                    Updated in light of Forsgren v Österreichisches Patentamt C-631/13.
  SPM3.02.4                                                    Updated in light of Ichan School of Medicine at Mount Sinai BL O552/14.
  SPM3.02.6                                                    Updated in light of Eli Lilly & Company v Human Genome Sciences Inc \[2014\] EWHC 2404 (Pat).
  SPM8.03.1, SPM13.05.1                                        Updated in light of Seattle Genetics Inc. C-471/14.
  Table of cases, Index                                        Updated accordingly
  ------------------------------------------------------------ -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### January 2015 changes

  ----------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  Paragraph               Update
  2.08                    Updated to reflect that prior disclosures should be read in light of CGK of skilled person at the date of disclosure (as in Teva UK Limited & another v AstraZeneca AB \[2014\] EWHC 2873)
  3.30.0                  Updated in light of Teva UK Limited & another v AstraZeneca AB \[2014\] EWHC 2873 (Pat) to indicate that CGK may in some circumstances include material readily identified in a literature search
  4.05                    Update to refer to Peter Crowley v United Kingdom Intellectual Property Office \[2014\]
  4A.27.1 4A.12 18.97.1   Updated in light of EPO Technical Board of Appeal decisions in T 250/05, T 1790/12 and T 879/12 relating to double patenting and 2nd medical use claims
  15.21                   Updated in light Fergusson's Application, BL O/272/09 and Knauf Insulation's Application BL O/098/13
  17.77                   Updated to clarify practice on the use of the phrase "at least" in search reports
  17.05.1                 Updated to clarify that how the Office considers Green Channel acceleration requests
  17.45 17.104.1 18.10    Updated to reflect Office practice on requiring or providing translations of citations
  61.07                   Updated to refer to IPcom GmbH & Co Kg v HTC Europe Co Ltd & Ors \[2013\] EWCA Civ 1496
  71.02                   Updated to include reference to Actavis v Pharmacia \[2014\] EWHC 2265 (Pat) and IPcom GmbH & Co Kg v HTC Europe Co Ltd & Ors \[2013\] EWCA Civ 1496
  72.42                   Update to clarify the circumstances in which amendments proposed during revocation or infringement proceedings must be advertised
  76.15.5                 Updated in light of Teva UK Limited & another v AstraZeneca AB \[2014\] EWHC 2873 (Pat)
  76.15.2 76.26           Updated information relating to 2nd medical use claims
  123.47.1                Updated to clarify use of rule 111 in light of BL O/234/14
  ----------------------- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
