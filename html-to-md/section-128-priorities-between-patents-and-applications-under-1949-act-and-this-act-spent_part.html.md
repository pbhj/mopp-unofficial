::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 128: Priorities between patents and applications under 1949 Act and this Act \[spent\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (128.01) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 128.01

This section provided transitional provisions to resolve questions of
priority between 1949 Act and 1977 Act patents and applications
(including European patents (UK)), and is now spent.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 128(1)**
  The following provisions of this section shall have effect for the purpose of resolving questions of priority arising between patents and applications for patents under the 1949 Act and patents and applications for patents under this Act.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 128(2)**

  A complete specification under the 1949 Act shall be treated for the
  purposes of sections 2(3) and 5(2) above\
  (a) if published under that Act, as a published application for a
  patent under this Act;\
  (b) if it has a date of filing under that Act, as an application for a
  patent underthis Act which has a date of filing under this Act;\
  \
  and in the said section 2(3), as it applies by virtue of this
  sub-section in relation to any such specification, the words "both as
  filed and" shall be omitted.
  -----------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 128(3)**
  In section 8(1), (2) and (4) of the 1949 Act (search for anticipation by prior claim) the references to any claim of a complete specification, other than the applicant's, published and filed as mentioned in section 8(1) shall include references to any claim contained in an application made and published under this Act or in the specification of a patent granted under this Act, being a claim in respect of an invention having a priority date earlier than the date of filing the complete specification under the 1949 Act.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 128(4)**

  In section 32(1)(a) of the 1949 Act (which specifies, as one of the
  grounds of revoking a patent, that the invention was claimed in a valid
  claim of earlier priority date contained in the complete specification
  of another patent), the reference to such a claim shall include a
  reference to a claim contained in the specification of a patent granted
  under this Act (a new claim) which satisfies the following conditions\
  (a) the new claim must be in respect of an invention having an earlier
  priority date than that of the relevant claim of the complete
  specification of the patent sought to be revoked; and\
  (b) the patent containing the new claim must be wholly valid or be
  valid in those respects which have a bearing on that relevant claim.
  -----------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 128(5)**
  For the purposes of this section and the provisions of the 1949 Act mentioned in this section the date of filing an application for a patent under that Act and the priority date of a claim of a complete specification under that Act shall be determined in accordance with the provisions of that Act, and the priority date of an invention which is the subject of a patent or application for a patent under this Act shall be determined in accordance with the provisions of this Act.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
