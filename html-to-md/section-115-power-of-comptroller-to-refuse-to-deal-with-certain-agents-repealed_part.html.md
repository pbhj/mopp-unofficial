::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 115: Power of comptroller to refuse to deal with certain agents \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (115.01 - 115.02) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 115.01

This section authorised the provisions of rule 90 of the 1982 Rules
whereby the comptroller could refuse to recognise persons in certain
categories as agents. It also concerned recognition of agents not
residing or having a place of business in the UK.

### 115.02

Section 115, together with other provisions of the 1977 Act concerning
patent agents, has been repealed by the CDP Act and replaced by Part V
of the CDP Act (Patent Agents and Trade Mark Agents), see particularly
s.281 discussed in
[281.01-04](/guidance/manual-of-patent-practice-mopp/section-281-power-of-comptroller-to-refuse-to-deal-with-certain-agents/#ref281-01).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
