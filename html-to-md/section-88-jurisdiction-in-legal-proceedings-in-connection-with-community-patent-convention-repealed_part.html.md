::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 88: Jurisdiction in legal proceedings in connection with Community Patent Convention \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (88.01) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 88.01

The CDP Act repealed section 88 of the 1977 Act, as this section had
been rendered otiose by the coming into force in respect of the UK of
the "Judgments Convention" (the 1968 Convention on Jurisdiction and
Enforcement of Judgments in Civil and Commercial Matters). Section 88
specified how to determine the residence of a party for the purposes of
deciding which courts of the Member States of the then European
Community had jurisdiction in proceedings relating to infringement of a
patent granted under the Community Patent Convention. It was introduced
as an interim measure because the Community Patent Convention was
expected to enter into force before the Judgments Convention - which
also specifies how this question is to be determined - became effective
in the UK. In the event the Community Patent Convention has not entered
into force, whereas the Judgments Convention is now effective.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
