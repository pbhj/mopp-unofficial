::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 26: Patent not to be impugned for lack of unity {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (26.01 - 26.02) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------
   

  **Section 26**

  No person may in any proceeding object to a patent or to an amendment
  of a specification of a patent on the ground that the claims contained
  in the specification of the patent, as they stand or, as the case may
  be, as proposed to be amended, relate -\
  (a) to more than one invention, or\
  (b) to a group of inventions which are not so linked as to form a
  single inventive concept.
  -----------------------------------------------------------------------

### 26.01 {#ref26-01}

s.18(4), s.14(5)(d) is also relevant.

Before a patent is granted the examiner must have reported that the
application complied with all the requirements of the Act and Rules,
including the stipulation that the claims should have unity of
invention. S.26 makes clear however that even if the examiner has
wrongly decided that there is unity, the grant is in no way invalidated.
Nor may an opponent to an application to amend under s.27 or 75 object
that a proposed amendment would introduce plurality of invention.
Moreover this matter may not be argued in any other proceedings, for
example in an application for judicial review.

### 26.02 {#ref26-02}

S.26 is not regarded as fettering the comptroller's discretion to refuse
or allow amendment under s.27 or 75. Nonetheless it is not the practice
of the Office to withhold consent to an amendment solely because the
amended specification would relate to more than one invention.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
