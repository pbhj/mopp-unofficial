::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 76A: Biotechnological inventions {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (76A.01 - 76A.10) last updated: January 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 76A.01 {#ref76A-01}

This section was introduced by the Patents Regulations 2000 (SI
2000/2037), which implemented articles 1-11 of Directive 98/44/EC on the
legal protection of biotechnological inventions. The Patents Act 1977
(Isle of Man) Order 2003 (SI 2003/1249) brought this section into force
in the Isle of Man.

\[The Examination Guidelines for Patent Applications relating to
[Biotechnological
Inventions](https://www.gov.uk/government/publications/examining-patent-applications-for-biotechnological-inventions)
in the Intellectual Property Office provide further details on practice
in this field.\]

### 76A.01.1 {#ref76A-01-1}

Under the provisions of the European Union (Withdrawal) Act 2018 ("the
Withdrawal Act"), EU-derived domestic legislation and direct EU
legislation in force in the UK at 11pm on 31 December 2020 was saved or
incorporated into domestic law. This includes the Patents Regulations
2000, which continues to have effect as assimilated law under section 2
of the Withdrawal Act. As it is not direct EU legislation, Directive
98/44/EC no longer has effect in the UK; however, it remains relevant
for interpreting the provisions as implemented in UK law, as does case
law of the Court of Justice of the European Union issued before 31
December 2020, as set out in section 6 of the Withdrawal Act. Courts and
tribunals below the level of the Court of Appeal remain bound to follow
the principles and interpretations provided by this assimilated case
law, as is the Office - in particular, as it relates to the processing
and examination of patent applications.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 76A(1)**
  Any provision of, or made under, this Act is to have effect in relation to a patent or an application for a patent which concerns a biotechnological invention, subject to the provisions of Schedule A2.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Patentability of biotechnological inventions

Sch. A2 paras 1 & 3 is also relevant.

### 76A.02 {#ref76A-02}

The provisions of Schedule A2 to the Patents Act 1977 (as amended)
repeat and paras 1& 3 expand upon the exclusions from patentability for
animals, plants and biological processes that were contained in
s.1(3)(b) of the Act before it was repealed by the Patents Regulations
2000 ([see
1.41-1.46](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-41)).
The Schedule states that an invention shall not be considered
unpatentable solely on the ground that it concerns (a) a product
consisting of or containing biological material; or (b) a process by
which biological material is produced, processed or used (the definition
of "biological material" being inserted into s.130(1) - [see
130.04.1](/guidance/manual-of-patent-practice-mopp/section-130-interpretation/#ref130-04-1)).
However, it then sets out the following as not being patentable
inventions:

\(a\) the human body, at the various stages of its formation and
development, and the simple discovery of one of its elements, including
the sequence or partial sequence of a gene;

\(b\) processes for cloning human beings;

\(c\) processes for modifying the germ line genetic identity of human
beings;

\(d\) uses of human embryos for industrial or commercial purposes;

\(e\) processes for modifying the genetic identity of animals which are
likely to cause them suffering without any substantial medical benefit
to man or animal, and also animals resulting from such processes;

\(f\) any variety of animal or plant or any essentially biological
process for the production of animals or plants, not being a
micro-biological or other technical process or the product of such a
process.

### 76A.02.1 {#ref76A-02-1}

In C-34/10 ("Brüstle"), the Court of Justice of the European Union
(CJEU) ruled on the interpretation of Article 6(2)(c) of Directive
98/44/EC, which corresponds with paragraph 3(d) of Schedule A2 of the
Act. The CJEU ruled that, for the purposes of Article 6(2)(c), the term
"human embryo" must be interpreted broadly to include any organism that
is "capable of commencing the process of development of a human being".
In paragraphs 35-38 the

CJEU held that the term "human embryo" included:

-   any human ovum after fertilisation, if that fertilisation is such as
    to commence the process of development of a human being;
-   a non-fertilised human ovum into which the cell nucleus from a
    mature human cell has been transplanted, insofar as it is capable of
    commencing the process of development of a human being;
-   a non-fertilised human ovum whose division and further development
    have been stimulated by parthenogenesis, insofar as it is capable of
    commencing the process of development of a human being.

In
[C-364/13](http://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A62013CJ0364){rel="external"}
("International Stem Cell Corporation"), the CJEU further clarified the
definition of the term "embryo" for the purposes of Article 6(2)(c) of
the Directive, by ruling that:\
\

> an unfertilised human ovum whose division and further development have
> been stimulated by parthenogenesis does not constitute a 'human
> embryo' within the meaning of that provision \[i.e. Article 6(2)(c) of
> the Directive\], if, in the light of current scientific knowledge, it
> does not, in itself, have the inherent capacity of developing into a
> human being.

### 76A.02.2 {#a022}

In light of the aforementioned decisions, the examination practice for
inventions involving human embryonic stem cells is set out below, with
the term "human embryo" interpreted in accordance with those decisions:\
\
(i) Processes for obtaining stem cells from human embryos\
The Office will not grant patents for processes of obtaining stem cells
from human embryos because they involve the use of human embryos for
industrial or commercial purposes, as excluded under paragraph 3(d) of
schedule 2 of the Act.\
\
(ii) Human totipotent cells\
\
Human totipotent cells have the potential to develop into an entire
human body. In view of this potential, such cells are not patentable
because the human body at the various stages of its formation and
development is excluded from patentability by paragraph 3(a) of Schedule
A2 to the Act. The Office will therefore not grant patents for human
totipotent cells.\
\
(iii) Inventions requiring the destruction of human embryos\
\
In Brüstle, the CJEU ruled that use of human embryos within the meaning
of Article 6(2)(c) of the Directive occurs if the implementation of the
invention requires the destruction of human embryos, even if the claims
of the patent do not refer to the use of human embryos. The CJEU also
ruled that the destruction may occur at any stage, including long before
the implementation of the invention. Thus, where the implementation of
an invention requires the use of cells that originate from a process
which requires the destruction of a human embryo, the invention is not
patentable according to paragraph 3(d) of Schedule A2 to the Act. For
example, where the implementation of the invention requires the use of a
human embryonic stem cell line, the establishment of which originally
required the destruction of a human embryo, the invention is not
patentable.\
\
(iv) Human stem cells not derived from human embryos\
\
Patents for inventions concerning human stem cells that are not derived
from human embryos, such as induced pluripotent cells and adult stem
cells, will be granted, provided they satisfy the other requirements for
patentability.\
\
(v) Inventions for therapeutic or diagnostic purposes\
\
The CJEU judgment in Brüstle confirmed that inventions that are for
therapeutic or diagnostic purposes that are applied to and useful to the
human embryo are not excluded from patentability. The Office will
continue to grant patents for such inventions, provided they meet the
other legal requirements.\
\
Further details of examination practice in relation to inventions
involving human embryonic stem cells can be found in the Practice Notice
dated March 2015.

### 76A.02.3 {#ref76A-02-3}

In the Harvard "oncomouse" case T 315/03 (\[2006\] 1 OJEPO 15, \[2005\]
EPOR 31), the EPO Board of Appeal held that an assessment of an
objection to patentability under r.23 (d) EPC \[1973\] (the
corresponding provisions of subparagraphs (b) to (e)) is to be made as
of the filing or priority date of the patent application; evidence
arising after this date may be taken into account provided it is
directed to the position at that date. The Board held that when
assessing the factors in subparagraph (e), only three matters should be
considered: animal suffering, medical benefit and the necessary
correspondence between the two in terms of the animals in question. The
level of proof required is the same for both animal suffering and
substantial medical benefit, namely a likelihood. In this case, claims
covering transgenic rodents were refused as there was no evidence of
substantial medical benefit deriving from applying the claimed process
to all rodents. However, the Board allowed claims restricted to "mice",
considering that the likelihood of substantial medical benefit in
relation to advances in cancer research was relevant to all members of
the mouse family, and there was clear correspondence between the medical
benefit and the animal suffering.

### 76A.03 {#ref76A-03}

The wording of subparagraph (f) closely follows that of the repealed
s.1(3)(b) - see 1.46. Whether or not a (non-microbiological) process for
the production of animals or plants is to be considered as "essentially
biological" has to be judged on the basis of the essence of the
invention taking into account the totality of human intervention and its
impact on the result achieved. However, the necessity for human
intervention alone is not a sufficient criterion for an invention not
being "essentially biological". The EPO Enlarged Board of Appeal in two
related decisions (G 02/07 and G 01/08) held that claims to any non-
microbiological processes for the sexual crossing of the whole genomes
of plants are excluded as being "essentially biological". Furthermore,
claims to a breeding process do not escape the exclusion merely by the
addition of a further technical step which serves to enable or assist
the the steps of sexually crossing or subsequently selecting the
offspring. This is because such steps do not move the process beyond
what is considered to be "essentially biological" processes. Therefore,
in order to be patentable at least one additional technical step must be
performed within the steps of sexual crossing and selection, which "by
itself introduces a trait into the genome or modifies a trait in the
genome of the plant produced, so that the introduction or modification
of that trait is not the result of the mixing of the genes of the plants
chosen for sexual crossing".

### 76A.03.1 {#ref76A-03-1}

76A.03.1 In G2/12 and G 2/13, the Enlarged Board of Appeal found that
the exclusion of essentially biological processes for the production of
plants in Article 53(b) EPC does not have a negative effect on the
allowability of a product claim directed to plants or plant material
such as plant parts. Furthermore, a product-by-process claim directed to
plants or plant material (and not limited to a plant variety), wherein
the process features define an essentially biological process for the
production of plants, was not considered to be unpatentable under
Article 53(b) EPC. In these decisions, the Enlarged Board found that a
claim is also not excluded merely because of the fact that the only
method available at the filing date for generating the claimed subject
matter is an essentially biological process. However, in the later
decision G 3/19 the Enlarged Board reversed the earlier decisions made
in G2/12 and G 2/13 and gave its view that the exclusion of essentially
biological processes under Article 53(b) EPC does have a negative effect
on the allowability of product claims and product-by-process claims
directed to plants, plant material, or animals, if the claimed product
is obtained exclusively by an essentially biological process or if the
claimed process features in a product-by-process claim define an
essentially biological process.

### 76A.04 {#a04}

As subparagraph (f) makes clear, the exclusion does not apply to
micro-biological or other technical processes or the products thereof,
and patents may therefore be obtained not only for processes involving
micro-organisms, but also for micro-organisms themselves (as well as
inanimate products) when produced by a micro-biological process. "Micro-
biological or other technical processes" should be construed widely as
including selective culturing or cross-breeding of micro-organisms
including sub micro-organisms and not be restricted to essentially
chemical manufacturing processes in which micro-organisms are used. Thus
a claim to micro-organisms per se may be allowed when they have been
obtained by cross-selecting from known micro-organisms, by artificial
mutation or by micro- biological reproduction processes in which normal
conditions have been altered by human intervention.

### 76A.05 {#ref76A-05}

Sch. A2 para 4 is also relevant.

Inventions which concern plants or animals may be patentable if the
technical feasibility of the invention is not confined to a particular
plant or animal variety. The EPO Enlarged Board of Appeal in
Novartis/Transgenic plant G1/98 (\[2000\] EPOR 303; \[2000\] 3 OJEPO
111) has stated that a claim in which specific varieties are not
individually claimed is not excluded from patentability under a.53(b)
EPC (equivalent to Sch A2 para 3(f)) even if the claim embraces within
its scope plant varieties. (Certain plant varieties are capable of
protection in the UK under the Plant Varieties and Seeds Act 1964, which
is administered by the Plant Variety Rights Office, White House Lane,
Huntingdon Road, Cambridge CB3 OLF). The scope of the exclusion of
animal varieties was considered by the EPO in the Harvard "oncomouse"
case T 315/03 (\[2006\] 1 OJEPO 15, \[2005\] EPOR 31). Here it was held
that the principle enunciated in G1/98 concerning plant and plant
varieties should be followed in the case of animals: a patent should not
be granted for a single animal variety (or species or race, depending on
which language text of the EPC is used) but can be granted if varieties
may fall within the scope of its claims. The Board pointed out the
inconsistency in taxonomical rank between variety, species and race,
with the German term "Tierarten" (i.e. "animal species") having the
highest taxonomical order, but it concluded that mice constituted a
taxonomic classification broader than species, and therefore claims to
transgenic mice were not excluded from patentability under a.53(b) EPC.
([see also
1.43](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-43)).

### 76A.06 {#ref76A-06}

Sch. A2 paras 2, 5 Sch. A2 para 6 is also relevant.

Finding biological material, such as a micro-organism, occurring freely
in nature is a mere discovery and is therefore unpatentable as such
([see
1.27-1.28](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-27)).
However, biological material which is isolated from its natural
environment or produced by means of a technical process may be the
subject of an invention, even if the material previously occurred in
nature. More specifically, following the Relaxin case (Howard Florey
Institute of Experimental Physiology \[1995\] 6 OJEPO 388 (V 08/94), the
Schedule states that an element isolated from the human body or
otherwise produced by means of a technical process may be patentable
even if the structure of that element is identical to that of the
natural element. This includes the sequence or partial sequence of a
gene. However, where the invention in a patent application resides in a
whole or partial gene sequence, the industrial application of the
sequence must be disclosed in the application as filed -- [see
4.07.1-2](/guidance/manual-of-patent-practice-mopp/section-4-industrial-application/#ref4-07-1).

### Construction of claims relating to biotechnological inventions

### 76A.07 {#ref76A-07}

Sch. A2 para 7 Sch.A2 para 8 Sch. A2 para 9 Sch. A2 para 10 ss.60(5)
&(6A)-(6C) is also relevant.

Paragraphs 7 to 10 of Schedule A2 set out certain rules for the
construction of claims which relate to biological material or to
processes that enable biological material to be produced. In particular,
a claim to a biological material which has certain specific
characteristics as a result of the invention is construed as extending
to protect biological material possessing the same specific
characteristics which has been derived from the claimed material by
propagation or multiplication. Similarly, a claim to a process for
producing biological material which has certain specific characteristics
as a result of the invention is construed as extending to protect the
biological material itself and to biological material possessing the
same specific characteristics which has been derived from that material
by propagation or multiplication.

### 76A.08 {#a08}

Paragraph 9 of Schedule A2 (derived from Article 9 of Directive
98/44/EC) states that a claim to a product containing or consisting of
genetic information is construed as extending to all material (except
excluded material - [see 76A.02](#ref76A-02)) in which the product is
incorporated and in which the genetic information is contained and
performs its function. The interpretation of Article 9 of the Directive
was considered by the European Court of Justice in Monsanto Technology
LLC v Cefetra BV et al ECJ Case C-428/08. This was a reference from the
Dutch courts on a case concerning the import of soy meal derived from
genetically modified soybeans. The soy meal was found to contain a
modified gene; this modified gene, and plants and plant cells containing
it, were covered by a valid European patent held by Monsanto. The DNA in
the soy meal was clearly not "performing its function", as the soy meal
was dead, but it had performed its function (of protecting the plant
against a herbicide) in the living plant from which the soy meal was
derived, and could conceivably do so again if it was extracted and
transferred to a living plant or cell.

### 76A.08.1 {#ref76a-08-1}

The ECJ held that patent protection for genetic material does not extend
to circumstances where the genetic material has ceased to perform its
function, even if it did perform this function in the past and could
possibly do so in the future if isolated and transferred into a living
cell. Moreover, Article 9 of the Directive precludes national patent law
from offering absolute protection to the patented genetic material,
regardless of whether it performs its function in the material
containing it. This has implications for the interpretation of s.60 in
the case of biotechnological inventions. Section 60(1)(a) states that
where the invention is a product, it is an infringement to make, dispose
of, offer to dispose of, use or import the product or keep it whether
for disposal or otherwise ([see
60.06-16](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-06)).
However, if the product contains or consists of genetic material, this
is subject to the proviso that it performs its function in the material
containing it. In other words, s.60 must be read in the light of
paragraph 9 of Schedule A2 for inventions of this type, and it may be
inferred that the same applies for all of paragraphs 7-10 of Schedule
A2. In addition, the ECJ held that it was irrelevant whether the patent
had been filed or granted before adoption of the Directive, and that the
interpretation of Article 9 is not affected by the provisions of the
TRIPS Agreement.

### 76A.09 {#a09}

Where biological material which is subject to patent protection is put
on the market by the proprietor (or with their consent), and where
propagation or multiplication of further biological material necessarily
results, the protection conferred by the patent does not extend to this
further biological material, provided that this further material is not
then used for further propagation or multiplication. (Under certain
limited circumstances, however, it is possible for farmers to use this
further material for further propagation or multiplication without
infringing - [see
60.26-27](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-26)).

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 76A(2)**
  Nothing in this section or Schedule A2 is to be read as affecting the application of any provision in relation to any other kind of patent or application for a patent.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 76A.10 {#a10}

Section 76A(2) makes clear that the provisions of this section and of
Schedule A2 cannot be construed as having any effect on a patent or
application which does not concern a biotechnological invention. The
Patents Regulations 2000 added to s.130(1) a definition of what
constitutes a 'biotechnological invention' - [see
130.04.1](/guidance/manual-of-patent-practice-mopp/section-130-interpretation/#ref130-04-1).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
