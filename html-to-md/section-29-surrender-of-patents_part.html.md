::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 29: Surrender of patents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (29.01 - 29.07) last updated: April 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 29.01 {#ref29-01}

s.77(1) is also relevant.

This section provides for the surrender of patents, including European
patents (UK), and for opposition by a third party to an offer to
surrender. Procedures are prescribed by r.42 and Part 7 -- Proceedings
before the Comptroller.

  --------------------------------------------------------------------------------------------------------------
   
  **Section 29(1)**
  The proprietor of a patent may at any time by notice given to the comptroller offer to surrender his patent.
  --------------------------------------------------------------------------------------------------------------

### 29.02 {#ref29-02}

r42, r.75 is also relevant.

Notice of an offer by a proprietor to surrender their patent should be
given in writing. The offer must then be advertised by the comptroller
in the Journal.

### 29.03 {#ref29-03}

The offer to surrender should be accompanied by either (a) a declaration
that no infringement or revocation action relating to the patent is
pending before the court; or (b) if such action is pending, full
particulars of the action in writing. Where a revocation action is
pending before the court, the comptroller may stay any consideration of
the offer to surrender and order the proprietor to inform the court that
an offer to surrender has been made (Dyson Ltd's Patent \[2003\] RPC 24
and \[2003\] RPC 48). [Genentech Inc's patent (BL
O/360/14)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/360/14){rel="external"}
related to an offer to the comptroller to surrender a patent, where a
revocation action was pending before the court. The patentee had already
informed the claimant in the revocation action, as well as the court
itself, of the offer to surrender. In addition, the court had issued a
judgment stating the patentee had "applied to the UK IPO and surrendered
the ... patent. Therefore there is no patent and no reason to continue
the \[revocation\] action in relation to that patent". It was therefore
clear that all parties had proceeded on the basis that the patent had
been surrendered. In addition, no one opposed the offer to surrender.
The hearing officer therefore determined that the offer to surrender
could be accepted prior to the resolution of the revocation proceedings.
More generally, the hearing officer found that if it is clear that the
court, the claimant and patent holder in a revocation action are all
aware that an offer of surrender has been made to the comptroller, and
have proceeded on the basis that it has taken effect, then that offer to
surrender should be accepted without the need to wait for the resolution
of the revocation proceedings (subject to advertisement of the offer,
and subject to any opposition being satisfactorily dealt with). For
procedure where a revocation action is pending before the comptroller
when the offer to surrender is made, [see
72.36-72.39](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-36).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 29(2)**
  A person may give notice to the comptroller of his opposition to the surrender of a patent under this section, and if he does so the comptroller shall notify the proprietor of the patent and determine the question.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 29.04 {#section}

r.76(3), r.108(1) is also relevant.

Any person who wishes to oppose the surrender must do so on Form 15
within four weeks from the date of the advertisement. This period may
not be extended. There is no need for an opponent to have a locus
standi.

### 29.05 {#section-1}

PR part 7 is also relevant.

The Form should be accompanied by a copy and a statement of grounds (in
duplicate) This starts proceedings before the comptroller, the procedure
for which is discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).

### 29.06 {#ref29-06}

If proceedings to revoke the patent are pending before the comptroller
the opposition to surrender will normally be stayed pending the outcome
of the revocation action ([see
72.36-72.39](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-36)).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 29(3)**
  If the comptroller is satisfied that the patent may properly be surrendered, he may accept the offer and, as from the date when notice of his acceptance is published in the journal, the patent shall cease to have effect, but no action for infringement shall lie in respect of any act done before that date and no right to compensation shall accrue for any use of the patented invention before that date for the services of the Crown.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 29.07 {#section-2}

s.73(4) is also relevant.

If the comptroller accepts the offer to surrender this takes effect ex
nunc. Consequently although no action can be taken in respect of an
infringement which has taken place before the date on which the
acceptance is advertised in the Journal, any royalties or other monies
already paid to the proprietor cannot be recovered. Moreover since a
prima facie valid patent will have been in existence for a period of
time, however short, up to the surrender, an offer to surrender will not
automatically terminate revocation proceedings ([see
72.36](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-36)).
In addition, surrender of a European patent (UK) will only obviate
revocation of a national patent with which it is in conflict if the
offer to surrender is made before the date on which the notice of grant
of the national patent appears in the Journal; [see
73.11-12](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-11-12).

\[ If no revocation proceedings are pending and the offer to surrender
is not opposed, an acceptance of the offer to surrender should be
prepared by Restoration and Post Grant Section (RAPS) and sent for
signature to the Group Head of the group to which the subject-matter of
the patent belongs. If they decide that the patent may properly be
surrendered, then after signing the acceptance they should return the
case to RAPS for the acceptance of the offer to be advertised. \]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
