::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 90: Orders in Council as to convention countries {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (90.01 - 90.04) last updated: April 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 90(1)**
  His Majesty may with a view to the fulfilment of a treaty or international convention, arrangement or engagement, by Order in Council declare that any country specified in the Order is a convention country for the purposes of section 5 above.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 90(2)**
  His Majesty may by Order in Council direct that any of the Channel Islands, any colony shall be taken to be a convention country for those purposes.
  ------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 90(3)**
  For the purposes of subsection (1) above every colony, protectorate, and territory subject to the authority or under the suzerainty of another country, and every territory administered by another country under the trusteeship system of the United Nations shall be taken to be a country in the case of which a declaration may be made under that subsection.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 90.01

Any country may be declared a convention country by Order in Council,
with the effect that priority may be claimed from an application made in
or for that country ([see
5.30](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-30)).
The making of a new Order is advertised in the Journal.

### 90.02 {#ref90-02}

The following is a list of the countries which have been declared to be
a convention country under the Patents (Convention Countries) Order 2007
(SI 2007 No. 276), which revoked the Patents (Convention Countries)
Order 2006, and the Patents (Convention Countries) (Amendment) Order
2009 (SI 2009 No. 2746).

  ----------------- ----------------- ----------------- -----------------
                                                         

  Albania           Bahamas           Bosnia and        Cape Verde
                                      Herzegovina       

  Algeria           Bahrain           Botswana          Central African
                                                        Republic

  Andorra           Bangladesh        Brazil            Chad

  Angola            Barbados          Brunei Darussalam Chile

  Antigua\          Belarus           Bulgaria          China
  and Barbuda                                           

  Argentina         Belgium           Burkina Faso      Colombia

  Armenia           Belize            Burundi           Comoros

  Australia         Benin             Cambodia          Congo

  Austria           Bhutan            Cameroon          Congo

  Azerbaijan        Bolivia           Canada            Congo, Democratic
                                                        Republic of the

  Costa Rica        Guinea            Lebanon           Netherlands

  Côte d'Ivoire     Guinea - Bissau   Lesotho           Netherlands
                                                        Antilles\
                                                        and Aruba

  Croatia           Guyana            Liberia           New Zealand,
                                                        including the
                                                        Cook Islands,\
                                                        Niue and\
                                                        Tokelau

  Cuba              Haiti             Libyan Arab\      Nicaragua
                                      Jamahiriya        

  Cyprus            Holy See          Liechtenstein     Niger

  Czech\            Honduras          Lithuania         Nigeria
  Republic                                              

  Denmark           Hong Kong         Luxembourg        Norway

  Djibouti          Hungary           Macao             Oman

  Dominica          Iceland           Macedonia,\       Pakistan
                                      Former Yugoslav\  
                                      Republic of       

  Dominican\        India             Madagascar        Papua New Guinea
  Republic                                              

  Ecuador           Indonesia         Malawi            Paraguay

  Egypt             Iran, Islamic     Malaysia          Peru
                    Republic of                         

  El Salvador       Iraq              Maldives          Philippines

  Equatorial Guinea Ireland           Mali              Poland

  Estonia           Israel            Malta             Portugal

  Faeroe Islands    Italy             Mauritania        Romania

  Fiji              Jamaica           Mauritius         Russian
                                                        Federation

  Finland           Japan             Mexico            Rwanda

  France,\          Jordan            Moldova, Republic Qatar
  including\                          of                
  all Overseas                                          
  Departments\                                          
  and Territories                                       

  Gabon             Kazakhstan        Monaco            Saint Kitts and
                                                        Nevis

  Gambia            Kenya             Mongolia          Saint Lucia

  Georgia           Korea,\           Montenegro        Saint Vincent\
                    Democratic\                         and the
                    People's                            Grenadines
                    Republic\                           
                    of                                  

  Germany           Korea, Republic   Morocco           Samoa
                    of                                  

  Ghana             Kuwait            Mozambique        San Marino

  Greece            Kyrgyzstan        Myanmar           Sao Tome and
                                                        Principe

  Grenada           Lao People's\     Namibia           Spain
                    Democratic\                         
                    Republic                            

  Guatemala         Latvia            Nepal             Sri Lanka

  Saudi Arabia      Sudan             Tonga             Uruguay

  Senegal           Suriname          Togo              Uzbekistan

  Serbia            Swaziland         Trinidad and      Venezuela
                                      Tobago            

  Seychelles        Sweden            Tunisia           Vanuatu

  Sierra Leone      Switerland        Turkey            Vietnam

  Singapore         Syrian Arab\      Turkmenistan      Yemen
                    Republic                            

  Slovakia          Taiwan            Uganda            Zambia

  Slovenia          Tajikistan        Ukraine           Zimbabwe

  Solomon Islands   Tanzania,\        United Arab        
                    United\           Emirates          
                    Republic of                         

  South Africa      Thailand          United States of   
                                      America\          
                                      (including Puerto 
                                      Rico\             
                                      and all           
                                      territories and   
                                      possessions)      
  ----------------- ----------------- ----------------- -----------------

### 90.03 {#section-1}

The United Kingdom is not a convention country within the meaning of the
Patents Act.

### 90.04 {#section-2}

In s.90(2), "any colony" was originally followed by the words "or any
British protectorate or protected state" which have been deleted by the
Statute Law (Repeals) Act 1986 (c.12).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
