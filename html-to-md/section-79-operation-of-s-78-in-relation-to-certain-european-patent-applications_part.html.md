::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 79: Operation of s.78 in relation to certain European patent applications {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (79.01 - 79.04) last updated April 2008.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 79.01

a.150 EPC is also relevant.

An international application made under the PCT in which the EPO acts as
designated or elected Office (and will therefore conduct the substantive
examination) is deemed to be a European patent application. This section
provides for the operation of s.78 in relation to such an application
which designates the UK as a country in which protection is sought.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 79(1)**
  Subject to the following provisions of this section, section 78 above, in its operation in relation to an international application for a patent (UK) which is treated by virtue of the European Patent Convention as an application for a European patent (UK), shall have effect as if any reference in that section to anything done in relation to the application under the European Patent Convention included a reference to the corresponding thing done under the Patent Co-operation Treaty.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 79.02

s.79(1)-(3) s.78(7) are also relevant.

An application for a European patent (UK) initiated by an international
application is treated as an application under the 1977 Act to the
extent specified in s.78, in the same way as any other application for a
European patent (UK) except for requirements with regard to language and
filing at the EPO ([see 79.03-04](#ref79-03)) and the fact that acts
done under the PCT are treated as if they were the corresponding acts
done under the EPC. Publication by the International Bureau under PCT
a.21 in French or German can be taken to be publication by the EPO for
the purposes of s.78(7), [see
78.11](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-11).

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 79(2)**
  Any such international application which is published under that treaty shall be treated for the purposes of section 2(3) above as published only when a copy of the application has been supplied to the European Patent Office in English, French or German and the relevant fee has been paid under that convention.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 79.03 {#ref79-03}

The matter in such an international application published under the PCT
becomes part of the state of the art under s.2(3) only if a copy of the
application in English, French or German has been filed at the EPO.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 79(3)**
  Any such international application which is published under that treaty in a language other than English, French or German shall, subject to section 78(7) above, be treated for the purposes of sections 55 and 69 above as published only when it is re-published in English, French or German by the European Patent Office under that convention.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 79.04 {#ref79-04}

If the international application is published under the PCT in a
language other than English, French or German, it is not treated as
published for Crown use or infringement purposes under the Act until it
is re-published in English, French or German by the EPO. However, under
s.78(7), those re-published in French or German are not so treated until
an English translation of the claims has been filed at and published by
the UK Office, [see
78.11](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-11).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
