::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 10: Handling of application by joint applicants {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (10.01 - 10.10) last updated: January 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 10.01

Under this section, certain disputes between joint applicants for a
patent may be resolved by the comptroller in response to a request in
accordance with part 7 of the Patents Rules 2007 ([see 123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05)).

### 10.02

In addition, a joint applicant may under s.8(1)(b) refer to the
comptroller the question whether any right in or under the application
should be transferred or granted to any other person, [see 8.05 to
8.12](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-05)
and
[8.20](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-20).
The rights of and relating to joint applicants are laid down by s.36.

### 10.03 {#ref10-03}

Section 10 is applicable in relation to European and foreign
applications to the extent laid down by s.12(4), [see 10.05](#ref10-05).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 10**
  If any dispute arises between joint applicants for a patent whether or in what manner the application should be proceeded with, the comptroller may, on a request made by any of the parties, give such directions as he thinks fit for enabling the application to proceed in the name of one or more of the parties alone or for regulating the manner in which it shall be proceeded with, or for both those purposes, according as the case may require.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 10.04 {#ref10-04}

The disputes to which s.10 relates are those between joint applicants
whether or in what manner the application should be proceeded with. Any
of the parties may make a request to the comptroller for directions
under this section.

### 10.05 {#ref10-05}

S.12(4) is also relevant

In the case of an application under the Act, the comptroller may give
such directions as they think fit for enabling the application to
proceed in the name of one or more of the parties alone and/or for
regulating the manner in which it should be proceeded with. However, in
the case of an application under foreign or international law (including
the EPC) such directions enabling the comptroller to regulate the manner
in which an application is to proceed cannot be given. In all other
respects, s.10 applies to disputes between joint applicants under
foreign or international law as it applies to joint applicants under the
Act.

### 10.06 {#section-2}

s.12(5) is also relevant

Any directions given under s.10, including those given under s.10 by
virtue of s.12(4), are subject to s.11. Section 11 concerns the effects
of such directions in relation to any licences or other rights in or
under the application.

### 10.07 {#ref10-07}

In N J M Pelling and R J Campbell's Application ([BL
O/134/87](https://www.gov.uk/government/publications/patent-decision-o13487))
the first of the joint applicants had paid all the costs of making
patent applications in the UK and elsewhere; the second applicant
refused to contribute except from any proceeds of future exploitation of
the invention. The hearing officer declined to make any direction under
s.8 or s.12 but, under s.10, directed the second applicant to assign his
interest in the UK patent application to the first, in return for a free
non-assignable, non-revocable licence excluding any right to
sub-licence. With regard to the object to be achieved in such
proceedings, he observed: "the correct approach is for the comptroller
to seek to implement the overall purpose of the Act in the most
equitable manner that he can".

### 10.07.1 {#ref10-07-1}

At a preliminary hearing the hearing officer in Brooks and Cope's
Application ([BL
O/71/93](https://www.gov.uk/government/publications/patent-decision-o07193))
made a direction under s.10 that substantive examination of a UK
application should proceed and that the report under s.18 should be sent
to both parties.

### Procedure

### 10.08 {#ref10-08}

PR part 7 is also relevant

A request under s.10 (or s.12(4)) by a joint applicant should be made on
Patents Form 2 accompanied by a copy thereof and a statement of grounds
in duplicate. This starts proceedings before the comptroller, the
procedure for which is discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).

### 10.9 {#section-3}

\[deleted\]

### 10.10 {#section-4}

\[deleted\]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
