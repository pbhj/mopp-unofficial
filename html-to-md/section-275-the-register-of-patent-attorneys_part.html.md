::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 275: The register of patent attorneys {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (275.01 - 275.05) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 275.01

This provides for the continued existence of a register of patent
attorneys (other sections of Part V of the CDP Act and the Legal
Services Act 2007 provide that those on the register are entitled to
certain benefits not available to the lay person). Provision is also
made for the regulation of patent attorneys under section 275A
introduced through section 185(3) of the Legal Services Act 2007.

### 275.02 {#ref275-02}

In addition to the rights of registered patent attorneys provided by
statute, it has long been the accepted practice that in patent
proceedings before the comptroller, and on appeals from any decision of
theirs in those proceedings, counsel may be instructed by registered
patent attorneys without the intermediary of a solicitor (confirmed by
the Patents Court in Reiss Engineering Co Ltd v G J Harris \[1987\] RPC
171).

### 275.03 {#section-1}

This section was amended by the Legal Services Act, section 185(3) on 1
January 2010.

  ------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 275(1)**
  There is to continue to be a register of persons who act as agent for others for the purpose of applying for or obtaining patents.
  ------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------
   
  **Section 275(2)**
  In this Part a registered patent attorney means an individual whose name is entered on the register kept under this section.
  ------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------
   
  **Section 275(3)**
  The register is to be kept by the Chartered Institute of Patent Attorneys.
  ----------------------------------------------------------------------------

### 275.04 {#ref275-04}

The CDP Act previously empowered the Secretary of State to make rules
requiring the keeping of a register of patent attorneys (the Register of
Patent Agent Rules 1990 (SI 1990/1457) which required the Chartered
Institute of Patent Attorneys (CIPA) to keep the register and appoint
the registrar). Following amendment of section 275 by section 185(3) of
the Legal Services Act 2007 this power has been replaced by the
requirement for a register of patent attorneys to be kept by CIPA.

  -------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 275(4)**
  The Secretary of State may, by order, amend subsection (3) so as to require the register to be kept by the person specified in the order.
  -------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------
   
  **Section 275(5)**
  Before making an order under subsection (4), the Secretary of State must consult the Legal Services Board.
  ------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------
   
  **Section 275(6)**
  An order under this section must be made by statutory instrument.
  -------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 275(7)**
  An order under this section may not be made unless a draft of it has been laid before, and approved by a resolution of, each House of Parliament.
  ---------------------------------------------------------------------------------------------------------------------------------------------------

### 275.05 {#section-2}

Under subsections (4) to (7), the Secretary of State is empowered to
amend by order subsection (3) to require someone other than CIPA to be
the keeper of the register. The Secretary of State must consult the
Legal Services Board and enact a statutory instrument which has been
laid and approved by each House of Parliament.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
