::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 15: Date of filing application {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (15.01 - 15.58) last updated: April 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 15.01

This section states the conditions which are necessary and sufficient
for an application to be accorded a filing date, sets out the
circumstances in which an application may be re-dated to a later date,
makes provision for divisional applications and specifies further
conditions which must be fulfilled before an application can proceed.
Time limits and other provisions relating to these matters are
prescribed in rr.18-22.

### 15.01.1

Section 15 was amended by the Regulatory Reform (Patents) Order 2004
(S.I. 2004 No. 3204) to incorporate the principles of Articles 5 and 6
of the Patent Law Treaty (PLT). The amended section applies to
applications initiated on or after 1 January 2005 by the filing of
documents which comply with the requirements of s.15(1). The relevant
rules were amended with effect from this date by the Patents (Amendment)
Rules 2004 (S.I. 2004 No. 3205). For applications initiated by documents
that met the relevant requirements of the former section 15(1) on or
before 31 December 2004, unamended sections 14(1) 15, 17 and 18 of the
Act and the Rules continue to apply. The Patents Rules 2007 (SI 2007 No.
3291) have replaced the Patents Rules 1995 (as amended) with effect from
17 December 2007.

  -----------------------------------------------------------------------
   

  **Section 15(1)**

  Subject to the following provisions of this Act, the date of filing an
  application for a patent shall be taken to be the earliest date on
  which documents filed at the Patent Office to initiate the application
  satisfy the following conditions-\
  (a) the documents indicate that a patent is sought;\
  (b) the documents identify the person applying for a patent or contain
  information sufficient to enable that person to be contacted by the
  Patent Office; and\
  (c) the documents contain either (i) something which is or appears to
  be a description of the invention for which a patent is sought; or (ii)
  a reference, complying with the relevant requirements of rules, to an
  earlier relevant application made by the applicant or a predecessor in
  title of his.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 15(2)**

  It is immaterial for the purposes of subsection (1)(c)(i) above-\
  (a) whether the thing is in, or is accompanied by a translation into, a
  language accepted by the Patent Office in accordance with rules;\
  (b) whether the thing otherwise complies with the other provisions of
  this Act and with any relevant rules.
  -----------------------------------------------------------------------

### 15.02 {#ref15-02-15-06}

r.12(1), r.25(1)(a) is also relevant.

Normally the indication that a patent is sought will be given by the
completion of Patents Form 1. This is designated a formal requirement,
but it is not necessary that it be explicit in order for a date of
filing to be accorded. If the documents filed contain an implicit
indication, however informal, which makes it reasonably clear that an
attempt is being made to file an application for a patent then
s.15(1)(a) can be regarded as having been complied with. The indication
must, however, be in English or Welsh (at least to the extent that it is
apparent to a literate person acquainted only with the English and/or
Welsh language that a patent is sought).

### 15.03 {#ref15-03}

The applicant is sufficiently identified if the name and address given
are adequate to meet the customary requirements of postal delivery. The
address may be anywhere in the world. Other information (such as a phone
number) instead of an address may be sufficient identification to grant
a filing date [see 15.03.1](#ref15-03-01). Where there is more than one
applicant each must be adequately identified.

### 15.03.1 {#ref15-03-01}

r.12(2), r.12(3) is also relevant.

The applicant's name and address should normally be given at the time of
filing,but failing that a date of filing can be accorded when the
application contains sufficient information to enable the applicant to
be contacted. Where an application which does not include both the
applicant's name and address is accorded a date of filing, the applicant
must be notified of the failure, and the comptroller may refuse the
application if the applicant does not file the missing information
within two months of issue of the notification. The name and address (or
other means of contacting the applicant) must be in English or Welsh to
the extent that it is useable by a literate person acquainted only with
the English and/or Welsh language.

\[ Where Parts 2 and 4 of Form 1 do not, between them contain enough
information to enable the applicant to be contacted, contact information
in Part 12 may be good enough, but where there is doubt the advice of
Legal Section should be sought.\]

### 15.04 {#section-2}

s.7(1) is also relevant.

If the application is made in the name of a firm or other organisation
which is not a corporate body this will not prevent it being accorded a
filing date, provided the organisation is adequately identified. The
formalities examiner should however require that one or more persons
(that is, individuals or corporate bodies) be named at part 2 of Form 1.

### 15.05 {#section-3}

Provided a document contains at least a small amount of technical
information it will be regarded as complying with s.15(1)(c), and
failure to comply with, for example, s.14(3) or r.14 will not prevent a
filing date being accorded. If the description fails to comply with any
formal or substantive requirement of the Act or Rules, objection will be
raised sooner or later and the applicant given an opportunity to rectify
the matter. It is therefore in the interest of the applicant to ensure
that, at the time of filing, the description is not flawed in such a way
that rectification would not be possible. For example, no amendment is
allowed which would result in the description disclosing matter not
either present at or referred to on the filing date.

\[ In the rare event that although a filing date has been accorded, it
transpires that what has been filed cannot conceivably be called a
description, the filing date should be cancelled. \]

### 15.06 {#ref15-06}

There is no requirement to pay a filing fee as a prerequisite to an
application being accorded a date of filing. For details of payment of
the application fee required for preliminary examination, see
[14.02](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-02)
and
[14.04.17](/guidance/manual-of-patent-practice-mopp/section-14-the-application#ref14-04-17).

### Disclosure

### 1. Description, Language of Description

### 15.06.1 {#ref15-06-01}

In order to qualify for a date of filing something that is or appears to
be a description will normally be required. Providing that the remainder
of the application is in English or Welsh (i.e. with names and addresses
etc. intelligible to and useable by a literate person acquainted only
with English and/or Welsh) an application can qualify for a date of
filing if the description (or what appears to be the description) is
neither in English or Welsh nor compliant with rules such as r.14. The
wording "something that appears to be a description" in s 15(1)(c)(i)
reflects the wording "a part which, on the face of it, appears to be a
description" of the PLT, and caters for possibility of that part of the
application being in non-Roman notation.

### 15.06.2 {#ref15-06-02}

r.12(8), r.12(9) is also relevant.

Where a date of filing is accorded to an application comprising a
foreign-language description, the applicant must be notified of the
failure to comply with r.14(1). The comptroller may refuse the
application if the applicant fails to file a translation into English or
Welsh within two months of such a notification.

### 2. Reference to an earlier application

### 15.06.3 {#ref15-06-03}

r.17(1) is also relevant.

Article 5(7) of the PLT requires Contracting Parties to accept, for the
purposes of according a date of filing only, a reference to an earlier
application as a replacement for the description, and this is reflected
in s.15(1)(c)(ii). Where such a reference is made the application
number, date of filing and country of filing must be stated in part 9 of
Form 1.

### 15.06.4 {#section-4}

s.15(10)(b), r.17(2), r.22(3), r.108(1), r.108(5)(7), r.17(3), r.112(1),
r.112(2) is also relevant.

The application will be treated as withdrawn if the disclosure contained
in the reference fails to be substantiated by the filing of a certified
copy of the earlier application (and, if that copy is not in English or
Welsh, by an accompanying translation) within the four months
(extendable at the discretion of the comptroller, [see 123.34 to
123.41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).
Applicants must bear in mind any difficulties which may be inherent in
obtaining the necessary certified copies and translations within the
time allowed. Where, however, a copy of the earlier application in
question is available to the comptroller, the comptroller will make and
certify the necessary copy and place it on the file without the
applicant needing to request or pay for it.

### 15.06.5 {#section-5}

r.22(1), r.108(2)-(7) is also relevant.

The application will also be treated as withdrawn if a description (in
English or Welsh) is not filed (as required by s.15(10)(b)(i)) before
the expiry of the period prescribed in r.22(1). This period is
extendable once by two months as of right under r.108(2) and further
extendable in tranches of two months at the discretion of the
comptroller under r.108(3)-(7) [see 123.34 to
123.42](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).
Rule 22 is listed in Part 3 of Schedule 4, so if a request for extension
(or further extension) is not received before expiry of the requested
extension, the application must be treated as having been withdrawn [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).
A description must be filed in addition to the certified copy of the
earlier application, and must be filed even where the earlier
application is available to the comptroller. Where the description
required by s.15(10)(b)(i) contains matter additional to that contained
in the earlier application to which reference was made, s.76(1A) will
not allow the application to proceed unless or until that matter has
been removed from the description. Whether or not there is any added
matter is a matter of fact which will have to be determined comparing
the description against the certified copy (or, where relevant, the
translation of the certified copy). Except in the clearest of cases
(e.g. when the description is manifestly a complete photocopy of part or
all of the certified copy) the document should be referred to an
examiner to make the comparison.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 15(3)**
  Where documents filed at the Patent Office to initiate an application for a patent satisfy one or more of the conditions specified in subsection (1) above, but do not satisfy all those conditions, the comptroller shall as soon as practicable after the filing of those documents notify the applicant of what else must be filed in order for the application to have a date of filing.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 15.06.6 {#section-6}

Where an application fails to meet the requirements for being given a
date of filing, the comptroller must notify them of that failure and
state the reason.

  -----------------------------------------------------------------------
   

  **Section 15(4)**

  Where documents filed at the Patent Office to initiate an application
  for a patent satisfy all the conditions specified in subsection (1)
  above, the comptroller shall as soon as practicable after the filing of
  the last of those documents notify the applicant of-\
  (a) the date of filing the application, and\
  (b) the requirements that must be complied with, and the periods within
  which they are required by this Act or rules to be complied with, if
  the application is not to be treated as having been withdrawn.
  -----------------------------------------------------------------------

### 15.06.7 {#section-7}

Where an application meets the requirements for being given a date of
filing, the comptroller must notify the applicant of that date, and must
also notify the applicant of anything that still requires to be filed
and for which the sanction for failing to file it within the time
prescribed is that the application will be treated as withdrawn. Where
applications (including divisional applications) are initiated by
documents complying with section 15(1), these notifications will be
embodied in the receipts issued by Document Reception.

### 15.06.8 {#section-8}

s.89A(3) is also relevant.

For international applications, the application fee is set at nil, but
there is instead a national phase entry fee. As soon as the requirements
of s.89A for national phase entry are met, preliminary examination and
search can be carried out. In this case the notifications required by
s.15(4) will be included in the formalities report rather than in the
filing receipt.

  -----------------------------------------------------------------------
   

  **Section 15(5)**

  Subsection (6) below applies where-\
  (a) an application has a date of filing by virtue of subsection (1)
  above;\
  (b) within the prescribed period the applicant files at the Patent
  Office-\
  (i) a drawing, or\
  (ii) part of the description of the invention for which a patent is
  sought, and\
  (c) that drawing or that part of the description was missing from the
  application at the date of filing.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 15(6)**

  Unless the applicant withdraws the drawing or the part of the
  description filed under subsection (5)(b) above ("the missing part")
  before the end of the prescribed period-\
  (a) the missing part shall be treated as included in the application;
  and\
  (b) the date of filing the application shall be the date on which the
  missing part is filed at the Patent Office.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 15(7)**

  Subsection (6)(b) above does not apply if-\
  (a) on or before the date which is the date of filing the application
  by virtue of subsection (1) above a declaration is made under section
  5(2) above in or in connection with the application;\
  (b) the applicant makes a request for subsection (6)(b) above not to
  apply; and\
  (c) the request complies with the relevant requirements of rules and is
  made within the prescribed period.
  -----------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 15(8)**
  Subsections (6) and (7) above do not affect the power of the comptroller under section 117(1) below to correct an error or mistake.
  -------------------------------------------------------------------------------------------------------------------------------------

### General Considerations: Missing Drawings and Missing Parts of the Description

### 15.07 {#ref15-07}

Subsections (5) to (8) provide for missing drawings and parts of the
description to be filed after the date of filing, and are intended to
reflect the provisions of a.5(6) of the PLT. Unless or until the
terminology is further clarified by case law, it should be considered
that a part is missing only when it is evident from inspection of the
documents on file that the description or drawings are manifestly
incomplete. The page numbering may be discontinuous, or there may be a
mismatch between the number of pages of text alleged to have been filed
and the number actually present. In the case of drawings there may
either be a discontinuity in sheet/Figure numbering or the absence of a
drawing listed in the description. In BL O/769/18, the hearing officer
did not allow an auxiliary request from the applicant to replace the
text of the specification as filed with the text of the priority
application because the description in this case was not manifestly
incomplete. It was a complete description, just the incorrect one.

### 15.07.1 {#ref15-07-1}

Where, upon performing the preliminary examination, the formalities
examiner finds that a drawing referred to in the description, or a part
of the description, appears to be missing this must be formally notified
to the applicant in the preliminary examination report as required by
s.15A(4) and (9) [see
15A.23](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-23).
The examination should be no more rigorous than is necessary to check
for missing pages, missing sheets of drawings, or the absence of
individual Figures listed in the description. There is no requirement to
read and construe the text of the description (other than to identify
the drawings that are listed).

### 15.07.2 {#section-9}

Preliminary examination is not always performed soon after the date of
filing, and although there is no statutory requirement for document
reception to notify the applicant of any parts missing from an
application at the time it qualifies for a date of filing, it is
nevertheless in the applicant's interest that they should be informed as
soon as possible of any such defects. For this reason document reception
should, as a matter of best practice and good customer relations,
continue to report to the applicant any missing matter that is noticed
when document reception procedures are being carried out.

### Time Limits

### 15.07.3 {#section-10}

s.15(6)(a) is also relevant.

Where a drawing or a part of the description is missing from an
application on the date of filing, the missing material can be filed at
any time before expiry of the period prescribed in r.18 (which is either
at any time up to the preliminary examination per r.18(1) or, if the
absence of the missing matter is notified with the preliminary
examination report under s.15A(9), within two months of the date of
issue of that notification per r.18(2)). When missing matter is filed
within the allowed period, then the matter shall be treated as included
in the application, unless the applicant requests in writing that the
missing material is withdrawn before the remainder of that period has
expired.

### 15.07.4 {#section-11}

The period prescribed by r.18(1) can be extended in accordance with
r.108(2) or (3) [see
123.36.5-123.36.8](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36-5).
The period prescribed by r.18(2) can be extended at the comptroller's
discretion in accordance with r.108(1) [see
123.36.9](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36-9).
The effect of these time periods and extensions is that missing material
may be filed at any time up to the issue of the preliminary examination
report. Once the preliminary examination report has been issued, then if
the absence of the missing matter is notified in the report, the
applicant has 2 months to file the missing material, and this period may
be extended at the Comptroller's discretion. If the preliminary
examination report has been issued, but the report does not refer to the
missing matter, the applicant may file the missing matter within two
months of the report on filing Form 52 together with the appropriate fee
(if any). At the Comptroller's discretion, this period may be further
extended on filing a further Form 52 and fee (if any).

### Re-dating

### 15.08 {#ref15-08}

Unless s.15(7) applies the date of filing the application must be
amended to be the date on which the missing part was filed. Re-dating as
a consequence of including missing matter is the only circumstance in
which an application may be re-dated to a date later than the originally
accorded filing date.

### 15.08.1 {#ref15-08-1}

With respect to drawings, s.15(5) and (6) can only be applied where a
drawing referred to in an application was not present in any form in the
application at the filing date of the application; an application cannot
be re-dated when formal drawings are filed to replace informal drawings
which had been filed on the filing date. (If a late filed formal drawing
differs materially from an originally-filed informal drawing the only
remedy lies in replacing the formal drawing with one which is in
agreement with the drawing originally filed). The expression "any
drawing" has been held to mean "any figure" (Alps Electric Co's
Application (BL O/12/90)). In the case of a divisional application filed
without a particular drawing, the date on which the application was
initiated may be re-dated under s.15(6) to the date on which the missing
drawing is filed. This does not affect the date which the application
may be treated as having as its date of filing under s.15(9), allowance
of which is determined in the normal way after the question of re-dating
has been settled. (However, the change in the date on which the
application was initiated may result in failure to comply with the
period specified in r.19, but an extension of the period may be allowed
in two month tranches, if considered appropriate, by exercise of the
comptroller's discretion under r.108(1) and (5)-(7)).

### 15.09 {#section-12}

Section 15(8) states that s.15(6) and (7) do not affect the
comptroller's discretion under s.117(1) to correct an error or mistake.
Thus, if all of the requirements of s.117 and r.105 have been met [see
117.03-09](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-03),
it is possible for the comptroller to permit a drawing which was omitted
by mistake from the documents originally filed with an application to be
filed later without loss of the original filing date. Requests to this
effect should be considered on their merits, see 117.10.

### 15.10 {#section-13}

r.18(2) is also relevant.

If on preliminary examination ([see 15.07.1](#ref15-07-1)) it is found
that a part of the description or a drawing referred to in the
application has not been filed at all, a letter should be issued
specifying a period of two months for filing the missing material, the
application then being re-dated to the date on which the missing
material is filed. However, sight should not be lost of the possibility
that, rather than re-date, the applicant might prefer to use the
defective application to provide priority for a subsequent application,
which includes the missing part(s), so preserving the date for the
disclosure present in the original, defective application (subject of
course to the provisions of s.5).

\[ If a part of a description or drawing has been filed late, LFB should
be used, if it has not been filed at all, LFA should be used. After the
formalities examiner has issued the appropriate letter and on completion
of preliminary examination they should refer the case to the search
examiner. The formalities examiner should create a minute to draw the
search examiner's attention to the invitation to redate. If the
re-dating will or might invalidate a declaration of priority this should
also be drawn to the search examiner's attention. It is for the search
examiner to decide whether to carry out the search immediately or
whether it would be more sensible to wait until the applicant has had
the opportunity to respond to the invitation to re-date. For example,
there may be no point in deferring the search if the applicant is
unlikely to re-date, perhaps because the missing part is unimportant and
re-dating would result in loss of priority. On the other hand, it may
well be sensible to wait if there is a fair risk that the application
will be withdrawn and used to provide priority for a new one. If the
search examiner decides to defer the search, the COPS status should be
set to 2 - "Out for ABS" and an appropriate diary entry should be
entered in the PD electronic diary. This entry should state the
examining group after the application number in the format GB0000000.0
-- EX00. A diary action should also be added to the PDAX dossier. \]

### 15.11 {#section-14}

While a formal report under s.15(5) or (6) will normally only issue
after preliminary examination, if the formalities examiner becomes aware
before the filing of this form that a drawing is missing an informal
notice should be issued informing the applicant of the options available
from these subsections.

\[This will normally only occur with private applicant cases. LFAA
should be used.\]

### 15.12 {#section-15}

If, after re-dating, the new filing date is more than twelve months
after a declared priority date then the claim to priority must be
relinquished and the declared date cancelled, but [see
15.16.1](#ref15-16-1) and [2](#ref15-16-2).

### 15.13 {#section-16}

The applicant must be notified in writing whenever an application has
been re-dated, and, if applicable, that the declared priority date has
been cancelled.

### 15.14 {#section-17}

r.18, s.118 is also relevant.

Where the absence of missing matter is notified in the preliminary
examination report, the period of two months allowed for filing the
missing material and/or withdrawing the late-filed missing material may
be extended at the Comptroller's discretion in accordance with r.108(1)
[see
123.36.9](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36-9).
If no reply or extension request is received within the two month
period, or if a reply indicates that the applicant does not intend to
avail themselves of the opportunity to re-date, the missing matter and
all references to it should be treated as omitted from the application.
Instructions for publication should be provided to include on the front
page of the published application a notice [see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29)
that no copy of the particular drawing(s) or part of the description was
included in the application as filed. Where the missing part or drawing
is filed, it is not included in the published application s.16 but is
open to public inspection after publication.

### 15.15 {#section-18}

Where missing matter is treated as omitted, the specification as filed
and published under s.16(1) is to be read skipping any missing pages of
description or any reference to an absent figure (or a reference
character in it which does not occur also in a figure which is present).
For example, if Figures 2-4 are absent, a passage which reads "The cam
31 shown in Figures 2-4 controls the feed and is driven by the link
shown in Figures 1 and 3 operated by the crank 33 shown in Figure 1"
notionally reads "A cam (31) controls the feed and is driven by the link
shown in Figure 1 operated by the crank 33 shown in Figure 1"; "31" is
included if it occurs in a figure other than 2-4. If the specification
when so read fails to comply with s.14(3) and/or (5) objection should be
raised during substantive examination. Unless the applicant voluntarily
amends the specification to delete those references which are to be
treated as omitted, their deletion should be sought. (Such deletion
should be requested in the substantive examination report. However,
removal of references in the claims can be requested before s.16
publication).

### 15.16 {#section-19}

s.76(2) is also relevant.

If the applicant seeks to amend the specification after s.16 publication
by filing or reinstating an omitted drawing, this may be allowed,
provided it meets the requirements laid down for the timing and nature
of amendments [see
19.13-19.22](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-13),
in particular that it must not add subject-matter [see
76.09](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-09).

### Avoiding re-dating where there is a claim to priority

r.18(4), r.18(4)(b), r.18(4)(a), r.18(5)(a), r.18(5)(b) and r.18(7) is
also relevant.

### 15.16.1 {#ref15-16-1}

Where a declaration of priority is present on the date of filing,
s.15(7) allows that s.15(6)(b) shall not apply providing that:­-

\(a\) the applicant requests that it shall not apply;

\(b\) the request is made in writing (i.e. in a letter or e-mail) before
expiry of the period for filing the missing part(s) or drawing(s);

\(c\) the request identifies where the missing matter in question is
contained in the priority application (or applications);

\(d\) all the missing material is contained in the priority application
(or applications); and

\(e\) copies of the relevant priority applications, either certified or
otherwise verified to the satisfaction of the comptroller, are filed
within either sixteen months from the declared priority date or four
months from the date of the request that s.15(6)(b) should not apply.

\[Where there is any doubt as to whether the disclosure of late-filed
allegedly missing drawings or parts of the description are contained in
the priority application (or applications) the matter should be referred
to an examiner.\]

### 15.16.2 {#ref15-16-2}

r.18(6), r.112 is also relevant.

Where the Office has access to a copy of the (or each) priority
application, the comptroller will make and certify the necessary copies.

  -----------------------------------------------------------------------
   

  **Section 15(9)**

  Where, after an application for a patent has been filed and before the
  patent is granted -\
  (a) a new application is filed by the original applicant or his
  successor in title in accordance with rules in respect of any part of
  the matter contained in the earlier application, and\
  (b) the conditions mentioned in subsection (1) above are satisfied in
  relation to the new application (without the new application
  contravening section 76 below),\
  the new application shall be treated as having, as its date of filing,
  the date of filing the earlier application.
  -----------------------------------------------------------------------

### 15.17 {#ref15-17}

Section 15(9) was introduced by the Regulatory Reform (Patents)
Order2004; the wording of this provision is identical to the previous
section 15(4) in existence before the Order came into force on 1 January
2005. It allows for applicants to file, in certain circumstances, a new
application (commonly referred to as a "divisional application") which
takes the date of filing of an earlier application (commonly referred to
as a "parent application"). A divisional application under s.15(9) is
made by inserting the application number and filing date of the parent
application in part 6 of Patents Form 1. Section 15(9) does not exclude
the possibility of a divisional application itself giving rise to a
divisional application. Thus no objection should be raised simply
because an application claims the date of an earlier divisional
application. Where this is the case, the "earlier application" referred
to in s.15(9) and r.19 will be the earlier divisional application, and
so when deciding if the conditions of r.19 are fulfilled, it is the
status of this application and not that of the parent application from
which the earlier divisional application derived that should be
considered ([see 15.19](#ref15-19)). The later divisional application
will then take the filing date of the earlier divisional application,
which, if validly claimed, will be the filing date of the original
parent application.

\[ Examiners in particular should be aware of the different procedures
which apply when processing divisional applications. As well as the
detailed guidance contained in the following paragraphs, reference
should be made to the examiners' aide-memoire and checklist for
divisional applications ([see15.58](#ref15-58)). \]

### 15.18 {#ref15-18}

r.19(3) is also relevant.

The requirement that such an application must include a request for
antedating means that such a request must be made at the time of filing;
there is no provision for converting an ordinary application into a
divisional application after filing (P's Application \[1983\] RPC 269).

### 15.19 {#ref15-19}

r.19(2)(a), s.15(9) is also relevant.

A divisional application may not be made if the parent application has
been refused, withdrawn or taken to be withdrawn or treated as having
been withdrawn, if the period prescribed for putting the parent
application in order has expired, or if a patent has been granted on it.

### 15.20 {#ref15-20}

r.19(2) is also relevant.

Subject to the overriding considerations in 15.19 a divisional
application may be made at any time up to the date three months before
the compliance date (as prescribed by r.30). If the compliance period
(i.e. the period for putting the application in order, as prescribed by
r.30) is extended under rules 108(2) or 108(3), then the three month
deadline for filing a divisional application is calculated from this
extended period.

### 15.20.1 {#ref15-20-1}

r.30(3)(b), r.2(2), r.108(4), r.30(2)(b) is also relevant.

The compliance period for putting a divisional application in order
generally expires at the same time as the compliance period for the
earlier application, whose date of filing is to be treated under s.15(9)
as the date of filing of the divisional application. From 1st May 2023
the way divisional applications are accorded their compliance date
changed:

\(i\) For divisional applications filed prior to 1st May 2023, if the
compliance period prescribed on the earlier application (the so-called
''parent'' application) has been extended under rule 108, this extended
compliance period applied -- as the unextended compliance period -- to
the divisional application. However, note that if the compliance period
prescribed on the earlier application is later extended under rule 108,
that extension will not apply to any divisional applications which have
already been filed; a separate request will be required to extend the
compliance period for any such divisional applications.

\(ii\) For divisional applications filed on or after 1st May 2023, the
IPO attributes the divisional application a compliance period according
to r.30(2) of:\
\
(a) four years and six months beginning immediately after -\
\
i) where there is no declared priority date, the date of filing of the
earlier application;\
\
ii) or where there is a declared priority date, that date; or\
\
(b) if it expires later, the period of twelve months beginning
immediately after the date on which the first substantive examination
report for the earlier application is sent to the applicant.

All divisional applications filed on or after 1st May 2023 are accorded
a compliance date that is the same as the un-extended compliance date of
their parent application.

It is important to note that a divisional application which is filed
later than the end of the un-extended compliance period of the parent
application will be accorded an un-extended compliance date that has
already passed. Where a divisional application is lodged no later than
2-months after the expiry of the compliance period of the parent
application, the applicant can extend the compliance period under
r.108(2)/r.108(3). Such an extension may be necessary to allow amendment
of the application to address non-compliance with the Act and Rules.
Where a divisional application is lodged more than 2-months after the
expiry of the compliance period of the parent application, r.108(7)
prohibits any extension of the compliance period. Therefore, any
non-compliance with the Act or Rules will result in refusal of the
application.

More information on this practice change, including a set commonly asked
questions and answers, is available to view on the [Compliance periods
on divisional
applications](https://www.gov.uk/government/publications/compliance-periods-on-divisional-applications).

\[Where a divisional is received 3 or more months before the extended
compliance date of the parent application but less than 3 months before
the divisional application's compliance date, care must be taken to
correctly set out what options are available to the applicant. The EL5
letter may provide a useful starting point, but it will be necessary to
tailor this letter depending on the exact date on which an examination
report is issued. Due care should be given to the limitations imposed by
r.108(3) and r.108(7) (see above) on the availability of extensions to
the compliance period.\]

\[Where a divisional is received 3 or more months before the extended
compliance date of the parent application and up to 2 months after the
compliance date of the divisional application the examiner should
consider the application as a matter of urgency. If it is likely that
more than two months will pass from the divisional applications
compliance date before a report is issued, the applicant or their agent
should be contacted immediately to highlight this point. This
communication should emphasise that if the applicant does not extend the
compliance period prior to two months passing from the compliance date,
then they will no longer be able to do so and will no longer be able to
amend the application. In such cases, the application will be refused if
any objections are raised. The EL5 may provide a useful basis for such
communication but must be suitably amended to reflect the circumstances
of the application.\]

\[Where a divisional is received 3 or more months before the extended
compliance date of the parent application and more than 2 months after
the compliance date of the divisional application, it will stand or fall
as filed. All such cases should be highlighted to your Group Head. The
compliance period on such cases cannot be extended and no amendment of
the application will be possible. The EL5 may provide a useful basis for
such communication but must be suitably amended to reflect the
circumstances of the application.\]

::: call-to-action
\[Examiners should note that the provision under r.30(2)(b) whereby the
unextended compliance period expires 12 months from issue of the first
report if the first report is issued later than three years and six
months from the earliest date (see 18.47), only applies to the earliest
parent application. This provision does not apply to divisional
applications. However, the new date is treated as the unextended
compliance date for the parent application and any divisional
applications will be accorded that compliance date. \]
:::

### 15.20.2 {#ref15-20-2}

CoP is also relevant.

In view of the fixed time period for placing an application in order
prescribed by r.30, divisional applications should be filed early. For
example, if all possible search reports under s.17(6) have been issued,
it is helpful for applicants to come to early decisions as to what
inventions are to be pursued in divisional applications and to file
these applications promptly, filing the same number of divisional
applications as there are inventions requiring protection since it is
undesirable for divisional applications to raise further plurality of
invention issues.

### 15.21 {#ref15-21}

r.108(1), r.108(7) and r.108(3) is also relevant.

The comptroller has discretion under r.108(1) to extend the period of
time allowed under r.19 for filing a divisional application. However,
this discretion will normally be exercised only if the applicant shows
that the circumstances are exceptional and that they have been properly
diligent [Penwalt Corporation's Application BL
O/72/82](https://www.gov.uk/government/publications/patent-decision-007282);
International Barrier Corporations's Application; Kokusai Denshin
Denwa's Application (BL O/9/83); [Luk Lamellan und Kupplungsbau GmbH's
Application \[1997\] RPC
104](http://rpc.oxfordjournals.org/content/114/3/104){rel="external"}.

The comptroller can also extend the compliance period of a parent
application under r.108 to allow a divisional application to be filed
within the period prescribed by r.19. An extension under r.108(2) is
available as of right, a second extension (using r.108(3)) would require
the Comptroller's discretion. As with the extension to the time limit of
r.19, this discretion will normally be exercised only if the applicant
shows that the circumstances are exceptional and that they have been
properly diligent [see Ferguson's Application, BL
O/272/09](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/272/09){rel="external"}
and [Knauf Insulation's Application BL
O/098/13](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/098/13){rel="external"}.

If more than one month, but not more than three, are remaining on the
compliance period of the parent there are two options to allow a
divisional to be filed in time: either extend the r.19 period using
r.108(1) or extend the compliance date of the parent using r.108(2).

To file a divisional application when there is one month or less
remaining on the compliance period it will be necessary for either the
compliance period of the parent to be extended (under r. 108(2)) and the
period allowed for filing a divisional (r.19) to be extended (under
r.108(1)); or the compliance period of the parent to be extended twice
(once under r.108(2) and once under r.108(3)). The request(s) for
extending the compliance period of the parent will need to be in place
before or on the same date as the date the divisional application is
filed.

It was held by the Patents Court in [Kiwi Coders Corporation's
Application \[1986\] RPC
106](https://academic.oup.com/rpc/article/103/6/106/1568888){rel="external"}
that an appeal against a decision to refuse the allowance of a
divisional application will succeed only if it is established that the
comptroller's discretion was exercised wrongly in principle, i.e. there
was some error in approach. It is not sufficient that somebody else
might have reached a different conclusion upon the same facts. The
hearing officer had held that although a fresh citation had been raised
in the second report on the parent application, the citation did not
require or suggest the splitting out of subject matter and consequently
did not provide justification for the comptroller to permit filing well
beyond the period allowed.

In [Anderson's Application BL
O/297/02](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F297%2F02&submit=Go+%BB){rel="external"}
the hearing officer held, on the facts of the case, that the comptroller
should not exercise discretion to extend the period for filing a
divisional application. He then rejected an argument that to refuse late
filing of the divisional application was to deny the applicant his
rightful property in contravention of his human rights. The applicant
had been given ample opportunity to patent his invention, so there had
been no limitation of access to the patent system and no deprivation of
property within the meaning of UK patent law and the general principles
of international law enforced by the Human Rights Act 1998.

### 15.22 {#ref15-22}

r.6(5) is also relevant.

A divisional application may, but need not, make a declaration of
priority where one is made in the parent application. However, a
divisional application may not make a declaration of priority which is
not also made in the parent application. It should not be overlooked
that where a declaration of priority has been made in the parent
application for matter forming the invention of the divisional
application, then failure to make the same declaration in the divisional
application will mean that the parent application will, when published,
form part of the state of the art under s.2(3) for the divisional
application. If a priority declaration on a divisional is not made or is
determined to be invalid, there may be implications for the novelty of
the divisional application as demonstrated in [Nestec SA & Ors v Dualit
Ltd & Ors \[2013\] EWHC 923
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2013/923.html){rel="external"}
(see
[5.25.1-5.25.3](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-25-1)).

\[If a divisional does not make a claim to priority when one is present
on the parent, formalities should contact the applicant to establish if
this is intentional or an error.\]

### 15.23 {#section-20}

r.8(2) is also relevant.

In a case where the application specified in the declaration of priority
is one for a UK (national) patent, is an international application filed
at the Office, or is any other application of which a copy is on the
file of another UK (national) patent application (eg the parent
application) r.8(4)(c) in conjunction with r.112 relieves the applicant
from the necessity to request or pay for a certified copy. Where the
copy of the priority application is in a foreign language, there is no
routine obligation to file a translation, but the applicant may be
directed to do so under rule 9(1) if the examiner thinks necessary.
Where a translation is required, there is no obligation that it must be
verified.

### 15.24 {#ref15-24}

A divisional application must be filed by the original applicant for the
parent application or by their successor in title. Where more than one
applicant is named in the parent application, it is possible for the
divisional application to be made by only some of the original
applicants if there has been an assignment. Where the applicants in the
parent and divisional applications differ and no explanation is either
apparent or submitted the formalities examiner should raise an
objection; the application cannot proceed as a divisional if the
provisions of s.15(9) have not been complied with. A divisional
application may be filed, after the death of an applicant on the parent
application, in the name of the personal representative of the deceased.
The EPO Legal Board of Appeal in Trustees of Dartmouth College's
Application (J 2/01 \[2005\] OJEPO 88 and \[2004\] EPOR 54) held that
for an application filed jointly with two or more applicants, the right
to file a divisional application was only available to the registered
applicants jointly, and not to one of them alone or to fewer than all of
them.

### 15.25 {#section-21}

r.31(5)(b) is also relevant.

If one of several applicants for the parent application no longer has
any interest in or right to the invention of the parent application,
that application may be amended by making a written request [see
19.09](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-09).

### 15.25.1 {#section-22}

If, following amendment of a parent application, the applicant wishes to
delete the name of an inventor, the procedure described in paragraph
[13.18.1](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-18-1)
should be followed.

### 15.26 {#ref15-26}

r.10(1), r.10(2), r.21 is also relevant.

Where any applicant in the divisional application is not an inventor,
Form 7 must be filed, even if this has already been done in respect of
the parent application. This must be done within two months of filing
the divisional application, or, if is expires later, within the time
allowed for filing this form for the parent application (see 13.11).

### 15.27 {#ref15-27}

r.106(2), r.22(5), r.22(6), PR Sch. 4, Parts 2 and 3 is also relevant.

A request for preliminary examination and search, together with the
search fee, must be filed in respect of every divisional application,
even if the invention to which it relates has already been the subject
of a further search under s.17(6), on the parent application see
[17.111-17.114](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-111).
In such a case however the comptroller may refund the whole or part of
the search fee paid in respect of the divisional application ([see
15.47-49](#ref15-47)). The Form 9A and the search fee should be filed
within two months of filing the divisional application or within the
period allowed for doing this in respect of the parent application,
whichever expires later ([see 15.50](#ref15-50)). This period may be
extended in accordance with r.108(2) or (3) and (5)-(7) [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).
However, if the divisional application is initiated within the last six
months of the compliance period (including any extensions) [see 20.02
20.02.1](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)
then the period for filing the Form 9A and fee expires on the date on
which the divisional application is initiated. In this case, the period
may be extended in accordance with r.108(1) and (5) to (7) [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).

In the case that a divisional application is lodged more than two months
after the end of the un-extended compliance period of the parent
application, all aspects of the divisional application must be in order
for grant on the date on which the divisional application is lodged (see
15.20.1) and no extensions are available. In such cases, all necessary
forms must be filed on the date that the divisional application is
lodged.

### 15.28 {#section-23}

r.28(5), PR Sch. 4, Parts 2 and 3 is also relevant.

A request for substantive examination (Form 10) must be filed by the
later of a) two months after filing the new application or b) two years
from the declared priority date, or, where there is no declared priority
date, two years from the date to be treated as the filing date. This
time limit may be extended in accordance with r.108(2) or (3) and (5) to
(7) [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).
However, as with the search request, if the divisional application is
initiated within the last six months of the compliance period (including
any extensions) [see
20.02-20.02.1](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)
then the period for filing the Form 10 and fee expires on the date on
which the divisional application is initiated. In this case, the period
may be extended in accordance with r.108(1) and (5) to (7) [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).

In the case that a divisional application is filed more than two months
after the end of the un-extended compliance period of the parent
application, all aspects of the divisional application must be in order
for grant on the date on which the divisional application is initiated
(see 15.20.1), and no extensions are available.

### 15.28.1 {#section-24}

A divisional application on which Forms 9A and 10 have been filed
together should be subject to combined search and examination. This
applies even if no actual search is required because the claims of the
divisional application have already been searched as part of the parent
application.

### 15.29 {#ref15-29}

CoP is also relevant.

An application filed under s.15(9) containing additional matter not
disclosed in the earlier application may be filed, but shall not be
allowed to proceed under s.15(9) (i.e. with the filing date of the
earlier application) unless it is amended so as to exclude the
additional matter [see
76.02](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-02).
It is therefore unnecessary to take the precaution of filing identical
claims to the parent application in a divisional application to secure a
filing date. This practice is discouraged as amendment of the claims
will be necessary ([see 15.31](#ref15-31)), and the search examiner will
have no discretion but to defer the search [see 15.36](#ref15-36) or
issue a search report under s.17(5) in respect of the first invention
claimed, even if this is identical to the parent application ([see
15.38](#ref15-38)). An alternative remedy to the removal of the
additional matter in a divisional application lies in the relinquishment
of the request for divisional status [see
19.11](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-11).
The search/ substantive examiner should object to the presence of
additional subject matter either as a preliminary objection or at the
time of the search or examination report, as set out in
[15.35](#ref15-35) or [15.45](#ref15-45).

### 15.30 {#ref15-30}

There is no need for the claims of the divisional application to be of
the same scope as claims which were directed to the same subject-matter
in the parent application; nor need there have been equivalent claims in
the parent application. If a claim in a divisional application is
broader in scope than an equivalent claim originally present in the
parent application, this does not necessarily mean that the divisional
application contains additional disclosure; this must be decided on the
facts of the case [see
76.16-76.20](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-16).
If however the objection arises under s.14(5)(c) (i.e. a support
objection) instead of s.76(1) the divisional status is not impugned [see
76.21](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-21).
Where the parent application specified a range and a claim in the
divisional claims a sub-range, see
[18.69-18.69.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-69).

### 15.31 {#ref15-31}

s.18(5)

The parent and divisional applications or two or more applications
divided from the same parent, may not claim the same invention [see
18.91-18.97.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-91).

### 15.32 {#ref15-32}

CoP

In a patent or divisional application, there is no requirement to remove
description appearing in the other application or for inclusion of a
cross-reference if matter is described which is claimed in the other
application. However, as for any other application, it should be
considered whether the presence of extraneous description which does not
relate to the invention claimed in the particular application may cast
doubt on the scope of the claims. It is therefore helpful for the
description and drawings of a divisional application to be filed in a
form appropriate to the application, e.g. in terms of identification of
the embodiments relevant to the claimed invention.

### Procedure in examining groups

### Preliminary examination

### 15.33 {#ref15-33}

Whether or not an application is allowed to be antedated determines the
date when it should be published, and the time allowed for filing
priority documents, Forms 7, 9A and 10, and for putting the application
in order. Therefore where the formalities examiner has reported that an
application is made under s.15(9), it is necessary for the search
examiner, before doing anything else with the case, to consider whether
all the conditions set out in s.15(9) and r.19 have been met. However,
if a drawing referred to in the application is not filed with the
application then the procedure under s.15(5), (6) and (7) concerning the
possible re-dating of the date on which the application was initiated
should be dealt with before consideration of whether the application can
be treated as having an earlier date of filing under s.15(9) [see
15.08-16](#ref15-08).

::: call-to-action
### 15.33.1 {#ref15-33-1}

If antedating is not allowable then it is possible for the later
application to proceed with its date of filing as the filing date. The
applicant may proactively submit a request, in writing, to amend the
Form 1, see 19.11. The later application may then be examined as a new
application. If the applicant does not respond within the specified time
limit after being informed that antedating is not allowable, the later
application will be treated as a new application having been filed on
its actual filing date.

\[If no response is received within the specified time limit after
informing the applicant that antedating is not allowable, the
substantive examiner should issue a LETTERAFS on the later application
informing the applicant that their later application will be treated as
a new application with its actual date of filing as the filing date. If
appropriate, the examiner may warn the applicant that the earlier
"parent" application is likely to be citable against the novelty and
inventiveness of the later application and that the search will
therefore be truncated. The applicant should be informed of the option
to withdraw the later application and request a refund of the fees paid.
If no response is received to this letter, search and/or substantive
examination should be carried out in accordance with the forms already
filed in association with the later application.\]
:::

### 15.34 {#ref15-34}

When a divisional application is filed with less than three months
remaining on the compliance period (but earlier than the date of issue
of the grant letter) the first question to be decided is whether or not
discretion under r.108(1) should be exercised to allow the application
to proceed under s.15(9) ([see 15.21](#ref15-21)). This matter should be
disposed of before any further proceedings on the case, including any
consideration as to whether amendment is necessary under s.76(1), take
place. If no, or inadequate, reasons as to why the late filing should be
allowed are given initially, a suitable explanation should be sought,
only a short time being allowed for a reply. If a convincing response is
not forthcoming, a hearing should be offered. The applicant should be
informed that proceedings on the application will not continue until the
question as to whether the late filing will be allowed has been settled.

\[ The decision whether to allow the late filing may be taken by a
Senior Examiner, who may also offer a hearing when a convincing reason
for the late filing is not forthcoming. If an application seeking
divisional status is received after an EL34/3 has been issued on the
"parent" but before issue of the grant letter, the Examiner Assistant
(EA) will not set the COPS status of the parent application to "Ready
for Grant" on the date shown in the EL34/EL3 and the grant letter will
not be issued. Instead, the EA will check that a PDAX message for the
divisional application has been sent to the substantive examiner of the
parent application. The EA will not set the COPS status of the parent
application to "Ready for Grant" until the substantive examiner informs
them that the parent may be granted. Where issue of the grant letter has
already occurred, no further action can be taken as a divisional cannot
be filed after grant of the parent.\]

### 15.35 {#ref15-35}

The search examiner should not make an exhaustive analysis of the parent
and divisional applications, but should investigate them sufficiently to
come to a prima facie view as to whether s.76(1) has been complied with.
If it has not, search should be deferred and a preliminary objection
raised, requiring excision of the additional matter (unless the
applicant can satisfy the examiner that there is no such matter present)
before the application can proceed. When making this objection the
examiner should clearly notify the applicant that the application will
not proceed to publication until any added matter is removed, see 15.39.
The period allowed for response to the objection should normally be one
month (extendable as-of-right by two months or until the expiry of the
s.20 period - see
[117B.01-05](/guidance/manual-of-patent-practice-mopp/section-117b-extension-of-time-limits-specified-by-comptroller#ref117b-01).
However, if it is clear that the added matter is not relevant to the
search, the search may be made and objection to the added matter issued
simultaneously with the search report ([see also 15.39](#ref15-39) and
[15.45](#ref15-45)).

\[If search and/or examination are deferred EL24 should be used to make
a preliminary objection. SC15 should be used to issue the objection to
added matter with the search report. The Examination Support Officer
should be instructed as to what is the correct processing status to be
recorded. If EL24 issues the status should be set as "Out for ABS/ABCSE"
(i.e. Out for Action Before Search/Action Before CSE), but the case
should not be booked out on PAFS. Only when the search/CSE is ready to
be actioned should it be booked out on PAFS. If added matter is present
and search (or CSE) is performed see [15.39](#ref15-39)\]

\[ If an EL24 is issued on a divisional application and whilst waiting
for the applicant to respond to the EL24, the parent application is
granted or otherwise sent to grant, this does not affect the ability of
the divisional application to later be ante-dated provided any added
matter is subsequently removed and the criteria for ante-dating is
met\].

### 15.36 {#ref15-36}

CoP is also relevant.

The search examiner may also defer search and examination of a
divisional application not fulfilling the points laid out in the Code of
Practice by requesting the filing of an amended specification before
performing the search. The applicant should be informed which points of
the Code are not met, and a period of one month should normally be
allowed for filing an amended specification.

### Search

### 15.37 {#section-25}

Since, where it is determined that the antedating is allowable, the
application will in many cases already be due for publication, a
divisional application should, as far as possible, be given on receipt
priority over other applications for search and publication. Where
however the application, on the basis of the divisional date, is very
close to the compliance date, it must be treated, in preference to all
others, as a matter of urgency ([see 15.20.1](#ref15-20-1)). See
[15.44](#ref15-44) for procedure on reporting the allowance of
ante-dating.

### 15.38 {#ref15-38}

A search report under s.17(5), accompanied by a copy of each document
cited [see
17.104.1](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-104-1),
should be issued in the same manner as for an ordinary application. The
fact that no actual search has been carried out on a particular
divisional application, either because the initial search on the parent
application was sufficiently wide to comprehend the invention of the
divisional application, or because the invention had been the subject of
a further search under s.17(6) on the parent application, does not
render the issuing of a formal search report unnecessary. If the claims
of the divisional application lack unity of invention the search report
should be in respect of the first invention, even when a report in
respect of this invention has already issued in connection with the
parent application, since there is no discretion to do otherwise [see
17.106](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-106).
If the parent application was an international application, it will be
necessary to carry out a search unless it can be confidently deduced
that the international search on the parent must have comprehended the
divisional invention, but in any event a search report must be issued.
If the claims were amended in response to an objection under s.76(1) and
the search was done in respect of the amended claims, a notice to that
effect should be included on the front page of the published A document
[see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29).

\[ To obtain such a notice on the front page, the examiner should add a
minute to the dossier instructing Formalities of the exact wording of
the notice, for example: "The claims of this application were amended in
response to an objection raised under s.76(1) of the Patents Act 1977
and the searches carried out under s.17 of the Act were conducted in
respect of the amended claims." A PSM message should then be sent to the
relevant Formalities Group/Examiner alerting them to the minute on file,
[see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29)\]

### S.16 Publication {#s16-publication}

### 15.39 {#ref15-39}

s.16(1) is also relevant.

The requirement that an application should be published "as soon as
possible after the end of the prescribed period" [see
16.01](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-01)
is construed as meaning as soon as is reasonably practicable after such
date and, of course, applies equally to a divisional application.
However, publication cannot occur until any added matter detected at
that stage has been deleted from the application, under s.76(1) [see
15.35](#ref15-35),
[16.08](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-08)
and
[76.02](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-02).
What is published under s.16(1) is not the application as so amended but
the application as filed, ie including the additional matter. The
divisional application should normally be sent for publication after
search and as soon as at least 16 months and 3 weeks from the priority
or filing date has elapsed, and before substantive examination, provided
this is not due.

\[Processing status 3 "Searched - Do not s.16 publish yet" should be
used when added matter has been found in a divisional application. The
examiner should also add a minute to the file noting where added matter
is present and that the processing status has been changed. Once the
added matter has been removed the processing status 5 "May be s.16
published" should be used and the examiner should send a minute to FMLS
informing them publication is now possible.\]

### 15.40 {#ref15-40}

Full consideration of whether ante-dating is allowable is made at the
substantive examination stage; at search stage (where the divisional
does not undergo combined search and examination) only a prima facie
assessment of added matter is made ([see 15.35](#ref15-35)). Therefore
when a divisional application is sent for publication before substantive
examination is completed, allowance of ante-dating is only provisional.
In such cases, instructions for publication should be provided to
include a notice [see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29)
on the front page of the application to the effect that the filing date
accorded is provisional and is subject to ratification or amendment.

\[ To obtain such a notice on the front page, the examiner should add a
minute to the dossier instructing Formalities of the exact wording of
the notice, for example: "This divisional application is awaiting
substantive examination. Consequently, the filing date as currently
accorded is provisional and is subject to ratification or amendment." A
PSM message should then be sent to the relevant Formalities
Group/Examiner alerting them to the minute on file, [see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29).
\]

### 15.41 {#ref15-41}

r.31 is also relevant.

If however the application is due or overdue for substantive examination
when the search has been made, the search examiner should proceed to
carry out the substantive examination, the report of which should be
issued at the same time as the search report. When amendments have been
filed the search report should relate to any amended claims if the
amendments are allowable or to the original claims if the Comptroller's
consent to the amendments is withheld and the amendments would not have
substantially affected the search strategy [see
17.35](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-35).
Similarly, the substantive examination should be carried out on the
basis of the original application or the application as amended,
depending on whether the Comptroller consents to the amendments. If the
comptroller's consent is withheld, the objections to the amendments
should be explained in the examination report.

\[ SE4 should be used in the case of a report under s.18(3). If there is
plurality of invention, RC6 and RC6A should be added to the examiner's
combined search and examination report. \]

\[ Where the examiner has no objections to the application and the
invention in suit was clearly claimed in the published parent and if the
parent has been published for at least three months, an SE5 should be
issued. The SE5 indicates that a report under s.18(4), stating that the
application complies with the Act and Rules, will not be issued until
the application has been published. If an SE5 is issued, the examiner
should set a diary action for on or around the intended date of
publication. Whenever a diary is set, a diary action should also be
added to the PDAX dossier. Once the application is published and if it
remains in order, the examiner should complete a grant form (see
[18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18.86))
and arrange for EL3 to be issued. If amendments are filed after SE5 is
issued but before publication and the amendments are allowable, then the
examiner should wait until the case is published and at that time
complete a grant form and arrange for EL3 to be issued. Note that when
sending cases to grant, if less than 21 months have elapsed from the
priority date or the substantive examiner is not otherwise satisfied
that the search is complete in respect of s.2(3) art, the application
should be diarised for return after grant in order to complete this
search and the applicant should be informed by the examiner arranging
(via the grant form) for RC46 to be included in the relevant intention
to grant letter (see also
[17.118](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-118)).
\]

\[Where the examiner has no objections to the application and the
invention in suit was not clearly claimed in the parent, SE3 should
issue indicating that a report under s.18(4) that the application
complies with the Act and Rules will not normally be issued until three
months after the application has been published; that such delay is to
allow for third parties to be given the opportunity to file observations
under s.21 and for the search to be updated (if necessary). However, in
the case of a divisional filed very close to the compliance date, there
may not be sufficient time remaining to allow the usual three month
period after publication for s.21 observations. Even if the invention of
the divisional was not clearly claimed in the published parent
application, the issuing of the intention to grant letter on the
divisional cannot be delayed beyond the compliance date in order simply
to allow for the expiry of this three-month period (for the procedure in
this situation see
[18.07.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#18.07-2) -
second square bracket). If a divisional is filed late such that it will
be published after the end of the compliance period, a modified SE3 or
EL32 if a s.18 report has previously been issued, should be issued and a
diary entry should be created for the return of the file shortly after
publication. The SE3/EL32 should be modified as required, for example:
to note that the delay is to wait for the application to be published
and to remove any reference to the three month period/updating the
search. When the diary returns, the examiner should complete a grant
form ([see
18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#18.86)).

\[Any allowable amendments filed before combined search and examination
should be acknowledged using SC1.

\[The fact that COC action (consequent on the filing of Form 23) has yet
to be taken is not an outstanding formal requirement and, thus, does not
prevent the examiner from issuing a s.18(4) report or from marking the
application in order for s.16 publication ([see 15.42](#ref15-42)). \]

### 15.42 {#ref15-42}

If formalities are complied with for s.16 publication, the application
should then be sent for publication. If formalities have not been
complied with, the applicant will have been informed that the
application cannot be published until the outstanding requirements have
been met.

### Substantive examination

[See also 15.41-15.42](#ref15-41).

### 15.43 {#ref15-43}

s.18(5) is also relevant.

During the substantive examination, the substantive examiner should not
only satisfy themselves that there is no added subject matter in any
part of the application, but also that there is no conflict of claims
between the parent and divisional applications [see
18.91-18.97.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-91).
Likewise if two or more applications are divided from a single parent,
then their claims must not be in conflict.

### 15.44 {#ref15-44}

If at the first substantive examination stage a divisional application
satisfies all the requirements of s.15(9) the allowance of the
divisional date should be notified in the first report under s.18; if it
does not, then notification of the allowance of antedating should be
deferred until compliance is obtained.

\[RC21 should be used in an examination report (or SE3/SE5 if
appropriate) to notify the applicant of the allowance of the divisional
date. If allowance is deferred until grant, RC21 should be used in the
appropriate intention to grant letter. \]

### 15.45 {#ref15-45}

If an objection that a divisional application as filed contains added
matter is raised at substantive examination, it may be included in the
report under s.18(3). However, if the nature of the added matter is such
that it seems unlikely that the application will be pursued, a
preliminary objection may be made and full examination deferred ([see
15.35](#ref15-35)). If the application is withdrawn or refused, a
request for the refund of the fee paid on Form 10 should be allowed if
no report under s.18 has been issued.

\[If examination is deferred EL24 should be used to make a preliminary
objection. The Examination Support Officer should be clearly instructed
to change the PAFS action to ''Action before Examination (ABE)", but the
case should not be booked out on PAFS ([see also 15.35](#ref15-35)).
RC24 should be used to make the objection to added matter in the s.18(3)
report. \]

::: call-to-action
\[If an objection to added matter is raised during combined search and
examination of a divisional application, processing status 3 "Searched
-- Do not s.16 publish yet" should be used, see 15.39. The examiner
should also add a minute to the file noting where added matter is
present and that the processing status has been changed. When making
this objection the letter accompanying the examination report should
clearly notify the applicant that the application will not proceed to
publication until any added matter is removed.

Once the added matter has been removed the processing status 5 "May be
s.16 published" should be used and the examiner should send a minute to
FMLS informing them publication is now possible.\]
:::

### 15.46 {#ref15-46}

A parent application may be substantively examined early (provided Form
10 has been filed) and at the same time as a divisional undergoes
combined search and examination unless the applicant or their agent
agrees otherwise. When the parent application and the divisional
application are processed together in this way, the same period for
response to reports under s.18(3) should be set for both applications.
If the examination of the parent application is deferred until after
combined search and examination of the divisional application, the
period for response to a report under s.18(3) on the divisional should
be set to match that period which would have been applicable for a first
response on the parent application see
[18.49](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-49).
One application may be sent for grant before the other, however if
amendment of the outstanding application subsequently resulted in
conflict this would then need to be cured by amendment of that
application, unless amendment is applied for under s.27 in respect of
the granted application.

\[Normally, a notification of intention to grant letter (EL34 or EL3)
should be issued and the application sent to grant as soon as it is in
order, irrespective of the state of the remainder of the family.
However, an application may be held back where appropriate, for example
if there is conflict of claims with another family member which is not
yet in order or if it is not clear whether the current scope of another
family member which is not yet in order conflicts with the claims of the
application which is in order.

\[The PROSE clause for objecting under s.14(5)(d) in the first instance
(RC6) warns the applicant that there will be no automatic reminders of
the deadline for filing a divisional. \]

\[When a plurality objection has been overcome but a divisional
application has not been filed, PROSE clause RC26 may be inserted into
the subsequent s.18(3) report to inform the applicant of the deadline
for filing a divisional application. This clause should be used at this
stage regardless of whether a divisional has been foreshadowed or not.\]

\[Following the introduction of notifications of intention to grant on 1
October 2016, the practice of "foreshadowing" the filing of a divisional
application (i.e. indicating that a divisional application is likely to
be filed in the future) has no effect. The examiner should therefore
take no further action in response to such a foreshadowing. The
notification of intention to grant will always provide the applicant
with at least one month's advance notice of grant, during which time a
divisional application may be filed (as long as it is filed at least
three months before the compliance date) or other actions taken. The
date given in the intention to grant letter (EL34 or EL3) is not a
specified period and is therefore not subject to the two-month
as-of-right extension. The application should be sent to grant shortly
after this date if no actions have been taken since issue of the
intention to grant letter (see
[18.86.1-18.86.3](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-87)).\]

\[ In the exceptional circumstance of a request for accelerated
examination being acceded to in respect of a member of the family, the
formalities examiner should be instructed to process that member for
grant before the other member(s). Any conflict would need to be cured by
amendment of the outstanding application, unless amendment is applied
for under s.27 in respect of the granted application. For grant of a
family member, [see
18.87](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-87).\]

### 15.46.1 {#section-26}

r.108(2) and r.108(3) is also relevant

Rule 108(2) and rule 108(3) can be used to extend the compliance period
after the intention to grant letter has been issued, [see
18.86.5](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86-5).

### Requests for refund of fees

r.106 is also relevant.

### 15.47 {#ref15-47}

The comptroller has discretion to remit the whole or part of the search
fee paid in connection with a divisional application which relates to an
invention in respect of which a search fee has already been paid on the
parent application. A request for remission of the search fee must be
made in writing by the applicant to the comptroller. When an applicant
appears to be entitled to a refund and it appears most unlikely that
s.17(8) would need to be invoked after a refund had been made, a refund
can be made before the divisional application is in order for grant. If
on the other hand, an early refund seems inappropriate, eg because the
need for a supplementary search under s.17(8) cannot be ruled out,
consideration of a request for a refund should be deferred and
reconsidered later, normally when the application is in order for grant.

### 15.48 {#section-27}

When considering an application for a refund of the search fee, the
general principle of one search fee per patent application should be
followed. Therefore, if two (or more) search fees have been paid on the
parent application, and the parent and divisional applications relate to
different ones of the inventions searched for on the parent application,
a refund should normally be given. If the parent application is an
international application made under the PCT, a refund of the search fee
paid on the divisional application should only be given if more than one
search fee was paid during the national phase of the parent application,
and one of those fees covered the invention being searched in the
divisional application. If just one search fee was paid during the
national phase then no refund is given. The number of search fees paid
to the International Search Authority is not a consideration.

### 15.49 {#ref15-49}

If the searching necessary on the divisional application has been
restricted at all stages to the "top-up" search and to any further
searching consequent upon the premature termination of the relevant
further search on the parent application, the full search fee paid on
the divisional application should be refunded unless this results in the
payment of fewer search fees than the total number of parent and
divisional applications in the patent family. Furthermore, no refund
should be made if a search becomes necessary due to amendment of the
claims of a divisional application. If, in view of the search made, the
request is refused the examiner should inform the applicant so. If only
one search fee has been paid on the parent application (and hence only
the first invention specified in its claims has been searched), a refund
of the search fee paid on a divisional application should not be given
even if the divisional application is for the first invention of the
parent. There is no appeal from a decision of the comptroller to refuse
a refund.

\[The substantive examiner should minute the file to indicate whether or
not the request for a refund may be allowed. If a request for refund of
the search fee paid in a divisional application is allowed before the
application is in order, the substantive examiner should issue EL8 or
ELC8, as appropriate, and instruct the appropriate formalities group to
arrange for an early refund. On the other hand, if consideration of a
refund request is deferred, the examiner should issue ELC9. If and when
the refund is allowed, the appropriate formalities group should be asked
to attend to it.

\[Although no refund is given, an additional search fee is not required
for a second invention in a divisional application when a search report
has issued in respect of the first invention in the divisional
application, where this first invention was already searched in
connection with the parent application (see 15.38), and then a further
search becomes necessary for the second, previously unsearched invention
in the divisional application sometime later due to the deletion of the
first (and previously searched) invention. \]

  -----------------------------------------------------------------------
   

  **Section 15(10)**

  Where an application has a date of filing by virtue of this section,
  the application shall be treated as having been withdrawn if any of the
  following applies­\
  (a) the applicant fails to file at the Patent Office, before the end of
  the prescribed period, one or more claims and the abstract;\
  (b) where a reference to an earlier relevant application has been filed
  as mentioned in subsection (1)(c)(ii) above-\
  ­(i) the applicant fails to file at the Patent Office, before the end
  of the prescribed period, a description of the invention for which the
  patent is sought;\
  (ii) the applicant fails to file at the Patent Office, before the end
  of the prescribed period, a copy of the application referred to,
  complying with the relevant requirements of rules;\
  (c) the applicant fails to pay the application fee before the end of
  the prescribed period;\
  (d) the applicant fails, before the end of the prescribed period, to
  make a request for a search under section 17 below and pay the search
  fee.
  -----------------------------------------------------------------------

### 15.50 {#ref15-50}

r.22(1), r.22(2), r.22(7)(a), r.22(7)(b) is also relevant.

Where there is no declared priority date, the claims, abstract,
application fee, request for search (on Form 9A) and the search fee must
all be filed not later than twelve months from the date of filing.
Where, instead of filing a description, a reference is made under
s.15(1)(c)(ii) to an earlier application, s.15(10)(b)(i) also requires
the description to be filed within this period. Where there is a
declared priority date these elements (including, where relevant, a
description filed under s.15(10)(b)(i)) must be filed before expiry of
whichever is the later of twelve months from the earliest priority date
or two months from the date of filing. The periods specified in r.22(1)
and (2) are listed in Part 2 of Schedule 4 of the Rules. They may
therefore be extended once by two months as of right under r.108(2), and
further extensions (in tranches of two months) may be granted at the
discretion of the comptroller under rr.108(3) to (7). So as to provide a
clear demarcation between extension of time under r.108 and
reinstatement under s.20A, these Rules are also listed in Part 3 of
Schedule 4, and if a request for extension (or further extension) is not
received before expiry of the requested extension, the application must
be treated as having been withdrawn [see
123.34-42](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).
Once extension is not longer available, reinstatement under s.20A may be
requested.

### 15.51 {#section-28}

r.22(3), r.108(5), r.108(7) is also relevant.

Where a reference is made under s.15(1)(c)(ii) to an earlier
application, s.15(10)(b)(ii) requires that a certified copy of the
application referred to (accompanied, if necessary by a translation)
must be filed within the four months if the application is not to be
treated as withdrawn. This period can be extended under r.108(1) and
(5)-(7) in tranches of two months [see
123.36.10-123.41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36-10)
at the discretion of the comptroller in cases where there is a genuine
difficulty in obtaining the necessary certified copy within the time
prescribed.

### 15.52 {#ref15-52}

r.22(5), r.22(6), PR Sch. 4, Parts 2 and 3 also relevant.

In the case of an application which claims an earlier filing date under
s.8(3), 12(6), 15(9) or 37(4), the claims, abstract, application fee,
request for search and search fee must be filed within two months of
making the application or within the relevant period for the earlier
application - whichever is the later. This period is extendable once by
two months as of right under r.108(2), and then in two month tranches at
the discretion of the comptroller under rr.108(3) and (5) to (7),
provided the request is received before the expiry of the requested
extension [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).
If this extension is no longer available, reinstatement under s.20A may
be requested. However, if the application is filed within the last six
months of the compliance period (including any extensions), the claims,
abstract, application fee and the search request and fee must be filed
at the time of making the application.

### 15.53 {#section-29}

In the case of an application which has been converted from an
application for a European patent (UK), the period for paying the
application fee, filing Form 9A, paying the search fee and filing Form 7
is governed by r.58(4), (see s.81); in the case of an international
application the application fee is nil, and the period for filing Form
9A and paying the search fee is governed by r.68(3) [see
89A.12](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-12)
and
[89A.18](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-18).

### 15.54 {#section-30}

s.14(5)(c) is also relevant.

Although the Act provides for the claims to be filed later than the
description, they must of course be supported by the description as
originally filed. If no claims were present on the filing date of the
application, then when claims are filed for the first time this does not
constitute amendment of the application. However once the application
contains at least one claim, the subsequent filing of claims (except by
way of correction under s.117) does entail amendment of the application
and is governed by r.31.

### 15.55 {#ref15-55}

s.16(1), s.118(3)(b), r.55 is also relevant.

If the claims, abstract, application fee, request for search, search
fee, and where a reference under s.15(1)(c)(ii) is made, a description
and certified copy of the application, have not all been filed by the
end of the respective prescribed periods, the application is taken to be
withdrawn. It is not published, but the fact that it has been withdrawn
is advertised in the Journal. It may continue to serve as a basis for a
declaration of priority in respect of a later application, subject to
s.5(2), (2A) and (3).

r.22, r.108, r.10 is also relevant.

\[Where no claims and/or an abstract have been filed within the period
prescribed by r.22 and the two-month period in which an extension could
be requested on Patents Form 52 under r.108(2) has expired, the
application is taken to be withdrawn. It is not possible for any
extension to be granted once this two-month period has passed
(r.108(7)). A standard letter (WR4) is issued by the Formalities Manager
warning of the intention to advertise the application as taken to be
withdrawn. Similar considerations apply where a Patents Form 7 has not
been filed within the period prescribed by r.10. For termination
procedure, see
[14.199.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-199-1).\]

### 15.56 {#ref15-56}

Once the application has been treated as withdrawn through failure to
file any of the claims, abstract, application fee, request for search,
search fee, and, where a reference under s.15(1)(c)(ii) is made, a
description and certified copy of the application, reinstatement under
s.20A is the only option remaining available. In particular, the
deletion of an incorrect claim to priority as a correction under section
117 is not allowable, even if allowance of the deletion would mean that
time for filing a request for search still remained (Payne's Application
\[1985\] RPC 193); see also
[117.19](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-19).

  -------------------------------------------------------------------------------------
   
  **Section 15(11)**
  In this section "relevant application" has the meaning given by section 5(5) above.
  -------------------------------------------------------------------------------------

### 15.57 {#section-31}

Where a reference is made under s.15(1)(c)(ii) to an earlier
application, that earlier application may be either an application for a
patent under the 1977 Act or an application made in a convention
country. The definition of what is a relevant application is the same as
for s.5 of the Act [see
5.30](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-30).

### Annex to section 15 of the Manual - Divisional applications

### 15.58 {#ref15-58}

Section 15 of the Manual refers at various points to the processing of
divisional applications. In particular, references are to be found from
[15.17](#ref15-17) to [15.49](#ref15-49).

\[Examiners in particular should be aware of the different procedures
which apply when processing divisional applications. Of particular help
will be the examiners' aide-memoire and checklist for divisional
applications, which are reproduced below. Neither the aide-memoire nor
the checklist is intended to be exhaustive. More detailed guidance is
given in the paragraphs of the Manual referred to in square brackets.

### \[Divisional applications: examiners' aide-memoire\]

### When a purported divisional is filed

1). There are various basic formal requirements which must be satisfied
if an application is to qualify as a "divisional". These matters are
considered by the appropriate formalities group
[15.17-15.19](#ref15-17), [15.22-15.24](#ref15-22).

### Has it been filed in time?

2).This is the first thing for the examiner to decide (unless a drawing
has been omitted). If the requirements of s.15(9) and r.19 are not met,
there can be no progress as a divisional [15.33](#ref15-33).

3). R.19 sets out the time periods within which a divisional can usually
be filed. Put simply, a divisional can be filed as-of-right on a parent
providing that the parent has not been granted or terminated, and the
divisional is filed at least three months before the compliance date
(including any extensions). Where the compliance date of the parent has
been extended, it is important to consider whether the compliance date
accorded to the divisional application has already passed
[15.19-15.20](#ref15-19).

4). If lodged outside these periods, an application can still be
admitted as a divisional filing, but only at the comptroller's
discretion. This requires exceptional circumstances and a diligent
applicant [15.21](#ref15-21), [15.34](#ref15-34) \]. However, a
divisional cannot be filed after grant [15.19](/#ref15-19).

### Added matter

5). S.15(9) means that a divisional cannot be allowed to proceed unless
amended to exclude any detected additional matter [15.29](#ref15-29),
[76.02](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-02),
[76.19](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-19).
The usual criteria are used to determine whether or not matter has been
added
[76.03](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-03):
for example, the presence in a divisional of claims having a different
scope from, or no equivalent in, the parent does not necessarily mean
that matter has been added. An objection under s.14(5)(c) should not be
confused with one under s.76; the former does not impugn divisional
status [15.30](#ref15-30).

6). At the search stage, if this is separate from substantive
examination, a prima facie view is taken by the search examiner on the
added matter question. If added matter is judged to be present, its
excision may be requested before the application is allowed to proceed,
the search being deferred; EL24 is sent and one month allowed. The
examiner should clearly notify the applicant in the EL24 that
publication cannot occur until any added matter is removed ([see
15.39](#ref15-39)). An alternative if the added matter is not relevant
to the search is to do the search and object to added matter in the
search letter using SC15 [15.35](#ref15-35). The implications of added
matter for publication under s.16 are given in para 11 below.

7). Since the search examiner's view is not definitive, the substantive
examiner needs to satisfy themself on the point [15.43](#ref15-43). If
added matter is judged to be present, full examination may be deferred
if the application is not likely to proceed, sending EL24 and allowing
one month for reply. Otherwise the full examination is conducted and
objection raised using RC24 [15.45](#ref15-45).

### Allowing divisional status

8). Once ante-dating is allowable, by virtue of the requirements of
s.15(9) and r.19 being met (see above), the applicant should be informed
using RC21. If a search or examination letter is issuing before it is
allowable, notice that compliance is awaited should be given
[15.44](#ref15-44).

### The search

9). The search itself is performed as usual, unless combined with
examination (see paras 17-20 below). Issue of a search report on a
divisional cannot be dispensed with even if the search has been covered
by that on the parent [15.38](#ref15-38). Consideration of a request for
a refund of the search fee [15.47-15.49](#ref15-47) may be given now
unless it appears likely that s.17(8) might need to be invoked, in which
case final consideration should be deferred until just before grant (see
para 20 below). ELC8(EL8)/ELC9 can be used to notify allowance/deferment
respectively of such a request.

### Publication

10). Publication cannot occur until any added matter detected at that
stage has been deleted from the application. However, what is published
is not the application as so amended but the application as filed, ie
including the added matter [15.39](#ref15-39),
[16.08](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-08),
[76.02](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-02).

11). In addition, special instructions for publication may need to be
given if:

-   publication is before allowance of the divisional date (e.g. because
    substantive examination has not yet taken place), to say that the
    filing date is provisional [15.40](#ref15-40)

-   amended claims have been filed in response to an objection under
    s.76(1). This is an examiner's free text entry [15.38](#ref15-38),
    [16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29)

12). \[deleted\]

### Substantive examination

13). It is not necessary (though it is of course allowable) to include
cross-references between parent and divisional applications. Nor is
there any specific requirement that disclosure should not be
unnecessarily duplicated between the applications. However, it should be
considered whether description of matter which is not relevant to the
application in suit throws any doubt on the scope of the claims
[15.32](#ref15-32).

14). To comply with s.18(5) parent and divisional claims should not
conflict. Special care is needed with omnibus claims [15.31](#ref15-31),
[18.91](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-91)-[18.97.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-97-1).

15). Reply periods to s.18(3) reports should be set having regard to the
expiry of the compliance period, which is the later of 4½ years from the
priority/filing date of the earlier case, whose date of filing is to be
treated under s.15(9) as the date of filing of the divisional, or 12
months from the date of issue of the first s.18 report on this earlier
case (see also para 17 below).

16). If Forms 9 and 10 have been filed on the same day or shortly one
after the other with a request for combined search and examination, the
search and examination are combined. If the examination report is one
under s.18(3) a single combined search and examination report should
issue using SE4. Where the examiner has no objections to the application
just the search report should issue, either SE5 if the invention claimed
in the divisional was clearly claimed in the published parent, or SE3 if
not [15.41](#ref15-41). If amendments have been filed before issue of
the search report the appropriate version of SC1 should be added
[15.41](#ref15-41). ELC5 should also be added to the covering letter of
the first s.18 report on a divisional when the first s.18 report on the
relevant earlier case issued more than 3½ years from its priority/filing
date (see also para 16 above)
[18.47](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-47),
[20.02](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02).

17). Post-"A"-publication COPS statuses, eg 9 and 10, must not be set
until after "A"-publication is complete.

18). If the first substantive examination report on a late-filed
divisional is under s.18(4), grant cannot be accelerated by waiving the
usual two-month period as it is necessarily delayed until after s.16
publication
[18.81](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-81).

### In order stage

19). Any outstanding request for a refund of the search fee can be
determined now and the applicant notified of the outcome (see para 10
above) [15.47-15.49](#ref15-47) .

20). Applications do not normally have to be held back from grant until
the remainder of the family is in order. However an application may be
held back where appropriate, for example if there is conflict of claims
with another family member which is not yet in order.
[15.46](#ref15-46).

21). Even if the invention of the divisional was not clearly claimed in
the published parent application, the issuing of the intention to grant
letter on the divisional cannot be delayed beyond the compliance date in
order simply to allow for the expiry of the three-month period for
third-party observations. [15.41](#ref15-41).

### \[Divisional applications: examiners' checklist\]

There are various basic formal requirements for a divisional, which the
formalities examiner assesses. [15.17-15.19](#ref15-17),
[15.22-15.24](#ref15-22)

1). Has the divisional been filed in time? [15.19-15-21](#ref15-19),
[15.33-15.34](#ref15-33)

2). Is there any added matter? Are SC15/EL24/RC24 necessary?
[15.29-15.30](#ref15-29), [15.35, 15.43](#ref15-35), [15.45](#ref15-45),
[76.02](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-02)-[76.03](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-03),
[76.19](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-19)

3). Is ante-dating allowable? Is RC21 necessary? [15.44](#ref15-44)

4). Is a search fee refund due? Is ELC8(EL8)/ELC9 necessary?

5). Publication:

a\. is it due?

b\. what is to be published?

c\. are any special instructions for publication needed?

d\. has the applicant been told?

6). Is any description of matter included which is relevant to the
claims of the parent but not the divisional and casts doubt on the scope
of the claims? [15.32](#ref15-32)

7). Is there claim conflict, eg of omnibus claims? [15.31](#ref15-31),
[18.91](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-91)-[18.97.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-97-1)

8). Is combined search and examination appropriate? Which of SE3, SE4 or
SE5 is necessary? Is ELC5 necessary? [15.41](#ref15-41),
[18.47](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-47),
[20.02](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)

9). When does the compliance period expire, and what should the s.18
report reply deadline be? [15.20.1](#ref15-20-1)

10). Don't set post -"A"-publication COPS statuses until "A"-
publication is complete.

11). Before issuing an intention to grant letter:

12\) a. is there any possibility of conflict with other applications in
family?

b\. have 3 months elapsed from publication if claims were not in parent?

\[NB: issuing of the intention to grant letter on the divisional cannot
be delayed beyond the compliance date solely to allow for the expiry of
the 3-month period - [15.41](#ref15-41) \]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
