::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 97: Appeals from the comptroller {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 97.01

This section provides for appeals to the court from decisions of the
comptroller. It applies, with certain exceptions ([see
97.03](#ref97-03)), to all such decisions under the 1977 Act and rules.

### 97.02

\[deleted\]

  -----------------------------------------------------------------------
   

  **Section 97(1)**

  Except as provided by subsection (4) below, an appeal shall lie to the
  Patents Court from any decision of the comptroller under this Act or
  rules except any of the following decisions, that is to say -\
  (a) A decision falling within section 14(7) above;\
  (b) a decision under section 16(2) above to omit matter from a
  specification;\
  (c) a decision to give directions under subsection (1) or (2) of
  section 22 above;\
  (d) a decision under rules which is excepted by rules from the right of
  appeal conferred by this section.
  -----------------------------------------------------------------------

### Appeals to Patents Court

### 97.03 {#ref97-03}

r.51 CPR 52.11

Except in the case of proceedings held in Scotland by the comptroller
[see 97.10](#ref97-10), an appeal lies to the Patents Court from any
decision of the comptroller under the Act or rules thereunder except as
follows. The exceptions are decisions under s.14(7) regarding the
abstract; decisions under s.16(2) to omit disparaging or offensive
matter from a specification or from any document made available for
public inspection under s.118; decisions to give directions under
s.22(1) or (2) prohibiting publication of information for security or
safety reasons; and other decisions under rules which are excepted by
rules from the right of appeal, ie under rule 106 remission of fees by
the comptroller, [see
123.16](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-16),
rule 88 (refusal of an application to hold proceedings in Scotland, [see
123.25 to
123.30](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-25)
and rule 100(3) (certain decisions on review of a Patent Office
opinion). An appeal to the Patents Court is limited to a review of the
comptroller's decision, unless the court considers that in the
circumstances of an individual appeal, it would be in the interests of
justice to hold a re-hearing. In the REEF Trade Mark case \[2003\] RPC
5, the Court of Appeal considered the extent that a decision of a
tribunal should be reviewed. The Court confirmed that findings of
primary fact would not be disturbed by a court unless the hearing
officer made an error of principle or was plainly wrong on the evidence,
and held that there was no error of principle simply because a judgment
or decision could have been better expressed. In considering how
reluctant an appellate court should be to interfere with the evaluation
of, and conclusion of the primary facts of the case, there was no single
standard to lay down, but the most important variables included the
nature of the evaluation required, the standing and experience of the
fact-finding judge or tribunal, and the extent to which the judge or
tribunal had to assess oral evidence. Where no oral evidence had been
heard by a tribunal, Robert Walker LJ held that "the appellate court
should show a real reluctance, but not the very highest degree of
reluctance to interfere in the absence of a distinct and material error
of principle". The guidance from this judgment was taken into account in
the appeal to the Patents Court in Hartington Conway Ltd's Patent
Applications \[2004\] RPC 7 heard after Part 63 of the Civil Procedure
Rules came into effect. In finding no error in the approach of the
hearing officer either to the assessment of the witnesses or to the
evidence before him, Pumfrey J held that this was a case where the
highest degree of reluctance should be felt in revisiting the findings
of primary fact. It would also seem necessary following the Court of
Appeal's decision in Merck & Co Inc's Patents \[2004\] FSR 16 that a
ground of appeal that the hearing officer erred "in principle" should
actually identify the principle and not be used simply to mask a
complaint about the assessment of evidence by the hearing officer.
Furthermore, in Dyson Ltd's Trade Mark Application \[2003\] RPC 47,
Patten J held that Art.6 of the European Convention on Human Rights did
not compel the court to conduct a re-hearing in the case of any appeal
from an ex parte decision; the power to order a re-hearing was present
under CPR 52.11(b) and could be exercised in rare cases in order to
allow justice to be done.

### 97.04 {#ref97-04}

The Patents Court confirmed, in Omron Tateisi Electronics Co's
Application \[1981\] RPC 125, that the right to appeal under the Patents
Acts and Rules against decisions of the comptroller is a general one to
which the only exceptions are those in s.97(1), [see 97.03](#ref97-03).
It is therefore applicable to administrative decisions under the Patents
Acts and Rules such as exercise of the comptroller's power under r.110
to grant a certificate that there had been a general interruption in the
postal services. The court used its right under s.99 to exercise any
power which the comptroller could have done [see
99.01](/guidance/manual-of-patent-practice-mopp/section-99-general-powers-of-the-court/#ref99-01)
to itself grant such a certificate.

### 97.04.1 {#ref97-04-1}

There is no appeal from decisions of the Office in its capacity of
receiving office under the PCT. However, such decisions are open to
judicial review (R v Comptroller- General of Patents, ex parte Archibald
Kenrick & Sons Ltd \[1994\] RPC 635, R v Comptroller- General of
Patents, ex parte Drazil \[1992\] RPC 479, R v Comptroller-General of
Patents, ex parte Celltech Ltd \[1991\] RPC 475, R v Comptroller-General
of Patents, ex parte Penife International Ltd \[2004\] RPC 37).

### 97.05 {#section-2}

The procedure for appeals to the Patents Court from decisions of the
comptroller is prescribed by [Rule
63.16](https://www.justice.gov.uk/courts/procedure-rules/civil/rules/part63){rel="external"}
of the Civil Procedure Rules (CPR) which in turn directs that CPR [Part
52](http://www.justice.gov.uk/courts/procedure-rules/civil/rules/part52){rel="external"}
(and its Practice Direction) applies to appeals from decisions of the
comptroller.

\[ See chapter 7 of the [Patent Hearings
Manual](https://www.gov.uk/government/publications/hearings-manual) for
procedure relating to such appeals. \]

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 97(2)**
  For the purpose of hearing appeals under this section the Patents Court may consist of one or more judges of that court in accordance with directions given by the Lord Chief Justice of England and Wales after consulting the Lord Chancellor.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 97.6 {#section-3}

Appeals to the Patents Court under s.97 are heard by one or more of the
Patents Court judges, [see 96.05 and
96.06](/guidance/manual-of-patent-practice-mopp/section-96-the-patents-court-repealed/#ref96-05).

### 97.7 {#section-4}

\[deleted\]

  -----------------------------------------------------------------------
   

  **Section 97(3)**

  An appeal shall not lie to the Court of Appeal from a decision of the
  Patents Court on appeal from a decision of the comptroller under this
  Act or rules -\
  (a) except where the comptroller's decision was given under section 8,
  12, 18, 20, 27, 37, 40, 61, 72, 73 or 75 above; or\
  (b) except where the ground of appeal is that the decision of the
  Patents Court is wrong in law;\
  but an appeal shall only lie to the Court of Appeal under this section
  if leave to appeal is given by the Patents Court or the Court of
  Appeal.
  -----------------------------------------------------------------------

### Appeals to Court of Appeal

### 97.08 {#ref97-08}

An appeal lies to the Court of Appeal from a decision of the Patents
Court on appeal from a decision of the comptroller under the Act or
rules where (a) the comptroller's decision was under any of certain
specified sections of the Act; or (b) the ground of appeal is that the
decision of the Patents Court is wrong in law. The specified sections
are s.8, 12 or 37 (entitlement), s.18 or 20 (whether the application
complies with the Act and rules within the prescribed period), s.27 or
75 (amendment of specification after grant), s.40 (compensation of
employee inventor), s.61 (infringement) or s.72 or 73 (revocation). In
Smith International Inc's Patent \[2006\] FSR 25, the relationship
between s.97(3) and s.55 of the Access to Justice Act 1999 was
considered by the Court of Appeal. Unlike s.97(3), the latter provision
requires permission for a second appeal to be given by the Court of
Appeal and it sets special stringent requirements to grant such
permission. The Court of Appeal held that there had been no express or
implied repeal or amendment of the particular appeal procedure in
s.97(3) for patents by s.55 of the Access to Justice Act 1999, and thus
the Patents Court could grant leave to appeal further.

### 97.09 {#section-5}

The procedure for appeals to the Court of Appeal is prescribed by Part
52 of the Civil Procedure Rules and its Practice Direction.

\[ See chapter 7 of the [Patent Hearings
Manual](https://www.gov.uk/government/publications/hearings-manual)
regarding such appeals. \]

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 97(4)**
  An appeal shall lie to the Court of Session from any decision of the comptroller in proceedings which under rules are held in Scotland, except any decision mentioned in paragraphs (a) to (d) of subsection (1) above.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Appeals from decisions in proceedings in Scotland

### 97.10 {#ref97-10}

r.88 is also relevant

Where there is more than one party to proceedings, a party to those
proceedings may request the comptroller to direct that the hearing or
hearings, if any, in such proceedings should be held in Scotland, [see
123.25 to
123.30](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-25).
Where, as a result, proceedings are held in Scotland, an appeal lies to
the Court of Session from any decision of the comptroller in those
proceedings except any decision mentioned in the second sentence of
[97.03](#ref97-03).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 97(4)**
  The Lord Chief Justice may nominate a judicial office holder (as defined in section 109(4) of the Constitutional Reform Act 2005) to exercise his functions under subsection (2).
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 97.10.1 {#section-6}

A further subsection (4) was added by the Constitutional Reform Act 2005
allowing for the Lord Chief Justice of England and Wales to nominate a
judicial officer holder to exercise their functions in nominating High
Court judges to sit in the Patents Court.

  -----------------------------------------------------------------------
   

  **Section 97(5)**

  An appeal shall not lie to the Inner House of the Court of Session from
  a decision of an Outer House judge on appeal from a decision of the
  comptroller under this Act or rules -\
  (a) except where the comptroller's decision was given under section 8,
  12, 18, 20, 27, 37, 40, 61, 72, 73 or 75 above; or\
  (b) except where the ground of appeal is that the decision of the Outer
  House judge is wrong in law.
  -----------------------------------------------------------------------

### 97.11 {#section-7}

Decisions of the Outer House judge of the Court of Session from
decisions of the comptroller ([see 97.10](#ref97-10)) cannot be appealed
to the Inner House unless the comptroller's decision was under any of
certain specified sections of the Act (the same as those mentioned in
[97.08](#ref97-08)) or the ground of appeal is that the decision of the
Outer House judge is wrong in law.

\[ See chapter 8 of the [Patent Hearings
Manual](https://www.gov.uk/government/publications/hearings-manual)
regarding appeals in Scotland.\]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
