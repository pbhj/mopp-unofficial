::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 100: Burden of proof in certain cases {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (100.01 - 100.06) last updated October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 100.01

s.60(1)(c) is also relevant

This section relates to the burden of proof with regard to whether a
product was obtained by a patented process. It is relevant to
infringement proceedings since a person may infringe a patent for a
process if they dispose of, offer to dispose of, use or import any
product obtained directly by means of that process or keeps any such
product whether for disposal or otherwise.

### 100.02

It applies to 1977 Act patents including European patents (UK).

### 100.03

s.130(7) is also relevant

Section 100 is so framed as to have, as nearly as practicable, the same
effects as the corresponding provisions of the EPC, CPC and PCT. It
corresponds to CPC Article 75 (renumbered as Article 35 \[1989\]), [see
100.06](#ref100-06).

### Section 100(1)

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 100(1)**
  If the invention for which a patent is granted is a process for obtaining a new product, the same product produced by a person other than the proprietor of the patent or a licensee of his shall, unless the contrary is proved, be taken in any proceedings to have been obtained by that process.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 100.04 {#ref100-04}

This section is concerned with patents where the invention is a process
for obtaining a "new product". If the "same product" is produced by a
person other than the patentee or their licensee, it is taken in any
proceedings to have been obtained by that process unless the contrary is
proved. The onus is thus on an alleged infringer to prove otherwise, ie,
that the product was not obtained by that process and therefore does not
infringe the patent. (This assumes that the product is not also claimed
independently of the process; if it is, this section will not normally
be relevant.) This moves the burden of proof from the patentee or
licensee who, without s.100(1), would normally have had to show that the
product constituted an infringement.

### 100.04.1 {#ref100-04-1}

In [Magnesium Elektron Ltd v Molycorp Chemicals & Oxides (Europe) Ltd &
Anor \[2015\] EWHC
3596](http://www.bailii.org/ew/cases/EWHC/Patents/2015/3596.html){rel="external"},
the claimants sought permission to serve proceedings for patent
infringement against a defendant based in China. In order to gain
permission from the court, the claimant had to show there was a serious
issue to be tried. The claims related to a process for preparing
zirconium-cerium-based mixed oxides. The claimant relied on s.100 to
argue that the defendant's product was obtained directly by the patented
process and so there was a serious issue to be tried. The claimant said
that experimental results indicated that products made by the patented
process have a particular fingerprint which is unique to the patented
process, and that the alleged infringing product had that fingerprint.
Birss J held that the word "new" in s.100 must mean the same thing as in
s.1(1)(a), that is, the "new product" must be novel. The judge also
found there is nothing in s.100 which requires that the product which
must be a new product is a thing defined in the same level of generality
as the words used in the process claim. Whilst the patent acknowledged
that zirconium-cerium-based mixed oxides as such are not novel, the
court found that the fingerprint could nevertheless make the product new
for the purposes of s.100 - the fact that the patent did not assert that
products made that way have a unique fingerprint did not matter. The
court therefore gave permission to serve proceedings.

### Section 100(2)

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 100(2)**
  In considering whether a party has discharged the burden imposed upon him by this section, the court shall not require him to disclose any manufacturing or commercial secrets if it appears to the court that it would be unreasonable to do so.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 100.05 {#section-3}

Subsection (2) concerns the disclosure of manufacturing or commercial
secrets by an alleged infringer in attempting to prove that a product
was not obtained by the patented process. They are not required to
disclose any such secrets if the court considers that it would be
unreasonable to require them to do so. Disclosure of a secret would
apparently not be required if it was possible to discharge the burden of
proof without it.

### 100.06 {#ref100-06}

The wording of s.100(2) differs substantially from that of the
corresponding part of CPC Article 75 (renumbered as Article 35 \[1989\])
which reads:

2.In the adduction of proof to the contrary, the legitimate interests of
the defendant in protecting his manufacturing and business secrets shall
be taken into account.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
