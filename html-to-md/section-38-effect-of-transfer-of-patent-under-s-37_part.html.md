::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 38: Effect of transfer of patent under s.37 {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (38.01 - 38.10) last updated: April 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 38.01

Orders that a patent (ie a patent granted under the Act or a granted
European patent (UK)) should be transferred from its existing
proprietor(s) to specified proprietor(s) may be made under s.37. The
effects of such orders on licences or other rights granted or created by
the existing proprietor(s) are laid down by s.38. This section also
gives certain rights to existing proprietor(s) or licensee(s) who worked
the invention, or prepared to do so, before the registration of a
reference under s.37, see [38.05 to 38.07](#ref38-05).

### 38.02

The provisions of s.38 in relation to a patent correspond to those of
s.11 in relation to an application for a patent.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 38(1)**
  Where an order is made under section 37 above that a patent shall be transferred from any person or persons (the old proprietor or proprietors) to one or more persons (whether or not including an old proprietor), then, except in a case falling within subsection (2) below, any licences or other rights granted or created by the old proprietor or proprietors shall, subject to section 33 above and to the provisions of the order, continue in force and be treated as granted by the person or persons to whom the patent is ordered to be transferred (the new proprietor or proprietors).
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Where an existing proprietor is retained

### 38.03

This subsection applies where the effect of an order under s.37 is that
the proprietor(s) to whom the patent is transferred include one or more
of the existing proprietor(s). Licences or other rights continue in
force as if granted by the new proprietor(s), unless the order provides
otherwise and subject to s.33 (which concerns the effects of
transactions, instruments or events, and registration thereof, on rights
in patents).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 38(2)**
  Where an order is so made that a patent shall be transferred from the old proprietor or proprietors to one or more persons none of whom was an old proprietor (on the ground that the patent was granted to a person not entitled to be granted the patent), any licences or other rights in or under the patent shall, subject to the provisions of the order and subsection (3) below, lapse on the registration of that person or those persons as the new proprietor or proprietors of the patent.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Where no existing proprietor is retained

### 38.04

Subsection (2) applies instead of subsection (1) where the effect of an
order under s.37 is that the proprietor(s) to whom the patent is
transferred include none of the existing proprietor(s). Licences or
other rights lapse on registration of the new proprietor(s), unless the
order provides otherwise (but [see 38.05](#ref38-05)).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 38(3)**
  Where an order is so made that a patent shall be transferred as mentioned in subsection (2) above or that a person other than an old proprietor may make a new application for a patent and before the reference of the question under that section resulting in the making of any such order is registered, the old proprietor or proprietors or a licensee of the patent, acting in good faith, worked the invention in question in the United Kingdom or made effective and serious preparations to do so, the old proprietor or proprietors or the licensee shall, on making a request to the new proprietor or proprietors or, as the case may be, the new applicant within the prescribed period, be entitled to be granted a licence (but not an exclusive licence) to continue working or, as the case may be, to work the invention, so far as it is the subject of the new application.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 38.05 {#ref38-05}

Subsection (3) concerns the situation where an order is made under s.37
transferring the patent to new proprietor(s) none of whom was an
existing proprietor or allowing a person other than an existing
proprietor to make a new application ([see 37.15 to
37.18](/guidance/manual-of-patent-practice-mopp/section-37-determination-of-right-to-patent-after-grant/#ref37-15)).
The Office notifies all existing proprietors and their licensees of whom
it is aware of the making of the order. In that situation, an existing
proprietor or licensee who before registration of the relevant s.37
reference ([see
37.08](/guidance/manual-of-patent-practice-mopp/section-37-determination-of-right-to-patent-after-grant/#ref37-08))
acting in good faith worked the invention in the UK or made effective
and serious preparations to do so, is entitled to a nonexclusive licence
to work or continue working the invention. (For the meanings of
"exclusive licence" and "non-exclusive licence", see s.130(1).)

### 38.06 {#section-4}

r.90(2) r.108(1) is also relevant.

In order to obtain such a licence, a request for its grant should be
made, in the case of an existing proprietor, within two months of the
date of the order mentioned in s.38(2). This period is extensible at the
discretion of the comptroller.

### 38.07 {#ref38-07}

The period and terms of the licence to be granted should comply with
s.38(4), ie, be "reasonable". Any dispute regarding such period or terms
or whether the person requesting the licence is entitled to one may be
referred to the comptroller under s.38(5) by either party, [see 38.08 to
38.10](#ref38-08).

  ------------------------------------------------------------------------------------
   
  **Section 38(4)**
  Any such licence shall be granted for a reasonable period and on reasonable terms.
  ------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 38(5)**
  The new proprietor or proprietors of the patent or, as the case may be, the new applicant or any person claiming that he is entitled to be granted any such licence may refer to the comptroller the question whether that person is so entitled and whether any such period is or terms are reasonable, and the comptroller shall determine the question and may, if he considers it appropriate, order the grant of such a licence.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 38.08 {#ref38-08}

PR part 7 is also relevant.

The dispute ([see 38.07](#ref38-07)) should be referred by filing
Patents Form 2 accompanied by a copy thereof and a statement of grounds
in duplicate. This starts proceedings before the comptroller, the
procedure for which is discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).
In accordance with r.76(4)(c) the statement should set out the period or
the terms of the licence which the claimant is prepared to accept or
grant.

### 38.09 {#section-5}

r.77 is also relevant.

The Office sends a copy of the reference and statement to the new
proprietor(s) or, as the case may be, the new applicant, and each person
claiming to be entitled to a licence, other than the referrer. If any
recipient does not agree to grant or accept a licence as specified they
should file a counter-statement in the proceedings.

### 38.10 {#section-6}

s.108 is also relevant.

When the comptroller determines the question to which the s.38(5)
reference relates they may, if appropriate, order the grant of such a
licence ([see 38.05](#ref38-05)). Without prejudice to any other method
of enforcement, such an order has effect as if it were a deed, executed
by the proprietor of the patent and all other necessary parties,
granting a licence in accordance with the order.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
