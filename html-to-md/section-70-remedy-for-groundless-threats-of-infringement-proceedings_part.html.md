::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 70: Threats of infringement proceedings {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (70.01 - 70.08) last updated: September 2017.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Unjustified threats

Section 70 of the Patents Act 1977 was replaced on 1 October 2017 by the
Intellectual Property (Unjustified Threats) Act 2017 with new sections
70-70F. Sections 70-70F apply to alleged threats made from 1 October
2017 onwards. Any alleged threats made before 1 October 2017 are subject
to the law as it was before the changes made by the 2017 Act.

### Section 70A: Threats of infringement proceedings

### 70.01

This section sets out the test for determining whether a communication
contains a threat of infringement proceedings.

### 70.02

For the applicability of sections 70-70F in relation to European
patents, \[see
60.02\](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60.

Section 70(1) A communication contains a "threat of infringement
proceedings" if a reasonable person in the position of a recipient would
understand from the communication that- (a) a patent exists, and (b) a
person intends to bring proceedings (whether in a court in the United
Kingdom or elsewhere) against another person for infringement of the
patent by--- (i) an act done in the United Kingdom, or (ii) an act
which, if done, would be done in the United Kingdom.

### 70.03 {#ref70-03}

Section 70 defines what amounts to a "threat of infringement
proceedings". In applying the two-part test, the contents of the
communication are considered from the perspective of a reasonable person
in the position of the recipient of a communication. The first part is
whether the communication would be understood by a reasonable person in
the position of a recipient to mean that a patent (or application for a
patent) exists. The second part is whether the communication would be
understood by such a person to mean that someone intends to bring
infringement proceedings in respect of that patent (or application for a
patent) for an act done (or which would be done) in the UK. If both
parts of the test are satisfied, the communication contains a threat of
infringement proceedings. It is worth noting that the second part of the
test specifically covers an act which, if done, would be done in the UK;
this provides for threats made in relation to acts which have not taken
place yet. Threats can be in any form, for example written, oral,
implied or express. The intention of the person making the threat is not
important; it is only important whether a communication would be
considered to contain a threat from the point of view of a reasonable
person in the position of the recipient.

### 70.04 {#section-2}

In FH Brundle v Perry \[2014\] EWHC 475 (IPEC) (a case decided under the
previous threats provisions) the defendant, Mr Perry, submitted that the
letter containing the alleged threat was addressed to the "Chief
Executive/Chairman" of Brundle, and went on to claim that the CEO or
Chairman of such a company would not take such letters seriously. The
IPEC judge held that: "If a reasonable person in the shoes of a CEO or
Chairman would understand the words of a communication as containing a
threat of infringement proceedings, the extent to which he or she goes
on to treat the threat seriously is irrelevant. The threat has still
been made."

### Section 70(2)

References in this section and in [section
70C](/guidance/manual-of-patent-practice-mopp/section-70c-remedies-and-defences)
to a "recipient" include, in the case of a communication directed to the
public or a section of the public, references to a person to whom the
communication is directed.

### 70.05 {#section-3}

Threats need not be made directly to an identified individual; a threat
can be made more generally. However, it must be more than a general
warning. Section 70(2) ensures that the threats provisions are engaged
when a threat is made in a mass communication. It provides that, in such
cases, the understanding of the reasonable person will be that of a
recipient who is a member of the public, or a member of the section of
the public to which the communication was directed.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
