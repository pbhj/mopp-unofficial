::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 85: European patent attorneys \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (85.01 - 85.02) last updated October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 85.01

This section provided that a person on the European list (ie the list of
professional representatives maintained by the EPO under the EPC) could
describe themselves as a European patent attorney, and could prepare
documents (other than deeds) for use in proceedings before the
comptroller under the Act in relation to a European patent or
application therefor, without being guilty of an offence under certain
enactments.

### 85.02

Section 85, together with other provisions of the 1977 Act concerning
patent agents, has been repealed by the CDP Act and been replaced by
Part V of the CDP Act (Patent Agents and Trade Mark Agents), in which
s.277 relates particularly to European patent attorneys.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
