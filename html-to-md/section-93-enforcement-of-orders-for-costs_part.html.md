::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 93: Enforcement of orders for costs {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (93-01 - 93-02) last updated October 2013.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------
   

  **Section 93**

  If the European Patent Office orders the payment of costs in any
  proceedings before it\
  (a) in England and Wales the costs shall, if the county court so
  orders, be recoverable by execution issued from the county court or
  otherwise as if they were payable under an order of that court;\
  (b) in Scotland the order may be enforced in like manner as an extract
  registered decree arbitral bearing a warrant for execution issued by
  the sheriff court of any sheriffdom in Scotland;\
  (c) in Northern Ireland the order may be enforced as if it were a money
  judgment;\
  (d) in the Isle of Man the order may be enforced in like manner as an
  execution issued out of the court.
  -----------------------------------------------------------------------

### 93.01

This section provides for the enforcement in the UK of orders for costs
made by the EPO. The methods of enforcement are the same as those for an
award of costs by the comptroller under s.107, [see
107.09](/guidance/manual-of-patent-practice-mopp/section-107-costs-and-expenses-in-proceedings-before-the-comptroller/#ref107-09).

### 93.01.1

In paragraph (b) which applies to Scotland only, the words "a recorded
decree arbitral" were replaced by "an extract registered decree arbitral
bearing a warrant for execution issued by the sheriff court of any
sheriffdom in Scotland" by the Debtors (Scotland) Act 1987, s.108(1),
Sch 6, para 20.

### 93.02

Part (d) is was added by The Patents Act (Isle of Man) Order 1978( SI
1978 No 621). This Order has since been revoked and replaced by The
Patents Act (Isle of Man) Order 2003 (SI 2003 No 1249).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
