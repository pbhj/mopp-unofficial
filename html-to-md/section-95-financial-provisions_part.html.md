::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 95: Financial provisions {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (95.01) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 95: Financial provisions {#section-95-financial-provisions}

This section has been affected by the use of powers under the Government
Trading Funds Act 1973 (as amended) enabling the Patent Office to be
operated as a trading fund (The Patent Office Trading Fund Order, SI
1991/1796). The effect of the provisions of the Government Trading Funds
Act 1973 is to allow the establishment of new financing arrangements,
which supersede any existing statutory provision for vote funding.

### Section 95(1)

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 95(1)**
  There shall be paid out of moneys provided by Parliament any sums required by any Minister of the Crown or government department to meet any financial obligation of the United Kingdom under the European Patent Convention or the Patent Co-operation Treaty.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Section 95(2)

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 95(2)**
  Any sums received by any Minister of the Crown or government department in pursuance of that convention or that treaty shall be paid into the Consolidated Fund.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
