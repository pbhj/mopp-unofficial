::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Regulation (EC) No 1610/96 of the European Parliament and of the council (Plant Protection Products) {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (SPP0.01 - SPP21.01) last updated: October 2022.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Recitals to the regulation

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  The European Parliament and The Council of The European Union
  Having regard to the Treaty establishing the European Community, and in particular Article 100a thereof,
  Having regard to the proposal from the Commission (OJ No C 390, 31.12.1994, p 21 and OJ No C 335, 13.12.1995, p 15)
  Having regard to the opinion of the Economic and Social Committee (OJ No C 155, 21.6.1995, p 14),
  Acting in accordance with the procedure referred to in Article 189b of the Treaty (Opinion of the European Parliament of 15 June 1995 (OJ No C 166, 3.7.1995, p 89), common position of the Council of 27 November 1995 (OJ No C 353, 30.12.1995, p 36) and decision of the European Parliament of 12 March 1996 (OJ No C 96, 1.4.1996, p 30)),
  \(1\) Whereas research into plant protection products contributes to the continuing improvement in the production and procurement of plentiful food of good quality at affordable prices;
  \(2\) Whereas plant protection research contributes to the continuing improvement in crop production;
  \(3\) Whereas plant protection products, especially those that are the result of long, costly research, will continue to be developed in the Community and in Europe if they are covered by favourable rules that provide for sufficient protection to encourage such research;
  \(4\) Whereas the competitiveness of the plant protection sector, by the very nature of the industry, requires a level of protection for innovation which is equivalent to that granted to medicinal products by Council Regulation (EEC) No 1768/92 of 18 June 1992 concerning the creation of a supplementary protection certificate for medicinal products (OJ No L 182, 2.7.1992, p. 1.);
  \(5\) Whereas, at the moment, the period that elapses between the filing of an application for a patent for a new plant protection product and authorization to place the said plant protection product on the market makes the period of effective protection under the patent insufficient to cover the investment put into the research and to generate the resources needed to maintain a high level of research;
  \(6\) Whereas this situation leads to a lack of protection which penalises plant protection research and the competitiveness of the sector;
  \(7\) Whereas one of the main objectives of the supplementary protection certificate is to place European industry on the same competitive footing as its North American and Japanese counterparts;
  \(8\) Whereas, in its Resolution of 1 February 1993 (OJ No C 138, 17.5.1993, p. 1.) on a Community programme of policy and action in relation to the environment and sustainable development, the Council adopted the general approach and strategy of the programme presented by the Commission, which stressed the interdependence of economic growth and environmental quality; whereas improving protection of the environment means maintaining the economic competitiveness of industry; whereas, accordingly, the issue of a supplementary protection certificate can be regarded as a positive measure in favour of environmental protection;
  \(9\) Whereas a uniform solution at Community level should be provided for, thereby preventing the heterogeneous development of national laws leading to further disparities which would be likely to hinder the free movement of plant protection products within the Community and thus directly affect the functioning of the internal market; whereas this is in accordance with the principle of subsidiarity as defined by Article 3b of the Treaty;
  \(10\) Whereas, therefore, there is a need to create a supplementary protection certificate granted, under the same conditions, by each of the Member States at the request of the holder of a national or European patent relating to a plant protection product for which marketing authorization has been granted is necessary; whereas a Regulation is therefore the most appropriate legal instrument;
  \(11\) Whereas the duration of the protection granted by the certificate should be such as to provide adequate, effective protection; whereas, for this purpose, the holder of both a patent and a certificate should be able to enjoy an overall maximum of fifteen years of exclusivity from the time the plant protection product in question first obtains authorization to be placed on the market in the Community;
  \(12\) Whereas all the interests at stake in a sector as complex and sensitive as plant protection must nevertheless be taken into account; whereas, for this purpose, the certificate cannot be granted for a period exceeding five years;
  \(13\) Whereas the certificate confers the same rights as those conferred by the basic patent; whereas, consequently, where the basic patent covers an active substance and its various derivatives (salts and esters), the certificate confers the same protection;
  \(14\) Whereas the issue of a certificate for a product consisting of an active substance does not prejudice the issue of other certificates for derivatives (salts and esters) of the substance, provided that the derivatives are the subject of patents specifically covering them;
  \(15\) Whereas a fair balance should also be struck with regard to the determination of the transitional arrangements; whereas such arrangements should enable the Community plant protection industry to catch up to some extent with its main competitors, while making sure that the arrangements do not compromise the achievement of other legitimate objectives concerning the agricultural policy and environment protection policy pursued at both national and Community level;
  \(16\) Whereas only action at Community level will enable the objective, which consists in ensuring adequate protection for innovation in the field of plant protection, while guaranteeing the proper functioning of the internal market for plant protection products, to be attained effectively;
  \(17\) Whereas the detailed rules in recitals 12, 13 and 14 and in Articles 3(2), 4, 8(1)(c) and 17 (2) of this Regulation are also valid, mutatis mutandis, for the interpretation in particular of recital 9 and Articles 3, 4, 8(1)(c) and 17 of Council Regulation (EEC) No 1768/92
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Have adopted this regulation

### SPP 0.01 {#spp-001}

Under Article 22 of the Medicinal Regulation, all references to the
original Regulation 1768/92 are to be construed as references to
Regulation 469/2009 (see
[SP0.01](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSP0-01),
[SPM0.04](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM0-04)
and Annex II to Regulation 469/2009). Annex II provides a correlation
table for the provisions of the two Regulations. Articles 3(1)(a) and
(c), 5, 6, 8(1)(a)(i) to (iv), 8(2), 9 to 12, 13(1) and (2), 14(a) to
(c), 15, 16, 17(1) and 18(2) of the Plant Protection Regulation are
identical in wording to Articles 3(a) to (c), 5, 6, 8(1)(a)(i) to (iv),
8(4), 9 to 12, 13(1) and (2), 14(a) to (c), 15, 17, 18 and 19(2)
respectively, of the Medicinal Regulation. For commentary on the said
Articles in the Plant Protection Regulation, reference should be made to
the corresponding paragraphs in [SPM3.01 to
19.11](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-01).
For commentary on the Recitals and the remaining Articles of the Plant
Protection Regulation, the following paragraphs should be consulted as
well as the equivalent paragraphs in [SPM 0.01 to
22.01](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM0-01).

### SPP0.02 {#refSPP0-02}

As with the Medicinal Regulation, the changes made to the assimilated EU
law apply to new applications filed after 1 January 2021, as well as
SPCs already granted before that date. Transitional arrangements apply
to applications for SPCs filed before, but still pending on, 1 January
2021, where the amendments made by SI 2020/1471 do not apply whilst the
application remains pending. Texts of the amended Regulation (in
consolidated and marked-up forms) and the transitional version of the
Regulation can be found at
<https://www.gov.uk/guidance/spcs-and-the-northern-ireland-protocol#more-information>
(see
[SP0.13.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSP0-13-1)).

  -----------------------------------------------------------------------
   

  **Article 1:Definitions**

  For the purposes of this Regulation, the following definitions shall
  apply:\
  1. "plant protection products": active substances and preparations
  containing one or more active substances, put up in the form in which
  they are supplied to the user, intended to:\
  (a) protect plants or plant products against all harmful organisms or
  prevent the action of such organisms, in so far as such substances or
  preparations are not otherwise defined below;\
  (b) influence the life processes of plants, other than as a nutrient
  (e.g. plant growth regulators);\
  (c) preserve plant products, in so far as such substances or products
  are not subject to special provisions on preservatives;\
  (d) destroy undesirable plants; or\
  (e) destroy parts of plants, check or prevent undesirable growth of
  plants;\
  2. "substances": chemical elements and their compounds, as they occur
  naturally or by manufacture, including any impurity inevitably
  resulting from the manufacturing process;\
  3. "active substances": substances or micro-organisms including
  viruses, having general or specific action:\
  (a) against harmful organisms; or\
  (b) on plants, parts of plants or plant products;\
  4. "preparations": mixtures or solutions composed of two more
  substances, of which at least one is an active substance, intended for
  use as plant protection products;\
  5. "plants": live plants and live parts of plants, including fresh
  fruit and seeds;\
  6. "plant products": products in the unprocessed state or having
  undergone only simple preparation such as milling, drying or pressing,
  derived from plants, but excluding plants themselves as defined in
  point 5;\
  7. "harmful organisms": pests of plants or plant products belonging to
  the animal or plant kingdom, and also viruses, bacteria and mycoplasmas
  and other pathogens;\
  8. "product": the active substance as defined in point 3 or combination
  of active substances of a plant protection product;\
  9. "basic patent": a patent which protects a product as defined in
  point 8 as such, a preparation as defined in point 4, a process to
  obtain a product or an application of a product, and which is
  designated by its holder for the purpose of the procedure for grant of
  a certificate;\
  10. "certificate": the supplementary protection certificate.\
  11. 'comptroller' means the Comptroller-General of Patents, Designs and
  Trade Marks;\
  12. 'court' means-\
  (i) as respects England and Wales, the High Court;\
  (ii) as respects Scotland, the Court of Session; and\
  (iii) as respects Northern Ireland, the High Court in Northern
  Ireland.\
  13. 'EEA authorization' means an authorization to place a plant
  protection product on the market which has effect in an EEA state in
  accordance with Regulation (EC) No 1107/2009;\
  14. 'patent' means a patent which has effect in the United Kingdom;\
  15. "GB authorisation" means an authorisation, to place a plant
  protection product on the market in England and Wales and Scotland,
  granted or having effect as if granted under Regulation (EC)
  1107/2009;\
  16. "NI authorisation" means an authorisation, to place a plant
  protection product on the market in Northern Ireland, granted or having
  effect as if granted in accordance with Regulation (EC) 1107/2009 as it
  has effect by virtue of the Protocol on Ireland/Northern Ireland in the
  EU withdrawal agreement;\
  17. "prescribed" means prescribed by rules under section 123 of the
  Patents Act 1977.
  -----------------------------------------------------------------------

### Product and Plant Protection Product

### SPP 1.01 {#refSPP1-01}

Article 1 distinguishes between the terms "plant protection product" and
"product". Although the EC Plant Protection Regulation creates a
certificate for plant protection products, it is the product - defined
by Article 1(8) as the active substance or combination of active
substances - which is the subject of the certificate pursuant to Article
2. The meaning of "product" was clarified by the European Court of
Justice in [BASF AG v Bureau Voor de Industriële Eigendom \[2002\] RPC
9(PDF, 80.7
KB)](http://rpc.oxfordjournals.org/content/119/7/274.full.pdf){rel="external"}.
It was held that a "product" as defined by Article 1(8) includes, along
with the active substance, any impurity which inevitably results from
the manufacturing process of that substance. However, two products which
differ only in the proportion of active substance to impurity they
contain (one being more pure than the other) are considered to be one
and same "product" within the definition of Article 1(8) - the court
holding that a product "cannot change solely because of an alteration in
the unit quantity of impurities where both the chemical compound it
contains and that compound's action on its targets remain unchanged".
The fact that, in the case in question, the two products of differing
purity required separate marketing authorisations was not relevant in
determining whether they amounted to the same "product" within the
definition of Article 1. See also [SPP3.01](#refSPP3-01). Clarification
of the meaning of the term "product" has also been sought in Bayer
CropScience AG v Bundespatentgericht, C-11/13, it was determined that
"product" is to be interpreted as covering a "safener", a substance
added to plant protection product to eliminate or reduce phytotoxic
effects of the protection product on certain plants.

### SPP 1.02 {#spp-102}

The definitions of the terms "plant protection products" to "product" in
Article 1(1) to 1(8) are based on those specified in Article 2 of
Council Directive 91/414/EEC of 15 July 1991 concerning the placing of
plant protection products on the market, now repealed by Regulation (EC)
No 1107/2009 with effect from 14 June 2011. However, these definitions
do not always correspond to the terminology used in UK Marketing
Authorizations, or the details published in official Gazettes. Thus, the
product specified in the Marketing Authorization is generally broadly
equivalent to the "plant protection product" as defined in Article 1(1),
and the "active constituent(s)" or "active ingredient(s)" are generally
broadly equivalent to the "product" as defined by Article 1(8).

### SPP 1.03 {#refSPP1-03}

The term "active substance" in the Plant Protection Regulation is
identical in effect to "active ingredient" in the Medicinal Regulation.
In accordance with recital (13) the term "active substance" in Article
1(8) is interpreted as including any closely related derivative, in
particular a salt or ester, which has obtained authorization to be
placed on the market and is protected by the claims of the basic patent.

### SPP 1.04 {#spp-104}

If the derivative in question can be regarded as a new and inventive
active substance which is protected by a patent which specifically
relates to it, e.g. a selection patent, recital (14) allows for the new
derivative to be protected by its own certificate in spite of the fact
that the non-derivatised form of the active substance and its
non-inventive derivatives are the subject of a different certificate.

### GB and NI Authorisations

### SPP 1.05 {#refSPP1-05}

As introduced in the Supplementary Protection Certificates (Amendment)
(EU Exit) Regulations 2020 (see
[SP0.13.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSP0-13-1)),
Article 1(15) and 1(16) defines:

a "GB authorisation" as an authorisation placing a product on the market
in England and Wales and Scotland in accordance with Regulation (EC)
1107/2009; and

an "NI authorisation" as an authorisation placing a product on the
market in Northern Ireland granted in accordance with Regulation (EC)
1107/2009 as it has effect by virtue of the Northern Ireland Protocol in
the Withdrawal Agreement (see [SP0.15 - SP015.2](#refSP0-15)).

### SPP 1.05.1 {#refSPP1-05-1}

In relation to "GB authorisation", the reference to Regulation 1107/2009
is to the version incorporated into UK domestic law under section 3 of
the European Union (Withdrawal) Act 2018, whilst the reference in
relation to "NI authorisation" is to the EU legislation which applies
via the Protocol. Other references to Regulation 1107/2009 in the Plant
Protection Regulation are to be construed accordingly.

### Basic patent

### SPP 1.06 {#refSPP1-06}

The basic patent may be either a UK patent or a European patent (UK)
(see
[SPM1.05](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM1-05)).

  -----------------------------------------------------------------------
   

  **Article 2: Scope**

  A plant protection product may, under the terms and conditions provided
  for in this Regulation, be the subject of a certificate if it is-\
  (a) protected by a patent; and\
  (b) the subject of a GB or NI authorization prior to being placed on
  the market as a plant protection product.
  -----------------------------------------------------------------------

### SPP 2.01 {#refSPP2-01}

A certificate can be granted in the UK for a product which has received
an authorisation to be placed on the market in accordance with
Regulation 1107/2009 as it applies either in Great Britain or Northern
Ireland, as applicable. GB and NI authorisations are granted by the
Health and Safety Executive (HSE) under the appropriate legislation. In
the case of Great Britain, Regulation 1107/2009 designates separate
competent authorities for the granting of authorisations in England,
Wales and Scotland, as environment and agriculture policy are devolved
competencies. However, the Welsh and Scottish Ministers have consented
to HSE performing that function on their behalf by way of agency
agreements, meaning that the authorisation granted by the HSE has effect
in all three territories. A similar arrangement is provided for the HSE
to grant NI authorisations on behalf of the Northern Ireland Executive.
As a result of this series of delegations and agreements, the
authorisations granted by HSE may be documented in a single record,
granted under both sets of legislation. Marketing authorisations are
preferably identified by the MAPP number, a unique product registration
number allocated upon issue of the first commercial authorisation for a
plant protection product, (see also [SPP8.04](#refSPP8-04)).

### SPP 2.02 {#refSPP2-02}

In Erber Aktiengesellschaft BL O/732/21, an approval granted under
Regulation (EC) 1831/2003 relating to the use of animal feeds was deemed
to be outside the requirements of Article 2 as the product concerned had
not been the subject of an approval for use as a plant protection
product under Regulation (EC) 1107/2009. As a result, the hearing
officer refused the SPC application relying on this approval (see also
[SPM
2.01.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM2-01-1)).

  -----------------------------------------------------------------------
   

  **Article 3: Conditions for obtaining a certificate**

  1\. Where an application is submitted under Article 7, a certificate
  shall be granted if, at the date of submission of the application:\
  (a) the product is protected by a basic patent in force;\
  (b) there is a valid GB or NI authorization to place the product on the
  market;\
  (c) the product has not already been the subject of a certificate;\
  (d) the authorization referred to in (b) is the first authorization to
  place the product on the market as a plant protection product in the
  territory of England and Wales and Scotland or the territory of
  Northern Ireland as the case may be.\
  2. The holder of more than one patent for the same product shall not be
  granted more than one certificate for that product. However, where two
  or more applications concerning the same product and emanating from two
  or more holders of different patents are pending, one certificate for
  this product may be issued to each of these holders.
  -----------------------------------------------------------------------

### SPP 3.01 {#refSPP3-01}

The conditions of Article 3 must be satisfied at the date of making an
application. Thus, at that date:

-   the basic patent protecting the product must be in force;
-   the product must not previously have been the subject of a
    certificate in the UK;
-   a valid UK or NI authorization must have been granted (see
    [SPP2.01](#refSPP2-01)):
-   this authorization must be the first authorization to place the
    product on the market as a plant protection product in Great Britain
    or Northern Ireland as applicable (although there may have been an
    earlier first authorization in the EEA).

For example, in BASF AG v Bureau Voor de Industriële Eigendom \[2002\]
RPC 9 ([see SPP1.01](#refSPP1-01)), the product in question was a plant
protection product manufactured according to a patented process, which
resulted in a product of higher purity than previously achieved. A
marketing authorisation for this higher purity product was granted in
1987, but marketing authorisation for a lower purity product was granted
in 1967. The ECJ held that the higher purity product amounted to the
same "product" within the meaning of Article 3 as the lower purity
product and so held that in such circumstances the conditions of Article
3(1)(d) were not satisfied. Clarification of the meaning of the term
"product" has also been sought in Bayer CropScience AG v
Bundespatentgericht C-11/13, where it was determined that "product" is
to be interpreted as covering a "safener", a substance added to plant
protection product to eliminate or reduce phytotoxic effects of the
protection product on certain plants.

### SPP 3.02 {#refSPP3-02}

Under the Plant Protection Regulation, there are three different ways a
product can be approved:

-   an experimental permit for supply, storage and use only, for a
    limited period, under Article 54 (this is a derogation from the
    requirement for an authorisation);
-   provisional approval for sale, supply, storage, use and
    advertisement for a limited period whilst outstanding data are
    obtained, under Article 30;
-   full (unlimited) approval for sale, supply, storage, use and
    advertisement, where there are no outstanding data requirements,
    under Article 28.

Experimental permits only allow experimental work on new substances or
new uses to be carried out over a limited area to produce data in
support of a future application for approval for commercial use. Such
permits are not acceptable as a basis for making an SPC application
under Article 2 or 3(1)(b) as they do not allow marketing to take place.
(see also
[SPM3.03.1](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM3-03-1))
Provisional and full authorizations both allow marketing of the product
from the date of grant thereof. In Hogan Lovells International (C
229/09), the Court of Justice of the European Union confirmed that
Article 3(1)(b) of the Plant Protection Regulation is interpreted as
meaning that a supplementary protection certificate can be issued for a
product in respect of which a provisional MA has been granted under
Article 8(1) of Directive 91/414 (now Article 30 of Regulation
1107/2009).

### SPP 3.02.1 {#refSPP3-02-1}

In Sumitomo Chemical (C-210/12), questions relating to whether or not
emergency marketing authorisations for plant protection products (under
Art 8(4) of Directive 91/414/EEC, now Article 53 of Regulation
1107/2009) can be used for the purposes of Article 3(1)(b), and whether
such authorisations still need to be in force at the time of an
application, were referred to the CJEU for a preliminary ruling. The
Court confirmed that Article 3(1)(b) should be interpreted as precluding
the issue of a SPC for a plant protection product in respect of which an
emergency MA has been issued under Article 8(4) of Directive 91/414/EEC,
and that Articles 3(1)(b) and 7(1) of the Regulation must be interpreted
as precluding an application for a SPC being lodged before the date on
which the plant protection product has obtained the MA referred to in
Article 3(1)(b) of that Regulation [see SPP7.01](#refSPP7-01).

### SPP 3.02.2 {#refSPP3-02-2}

Having regard to Regulation (EC) 1107/2009 as it applies in Northern
Ireland, the following steps are relevant as concerns the grant of an
Article 3(1)(b)-compliant MA. First, an application to seek approval of
the active substance for inclusion in plant protection products in the
EU is made under Article 4 of Regulation (EC) 1107/2009. This results in
a Commission implementing decision including the substance on the list
of approved substances provided by Regulation (EU) 540/2011. This step
does not allow a plant protection product containing the active
substance to be placed on the market, and so is not Article 3(1)(b)
compliant. Where a plant protection product is for use on an edible
crop, it is then necessary to set a maximum residue level in the
resultant food or feed, as provided by Articles 6 and 7 of Regulation EC
396/2005. Finally, Regulation (EC) 1107/2009 as it applies in Northern
Ireland stipulates that, before any plant protection product can be
placed on the market, it must be authorised according to the zonal
authorisation process provided by Articles 33-39 or the mutual
recognition process provided by Articles 40-42; these authorisations are
capable of being considered compliant with Article 3(1)(b).

### SPP 3.03 {#spp-303}

Article 3(2) precludes the granting of more than one certificate for a
single product to the holder of a portfolio of related patents all
covering the said product, even when the SPC applications are all
pending together and meet the requirements of Article 3(1)(c). When the
product is protected by several patents held by an applicant -- for
example, by a patent for the product per se, a patent for a process for
making the product and a patent for a plant protection formulation
comprising the product -- it is for the holder of the patents concerned
to chose one of them as the basic patent, bearing in mind that the
subject-matter protected by the certificate is constrained by the
protection conferred by the patent. The hearing officer in Takeda
Chemical Industries Ltd's Applications \[2004\] RPC 2 found that only
one certificate for a product should be granted to the same applicant,
having also considered the European Court of Justice decision in Biogen
Inc. v Smithkline Beecham Biologicals SA C-181/95; \[1997\] RPC 23.

### SPP 3.04 {#refSPP3-04}

As with the Medicinal Regulation, a transitional form of Article 3
applies to SPC applications pending as of 1 January 2021. In particular,
the wording of Article 3(1) states:

1.  Where an application is submitted under Article 7, a certificate
    shall be granted if at the date of submission of the application-\
    (a) the product is protected by a basic patent in force;\
    (b) there is a valid UK authorization to place the product on the
    market;\
    (c) the product has not already been the subject of a certificate;\
    (d) the authorization referred to in (b) is the first authorization
    to place the product on the market as a plant protection product.

Under this transitional form of the Article, the authorisation to place
the product on the market must be a "UK authorisation" - that is, one
granted under Regulation 1107/2009 for the UK as a whole -- and it must
be the first to place the product on the market in the UK. This
principle follows through to the rest of the Regulation in its
transitional form. If such an authorisation was replaced by a GB and NI
authorisation at the end of the transition period, regulation 7(7) of
the Supplementary Protection Certificates (EU Exit) Regulations 2020
applies, meaning that the combination of the two authorisations is
treated as equivalent to a UK authorisation when considering the pending
application under the transitional form of the Article.

\[As with pending applications under the Medicinal Regulation, the
examiner on the case may request details of the replacement
authorisations for recording on the Register.\]

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Article 4: Subject-matter of protection**
  Within the limits of the protection conferred by the basic patent, the protection conferred by a certificate shall extend only to the product covered by the GB or NI authorisation or both GB and NI authorisations to place the corresponding plant protection product on the market and for any use of the product as a plant protection product that has been authorized in the United Kingdom before the expiry of the certificate.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### SPP 4.01 {#spp-401}

A certificate extends the protection conferred by the basic patent
beyond the term of that patent but only in respect of the product
covered by the authorization or approval to place the corresponding
plant protection product on the market and any use of the product as a
plant protection product that has been authorized or approved before
expiry of the certificate. It does not, however, extend the term of the
patent itself.

  -----------------------------------------------------------------------
   

  **Article 5: Effects of the certificate**

  1\. Subject to Article 4 and paragraphs 2 and 3, the certificate shall
  confer the same rights as conferred by the basic patent and shall be
  subject to the same limitations and the same obligations.\
  2. The protection conferred by a certificate in accordance with
  paragraph 1 shall extend only to the territory in respect of which a
  valid GB or NI authorisation has been issued and the authorisation-\
  (a) is the first authorisation for the product in the territory in
  accordance with Article 3(1)(b) and (d), and\
  (b) has been issued before the certificate takes effect in accordance
  with Article 13(1).\
  3. Where after the submission of an application for a certificate in
  accordance with Article 7 and before the certificate takes effect in
  accordance with Article 13(1), a GB or NI authorisation is granted in
  respect of the same product and the authorisation would have met the
  requirements of Article 3(b) and (d) had it been granted on the date of
  submission of the application, the protection conferred by a
  certificate in accordance with paragraph 1 shall extend to the
  territory of England and Wales and Scotland or the territory of
  Northern Ireland as the case may be.
  -----------------------------------------------------------------------

### SPP5.01 {#refSPP5-01}

Paragraphs 2 and 3 were inserted by the Supplementary Protection
Certificates (Amendment) (EU Exit) Regulations 2020. As with the
Medicinal Regulation, they establish that a certificate will only confer
protection within the territory in which relevant marketing
authorisations allow the product to be placed on the market. These
authorisations must meet the requirements of Article 3(1)(b) and (d),
and must have been issued before the certificate takes effect (see
[SPM5.05-5.05.3](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM5-05)).

### SPP5.02 {#refSPP5-02}

Mirroring Article 5(1b) of the Medicinal Regulation, Article 5(3) allows
for the protection provided by a certificate to extend to additional
territory if a marketing authorisation is granted in that territory
after the application for the certificate has been filed.

### SPP5.03 {#refSPP5-03}

As with the Medicinal Regulation, it is not possible to extend the
territorial scope (by notification of additional authorisations) once a
certificate takes effect. If an authorisation is withdrawn, the effect
on the territorial scope of the certificate will depend on the
protection conferred by the certificate, as per the provisions on lapse
(see [SPP14.03](#refSPP14-03)).

  -----------------------------------------------------------------------------------------------
   
  **Article 6: Entitlement to the certificate**
  The certificate shall be granted to the holder of the basic patent or his successor in title.
  -----------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Article 7: Application for a certificate**

  1\. The application for a certificate shall be lodged within six months
  of the date on which the GB or NI authorization referred to in Article
  3(1)(b) and (d) to place the product on the market as a plant
  protection product was granted. Where more than one such authorisation
  is granted before the application for a certificate is lodged, the
  application shall be lodged within six months of the date of grant of
  the earliest of such authorisations.\
  2. Notwithstanding paragraph 1, where the authorization to place the
  product on the market is granted before the basic patent is granted,
  the application for a certificate shall be lodged within six months of
  the date on which the patent is granted.
  -----------------------------------------------------------------------

### SPP 7.01 {#refSPP7-01}

The requirements to be met with respect to the period for filing an
application under the Plant Protection Regulation are identical to those
under the Medicinal Regulation ([see SPM7.01 to
SPM7.03](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM7-01)).
In Sumitomo C-210/12, the CJEU confirmed that Article 7(1) is to be
interpreted as precluding an application for a SPC being lodged before
the date on which the plant protection product has obtained the MA
referred to in Article 3(1)(b) ([see SPP3.02.1](#refSPP3-02-1)).

  -----------------------------------------------------------------------
   

  **Article 8: Content of the application for a certificate**

  1\. The application for a certificate shall contain:\
  (a) a request for the grant of a certificate, stating in particular:\
  (i) the name and address of the applicant;\
  (ii) the name and address of the representative, if any;\
  (iii) the number of the basic patent and the title of the invention;\
  (iv) the number and date of the GB or NI authorisation or both GB and
  NI authorisations as referred to in Article 3(1)(b) and (d); and\
  (v) the number and date of the earliest EEA authorization, the granting
  of which predates the granting of the GB or NI authorization as
  referred to in Article 3(1)(b) and (d);\
  (b) a copy of the GB or NI authorisation or both GB and NI
  authorisations to place the product on the market, as referred to in
  Article 3(1)(b) and (d), in which the product is identified, containing
  in particular the number and date of the authorization and the summary
  of the product characteristics listed in Commission Regulation
  283/2013, Part A section 1, points 1.1 to 1.7 or Part B, Section 1
  points 1.1 to 1.4.3;\
  (c) where the product is the subject of one or more EEA authorizations
  granted prior to the GB or NI authorization referred to in Article
  3(1)(b) and (d), the applicant must provide in relation to the earliest
  of any such EEA authorizations-\
  (i) information regarding the identity of the product thus authorised;\
  (ii) information regarding the legal provision under which the
  authorization procedure took place; and\
  (iii) a copy of the notice publishing the authorization in the
  appropriate official publication or, failing such a notice, any other
  document proving that the authorization has been issued, the date on
  which it was issued and the identity of the product authorized.
  -----------------------------------------------------------------------

### SPP 8.01 {#spp-801}

The request for grant of a certificate must specify:

-   the name and address of the applicant (Section 3 of Form SP1);
-   the name of the applicant's agent (if any) and address for service
    in the UK, Gibraltar or Channel Islands (Section 4);
-   the EC Regulation (469/2009, formally 1768/92; or 1610/96) under
    which the application is made Section 5);
-   the product in respect of which the certificate is sought (i.e. the
    active substance or combination of active substances of the plant
    protection product) (Section 6);
-   the number, title, expiry date and (if later than the first UK
    authorization or approval) the date of grant of the basic patent
    (Section 7);
-   the number and date of the first GB or NI authorization or approval
    (Section 8a);
-   the number and date of any other GB or NI authorisation which has
    been granted ahead of the application being filed (Section 8b)
-   (where granted before the first GB or NI authorization) the State,
    number and date of the first authorization in the EEA, plus the
    identity of the authorized product and the legal provision under
    which the authorization took place (Section 9).

### SPP 8.02 {#refSPP8-02}

A new SPC application must be based on the earliest GB or NI marketing
authorisation for the product that has been granted by the time the
application is submitted. The application must also include the number
and date of each GB or NI authorisation granted before the application
is submitted (as applicable) as well as a copy of each authorisation.
These may take the form of a single document issued by HSE comprising
both authorisation types (see [SPP2.01](#refSPP2-01)). At any time after
an original application is submitted but before grant of the SPC
application, Form SP6 should be used to submit details of any
newly-granted relevant authorisation that covers additional territory
(GB or NI), rather than submitting a new Form SP1 or seeking a
correction to the original form.

### SPP 8.03 {#refSPP8-03}

If the application for a certificate is based on a marketing
authorisation granted before 1 January 2021 (see
[SPP3.04](#refSPP3-04)), the applicant must provide the details of that
authorisation. However, if the pre-2021 marketing authorisation has been
changed as a result of the Northern Ireland Protocol, the application
must contain details of each authorisation that relates to the same
product (whether GB or NI). Therefore, if a pre-2021 authorisation has
become a combination of GB and NI authorisations, the application for a
certificate must include details of both authorisations, so that it
properly reflects the regulatory basis for the SPC.

### SPP8.04 {#refSPP8-04}

Marketing authorisations are preferably identified by their MAPP number
at item 8 of form SP1. The MAPP number is a unique product registration
number allocated upon issue of the first commercial authorisation for a
plant protection product, as the MAPP number by definition relates to
the first authorisation it is preferred to the authorisation number,
(see also [SPP2.01](#refSPP2-01)).

  -----------------------------------------------------------------------
   

  **Article 9: Lodging of an application for a certificate**

  1\. An application for a certificate shall be lodged with the
  comptroller.\
  2. Notification of the application for a certificate shall be published
  by the comptroller. The notification shall contain at least the
  following information:\
  (a) the name and address of the applicant;\
  (b) the number of the basic patent;\
  (c) the title of the invention;\
  (d) the number and date of the GB or NI authorisation or both a GB and
  a NI authorisation provided under Article 8(1)(b), the product
  identified in the authorisation and the territory in respect of which
  the authorisation has been granted or has effect as if granted;\
  (e) where there are EEA authorizations granted before any GB or NI
  authorisation provided under Article 8(1)(b), the number and date of
  the earliest EEA authorization.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Article 10: Grant of the certificate or rejection of the
  application**

  1\. Where the application for a certificate and the product to which it
  relates meet the conditions laid down in this Regulation, the
  comptroller shall grant the certificate.\
  2. The comptroller shall, subject to paragraph 3, reject the
  application for a certificate if the application or the product to
  which it relates does not meet the conditions laid down in this
  Regulation or any prescribed fee is not paid.\
  3. Where the application for a certificate does not meet the conditions
  laid down in Article 8 or the prescribed fee relating to the
  application has not been paid, the comptroller shall ask the applicant
  to rectify the irregularity, or to settle the fee, within a stated
  time.\
  4. If the irregularity is not rectified or the fee is not settled under
  paragraph 3 within the stated time, the application shall be rejected.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Article 11: Publication**

  1\. Notification of the fact that a certificate has been granted shall
  be published by the comptroller. The notification shall contain at
  least the following information:\
  (a) the name and address of the holder of the certificate;\
  (b) the number of the basic patent;\
  (c) the title of the invention;\
  (d) the number and date of the UK, GB or NI authorisation or, where
  there is more than one such authorisation, of each authorisation
  provided under Article 8(1)(b) or Article 13A(1), the product
  identified in the authorisation and the territory in respect of which
  the authorisation has been granted or has effect as if granted;\
  (e) where there are EEA authorizations granted before any authorisation
  provided under Article 8(1)(b), the number and date of the earliest EEA
  authorization;\
  (f) the duration of the certificate.\
  2. Notification of the fact that the application for a certificate has
  been rejected shall be published by the comptroller. The notification
  shall contain at least the information listed in Article 9(2).
  -----------------------------------------------------------------------

  -----------------------------
   
  **Article 12: Annual fees**
  \[Omitted\]
  -----------------------------

  -----------------------------------------------------------------------
   

  **Article13: Duration of the certificate**

  1\. The certificate shall take effect at the end of the lawful term of
  the basic patent for a period equal to the period which elapsed between
  the date on which the application for a basic patent was lodged and the
  date of the first authorization to place the product on the market in
  the area comprising the European Economic Area and the United Kingdom,
  reduced by a period of five years.\
  2. Notwithstanding paragraph 1, the duration of the certificate may not
  exceed five years from the date on which it takes effect.\
  3. For the purposes of calculating the duration of the certificate,
  account shall be taken of a provisional first marketing authorization
  only if it is directly followed by a definitive authorization
  concerning the same product.
  -----------------------------------------------------------------------

### Provisional first authorization

### SPP 13.01 {#spp-1301}

During the substantive examination of the application, the applicant
will generally be requested to provide details of any full
authorizations which have followed an acknowledged first provisional
authorization so that the provisions of Article 13(3) can be given
effect. However, it is considered that Article 13(3) does not prevent
the grant of a certificate on the basis of provisional authorizations
which allow marketing of the product see [SPP3.02](SPP3-02).

  -----------------------------------------------------------------------
   

  **Article 13A: Authorisation granted after submission of an application
  for a certificate**

  1\. Where after the submission of an application under Article 7(1),
  but before the grant of a certificate under Article 10(1) in relation
  to a GB authorisation, a valid NI authorisation is granted which, at
  its date of grant, is the first authorisation to place the product on
  the market as a plant protection product in the territory of Northern
  Ireland, the applicant shall notify the comptroller of the grant of the
  NI authorisation, within six months of its date of grant and before the
  certificate takes effect under Article 13(1), and provide the details
  set out in Article 8(1)(a)(iv) and (b) on the prescribed form.\
  2. Where after the submission of an application under Article 7(1), but
  before the grant of a certificate under Article 10(1) in relation to a
  NI authorisation, a valid GB authorisation is granted which, at its
  date of grant, is the first authorisation to place the product on the
  market as a plant protection product in the territory of England and
  Wales and Scotland, the applicant shall notify the comptroller of the
  grant of the GB authorisation, within six months of its date of grant
  and before the certificate takes effect under Article 13(1), and
  provide the details set out in Article 8(1)(a)(iv) and (b) on the
  prescribed form.\
  3. Where after the grant of a certificate under Article 10(1) in
  relation to a GB authorisation, but before expiry of the basic patent,
  a valid NI authorisation is granted which, at its date of grant, is the
  first authorisation to place the product on the market as a plant
  protection product in the territory of Northern Ireland, the
  certificate holder shall notify the comptroller of the NI
  authorisation, within six months of its date of grant and before the
  certificate takes effect under Article 13(1), and provide the details
  set out in Article 8(1)(a)(iv) and (b) on the prescribed form.\
  4. Where after the grant of a certificate under Article 10(1) in
  relation to a NI authorisation, but before expiry of the basic patent,
  a valid GB authorisation is granted, which at its date of grant, is the
  first authorisation to place the product on the market as a plant
  protection product in the territory of England and Wales and Scotland,
  the certificate holder shall notify the comptroller of the grant of the
  GB authorisation, within six months of its date of grant and before the
  certificate takes effect under Article 13(1), and provide the details
  set out in Article 8(1)(a)(iv) and (b) on the prescribed form.\
  5. If the applicant or certificate holder fails to notify the
  comptroller of the grant of an authorisation in accordance with any of
  paragraphs 1 to 4, the protection conferred by a certificate granted
  under Article 10(1) shall not extend to any additional territory
  covered by that authorisation.\
  6. On receipt of a notification under any of paragraphs 1 to 4, the
  comptroller shall publish:\
  (a) the number and date of the authorisation,\
  (b) the product identified in that authorisation, and\
  (c) the relevant territory in respect of which the authorisation has
  been granted or has effect as if granted.
  -----------------------------------------------------------------------

### SPP13A.01 {#refSPP13A-01}

Article 13A was introduced by the [Supplementary Protection Certificates
(Amendment) (EU Exit) Regulations
2020](https://www.legislation.gov.uk/uksi/2020/1471/contents/made){rel="external"}
(SI 2020/1471). The Article sets out how an additional authorisation
granted after an SPC application has been filed must be notified,
depending on whether a) the original application was filed based on a GB
or NI authorisation and b) whether the application is still pending or
has been granted. The process and requirements are the same in each
circumstance and mirror those for medicinal products (see
[SPM13A.02-04](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM13A-02)).

  -----------------------------------------------------------------------
   

  **Article 14: expiry of certificate**

  1\. The certificate shall lapse:\
  (a) at the end of the period provided for in Article 13;\
  (b) if the certificate-holder surrenders it;\
  (c) if the prescribed annual fee is not paid in time; or\
  (d)if and as long as the product covered by the certificate may no
  longer be placed on the market following the withdrawal of all
  authorizations to place it on the market in accordance with Article 28
  of Regulation 1107/2009. The comptroller may decide on the lapse of the
  certificate either of the comptroller's own motion or at the request of
  a third party.\
  2. Where a UK authorisation is withdrawn and replaced simultaneously
  with a GB authorisation and a NI authorisation, the certificate granted
  in respect of the UK authorisation shall not lapse.\
  3. Where a UK, GB or NI authorisation is withdrawn, but one or more
  such authorisations remain valid, the protection conferred by the
  certificate shall, as from the date of withdrawal, no longer extend to
  the territory covered by the authorisation withdrawn but shall continue
  in respect of the territory covered by any remaining authorisation.\
  4. For the purposes of paragraphs 2 and 3, "UK authorisation" means an
  authorisation to place a plant protection product on the market in the
  United Kingdom, granted or having effect as if granted, prior to IP
  completion day, under Regulation (EC) 1107/2009 of the European
  Parliament and of the Council of 21 October 2009 concerning the placing
  of plant protection products on the market.
  -----------------------------------------------------------------------

### SPP 14.01 {#spp-1401}

As amended by the Supplementary Protection Certificates (Amendment) (EU
Exit) Regulations 2020, Article 14 additionally relates to UK
authorisations, as defined in paragraph 4. This captures authorisations
granted under the EU regulatory regime before the end of the transition
period, on which certificates applied for and/or granted before that
point may still depend.

### Declaration of lapse under Article 14(1)(d)

### SPP14.02 {#spp1402}

Article 14(1)(d) of the Plant Protection Regulation is equivalent to
Article 14(1)(d) of the Medicinal Regulation but refers, naturally, to
Regulation 1107/2009 (see
[SPM14.02-08](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM14-02)).

### SPP14.03 {#refSPP14-03}

As with the Medicinal Regulation, the certificate will only lapse under
14(1)(d) if all applicable authorisations are withdrawn. If only one
such authorisation is withdrawn, and there are others still in effect
covering other territories, the certificate will remain in force but its
protection will only extend to the territory where an authorisation
remains in effect.

  -----------------------------------------------------------------------
   

  **Article 15: Invalidity of the Certificate**

  1\. The certificate shall be invalid if:\
  (a) it was granted contrary to the provisions of Article 3;\
  (b) the basic patent has lapsed before its lawful term expires;\
  (c) the basic patent is revoked or limited to the extent that the
  product for which the certificate was granted would no longer be
  protected by the claims of the basic patent or, after the basic patent
  has expired, grounds for revocation exist which would have justified
  such revocation or limitation.\
  2. Any person may submit an application or bring an action for a
  declaration of invalidity of the certificate before the comptroller or
  the court.
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Article 16: Notification of lapse or invalidity**
  If the certificate lapses in accordance with Article 14(1)(b), (c) or (d) or is invalid in accordance with Article 15, or if the territorial extent of the certificate is limited in accordance with Article 14(3), notification thereof shall be published by the comptroller.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Article 17: Appeals**

  1\. \[Omitted\].\
  2. The decision to grant the certificate shall be open to an appeal
  aimed at rectifying the duration of the certificate where the date of
  the first authorization to place the product on the market in the
  Community, contained in the application for a certificate as provided
  for in Article 8, is incorrect.
  -----------------------------------------------------------------------

### SPP 17.01 {#refSPP17-01}

As with the Medicinal Regulation, the omission of Article 17(1) does not
affect the application of Section 97 of the Patents Act to decisions of
the comptroller.

### SPP 17.02 {#refSPP17-02}

Article 17(2) makes it possible to seek a correction of the duration of
a certificate after grant, if it can be shown that the date of the first
authorization in the EEA provided in an application -- and thus the
calculation in Article 13 -- is wrong (see also [SPM13.06 and
18.03](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM13-06)).

  -----------------------------------------------------------------------
   

  **Article 18: Procedure**

  1\. In the absence of procedural provisions in this Regulation, the
  procedural provisions applicable to the corresponding basic patent (as
  modified by section 128B of, and Schedule 4A to, the Patents Act 1977)
  shall apply to the certificate.\
  2. Notwithstanding paragraph 1, the procedure for opposition to the
  granting of a certificate shall be excluded.
  -----------------------------------------------------------------------

### SPP 18.01 {#spp-1801}

The procedural and fees provisions laid down in the 2007 Rules and 2007
Fees Rules apply to certificates for plant protection products as they
do to medicinal products [See
SPM19.01-19.03](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refSPM19-01).

  -----------------------------------------
   
  **Article 19: Transitional Provisions**
  \[Omitted\]
  -----------------------------------------

  --------------------------------------------------------------------------
   
  **Article 19A: Provisions relating to the enlargement of the community**
  \[Omitted\]
  --------------------------------------------------------------------------

  ----------------
   
  **Article 20**
  \[Omitted\]
  ----------------

  ------------------------------------------------------------------------------------------------------------------------------
   
  **Article 21: Entry into force**
  This Regulation shall enter into force six months after its publication in the Official Journal of the European Communities.
  ------------------------------------------------------------------------------------------------------------------------------

### SPP 21.01 {#spp-2101}

The Plant Protection Regulation was published in the Official Journal of
the European Communities on 8 August 1996. It therefore entered into
force on 8 February 1997.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
