::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 125: Extent of invention {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (125.01 - 125.27) last updated: April 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 125.01 {#ref125-01}

s.130(7) is also relevant

Section 125 is intended to have, as nearly as practicable, the same
effect as the corresponding provisions of the EPC, PCT and CPC.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 125(1)**
  For the purposes of this Act an invention for a patent for which an application has been made or for which a patent has been granted shall, unless the context otherwise requires, be taken to be that specified in a claim of the specification of the application or patent, as the case may be, as interpreted by the description and any drawings contained in that specification, and the extent of the protection conferred by a patent or application for a patent shall be determined accordingly
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 125.02 {#ref125-02}

This subsection sets out the meaning of an invention as that specified
in a claim as interpreted by the description and any drawings, the
protection conferred being determined accordingly. (The words "for a
patent" in s.125(1) have apparently been inadvertently transposed and
should follow the word "application" (first appearance), so then the
sub-section should read: "For the purposes of this Act an invention for
which an application for a patent has been made or ..... etc"). It thus
concerns the way in which the specification should be construed in this
respect but does not impose any requirement with which the applicant
must comply. Objection therefore should not be raised by the examiner
under s.125(1) as such, although its provision may sometimes usefully be
referred to when objecting on other grounds, eg under s.14(5). For
example when a claim read on its own appears to be clear and to define
the invention, such an objection under s.14(5) may arise if there is
matter in the description which is inconsistent with the claim or in
some other way casts doubt on the true scope of the invention, [see
14.129](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-129),
[see 14.114 to
14.146](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-114).

### 125.03 {#section}

\[deleted\]

### 125.04 {#section-1}

\[moved to [125.17.1](#ref125-17-1)\]

### 125.05 {#section-2}

\[moved to [125.17.2](#ref125-17-2)\]

### 125.06 {#ref125-06}

S.123(3) and s.130(7) is also relevant

\[deleted\]

### Biotechnological inventions

### 125.07 {#section-3}

\[moved to [125.27](#ref125-27)\]

### Section 125(2)

It is hereby declared for the avoidance of doubt that where more than
one invention is specified in any such claim, each invention may have a
different priority date under section 5 above.

### 125.08 {#ref125-08}

If a claim is construed to specify more than one invention, the priority
date of each invention should be determined separately. Note, partial
priority may arise (see the chapter on section 5, particularly [5.20 to
5.25.3](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-20).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 125(3)**
  The Protocol on the Interpretation of Article 69 of the European Patent Convention (which Article contains a provision corresponding to subsection (1) above) shall, as for the time being in force, apply for the purposes of subsection (1) above as it applies for the purposes of that Article.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### The Protocol on the Interpretation of Article 69 of the EPC

### 125.09 {#ref125-09}

Article 69(1) of the EPC corresponds to s.125(1) of the Act and reads:-\
\
The extent of the protection conferred by a European patent or a
European patent application shall be determined by the terms of the
claims. Nevertheless, the description and drawings shall be used to
interpret the claims.

### 125.10 {#ref125-10}

Both Article 69 of the EPC and s.125(1) of the Act should be construed
in the light of the Protocol on the Interpretation of Article 69 of the
EPC, which reads:

> Article 1\
> General principles\
> Article 69 should not be interpreted as meaning that the extent of the
> protection conferred by a European patent is to be understood as that
> defined by the strict, literal meaning of the wording used in the
> claims, the description and drawings being employed only for the
> purpose of resolving an ambiguity found in the claims. Nor should it
> be taken to mean that the claims serve only as a guideline and that
> the actual protection conferred may extend to what, from a
> consideration of the description and drawings by a person skilled in
> the art, the patent proprietor has contemplated. On the contrary, it
> is to be interpreted as defining a position between these extremes
> which combines a fair protection for the patent proprietor with a
> reasonable degree of legal certainty for third parties.\
> \
> Article 2\
> Equivalents\
> For the purpose of determining the extent of protection conferred by a
> European patent, due account shall be taken of any element which is
> equivalent to an element specified in the claims.

### 125.11 {#section-4}

\[deleted\]

### 125.12 {#section-5}

\[deleted\]

### General approach to construction

### 125.13 {#ref125-13}

The reference in the first sentence of the Protocol to "resolving an
ambiguity" rejects a literal approach to the construction of patent
claims, where words were given their "natural and ordinary meaning" of
words, ignoring their context or background unless they were ambiguous.
The courts even prior to the Protocol were instead favouring a purposive
construction of claims, by giving effect to what would have been
understood by the notional addressee. In Catnic Components Ltd and
another v Hill and Smith Ltd \[1982\] RPC 183, Lord Diplock stated (at
page 243):- "A patent specification should be given a purposive
construction rather than a purely literal one derived from applying to
it the kind of meticulous verbal analysis in which lawyers are too often
tempted by their training to indulge".

### 125.14 {#ref125-14}

The skilled reader is taken to suppose that the patentee knew some
patent law -- that their claim is for the purpose of defining the
monopoly and that it should be for something new, as held by the Court
of Appeal in [Virgin Atlantic Airways Ltd v Premium Aircraft Interiors
UK Ltd \[2010\] RPC
8](https://academic.oup.com/rpc/article/127/3/192/1610421){rel="external"}
(and discussed in [Saab Seaeye v Atlas Elektronik \[2017\] EWCA Civ
2175](https://www.bailii.org/ew/cases/EWCA/Civ/2017/2175.html){rel="external"}
at \[17\]-\[18\]). Knowledge of that may well affect how the claim is
construed. For instance, the patentee would not be expected to have
claimed what they had expressly acknowledged was old. However, the
Supreme Court in [Warner-Lambert Company LLC v Generics (UK) Ltd (t.a.
Mylan) & Anor. \[2018\] UKSC
56](http://www.bailii.org/uk/cases/UKSC/2018/56.html){rel="external"}
rejected the argument that, if the meaning of a term is ambiguous, the
principle of validating construction (a concept derived from contract
law) should be applied to give it a construction that results in a valid
claim. Instead, claims should be construed purposively in light of the
description, and validating construction should not be applied to give
claims a meaning which preserves their validity, if this is not the
meaning that the skilled person would read into the claim.

### 125.15 {#section-6}

\[deleted\]

### Relationship between the Protocol and the Catnic principle

### 125.16 {#section-7}

\[deleted\]

### Equivalents as a guide to construction and the Protocol questions

### 125.17 {#section-8}

\[deleted\]

### 125.17.1 {#ref125-17-1}

In approaching the construction of the claims the specification as a
whole should be read to obtain the necessary background, and in some
cases the meaning of the words used in the claims may be affected or
defined by what is said in the body of the specification. In Palmaz's
European Patents (UK) (\[1999\] RPC 47, upheld on appeal \[2000\] RPC
631) Pumfrey J held that words of degree such as "thin" must take their
meaning from the context. Different meanings could not be attributed
either to exclude acknowledged prior art or to be consistent with
representations made by the patentee's patent attorneys in a letter to a
patent office. Another example is Glatt's Application \[1983\] RPC 122
at page 129, where Whitford J considered that if a particular claim were
looked at alone, it would not be assumed that a certain fabric was one
which was in itself necessarily and inherently air-permeable. However,
against the description of the invention in question "on its true
construction the claims of the specification could properly only be read
as being limited to an article suitable for conditioning fabrics
comprising a flexible woven or non-woven air-permeable web".

### 125.17.2 {#ref125-17-2}

In Hoechst Celanese Corp v BP Chemicals \[1999\] FSR 319 the Court of
Appeal held that there was no rebuttable presumption that words in a
patent specification which could have a technical meaning did have that
meaning. The court was entitled to hear evidence on the meaning but
thereafter had to decide on the meaning from the context in which they
were used. See also 14.111 with regard to words used in a claim which
are given a special meaning by the description. Caution should be used
in considering the purpose or advantage of the invention. In Union
Carbide Corp. v BP Chemicals Ltd \[1999\] RPC 409, it was held that
where a stated advantage of the invention was that certain equipment was
not required, the fact that a person used that equipment did not mean
that they were not using the invention as they may have decided to use
it badly.

### The Protocol and the UK Supreme Court Judgment in Actavis v Eli Lilly

### 125.17.3 {#ref125-17-3}

The requirements of the Protocol to give fair protection for the
patentee and a reasonable degree of certainty for third parties were
considered by the Supreme Court in Actavis UK Limited and others v Eli
Lilly and Company \[2017\] UKSC 48.

### 125.17.4 {#section-9}

Lord Neuberger noted that he did not consider that the last part of the
first sentence of Article 1 only enables the description (i.e. the
specification) and the drawings to be taken into account when
interpreting the claims, in cases where the claims would otherwise be
ambiguous. He also noted that it is apparent from Article 2 that there
is at least potentially a difference between interpreting a claim and
the extent of the protection afforded by a claim and, when considering
the extent of such protection, equivalents must be taken into account.

### 125.17.5 {#section-10}

He went on to say that notwithstanding what Lord Diplock said in Catnic
Components Ltd and another v Hill and Smith Ltd \[1982\] RPC 183, a
problem of infringement is best approached by addressing two issues,
each of which is to be considered through the eyes of the notional
addressee of the patent in suit, i.e. the person skilled in the relevant
art. Those issues are:

\(i\) does the variant infringe any of the claims as a matter of normal
interpretation; and, if not, (ii) does the variant nonetheless infringe
because it varies from the invention in a way or ways which is or are
immaterial?

If the answer to either issue is "yes", there is an infringement;
otherwise, there is not. Such an approach complies with Article 2 of the
Protocol, as issue (ii) squarely raises the principle of equivalents,
but limits its ambit to those variants which contain immaterial
variations from the invention. Issue (i) self-evidently raises a
question of interpretation, whereas issue (ii) raises a question which
would normally have to be answered by reference to the facts and expert
evidence.

### 125.17.6 {#ref125-17-6}

It was further held in Actavis that to conflate these two issues as a
single question of interpretation, as had been done by Lord Hoffman in
Kirin-Amgen v Hoescht Marion Roussel Ltd \[2005\] RPC 9, following his
approach in Improver Corporation v Remington Consumer Products Ltd
\[1990\] FSR 181 (which itself had followed Lord Diplock's analysis in
Catnic) is wrong in principle, and can lead to error. Instead, issue
(ii) involves not merely identifying what the words of a claim would
mean in their context to the notional addressee, but also considering
the extent if any to which the scope of protection afforded by the claim
should extend beyond that meaning. With regard to issue (i), in
[Icescape Ltd v Ice-World International BV & Ors \[2018\] EWCA Civ
2219](http://www.bailii.org/ew/cases/EWCA/Civ/2018/2219.html){rel="external"},
Kitchin LJ said "I have no doubt that...issue (i) involves purposive
interpretation". Therefore when considering infringement using issue
(i), purposive interpretation must still be used.

### 125.17.7 {#ref125-17-7}

In Generics (U.K.) Limited and others v Yeda Research and Development
Company Limited and others \[2017\] EWHC 2629 (Pat), Arnold J held that
the doctrine of equivalents does not apply to novelty. Additionally, in
Actavis Group PTC EHF v ICOS Corporation & Ors \[2017\] EWCA Civ 1671
the court said that there is nothing in Actavis v Eli Lilly to change
the approach to the construction of the claims, i.e. what the skilled
person would have understood the patentee to mean. This adds weight to
the view that the new approach to determining scope of protection set
out by the Supreme Court in Actavis v Eli Lilly is relevant to
determining questions of infringement only, and not to other matters
which depend purely on claim construction. In Regen Lab SA v Estar
Medical Ltd & Ors \[2019\] EWHC 63 (Pat) Judge Hacon considered the
application of the doctrine of equivalence to numerical claims and found
that numerical claims should be treated no differently to any other
claim. He commented that "it is possible to conclude that as a matter of
normal construction a numerical limit cannot be stretched to cover the
accused product or process, but that the variant has a numerical value
sufficiently equivalent to that defined in the claim such that the
variant falls within its scope". However, these comments on infringement
were obiter dicta since they did not affect the outcome (invalidity),
and therefore are not binding precedent.

### 125.17.8 {#ref125-17-8}

In [Technetix B.V and others v Teleste Ltd \[2019\] EWHC 126
(IPEC)](http://www.bailii.org/ew/cases/EWHC/IPEC/2019/126.html){rel="external"}
the issue of the so-called "Formstein" defence was considered. This is a
principle developed under German patent law in relation to infringement
and the doctrine of equivalents (DoE), whereby if an alleged infringer
can show that a notionally-infringed equivalent of a patent lacks
novelty or is an obvious variant of the claimed invention, then the
patent's scope for the purposes of determining any infringement is held
to its normal construction -- in other words the DoE does not apply. On
the facts of this case, there was held to be no infringement because the
patent was found to be invalid but HHJ Hacon suggested that an
equivalent to the Formstein defence may be required in the future in
order to preserve the established principle that a defence to
infringement is possible if the proposed infringing product lacked
novelty or inventive step over the prior art. In [Facebook Ireland Ltd v
Voxer IP LLC \[2021\] EWHC 1377
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2021/1377.html){rel="external"},
Birss LJ made obiter remarks in relation to the use of the Formstein
defence in the UK. At paragraph 216 he said, ''...if I had to decide the
matter I would hold that the correct approach is the Formstein approach
so that the conclusion if the equivalent device lacks novelty or is
obvious is that the claim scope must be confined to its normal
construction in that respect. I would do so for two reasons. If the
claim on its normal construction is valid, then it seems harsh to
invalidate it on this ground. What else could the patentee do but write
their claim in a way which, normally construed, did not cover the prior
art. So that approach promotes certainty. Secondly, since it is clear
that other EPC countries work that way, this is a reason in itself for
this EPC state to take the same approach.'' In [Vernacare Ltd v Moulded
Fibre Products (MFP) \[2022\] EWHC
2197](https://caselaw.nationalarchives.gov.uk/ewhc/ipec/2022/2197){rel="external"}
(IPEC), Nicholas Caddick QC (sitting as a Deputy High Court Judge),
endorsed the obiter guidance provided by Birss LJ in Facebook Ireland
Ltd v Voxer IP LLC \[2021\] EWHC 1377 (Pat) and allowed MFP to rely on
the Formstein defence. This marked the first time that the defence was
determinative in patent infringement proceedings in the UK.

### 125.17.9 {#ref125-17-9}

In [Akebia Therapeutics Inc v Fibrogen Inc \[2020\] EWHC 866
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2020/866.html){rel="external"},
Arnold LJ commented that a patent cannot be both amended to limit the
claims to a specific embodiment, and yet still have broader protection
to a wider class under the doctrine of equivalents. The judge said that
by disclaiming down to a specific embodiment, the patentee is
"disclaiming the other ways of achieving the same effect disclosed in
the specification, and in particular everything covered by the broader
granted claims".

### The Actavis Questions

### 125.18 {#ref125-18}

In Improver Corporation v Remington Consumer Products Ltd \[1990\] FSR
181, Hoffmann J formulated Lord Diplock's approach in Catnic into three
questions which the court should ask itself. These have subsequently
become known as the 'Improver' questions, and in Wheatley v Drillsafe
Ltd \[2001\] RPC 7 were re-named by the Court of Appeal, the 'Protocol
questions'. In Actavis it was emphasised that these questions are
guidelines, not strict rules that provide helpful assistance in
determining whether a variant infringed. The UKSC in Actavis did however
reformulate the questions as follows and these questions are referred to
below as the Actavis questions:\
(i) Notwithstanding that it is not within the literal meaning of the
relevant claim(s) of the patent, does the variant achieve substantially
the same result in substantially the same way as the invention, i.e. the
inventive concept revealed by the patent?\
(ii) Would it be obvious to the person skilled in the art, reading the
patent at the priority date, but knowing that the variant achieves
substantially the same result as the invention, that it does so in
substantially the same way as the invention?\
(iii) Would such a reader of the patent have concluded that the patentee
nonetheless intended that strict compliance with the literal meaning of
the relevant claim(s) of the patent was an essential requirement of the
invention?

In order to establish infringement in a case where there is no literal
infringement, a patentee would have to establish that the answer to the
first two questions was "yes" and that the answer to the third question
was "no".

### 125.18.1 {#section-11}

The first Actavis question, which asks whether the variant has a
material effect on the way in which the invention works, is a question
which was framed in the context of a mechanical patent, and is not
wholly aptly expressed for every type of case. However, in practice, the
question as framed by Hoffmann J, with its emphasis on how "the
invention" works, should correctly involve the court focussing on the
"the problem underlying the invention". In effect, the question is
whether the variant achieves the same result in substantially the same
way as the invention. If the answer to that question is no, then it
would plainly be inappropriate to conclude that it could infringe. If,
by contrast, the answer is yes, then it provides a sound initial basis
for concluding that the variant may infringe, but the answer should not
be the end of the matter.

### 125.18.2 {#section-12}

The reformulated second question should also apply to variants which
rely on, or are based on, developments which have occurred since the
priority date, even though the notional addressee is treated as
considering the second question as at the priority date. It seems right
in principle to have the same question, including the same assumption
(i.e. that the variant works) for all cases. While the notional
addressee may answer the reformulated second question affirmatively even
where the variant was unforeseeable at the priority date, the Supreme
Court held that they are less likely to do so in such circumstances.

### 125.18.3 {#section-13}

If the variation represents an inventive step, while it may render it
less likely that the patentee will succeed on the second reformulated
question that alone should not necessarily prevent the resultant variant
from infringing the original invention.

### 125.18.4 {#section-14}

The third Actavis question as expressed by Hoffmann J is whether the
notional addressee would have understood from the language of the claim
that the patentee intended that strict compliance with the primary
meaning was an essential requirement of the invention. Although the
language of the claim is important, consideration of the third question
certainly does not exclude the specification of the patent and all the
knowledge and expertise which the notional addressee is assumed to have.
Further, the fact that the language of the claim does not on any
sensible reading cover the variant is certainly not enough to justify
holding that the patentee does not satisfy the third question. Hence,
the fact that the rubber rod in Improver could not possibly be said to
be "an approximation to a helical spring" was not the end of the
infringement issue. When considering the third question, it is
appropriate to ask whether the component at issue is an "essential" part
of the invention, but that is not the same thing as asking if it is an
"essential" part of the overall product or process of which the
inventive concept is part. Hence in Improver the question was whether
the spring would have been regarded by the addressee as essential to the
inventive concept, or inventive core, of the patent in suit. Finally
when one is considering a variant which would have been obvious at the
date of infringement rather than at the priority date, it is necessary
to imbue the notional addressee with rather more information than they
might have had at the priority date.

### 125.18.5 {#section-15}

In Actavis UK Limited and others v Eli Lilly and Company \[2017\] UKSC
48, the Supreme Court found that pemetrexed dipotassium would infringe a
claim to pemetrexed disodium. The skilled person would understand that
"the reason why the claims were limited to the disodium salt was because
that was the only pemetrexed salt on which the experiments described in
the specification had been carried out. However, it does not follow that
the patentee did not intend any other pemetrexed salts to infringe". The
Supreme Court went on to say that this suggestion "confuses the
disclosure of the specification of a patent with the scope of protection
afforded by its claims".

### 125.18.6 {#ref125-18-6}

The above test is for determining whether certain equivalents fall
within the scope of protection of the claims. This test does not mean
that all equivalents are protected by the claims. An inaccurate
statement in the description about equivalents does not affect the
boundaries of the claims, and so any such statement should be ignored.

### 125.18.7 {#ref125-18-7}

In [Illumina Cambridge Ltd v Latvia MGI Tech SIA & Ors \[2021\] EWHC 57
(Pat)](https://www.bailii.org/cgi-bin/format.cgi?doc=/ew/cases/EWHC/Patents/2021/57.html){rel="external"},
Birss J held that language used in the description could act as a basis
for answering the third Actavis question in the affirmative i.e. a
definition provided in the description excluding a variant could be
taken as evidence of the patentee's intention for strict compliance with
the literal meaning of the claim(s). In this case, which dealt with
modified nucleotides for DNA sequencing, Illumina alleged that MGI's
''Cool MPS'' sequencing method infringed their EP 1530578 and EP 1828412
patents. MGI argued that their method could not be considered equivalent
to Illumina's patented method because the phrase ''incorporation of a
nucleotide'' in the Illumina patents required nucleotide and label to be
incorporated in a single step, and this did not cover the two-step
incorporation of a nucleotide and label (via later binding of an
antibody) that characterised their ''Cool MPS'' method. Birss J found
that because the description in EP 1530578 defined ''incorporation of a
nucleotide'' in general terms, the skilled person would not understand
an intention from the patentee to limit the claims to single-step
incorporation of nucleotides only, it followed then that ''Cool MPS''
infringed EP 1530578 by equivalence. With regards to EP 1828412 however,
Birss J found that because the description had a much narrower
definition of ''incorporation of a nucleotide'', one that referred
explicitly to covalent bonding, a two-step method was explicitly
excluded and the skilled person would consider that the patentee did
have an intention for strict compliance with the literal meaning of the
claim(s). Consequently ''Cool MPS'' did not infringe EP 1828412 by
equivalence.

### 125.18.8 {#ref125-18-8}

In [Vernacare Ltd v Moulded Fibre Products (MFP) \[2022\] EWHC 2197
(IPEC)](https://caselaw.nationalarchives.gov.uk/ewhc/ipec/2022/2197){rel="external"},
Nicholas Caddick QC (sitting as a Deputy High Court Judge), allowed MFP
to rely on the Formstein defence (see 125.17.8) and noted that for the
same reasons, in the alternative, that the third Actavis question must
be answered in the affirmative i.e. the skilled person would have
concluded that the patentee intended a strict compliance with the
literal meaning of claim 1, because otherwise claim 1 would be invalid
for obviousness.

### 125.19 {#section-16}

\[Deleted\]

### 125.20 {#section-17}

\[deleted\]

### Application of the Protocol questions

### 125.21 {#section-18}

\[deleted\]

### 125.22 {#section-19}

\[deleted\]

### 125.23 {#section-20}

\[deleted\]

### Extent of Protection and the Protocol: Summary

### 125.24 {#section-21}

\[deleted\]

### 125.25 {#section-22}

\[deleted\]

### Prosectution History

### 125.26 {#ref125-26}

In Actavis it was held that it is appropriate for the UK courts to adopt
a sceptical, but not absolutist, attitude to a suggestion that the
contents of the prosecution file of a patent should be referred to when
considering a question of interpretation or infringement. However, given
that the contents of the file are publicly available and are unlikely to
be extensive, there will be occasions when justice may fairly be said to
require reference to be made to the contents of the file. However, not
least in the light of the wording of Article 69 EPC 2000, which is
discussed above, the circumstances in which a court can rely on the
prosecution history to determine the extent of protection or scope of a
patent must be limited. The Court held that reference to the file would
only be appropriate where (i) the point at issue is truly unclear if one
confines oneself to the specification and claims of the patent, and the
contents of the file unambiguously resolve the point, or (ii) it would
be contrary to the public interest for the contents of the file to be
ignored. The latter would be exemplified by a case where the patentee
had made it clear that they were not seeking to contend that their
patent, if granted, would extend its scope to the sort of variant which
they now claim infringes. In L'Oréal Ltd v RN Ventures Ltd \[2018\] EWHC
173 (Pat), Carr J held that reference to the prosecution history is the
exception and not the rule.

### 125.26.1 {#ref125-26-1}

In [Akebia Therapeutics Inc v Fibrogen Inc \[2020\] EWHC 866
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2020/866.html){rel="external"},
Arnold LJ commented that "this is one of those cases referred to by Lord
Neuberger in Actavis v Lilly at \[88\] where it would be contrary to the
public interest for the contents of the prosecution file to be ignored".
In the application process, Fibrogen limited the scope of their claim to
overcome a novelty objection. This indicated that they did not intend
that the scope of any granted patent would include the removed features.
However, in the court proceedings they were attempting to extend the
effective scope through the doctrine of equivalents to include these
features again. Therefore the prosecution history was taken into
account.

### Biotechnological inventions. {#biotechnological-inventions-1}

### 125.27 {#ref125-27}

Schedule A2 to the Act was introduced by the Patents Regulations 2000
(2000 SI No 2037) as part of the implementation of Directive 98/44/EC on
the legal protection of biotechnological inventions. Paragraphs 7 to 10
of the Schedule set out certain rules for the construction of claims
which relate to biological material or to processes that enable
biological material to be produced - [see
76A.07-09](/guidance/manual-of-patent-practice-mopp/section-76a-biotechnological-inventions/#ref76A-07).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
