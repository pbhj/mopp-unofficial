::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 124A: Use of electronic communications {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated: October 2009
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 124A.01 {#a01}

This section was introduced by the Patents Act 1977 (Electronic
Communications) Order 2003 (S.I. 2003 No. 512), made under the
provisions of Sections 8 and 9 of the Electronic Communications Act
2000, and came into force on 1 April 2003. It allows the comptroller to
make directions on the form of electronic patent documents and the
manner by which they are delivered by electronic communications to the
Office. The provisions came into force in the Isle of Man by virtue of
the Patents Act 1977 (Isle of Man) Order 2003 (SI 2003 No. 1249).

### 124A.02 {#a02}

s.21 is also relevant

The comptroller has made Directions governing the following activities:
filing patent applications by electronic means; filing documents
relating to pending patent applications by electronic means; electronic
delivery of applications to amend patents under s.27 and s.75;
withdrawal of patent applications by email; email requests for
extensions of specified periods; submitting third party observations by
electronic means; submitting opinion observations by electronic means;
and requesting electronic uncertified copies from patent files. The
current Directions are reprinted in the "Relevant Official Notices and
Directions" section of this Manual, and are available on the [Office
website](http://www.ipo.gov.uk){rel="external"}.

  -----------------------------------------------------------------------
   

  **Section 124A(1)**

  The comptroller may make directions as to the form and manner in which
  documents to be delivered to the comptroller --\
  (a) in electronic form; or\
  (b) using electronic communications, are to be delivered to him.
  -----------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 124A(2)**
  A direction under subsection (1) may provide that in order for a document to be delivered in compliance with the direction it shall be accompanied by one or more additional documents specified in the direction.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 124A(3)**
  Subject to subsections (14) and (15), if a document to which a direction under subsection (1) or (2) applies is delivered to the comptroller in a form or manner which does not comply with the direction the comptroller may treat the document as not having been delivered.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 124A(4)**

  Subsection (5) applies in relation to a case where --\
  (a) a document is delivered using electronic communications, and\
  (b) there is a requirement for a fee to accompany the document.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 124A(5)**

  The comptroller may give directions specifying --\
  (a) how the fee shall be paid;\
  (b) when the fee shall be deemed to have been paid.
  -----------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 124A(6)**
  The comptroller may give directions specifying that a person who delivers a document to the comptroller in electronic form or using electronic communications cannot treat the document as having been delivered unless its delivery has been acknowledged.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 124A(7)**
  The comptroller may make directions specifying how a time of delivery is to be accorded to a document delivered to him in electronic form or using electronic communications.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 124A(8)**

  A direction under this section may be given --\
  (a) generally\
  (b) in relation to a description of cases specified in the direction;\
  (c) in relation to a particular person or persons.
  -----------------------------------------------------------------------

  ---------------------
   
  **Section 124A(9)**
  \[repealed\]
  ---------------------

  ----------------------
   
  **Section 124A(10)**
  \[repealed\]
  ----------------------

  -------------------------------------------------------------------------------------------------------
   
  **Section 124A(11)**
  A direction under this section may be varied or revoked by a subsequent direction under this section.
  -------------------------------------------------------------------------------------------------------

  ----------------------
   
  **Section 124A(12)**
  \[repealed\]
  ----------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 124A(13)**
  The delivery using electronic communications to any person by the comptroller of any document is deemed to be effected, unless the comptroller has otherwise specified, by transmitting an electronic communication containing the document to an address provided or made available to the comptroller by that person as an address of his for the receipt of electronic communications; and unless the contrary is proved such delivery is deemed to be effected immediately upon the transmission of the communication.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 124A(14)**

  A requirement of this Act that something must be done in the prescribed
  manner is satisfied in the case of something that is done­\
  (a) using a document in electronic form, or\
  (b) using electronic communications, only if the directions under this
  section that apply to the manner in which it is done are complied with.
  -----------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 124A(15)**
  In the case of an application made as mentioned in subsection 14(a) or (b) above, a reference in this Act to the application not having been made in compliance with rules of requirements of this Act includes a reference to its not having been made in compliance with any applicable directions under this section.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 124A(16)**

  This section applies­\
  (a) to delivery at, in, with or to the Patent Office as it applies to
  delivery to the comptroller; and\
  (b) to delivery by the Patent Office as it applies to delivery by the
  comptroller.
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
