::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 42: Enforceability of contracts relating to employees\' inventions {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (42.01 - 42.06) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 42.01

This is the fourth of the group of sections relating to inventions made
by employees. It relates to the enforceability of contractual terms
diminishing an employee's rights in connection with inventions made by
them after 1 June 1978.

### 42.02

For the applicability of s.42 and interpretation of certain terms used
therein, [see
39.01-04](/guidance/manual-of-patent-practice-mopp/section-39-right-to-employees-inventions/#ref39-01).

  -----------------------------------------------------------------------
   

  **Section 42(1)**

  This section applies to any contract (whenever made) relating to
  inventions made by an employee, being a contract entered into by him -\
  (a) with the employer (alone or with another); or\
  (b) with some other person at the request of the employer or in
  pursuance of the employee's contract of employment.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 42(2)**
  Any term in a contract to which this section applies which diminishes the employee's rights in inventions of any description made by him after the appointed day and the date of the contract, or in or under patents for those inventions or applications for such patents, shall be unenforceable against him to the extent that it diminishes his rights in an invention of that description so made, or in or under a patent for such an invention or an application for any such patent.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 42.03 {#ref42-03}

The contracts to which s.42 applies may be of any date but must have
been entered into by the employee with the employer or with some other
person at the request of the employer or in pursuance of the employee's
contract of employment (as detailed in subsection (1)). Any term in such
a contract diminishing the employee's rights in inventions (or patents
for them or applications for such patents) of any description made by
them after the appointed day (1 June 1978) and the date of the contract
is unenforceable against him to the extent that it diminishes those
rights.

### 42.04 {#section-2}

Thus a contractual term cannot be enforced in a way which would deny an
employee their rights to certain of their inventions as laid down by
s.39, [see
39.08](/guidance/manual-of-patent-practice-mopp/section-39-right-to-employees-inventions/#ref39-08).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 42(3)**
  Subsection (2) above shall not be construed as derogating from any duty of confidentiality owed to their employer by an employee by virtue of any rule of law or otherwise.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 42.05 {#section-3}

Any duty of confidentiality owed to his employer by an employee is not
overridden or diminished by s.42.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 42(4)**
  This section applies to any arrangements made with a Crown employee by or on behalf of the Crown as his employer as it applies to any contract made between an employee and an employer other than the Crown, and for the purposes of his section "Crown employee" means a person employed under or for the purposes of a government department or any officer or body exercising on behalf of the Crown functions conferred by any enactment or a person serving in the naval, military or air forces of the Crown.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 42.06 {#section-4}

Section 42 is made applicable to Crown employees by subsection (4), any
"arrangement" between such an employee and the Crown as their employer
taking the place of a contract between an employee and their employer
other than the Crown. The wording following "enactment" in subsection
(4) was added by s.22 of the Armed Forces Act 1981.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
