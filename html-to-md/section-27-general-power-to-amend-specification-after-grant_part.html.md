::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 27: General power to amend specification after grant {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (27.01 - 27.33) last updated: April 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 27.01

(s.77(1) is also relevant)

This section is concerned with the amendment, at the request of the
proprietor, of the specification of a granted patent, including a
European patent (UK), provided that there are no proceedings pending in
which the validity of the patent may be put in issue ([see
27.21](#ref27-21)). (Amendments where the validity of the patent may be
put in issue are covered by section 75). Prescribed regulations are set
out in r.35 and Part 7 (Proceedings heard before the Comptroller). (For
the distinction between amendment and correction [see
19.03-19.04](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-03))

### 27.02

Amendment during revocation proceedings initiated by the comptroller is
effected under s.73 and is not governed by the rules relating to
amendment under s.27 or s.75 (see also [27.05.2](#ref27-05-2) and
[73.10](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-10)).

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 27(1)**
  Subject to the following provisions of this section and to section 76 below, the comptroller may, on an application made by the proprietor of a patent, allow the specification of the patent to be amended subject to such conditions, if any, as he thinks fit.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Initial procedure

### 27.03 {#ref27-03}

r.35(1) is also relevant

An application to the comptroller for leave to amend the specification
under this section must be made in writing. The proposed amendment
should be identified and the applicant must also state the reasons for
seeking the amendment ([see 27.07](#ref27-07)). In particular, if the
amendments are intended to distinguish the invention claimed from any
published prior art which might invalidate the patent, this should be
disclosed and the prior art identified. A copy of any document referred
to by the applicant, or a translation thereof, may be called for. Where
the amendments are intended to distinguish the invention from prior use,
an outline of the nature of the prior use and when the proprietors
became aware of it should be disclosed, but more detailed information
regarding the prior use is not normally required.

### 27.03.1 {#ref27-03-1}

r.35(2) is also relevant

If it is reasonably possible, the proposed amendment and the reasons for
it shall be set out and delivered to the comptroller in electronic form
or using electronic communications. Directions prescribing the form and
manner for electronic delivery of applications to amend under s.27 were
published in the PDJ No. 5938 on 12 March 2003, and came into force with
the Patents Act 1997 (Electronic Communications Order) 2003 (S.I. 2003
No. 512) on 1 April 2003. Electronic delivery should be made either by
[email to
litigationamend@ipo.gov.uk](mailto:litigationamend@ipo@ipo.gov.uk)
entitled "Proposal to amend under s.27", or on an electronic carrier
(such as floppy disk or CD-R) delivered to the Office and accompanied by
an identifying letter. Text produced using well-known word processing
packages or filed in PDF format will be accepted, and if filed by email,
this may be provided as an attachment. It is preferable for the text to
use conventional word-processing editing features to set out the
amendments on the original version of the text to distinguish the
changes to the text. If the text cannot be read by the Office, it will
be treated as not delivered, and the applicant will be contacted with a
view to making alternative arrangements. The Office will not accept
email messages for the purpose of amendment under s.27 at any email
address other than that given above, neither will email messages in
MS-TNEF/RTF or HTML formats nor messages that are encrypted or digitally
signed be accepted. It is expected that where word-processing facilities
are available and have been used for preparation of patent
specifications, it will be "reasonably possible" for amendments under
s.27 to be submitted by electronic delivery.

\[ The application to amend is examined in Restoration and Post Grant
Section (RAPS) to see that it is formally in accordance with the Act and
Rules. This examination includes checking:\
(i) that reasons for making the amendment have been given;\
(ii) that any prior art referred to has been adequately identified;\
(iii) whether the Office is aware of any pending proceedings in which
the validity of the patent may be put in issue.\]

\[ RAPS should report any formal defects to the applicant and request
copies of any documents referred to which have not been provided and
which are not available to the Office. A translation of any document not
in English should also be requested under r.113. A period of one month
should be allowed for reply. \]

### 27.04 {#ref27-04}

s.25(1), s.77(1) is also relevant

An application to amend under this section cannot be considered until
the date on which the notice of the grant of the patent is published in
the journal, or, in the case of a European patent (UK), is mentioned in
the European Patent Bulletin, and the proprietor should be so informed
if an application is received before that date.

### 27.05 {#ref27-05}

r.35(3),r.35(4) is also relevant

Where it is considered necessary for clarity, the Office may request the
filing of a copy of the unamended specification on which the amendment
is shown marked up. In the case of a European patent (UK) specification
published in English by the EPO, the general procedure is the same as
for a UK specification. Since the authentic text is in English
translations of any amendments to the claims into French or German will
not be required, and will not be advertised or published. If the EP(UK)
specification is instead in French or German, translations into English
of the part of the specification proposed to be amended and of the part
as proposed to be amended are also required. The comptroller may require
a translation of the entirety of the specification if this is necessary
to determine allowability of the amendments. Where no full translation
has been filed or called for the applicant should be advised by RAPS
that a translation of the whole specification may be required later, for
example during any opposition proceedings to the amendment ([see
77.13-77.15](/guidance/manual-of-patent-practice-mopp/section-77-effect-of-european-patent-uk/#ref77-13)).

### 27.05.1 {#ref27-05-1}

If a European patent which designates the UK is amended during
opposition proceedings before the EPO, and the decision of the EPO is
that the patent should be maintained in its amended form, the amendments
automatically apply to the European patent (UK), [see
77.08-09](/guidance/manual-of-patent-practice-mopp/section-77-effect-of-european-patent-uk/#ref77-08).
Action under s.27 with regard to those amendments is not required. Where
amendment under s.27 of a European Patent (UK) is sought before the nine
months allowed for opposing the patent has expired or when there is an
opposition outstanding, the possibility exists that the specification
may be amended before the EPO. In such circumstances the proprietor or
agent is given the option of either (a) staying the request until the
opposition period has expired or the opposition proceedings have been
settled or (b) proceeding with the request under s.27 on the
understanding that the desired amendment may be negated as a result of
subsequent amendment before the EPO.

\[ Where an application is made to amend a European patent (UK) RAPS
should ascertain on COPS or the European Patent Register whether (a) the
period for opposing the patent has expired and whether any opposition
has been entered and (b) whether any application for central amendment
of the patent at the EPO has been made and whether any amendment has
been allowed. Where necessary the proprietor or agent will be given the
option of staying the request.

\[Deleted\]

### 27.05.2 {#ref27-05-2}

Where a European patent designating UK relates to the same invention as
a UK patent, amendments to the UK patent may be submitted under s.27 in
order to remove the conflict and so avoid revocation of the UK patent
under s.73(2). Furthermore, in Henry Reed v Sir James Laing & Sons Ltd
(BL C/74/96) Laddie J allowed amendment of the European patent (UK)
under s.27 to avoid s.73(2) conflict even though the EP(UK) patent had
lapsed - [see
73.10](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-10).

### 27.06 {#section-2}

s.118(1) is also relevant

A preliminary notice is inserted in the Journal giving only the
publication number and title of the specification and the proprietor's
name. The preliminary notice will state that, unless the application to
amend is abandoned, a further notice will be made which will announce
the amendments as open to opposition, and that meanwhile the proposed
amendments may be inspected in the Office.

\[ RAPS should arrange for preliminary advertisement of the application,
and should then refer the case to the Group Head in charge of the
subject matter to which the specification relates. \]

### Examination of the amendments

**Exercising discretion**

### 27.07 {#ref27-07}

The examiner to whom the application is referred should determine
whether the reasons given for making the amendments are sufficient. The
reasons must be such that it can be established that the amendments
effect a proper cure for any defect they are intended to rectify. The
allowance of amendments under s.27 is a matter for the discretion of the
comptroller and the onus is on the patentee to make full disclosure of
all matters material to the exercise of this discretion (Hsuing's Patent
\[1992\] RPC 497). Thus, unless full particulars of legitimate reasons
are given ([see 27.03](#ref27-03)) discretion should not be exercised in
favour of allowing the amendments. The comptroller is entitled to know
before making their decision whether it is mere whim or necessity which
has driven the proprietor to seek leave to amend (Clevite Corporations
Patent, \[1966\] RPC 199). Although the Clevite judgment was under the
1949 Act, its relevance under the 1977 Act has been confirmed by the
hearing officer in Waddingtons Ltd's Patent \[1986\] RPC 158. The
hearing officer also supported the view that the allowability of a
request to amend under s.27 is discretionary and compliance with s.76 is
not itself necessarily sufficient and where amendments are proposed to
distinguish the invention from prior art it is necessary for the
comptroller to have particulars of that prior art so that they may take
it into account in the exercise of their discretion. He held that the
omission from s.27 of any specific requirement for full particulars does
not therefore mean that such particulars can never be demanded.

### 27.08 {#ref27-08}

s.27(6) is also relevant

In considering whether to allow an application to amend, the examiner or
Group Head considering the application shall have regard to relevant
principles under the European Patent Convention ([see 27.32 and
27.32.1](#ref27-32)). For example, the EPO do not consider the behaviour
of the patent proprietor when exercising discretion in allowing
amendments. According to s.27(6), the behaviour of the patent proprietor
will therefore no longer be an issue to be considered in the UK when
deciding whether to allow an amendment to be made under section 27. This
effect was explicitly confirmed by Floyd J in [Zipher Ltd v Markem
Systems Ltd & Anr \[2008\] EWCH 1379
(Pat)](http://alpha.bailii.org/ew/cases/EWHC/Patents/2008/1379.html){rel="external"}:

> It follows that if I am to have regard to the principles applicable
> under the EPC, the discretion which I have to refuse amendments which
> comply with the Act has been limited. Considerations such as those
> formerly considered relevant to the discretion, such as the conduct of
> the patentee, are no longer relevant.

### Allowability of the amendments

### 27.09 {#section-3}

When the amendments are proposed to distinguish the invention from
specified prior art they must be such that the invention is both novel
and involves an inventive step having regard to the specified prior art
when considered in the light of common general knowledge and of the
prior art taken into account during examination of the application for
the patent.

### 27.09.1 {#ref27-09-1}

In [BL O/531/16 (PDF,
108KB)](https://www.ipo.gov.uk/p-challenge-decision-results/o53116.pdf){rel="external"},
the hearing officer stated that case law establishes that an opponent
cannot advance arguments which challenge the validity of the patent
beyond that suggested by the patentee ([see 27.28](#ref27-28)). The
hearing officer also reinforced the view that an examiner is allowed to
take into account their own background knowledge when deciding whether
to exercise the comptroller's discretion to allow the amendments and is
entitled to take into account the prior art cited during examination of
the application.

### 27.10 {#section-4}

\[Deleted\]

### 27.11 {#ref27-11}

s.76(3) is also relevant The amendments must not add matter nor must
they extend the protection conferred by the patent ([see
76.24](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-24)).
Nor must they introduce any other objection into the specification; for
example the description and claims must continue to be clear. Although
s.14(5) does not provide grounds for revocation of a patent once
granted, objections to non-compliance with s.14(5) can be raised if the
patentee applies to amend their claim judgment of Dillon LJ in
[Genentech Inc's Patent \[1989\] RPC 147 (PDF,
6.0MB)](http://rpc.oxfordjournals.org/content/106/9/205.full.pdf){rel="external"}
at pages 248-249). Objection is however not raised against proposed
amendments on the ground that the claims as amended would lack unity of
invention [(see
26.02)](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-26-patent-not-to-be-impugned-for-lack-of-unity).
An application under s.27 to amend the specification of a granted
European patent (Philips Electronic and Associated Industries Ltd's
Patent \[1987\] RPC 244) by (a) deleting French and German translations
of the claims, (b) deleting reference numerals in the claims and (c)
adding an omnibus claim to bring the specification into conformity with
UK practice was opposed as regards (b) and (c) on the grounds that these
amendments would extend the protection conferred by the patent, contrary
to s.76(2)(b) of the Patents Act 1977 at the time (now s.76(3)(b)). The
hearing officer considered that the law under the 1949 Act, whereby
reference numerals in claims are to be regarded as helpful, rather than
restrictive, is unchanged in the 1977 Act, and that deletion of the
numerals did not give rise to objection under s.76(2)(b) of the Patents
Act 1977 at the time (now s.76(3)(b)) (see also 14.135). He also held
that with the functional form of claim 1 in the present case, there was
a distinction over that in Rotocrop International Ltd v Genbourne Ltd
\[1982\] FSR 241 in which it was held that an omnibus claim, even though
tied to claim 1, covered obvious modifications of the illustrated
structure. The addition of an omnibus claim of independent form was not
objectionable in this case. As regards (a), since the authentic text of
the claims in this case was the English version, the translations
fulfilled no useful function in the UK and there was no reason to refuse
to delete them. Hence changes (a) and (b) which concerned only
presentational matters were allowed at the same time as substantive
amendment (c); but whether presentational changes alone would have been
allowed as amendments under s.27 was undecided.

### 27.11.1 {#ref27-11-1}

Since 6 April 2017 it has not been possible to amend a granted patent to
insert an omnibus claim ([see
14.124](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-124)).
However the presence of an omnibus claim in a granted patent will not be
a ground for revocation.

### Procedure

### 27.12 {#ref27-12}

Any objections to the amendment should be reported to the applicant.
Where amendments consequential to those proposed appear necessary this
should be pointed out in the report. Two months will be specified for
reply. The examination of proposed amendments to EP(UK) specifications
is based on the English text.

\[ The examiner should report in a minute whether the reasons given for
making the amendments are sufficient and whether the amendments are
allowable (or -strictly speaking - prima facie allowable, since the
amendments are subject to advertisement and opposition; [see
27.24-27.31](#ref27-24)). The examiner should then send a PDAX message
to the Group Head in charge of the subject matter. The Group Head will
forward the case to RAPS who will contact the applicant. Each amendment
or group of amendments should be dealt with separately. If any amendment
or group of amendments is not regarded as prima facie allowable the
objection should be stated in a form which is suitable for inclusion in
an official letter, which will be sent over the signature of a member of
RAPS but will give the name and telephone number of the examiner. RAPS
will inform the applicant of the latest date for response and will deal
with requests for extensions of time periods. The letter may include PC1
advising the patentee of the form in which any further amendments should
be submitted. \]

### 27.13 {#ref27-13}

Where the further amendment proposed is of a simple nature and small in
amount, it may be submitted in a letter and (in appropriate cases)
incorporated by the examiner in the original amended copy or the agent
may call at the Office to effect the further amendments. In these
circumstances the further amendments should be in a different colour and
the copy should be annotated e.g.: "further amendments in green effected
by examiner (or agent) on (date)". When the further amendment is
extensive, a published copy, showing the whole of the amendments, should
normally be provided. If the specification is heavily amended or lengthy
and the further amendment only affects some of the pages, these pages
only need be submitted. The date of filing should be annotated on each
page. Further amendments to EP(UK) specifications that are not in
English should be submitted in accordance with [27.05](#ref27-05). Where
amendments have initially been submitted electronically, further
amendments should also be filed by electronic means.

### 27.14 {#ref27-14}

s.101 is also relevant

If the examiner considers the amendments not to be allowable, and
agreement is not reached a hearing should be offered. If a hearing is to
be offered the Group Head should inform RAPS who will contact the
applicant. If some but not all of the proposed amendments appear to be
allowable, this should be reported to the applicant, who must be given
an opportunity to decide whether to proceed with the allowable
amendments or to withdraw all of the proposed amendments (again, subject
to the offer of a hearing). If the applicant requests to be heard they
should be informed that an ex parte hearing will be appointed subject to
advertisement in the Journal of notice of the proposed amendment and
absence of opposition ([see 27.17](#ref27-17)).

\[ A Deputy Director takes any ex parte hearing. \]

### 27.15 {#ref27-15}

The comptroller has discretion to allow withdrawal of an application to
amend (Upjohn Co (Beal's) Patent, \[1973\] RPC 77) but the application
cannot be withdrawn after the determination of the allowability of the
amendments by the comptroller (Emulsol Corporation's Application, 57 RPC
256). The point of "determination" is taken to be the date of issue of
the letter informing the applicant that the amendments are either
allowed (after the amendments have been effected or a new specification
has been provided -[see 27.18](#ref27-18)), or refused. If some but not
all of the proposed amendments appear to be allowable, this should be
reported to the applicant, who must be given an opportunity to decide
whether to proceed with the allowable amendments or to withdraw all of
the proposed amendments. Prior to this point, the comptroller has
discretion to accept a withdrawal request. However, where they are of
the view that the amendments submitted cure a defect that has been
identified, discretion is likely to be exercised to refuse the request
to withdraw. Where withdrawal of an application to amend has been
refused, the amendments submitted do not cure the defect in the patent
and the applicant fails to submit alternative amendments which will cure
the defect, the application to amend must also be refused. The fact that
the patentee has sought to withdraw their application to amend and that
this withdrawal request has been refused should be recorded in the
register, in addition to the normal entries relating to the application
to amend and refusal to allow the amendment. Where amendments submitted
do cure the defect, the applicant should be informed that the amendment
application will progress as usual notwithstanding their wish to
withdraw.

\[ Suitable wording for the additional register entry concerning refusal
to allow withdrawal should be provided by the examiner. \]

### 27.16 {#ref27-16}

r.75 is also relevant

When the examiner considers that the amendments appear to be allowable
(subject to any opposition) the actions set out in [27.12](#ref27-12)
should be taken. RAPS will arrange for a notice to be made in the
Journal declaring that the amendments are now open to opposition. The
notice will state that copies of the amendments and the reasons for it
are available by application to the RAPS, and, if amendments have been
filed electronically, details of the amendments can also be viewed
through the website.

### 27.17 {#ref27-17}

If on the other hand no agreement has been reached and the matter is to
be decided at a hearing, notice of the proposed amendments may be
advertised in the Journal, the advertisement making clear that the
question of the allowability of the amendments has not yet been
determined, but that if it proves to be acceptable in the form proposed,
a further advertisement will not be made. If no notice of opposition is
received within the prescribed period ([see 27.25](#ref27-25)) an ex
parte hearing will be held. If the proposed amendments are accepted
essentially unchanged they should be effected without further delay, but
if the amendments approved differ materially from those originally
proposed, a further notice announcing a new opposition period will be
made, and the decision is an interim one [see 27.29](#ref27-29) If the
decision of the hearing is to refuse the amendments this fact must be
advertised.

### Final procedure

### 27.18 {#ref27-18}

r.35(6) is also relevant

When, either in the absence of opposition or following the conclusion of
opposition proceedings, the form of the amendments to be accepted is
decided upon, either the amendments may be effected in the original
specification in the Office, or the applicant may be called upon to
provide, within a specified period, a new specification complying with
Schedule 2 to the Rules and incorporating the amendments, including any
amended drawings. Such a new specification should be required whenever
the amendments are extensive or confusing. In the case of a EP(UK)
specification, the amendments are entered in the file copy onto the
authentic text (and also onto the translation of the claims if the
authentic text is not in English) by RAPS if this is practicable.
However if the amendments are extensive or would be confusing bearing in
mind that the file copy will have been printed in comparatively small
print a new specification as amended and prepared in accordance with
Schedule 2 to the Rules should be required under rule 35(6). Once the
amendments have been effected or a new specification has been provided,
the applicant for amendment should be informed by letter that the
amendments are allowed.

\[ RAPS will decide whether or not to require a new specification under
r.35(6). If the amendments are to be effected in the Office, this should
be done by RAPS. \]

### 27.19 {#ref27-19}

Where a fresh specification has been supplied as referred to in
paragraph [27.18](#ref27-18), the original specification is not
cancelled but certificates are placed on the dossier for both old and
new specifications. The certificate relating to the old specification
will read:

> This specification has been amended under Section 27 of the Patents
> Act, 1977, the proprietor of the patent having been notified of the
> decision to allow the amendment on \[date\] and the present form is
> shown in a new specification attached hereto, filed under rule 35(6).

The certificate relating to the new specification will read:

> This specification, filed under rule 35(6), is a true copy of the
> specification as amended under Section 27 of the Patents Act, 1977,
> the proprietor of the patent having been notified of the decision to
> allow the amendment on \[date\].

If the amendments are instead effected in the original specification
this will bear a certificate reading:

> The amendments shown on pages ........ of this specification, were
> made under Section 27 of the Patents Act, 1977, the proprietor of the
> patent having been notified of the decision to allow the amendment on
> ............. .....................

\[ RAPS should prepare the certificate(s) for the signature of the Group
Head. Where a new specification has been filed, RAPS should also check
that it is in fact an exact copy of the specification as amended, and
create a minute to confirm that the check has been made and to point out
any apparent discrepancies. The final responsibility lies with the Group
Head who should, when satisfied, confirm that the signed certificate(s)
may be placed on file. Should allowability be determined at a hearing,
the date on which the application to amend was allowed will be the date
of the decision.\]

### 27.20 {#ref27-20}

When the specification has been amended, that fact is advertised in the
Journal. When the advertisement appears or shortly thereafter, the
amended specification is published accompanied by a front page carrying
a "C" designation and including the bibliographic data (updated if
necessary) of the B specification together with an endorsement that the
patent has been amended or, in the case of an EP(UK) specification,
amended for the UK. Any orders for copies of the amendments received
before the C document is available are retained and dealt with when it
becomes available. If further amendments lead to second and further C
publications, these carry designations C2, C3, etc. In the case of an
amended EP(UK) specification, the country code given at the top of the C
front page is EP/UK.

\[ On forwarding an amended specification to Publishing Section, RAPS
will indicate any changes in the bibliographic data from those on the
front page of the B specification. RAPS also indicate the date on which
the amendment(s) is/are allowed. In preparing the C front page,
Publishing Section should take the relevant data from the front page of
the B document unless RAPS have indicated a change. A suitable footnote
should be provided to indicate any changes. In the case of an amended
EP(UK) specification the layout of the C front page is changed from that
of the front page of the B document and is instead much the same as that
of the front page of the B or C document of a domestic patent. \]

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 27(2)**
  No such amendment shall be allowed under this section where there are pending before the court or the comptroller proceedings in which the validity of the patent may be put in issue.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 27.21 {#ref27-21}

Proceedings in which validity may be put in issue are defined in s.74(1)
([see
74.03](/guidance/manual-of-patent-practice-mopp/section-74-proceedings-in-which-validity-of-patent-may-be-put-in-issue/#ref74-03)),
and while they are pending amendment can only be made under s.75. It was
decided in Lever Bros' Patent \[1955\] RPC 198 that proceedings are
"pending" until the period for appeal has expired and any appeal has
been determined. Where the proceedings are concluded by a consent order,
they are normally "pending" until the court approves the order
(Critchley v Engelmann \[1971\] RPC 346); [see also
75.12.1](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-12-1).
Proceedings are not regarded as pending merely because a writ has been
issued, but are considered to have begun only when it has been served
(Foseco International Ltd's Patent, \[1976\] FSR 244). For the situation
where amendment under s.27 of a European Patent (UK) is sought before
the nine months allowed for opposing the patent has expired or when an
opposition is outstanding, [see 27.05.1](#ref27-05-1).

\[ A check is made as part of the formal examination of the application
to amend. [See 27.03.1](#ref27-03-1) \]

### 27.21.1 {#section-5}

If proceedings in which validity may be put in issue are instituted
while an application to amend under s.27 is pending, the application to
amend should normally be stayed pending resolution of those proceedings.
However, where an opposition to the application to amend has been filed
and the applicant has not filed a counter-statement within the
prescribed period, the application should normally be treated as
withdrawn rather than stayed ([see 27.26](#ref27-26)).

  --------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 27(3)**
  An amendment of a specification of a patent under this section shall have effect and be deemed always to have had effect from the grant of the patent.
  --------------------------------------------------------------------------------------------------------------------------------------------------------

### 27.22 {#ref27-22}

Hence the issue of infringement between grant and amendment is to be
decided upon the claims of the amended patent (but see s.62(3)). If an
act would have infringed these claims then infringement may be
considered to have taken place, even though the patent before amendment
may have been invalid; conversely if the amended claims would not have
been infringed then infringement is considered not to have occurred,
even though the claims before amendment may have been both valid and
infringed.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 27(4)**
  The comptroller may, without an application being made to him for the purpose, amend the specification of a patent so as to acknowledge a registered trade-mark.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 27.23 {#section-6}

Under the Trade Marks Act 1994, a reference to a registered trade mark
includes a service mark. If it comes to the notice of the Office that a
term used in the specification of a patent is a registered trade mark
and this fact has not been acknowledged in the specification, the
proprietor should be so informed and advised that before a decision is
made whether to amend the specification they have a period of two months
in which to submit any comments. If the proprietor replies that the term
is not in fact a registered trade or service mark the matter will have
to be determined, but the validity of the mark is not material.

\[ See under
[14.70](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-70)
for the procedure to check whether a word is a registered mark. The
necessary acknowledgement of the registered trade or service mark should
be effected by amendment in the original specification by RAPS. The
procedure set out under [27.18 to 27.20](#ref27-18) should be followed.
\]

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 27(5)**
  A person may give notice to the comptroller of his opposition to an application under this section by the proprietor of a patent, and if he does so the comptroller shall notify the proprietor and consider the opposition in deciding whether to grant the application.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 27.24 {#ref27-24}

Any person may oppose an amendment; there is no need for an opponent to
show a locus standi (Braun AG's Application \[1981\] RPC 355), nor even
for an agent to identify an opponent for whom they are acting (Sanders
Associates' Patent BL O/89/81).

### 27.25 {#ref27-25}

Notice of opposition should be given on Form 15 which should be filed in
duplicate within four weeks of the date of the advertisement in the
Journal announcing the amendments as open to opposition ([see 27.16,
27.17](#ref27-16)). Except where proceedings are pending before the
court or comptroller in which validity is put in issue ([see 27.21 --
27.21.1](#ref27-21), this period may not be extended. The notice should
be accompanied by a copy thereof and a statement of grounds (in
duplicate). This starts proceedings before the comptroller, the
procedure for which is discussed [at 123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).

### 27.26 {#ref27-26}

If, having been notified of the opposition, the applicant for amendment
does not file a counter-statement they will under r.77(9) be treated as
supporting the opponent's case. In that event, the applicant for
amendment should be informed in an official letter that, subject to
comments within a specified period (normally 14 days), the applicant
will forfeit the right to take any further part in the opposition
proceedings. A copy of the official letter should be sent to the
opponent. In Norsk Hydro's Patent \[1997\] RPC 89 (decided under r.40(4)
of the Patents Rules 1995 which required the applicant to file a
counter-statement if he wished to continue with the application) it was
held that the comptroller had jurisdiction to allow a request for
extension to the period retrospectively as a matter of discretion. In
the particular instance it was not appropriate to exercise that
discretion because at the material time (the end of the specified
period) it was not evident that N did not wish the application for
amendment to be withdrawn.

\[Tribunal is responsible for sending the respective copies to the
applicant and the opponent\]

### 27.26.1 {#section-7}

Any offer of alternative amendments should be made in the
counter-statement, with the applicant for amendment making it clear
whether the offer is a firm one or is conditional upon an adverse
finding on the originally requested amendments. Such alternative
amendments should be submitted in accordance with the procedure set out
in [27.13](#ref27-13).

### 27.27 {#section-8}

r.82 is also relevant

The comptroller may give such directions as they think fit with regard
to the subsequent procedure. Thus, if the alternative amendments are
significantly different from the original ones, a further notice
announcing a new opposition period should be made in the Journal ([see
27.17](#ref27-17), making it clear whether they supersede or are
conditional upon the refusal of the amendments previously open to
opposition. In any case, the opponent(s) to the amendments as originally
requested should be given the opportunity to file a supplementary
statement opposing the new amendments and setting out the grounds for so
doing. Any objection to the alternative amendments by the Office should
also be reported as outlined in [27.12](#ref27-12). If a form of
amendment acceptable to the parties and the Office is not arrived at,
the matter will need to be decided at a hearing ([see
27.29](#ref27-29)).

\[ The alternative amendments offered should be referred to a Deputy
Director in charge of the subject matter to which the specification
relates. When these amendments are significantly different from those
originally requested, the Deputy Director should defer their
consideration until after the new amendments have been advertised.
Hearings on opposed s.27 proceedings must be taken by a Deputy Director
or above. \]

### 27.27.1 {#ref27-27-1}

Following the hearing officer's decision in Intel Corporation's Patent
\[2002\] RPC 48, the evidence rounds are generally reversed, so that the
applicant for amendment will normally have the first evidence round. The
hearing officer - while accepting that in opposed amendment proceedings
both sides have a substantial onus to discharge - could see no good
reason why the evidence rounds should differ from the normal procedure
in the Patents Court for opposed amendments under s.75. He also made
clear that the possibility remains of requiring the opponent(s) to go
first if circumstances justify that, or of requiring the first two
evidence rounds to be conducted in parallel. \]

### 27.28 {#ref27-28}

s.74(2) s.26 is also relevant

Both in the notice of opposition and supporting statement and in any
subsequent proceedings the opponent must address themselves solely to
the allowability of the proposed amendments, and may not attack the
validity of the patent as it would be after amendment, except that if
the amendments have been sought in order to remove an admitted defect
casting doubt on the validity of the patent, the opponent may argue that
the proposed amendments are not adequate to remove the defect (James
Gibbons Ltd's Application \[1957\] RPC 158, Bridgestone Tire KK's
Patent - [BL
O/166/92](https://www.gov.uk/government/publications/patent-decision-o16692)).
The opponent may not object that the proposed amendment would introduce
plurality of invention ([see
26.01](/guidance/manual-of-patent-practice-mopp/section-26-patent-not-to-be-impugned-for-lack-of-unity/#ref26-01)).

### 27.29 {#ref27-29}

The fact of allowance or refusal of the amendments is advertised in the
Journal. If the form of the amendments eventually found acceptable
differs materially from the original form, the decision allowing them
should be an interim one, and an advertisement announcing the
provisionally allowed amendments as open to opposition should be made.
In Clear Focus Imaging Inc. v Contra Vision Ltd (Patents Court, 16
November 2001, unreported) Jacob J emphasised that the decision to
re-advertise or not is pre-eminently a matter of discretion of the
hearing officer. Furthermore, he noted that re-advertisement is not
ordered in the interests of any existing opponent to the amendment; it
is done to alert other potential opponents. Neither is it ordered to
account for latecomers who, had they known, might have wished to oppose
the amendments from the outset.

### 27.30 {#section-9}

Under the 1949 Act the opponent was not normally required to pay costs,
whatever the outcome of the opposition (see BTH Co Ltd's Patent, 53 RPC
255). However the ordering of an opponent to pay costs is clearly
envisaged by section 107 in view of the specific reference to section
27(5) in section 107(4)(c) which provides that an opponent who neither
resides or carries on business in the United Kingdom may be required to
give security for costs.

### 27.31 {#ref27-31}

Where an opposition to the amendments has been properly launched by the
filing of Form 15 accompanied by a supporting statement, but the
opponent subsequently withdraws at any stage, the comptroller
nevertheless takes account of matters raised by the opponent in deciding
whether discretion to allow amendment should be exercised (Ministry of
Agriculture, Fisheries and Food's Patent, [BL
O/11/92](https://www.gov.uk/government/publications/patent-decision-o01192)).
If no agreement is reached with the applicant, an ex parte hearing is
held as described in [27.17](#ref27-17).

\[ The matters raised by an opponent prior to the withdrawal of an
opposition should be considered by the hearing officer, if one has been
appointed ([see 27.14](#ref27-14)), or otherwise the Group Head
originally charged with the assessment of the amendments ([see
27.12](#ref27-12). \]

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 27(6)**
  In considering whether or not to allow an application under this section, the comptroller shall have regard to any relevant principles under the European Patent Convention.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 27.32 {#ref27-32}

Section 27(1) confers on the comptroller discretion to allow or refuse
an amendment to the patent. Articles 105a(1) and 123 EPC also confer on
the EPO discretion to allow or refuse an amendment of the European
patent. The comptroller continues to have discretion to allow or refuse
an amendment, but in exercising that discretion, section 27(6) requires
the comptroller to have regard to any relevant principles which are
applicable to amendment or limitation proceedings under the EPC. These
may include relevant regulations made under the EPC, any relevant
guidelines produced by the EPO, and decisions of the Opposition Division
and Boards of Appeal. This should ensure that, as far as possible, there
is consistency in approach as regards post-grant amendment in national
proceedings and before the EPO. The EPO do not consider the behaviour of
the patent proprietor when exercising discretion in allowing amendments.
The intended effect of section 27(6) is that the behaviour of the patent
proprietor will no longer be an issue to be considered in the UK when
deciding whether to allow an amendment to be made under section 27. This
effect was confirmed by Floyd J in Zipher v Markem ([see
27.08](#ref27-08) and below).

### 27.32.1 {#ref27-32-1}

In Zipher Ltd v Markem Systems Ltd & Anr \[2008\] EWCH 1379 (Pat), Floyd
J summarised the position under the EPC as follows:

i\) in opposition proceedings, appropriateness of the amendments to the
proceedings, their necessity and procedural fairness are the main,
perhaps only, factors considered relevant to the discretion to allow
amendment;

ii\) in central amendment proceedings, compliance with the procedural
requirements gives rise to a right to have the patent limited in
accordance with the request.

These are therefore the factors which should be taken into account when
considering whether or not to allow an amendment under this section.

### 27.33 {#section-10}

In central limitation before the EPO it is only necessary to meet the
procedural requirements in order for a patent to have its claims
centrally limited. There is no assessment of novelty or inventiveness of
the proposed claims in this process and it has been argued that these
aspects should not be considered for amendments under section 27 since
section 27(6) requires the comptroller to have regard to relevant
principles under the EPC. However, amendment under section 27 in the UK
is still subject to the comptroller's discretion and although central
limitation is a similar process to amendment under section 27 it is not
an identical process. The main differences between the two processes are
that i) no reason need be supplied for central limitation at the EPO
while rule 35(1)(c) requires a reason to be provided for amendment under
section 27, ii) there is no opportunity for third party opposition to
central limitation while section 27(5) provides for third party
opposition to the proposed amendments and iii) the EPC requires that the
claims are a limitation of the granted claims while section 27 does not
have such a requirement (though section 76 essentially means that they
must be). Section 27 still gives the comptroller discretion to allow or
refuse an amendment provided they have had regard to the relevant EPO
principles. As long as they have exercised their discretion reasonably
and judicially there is no reason why they should not exercise that
discretion. Having given regard to the principles of the EPC with
respect to central limitation and having sufficiently distinguished the
two processes, the reasonable conclusion is that the lack of discretion
for the EPO to refuse a central limitation request meeting the formal
requirements does not limit the comptroller's discretion to refuse an
amendment under section 27 on the basis of lack of novelty or
inventiveness of the proposed amendment.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
