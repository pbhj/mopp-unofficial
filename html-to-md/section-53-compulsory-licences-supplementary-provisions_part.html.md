::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 53: Compulsory licences; supplementary provisions {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (53.01 - 53.06) last updated: April 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 53.01

This section comprises various supplementary provisions relating to
ss.48 to 51 and applications thereunder. Such applications may be for
compulsory licences or "licences of right" entries or modification of
licences, as outlined in
[48.01](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-01)
and
[51.01](/guidance/manual-of-patent-practice-mopp/section-51-powers-exercisable-in-consequence-of-report-of-competition-and-markets-authority/#ref51-01).

### 53.02

These sections are members of a group (46 to 54) relating to licences of
right, compulsory licences etc. In April 2014, sections 50A, 51 and this
section were amended by the Enterprise and Regulatory Reform Act 2013
(Competition) (Consequential, Transitional and Saving Provisions) Order
2014 to reflect the change in name and responsibilities from the
Competition Commission to the Competition and Markets Authority.

### \[Section 53(1) Repealed\]

### 53.03 {#ref53-03}

Section 53(1) was repealed by the Patents Act 2004. This subsection was
concerned with provisions under the Community Patent Convention, which
never came into force.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 53(2)**
  In any proceedings on an application made under section 48 above in respect of a patent, any statement with respect to any activity in relation to the patented invention, or with respect to the grant or refusal of licences under the patent, contained in a report of the Competition and Markets Authority laid before Parliament under Part VII of the Fair Trading Act 1973 or section 17 of the Competition Act 1980 or published under Part 3 or 4 of the Enterprise Act 2002 shall be prima facie evidence of the matters stated, and in Scotland shall be sufficient evidence of those matters.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Effect of statements by Monopolies and Mergers Commission

### 53.04 {#ref53-04}

The status as evidence, in proceedings under s.48, of statements in
reports of the Competition Commission is set out in subsection (2). The
scope of this provision was adjusted by the CDP Act, including
introduction of the reference to the Competition Act 1980, and the
reference to the Enterprise Act 2002 was added by Paragraph 8 of
Schedule 25 of this Act, which came into force on 20 June 2003.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 53(3)**
  The comptroller may make an entry in the register under sections 48 to 51 above notwithstanding any contract which would have precluded the entry on the application of the proprietor of the patent under section 46 above.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 53(4)**
  An entry made in the register under sections 48 to 51 above shall for all purposes have the same effect as an entry made under section 46 above.
  --------------------------------------------------------------------------------------------------------------------------------------------------

### Relationship with entries made under s.46 {#relationship-with-entries-made-under-s46}

### 53.05 {#section-2}

Section 46 provides for "licences of right" entries in the register to
be made voluntarily, i.e. at the request of the proprietor of the patent
in question. Section 53(3) and (4) give relationships between such
entries and those made compulsorily under ss.48 to 51.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 53(5)**
  No order or entry shall be made in pursuance of an application under sections 48 to 51 above which would be at variance with any treaty or international convention to which the United Kingdom is a party.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Effect of treaties and international conventions

### 53.06 {#ref53-06}

Register entries or orders regarding licences are not made under ss.48
to 51 if contrary to a treaty or convention, eg TRIPS or the Treaty
establishing the European Community. Section 54 makes similar provision
with regard to reciprocal arrangements with any country specified in an
Order in Council, [see
54.01](/guidance/manual-of-patent-practice-mopp/section-54-special-provisions-where-patented-invention-is-being-worked-abroard/#ref54-01).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
