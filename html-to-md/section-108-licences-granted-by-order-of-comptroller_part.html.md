::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 108: Licences granted by order of comptroller {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (108.01 - 108.03) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 108**
  Any order for the grant of a licence under section 11, 38, 48 or 49 above shall, without prejudice to any other method of enforcement, have effect as if it were a deed, executed by the proprietor of the patent and all other necessary parties, granting a licence in accordance with the order.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 108.01

The grant of a licence may be ordered by the comptroller (or by the
court on appeal) under s.11 or 38 (order in favour of original
applicant, patentee or licensee who worked the invention prior to a
reference under s.8 or 37 resulting in substitution of new applicant or
patentee) or s.48 or 49 (order for compulsory licence because invention
not being adequately worked or activities being unfairly prejudiced by
actions of patentee).

### 108.02

Section 108 ensures that such an order under one of those sections will
be effective even if the parties affected take no action in response to
it. The order has effect as if it were a deed, executed by the patentee
and all other necessary parties, granting a licence in accordance with
the order.

### 108.03

This section applies to 1977 Act patents including European patents
(UK).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
