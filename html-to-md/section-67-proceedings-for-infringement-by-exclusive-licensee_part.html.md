::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 67: Proceedings for infringement by exclusive licensee {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (67.01 - 67.06) last updated: July 2022.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 67.01

This section lays down the position of an exclusive licensee under a
patent with regard to infringement of that patent. The exclusive
licensee has the same right as the proprietor of the patent to bring
infringement proceedings in respect of acts committed after the date of
the licence (subsection (1)) and may be awarded damages or other relief
by the court or the comptroller in respect of infringement of their
rights as exclusive licensee (subsection (2)). The proprietor must be
made a party to any such proceedings (subsection (3)).

### 67.02

In accordance with s.69, references in this section to a patent and the
proprietor of a patent are to be respectively construed as including
references to an application which has been published but not yet
granted and the applicant.

### 67.03

For the applicability of s.67 in relation to European patents, [see
60.02](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-02).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 67(1)**
  Subject to the provisions of this section, the holder of an exclusive licence under a patent shall have the same right as the proprietor of the patent to bring proceedings in respect of any infringement of the patent committed after the date of the licence; and references to the proprietor of the patent in the provisions of this Act relating to infringement shall be construed accordingly.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 67.04 {#ref67-04}

s.130(1) is also relevant

::: call-to-action
Section 130(1) of the 1977 Act defines an exclusive licence as meaning
''a licence from the proprietor of or applicant for a patent conferring
on the licensee, or on them and persons authorised by them, to the
exclusion of all other persons (including the proprietor or applicant),
any right in respect of the invention to which the patent or application
relates.'' Multiple exclusive licences may be granted under a single
patent or application. For example, separate exclusive licences could be
granted to different parties for the respective rights of manufacturing
the invention and using the invention, or to confer the rights in
different geographical areas. However, exclusive licenses need not be
limited to such distinct rights. [Pumfrey J in Spring Form Inc v Toy
Brokers Ltd & Ors \[2001\] EWHC 535
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2001/535.html){rel="external"}
considered that a licence to "any right" could cover "any subdivision of
the monopoly conferred on the proprietor" (at \[20\]). The idea that an
exclusive license does not have to be aligned directly i.e. ''be
co-extensive'' with a claim of a patent, and that multiple exclusive
licenses may be awarded for working different aspects of a patent claim
(e.g. different dosage regimes), was reinforced by the Court of Appeal
in [Neurim Pharmaceuticals (1991) Ltd v Generics (UK) Ltd \[2022\] EWCA
Civ
359](https://www.bailii.org/ew/cases/EWCA/Civ/2022/359.html){rel="external"}
(see \[23\]-\[29\]).
:::

### 67.05 {#ref67-05}

In Morton-Norwich Products Inc and others v Intercen Ltd \[1981\] FSR
337 the Patents Court held that the corresponding provisions of the 1949
Act were to be intended to give a licensee, who has stepped into the
shoes of the patentee so as to be able to exercise any right in respect
of the invention for their own benefit to the exclusion of all others
including the patentee, exactly the same right as the patentee would
have had. It was held that there was no requirement for any particular
document or form of grant as being necessary to constitute an exclusive
licence and that the position was a mixed question of law and fact.

### 67.06 {#ref67-06}

The consequences of non-registration of the licence on infringement
proceedings are laid down by s.68.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 67(2)**
  In awarding damages or granting any other relief in any such proceedings the court or the comptroller shall take into consideration any loss suffered or likely to be suffered by the exclusive licensee as such as a result of the infringement, or, as the case may be, the profits derived from the infringement, so far as it constitutes an infringement of the rights of the exclusive licensee as such.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 67(3)**
  In any proceedings taken by an exclusive licensee by virtue of this section the proprietor of the patent shall be made a party to the proceedings, but if made a defendant or defender shall not be liable for any costs or expenses unless he enters an appearance and takes part in the proceedings.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
