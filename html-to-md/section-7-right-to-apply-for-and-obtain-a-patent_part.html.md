::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 7: Right to apply for and obtain a patent {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (7.01 - 7.13) last updated: April 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  --------------------------------------------------------------------------------------
   
  **Section 7(1)**
  Any person may make an application for a patent either alone or jointly with another
  --------------------------------------------------------------------------------------

### 7.01

The status of a properly-launched application ([see
15.02-15.06](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-02-15-06))
is not impugned if it subsequently transpires that the applicant has no
right to the grant of a patent or that they only acquired the right
after making the application.

### 7.02

The term "person" includes one or more individuals or a corporate body
but not a firm, partnership or body which is unincorporate, although in
such cases application may be made by individual partners jointly. In
the case of a limited partnership, the application may be in the names
of all personally responsible partners see also [7.05](#ref7-05).

### 7.03

The following are regarded as corporate bodies:-

-   British companies limited by guarantee;
-   cooperative Societies (Home and Foreign)
-   euratom and similar international organisations
-   foreign states;
-   limited liability partnerships (LLP) in the UK
-   ministers and other Heads of government departments (home and
    foreign);
-   trade associations (on evidence of incorporation);
-   universities

[See
32.06](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-06)
with regard to the effect of re-registration as a p.l.c and conversion
to LLP.

### 7.04

The following are examples of foreign companies regarded as corporate
bodies by whom applications may be made:-

-   Akciova spolecnost
-   Aktiebolag, Aktiebolaget
-   Aktiengesellschaft (A.G.)
-   Aktieselskab, Aktieselskabet, Aktieselskapet
-   Arbeitsgemeinschaft
-   Besloten Vennootschap met beperkte aansprakelijkheid (B.V)
-   Eingetragene Genossenschaft
-   Eingetragenen Verein
-   Gesellschaft mit beschrankter Haftung (G.m.b.H)
-   Gewerkschaft
-   Handelsbolag
-   Handelsvenootschap
-   Kabushika Kaisha
-   Kollektivgesellschaft
-   Kommanditbolag
-   Kommanditgesellschaft auf Aktien
-   Naamlooze Vennootschap (N.V.)
-   Reszveny-tarsasag
-   Sociedad anonima
-   Sociedad Limitada
-   Societa Anonima
-   Societa per Azioni (S.p.A.)
-   Societe Anonyme
-   Societe a responsibilite limitee
-   Societe en commandite par actions
-   Societe en commandite simple
-   Societe en nom collectif
-   Versicherungsverein auf Gegenseitigkeit

### 7.05 {#ref7-05}

An application by a German or Austrian "Kommanditgesellschaft", "offene
Handelsgesellschaft" or a Swiss "Societe Commandite", may be made in the
names of the responsible partners. Following a decision by the Patents
Appeal Tribunal ([Schwarzkopf's Application, \[1965\] RPC
387](https://doi.org/10.1093/rpc/82.12.387){rel="external"}) an
application made in the names of one of these bodies is also allowed to
proceed, but such a course is at the risk of the applicants should their
representations thereafter be found to be false. Consideration will be
given to other foreign firms if it is affirmed by the applicant or
agent, that under the laws of the foreign state in question, such firms
can acquire title to land and property in their own name, such title
being wholly unaffected by changes in the personnel of the members. A
Scottish partnership firm may apply in its own name, the partners' names
being given. Applications may also proceed in the name of a limited
partnership organised under the laws of the American state of Arkansas,
California, Connecticut, Delaware, Illinois, Louisiana, Michigan,
Minnesota, Missouri, New York, Ohio, Pennsylvania , Tennessee, Texas or
Wyoming. In Canada, limited partnerships may be incorporated either
federally or provincially. In the latter case, the name of the province
(e.g. Quebec, Ontario) should be given as the state or incorporation.

  -----------------------------------------------------------------------
   

  **Section 7(2)**

  A patent for an invention may be granted -\
  (a) primarily to the inventor or joint inventors;\
  (b) in preference to the foregoing, to any person or persons who, by
  virtue of any enactment or rule of law, or any foreign law or treaty or
  international convention, or by virtue of an enforceable term of any
  agreement entered into with the inventor before the making of the
  invention, was or were at the time of the making of the invention
  entitled to the whole of the property in it (other than equitable
  interests) in the United Kingdom;\
  (c) in any event, to the successor or successors in title of any person
  or persons mentioned in paragraph (a)\
  or (b) above or any person so mentioned and the successor or successors
  in title of another person so mentioned; and to no other person.
  -----------------------------------------------------------------------

### 7.06 {#ref7-06}

7.06 The right to grant of a patent belongs primarily to the inventor,
but this may be overridden by any rule of law or any legally enforceable
agreement existing at the time the invention was made. The commonest way
in which this proviso would cause the rights to pass from the inventor
is when the invention was made in the course of employment (see s.39).
The right to the patent will also pass to another person if for example
the inventor or a person who has acquired the right by operation of law
assigns those rights, or dies or becomes bankrupt.

### 7.07 {#section-4}

s.13(2) is also relevant

Any applicant who is not an inventor must file Form 7 identifying the
inventor and indicating their own right to be granted a patent - [see
13.08 to
13.16](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-08).
If the period allowed for doing this has expired at the time the rights
are assigned, the procedure described in
[32.08-09](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc#ref32-08)
must be followed.

### 7.08 {#section-5}

If an applicant dies the application may proceed in the name of their
personal representative. Likewise if an inventor dies before an
application is made the personal representative may act on their behalf.
If the Office becomes aware of the death of an applicant before a patent
is granted but the application has not been assigned to another party by
the time it is in order for grant then, if a personal representative has
been appointed, the patent should be granted in the name of the personal
representative. If, however, a personal representative has not been
appointed by the time the application is in order for grant, the patent
should be granted in the name of the deceased applicant.

### 7.09 {#section-6}

The term "personal representative" is interpreted in its ordinary
meaning as "executor or administrator", and is restricted to a
representative appointed in the UK. Probate or Letters of Administration
must be produced to confirm the standing of the personal representative.
A person is not regarded as a personal representative merely by being an
assignee or by holding a power of attorney, nor is an Official Receiver
or trustee in bankruptcy. The personal representative may be a corporate
body.

### 7.10 {#section-7}

Where an applicant is less than eighteen years of age the application
may proceed in their name. Alternatively, the application may be made by
their parent or guardian.

### 7.11 {#section-8}

Where an applicant is of unsound mind the application should nonetheless
proceed in their name. Where there is no patent agent acting the
application should be signed ([see
14.04.20](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-04-20))
by the person duly appointed to administer the applicants' property.

### 7.11.1 {#ref7-11-1}

::: call-to-action
The question of whether an applicant can derive the right to apply for a
patent from ownership of an artificial intelligence (AI) machine, when
the AI machine is designated by the applicant as the inventor for the
application in question, was considered in [Thaler v Comptroller-General
of Patents \[2021\] EWCA Civ
1374](https://www.bailii.org/ew/cases/EWCA/Civ/2021/1374.html){rel="external"}.
The final appeal to the Supreme Court was considered in [Thaler v
Comptroller-General of Patents, Designs and Trade Marks \[2023\] UKSC
49](https://www.bailii.org/uk/cases/UKSC/2023/49.html){rel="external"}.

In the Court of Appeal, delivering the majority opinion of the court
(Birss LJ dissenting), Arnold LJ agreed with the findings of the High
Court that ownership of the AI machine (which the applicant referred to
as DABUS) was not covered by either s.7(2)(b) or s.7(2)(c). In the
Supreme Court judgment, LJ Kitchin stated that the applicant had "not
identified any basis in law on which he acquired such a right \[to apply
for a patent\] through his ownership of DABUS".  The COA and SC both
rejected the applicant's argument that the common law principle of
accession applied to intangible property. The COA noted that s.7 of the
Patents Act 1977 (as amended) required a transfer of the right to apply
by agreement, and because the named inventor in this case was neither a
natural nor a legal person, they could not own intellectual property or
transfer any rights associated with it to the applicant. The applicant
had not therefore provided a satisfactory derivation of right through
ownership of the AI machine and hence was not entitled to apply for the
patents in question. The COA and Supreme Court also agreed with the High
Court that as DABUS was not a natural person, it could not be regarded
as an inventor for the purposes of sections 7 or 13 of the Patents Act
1977 ([see
13.10.1](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-10-1)).
The Supreme Court noted that it is not the function of the IPO to
investigate the correctness of an apparently genuine statement of fact
by the applicant regarding their derivation of rights to be granted the
patent. Rather, such information (generally provided on a F7) should be
taken at face value.
:::

  ---------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 7(3)**
  In this Act "inventor" in relation to an invention means the actual deviser of the invention and "joint inventor" shall be construed accordingly.
  ---------------------------------------------------------------------------------------------------------------------------------------------------

### 7.12 {#ref7-12}

s.130(1) is also relevant

This definition of "inventor" applies to all references in the 1977 Act.
In [Henry Brothers (Magherafelt) Ltd v The Ministry of Defence and the
Northern Ireland Office \[1999\] RPC
442](https://doi.org/10.1093/rpc/1999rpc442){rel="external"} the Court
of Appeal emphasised that a two-step approach was necessary to determine
inventorship. One must first identify the inventive concept and then
determine who devised that concept.

### Identifying the inventive concept

### 7.12.1 {#ref7-12-1}

The need to keep in mind the inventive concept was also highlighted in
[Stanelco Fibre Optics Ltd's Applications \[2005\] RPC
15](https://doi.org/10.1093/rpc/2005rpc16){rel="external"}, where
Christopher Floyd QC, sitting as a Deputy Judge in the Patents Court,
commented "It is clear that a mechanistic, element by element approach
to inventorship will not produce a fair result. If A discloses a new
idea to B whose only suggestion is to paint it pink, B should not be a
joint inventor of a patent for A's product painted pink. That is because
the additional feature does not really create a new inventive concept at
all. The feature is merely a claim limitation, adequate to overcome a
bare novelty objection, but having no substantial bearing on the
inventive concept. Patent agents will frequently suggest claim
limitations, but doing so does not make them joint inventors. Some
stripping of a claim of its verbiage, may be necessary to determine the
inventive concept, and consequently the inventor." This statement was
approved by the Court of Appeal in Markem Corp v Zipher Ltd \[2005\] RPC
31. In the case of joint inventorship, the question is therefore whether
all parties are jointly responsible for devising the inventive concept.

### 7.12.2 {#ref7-12-2}

The inventive concept may reside in more than an idea and may encompass
the means of realisation of that idea ([Minnesota Mining & Manufacturing
Company v Birtles, Lovatt and Evode Ltd (BL
O/237/00](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F237%2F00&submit=Go+%BB){rel="external"}).
Where the invention consists of a combination of individually known
elements, the inventor is the person who in substance made the
combination rather than one who merely contributed to it ([Henry
Brothers (Magherafelt) Ltd v The Ministry of Defence and the Northern
Ireland Office \[1997\] RPC
693](https://doi.org/10.1093/rpc/1997rpc693){rel="external"}; whilst the
Court of Appeal disagreed with Jacob J on the facts of this case, it did
not disagree with the principle). In [Statoil ASA v University of
Southampton (BL
O/204/05](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F204%2F05&submit=Go+%BB){rel="external"}),
the hearing officer held that if the thrust of the disclosure is that
the invention covers a broad area, it would be wrong to determine
inventorship and entitlement solely by considering only a narrow subset
of that area.

### Determining who devised the inventive concept

### 7.12.3 {#ref7-12-3}

A person is not the inventor merely because they contributed to a
claim - their contribution has to be the formulation of the inventive
concept ([University of Southampton's Applications \[2005\] RPC
11](https://doi.org/10.1093/rpc/2005rpc11){rel="external"}). In this
case, Laddie J also held that devising an invention and providing an
enabling disclosure were two quite different things, and pointed out
that it is possible to make a good invention but to lose one's patent
for failure to make an enabling disclosure. The requirement to include
an enabling disclosure is concerned with teaching the public how the
invention works, not with devising the invention in the first place. On
the facts of this case, the Court of Appeal ([\[2006\] RPC
21](https://doi.org/10.1093/rpc/2006rpc21){rel="external"}) concluded
that all that was needed to get a patent was disclosure of the idea for
the invention (substituting electrostatic particles for magnetic
particles in an insect trap); from this idea the skilled person could
readily practice the invention without disclosure of a means of
enablement. However, Jacob LJ held obiter dictum that: "In the context
of entitlement to a patent a mere, non-enabling idea, is probably not
enough to give the patent for it to solely the deviser. Those who
contribute enough information by way of necessary enablement to make the
idea patentable would count as 'actual devisers', having turned what was
'airy-fairy' into that which is practical ... On the other hand those
who contribute no more than essentially unnecessary detail cannot on any
view count as 'actual devisers' as Laddie J rightly said, see his para.
\[45\]."

### 7.12.4 {#ref7-12-4}

[Stanelco Fibre Optics Ltd's Applications \[2005\] RPC
15](https://doi.org/10.1093/rpc/2005rpc16){rel="external"} demonstrated
that more than a theoretical proposal is required to be an "actual
deviser". An antecedent worker responsible for an initial prompt without
which the invention might never have been made but with no idea as to
whether it could actually be done or how it might be done could never be
an inventor. Christopher Floyd QC (sitting as Deputy Judge) continued in
this case: "But where the antecedent worker comes up with and
communicates an idea consisting of all of the elements in the claim,
even though it is just an idea at that stage, it seems to me that he or
she will normally, at the very least, be an inventor of the claim. What
US patent law calls 'reduction to practice' is not, it seems to me, a
necessary component of a valid claim to any entitlement."

### 7.12.5 {#section-9}

In [Statoil ASA v University of Southampton (BL
O/204/05](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F204%2F05&submit=Go+%BB){rel="external"}),
the hearing officer held that contributing information that cannot
really be said to have an owner - and that might include the knowledge
of an expert - may not be sufficient to justify a claim to entitlement.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 7(4)**
  Except so far as the contrary is established, a person who makes an application for a patent shall be taken to be the person who is entitled under subsection (2) above to be granted a patent and two or more persons who make such an application jointly shall be taken to be the persons so entitled.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 7.13 {#ref7-13}

s.72(2), s.37(4) is also relevant

Thus an applicant who has stated that they are the inventor or that they
derive a right from the inventor ([see 13.08 to
13.10](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-08))
is presumed prima facie to be entitled to be granted a patent. The
Office makes no attempt to question these assertions. If any person
wishes to dispute them and to claim that the patent should be granted to
them, either instead of or as well as, the present applicant they must
take action under s.8 or 37 as appropriate. In [University of
Southampton's Applications \[2005\] RPC
11](https://doi.org/10.1093/rpc/2005rpc11){rel="external"}, the Patents
Court held that in order to succeed in being added to an application the
claimants had to prove on the balance of probabilities that they had
made a relevant contribution to the inventive concept. But the
requirement to remove the defendants as inventors was greater, as the
claimants also had to overcome the presumption in s.7(4) and prove not
only that they devised the inventive concept or concepts but that the
named inventors contributed nothing of substance to any of them.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
