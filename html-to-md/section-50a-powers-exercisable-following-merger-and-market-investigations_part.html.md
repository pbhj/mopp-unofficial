::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 50A: Powers exercisable following merger and market investigations {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (50A.01 - 50A.05) last updated: April 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 50A.01 {#ref50A-01}

Section 50A was introduced by the Enterprise Act 2002, which came into
force on 20 June 2003. This replaced the provisions in section 51 for
Ministers to apply to the comptroller in response to reports of the
Competition and Markets Authority (previously known as the Competition
Commission) in relation to monopoly or merger situations operating
against the public interest. In April 2014, this section was amended by
the Enterprise and Regulatory Reform Act 2013 (Competition)
(Consequential, Transitional and Saving Provisions) Order 2014 to
reflect the change in name and responsibilities from the Competition
Commission to the Competition and Markets Authority.

### 50A.02 {#a02}

Section 50A allows the Competition and Markets Authority or the
Secretary of State to apply to the comptroller to take action following
a merger or market investigation to remedy, mitigate or prevent a
competition matter that cannot be dealt with in any other way under the
Enterprise Act. This application must involve conditions in licences
granted under a patent by its proprietor restricting the use of
invention by the licensee or the right of the proprietor to grant other
licences, or a refusal by the proprietor to grant licences on reasonable
terms.

### 50A.03 {#a03}

The Competition and Markets Authority must publish a notice describing
the nature of the proposed application, and must consider any
representations made within thirty days by persons likely to be
affected.

### 50A.04 {#a04}

If, following the public consultation, the Competition and Markets
Authority considers that the application should go ahead, section 50A(4)
enables the comptroller to cancel or amend any licence conditions, and
make an entry in the register to the effect that licences under the
patent are available as of right.

  -----------------------------------------------------------------------
   

  **Section 50A(1)**

  Subsection (2) below applies where --\
  (a) section 41(2), 55(2), 66(6), 75(2), 83(2), 138(2), 147(2), 147A(2)
  or 160(2) of, or paragraph 5(2) or 10(2) of Schedule 7 to, the
  Enterprise Act 2002 (powers to take remedial action following merger or
  market investigations) applies;\
  (b) the Competition and Markets Authority or (as the case may be) the
  Secretary of State considers that it would be appropriate to make an
  application under this section for the purpose of remedying, mitigating
  or preventing a matter which cannot be dealt with under the enactment
  concerned; and\
  (c) the matter concerned involves-\
  (i) conditions in licences granted under a patent by its proprietor
  restricting the use of the invention by the licensee or the right of
  the proprietor to grant other licences; or\
  (ii) a refusal by the proprietor of a patent to grant licences on
  reasonable terms.
  -----------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 50A(2)**
  The Competition and Markets Authority or (as the case may be) the Secretary of State may apply to the comptroller to take action under this section.
  ------------------------------------------------------------------------------------------------------------------------------------------------------

### 50A.05 {#a05}

PR part 7 is also relevant

The application should be made by filing (in duplicate) Form 2 and a
statement of grounds. This starts proceedings before the comptroller,
the procedure for which is discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05)).

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 50A(3)**
  Before making an application the Competition and Markets Authority or (as the case may be) the Secretary of State shall publish, in such manner as it or he thinks appropriate, a notice describing the nature of the proposed application and shall consider any representations which may be made within 30 days of such publication by persons whose interests appear to it or him to be affected.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 50A(4)**
  The comptroller may, if it appears to him on an application under this section that the application is made in accordance with this section, by order cancel or modify any condition concerned of the kind mentioned in subsection (1)(c)(i) above or may, instead or in addition, make an entry in the register to the effect that licences under the patent are to be available as of right.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 50A(5)**

  References in this section to the Competition and Markets Authority are
  references to a CMA group except where --\
  (a) section 75(2) of the Enterprise Act 2002 applies; or\
  (b) any other enactment mentioned in subsection (1)(a) above applies
  and the functions of the Competition and Markets Authority under that
  enactment are being performed by the CMA Board by virtue of section
  34C(3) or 133A(2) of the Enterprise Act 2002.
  -----------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 50A(6)**
  References in section 35, 36, 47, 63, 134, 141 or 141A of the Enterprise Act 2002 (questions to be decided by the Competition and Markets Authority in its reports) to taking action under section 41(2), 55, 66, 138, 147 or 147A shall include references to taking action under subsection (2) above.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 50A(7)**
  Action taken by virtue of subsection (4) above in consequence of an application under subsection (2) above where an enactment mentioned in subsection (1)(a) above applies shall be treated, for the purposes of sections 91(3), 92(1)(a), 162(1) and 166(3) of the Enterprise Act 2002 (duties to register and keep under review enforcement orders etc.), as if it were the making of an enforcement order (within the meaning of the Part concerned) under the relevant power in Part 3 or (as the case may be) 4 of that Act.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
