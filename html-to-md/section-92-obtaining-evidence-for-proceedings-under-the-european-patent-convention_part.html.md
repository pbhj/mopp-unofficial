::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 92: Obtaining evidence for proceedings under the European Patent Convention {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (92.01 - 92.08) last updated: July 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 92.01 {#ref92-01}

This section relates to the provision in the UK of evidence for
proceedings before a relevant convention court under the EPC, and
authorises rules for that purpose. The relevant convention court is that
court or other body which under the EPC has jurisdiction over the
proceedings in question, including (where it has such jurisdiction) any
department of the EPO.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 92(1)**
  Sections 1 to 3 of the Evidence (Proceedings in Other Jurisdictions) Act 1975 (provisions enabling United Kingdom courts to assist in obtaining evidence for foreign courts) shall apply for the purpose of proceedings before a relevant convention court under the European Patent Convention as they apply for the purpose of civil proceedings in a court exercising jurisdiction in a country outside the United Kingdom.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 92(2)**
  In the application of those sections by virtue of this section any reference to the High Court, the Court of Session or the High Court of Justice in Northern Ireland shall include a reference to the comptroller.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 92.02 {#section}

Appropriate provisions of the Evidence (Proceedings in Other
Jurisdictions) Act 1975 are thus applied to the UK courts (including the
comptroller, by virtue of s.92(2)) for the purpose of obtaining evidence
for such proceedings, [see 92.01](#ref92-01). The 1975 Act was extended
to the Isle of Man by SI 1979/1711.

  -----------------------------------------------------------------------
   

  **Section 92(3)**

  Rules under this Act may include provision\
  (a) as to the manner in which an application under section 1 of the
  said Act of 1975 is to be made to the comptroller for the purpose of
  proceedings before a relevant convention court under the European
  Patent Convention; and\
  (b) subject to the provisions of that Act, as to the circumstances in
  which an order can be made under section 2 of that Act on any such
  application.
  -----------------------------------------------------------------------

### 92.03 {#ref92-03}

r.62(1)& (2) is also relevant

An application to the comptroller under the Evidence (Proceedings in
Other Jurisdictions) Act 1975 for an order for evidence to be obtained
in the UK should be made in writing without notice. The application
should be supported by written evidence and accompanied by the
appropriate fee, the request as a result of which the application is
made, and a translation of the request into English where appropriate.

### 92.04 {#section-1}

After such an application has been made, an application for a further
order or directions in relation to the same matter may be made to the
comptroller in writing.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 92(4)**
  Rules of the court and rules under this Act may provide for an officer of the European Patent Office to attend the hearing of an application under section 1 of that Act before the court or the comptroller, as the case may be, and examine the witnesses or request the court or comptroller to put specified questions to the witnesses.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 92.05 {#section-2}

r.62(3) is also relevant

The comptroller may allow an officer of the EPO to attend the hearing of
such an application as is mentioned in [92.03](#ref92-03) and examine
the witnesses or request the comptroller to put specified questions to
them.

### 92.06 {#section-3}

Rule 34.21 of Part 34 of the Civil Procedure Rules contains an
equivalent provision with regard to an officer of the EPO where the
application is made to the court and an order is made for the
examination of witnesses.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 92(5)**
  Section 1(4) of the Perjury Act 1911 and article 3(4) of the Perjury (Northern Ireland) Order 1979 (statements made for the purposes, among others, of judicial proceedings in a tribunal of a foreign state) shall apply in relation to proceedings before a relevant convention court under the European Patent Convention as they apply to a judicial proceeding in a tribunal of a foreign state.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 92.07 {#section-4}

Appropriate provisions with regard to perjury are thus applied to
statements made in England, Wales and Northern Ireland for the purposes
of proceedings before a relevant convention court under the EPC. There
is no reference to Scotland in s.92(5) since perjury is a common law
offence in Scottish law.

### 92.08 {#section-5}

The reference to the Perjury (Northern Ireland) Order 1979 in subsection
(5) replaced a reference to the Perjury Act (Northern Ireland) 1946.
This amendment was effected by S.I. 1979 No. 1714.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
