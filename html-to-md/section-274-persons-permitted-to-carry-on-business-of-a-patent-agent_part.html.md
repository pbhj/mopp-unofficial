::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 274: Persons permitted to carry on business of a patent agent {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (274.01 - 274.07) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 274.01 {#ref274-01}

Part V of the CDP Act comprises sections 274 to 286 and deals with
patent attorneys and trade mark attorneys. It came into force on 13
August 1990 and completely replaced sections 84, 85, 104, 114, 115,
123(2)(k) and 130(1) (definition of "patent agent" only) of the Patents
Act 1977. Qualified patent attorneys had for many years enjoyed what was
effectively an exclusive right to represent patent applicants before the
Office. (Solicitors have also been entitled to do so, but have in fact
done so only rarely.) To practice, patent attorneys had to be entered on
a register which was maintained under the Patents Act 1977 (and
continues under s.275 of the CDP Act). Getting on to the register
involves the passing of examinations and serving a period of supervised
practice, and it has been a criminal offence for an unqualified person
to charge money for acting as a patent attorney, or even use the title.

### 274.02 {#section}

This part of the CDP Act allows increased competition among those
offering patent agency services by lifting restrictions on who may
represent patent applicants. It also improves the position of clients
seeking advice from qualified patent attorneys and trade mark attorneys
by providing that communications with these attorneys are privileged
from disclosure in legal proceedings just as communications with lawyers
are. The Legal Services Act 2007 amended various sections of this part
of the CDP Act on 1 January 2010.

### 274.03 {#ref274-03}

Section 274 to 281 relate to patent attorneys, sections 282 to 284 being
concerned with trade mark attorneys and sections 285 and 286 being
supplementary and applying to both classes of attorney. Section 274
abolishes the so-called "patent agents' monopoly" of rights of
representation of patent applicants in proceedings before the Office
which existed under the 1977 Act, [see 274.01](#ref274-01).

  -----------------------------------------------------------------------
   

  **Section 274(1)**

  Any individual, partnership or body corporate may, subject to the
  following provisions of this Part and to the Legal Services Act 2007,
  carry on the business of acting as agent for others for the purpose of
  ­\
  (a) applying for or obtaining patents, in the United Kingdom or
  elsewhere, or\
  (b) conducting proceedings before the comptroller relating to
  applications for, or otherwise in connection with, patents.
  -----------------------------------------------------------------------

### 274.04 {#section-1}

Any person (or partnership or company) may, subject to provisions
elsewhere in this Part, carry on the business of acting for others in
(a) applying for patents or (b) conducting proceedings before the
comptroller in connection with patents. The main effect is to replace
the requirement, in section 114(1) of the Patents Act 1977, that only
registered patent attorneys may practise as patent attorneys, that is
represent patent applicants for gain (contravention of this provision
was a criminal offence). Unregistered persons wishing to offer patent
agency services are now able to do so. The only constraints are that
they are unable to use the title "patent attorney" or similar
expressions (section 276) and the comptroller may refuse to deal with
them on certain limited grounds (chiefly misconduct) (section 281).

\[ Staff should be alert to the possibility that representatives who are
not registered patent attorneys may be less familiar with the patent
system than a registered patent attorney. Any agent should be given
assistance if needed and in this respect unregistered agents should be
treated in the same way as registered attorneys; however their cases
should not generally be given "private applicant" treatment unless the
unregistered agent requests this. \]

### 274.05 {#section-2}

r.101 is also relevant

Unless the comptroller otherwise directs in any particular case, all
attendances upon the comptroller may be made by or through an agent.
Further, every notice, application or other document filed under the
1977 Act may be signed by an agent. Where after a person has become a
party to proceedings before the comptroller they appoint an agent for
the first time or appoint one agent in substitution for another, the
newly appointed agent should make a declaration of authorisation. This
should be made on Patents Form 51 which should be filed (no fee) on or
before the first occasion when they act as agent. A copy of the Form 51
should be sent by the comptroller to the former agent.

### 274.06 {#section-3}

In accordance with s.102 of the Patents Act 1977, as amended by the CDP
Act and Legal Services Act 2007, a party to patent proceedings before
the comptroller may appear in person or be represented by any person of
their choice (subject to rules empowering the comptroller to refuse to
recognise certain agents). Thus a person acting as agent under s.274 may
also appear before the comptroller. However, under the Legal Services
Act 2007, only registered patent attorneys (see s.275) and legal
practitioners can appear on behalf of a party to an appeal from a
decision of the comptroller to the Patents Court.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 274(2)**
  This does not affect any restriction under the European Patent Convention as to who may act on behalf of another for any purpose relating to European patents.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------

### 274.07 {#section-4}

Section 274 does not affect the restrictions imposed by Article 134 of
the European Patent Convention (EPC) and rules made thereunder. These
restrictions provide that professional representation before the
European Patent Office can only be undertaken by those who names appear
on a list maintained for the purpose.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
