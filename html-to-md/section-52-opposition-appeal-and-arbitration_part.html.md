::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 52: Opposition, appeal and arbitration {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (52.01 - 52.10) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 52.01

The Patents and Trade Marks (World Trade Organisation) Regulations 1999
replaced the original section 52 with effect from 29 July 1999. This
section provides as before for opposition to applications under ss. 48
to 51, and for matters arising in such opposition proceedings to be
referred to an arbitrator by the comptroller. In addition, WTO
proprietors ([see
48.22](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-22))
may apply to have a compulsory licence or register entry cancelled if
the circumstances leading to the order or entry have ceased to exist and
are unlikely to recur. The section also makes special provision with
regard to appeals from decisions made by the comptroller on applications
under ss.48 to 51 or 52(2). Such applications may be for compulsory
licences or "licences of right" entries or modification of licences, as
outlined in
[48.01](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-01)
and
[51.01](/guidance/manual-of-patent-practice-mopp/section-51-powers-exercisable-in-consequence-of-report-of-competition-and-markets-authority/#ref51-01)
or cancellation of orders or register entries as outlined below.

### 52.02

These sections are members of a group (46 to 54) relating to licences of
right, compulsory licences etc. For general discussion of this group
[see
46.02-07](/guidance/manual-of-patent-practice-mopp/section-46-patentee-s-application-for-entry-in-register-that-licences-are-available-as-of-right/#ref46-02).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 52(1)**
  The proprietor of the patent concerned or any other person wishing to oppose an application under sections 48 to 51 above may, in accordance with rules, give to the comptroller notice of opposition; and the comptroller shall consider any opposition in deciding whether to grant the application.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Opposition

### 52.03 {#ref52-03}

Any person, including the patentee, may oppose an application under
ss.48 to 51. For example, the opponent was the exclusive licensee in
Montgomerie Reid's Application, [see
48.18](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-18)
(However, if the patentee does not lodge an opposition under s.52(1),
they are is not entitled to be heard in proceedings to settle the terms
of any licence, [see
48.13](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-13)).

### 52.04 {#ref52-04}

r.108(1) is also relevant

Notice of opposition under s.52(1) must be given within four weeks
(which cannot be extended) after the advertisement of the application in
the Journal as described [in
48.12](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-12).
The notice is given by filing a counter-statement in the proceedings in
accordance with rr.77(7)-(8).

### 52.05 {#section-2}

\[deleted\]

### 52.06 {#section-3}

s.52(5) is also relevant

The comptroller may refer matters to an arbitrator, [see
52.10](#ref52-10).

  -----------------------------------------------------------------------
   

  **Section 52(2)**

  Where an order or entry has been made under section 48 above in respect
  of a patent whose proprietor is a WTO proprietor-\
  (a) the proprietor or any other person may, in accordance with rules,
  apply to the comptroller to have the order revoked or the entry
  cancelled on the grounds that the circumstances which led to the making
  of the order or entry have ceased to exist and are unlikely to recur;\
  (b) any person wishing to oppose an application under paragraph (a)
  above may, in accordance with rules, give to the comptroller notice of
  opposition; and\
  (c) the comptroller shall consider any opposition in deciding whether
  to grant the application.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 52(3)**

  If it appears to the comptroller on an application under subsection
  (2)(a) above that the circumstances which led to the making of the
  order or entry have ceased to exist and are unlikely to recur, he may
  -\
  (a) revoke the order or cancel the entry; and\
  (b) terminate any licence granted to a person in pursuance of the order
  or entry subject to such terms and conditions as he thinks necessary
  for the protection of the legitimate interests of that person.
  -----------------------------------------------------------------------

### Revocation or cancellation of order or entry

### 52.07 {#ref52-07}

PR part 7 is also relevant

If the proprietor of the patent is a WTO proprietor, they or any other
person may apply to have an order or entry made under section 48 revoked
or cancelled on the grounds that the circumstances which led to the
making of the order or entry have ceased to exist and are unlikely to
recur. An application under s.52(2)(a) should be made on Form 2
accompanied by a copy thereof and a statement of grounds in duplicate.
This starts proceedings before the comptroller, the procedure for which
is discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).
It follows the same stages an application for an order or entry - [see
48.12-48.13](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-12)
mutatis mutandis.

### 52.08 {#section-4}

Under section 52(2)(b), and like an application under section 48, an
application under section 52(2)(a) may be opposed by anyone. The
procedure for opposition is the same as for an opposition under section
52(1) - [see
48.12](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-12)
[and 52.04](#ref52-04).

  -----------------------------------------------------------------------
   

  **Section 52(4)**

  Where an appeal is brought -\
  (a) from an order made by the comptroller in pursuance of an
  application under sections 48 to 51 above;\
  (b) from a decision of his to make an entry in the register in
  pursuance of such an application;\
  (c) from a revocation or cancellation made by him under subsection (3)
  above; or\
  (d) from a refusal of his to make such an order, entry, revocation or
  cancellation, the Attorney General,\
  the appropriate Law Officer within the meaning of section 4A of the
  Crown Suits (Scotland) Act 1857 or the Attorney General for Northern
  Ireland, or such other person who has a right of audience as any of
  them may appoint, shall be entitled to appear and be heard.
  -----------------------------------------------------------------------

### Appeal

### 52.09 {#section-5}

s.97(1) is also relevant

An appeal lies to the Patents Court from any decision of the comptroller
on an application under ss.48 to 51 or 52(2). Where such an appeal is
brought, the Attorney General (or the equivalent of that officer in
different parts of the UK) or such other person who has a right of
audience as they may appoint is entitled to appear and be heard.

  -----------------------------------------------------------------------
   

  **Section 52(5)**

  Where an application under sections 48 to 51 above or subsection (2)
  above is opposed, and either -\
  (a) the parties consent, or\
  (b) the proceedings require a prolonged examination of documents or any
  scientific or local investigation which cannot in the opinion of the
  comptroller conveniently be made before him,\
  the comptroller may at any time order the whole proceedings, or any
  question or issue of fact arising in them, to be referred to an
  arbitrator or arbiter agreed on by the parties or, in default of
  agreement, appointed by the comptroller.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 52(6)**
  Where the whole proceedings are so referred, unless the parties otherwise agree before the award of the arbitrator or arbiter is made, an appeal shall lie from the award to the court.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------
   
  **Section 52(7)**
  Where a question or issue of fact is so referred, the arbitrator shall report his findings to the comptroller.
  ----------------------------------------------------------------------------------------------------------------

### Arbitration

### 52.10 {#ref52-10}

Subsections (5) to (7) provide for reference to an arbitrator (arbiter
in Scotland) where an application under ss.48 to 51 or 52(2) is opposed.
Such a reference may be of the whole proceedings or of any question or
issue of fact arising therein, and may be made by the comptroller at any
time where the parties consent or the proceedings require examination or
investigation as detailed in subsection (5)(b). Unless the parties
previously agreed otherwise, an appeal lies from the award of the
arbitrator to the court. Where only a question or issue of fact is
referred, proceedings before the comptroller continue after the
comptroller receives the arbitrator's findings.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
