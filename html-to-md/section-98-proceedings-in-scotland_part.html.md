::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 98: Proceedings in Scotland {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (98.01 - 98.03-1) last updated: April 2008.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 98.01

This section concerns proceedings relating to patents (other than
proceedings before the comptroller) in Scotland.

### Section 98(1)

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 98(1)**
  In Scotland proceedings relating primarily to patents (other than proceedings before the comptroller) shall be competent in the Court of Session only, and any jurisdiction of the sheriff court relating to patents is hereby abolished except in relation to questions which are incidental to the issue in proceedings which are otherwise competent there.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 98.02

s.130(1) is also relevant

Court proceedings in Scotland relating primarily to patents may be in
the Court of Session only and not in the sheriff court. In the 1977 Act,
various powers are specifically given to the "court" which, as respects
Scotland, means the Court of Session (unless the context otherwise
requires). Powers in relation to appeals from decisions of the
comptroller in proceedings which under r.88 are held in Scotland, are
specifically given to the Court of Session by s.97(4) and (5), [see
97.10 and
97.11](/guidance/manual-of-patent-practice-mopp/section-97-appeals-from-the-comptroller/#ref97-10).
In addition, the Court of Session may order the Office to provide
reports, under s.99B. It should be noted that procedures before the
Court of Session are governed by the Act of Sederunt (Rules of Court of
Session) Rules 1994 (S.I. 1994 No.1443). Chapter 55 of these Rules,
which is relevant to intellectual property cases, was reproduced as
\[1995\] RPC 1.

### Section 98(2)

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 98(2)**
  The remuneration of any assessor appointed to assist the court in proceedings under this Act in the Court of Session shall be determined by the Lord President of the Court of Session with the consent of the Treasury and shall be defrayed out of moneys provided by Parliament.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 98.03

The reference to the "Treasury" in s.98(2) was substituted for a
reference to the "Minister for the Civil Service" by S.I. 1981 No.1670
arts.2(2), 3(5).

### 98.03.1

Subsection (2) lays down how any assessor assisting the Court of Session
in proceedings under the 1977 Act is remunerated.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
