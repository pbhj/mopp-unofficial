::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Citation formats {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated December 2015.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
Examiners at the Intellectual Property Office need to be consistent in
the way they cite patent documents and other prior art for any number of
clear reasons, for example:

1.  So there is no ambiguity over what is being referred to

2.  To ensure the correct documents are sent to the applicant

3.  To allow everyone to successfully retrieve the correct document in
    the future

4.  So that the information can be cleanly and predictably transferred
    to other systems

Until October 2013 the indented paragraphs below Manual of Patent
Practice (MoPP)
[17.75](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-75)
set out the detailed rules for formatting citations; this page
supersedes that MoPP information and so becomes best practice.

### Citing patent documents

Patent citations will be present on the search report in the form of the
published document's country code, publication number and document type
followed by name of applicant or patentee in capitals (possibly
abbreviated) in brackets. Passages of particular relevance or a summary
will follow, eg GB 2470828 A (FURUNO) See especially page 5 lines 8-14.
There is no need to include the date of publication, even where the
document is in the P or E category.

In general, patent documents should be entered on the search report in
the format that appears on the physical document, omitting any commas,
spaces, hyphens and full stops, but leaving a space between the country
code and number, and between the number and document type (e.g. A, B, U,
Y, C, etc). It is important to ensure the correct document type (also
known as the kind code) is included in the citation information. The
document publication number can be identified from the document's front
page as being labelled by INID code 11. Exceptions to this rule of thumb
are detailed below.

Citations are recorded directly within PROSE or using the Internal
Search Report form's Word Add-in. Following PDN 07/13, all patent
citations are entered using the "Patent Citation" option. Wherever
possible patent citations should be recorded as such, but due to
restrictions in the underlying OPTICS database some patent number
formats must be flagged for entry in the database as non-patent
literature (NPL) - PDN 07/13 para 6. The following paragraphs set out
special number format rules and where records should be flagged for
entry in OPTICS as NPL.

### Special number formats:

1.  GB patents with less than seven digits (i.e. pre-1949 Act patents
    published after 1915) should have leading zeros inserted to make up
    to seven digits in total as in the EPOQUE number format

2.  Irish application numbers should be entered in the format IE
    YYYYNNNNN inserting zeros after the date element to make 9 digits in
    total

3.  Japanese published applications from 2000 onward and references to
    grant numbers should be entered in the format that is used in EPOQUE
    but including a space between the country code and year
    indicator/number (see below)

### Requiring NPL flag:

1.  GB patents published before 1916 should be entered in the format
    that is used in EPOQUE but including a space between the country
    code and year indicator/number (see below), eg GB YYYYNNNNN

2.  Australian documents with a year in the publication number should be
    entered in the format that is used in EPOQUE but including a space
    between the country code and year indicator/number (see below), eg
    AU YYYYNNNNNN

3.  Japanese published applications before 2000 should be entered in the
    format that is used in EPOQUE but including a space between the
    country code and year indicator/number (see below), eg JP
    EYY\[nnnnn\]n where E is a letter indicating the emperor period
    (more information is available JP number formats)

4.  Korean documents should be cited using the format shown on the
    physical document removing any hyphens or spaces.

### Examples

The following table illustrates and contrasts the correct format for
entering citations on search reports in PROSE, and the formats appearing
in OPTICS and EPOQUE. The document type (e.g. A or B) should be added as
appropriate in the search report; if document types are not specifically
mentioned in this table it is an indication that the example may be
appropriate to a range of document types - as document type should
always be provided.

  ------------------ ----------------- ----------------- -----------------
                                                          

  **Physical         **Prose and       **Optics**        **Epoque**
  Document**         search reports**                    

  CN 1136129A\       CN 1136129 A      CN001136129       CN1136129
  (unexamined patent                                     
  application                                            
  pre-2007)                                              

  CN 101092954A\     CN 101092954 A    CN101092954       CN101092954
  (unexamined patent                                     
  application 2007                                       
  onward)                                                

  CN 1155431C\       CN 1155431 C      CN001155431       CN1155431C
  (granted patent                                        
  1993-2006)                                             

  CN 100580249C\     CN 100580249 C    CN100580249       CN100580249C
  (granted patent                                        
  2007 onward)                                           

  CN 2125873U\       CN 2125873 U      CN002125873       CN2125873U
  (utility model                                         
  pre-1993)                                              

  CN 2660120Y\       CN 2660120 Y      CN002660120       CN2660120Y
  (utility model                                         
  pre-2007)                                              

  CN 201080817Y\     CN 201080817 Y    CN 201080817      CN201080817Y
  (utility model                                         
  2007 onward)                                           

  DE 103 15 123      DE 10315123       DE010315123       DE10315123

  DE 10 2004 002 764 DE 102004002764   DE102004002764    DE102004002764

  EP 0 023 612       EP 0023612        EP0023612         EP0023612

  FR 52.648          FR 52648          FR000052648       FR52648

  GB 232, AD 1914\   GB 191400232\     GB 191400232\     GB191400232
  (pre-1916 GB doc)  (Tick "Patent     (Stored in NPL    
                     stored in NPL     field)            
                     field")                             

  GB 645,678         GB 0645678        GB0645678         GB645678

  GB 2 136 458       GB 2136458        GB2136458         GB2136458

  IE 1745/86 \*      IE 198601745      IE198601745       IE861745

  IE 922616 \*       IE 199202616      IE199202616       IE922616

  IE 040006 \*       IE 200400006      IE200400006       IE20040006

  IE 2008/0133 \*    IE 200800133      IE200800133       IE20080133

  JP 63-123456\      JP S63123456\     JP S63123456\     JPS63123456
  (Showa period;     (Tick "Patent     (Stored in NPL    
  before April 1989) stored in NPL     field)            
                     field")                             

  JP 5-132\          JP H05132\        JP H05132\        JPH05132
  (Heisei period;    (Tick "Patent     (Stored in NPL    
  April 1989 onward) stored in NPL     field)            
                     field")                             

  JP 5669120B        JP 5669120 B\     JP5669120 B\      JP5669120B
                     (Tick "Patent     (Stored in NPL    
                     stored in NPL     field)            
                     field")                             

  JP 2000-194710\    JP 2000194710     JP2000194710      JP2000194710
  (from 2000)                                            

  JP 2003-5005       JP 2003005005     JP2003005005      JP2003005005

  R 1996-0010715\    KR 19960010715 A\ KR 19960010715 A\ KR960010715
  (unexamined patent (Tick "Patent     (Stored in NPL    
  application        stored in NPL     field)            
  pre-2000)          field")                             

  KR2001-0091419\    KR 20010091419 A\ KR 20010091419 A\ KR20010091419
  (unexamined patent (Tick "Patent     (Stored in NPL    
  application        stored in NPL     field)            
  2000-2003)         field")                             

  KR                 KR 1020060100346  KR 1020060100346  KR20060100346
  10-2006-0100346\   A\                A\                
  (unexamined patent (Tick "Patent     (Stored in NPL    
  application 2004   stored in NPL     field)            
  onward)            field")                             

  KR 10-0765938\     KR 100765938 B    KR 100765938 B    KR100765938B
  (granted patent                                        
  2000 onward)                                           

  KR 96-17966\       KR 9617966 U      KR 9617966 U      KR960017966U
  (utility model                                         
  application                                            
  pre-2000)                                              

  KR 2001-0002210\   KR 20010002210 U\ KR 20010002210 U\ KR20010002210U
  (utility model     (Tick "Patent     (Stored in NPL    
  application        stored in NPL     field)            
  2000-2003)         field")                             

  KR                 KR 2020060000068  KR 2020060000068  KR20060000068U
  20-2006-0000068\   U\                U\                
  (utility model     (Tick "Patent     (Stored in NPL    
  application 2004   stored in NPL     field)            
  onward)            field")                             

  KR 20-0237149\     KR 200237149 Y    KR 200237149 Y    KR200237149Y
  (utility model                                         
  pre-2004)                                              

  US 4,539,814       US 4539814        US4539814         US4539814

  US 2002/0023230    US 2002/0023230   US20020023230     US2002023230

  US 10,120,240\*    US10120240\       US10120240\       US10120240
                     (Tick "Patent     (Stored in NPL    
                     stored in NPL     field)            
                     field")                             

  WO 88/31536        WO 88/31536       WO1988/031536     WO8831536

  WO 03/061360       WO 03/061360      WO2003/061360     WO03061360

  WO 2004/012345     WO 2004/012345    WO2004/012345     WO2004012345
  ------------------ ----------------- ----------------- -----------------

### Citing abstracts

It can be assumed that the search examiner will always have available a
copy of the source document as well as the abstract, particularly when
the document in question is a patent specification. So if the source
document is not in English at least the abstract is to be copied to the
applicant (see 17.104.1), the search report should be completed as below
if the examiner considers it desirable or useful to identify the
abstract as well as the source document.

Example if citing the WPI abstract: SU 1386135 A (Belo) See Figure 2,
and also WPI Abstract Accession No. 1988-298325.

Example if citing the EPODOC abstract: CN 103012102 A (CHINA PETROLEUM &
CHEMICAL) See figures, and EPODOC abstract.

### Citing foreign language documents

The examiner may, if he wishes, indicate a publically available online
source of machine translation of a foreign language citation.

Examples: JP nnnnnnn A (Name). Machine translation [available
from](http://aipn.ipdl.inpit.go.jp/AI2/cgi-bin/AIPNSEARCH){rel="external"},
\[Accessed date\].

KR nn-nnnnnnn A (Name). Machine translation is
[available](http://k2epat.kipris.or.kr/k2epat/searchLogina.do?next=MainSearch){rel="external"},
\[Accessed date\].

### Citing books and other separately issued publications (eg conference proceedings)

Books and other separately issued publications, such as conference
proceedings, and individual web pages (for further details see the
Internet Searching Manual) entered sequentially separated by commas or
semicolons:

Document box: Author or Editor, Title, Year of Publication, Publisher,

Additional information: Relevant passages

For documents obtained through the Web, authorship should be ascribed to
the smallest identifiable organisational unit, and the publisher can
refer to the organisation maintaining the Internet site. The URL of the
site and the date accessed should also be included.

### Example 1

Document box: H Walton, "Microwave Quantum Theory", published 1973,
Sweet and Maxwell. Additional information: see especially pages 146 to
148

### Example 2

Document box: Y Kim et al, "IEEE Engineering in Medicine & Biology
Society 11th Annual Conference", published 1989, IEEE, pp 1744-5, Vol 6,
Smith et al, "Digital demodulator for electrical impedance imaging"

Additional information:

### Example 3

Document box: M Holland, "Guide to citing references" \[online\],
published 2002, Bournemouth University. [Available
from](http://www.bath.ac.uk/library/help/infoguides/references.pdf){rel="external"},
\[Accessed 22 Oct 2005\].

Additional information:

### Citing periodicals and journal articles

Articles in periodicals entered sequentially separated by commas or
semicolons: Document box: Title of periodical, Volume, Date of issue,
Place of publication, Author, Title of article, Page numbers where the
article begins and ends

Additional information: Relevant passages

### Example 1

Document box: IBM Technical Disclosure Bulletin Vol. 17, No. 5, October
1974 (New York), J G Drop, "Integrated Circuits", pages 1344 to 1348

Additional information: especially page 1345.

### Example 2

Document box: Applied Physics Letters \[online\] Vol. 83, No. 17, 27
October 2003, A Sharma et al, "Instability and dynamics of thin slipping
films", [is
available](http://scitation.aip.org/content/aip/journal/apl/83/17/10.1063/1.1618376){rel="external"}
\[Accessed 28 November 2013\] pages 3549 to 3551.

Additional information: especially page 3550.

### Citing webpages, online discussions and bulletins

Reference to webpages entered sequentially separated by commas or
semicolons:

Document box: Author, date, page title, name of website/company,
\[online\], Available from: URL \[Accessed date\]

Additional information: relevant passages

As noted above, for documents obtained through the Web, authorship
should be ascribed to the smallest identifiable organisational unit.

### Example 1:

Document box: Celbius Ltd, 2012, "Sonobioprocessing", Celbius.com,
\[online\], [Available
from](http://www.celbius.com/index.php/sonobioprocessing){rel="external"},
\[Accessed 29 August 2013\].

Additional information: See figure

Reference to contributions to online discussion lists and bulletin
boards entered sequentially separated by commas or semicolons:

Document box: Author, date, subject of message, name of discussion list,
\[online\], Available from: list e-mail address or URL, \[Accessed
date\]

Additional information: relevant passages

### Example 2:

Document box: E V Brack, 2 May 1995, "Re: Computing short courses",
Lis-link \[online\]. [Available from](mailbase@mailbase.ac.uk)
\[Accessed 17 April 1996\].
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
