::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 70A: Actionable threats {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (70A.01-70A.08) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 70A.01 {#ref70A-01}

A communication that satisfies the test at section 70 will be a threat
to sue for infringement, and the threats provisions may be engaged in
respect of it. However, not all threats to sue for infringement will
give rise to a right to sue under these provisions. Section 70A sets out
which types of threats are actionable by a person aggrieved, i.e. those
threats where the person aggrieved can sue the person who made the
threat.

### Section 70A(1)

Subject to subsections (2) to (5), a threat of infringement proceedings
made by any person is actionable by any person aggrieved by the threat.

### 70A.02 {#a02}

The person making the threat need not have any right in a patent, nor
need the threat have been made to the claimant for the threat to be
actionable. However, the claimant is required to satisfy the court that
such threats were made and that they are a person aggrieved by them, in
order that the threat is actionable.

### 70A.03 {#a03}

Anyone whose commercial interests have, or might be, affected by the
threat in a real rather than fanciful way may sue as a person aggrieved
by the threat. See Brain v Ingledew Brown Bennison and Garrett (No 3)
\[1997‐98\] Info TLR 329, \[1997\] FSR 511 at 520 (decided under the
previous threats provisions). There are some exceptions which prevent a
person aggrieved from suing the person who made the threat (\[see
70A.04-70A.08(/guidance/manual-of-patent-practice-mopp/section-70-remedy-for-groundless-threats-of-infringement-proceedings-#ref70-04)\]).

### Section 70A(2)

A threat of infringement proceedings is not actionable if the
infringement is alleged to consist of--- (a) where the invention is a
product, making a product for disposal or importing a product for
disposal, or (b) where the invention is a process, using a process.

### 70A.04 {#a04}

Section 70A(2) sets out a "primary act" exception. A threat will not be
actionable if it is a threat to bring proceedings for an infringement
alleged to consist of an act of primary infringement. Thus a threats
action cannot be brought against the person who made such a threat. A
primary act is the making or importing of a patented product for
disposal or the use of a patented process.

### Section 70A(3)

A threat of infringement proceedings is not actionable if the
infringement is alleged to consist of an act which, if done, would
constitute an infringement of a kind mentioned in subsection (2)(a) or
(b).

### 70A.05 {#a05}

Since a threat can be made in respect of an intended or future act ([see
70.03](/guidance/manual-of-patent-practice-mopp/section-70-remedy-for-groundless-threats-of-infringement-proceedings-#ref70-03)),
section 70A(3) makes clear that the "primary act" exception also applies
to such acts -- that is, to acts of primary infringement which have not
yet been done. Thus a threat alleging that an act, if done, would
constitute infringement is not actionable if the act referred to is a
primary act.

### Section 70A(4)

A threat of infringement proceedings is not actionable if the threat---
(a) is made to a person who has done, or intends to do, an act mentioned
in subsection (2)(a) or (b) in relation to a product or process, and (b)
is a threat of proceedings for an infringement alleged to consist of
doing anything else in relation to that product or process.

### 70A.06 {#a06}

A threat made to a primary actor is not actionable. A person who has
done or intends to do a primary act ([see 70A.04](#ref70A-04)) in
relation to a product (or process) to which the threat relates cannot
bring a threats action even if the threat refers to other acts which are
not primary acts. This allows threats made to primary actors to extend
to refer to secondary acts (such as selling the product in question)
done by that primary actor. So, for example, a threat to sue a
manufacturer for making an allegedly-infringing product may also extend
to threatening in relation to acts of selling the product they have
manufactured. This is an exception to the general principle that threats
made to secondary actors are actionable.

### 70A.07 {#a07}

Importantly, this exception only applies where the mentioned secondary
act is in relation to the same product or process as the primary act. So
if a person produces and sells a product, a threat to sue for
infringement for both producing and selling the product will not be
actionable. But if the same person also sells an equivalent product
produced by someone else, a threat to sue for infringement for selling
that equivalent product will be actionable since, in this respect, the
person being threatened is a pure secondary actor.

### Section 70A(5)

A threat of infringement proceedings which is not an express threat is
not actionable if it is contained in a permitted communication.

### 70A.08 {#ref70A-08}

A threat, as determined by the test of section 70(1) ([see
70.03-70.04](/guidance/manual-of-patent-practice-mopp/section-70-remedy-for-groundless-threats-of-infringement-proceedings/#ref70-03),
is not actionable if it is contained in a "permitted communication",
provided that it is not an express threat to sue. Section 70B defines
what is meant by a permitted communication ([see
70B.02-70B.03](/guidance/manual-of-patent-practice-mopp/section-70b-permitted-communications/#ref70B-02)).

### Section 70A(6)

In sections
[70C](/guidance/manual-of-patent-practice-mopp/section-70c-remedies-and-defences)
and
[70D](/guidance/manual-of-patent-practice-mopp/section-70d-professional-advisers)
"an actionable threat" means a threat of infringement proceedings that
is actionable in accordance with this section.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
