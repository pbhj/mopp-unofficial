::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 23: Restrictions on applications abroad by United Kingdom residents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (23.01 - 23.08) last updated: April 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------
   

  **Section 23(1)**

  Subject to the following provisions of this section, no person resident
  in the United Kingdom shall, without written authority granted by the
  comptroller, file or cause to be filed outside the United Kingdom an
  application for a patent for an invention if subsection (1A) below
  applies to that application, unless -\
  (a) an application for a patent for the same invention has been filed
  in the Patent Office (whether before, on or after the appointed day)
  not less than six weeks before the application outside the United
  Kingdom; and\
  (b) either no directions have been given under section 22 above in
  relation to the application in the United Kingdom or all such
  directions have been revoked.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 23(1A)**

  This subsection applies to an application if -\
  (a) the application contains information which relates to military
  technology or for any other reason publication of the information might
  be prejudicial to national security; or\
  (b) the application contains information the publication of which might
  be prejudicial to the safety of the public.
  -----------------------------------------------------------------------

### 23.01

S.23 refers to residence and not to citizenship or nationality. If a
person normally resident in the United Kingdom lives abroad for a period
of several months, they will be regarded for the purposes of s.23 as
having ceased to be a United Kingdom resident during this period. On the
other hand, a person normally resident abroad but temporarily resident
in the United Kingdom or a person who is not a United Kingdom citizen
but has a residential address here is considered to be subject to s.23.
Any United Kingdom resident temporarily travelling abroad is considered
to be bound by the requirements of s.23 during their travels.
Furthermore a United Kingdom resident employed by a foreign organisation
is subject to s.23 irrespective of any term of their employment contract
requiring an initial foreign filing of a patent application relating to
an invention arising out of such employment. Additionally even when a
United Kingdom resident is a joint inventor with a foreign resident or
seeks to be a joint applicant therewith in relation to a foreign
application, the requirements of s.23 should be complied with.

### 23.01.1 {#ref23-01-1}

According to the Interpretation Act 1978, "person" includes a body of
persons corporate or unincorporated. Thus the "person" referred to in
s.23(1) includes not only the inventor who must be a natural person or
persons, but also the applicant which could be a company. The words
"file or cause to be filed" mean that this person could also be an agent
responsible for preparing a patent application for first filing outside
the United Kingdom.

### 23.02 {#ref23-02}

Subsection (1A) was inserted by the Patents Act 2004 and came into force
on 1 January 2005. The strictures of s.23(1) only apply to applications
that contain information relating to military technology or other
information whose publication might be prejudicial to national security
or the safety of the public. A UK resident who wishes to file such an
application abroad must therefore either file an application at the
Office and then wait six weeks (after which, provided no direction has
been given under s.22 ([see
22.03](/guidance/manual-of-patent-practice-mopp/section-22-information-prejudicial-to-national-security-or-safety-of-public/#ref22-03)),
applications may be made abroad without further formality) or else must
have written permission from the comptroller. Persons wanting such
permission should apply direct to Room G.R70, Cardiff Road, Newport,
South Wales, NP10 8QQ either by letter or, if more urgent attention is
required, by email to <screening@ipo.gov.uk>. A notice drawing attention
to these matters appears prominently in every issue of the Patents
Journal.

### 23.03 {#ref23-03}

An offence may be committed if, notwithstanding that an application has
been filed at the Office which is not the subject of prohibition
directions (whether because it did not attract them or they have been
revoked), a subsequent application is made abroad based upon but
containing matter not disclosed in the application as filed at the
Office. It should be ensured, therefore, that any such subsequent
application does not contain additional descriptive matter where
s.23(1A) applies without written prior authority from the comptroller
([see
22.16](/guidance/manual-of-patent-practice-mopp/section-22-information-prejudicial-to-national-security-or-safety-of-public/#ref22-16)).

### 23.04 {#ref23-04}

Even when an application is the subject of a prohibition direction it
may be permitted to form the basis of applications in foreign countries
having reciprocal arrangements with this country. An example of such an
arrangement is the NATO "Agreement for the mutual safeguarding of
secrecy of inventions relating to defence and for which applications for
patents have been made" (Cmnd 1595). Details of the very special
procedures for such conversions and foreign filings will be provided
after permission has been granted to file abroad. Such requests should
be sent directly to Room G.R70, Concept House, Cardiff Road, Newport,
South Wales, NP10 8QQ.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 23(2)**
  Subsection (1) above does not apply to an application for a patent for an invention for which an application for a patent has first been filed (whether before or after the appointed day) in a country outside the United Kingdom by a person resident outside the United Kingdom.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 23.05 {#section-1}

Thus where an application has been filed abroad by a person [(see
23.01.1)](#ref23-01-1) who is not a UK resident, further applications
for the same invention may be filed in other countries by a UK resident
without the prior approval of the comptroller.

  -----------------------------------------------------------------------
   

  **Section 23(3)**

  A person who files or causes to be filed an application for the grant
  of a patent in contravention of this section shall be liable -\
  (a) on summary conviction, to a fine not exceeding the prescribed sum;
  or\
  (b) on conviction on indictment, to imprisonment for a term not
  exceeding two years or a fine, or both.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 23(3A)**

  A person is liable under subsection (3) above only if -\
  (a) he knows that filing the application, or causing it to be filed,
  would contravene this section; or\
  (b) he is reckless as to whether filing the application, or causing it
  to be filed,would contravene this section.
  -----------------------------------------------------------------------

### 23.06 {#ref23-06}

It should be noted that a failure to comply with the provisions of
s.23(1) (or with a direction given under s.22) is a criminal offence.
However, s.23(3A) limits culpability of the offence to where a person
knows that filing an application or causing it to be filed would
contravene s.23, or where they are reckless as to whether filing the
application or causing it to be filed would contravene this section.
Therefore, a person acting in good faith who mistakenly believes that
the restrictions in s.23 do not apply to a patent application will not
be guilty of a criminal offence.

### 23.07 {#section-2}

The maximum fine on summary conviction corresponds to that applicable
under s.22(9) ([see
22.26.1](/guidance/manual-of-patent-practice-mopp/section-22-information-prejudicial-to-national-security-or-safety-of-public/#ref22-26-1).
The reference to "indictment" in (b) is treated as a reference to
"information" for the Isle of Man only (S.I. 2003 No. 1249).

  -----------------------------------------------------------------------
   

  **Section 23(4)**

  In this section -\
  (a) any reference to an application for a patent includes a reference
  to an application for other protection for an invention;\
  (b) any reference to either kind of application is a reference to an
  application under this Act, under the law of any country other than the
  United Kingdom or under any treaty or international convention to which
  the United Kingdom is a party.
  -----------------------------------------------------------------------

### 23.08 {#section-3}

Thus no application for a utility model or any other form of protection
of an invention may be sought abroad, either under the laws of another
country or under the EPC or the PCT, without first complying with the
requirements of s.23(1) ([see 23.02](#ref23-02)). UK residents wishing
to file applications under the EPC or PCT without first filing a UK
application can meet the requirements of s.23(1) by filing their
applications at the Office in its capacity as Receiving Office under
these treaties. If, however, they wish to file at any other Receiving
Office and s.23(1A) applies to the application, they must seek the prior
written approval of the comptroller.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
