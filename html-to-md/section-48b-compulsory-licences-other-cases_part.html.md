::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 48B: Compulsory licences: other cases {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (48B.01 - 48B.17) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 48B.01 {#b01}

s.48B(3) is also relevant

This section provides the grounds for application under section 48 when
the proprietor of the patent is a non-WTO proprietor and conditions
specific to such applications. Section 48B was introduced by regulation
5 of the Patents and Trade Marks (World Trade Organisation) Regulations
1999. which took effect from 29 July 1999. The provisions for the
non-WTO proprietor case are substantially unchanged from those in the
original Act; although there are now explicit provisions to the effect
that licences or register entries are not available because UK demand is
being met by importation from a member State of the EEA, rather than
production in the UK. Previously, section 53(5) would have required that
the comptroller should not make any order or entry in that case.

### 48B.02 {#b02}

In this section, as with the remainder of the Act, the term "member
State" is deemed, under the European Economic Area Act 1993, to refer to
a member State of the European Economic Area.

  -----------------------------------------------------------------------
   

  **Section 48B(1)**

  In the case of an application made under section 48 above in respect of
  a patent whose proprietor is not a WTO proprietor, the relevant grounds
  are-\
  (a) where the patented invention is capable of being commercially
  worked in the United Kingdom, that it is not being so worked or is not
  being so worked to the fullest extent that is reasonably practicable;
  -----------------------------------------------------------------------

### Grounds for application (non-WTO proprietor)

### 48B.03 {#ref48B-03}

s.130(1), s.48B(3) is also relevant

The first ground on which an application may be made applies where a
patented invention is capable of being commercially worked in the UK. By
"patented invention" is meant an invention for which a patent is
granted. The hearing officer in Enviro-Spray Systems Inc's Patents
\[1986\] RPC 147 declined to attempt a formal definition of "commercial
working" but could "see no reason for departing from its plain and
ordinary meaning. Thus, as far as manufactures are concerned, the
expression is ... clearly satisfied by the straightforward manufacture
of goods for the purposes of trade". He also observed that the wording
of the then s.48(3)(a), equivalent to present s.48B(1)(a), "seems to
recognise that working can be commercial and yet not be exploiting the
invention to the full" so that manufacture which is on a scale too small
to fully meet demand can still constitute commercial working. An
invention which is being commercially worked abroad is normally regarded
as capable of being commercially worked in the UK. However, no order or
entry may be made if demand is being met by importation from a member
State of the EEA where the invention is being worked.

### 48B.04 {#ref48b-04}

The onus is on the applicant to show that the invention is not being
commercially worked in the UK or is not being so worked to the fullest
extent that is reasonably practicable. In Kamborian's Patent \[1961\]
RPC 403, the hearing officer considered that the fullest extent to which
an invention may be worked may be expressed as "the highest rate of
production which is practicable and necessary substantially to meet the
demand". In order to be successful, the applicant must "bring evidence
to show what the demand for the invention might reasonably be expected
to be, and how far short, if at all, production under the patent falls,
as far as is practicable to supply it". Demand for a product which
merely includes the patented invention does not necessarily equate with
demand for the patented invention per se; the contribution which the
invention makes to the value of, or demand for, the product may need to
be considered [Quantel's Patents, BL
O/128/90](https://www.gov.uk/government/publications/patent-decision-o12890).

### 48B.05 {#ref48B-05}

A previous and discontinued commercial working of the invention is no
defence against an application on this ground (Cathro's Application 51
RPC 475; Gebhardt's Patent, [see 48B.17](#ref48b-17)). Unlikelihood of
making a profit or lack of demand are also insufficient defences. If in
fact there is a demand and manufacture in foreign countries, the
patentee must make an effort to create a demand in this country.

### 48B.06 {#b06}

s\. 8B(2) is also relevant

The comptroller may by order adjourn an application on this ground if it
appears to them that the time since grant of the patent has for any
reason been insufficient to allow the invention to be commercially
worked, [see 48B.17](#ref48b-17). The adjournment is for such period as
will in their opinion allow such working.

  -----------------------------------------------------------------------
   

  **Section 48B(1)(b)**

  where the patented invention is a product, that a demand for the
  product in the United Kingdom-\
  (i) is not being met on reasonable terms, or\
  (ii) is being met to a substantial extent by importation from a country
  which is not a member State;
  -----------------------------------------------------------------------

### 48B.07 {#ref48B-07}

This ground applies where the patented invention is a "product", which
in this context apparently does not include a product obtained by means
of a patented process or to which a patented process has been applied.
The ground is concerned with meeting a demand for the product in the UK.
The demand must be an actual one and not merely one which an applicant
hopes and expects to create if and when they have obtained a licence and
commenced business (Cathro's Applications 51 RPC 75).

### 48B.08 {#ref48b-08}

The applicant needs to show that such a demand is not being met on
reasonable terms or is being met to a substantial extent by importation.
What constitutes "reasonable terms" depends on a careful consideration
of all the surrounding circumstances in each case, eg the nature of the
invention, the terms of any licences under the patent, the expenditure
and liabilities of the patentee in respect of the patent, and the
requirements of the purchasing public. The price charged by the patentee
should be a bona fide one and not one adopted to suppress or depress
demand.

  -----------------------------------------------------------------------
   

  **Section 48B(1)(c)**

  where the patented invention is capable of being commercially worked in
  the United Kingdom, that it is being prevented or hindered from being
  so worked-\
  (i) where the invention is a product, by the importation of the product
  from a country which is not a member State,\
  (ii) where the invention is a process, by the importation from such a
  country of a product obtained directly by means of the process or to
  which the process has been applied;
  -----------------------------------------------------------------------

### 48B.09 {#b09}

Like that of s.48B(1)(a), this ground applies where the patented
invention is capable of being commercially worked in the UK (as
discussed in [48B.03](#ref48B-03)). The applicant needs to show that
such working is being prevented or hindered by importation of the
patented product from a country outside the EEA. This applies both where
the invention is the product and where the invention is a process by
which the product is directly obtained or which has been applied to the
product.

  -----------------------------------------------------------------------
   

  **Section 48B(1)(d)**

  that by reason of the refusal of the proprietor of the patent to grant
  a licence or licences on reasonable terms-\
  (i) a market for the export of any patented product made in the United
  Kingdom is not being supplied, or\
  (ii) the working or efficient working in the United Kingdom of any
  other patented invention which makes a substantial contribution to the
  art is prevented or hindered, or\
  (iii) the establishment or development of commercial or industrial
  activities in the United Kingdom is unfairly prejudiced;
  -----------------------------------------------------------------------

### 48B.10 {#ref48B-10}

This ground concerns various consequences of the refusal of the
proprietor of the patent to grant a licence on "reasonable terms". This
may be a refusal to grant a licence at all or an offer to grant a
licence but on terms which are unreasonable despite discussion in an
effort to agree terms (Loewe Radio Co Ltd's Applications 46 RPC 479).
This ground is not applicable to the refusal of an existing exclusive
licensee to grant a licence on such terms (Colbourne Engineering Co
Ltd's Application 72 RPC 169).

### 48B.11 {#ref48B-11}

With regard to the meaning of "reasonable terms", [see
48.18](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-18)
and [48B.08](#ref48b-08). In Brownie Wireless Co Ltd's Applications 46
RPC 457, the court considered the best test of whether a royalty is
reasonable is: How much are manufacturers who are anxious to make and
deal with the patented article on commercial lines ready and willing to
pay? The court also held that it can in certain circumstances be
reasonable to require licensees to take a licence under all patents
belonging to a group rather than under an individual patent from the
group, or to include terms requiring royalties to be paid on certain
non-patented articles. In Monsanto's CCP Patent \[1990\] FSR 93, it was
held that an offer of a licence covering a number of countries worldwide
in return for a fully paid-up royalty of 1 million US dollars did not
constitute a refusal to grant a licence on reasonable terms.

### 48B.12 {#ref48B-12}

s.130(1) is also relevant

The applicant here needs to show any of three things, firstly that a
market for the export of any patented product made in the UK is not
being supplied. (A "patented product" means a product which is a
patented invention or, in relation to a patented process, a product
obtained directly by means of the process or to which the process has
been applied. It may not be appropriate to regard a system which merely
includes the subject matter of a patent as a "patented product"
(Quantel's Patents, see also the last sentence of [48B.04](#ref48b-04)).
This condition can only be met if there is actual manufacture in being
in the UK. If the patentee is manufacturing in the UK and exporting to
certain foreign countries, it might well be reasonable for them to ask
that any compulsory licence granted should be restricted so as to
prevent export by the licensee to those countries (Penn Engineering &
Manufacturing Corp's Patent \[1973\] RPC 233).

### 48B.13 {#b13}

s.48B(4) is also relevant

Further to the preceding paragraph, the Act in fact specifically
provides that any licence granted on the unsupplied export market ground
should, at the discretion of the comptroller, restrict the countries in
which the product in question may be disposed of or used by the
licensee. In addition, "licences of right" entries cannot be made on
that ground.

### 48B.14 {#ref48B-14}

s.48B(5) is also relevant

The second condition is that the working or efficient working in the UK
of any other patented invention which makes a "substantial contribution
to the art" is prevented or hindered. However, the patentee of the other
invention must be able and willing to grant to the patentee and their
licensees a licence in respect of the other invention on reasonable
terms.

### 48B.15 {#ref48b-15}

The final condition under s.48B(1)(d) is that the establishment or
development of commercial or industrial activities in the UK is unfairly
prejudiced. An increase in the size of a business is regarded as
sufficient to constitute such development (Kamborian's Patent \[1961\]
RPC 403).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 48B(1)(e)**
  that by reason of conditions imposed by the proprietor of the patent on the grant of licences under the patent, or on the disposal or use of the patented product or on the use of the patented process, the manufacture, use or disposal of materials not protected by the patent, or the establishment or development of commercial or industrial activities in the United Kingdom, is unfairly prejudiced.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 48B.16 {#ref48b-16}

s.49(1) is also relevant

This last ground concerns conditions imposed by the patentee which
unfairly prejudice (a) the manufacture, use or disposal of materials not
protected by the patent, or (b) the establishment or development of
commercial or industrial activities in the UK ([see 48B.15](#ref48b-15)
re the latter). Where (a) is held to be unfairly prejudiced, the
comptroller may order the grant of licences to customers of the
applicant as well as to the applicant.

  -----------------------------------------------------------------------
   

  **Section 48B(2)**

  Where-\
  (a) an application is made on the ground that the patented invention is
  not being commercially worked in the United Kingdom or is not being so
  worked to the fullest extent that is reasonably practicable; and\
  (b) it appears to the comptroller that the time which has elapsed since
  the publication in the journal of a notice of the grant of the patent
  has for any reason been insufficient to enable the invention to be so
  worked,\
  \
  he may by order adjourn the application for such period as will in his
  opinion give sufficient time for the invention to be so worked.
  -----------------------------------------------------------------------

### 48B.17 {#ref48B-17}

Section 48B(2) provides for an application on the ground mentioned in
s.48B(1)(a) to be stayed in order to give sufficient time for the
invention to be commercially worked in the UK or so worked to the
fullest extent that is reasonably practical. In Gebhardt's Patent
\[1992\] RPC 1 the hearing officer (upheld on appeal) considered that
the then s.48(5), equivalent to the present s.48B(2), was intended
primarily, though not exclusively, for the situation where relatively
little time had elapsed since the grant of the patent. He declined to
stay the proceedings, having in mind the age of the patent (eleven years
since grant), the length of the hiatus in exploitation (some five years)
since earlier exploitation ceased and the absence of clear evidence of
sales in the period (seventeen months) since a new UK distributor was
appointed.

  -----------------------------------------------------------------------
   

  **Section 48B(3)**

  No order or entry shall be made under section 48 above in respect of a
  patent on the ground mentioned in subsection (1)(a) above if-\
  (a) the patented invention is being commercially worked in a country
  which is a member State; and\
  (b) demand in the United Kingdom is being met by importation from that
  country.
  -----------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 48B(4)**
  No entry shall be made in the register under section 48 above on the ground mentioned in subsection (1)(d)(i) above, and any licence granted under section 48 above on that ground shall contain such provisions as appear to the comptroller to be expedient for restricting the countries in which any product concerned may be disposed of or used by the licensee.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 48B(5)**
  No order or entry shall be made under section 48 above in respect of a patent on the ground mentioned in subsection (1)(d)(ii) above unless the comptroller is satisfied that the proprietor of the patent for the other invention is able and willing to grant to the proprietor of the patent concerned and his licensees a licence under the patent for the other invention on reasonable terms.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
