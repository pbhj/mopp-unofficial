::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 72: Power to revoke patents on application {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (72.01 - 72.45) last updated: October 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 72.01

s.77(1) s.130(7) is also relevant.

This section provides for the revocation of patents granted under the
1977 Act, including European patents (UK). Section 72(1) and (2) are so
framed as to have, as nearly as practicable, the same effects as the
corresponding provisions of the EPC and CPC. These are Articles 100 and
138(1) of the EPC and Article 57(1) (renumbered in December 1989 as
Article 56(1) \[1989\]) of the CPC.

### 72.02

The following discussion is largely concerned with revocation
proceedings brought before the comptroller. Procedure in applications to
the Patents Court and Patents County Court, which is outside the scope
of this Manual, is governed by Part 63 of the Civil Procedure Rules and
the Practice Direction supplementing Part 63.

  -----------------------------------------------------------------------
   

  Section 72(1)

  Subject to the following provisions of this Act, the court or the
  comptroller may by order revoke a patent for an invention on the
  application of any person (including the proprietor of the patent) on
  (but only on) any of the following grounds, that is to say\
  ­ (a) the invention is not a patentable invention;\
  (b) that the patent was granted to a person who was not entitled to be
  granted that patent;\
  (c) the specification of the patent does not disclose the invention
  clearly enough and completely enough for it to be performed by a person
  skilled in the art;\
  (d) the matter disclosed in the specification of the patent extends
  beyond that disclosed in the application for the patent, as filed, or,
  if the patent was granted on a new application filed under section
  8(3), 12 or 37(4) above or as mentioned in section 15(9) above, in the
  earlier application, as filed;\
  (e) the protection conferred by the patent has been extended by an
  amendment which should not have been allowed.
  -----------------------------------------------------------------------

### 72.03 {#ref72-03}

Revocation has effect ex tunc and the patent is therefore deemed never
to have been granted. The matters which are specified as grounds for
revocation in s.72(1) (a) to (e) are discussed more fully in (a)
paragraphs [1.01 to
1.39](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-01),
[2.01 to
2.56](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-01),
[3.01 to
3.99](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-01)
and [4.01 to
4.24](/guidance/manual-of-patent-practice-mopp/section-4-industrial-application/#ref4-01),
(b) paragraphs [7.06 to
7.11](/guidance/manual-of-patent-practice-mopp/section-7-right-to-apply-for-and-obtain-a-patent/#ref7-06),
(c) paragraphs
[14.58-14.105](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-58),
(d) paragraphs [76.01 to
76.23](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-01),
and (e) paragraphs
[76.24-76.27](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-24).
(It has been confirmed by the Patents Court in Liversidge v British
Telecommunications plc \[1991\] RPC 229 that s.72(1)(e) relates only to
amendment of the patent which occurred after it had been granted.)
Although the grounds listed for revocation do not, as such, include
non-compliance with section 14(5), Lord Hoffmann in Biogen v Medeva
\[1997\] RPC 1 held (at page 47) that "the substantive effect of section
14(5)(c), namely that the description should, together with the rest of
the specification, constitute an enabling disclosure, is given effect by
section 72(1)(c)". That said, in Kirin-Amgen Inc. v Transkaryotic
Therapies Inc. \[2003\] RPC 3, the Court of Appeal stated that "Section
72 which contains the grounds of revocation has no equivalent to
s.14(5). Thus clarity, conciseness and support are matters to be
considered by the Patent Office granting the patent, but lack of
clarity, conciseness and support are not grounds for revocation".

The list of grounds for revocation in s.72(1) is an exhaustive one. In
Virgin Atlantic Airways Ltd v Jet Airways (India) Ltd & Ors \[2013\]
EWCA Civ 1713 the defendants challenged the validity of Virgin's
European Patent (UK) based on the allegation that the EPO had applied
the UK designation to the application in error. The defendants sought to
engage Article 6 of the European Convention on Human Rights (ECHR)
claiming their inability, as a third party, to challenge the patent on
the basis of the actions of the EPO pre-grant breached their Article 6
rights by denying them a fair hearing. The Court of Appeal held that
Article 6 does not extend to creating substantive rights to challenge
the validity of a patent where none already exist. The only grounds
available to challenge the grant of a patent are therefore those under
s.72 and s.74 ([see also
77.03.1](/guidance/manual-of-patent-practice-mopp/section-77-effect-of-european-patent-uk/#ref77-03-1))

### 72.03.1 {#ref72-03-1}

The provision in subsection (b) allows revocation if the patent was
granted to one person when it should have been granted to another. It
also allows revocation if the patent was granted to two people when it
should have been granted only to one of them. In Henry Brothers
(Magherafelt) Ltd. v The Ministry of Defence and the Northern Ireland
Office \[1999\] RPC 442, the Court of Appeal considered the situation
where a patent was granted to one person when it should have been
granted jointly to that person and another person and concluded that,
although the question of co-ownership called for clarification in the
law, revocation did not appear to be possible in these circumstances.
Section 37(2)(a) of the 1977 Act allows the second person to be included
as one of the proprietors of the patent, and this is an adequate remedy.

### 72.04 {#ref72-04}

s.36(3) is also relevant.

Any person may apply for revocation, including the proprietor of the
patent. However, where there are co-owners, unless all co-owners agree
that it should be possible, one co-owner may not seek revocation of the
patent against the wishes of the others. More generally, there is no
need for the applicant for revocation to establish locus standi (except
in the circumstances set out in [72.40](#ref72-40) nor is there any
limit as to the time within which an action may be brought. Thus in
Cairnstores Ltd v Aktiebolaget Hassle \[2002\] FSR 35 the court refused
to strike out an application for revocation made by a company with no
assets, which did not trade and which was unrelated to the technology of
the patent in question. The court also refused to order the applicant to
disclose the name of any party on behalf of whom it was acting.
Similarly, in Oystertec Plc's Patent \[2003\] RPC 29, the Patents Court
upheld a hearing officer's decision ([BL
O/0298/02](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F298%2F02&submit=Go+%BB){rel="external"})
to refuse an application for an order for a firm of patent agents
applying for revocation of a patent to disclose the identity of the
principal they were assumed to be acting for. However, in British
Numberplate Manufacturers Association v Hills Numberplates Limited ([BL
O/066/05](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F066%2F05&submit=Go+%BB){rel="external"}),
the hearing officer held that an unincorporated association was not
entitled to bring revocation proceedings in its own name in light of
general case law which prevents an unincorporated body launching
proceedings. In Buehler AG v Chronos Richardson Ltd \[1998\] RPC 609, it
was held that both cause-of-action estoppel and issue estoppel may apply
to proceedings concerning the validity of a patent where there had been
a final judicial decision, but that the decision of the Opposition
Division of the EPO is not a final judicial decision as to the validity
of a European Patent. It was also noted that cause-of-action estoppel
would not be applicable in any case since the grounds of revocation
under section 72 are not the same as those in Article 100. Please refer
to 1.94-1.106 of the Patent Hearings Manual for detailed discussion of
the different types of estoppel.

### 72.04.1 {#ref72-04-1}

In Axis Genetics PLC's (In Administration) Patent \[2000\] FSR 448 it
was held that a revocation action falls within the wording of section
11(3)(d) of the Insolvency Act 1986. This means that proceedings for the
revocation of a patent held by a company in administration requires
either the consent of the administrators or the leave of the court. The
Enterprise Act 2002 replaced section 11 and inserted Schedule B1 into
the Insolvency Act 1986; the equivalent provision to section 11(3)(d) is
now in paragraph 43(6) of this Schedule.

### Preliminary procedure

### The Application, Statement and Counterstatement

### 72.05 {#section-2}

PR part 7 is also relevant.

An application for revocation should be made by filing Form 2
accompanied by a copy thereof and by a statement of grounds in
duplicate. This starts proceedings before the comptroller, the
procedures for which are discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).
Where the patent is co-owned, and an applicant for revocation is one of
the proprietors of the patent, references to the proprietor of the
patent in r.77 (and in 72.05-72.23) should be taken as a referring to
the proprietors who do not make the application. However, rr.75(2)­ (8)
do not apply where all the proprietors have made the application, and
instead, the comptroller shall give directions as they may think fit
with regard to the procedure for determining the application.

### 72.06 {#section-3}

The statement should be sufficiently explicit to allow the proprietor of
the patent to be aware of the specific allegations which they will have
to answer if they wish to defend their patent. Thus while normally it
will not be necessary to provide elaborations of technical fact,
nevertheless where prior disclosure or prior use of the invention are
alleged, or an argument of obviousness is based thereon, the disclosure
or use must be sufficiently identified to allow the proprietor to
appreciate the scope of the allegation they are required to meet. While
there are no prescribed requirements as to the particular facts which
need to be pleaded in the statement in proceedings before the
comptroller, guidance as to what is required under s.72(1)(a) may be
obtained from paragraph 4.4 of the Practice Direction supplementing Part
63 of the Civil Procedure Rules. The details required from this
paragraph are as follows:\
(1) in the case of matter made available to the public by written
description the date on which and the means by which it was so made
available, unless this is clear from the face of the matter; and (2) in
the case of matter made available to the public by use ­\
(a) the date or dates of such use;\
(b) the name of all persons making such use;\
(c) any written material which identifies such use;\
(d) the existence and location of any apparatus employed in such use;
and\
(e) all facts and matters relied on to establish that such matter was
made available to the public.

### Contesting an application for revocation

### 72.07 {#section-4}

r.77(5) r.77(6) is also relevant.

If the proprietor wishes to contest the application, they must file a
counter statement in the proceedings.

### 72.08 {#ref72-08}

The contents of the counter-statement are prescribed by r.78. Provided
that a ground is replied to, the proprietor is not required to argue
with every point of detail advanced by the applicant (Marshall's
Application, \[1969\] RPC 83).

### Failure to file counterstatement

### 72.09 {#ref72-09}

Failure to file a counterstatement (except where the applicant has
withdrawn unconditionally) will lead to the proprietor's case being
treated as undefended, and they will forfeit the right to take any
further part in the proceedings. However, the proceedings remain
strictly inter partes. The application will be considered by the
comptroller as if each specific fact set out in the statement were
conceded, except insofar as it is contradicted by other documents
available to the comptroller. If on this basis it is determined that a
ground has been made out, then the patent will be revoked. However, if
it is the preliminary opinion of the Office that no ground has been made
out then the applicant should be informed by Tribunal Section of this
preliminary opinion and offered a hearing before the application is
dismissed. The application will be considered in a similar way by the
comptroller if the patentee decides not to defend the application at a
later stage in the proceedings.

### Offer to amend under s.75 {#offer-to-amend-under-s75}

### 72.10 {#section-5}

A counterstatement is still required even if, before it is due to be
filed, the applicant has indicated that they are willing to withdraw on
the condition that certain amendments are made to the specification and
the proprietor is agreeable to these amendments. In such a case however
it will be sufficient for the counterstatement merely to offer the
proposed amendments, stating that the offer is unconditional ([see
72.11](#ref72-11)).

### 72.11 {#ref72-11}

s.76(2) is also relevant.

If the proprietor offers in their counterstatement to amend the
specification it should be made clear on what basis the offer is made,
that is, whether the offer is firm or is conditional upon an adverse
finding on the unamended specification. Unless the amendments are very
minor, they should preferably be identified on a copy of the published
specification showing the amendments, referred to in the
counterstatement, in red. Any such amendments are made under s.75 and
are subject to r.35 procedure ([see
75.04-07](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-04)
and
[75.15-17](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-15)).
The proposed amendments should be considered by the examiner to report
on whether in their opinion they are prima facie allowable. At this
stage the only questions which need to be considered by the examiner are
whether the amendments would add matter or extend the protection
conferred by the patent; no attempt should be made to consider whether
they meet the alleged ground of invalidity.

\[ Once the action officer has checked that the nature of the proposed
amendments and the basis upon which they are offered has been properly
indicated the case should be referred to the Group Head in charge of the
subject matter concerned for a preliminary report on the amendments. In
making their assessment the Group Head should make no attempt to review
the whole case; in most cases it should not be necessary to look beyond
the copy of the specification on which the proposed amendments are
shown. They should bring the case to the attention of the hearing
officer at this stage of the proceedings. Reporting on an offer to amend
should be regarded as urgent and if the Group Head is absent for more
than a few days the Senior Examiner acting for them should consult the
hearing officer as to the course to follow. \]

### 72.12 {#section-6}

Any objection arising out of this preliminary scrutiny of the amendments
should be communicated in an official letter to the proprietor allowing
a specified period for reply (normally one month). If the proprietor
maintains that the amendments are allowable, both parties should be
informed that this has been noted and that the matter will be determined
at the substantive hearing. If no reply is received within the specified
period, the proposed amendments should be treated as if the proprietor
maintains that they are allowable and the parties should be informed
accordingly. If new or modified amendments are proposed in reply, the
above procedures should be repeated. Although such amendments may
initially be proposed in correspondence, in order to be brought formally
into the proceedings, they must be incorporated into or referred to in
an amended counterstatement.

\[ If the Group Head is satisfied that there is a clear major objection
under s.76, they should report in a form suitable for incorporation in a
letter expressing the objection as a prima facie view. To this end such
a letter (issued by the tribunal manager) should open with, 'The
amendments have been referred to an examiner who has expressed the
following prima facie view:- ..........' If, however, there are no or
only minor objections or there is a reasonable element of doubt, the
Group Head should report accordingly in a minute and inform Tribunal
Section that in the circumstances no action on the amendments is
necessary at this stage; he should not expressly state or imply that the
amendments are allowable. It must be remembered that it is for the
hearing officer to pronounce on the allowability of the amendments and
nothing that the Group Head reports to the applicant should purport to
derogate from the hearing officer's authority. The Group Head must
accordingly communicate nothing which could be interpreted as a decision
or as a finding, regarding the allowability of the amendments. \]

### 72.12.1 {#section-7}

When amendments have been submitted in a counterstatement they should be
advertised in the form subsisting as soon as the procedure described in
paragraphs [72.11­ - 72.12](#ref72-11) has been completed unless they
are clearly minor and of no substance.

### 72.13 {#ref72-13}

s.75(2) is also relevant.

The revocation proceedings should usually proceed in parallel with the
consideration of the amendments under s.75. Thus the applicant should be
allowed (a) a specified period (normally one month) in which, if they
wish, to amend his statement or file a supplementary statement in
respect of the amendments (Dust Suppression Ltd's Application \[1976\]
FSR 438) and (b) a period in which to file evidence in chief ([see
123.05.9-123.05.11](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-9)).

The comptroller may however stay the revocation proceedings at the
request of the applicant pending consideration of the amendments.
Similarly, if the amendments are opposed the comptroller may stay the
revocation proceedings pending a resolution of the opposition or may
leave the opposition to be decided in the revocation proceedings. Note
that the proprietor also has the opportunity of amending a European
patent (UK) before the EPO ([see 72.44.1](#ref72-44-1) below).

### 72.14 {#ref72-14}

r.82(1) is also relevant.

Amendment of the application for revocation, the statement or the
counterstatement is allowable with the leave of the comptroller; for
example a new applicant for revocation may be added or substituted or a
new ground or further facts may be introduced. In a preliminary
decision, (GEC Avery Service Ltd v Derwent Measurement and Control Ltd,
[BL
O/62/94](https://www.gov.uk/government/publications/patent-decision-o06294))
the hearing officer acting for the comptroller held that, in exercising
discretion as to admit new or amended grounds, he should follow the
principles established in Owens-Corning Fibreglas Corporation's Patent
\[1972\] RPC 684 for cases decided under the 1949 Act, namely (a)
whether the applicant had used due diligence in preparing their case;
(b) the relevance of new art sought to be introduced; (c) the time that
had elapsed since the filing of the application; and (d) whether delay
will be caused which might be unjust to the patentee or against the
public interest. Evidence in the form of statutory declaration or
affidavit may be necessary to explain the lateness of the amendment. The
fact that evidence may be necessary in support of a new ground does not
of itself justify a refusal to allow the introduction of that ground
(Allmanna Svenska Electriska Aktiebolaget's Application, \[1976\] RPC
464).

### Admission of new grounds

### 72.15 {#section-8}

Where the comptroller decides to admit a new ground, a supplementary or
amended statement should be filed. The proprietor should then be given
the opportunity to file a supplementary or amended counterstatement in
reply. Unless the new ground is not contested or evidence relevant to
the new ground has already been filed, each party should be given a
specified period (which may run concurrently with any other period set)
in which to file further evidence in support of or in response to this
new ground.

### 72.16 {#ref72-16}

Matters not included in the statement or counterstatement as originally
filed cannot be raised at the hearing ([Bradford Dyers Association Ltd's
Application, \[1966\] FSR
79](https://uk.westlaw.com/Document/I799FBC60E42711DA8FC2A0F0355337E9/View/FullText.html?originationContext=document&transitionType=DocumentItem&ppcid=f8f4e043e5ac4cbda7f985abff967b47&contextData=(sc.Default)&comp=wluk){rel="external"}
and [Roussel-Uclaf (Joly and Warmant's) Patent, \[1971\] RPC
304](https://doi.org/10.1093/rpc/88.12.304){rel="external"}), unless
they have been allowed to be introduced into the statement or
counterstatement by way of amendment ([see
75.14](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-14)).

### Evidence

### 72.17 {#section-9}

\[deleted\]

### 72.18 {#section-10}

\[deleted\]

### 72.19 {#ref72-19}

The applicant for revocation should not wait until the reply stage to
complete the evidence necessary to make out their case. Further evidence
filed at this stage must be strictly in reply to the proprietor's
evidence. In [Scragg (Ernest) Ltd's Application, \[1972\] RPC
679](https://doi.org/10.1093/rpc/89.23.679){rel="external"}, which was
an opposition under the 1949 Act, Graham J quoted Halsbury's Laws of
England as authority for the principle that the party on which the onus
of proof falls (which in the present case is the applicant for
revocation) must adduce all their primary evidence when presenting their
case and not seek thereafter to adduce additional evidence to strengthen
their case. He stated (at page 682) "to my mind it is quite wrong in
these cases that there should be any sort of skirmishing in regard to
evidence, and if an opponent has a case he should straight away state
what his case is and should put in declarations dealing with any
evidence which he thinks may be relevant to his case". In Peckitt's
Application \[1999\] RPC 337 some further evidence was allowed to stand
which was little more than repetition of elements of the case put in
chief with some minor clarification and comment. The important factor
was said to be that it did not either alter or strengthen the party's
case or be such as to prolong the pre-hearing procedure by justifying
another round of evidence from the other party.

### 72.20 {#section-11}

r.87 is also relevant.

For the form of evidence, [see
123.17-18](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-17-18)
and Chapter 3 of the Patent Hearings Manual. Documents referred to

### 72.21 {#section-12}

The provision of copies of documents referred to in statements and
evidence is governed by r.79 - [see
123.18](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-18)
Extension of periods

### 72.22 {#section-13}

\[deleted\]

### Subsequent procedure

### 72.23 {#section-14}

After completion of the stages referred to above the matter will
normally proceed to a hearing unless either the applicant withdraws or
abandons by failure to give security for costs ([see
72.24-72.35](#ref72-24) and
[123.05.10-123.05.12](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-10))
or the proprietor offers to surrender their patent ([see
72.36-72.39](#ref72-36)). Following the hearing a decision will be
issued ([see 72.42-72.44](#ref72-42) and
[123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05-13)).
In some cases a preliminary hearing may be necessary to decide a dispute
between the parties or between the Office and either or both of the
parties.

### Withdrawal of Applicant

### Conditions of withdrawal

### 72.24 {#ref72-24}

A withdrawal by the applicant may be unconditional, or it may be
conditional on amendments which have been submitted by the proprietor
being allowed. The applicant's willingness to withdraw on the basis of
the proposed amendments may be indicated in a letter or by
counter-signature on a copy of the specification showing the amendments.
If it is not clear whether an offer to withdraw is conditional or
unconditional, or what the conditions for withdrawal are, Tribunal
Section should seek clarification from the applicant.

### 72.25 {#ref72-25}

In Flude (H) & Co (Hinckley) Ltd's Patent \[1993\] RPC 197 in which the
applicant sought to amend the statement of case under rule 75(1) by a
conditional withdrawal of certain grounds of revocation and some of the
evidence while leaving open the possibility of relying on them
subsequently once a decision had been made on the remaining issues, the
applicant was required to choose whether to proceed on the original
pleadings or to withdraw certain grounds of invalidity with an order
that they should not be raised again in any proceedings between the
parties before the comptroller or in the court without leave of the
court.

### Public interest

### 72.26 {#ref72-26}

Where an applicant serves notice of withdrawal from revocation
proceedings before the comptroller which have been properly launched, an
examiner considers whether the comptroller should accept the notice of
withdrawal without qualification or whether there are questions
remaining that the comptroller should further consider in the public
interest ([Abbott Laboratories (Chu's) Patent \[1992\] RPC
487](https://doi.org/10.1093/rpc/1992rpc487){rel="external"}, following
[General Motors (Tunney & Barr's Application) \[1976\] RPC
659](https://doi.org/10.1093/rpc/93.24.659){rel="external"} decided
under the 1949 Act). The proceedings are concluded by a formal decision
of a hearing officer acting for the comptroller. This procedure, which
differs from that in proceedings before the court because the
comptroller has technical staff to investigate the matter, was endorsed
in [R v Comptroller-General of Patents, ex parte Ash & Lacey Building
Products Ltd \[2002\] RPC
46](https://doi.org/10.1093/rpc/2002rpc46){rel="external"}. Prima facie,
where an application is treated as abandoned under s.107(4) in default
of security for costs or expenses, the same public interest
considerations apply as in the case of unconditional withdrawal of the
application.

### 72.27 {#ref72-27}

The specification, including any amendments submitted by the proprietor,
should be considered in order to ascertain whether a case for revocation
is made out on the evidence of the documents so far filed. The
allowability of amendments is subject to the principles referred to in
[75.04 -
75.05.1](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-04).
Normally only clear cases of lack of novelty or inventive step based on
prior documentary disclosure should be pursued by the comptroller. Only
exceptionally should some other ground of revocation be continued with
after withdrawal of the applicant. In [R v Comptroller-General of
Patents, ex parte Ash & Lacey Building Products Ltd \[2002\] RPC
46](https://doi.org/10.1093/rpc/2002rpc46){rel="external"}, it was held
that it was allowable for the hearing officer to first come to a view on
construction of the claims before determining whether a clear case of
lack of novelty or inventive step had been made out.

\[ If the applicant for revocation seeks to withdraw, the action officer
in Tribunal Section should refer the file to the hearing officer's
assistant, if one has been appointed, or otherwise to the Deputy
Director in charge of the subject-matter, to determine whether any
action is necessary in the public interest. If the Deputy Director
considers that revocation is justified on some ground other than lack of
novelty or of inventive step, then they should consult the hearing
officer concerned before issuing a report. The Deputy Director concerned
may deal with the case themselves or call on a Senior Examiner to make a
report but in either case the Deputy Director remains responsible for
any decision made and only they (or a hearing officer in their absence)
may sign the decision. When a Senior Examiner is asked to report on a
withdrawn revocation, no more than a brief report on the issues which
are required to consider is called for, and this should be minuted. Any
points to be raised with the proprietor should be the subject of an
appendix drafted in terms suitable for incorporation in a letter which
will be issued by the action officer in Tribunal Section. All action
taken should be recorded in a minute added to the the dossier (PDAX
cases) or on the proceedings sheets in the revocation shell (Paper
cases). \]

### Procedure when no ground for revocation remains

### 72.28 {#section-15}

Where the applicant has requested conditionally or unconditionally to
withdraw and the proprietor has submitted amendments which appear to be
allowable and to remove the ground for revocation, or where the
applicant has withdrawn unconditionally and no amendments have been
offered and the Office is satisfied that no ground for revocation has
been made out, the proceedings should be dealt with as described in
[72.42](#ref72-42) except that since the decision does not involve
determination of an issue in dispute between the parties it may be
issued by the Group Head rather than the hearing officer.

### Procedure when grounds for revocation remain

### Unconditional withdrawal of applicant

### 72.29 {#ref72-29}

Where the applicant has requested unconditionally to withdraw and the
examiner determines that the action should be continued in the public
interest by the comptroller, a letter should be sent to the proprietor
including a statement that whilst the request for withdrawal has been
noted the revocation proceedings are regarded as still in being and the
comptroller cannot yet decide the matter. The letter should set out
clearly the grounds which are being pursued in the public interest. This
letter from the Office must be worded in such a way that there is no
danger of it being construed as a decision.

\[ The following is a specimen letter:­

'The applicant's desire to withdraw from these proceedings, expressed in
their letter dated .........., has been noted. However, the application
has been referred to an examiner, who feels that the issue(s) of
.......... should be pursued in the public interest for the following
reasons:­-

'Accordingly the revocation proceedings are still regarded as in being
and the comptroller cannot yet decide the case. 'This letter has been
copied to the applicant for information. However since they are not
actively pursuing these proceedings, further correspondence need be sent
to the comptroller only.'

s.101 is also relevant.

\[ The letter should also indicate the action required, such as the
filing of a counterstatement, comments, evidence, amendments or further
amendments. A period for reply (normally one month) should be specified
and the proprietor should be informed of their right to be heard. The
letter should be issued by the action officer in Tribunal Section on the
instruction of the Group Head. A copy should be sent to the applicant.
\]

### 72.30 {#section-16}

The proprietor should be advised that any further proposals for
amendment should be filed with the Office within the period allowed for
reply to the letter. The proprietor should also be advised that such
proposals should be shown clearly on a copy of the published
specification. Since the applicant is no longer actively pursuing the
proceedings, the proposals need be sent to the Office only. Once a form
of amendment which is allowable is arrived at, the procedure in
paragraph [72.42](#ref72-42) should be followed.

### 72.31 {#section-17}

The applicant should be sent a copy of the initial letter to the
proprietor and warned that thereafter, unless they request otherwise, no
further correspondence will be copied to them except that, for
information, they will be sent a copy of any decision revoking the
patent or dismissing the application or an advertisement of amendments
to be published in the Journal.

### Conditional withdrawal of applicant

### 72.32 {#ref72-32-72-34}

Where the applicant has requested to withdraw conditionally on
amendments being allowed and the examiner determines that further action
would be required in the public interest by the comptroller, the
applicant remains an active party to the proceedings since their
conditions for withdrawal have not been met. The hearing officer should
then determine the most efficient course of action in the circumstances.

### 72.33 {#section-18}

Since the applicant had offered to withdraw subject to the amendments
being allowed, they presumably have no objections to them; the examiner
will therefore normally pursue any objections directly with the
proprietor until they are satisfied that the amendments are acceptable,
the correspondence being copied to the applicant for comment. The action
under section 72 will then usually be stayed pending the outcome of the
application to amend under section 75. In any case, the examiner will
write to the proprietor setting out the objections which would be
pursued. As in the case discussed in [72.29](#ref72-29) above, this
should not be in terms that could be construed as a decision.

\[ The following is a specimen letter:­

'The applicant's desire, expressed in their letter dated .........., to
withdraw from the revocation proceedings subject to acceptance of the
proposed amendments submitted with your letter dated ......... has been
noted. However, the amendments have been referred to an examiner, who
feels that the amendments do not meet the issue(s) of .......... for the
following reasons:­

'Accordingly the revocation proceedings are still regarded as in being
and the comptroller cannot yet decide the case. 'This letter has been
copied to the applicant for information.'

s.101 is also relevant.

\[ The letter should also indicate the action required, such as the
filing of a counterstatement, comments, evidence, amendments or further
amendments. A period for reply (normally one month) should be specified
and the proprietor should be informed of their right to be heard. The
letter should be issued by the action officer in Tribunal Section on the
instruction of the Deputy Director. A copy should be sent to the
applicant. \]

### 72.34 {#section-19}

Once amendments acceptable to the examiner have been reached, it should
be determined whether the applicant still wishes to withdraw the
application under section 72 on the basis of these amendments being
accepted; if not then the applicant should be given an opportunity to
amend their statement or file a supplementary statement as described in
[72.13](#ref72-13). If there is no likelihood of agreement being reached
or if further amendments are not acceptable to the examiner a hearing
may be necessary.

### Costs

### 72.35 {#section-20}

In any case involving withdrawal following agreement between the parties
the question of costs should normally have been dealt with as part of
the agreement, and in such a case the hearing officer will not normally
concern themselves with costs, and their decision will not refer to
them. If however an order for costs is sought, other than merely in the
statement or counterstatement, before the decision issues, then such an
order can be included in the decision. Where, after the issue of the
decision, one party asks for costs, the request should be referred to
the hearing officer for consideration of an award of costs.

\[ A Deputy Director is empowered to award costs in withdrawn revocation
proceedings. For further details on awarding costs, see Chapter 5 of the
Patents Hearings Manual. \]

### Surrender

### 72.36 {#ref72-36}

s.29(1) and s29(3) is also relevant.

The proprietor may at any time offer to surrender their patent ([see
29.01­
-29.07](/guidance/manual-of-patent-practice-mopp/section-29-surrender-of-patents/#ref29-01)).
Unlike revocation, however, the surrender only takes effect from the
date when notice of acceptance of the surrender is published in the
Journal. There will therefore have been some time during which the
patent was in force. It follows that an offer to surrender during
revocation proceedings will not automatically terminate those
proceedings. No suggestion should therefore be made that a proprietor
(or patentee) who does not wish to oppose an application for revocation
should offer to surrender their patent.

### 72.37 {#section-21}

r42, r75, r76(2) is also relevant.

The offer to surrender must be advertised and the parties informed of
the date of the advertisement. The four week period for opposition to
the surrender runs from that date.

\[ As soon as arrangements for the advertisement have been made and
letters sent to the proprietor and applicant for revocation notifying
them of the date of the advertisement the case should be referred to the
hearing officer to decide on the future action. Normally the hearing
officer should arrange for a member of their division, usually a Group
Head, to write a report on the revocation action. \]

### 72.38 {#section-22}

The matter should be considered as though no counterstatement had been
filed, that is, as if each specific fact set out in the statement had
been conceded except insofar as it is contradicted by other documents
before the Office. If on this basis it is determined that at least one
ground for revocation has been made out (and provided that the two month
period has expired without an opposition being lodged), the parties
should be informed that it is proposed to issue a formal decision
revoking the patent, and consequently not to accept the offer to
surrender, unless within one month either party opposes this course of
action.

\[ If it is determined that at least one ground for revocation has been
made out, the examiner should draft a letter for issue by Tribunal
Section. The case should be referred to Tribunal Section via the hearing
officer. If either party opposes the decision to revoke instead of
allowing surrender, or in the very rare cases when no ground for
revocation is found, the matter should be referred to the hearing
officer for direction as to future proceedings.

\[ The hearing officer may change the procedure outlined above in
exceptional cases, for example if (a) the offer to surrender is made
after both sides have filed evidence in the revocation action, (b) the
applicant for revocation is a private applicant who gives no substantial
grounds for revocation, or (c) an opposition to the advertised offer has
been entered. \]

### 72.39 {#section-23}

If, in revocation proceedings before the comptroller under this section,
the proprietor offers to surrender the patent, the comptroller shall, in
deciding whether costs should be awarded to the applicant for
revocation, consider whether proceedings might have been avoided if the
applicant had given reasonable notice to the proprietor before the
application was filed.

  -----------------------------------------------------------------------
   

  Section 72(2)

  An application for the revocation of a patent on the ground mentioned
  in subsection (1)(b) above ­\
  (a) may only be made by a person found by the court in an action for a
  declaration or declarator, or found by the court or the comptroller on
  a reference under section 37 above, to be entitled to be granted that
  patent or to be granted a patent for part of the matter comprised in
  the specification of the patent sought to be revoked; and\
  (b) may not be made if that action was commenced or that reference was
  made after the second anniversary of the date of the grant of the
  patent sought to be revoked, unless it is shown that any person
  registered as a proprietor of the patent knew at the time of the grant
  or of the transfer of the patent to him that he was not entitled to the
  patent.
  -----------------------------------------------------------------------

### 72.40 {#ref72-40}

Thus revocation of a patent on the ground that the patent was granted to
a person who was not entitled to be granted that patent may only be
sought by a person who has already satisfied the court or the
comptroller that they themselves should have been the or a proprietor.
(See also
[37.05](/guidance/manual-of-patent-practice-mopp/section-37-determination-of-right-to-patent-after-grant/#ref37-05)
and
[37.15](/guidance/manual-of-patent-practice-mopp/section-37-determination-of-right-to-patent-after-grant/#ref37-15)).
No one else may apply for revocation on this ground. Nor may any
application be made on this ground unless proceedings to contest the
proprietor's right to the patent had been started by, at the latest, the
second anniversary of the date of grant, unless the applicant can show
bad faith on the part of the proprietor.

### \[Section 72(3) Repealed\]

### 72.41 {#section-24}

Section 72(3) was concerned with patents for inventions requiring
microorganisms for their performance. This was repealed by the CDP Act,
and the equivalent provision is now in section 125A.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  Section 72(4)
  An order under this section may be an order for the unconditional revocation of the patent or, where the court or the comptroller determines that one of the grounds mentioned in subsection (1) above has been established, but only so as to invalidate the patent to a limited extent, an order that the patent should be revoked unless within a specified time the specification is amended to the satisfaction of the court or the comptroller, as the case may be.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 72.42 {#ref72-42}

r.75 is also relevant.

If the case for revocation has not been made out, and amendments have
been proposed by the proprietor during the hearing, the hearing officer
must ensure that the amendments are advertised (see [Tribunal Patents
Manual](https://www.gov.uk/government/publications/tribunal-patents-manual)
paragraph 14.30, and chapter 23) unless they are clearly not allowable
or would not cure invalidity. If the amendments are to be advertised the
issue of a final decision should be deferred until the procedure
described in 75.05 has been followed. If the amendments are not to be
advertised, or if the specification had already been amended before the
hearing ([see 72.11-72.13](#ref72-11)), or if no amendments have been
offered, a final decision should be issued dismissing the application
for revocation.

\[ In the case of revocation proceedings where the applicant has
withdrawn, when a decision is due to issue, Tribunal Section should be
instructed to prepare a Decisions Form; Form B should be used where the
applicant has withdrawn unconditionally and there have been no
amendments, Form D' where the applicant has withdrawn conditionally see
[72.32-72.34](#ref72-32-72-34) and Form G' where the specification has
been amended following an unconditional withdrawal see
[72.29-72.31](#ref72-29). The Group Head should sign but not date the
decision. Tribunal Section should apply the appropriate electronic
signature, prepare copies of the signed decision, date them and send
them to each of the parties.

\[ When a decision authorises amendment of a specification, the
amendments are shown in a copy of the specification attached to the
decision. After issue of the decision, the amendments should be
incorporated into the original specification by Tribunal Section and
upon receipt of confirmation from the hearing officer a certificate with
the appropriate electronic signature is placed on the dossier. If the
amendments are extensive, Tribunal Section should request retyped pages
in accordance with r.35(6) if these have not already been filed. The
file should then be referred to the hearing officer (or Group Head where
the applicant has withdrawn). Where there is an assistant they should
check the amendments and the certificate with the appropriate electronic
signature is then placed on the dossier. \]

### 72.43 {#ref72-43}

When the hearing officer decides that one or more of the grounds for
revocation has been made out but that the defects of the patent might be
cured by amendment of the specification they may if they see fit issue
an interim decision giving the proprietor a specified period, normally
two months, in which to amend to meet the findings and thereby avoid
revocation [see also 72.44.1](#ref72-44). A factor in deciding whether
to give an opportunity to amend ([see
75.04](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-04))
is the necessity for further proceedings to determine validity of any
amendments. In Nikken Kosakusho Works v Pioneer Trading Co. \[2006\] FSR
4, the Court of Appeal drew a distinction between post-trial amendments
only involving deletion of invalid claims or re-writing claims to
exclude various dependencies, and those where the patentee sought to
introduce a different claim which had not been under attack at the
trial, where there was bound to be a further battle in proceedings over
the proposed amendments to determine their validity. Considering the
overriding objective of the Civil Procedure Rules that in any given
litigation the parties are required to bring forward their whole case
and the specific requirements of Part 1.1.2 of the CPR that the court
should deal with cases justly by saving expense and ensuring that they
were dealt with expeditiously and fairly, the court held that the latter
sort of post-trial amendment should not be allowed if it would involve a
second trial on validity. Monkey Tower Ltd v Ability International Ltd
\[2013\] EWHC 18 (Pat) provided more details about the factors to be
considered when deciding whether to exercise discretion to allow an
opportunity to amend. Henry Carr QC, sitting as a Deputy Judge,
explained that it is necessary for the comptroller to have regard to all
circumstances which are relevant to the question of procedural fairness
to the parties. These circumstances might include:\
a) The resources already devoted by the parties to the proceedings.\
b) The extent of any re-litigation as a result of the amendment.\
c) The likelihood that a valid amendment can be proposed.\
d) Whether there is evidence that prejudice will be caused to the
applicant for revocation by the delay caused by an application to amend.

Where the hearing officer does not see fit to allow such an opportunity
to amend, or it appears that no saving amendment is possible, a final
decision should be issued revoking the patent without allowing an
opportunity to amend.

\[ Any request for extension of the specified period should be referred
by the action officer to the hearing officer. \]

### 72.44 {#ref72-44}

When amendments are submitted following an interim decision the
applicant should be invited to comment within a period specified in the
decision, usually one month. If a dispute is apparent the proprietor
should be allowed a similar period in which to reply. A further hearing
may be necessary if no agreement is reached. A further interim decision
may be issued with or without a further hearing. If the amendments are
allowable and meet the terms of the interim decision the matter should
proceed as described in [72.42](#ref72-42).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  Section 72(4A)
  The reference in subsection (4) above to the specification being amended is to its being amended under section 75 below and also, in the case of a European Patent (UK), to its being amended under any provision of the European Patent Convention under which the claims of the patent may be limited by amendment at the request of the proprietor.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 72.44.1 {#ref72-44-1}

Article 138(3) EPC provides a central amendment process for European
Patents. This is an alternative to the existing possibility of the
proprietor amending the patent under the 1977 Act. In the former case,
the amendments are effective in each Contracting State designated by the
patent whereas the latter would only affect the European patent (UK).
This subsection makes it clear that references to the patent being
amended take account of the central limitation process at the EPO.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  Section 72(5)
  A decision of the comptroller or on appeal from the comptroller shall not estop any party to civil proceedings in which infringement of a patent is in issue from alleging invalidity of the patent on any of the grounds referred to in subsection (1) above, whether or not any of the issues involved were decided in the said decision.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  Section 72(6)
  Where the comptroller refuses to grant an application made to him by any person under this section, no application (otherwise than by way of appeal or by way of putting validity in issue in proceedings for infringement) may be made to the court by that person under this section in relation to the patent concerned, without the leave of the court.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  Section 72(7)

  Where the comptroller has not disposed of an application made to him
  under this section, the applicant may not apply to the court under this
  section in respect of the patent concerned unless either\
  (a) the proprietor of the patent agrees that the applicant may so
  apply, or\
  (b) the comptroller certifies in writing that it appears to him that
  the question whether the patent should be revoked is one which would
  more properly be determined by the court.
  -----------------------------------------------------------------------

### 72.45 {#section-25}

CPR 63.11 is also relevant.

The comptroller may decide that it appears to them that the question
whether the patent should be revoked should be more properly determined
by the court. In such a case any person seeking the court's
determination of that question must issue a claim form within 14 days of
the comptroller's decision.

\[Further guidance on certification that revocation would more properly
be determined by the court is given in Chapter 2 of the Patent Hearings
Manual\]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
