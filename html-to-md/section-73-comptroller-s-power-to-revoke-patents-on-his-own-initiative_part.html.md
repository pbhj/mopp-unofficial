::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 73: Comptroller\'s power to revoke patents on his own initiative {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (73.01 - 73.12) last updated: October 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 73.01

This section specifies the only grounds on which a patent may be revoked
other than at the request of a third party.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 73(1)**
  If it appears to the comptroller that an invention for which a patent has been granted formed part of the state of the art by virtue only of section 2(3) above, he may on his own initiative by order revoke the patent, but shall not do so without giving the proprietor of the patent an opportunity of making any observations and of amending the specification of the patent so as to exclude any matter which formed part of the state of the art as aforesaid without contravening section 76 below.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 73.02 {#ref73-02}

Proceedings under s.73(1) will be initiated after the grant of a patent
where a relevant document has been brought to the attention of the
examiner (either because of actions within the Office or as a result of
observations from a third party - [see
21.17](/guidance/manual-of-patent-practice-mopp/section-21-observations-by-third-party-on-patentability/#ref21-17))
after the date of issue of the letter informing the applicant that the
application is in order [see
18.88](/guidance//manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-88).
Unless the examiner is not satisfied that the pre-grant search was
complete for art in the s.2(3) field [see
17.118](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-118),
no search is done after grant as a matter of course for such documents.
Action may also be taken under s.73(1) in the exceptional case where it
has not been possible before the end of the s.20 period to obtain a copy
of the priority document of a cited European application (UK) in order
to determine whether or not it has an earlier priority date than the
invention [see
18.18](/guidance//manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-18).
Proceedings under s.73(1) may also be initiated in the event that an
opinion is issued which concludes that a granted UK patent or European
patent (UK) lacks novelty over a patent application which forms part of
the state of the art under section 2(3) [see
74A.11.1](/guidance/manual-of-patent-practice-mopp/section-74a-opinions-on-matters-prescribed-in-the-rules/#ref74a-11-1)

### 73.02.1 {#section-1}

Where the patent is a European patent (UK), the examiner should ensure
that they are referring to the latest version of the patent, which could
have been amended under the central limitation procedure at the EPO.

### 73.02.2 {#ref73-02-2}

A UK or European patent application which was withdrawn prior to
publication, but too late to prevent publication, is not considered to
form part of the state of the art under s.2(3) (Woolard's Application
\[2002\] RPC 39 -- see 2.32). The examiner should therefore check the
status of the potential s.2(3) citation, and if it is withdrawn, the
date on which it was withdrawn.\

\[The status of a UK application, including the date of any withdrawal,
can be checked using the DISPLAY FULL REGISTER function on the Common
Enquiries Menu of COPS. The status of an EP application, including the
date of any withdrawal, can be checked using the [European Patent
Register](https://register.epo.org/regviewer){rel="external"}. \]

### 73.03 {#ref73-03}

r.35(6) and r.109 is also relevant.

When action appears necessary the proprietor must be informed and must
be given a period within which to submit observations and/or amendments.
A suitable period is normally three months, although a shorter period
may be specified at the comptroller's discretion, and this period may be
extended under r.109. (The amendments must comply with s.76. They are
not advertised since s.73 proceedings are ex parte and so third parties
cannot intervene.) If leave to amend is given, the applicant may be
required to file a new specification as amended, prepared in accordance
with rules 12 and 14, before the amendment is effected. Any offer to
amend and its outcome should be specifically recorded on the Register.

### 73.03.1 {#ref73-03-1}

Proceedings under s.73(1) should normally be initiated using Letter
Clause PL2. However where the document was a European application cited
before grant but action was deferred because of non-availability of
priority documents ([see 73.02](#ref73-02),
[18.18](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-18),
Letter Clause PL1 should be used). \[ (a) A report should be made by the
examiner in a minute and the case referred to Restoration and Post Grant
Section (RAPS) who will arrange for the action to be noted in the
Register, for the appropriate letter to be issued, and for the case to
be referred back to the examiner on receipt of any amendments and/or
observations.\]

\[The examiner should add the minute to the dossier, addressing the
minute to RAPS, and a PDAX message should be sent to the RAPS mailbox.\]

\[ (b) If the examiner is satisfied after consideration of any
amendments and/or observations submitted by the proprietor that
revocation is not necessary, they should report this in a minute and the
case should be referred to RAPS Section. RAPS Section will create a
minute and send a message to the Group Head in charge of the group of
the examiner considering the amendment. The Group Head will confirm
whether the amendments are allowable and that Decision Form 1, 1a or 2a
can be prepared. Where amendments have been allowed, RAPS Section should
effect them in the specification and prepare a certificate for signature
by the Group Head. The case should be referred to Publishing Section for
the issue of a 'C' publication as described in
[27.20](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-20).
Unless the amendments can be dealt with in the 'C' publication by means
of a schedule, RAPS Section should firstly obtain a retyped
specification from the patentee under r.35(6).\]

\[The group head should add the minute to the dossier, addressing the
minute to RAPS Section, and a PDAX message should be sent to the RAPS
mailbox\]

\[ (c) If amendments and/or observations are received but the group head
cannot be satisfied that the objection is overcome, and further
amendments appear possible, a further period may be specified in order
to allow such amendments to be filed. The time periods specified may be
extended if appropriate under s.117B and rule 109. If no agreement can
be reached, a hearing should be offered to be taken by a Group Head. The
examiner should prepare a report for the Group Head before the hearing.
If the Group Head decides, as a result of the hearing, that the patent
should be revoked, they should issue a reasoned decision to that
effect.\]

\[ The examiner should record their objection(s) in a minute which
should be added to the dossier and addressed to RAPS and a PDAX message
should be sent to the RAPS mailbox. The objection(s) should be stated in
a form which is suitable for inclusion in an official letter, which will
be sent over the signature of a member of RAPS but will give the name
and telephone number of the group head. RAPS will inform the proprietor
of the latest date for response and will deal with requests for
extensions of time periods.\]

\[ (d) Where the proprietor responds but raises no objection to
revocation of the patent or leaves the question of revocation to be
decided by the comptroller, RAPS should prepare Decision Form 3a and
refer the case along with the Decision Form to the group Group Head for
their signature.\]

\[ (e) If the proprietor fails to reply to Letter Clause PL1 or PL2,
RAPS should offer a hearing by issuing Letter Clause PL4. If there is
still no response from the proprietor, RAPS should prepare Decision Form
5 and refer the case along with the Decision Form to the group Group
Head for their signature.

### 73.04 {#section-2}

Article 54(3) of the EPC specifies that the only document published on
or after the filing date of an application for a European patent which
forms part of the state of the art for that application is another
European application. If follows therefore that a European patent (UK)
may be validly granted by the EPO even though the invention is disclosed
in a published UK patent application having an earlier priority date.
Such a patent could however then be revoked by the comptroller under
s.73(1). An examiner who becomes aware of such a situation should
proceed as described in paragraph [73.03](#ref73-03). Such action should
also be taken where a European patent (UK) is clearly anticipated by
another European patent application (UK) which form part of the state of
the art by virtue of s.2(3) and EPC a.54(3) but which was not cited by
the EPO.\
\
\[Action under s.73(1) against a European patent (UK) should be
initiated using Letter Clause PL2.\]\
\
\[Action to cite a prior European application against a European patent
(UK) under s.73(1) should be taken only in the clearest cases, and in
any case should not be taken if the application had been indicated on
the printed European patent specification as having been cited by the
EPO.\]

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 73(1A)**
  Where the comptroller issues an opinion under section 74A that section 1(1)(a) or (b) is not satisfied in relation to an invention for which there is a patent, the comptroller may revoke the patent.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 73(1B)**

  The power under subsection (1A) may not be exercised before-\
  (a) the end of the period in which the proprietor of the patent may
  apply under the rules (by virtue of section 74B) for a review of the
  opinion, or\
  (b) if the proprietor applies for a review, the decision on the review
  is made (or, if there is an appeal against that decision, the appeal is
  determined).
  -----------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 73(1C)**
  The comptroller shall not exercise the power under subsection (1A) without giving the proprietor of the patent an opportunity to make any observations and to amend the specification of the patent without contravening section 76.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 73.04.1 {#ref73-04-1}

Proceedings under s.73(1A) will be considered where an opinion issued
under section 74A has concluded that the invention in a granted patent
is not new or does not involve an inventive step. The patent cannot be
revoked until EITHER the period for requesting a review of the opinion
(which is three months from the date of issue of the opinion) has
expired and no review has been requested OR, where a review has been
requested, until after a decision has been made on the review (and if
there is an appeal against that decision, the appeal has been
determined) and that decision and any appeal has upheld the opinion.
However, action under section 73(1A) may be initiated during the period
for requesting a review. Action should not be initiated if a review has
already been requested.\
\
\[Once the opinion has been issued, Tribunal Section should send a
message to the group head (who handles the relevant subject matter), who
should consider whether action under section 73(1A) is necessary. Action
under this section should only be initiated if the group head considers
that the patent is clearly invalid due to lack of novelty or inventive
step. The opinion should be considered but the group head is in no way
bound by it.\]

### 73.04.2 {#section-3}

Where the patent is a European patent (UK), the group head should ensure
that they are referring to the latest version of the patent, which could
have been amended under the central limitation procedure at the EPO.

### 73.04.3 {#section-4}

If, after the relevant date, action under s.73(1A) appears necessary,
the procedure set out in [73.03 above](#ref73-03), and described in more
detail below, should be followed.\
\
(a) The group head should report in a minute whether or not action is
justified under s.73(1A) and refer the case to Tribunal Section who will
arrange for an appropriate entry in the Register, for the appropriate
letter to be issued, and for the case to be referred back to the group
head on receipt of any amendments and/or observations.\
\
The group head should add the minute to the dossier, addressing the
minute to Litigation Section, and a PDAX message should be sent to the
Litigation mailbox. The minute should also confirm that the group head
is content for the case not to be referred back to them in the event
that the proprietor fails to reply (see point (e) below).\
\
(b) If the group head is satisfied after consideration of any amendments
and/or observations submitted by the proprietor that revocation is not
necessary, they should report this in a minute and the case should be
referred to Tribunal Section. The group head should confirm that any
amendments are allowable and that Decision Form 1A or 2A can be
prepared. Where amendments have been allowed, Litigation Section should
effect them in the specification and prepare a certificate for signature
by the group head. The case should be referred to Publishing Section for
the issue of a 'C' publication as described in
[27.20](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-20).
Unless the amendments can be dealt with in the 'C' publication by means
of a schedule, Litigation Section should firstly obtain a retyped
specification from the patentee under r.35(6).\
\
The group head should add the minute to the dossier, addressing the
minute to Tribunal Section, and a PDAX message should be sent to the
Litigation mailbox\
\
(c) If amendments and/or observations are received but the group head
cannot be satisfied that the objection is overcome, and further
amendments appear possible, a further period may be specified in order
to allow such amendments to be filed. The time periods specified may be
extended if appropriate under s.117B and rule 109. If no agreement can
be reached, a hearing should be offered to be taken by the DD. If the DD
decides, as a result of the hearing, that the patent should be revoked,
he should issue a reasoned decision to that effect.\
\
The group head should record their objection(s) in a minute which should
be added to the dossier and addressed to Litigation Section, and a PDAX
message should be sent to the Tribunal mailbox. The objection(s) should
be stated in a form which is suitable for inclusion in an official
letter, which will be sent over the signature of a member of Tribunal
Section but will give the name and telephone number of the group head.
Tribunal Section will inform the proprietor of the latest date for
response and will deal with requests for extensions of time periods.\
\
(d) Where the proprietor responds but raises no objection to revocation
of the patent or leaves the question of revocation to be decided by the
comptroller, Litigation Section should prepare Decision Form 3A and
refer the case along with the Decision Form to the group head for their
signature.\
\
(e) If the proprietor fails to reply Tribunal Section should issue
Decision Form 5A on behalf of the group head. \]

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 73(2)**
  If it appears to the comptroller that a patent under this Act and a European patent (UK) have been granted for the same invention having the same priority date, and that the applications for the patents were filed by the same applicant or his successor in title, he shall give the proprietor of the patent under this Act an opportunity of making observations and of amending the specification of the patent, and if the proprietor fails to satisfy the comptroller that there are not two patents in respect of the same invention, or to amend the specification so as to prevent there being two patents in respect of the same invention, the comptroller shall revoke the patent.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 73(3)**

  The comptroller shall not take action under subsection (2) above
  before\
  (a) the end of the period for filing an opposition to the European
  patent (UK) under the European Patent Convention, or\
  (b) if later, the date on which opposition proceedings are finally
  disposed of; and he shall not then take any action if the decision is
  not to maintain the European patent or if it is amended so that there
  are not two patents in respect of the same invention.
  -----------------------------------------------------------------------

### 73.05 {#ref73-05}

Subsections (2) to (4) empower the comptroller to revoke a 1977 Act
patent which is for the same invention as a European patent (UK), in
certain circumstances, in order to avoid having two patents in force in
the UK for that invention. However, conflict with a granted European
patent (UK) does not constitute a reason for the comptroller to refuse
to grant a patent in the UK.

### 73.06 {#section-5}

The tests for determining whether the two patents relate to the same
invention are the same as for deciding whether two UK applications are
in conflict [see
18.91-18.97.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-91).

### 73.07 {#ref73-07}

If an examiner becomes aware of a European application or patent (UK),
or an international application designating GB (European Patent), which
apparently relates to the same invention as an application with which
they are dealing, both applications being filed by the same applicant
and having the same priority date, they should warn the applicant that
if this application proceeds to grant (after entering the regional phase
in the case of an international application), revocation under s.73(2)
may subsequently need to be considered. An opportunity to withdraw the
UK application should not be offered if the application has already been
marked in order for grant. For the case when the conflicting application
is an international application designating the UK (i.e. not a European
patent (UK)), [see
18.91](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-91).

\[The examiner should ensure that they are referring to the latest
version of the European patent (UK), as it could have been amended under
the central limitation procedure at the EPO.\]

\[ A warning to the applicant should be given by adding clause RC31 to
the examination report under s.18(3) or to EL3 in the appropriate
circumstances regardless of whether the corresponding EP(UK) patent has
already been granted. If the potential conflict is with an international
application designating EP(UK) which has not yet entered the regional
phase, RC31 will be used with the WO publication number \]

\[ If the application is in order an intention to grant letter will be
issued by the examiner assistants. The examiner should indicate that an
RC31 clause is needed in the intention to grant letter by checking that
box in the grant form and adding the associated EP or WO publication
number in the corresponding box [(see
18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86))
This ensures the applicant is notified about possible conflict (see
18.81 and 18.86.2 for detail on the intention to grant process).\]

\[ Examiners can determine whether an international application has
entered the regional phase by checking on the European Register, e.g.
using the European Patent Register (the COPS function DIS EQU cannot be
used to obtain this information). An international application will
normally be recorded on the European Patent Register as having entered
the regional phase within 40 months from the priority date. The COPS
function DIS FUL will give information about an EP(UK) once granted,
including about the filing of any opposition proceedings. If there is
reason to suspect that the relevant record is inaccurate or incomplete,
the matter should be referred to Patents IT BAU Team. \]

\[ Restoration and Post-grant section (RAPS) will identify all granted
GB patents that have granted EP(UK) equivalents and maintain an active
electronic report of these cases showing the status of each. For those
GB patents where an assessment of conflict has been made, this RAPS
report will indicate the action taken (for example: "no action", "PL3
issued", "revoked", etc.). The report will be periodically updated by
cross-referencing IPO records with data received from the EPO to
identify GB patents with potential s.73(2) conflict. For each newly
identified GB patent, RAPS will first determine if the EP equivalent is
in the opposition period. If so, no further action can be taken under
s.73(3)(a), and RAPS will therefore indicate the earliest date on which
the case should be re-inspected. Where opposition proceedings are
ongoing, further action will again be deferred until such proceedings
are disposed of under s.73(3)(b). If the opposition period for the EP
equivalent has ended (or opposition proceedings have been disposed of),
RAPS will then determine whether the EP equivalent no longer designates
UK, or has been refused, revoked or ceased. In these circumstances
action under s.73(2) does not need to be initiated. It is possible to
ascertain whether the European patent application has been refused,
withdrawn or treated as withdrawn or the UK designation has been
withdrawn (thus removing the possibility of section 73(2) action) from
the same COPS function. Furthermore, the European patent may have been
centrally revoked at the EPO. It can be determined if this is the case
by checking on the European Register, e.g. using the European Patent
Register All enquiries as to register status should be recorded in a
minute.\]

\[RAPS will determine if an application to surrender the EP(UK) has been
made before its date of grant (see 73.11-12 for where an offer to
surrender the EP(UK) patent has been made). If no application to
surrender has been made prior to grant, RAPS will then determine if the
EP patent designates UK; if no UK designation is present, no action is
required and RAPS will record this accordingly in their report. If the
EP patent designates UK, an assessment of conflict will be necessary.\]

\[The above procedure will be used even where either or both of the GB
or EP(UK) patents have ceased due to non-payment of renewal fees. Under
these circumstances there will have been a period of time where both
patents will have been in force at the same time (and therefore
potentially in conflict), which the applicant retains legal rights to
even though the patent has ceased. [See 73.10](#ref73-10). \]

### 73.08 {#ref73-08}

EPC a.99 and s.25(1) is also relevant.

In accordance with subsection (3), action to revoke a patent under
s.73(2) cannot be taken until after either the date of expiry of the
period within which an opposition to the European patent may be filed,
ie, nine months from the publication of the mention of the grant in the
European Patent Bulletin, or the date when any opposition proceedings
are disposed of by a decision to maintain the European patent, or, if
later than either of these dates, the date when notice of grant of the
UK patent is published in the Journal. The amended section removes any
doubt which there might previously have been as to what should happen if
the European patent (UK) failed as a result of opposition proceedings:
it is now made clear that revocation action should not be taken. The
"decision" in question in s.73(3) is that of the EPO in determining
opposition proceedings (Citizen Watch Ltd's Patent \[1993\] RPC 1).\
\
The date of publication of the mention of the grant in the European
Patent Bulletin is the same as the date of publication by the EPO of the
specification as granted, this date being given on COPS ([see
73.07](#ref73-07)).

### 73.09 {#ref73-09}

If, after the relevant date ([see 73.08](#ref73-08)), action under
s.73(2) appears necessary, this should be taken as described below in
paragraph [73.09.1](#ref73-09-1). Although under previous practice it
would have been considered unnecessary to take action under s.73(2) if a
UK patent had been surrendered or have lapsed, action under s.73(2)
should be taken for all granted UK patents, even if they have been
surrendered or have lapsed. Similarly, it would have previously been
considered unnecessary work if an offer to surrender the UK patent had
been made, unless the offer was withdrawn or refused; however s.73(2)
action should be taken on these UK patents too.

### 73.09.1 {#ref73-09-1}

Action should be initiated by the issue of Letter Clause PL3. This gives
a period of two months (reducible at the comptroller's discretion) for
the proprietor to submit amendments and/or observations.\
\
(a)

\[ (a) RAPS will create a "Section 73(2)" PAFS action and forward the
case via PDAX to an examiner assistant for a preliminary assessment of
conflict. If the examiner assistant identifies any identically worded
independent claims, they will report this in PROSE form GBEPCONF and
send the associated checklist via a PDAX message (S73(2) -- CHECKLIST
CONFLICT REPORT Examinername) to the RAPS mailbox. RAPS will then
complete the PAFS action, and arrange for the action to be noted in the
Register and for the Letter Clause PL3 to be issued. The claims of the
EP(UK) may be centrally amended before the EPO, and care should be taken
to ensure that the latest version of the claims is considered when
assessing the requirement for revocation action under section 73(2).

If the examiner assistant cannot identify any identically worded
independent claims, they will report this to RAPS and the PAFS action
will be transferred to the examiner, who will make their assessment of
conflict and report their findings in PROSE form GBEPCONF, sending the
associated checklist via a PDAX message (S73(2) -- CHECKLIST CONFLICT
REPORT Examinername) to RAPS once complete. Where conflict is present,
the PROSE form should indicate that revocation action should commence in
respect of the patent, outlining the reasons why claim conflict exists.
The content of PROSE form GBEPCONF should be recorded in a form which is
suitable for inclusion in the official PL3 letter, which will be sent
with the signature of a member of RAPS but will give the name and
telephone number of the examiner.

If no conflict is present, the examiner should record their reasons why
on the GBEPCONF form.

Note that only examiners can report a status of "no conflict". If
amendments or observations are made in a response to the PL3 letter,
RAPS will refer them to the examiner and create a new PAFS action.\]

\[ (b) If amendments and/or observations are submitted by the proprietor
and the examiner is satisfied that the UK patent and the European patent
(UK) are no longer in conflict, they should report this in PROSE form
GBEPCONF and the case should be referred to RAPS. RAPS will create a
minute and send a message to the group head or group head in charge of
the group of the examiner considering the amendment. The group head will
confirm whether the amendment is acceptable and that Decision Form 1B,
1C or 2 can be prepared. Where amendments have been allowed, RAPS should
effect them in the specification and prepare a certificate for signature
by the group head. The case should be referred to Publishing Section for
the issue of a 'C' publication as described in 27.19. \]

\[ (c) If the proprietor offers amendments and/or observations but fails
to satisfy the examiner that the UK patent and the European patent (UK)
are no longer in conflict, but further amendments appear possible, a
further period may be specified in order to allow such amendments to be
filed. The time periods specified may be extended if appropriate under
s.117B and rule 109. If no agreement can be reached, a hearing should be
appointed (see 73.03.1) to be taken by group head. A report should be
made to the group head before the hearing. If the group head decides, as
a result of the hearing, that the patent should be revoked, they should
issue a reasoned decision to that effect.\]

\[ The examiner should record their objection(s) in PROSE form GBEPCONF
and a PDAX checklist message should be sent to the RAPS mailbox. The
objection(s) should be stated in a form which is suitable for inclusion
in an official letter, which will be sent over the signature of a member
of RAPS but will give the name and telephone number of the examiner.
RAPS will inform the applicant of the latest date for response and will
deal with requests for extensions of time periods. \]

\[ (d) Where the proprietor responds but raises no objection to
revocation of the patent or leaves the question of revocation to be
decided by the comptroller, RAPS should prepare Decision Form 3 and
refer the case along with the Decision form to the group Deputy Director
or group head for their signature. \]

\[ (e) If the proprietor does not respond to Letter Clause PL3, RAPS
should issue Letter Clause PL5 offering a hearing and, if there is still
no response from the proprietor, RAPS should prepare Decision Form 6 and
refer the case along with the Decision Form to the group head for their
signature. \]

### 73.10 {#ref73-10}

EPC r.39, a.105a and s.27(3) is also relevant

It is not possible to withdraw the UK designation after grant of the
European patent. Nor does s.73(2) allow the comptroller discretion
instead to revoke the European patent (UK). One way envisaged by s.73(2)
to avoid revocation of the UK patent is to amend the specification of
the UK patent to remove the conflict. Such amendment is deemed to take
effect from the date of grant, so that any double grant that may have
taken place will be nullified. Any amendments offered under s.73(2) are
not advertised since s.73 proceedings are ex parte and so third parties
cannot intervene. A further alternative available to the proprietor may
be to request central revocation of the European patent before the EPO
under EPC Article 105a. This allows the proprietor to request that their
patent is revoked in all Contracting States in which the patent is in
force. If the request is allowed, the revocation takes place ab initio
having the effect of nullifying any double grant. Another course open to
the patentee seeking to avoid revocation under s.73(2) is to make
observations. In Henry Reed v Sir James Laing & Sons Ltd (BL C/74/96)
Laddie J allowed amendment of the European patent (UK) under s.27 to
avoid s.73(2) conflict even though the patent had lapsed. In this case
Laddie J did not view a delay of 2½ years in seeking amendment enough to
justify refusing leave to amend since the question whether or not the
patent protection should proceed under the UK patent or the European
patent (UK) or whether both could survive side by side was much more a
matter of patent formality than substance.

Another option available to the proprietor may be to amend the European
patent (UK) under s.27 to remove the conflict [see
27.03-27.06](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-03)
and
[27.12-27.1](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-12)
for further details of this process). In such circumstances, the s.27
proceedings should have a clear timetable in order to expedite the
s.73(2) proceedings. The procedure for advertising the notice of such
amendments in the Journal is the same as that of other requests to amend
under s.27 [see
27.20](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-20)
The patent proprietor also has the option of amending his patent
centrally before the EPO. If this course of action is taken then the
time periods specified by the comptroller will still apply ([see
73.09.1](#ref73-09-1)). If the patent in question has lapsed; the effect
of the patent would be altered for the time it was in force as well as
if it were to be restored. If s.28 proceedings are commenced for
restoration of a lapsed European patent (UK) in case the corresponding
UK patent gets revoked, but are not pursued following successful
disposal of the s.73(2) proceedings, refund of the s.28 proceedings fee
is not normally appropriate. [See 73.11-12](#ref73-11) for a further
option open to the patentee.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 73(4)**
  The comptroller shall not take action under subsection (2) above if the European patent (UK) has been surrendered under section 29(1) above before the date on which by virtue of section 25(1) above the patent under this Act is to be treated as having been granted or, if proceedings for the surrender of the European patent (UK) have been begun before that date, until those proceedings are finally disposed of; and he shall not then take any action if the decision is to accept the surrender of the European patent.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 73.11 {#ref73-11}

Section 73(4) gives an opportunity to avoid revocation of the UK patent
by, before grant of the UK patent, offering to surrender the
corresponding European patent (UK). Surrender differs from revocation in
that it is not fully retrospective in effect. There may, therefore, be a
period in which two patents are in force for the same invention, but as
one of those patents will be under notice of surrender throughout that
period, nobody will be misled. However, once objection under s.73(2) has
been made, revocation of the UK patent cannot be avoided by allowing the
EP(UK) patent to lapse by non-payment of renewal fees, irrespective of
whether the EP(UK) lapsed before or after the grant of the corresponding
UK patent (Albright & Wilson Ltd's Patent [BL
O/190/92](https://www.gov.uk/government/publications/patent-decision-o19092)
and Citizen Watch Ltd's Patent \[1993\] RPC 1).

### 73.11.1 {#section-6}

For the avoidance of doubt, s.73(4) makes it clear that the date at
which the UK patent is to be treated as granted for this purpose is
governed by s.25(1). In other words, for the purpose of the s.73(4), as
with all provisions of the Act following s.25(1), the effective date of
grant is the date on which the notice of grant appears in the Journal.
The grant letter informing the applicant under s.18(4) that the UK
application complies with the requirements of the Act and Rules and that
a patent is therefore granted is issued several weeks before this date.
This letter also informs the applicant of the date on which notice of
the grant will be published in the Journal. Receipt of this letter
therefore potentially gives the applicant a short window of time in
which they can offer to surrender the European patent (UK) under s.29,
secure in the knowledge that the UK patent will shortly come into force.

### 73.12 {#section-7}

Revocation action should not be initiated if an offer to surrender the
European patent (UK) under s.29 has been made and either been accepted
or is still pending when the UK patent is granted. Such an offer should
follow the procedure in 29.02-06. If however the offer is refused,
action under s.73(2) should then be taken in the usual way. Where there
are ongoing court proceedings which could result in an EP(UK) being
revoked, this does not prevent revocation action being initiated under
s.73(2) against a conflicting UK patent.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
