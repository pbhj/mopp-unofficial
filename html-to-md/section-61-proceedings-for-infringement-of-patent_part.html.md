::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 61: Proceedings for infringement of patent {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (61.01 - 61.25) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 61.01

s.69 is also relevant (:#ref-61)

This section provides for proceedings to be brought by the proprietor of
a patent in relation to allegedly infringing acts and lays down some of
the powers of the court and of the comptroller in such proceedings
before each. It covers acts which are infringements of patents granted
under the 1977 Act and, in respect of patent applications which have
been published but not yet granted, acts which would have been
infringements if a patent had already been granted under the Act.
Proceedings cannot be commenced until notice of grant of the patent has
been officially published, [see
25.02](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-02).
In Hartington Conway Ltd's Patent Applications \[2004\] RPC 7, the term
"proprietor" was held not to mean "registered proprietor"; the statutory
right of action was conferred on the person who could trace their title
as set out in s.7.

### 61.02

For the applicability of s.61 in relation to European patents, [see
60.02](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-02).

### 61.03 {#ref61-03}

s.74(1) and s.74(3) is also relevant.

In order to refute an accusation of infringement, the validity of the
patent in s.74(3) question is often challenged. The validity of a patent
may be put in issue by way of defence in s.61 proceedings on any of the
grounds on which the patent may be revoked under s.72. These grounds are
set out in s.72(1), [see
72.03](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-03).

### 61.04 {#section-2}

s.61(6) is also relevant.

In s.61 proceedings, when determining whether to grant the relief
claimed and the extent thereof, the court and the comptroller should
each apply the principles applied by the court in relation to the same
kind of relief immediately before the appointed day (1 June 1978),
subject to other provisions in this Part of the Act.

  -----------------------------------------------------------------------
   

  **Section 61(1)**

  Subject to the following provisions of this Part of this Act, civil
  proceedings may be brought in the court by the proprietor of a patent
  in respect of any act alleged to infringe the patent and (without
  prejudice to any other jurisdiction of the court) in those proceedings
  a claim may be made -\
  \
  (a) for an injunction or interdict restraining the defendant or
  defender from any apprehended act of infringement;\
  \
  (b) for an order for him to deliver up or destroy any patented product
  in relation to which the patent is infringed or any article in which
  that product is inextricably comprised;\
  \
  (c) for damages in respect of the infringement;\
  \
  (d) for an account of the profits derived by him from the
  infringement;\
  \
  (e) for a declaration or declarator that the patent is valid and has
  been infringed by him.
  -----------------------------------------------------------------------

### 61.04.1 {#ref61-04-1}

In the assessment of damages in an action for infringement where the
defendant knew, or had reasonable grounds to know, that they had engaged
in the infringing activity, the factors set out in regulation 3 of The
Intellectual Property (Enforcement, etc.) Regulations 2006 (SI 2006 No.
1028) should be taken into account. This implements article 13 of
Directive 2004/48/EC on the enforcement of intellectual property rights,
and requires the damages awarded to the claimant to be appropriate to
the actual prejudice they suffered as a result of the infringement. When
awarding such damages, all appropriate aspects shall be taken into
account, including in particular, negative economic consequences (e.g.
any lost profits which the claimant has suffered and any unfair profits
made by the defendant), and elements other than economic factors (e.g.
the moral prejudice caused to the claimant by the defendant).
Alternatively, where appropriate, the damages may be set on the basis of
royalties or fees which would have been due had the defendant obtained a
licence.

### 61.04.2 {#ref61-04-2}

s.61(2) is also relevant

Following the Supreme Court judgment in [Virgin Atlantic Airways Limited
v Zodiac Seats UK Limited \[2013\] UKSC
46](http://www.bailii.org/uk/cases/UKSC/2013/46.html){rel="external"},
revocation or amendment of the infringed patent can affect any damages
payable by the infringer. The Supreme Court judgment overturns that of
the Court of Appeal in [Unilin Beheer BV v Berry Floor NV and Others
\[2007\] EWCA Civ
364](http://www.bailii.org/ew/cases/EWCA/Civ/2007/364.html){rel="external"}.

### 61.04.3 {#ref61-04-3}

In [Les Laboratoires Servier & Anor v Apotex Inc & Ors (Rev 1) \[2014\]
UKSC
55](http://www.bailii.org/uk/cases/UKSC/2014/55.html){rel="external"},
Apotex sought damages on the basis that Servier's EP(UK), under which
Apotex had been subject to an interim injunction, had subsequently been
found to be invalid. Because Apotex had separately been found to
infringe Servier's Canadian patent, Servier argued that Apotex could not
recover the damages sought in the UK, relying upon the principle that a
party cannot benefit from their own illegal conduct (ex turpi causa).
However, Lord Sumption JSC concluded that the illegality ("turpitude")
must be one that "engage\[s ...\] the public interest" and that patent
infringement was a breach of private rights which only affected the
patentee's interests. Thus the ex turpi causa principle did not apply,
and did not prevent damages from being sought in those circumstances.

### Proceedings before the court

### 61.05 {#section-3}

s.61(2) is also relevant

In proceedings for infringement of a patent before the court, the
proprietor may claim an injunction (or interdict) restraining
infringement, an order to deliver up or destroy infringing articles,
damages [(see 61.11)](#ref:61-11), an account of profits and a
declaration (or declarator) that the patent is valid and infringed.
However, the court cannot both award damages and order an account of
profits in respect of the same infringement. (Interdict and declarator
are the Scottish terms).

### 61.06 {#section-4}

The procedure in an action before the court is in general outside the
scope of this Manual, and is governed by the Civil Procedure Rules.

### 61.07 {#ref61-07}

Court proceedings for infringement of a patent may be stayed pending the
outcome of proceedings under ss.71 and 72 before the comptroller (Hawker
Siddeley Dynamics Engineering Ltd v Real Time Developments Ltd \[1983\]
RPC 395) . In [IPcom GmbH & Co Kg v HTC Europe Co Ltd & Ors \[2013\]
EWCA Civ
1496](http://www.bailii.org/ew/cases/EWCA/Civ/2013/1496.html){rel="external"}
the Court of Appeal provided guidance on deciding whether to allow a
stay of legal proceedings on the ground that there are parallel
proceedings pending in the EPO contesting the validity of the patent
([see
71.02](/guidance/manual-of-patent-practice-mopp/section-71-declaration-or-declarator-as-to-non-infringement/#ref71-02)).

### 61.08 {#section-5}

s.130(1) and CPR 63.3 are also relevant

The "court" means the High Court (or the Court of Session in Scotland)
and, for England and Wales, the patents county court. However, all High
Court s.61 proceedings in England and Wales are assigned to the Chancery
Division of the High Court and taken by the Patents Court.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 61(2)**
  The court shall not, in respect of the same infringement, both award the proprietor of a patent damages and order that he shall be given an account of the profits.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 61(3)**
  The proprietor of a patent and any other person may by agreement with each other refer to the comptroller the question whether that other person has infringed the patent and on the reference the proprietor of the patent may make any claim mentioned in subsection (1)(c) or (e) above.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Proceedings before the comptroller

### 61.09 {#section-6}

s.61(5) is also relevant

If the proprietor and an alleged infringer agree to do so, they may
refer the question of infringement to the comptroller. Unless the
comptroller declines to do so ([see 61.24](#ref61-24)) the comptroller
will determine whether the alleged infringer has in fact infringed the
patent. However, the powers of the comptroller are more limited than
those of the court, and the proprietor can claim only the reliefs
mentioned in s.61(1)(c) (damages in respect of the infringement) and (e)
(a declaration that the patent is valid and infringed).

### 61.10 {#section-7}

It is customary to seek, in the first instance, a finding with regard to
validity and infringement and an order for an enquiry as to damages; the
amount of damages is then determined in separate proceedings.

### 61.11 {#ref:61-11}

The Intellectual Property (Enforcement, etc.) Regulations 2006 (S.I.
2006 No. 1028) provides some guidance on the assessment of damages in an
action for infringement [(see 61.04.1)](#ref61-04-1).

### 61.12 {#section-8}

s.74(7) is also relevant

Where proceedings are pending in the court under any of sections 58, 61,
69, 70-70F, 71 and 72, no proceedings may be instituted before the
comptroller under s.61(3) without the leave of the court (see also
[61.07](#ref61-07) and
[74.07](/guidance/manual-of-patent-practice-mopp/section-74-proceedings-in-which-validity-of-patent-may-be-put-in-issue/#ref74-07)).

### Procedure

### 61.13 {#section-9}

PR part 7 is also relevant

A reference to the comptroller under s.61(3) is made jointly by the
parties thereto on Patents Form 2, accompanied by a joint statement
giving full particulars of the matters which are in dispute and of those
on which they are in agreement, and the capacity in which each is a
party to the reference. The application should be accompanied by a
statement of grounds in duplicate (see [61.14](#ref61-14) and
[61.22](#ref61-22). This starts proceedings before the comptroller, the
Procedure for which is discussed at [123.05 --
123.05](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).

Where validity is not the only matter in dispute

### 61.14 {#ref61-14}

r.73(3) is also relevant

The proprietor of the patent or an exclusive licensee should file the
statement of grounds, thus becoming the claimant in the proceedings.

### 61.15 {#section-10}

\[deleted\]

### 61.16 {#section-11}

r.73(3) and r.82(1) are also relevant

The other party (the defendant in the proceedings before the
comptroller) r.82(1) may in their counterstatement challenge the
validity of the patent or part thereof [(see 61.03)](#ref61-03). If they
do, the claimant may be allowed to file a further statement setting out
the grounds on which they contest the allegation of invalidity. The
further statement may also include an application to amend the
specification under s.75 ([see 75.04 to
75.08](/guidance/manual-of-patent-practice-mopp/section-75-amendment-of-patent-in-infringement-or-revocation-proceedings/#ref75-04)).

### 61.17 {#section-12}

\[deleted\]

### 61.18 {#section-13}

\[deleted\]

### 61.19 {#section-14}

\[deleted\]

### 61.20 {#ref61-20}

s.65 is also relevant

If the validity of the patent in question is contested to any extent and
the patent is found to be wholly or partially valid, the hearing officer
acting for the comptroller may certify the finding and the fact that its
validity was so contested.

### 61.21 {#section-15}

In determining whether or not infringement has occurred, regard is had
to the provisions of ss.60 and 64 as to what constitutes infringement.
Regard is also had to the provisions of ss.46(3)(c), 62, 63 and 68 which
inhibit the award of damages in various respects.

### Where only validity is in dispute

### 61.22 {#ref61-22}

r.73(3) is also relevant

The procedure where only validity is in dispute is generally similar to
that in [61.14](#ref61-14) to [61.20](#ref61-20) except that the other
party should file the initial statement of grounds (including full
particulars of the grounds on which invalidity is alleged) to become the
claimant in the proceedings. The proprietor's counter-statement may
include an application to amend the specification under s.75.

### 61.23 {#section-16}

s.72(5) is also relevant

A decision of the comptroller (or on appeal from the comptroller) does
not estop any party to civil proceedings in which infringement of a
patent is in issue from alleging invalidity of the patent on any of the
grounds referred to in s.72(1), whether or not any of the issues
involved were decided in that decision.

  -----------------------------------------------------------------------
   

  **Section 61(4)**

  Except so far as the context requires, in the following provisions of
  this Act -\
  \
  (a) any reference to proceedings for infringement and the bringing of
  such proceedings includes a reference to a reference under subsection
  (3) above and the making of such a reference;\
  \
  (b) any reference to a claimant or pursuer includes a reference to the
  proprietor of the patent; and\
  \
  (c) any reference to a defendant or defender includes a reference to
  any other party to the reference
  -----------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 61(5)**
  If it appears to the comptroller on a reference under subsection (3) above that the question referred to him would more properly be determined by the court, he may decline to deal with it and the court shall have jurisdiction to determine the question as if the reference were proceedings brought in the court.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Comptroller declines to deal with question

### 61.24 {#ref61-24}

CPR 63.11 is also relevant

The comptroller may consider that a question referred to them under
s.61(3) would more properly be determined by the court and decline to
deal with it. In such a case any person entitled to do must, within 14
days of the comptroller's decision, issue a claim form to the court to
determine the question. The court then determines the question as if the
reference to the comptroller were proceedings brought in the court.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 61(6)**
  Subject to the following provisions of this Part of this Act, in determining whether or not to grant any kind of relief claimed under this section and the extent of the relief granted the court or the comptroller shall apply the principles applied by the court in relation to that kind of relief immediately before the appointed day.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 61(7)**

  If the comptroller awards any sum by way of damages on a reference
  under subsection (3) above, then -\
  \
  (a) in England and Wales, the sum shall be recoverable, if a county
  court so orders, by execution issued from the county court or otherwise
  as if it were payable under an order of that court;\
  \
  (b) in Scotland, payment of the sum may be enforced in like manner as
  an extract registered decree arbitral bearing a warrant for execution
  issued by the sheriff court of any sheriffdom in Scotland;\
  \
  (c) in Northern Ireland, payment of the sum may be enforced as if it
  were a money judgment.
  -----------------------------------------------------------------------

### 61.25 {#section-17}

Section 61(7) was added by the Patents Act 2004 and came into force on 1
January 2005. This subsection enables any award of damages by the
comptroller in infringement proceedings to be recovered through the
enforcement mechanism of the county court in England and Wales (or the
equivalent mechanisms in Scotland and Northern Ireland) without the need
to bring fresh proceedings to enforce the award.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
