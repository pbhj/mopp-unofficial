::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 17: Search {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (17.01 - 17.123) last updated: January 2024.
:::

::: govuk-grid-column-two-thirds
::: {#default-id-330cc385 .gem-c-accordion .govuk-accordion .govuk-!-margin-bottom-6 module="govuk-accordion gem-accordion ga4-event-tracker" ga4-expandable="" anchor-navigation="true" show-text="Show" hide-text="Hide" show-all-text="Show all sections" hide-all-text="Hide all sections" this-section-visually-hidden=" this section"}
::: govuk-accordion__section
::: govuk-accordion__section-header
## [Introduction]{#default-id-330cc385-heading-1 .govuk-accordion__section-button} {#introduction .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Introduction\",\"index_section\":1,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-1 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-1" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Introduction\",\"index_section\":1,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.01

This section sets out the conditions necessary for an application to
proceed to search, makes provision for carrying out the search and for
reporting the results, provides that in some instances a search may not
be performed or may not be completed, specifies how the search shall be
conducted in the case of an application which relates to more than one
invention, and authorises the performance of supplementary searches.
Rule 27 is particularly relevant to this Section.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Information security: a reminder]{#default-id-330cc385-heading-2 .govuk-accordion__section-button} {#information-security-a-reminder .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Information security: a reminder\",\"index_section\":2,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-2 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-2" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Information security: a reminder\",\"index_section\":2,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.01.1

s.118(2) is also relevant.

The initial search at least for UK patent applications is carried out
before A publication, and so most of the guidance in this section
relates to pre-publication actions. Before A publication, the
application, the search report, and any other documents or information
concerning the content of the application (other than that prescribed
under s.118(3)) must be protected and must not be communicated to anyone
outside the Office other than the applicant or their designated
representative -- [see
118.16-118.16.1](/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-16)
for further guidance on these issues.

### 17.01.2

More generally, in all cases, whether pre-or post-publication,
formalities examiners should ensure that any communications (including
telephone conversations) are directed to the intended recipient.\
\

\[Where correspondence from the Office is reported as never having
arrived at its intended destination, or is reported as being misdirected
or delayed, this fact should be recorded by sending a minute to the
relevant formalities group with any relevant details. Use of r.111 to
extend a deadline may only be authorised by the relevant Head of
Administration see
[123.47.1](/guidance/manual-of-patent-practice-mopp/section-123-rules/#123-47-1).\]

  -----------------------------------------------------------------------
   

  **Section 17(1)**

  The comptroller shall refer an application for a patent to an examiner
  for search if, and only if­\
  (a) the comptroller has referred the application to an examiner for
  preliminary examination under section 15A(1) above;\
  (b) the application has not been withdrawn or treated as withdrawn;\
  (c) before the end of the prescribed period­\
  (i) the applicant makes a request to the Patent Office in the
  prescribed form for a search; and\
  (ii) the fee prescribed for the search ("the search fee") is paid;\
  (d) the application includes­\
  (i) a description of the invention for which a patent is sought; and\
  (ii) one or more claims; and\
  (e) the description and each of the claims comply with the requirements
  of the rules as to language.
  -----------------------------------------------------------------------

### 17.02 {#ref17-02}

r.27(1), r.22(2) is also relevant.

The request for search must be made on Patents Form 9A; the time r.22(2)
allowed for filing this is prescribed in r.22(7), ([see also 15.52 -
15.53](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-52))
Any further search under s.17(6) or supplementary search under s.17(8)
on an application should be requested on Patents Form 9 (for
applications filed before 1 January 2005) or Form 9A (for applications
filed after this date).

### 17.03 {#ref17-03}

Before the application is sent to an examiner for search the
requirements of s.17(1) must have been met, this includes the payment of
any excess claims fees. The excess claims fee is part of the search fee
therefore it follows that if the excess claims fee is not paid, the
Patents Form 9A is considered not to have been filed.

\[If any application does not meet the requirements of this section, on
payment of the Application Fee, these will be requested in a preliminary
examination report issued under s.15A by the Formalities Examiner ([see
15A.20](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-20))
Once the requirements of section 17(1) have been met, the Formalities
Examiner will send the application to the appropriate examining group
via the ESO.

\[The application should be inspected immediately upon receipt in the
examining group to ensure that it has been correctly allocated. Any case
found to be incorrectly allocated should be transferred as quickly as
possible. This should be done by direct enquiry on the part of the group
head or of the search examiner in charge of the heading to which the
case has been allocated. Difficult re-allocations should be settled
between group heads. When transfer has been agreed to a heading in
another examining group, a minute titled "ALLOCATED \[HEADING\]
\[GROUP\]", should be added to the dossier and the appropriate PDAX
message should be sent to the RE-ALL/BOOKING PDAX mailbox. Since it is
for the search examiner to decide the subject matter to be searched, the
dossier minute should indicate which headings have been considered and
the subject matter for which search is or is not appropriate.
Reallocations/ name transfers should not be sent to individual ESO's. If
three or more applications are being transferred, they should be sent in
bulk to the dedicated Examiner Case Transfer email address.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [\[Private applicant cases\]]{#default-id-330cc385-heading-3 .govuk-accordion__section-button} {#private-applicant-cases .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"[Private applicant cases]\",\"index_section\":3,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-3 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-3" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"[Private applicant cases]\",\"index_section\":3,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
\[Applications from private applicants are typically dealt with by the
Private Applicant Unit (PAU). Unless the application has been identified
as a potential 'no case' ([see
15A.03](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-03)),
most applications from private applicants will be sent to PAU after
preliminary examination in the normal way. PAU examiners will consider
whether the application is insufficient under s.14(3), or clearly
unpatentable under s.1(1)(c) or (d) because it lacks industrial
applicability or falls within one of the excluded categories. If the PAU
examiner considers that the application has no patentable content for
one of these reasons, then the applicant should be sent a letter under
the action before search (ABS), or action before combined search and
examination (ABCSE), procedure described in full at
[17.94.5-9](#ref17-94), informing them of the shortcomings, and giving
them the option to withdraw (with a refund of the search fee) or
continue with the application.

\[Otherwise, the PAU examiner will perform a search sufficient to
identify documents relevant to novelty and inventive step. If the
application contains subject matter that requires specialist knowledge,
PAU examiners will consult with the relevant examination group, or
transfer the case to that group.

\[If the search provides strong evidence of a lack of novelty or
inventive step, and combined search and examination has been requested,
the PAU examiner may defer the substantive examination and offer the
applicant the option of withdrawing their application and receiving a
refund of their examination fee.

\[Further information on PAU workflow and process can be found in PAU
desk notes.\]

\[If it appears that an invention is not patentable but may be suitable
for some other form of protection, care should be exercised in
suggesting this to applicants. It is important not to imply that such
protection will be obtainable, nor to indicate that the subject-matter
is appropriate to an application for design registration, without first
referring the matter to Designs Section. If, e.g.: because of urgency,
the opinion of Designs Section is not obtained, then the advice given to
the applicant should be in general terms, e.g.: that it may be possible
to take advantage of some other form of protection such as that afforded
by design registration or copyright; the pamphlet "How to register a
design" being enclosed.\]

### 17.04 \[deleted\]

### 17.05 {#ref17-05}

The search should be undertaken as promptly as possible, not only so
that the application may be sent for publication in due time ([see
16.31](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-31))
but also so that the applicant can have an early opportunity to consider
the search report and any documents cited ([see 17.104.1](#ref17-104-1))
They will then be in a position to decide whether to take any action,
such as the filing of amended claims or withdrawal, before preparations
for publication have been completed. Moreover an applicant who files an
application either without a declaration of priority or with some of the
twelve-month priority period remaining may want to receive the search
report in sufficient time to enable them to decide whether to file
abroad under the International Convention.

\[The aim should be for a search to be done as soon as possible. Where a
backlog of work begins to build up a search examiner should ensure that
their group head is made aware of the situation. The group head should
make it one of their primary objectives to ensure that the work of
searching is fairly and reasonably distributed among the members of
their group.\]

### 17.05.1 {#ref17-05-1}

If the applicant has requested for an accelerated search to be
performed, this should have been marked clearly to ensure prompt action
is taken. Requests for acceleration should be in writing, and may be
made electronically using the Office's [online patent filing
services](https://www.gov.uk/file-documents-pending-patent) (as a
covering letter). It is possible exercise discretion to accept an
acceleration request by email; however this practice should not be
encouraged. If a request for acceleration is sent direct to an examiner
then they should forward it to the examiner assistants, unless the
request relates to a further search in which case it is the
responsibility of the examiner (see square bracket below). A request for
an accelerated search, examination or CSE should be allowed if an
adequate case specific reason is given; requests giving no (or
inadequate) reasons should be refused. Where requests for accelerated
search and publication are made at the same time, but the request for
accelerated search is refused, the request for accelerated publication
should still be allowed. Once the search has been carried out and the
necessary formal requirements have been met, the application should be
sent for accelerated publication. Awareness of a potential infringer, a
need for a faster processing to secure an investor, or a need for a
granted patent in order to subsequently request accelerated processing
under the Patent Prosecution Highway (PPH) at another office ([see
18.07.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07-1))
for details of PPH processing at this Office) are likely to be accepted
as adequate reasons for acceleration. These reasons are also set out in
paragraph 6.2 of the [Patents Fast Grant
Guidance](https://www.gov.uk/government/publications/patents-fast-grant).
In addition, where the applicant requests acceleration in order to take
advantage of the Patent Box, the applicant should explain how a delay in
grant would have a significant cash flow impact because of its effect on
eligibility for the Patent Box; however, detailed financial records are
not normally required. (A significant cash flow impact would be one
which might affect solvency of the company or its ability to obtain
significant financing or to continue with investment or research
programmes). For [Green
Channel](https://www.gov.uk/patents-accelerated-processing#green-channel)
requests, the applicant must state in writing which action(s) they wish
to accelerate, and provide an appropriate explanation of how their
application relates to a "green" or environmentally-friendly technology.
Detailed investigation into such Green Channel explanations should not
be conducted, but Green Channel requests will be refused if they are
clearly unfounded, for example if the application relates to a perpetual
motion machine.

\[Acceleration requests made at the time of filing a request for a
further search should be processed directly by patent examiners, who
will decide whether the acceleration request should be accepted or
refused and are responsible for communicating this to the applicant. If
the acceleration request is accepted by the examiner, then they should
issue standard letter ACCS1-EXR. If the acceleration request is refused,
then they should issue either ACCS2-EXR or ACCS3--EXR depending on the
reason for refusal (insufficient information provided and no reason
provided, respectively). These letters can be found in the ''SL
Letters'' drop-down menu in PROSE.\]

### 17.05.2 {#section-3}

What constitutes an adequate reason for acceleration may depend, in
part, on the applicant's actions. For example, any delay in filing the
Form 9A may be taken into account, particularly if acceleration is being
requested in order to obtain a search report before the end of the
priority year, and filing a timely Form 9A earlier in the year would
have achieved this. Where search and examination would normally be
combined, similar considerations should be made when deciding whether to
allow accelerated treatment, and if this is acceded to, both the search
and examination of the application should be performed at the same time.
The applicant should always be informed as soon as possible whether the
request has been accepted or refused, and a record of this decision and
its reasons should be placed on the dossier. If the request has been
accepted, the search (or combined search and examination) report should
be issued in a timescale which is in line with any existing Agency
targets. It should be assumed, unless an explicit request is made, that
accelerated publication is not required. A regular Notice to this effect
appears in the Journal. ([See
18.07.3](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-07-3))
concerning a request for confidentiality relating to accelerated
processing of an application.)

\[A case on which accelerated search has been requested should be
identified by the appropriate dossier cover label and an acceleration
PDAX message. A GREEN CHANNEL label should also be added to the dossier
cover where the acceleration request is under the [Green
Channel](https://www.gov.uk/patents-acceleratedprocessing#green-channel),
and this should be recorded on COPS using the function Record Green
Channel Patent Application (REC GRE). The examiner assistant, consulting
the heading examiner if needed, may decide whether to allow such
requests\]

### 17.06 {#ref17-06}

A request for refund of the search fee is normally acceded to if it is
received before a report under s.17(5) has been issued. This refund is a
matter of discretion; it is not a right. Where a refund is requested
after an ABS letter has been issued [see 17.94.9](#ref17-94-9), and [see
17.96](#ref17-96) where a refund is requested after the issue of a
report under s.17(5)(b).

\[The request should be dealt with by the formalities group; if the
application has already been sent to an examining group the group should
be informed immediately. A search report should not be issued after
receipt of a request for a refund, even if the search has already been
performed.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [\[Sections 17(2) and 17(3) Repealed\]]{#default-id-330cc385-heading-4 .govuk-accordion__section-button} {#sections-172-and-173-repealed .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"[Sections 17(2) and 17(3) Repealed]\",\"index_section\":4,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-4 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-4" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"[Sections 17(2) and 17(3) Repealed]\",\"index_section\":4,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.07 {#ref17-07}

These subsections which covered preliminary examination of an
application ceased to have effect when the Regulatory Reform (Patents)
Order 2004 (S.I. 2004 No. 2357) came into force on 1 January 2005. For
applications filed after this date, preliminary examination is covered
by s.15A. For applications filed before 1 January 2005, preliminary
examination is still performed under sections 17(2) and 17(3). The
procedure for preliminary examination is described in
[15A.01](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-01%20et.seq.)

### 17.08 to 17.29 \[deleted\] {#to-1729-deleted}

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 17(4)**
  Subject to subsections (5) and (6) below, on a search requested under this section, the examiner shall make such investigation as in his opinion is reasonably practicable and necessary for him to identify the documents which he thinks will be needed to decide, on a substantive examination under section 18 below, whether the invention for which a patent is sought is new and involves an inventive step.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 17.30 {#section-4}

The search examiner must therefore put themselves in the position of the
substantive examiner and consider what kind of evidence they would
require in order to make an objection of lack of novelty or inventive
step (see also s.3). At the same time they may also need, in order to
carry out the search, to come to an opinion on such matters as clarity
of the claims and unity of invention. Also, although the search under
s.17(4) is concerned solely with providing evidence touching on whether
the invention satisfies the conditions for patentability set out in
s.1(1)(a) and (b), it may in some cases be necessary at this stage for
the search examiner to form an opinion as to whether the invention is
excluded from patentability on other grounds. The search examiner should
however act on these opinions only to the extent that this is necessary
in order to carry out the search (but see [17.67](#ref17-67),
[17.94-17.98.1](#ref17-94)). Views formed at this stage do not bind the
substantive examiner.

### 17.31 {#section-5}

It is not possible, given the wide variation in nature and scope of
protection sought in different applications, the great variety of
technical subjects and the wide divergence between classification
systems in different subject-matter areas, to give more than general
guidance as to how the search is to be performed. The search examiner is
required to make "such investigation as in their opinion is reasonably
practical and necessary", and while what is set out in the following
paragraphs will apply to most applications, an unusual case may require
an unconventional approach.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Preparing for the search]{#default-id-330cc385-heading-5 .govuk-accordion__section-button} {#preparing-for-the-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Preparing for the search\",\"index_section\":5,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-5 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-5" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Preparing for the search\",\"index_section\":5,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Assessing the invention

### 17.32 {#section-6}

In order to carry out their statutory duty the search examiner must read
as much of the specification as is necessary to obtain a clear
understanding of the essential nature of the invention. The main claim
itself may give a guide to this, particularly if it is in the two-part
form prescribed by Rule 43(1) EPC and Rule 6.3 PCT, with a preamble
setting out the state of the art followed by a statement of what
characterises the invention. (This type of claim is also sometimes
referred to as the Germanic form of claim or, in the US, a Jepson
claim). A discussion in the specification of the prior art and/or of the
problem to be solved by the invention may also provide, in conjunction
with the main claim, a good guide to the essential nature of the
invention. Neither the search examiner nor the substantive examiner is
however bound to accept the applicant's assessment of what constitutes
the inventive step.

### 17.33 {#section-7}

Where there is no discussion of the prior art in the specification the
search examiner will have to rely on their own knowledge to form an
opinion as to what the invention is. If they are unable to do this a
preliminary search may be necessary before the nature of the invention
can be established with reasonable certainty.

### 17.34 {#ref17-34}

The extent to which it is necessary to read the description and the
dependent claims will therefore depend on the case. Although the search
examiner need not study all the details of the description, they should
consider them to the extent that this is necessary to understand the
workings of the invention, the problem to be solved, the way in which
this solution is arrived at, and whether all of the features specified
in the main claim as characterising the invention are in fact
technically essential in the sense that the invention could not be
performed without them, or whether some could, without ingenuity, be
replaced by equivalents or be omitted altogether ([see
14.114-14.116.2](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-114))
It is also necessary to establish whether there is unity of invention
([see
14.157-14.168](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-157)),
particularly where there are two or more independent claims but [see
14.164](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-164);
if there is not unity the search must be directed to the first invention
claimed ([see 17.106 and 17.107](#ref17-106)). See
[19.23-19.26](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-23)
for practice relating to trade marks in patent specifications.

### 17.34.1 {#ref17-34-1}

For cases where the original claims were filed after the filing date of
the application, the search examiner should also make a prima facie
judgement of whether they are supported by the description. If support
is clearly lacking (e.g. the claims include additional features not
present in the description, or the scope of the claims is broader than
the original teaching in the description), amendment will in due course
be necessary to comply with s.14(5)(c) ([see
14.145](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-145)).
The examiner should therefore direct their search to what the claim
might reasonably be expected to cover after amendment ([see
17.36](#ref17-36)). It is essential that this is drawn to the
applicant's attention at the search stage ([see 17.83.2](#ref17-83-2))
in order for the applicant to decide how they wish to proceed, since
this defect may have serious consequences if significant amounts of
technical disclosure are unsupported.

\[A minute should be placed on the dossier to state whether late-filed
claims are supported or not. If unsupported, the internal search report
form should make clear if/how the search has been restricted. Where the
claims contain additional features not in the original description,
clause SC5 (at search stage) or SC6 (at CS&E) should be used. Private
applicant versions of both these clauses are available (SC5PA and
SC6PA).\]

### 17.35 {#ref17-35}

r.31(5)(a) and s.117(1) is also relevant.

The applicant is allowed to amend the specification with the
comptroller's consent before being informed of the search report,
although only corrections under s.117(1) or amendments to the claims
will be published in the A specification ([see
16.20-16.23](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-20)).

When amendments are proposed before the search, the search examiner
should consider their allowability, having regard to ss. 14(3), 14(5)
and 76, and consent to those found allowable. Where relevant, an
allowable amendment should be taken into account in determining the
search strategy. If, during combined search and examination, a proposed
amendment is found not to be allowable but would not, in any event,
affect the search strategy, the amendment should be refused and the
applicant advised accordingly, with reasons. Where search only is being
undertaken and either it is impracticable for the search examiner to
determine the allowability of a proposed amendment, or they consider
that it is not allowable, consent should be withheld and the applicant
advised that, pending consideration of the amendment at the examination
stage, the amendment will be treated as a voluntary amendment filed
under r.31(3) on the date of issue of the search report ([see
19.15-19.19](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-15)).
If the proposed amendment is not allowable and would have affected the
search strategy substantially, consideration should be given to
consulting the applicant to determine whether the search should proceed
in respect of the invention claimed in the application as filed.
Amendments filed before A publication and unable to be published in the
A specification will be incorporated in the specification and laid open
to public inspection after A publication.

\[The appropriate version of SC1 will usually serve to advise an
applicant that consent has been given under r.31(5)(a) to an amendment
filed before issue of the search report. Where search only is being
undertaken and consent to an amendment filed before issue of the search
report has been withheld, the other version of SC1 may be used to inform
the applicant that the amendment will be treated as a voluntary
amendment filed under r.31(3) and that it will be considered later at
the examination stage.

\[Purported claims which cannot be regarded as claims within the meaning
of s.14 (eg claims to a sum of money), or claims which have clearly been
filed on the wrong case may be replaced and if such replacement claims
are filed within the prescribed period ([see
15.50](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-50)),
they may be regarded as original claims for the purposes of the
search.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Matter to be searched for]{#default-id-330cc385-heading-6 .govuk-accordion__section-button} {#matter-to-be-searched-for .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Matter to be searched for\",\"index_section\":6,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-6 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-6" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Matter to be searched for\",\"index_section\":6,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.36 {#ref17-36}

The search should ideally cover all the subject-matter to which the
claims are directed, or to which they might reasonably be expected to be
directed after amendment ([see also 17.34.1](#ref17-34) and
[17.83.2](#ref17-83-2)). On the other hand the search must be limited by
what is practicable and reasonable. The completeness of any search is
limited by the inevitable imperfections of any classification system and
its implementation, and the vagaries of search words, and a complete
search may not be justified, bearing in mind that the cost must be kept
within reasonable bounds. The search examiner should therefore do their
best within the time that is reasonably available to minimise the
likelihood of failing to find existing highly relevant documents.

s.125 is also relevant.

### 17.37 {#ref17-37}

Although sometimes the subject-matter to be searched for will be based
on what is defined in claim 1, as interpreted with due regard to the
description ([see 17.34](#ref17-34)), there are many instances in which
a somewhat broader search will be necessary. This would be the case if
there were a later, broader, independent claim linked to it by a single
inventive concept, for example if claim 1 were directed to apparatus,
defined narrowly, and a later claim to a method, defined more broadly.
Also there may be a later claim which is formally dependent on, but is
not within the scope of, the main claim ([see
14.164](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-164)),
or there may be a specifically described embodiment, or a generalising
statement in the description, which is not consistent with claim 1 ([see
14.144](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-144)).
It is desirable that account be taken of such matters at the search
stage in order to minimise the need for further searching at the
substantive examination stage should the main claim be broadened on
amendment. Moreover, it will usually be necessary for the matter to be
searched for to be broader than claim 1 in some respects in order to
cover inventive step as well as novelty [(see also 17.50)](#ref17-50).

### 17.38 {#section-8}

If what is claimed constitutes a novel application of what can be shown
to be a known technique, material or structure, chosen for properties
useful in the claimed field, the search will need to be directed to
establishing whether there is anything in the prior art which points
towards its use in the field in question, or whether it has been
recognised as a generally available option in circumstances where the
need for those properties arises. If either of these circumstances
applies, or if it can be argued that the technique, material or
structure and its properties would form part of the common general
knowledge of the skilled person in the field in question, then the
relevant disclosure(s) should be cited for lack of inventive step. The
onus is then put on the applicant at substantive examination to argue
that the skilled person in that field would be unaware of, or have
reason to disregard that technique, material or structure.

### 17.39 {#section-9}

If all or part of what is claimed is functionally equivalent to that
which is known in the particular field, search should be directed to
establishing this equivalence, unless it appears to be sufficient to
rely on common general knowledge. If however the differences between
what is known and what is claimed are matters of design having no
functional significance it will normally be sufficient to assume that
such differences do not provide a basis for establishing an inventive
step, and no extensive search should be made for the non-significant
differences.

### 17.40 {#section-10}

If the claim is directed to a compound defined by a general chemical
formula, the search should primarily be directed to finding compounds
falling within the scope of the claim (preferably those described in the
worked examples), and secondarily to discovering documents which
disclose generic formulae overlapping the generic formula claimed. When
citing documents disclosing generic formulae, citations relating to the
same purpose as the application being searched should be cited in
preference to those where a different use is envisaged.

### 17.41 {#ref17-41}

If an invention is defined by reference to parameters (of a material or
composition) which are not usually specified in the documents searched,
and also by reference to a method of producing the material or
composition, a search for the method per se may suffice. If the claim
does not specify a method, reference data may be utilised to identify
materials likely to possess the specified parameters, with a search then
being made for such materials, and/or a search may be made for
disclosures of prior art materials akin to those exemplified in the
application and which might reasonably be expected to possess the
required properties. Prior disclosures of such likely anticipations
should be cited and the onus of distinguishing the invention thus put
upon the applicant. The same procedure may be used if difficulties arise
with the use of parameters in a method claim. ([See also
2.18­-2.20](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-18))
[3.88-3.93](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-88)
and
[14.121](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-121).

### 17.42 {#ref17-42}

If the search examiner is of the opinion, after considering the whole
specification, that the claims as they stand do not clearly define the
invention, when for example because of an inappropriate choice of
wording or terminology the claims in fact define something different
from what has been described or unwittingly embrace matter which is well
known, then the search should primarily be directed to what the search
examiner perceives to have been invented. Documents may be cited against
the claim as presently worded, but undue time should not be spent in
finding "paper citations" which are likely to be rendered irrelevant
when the claims are clarified.

### 17.43 {#section-11}

No special effort need be made to carry out a complete search in respect
of an unduly broad or speculative claim ([see
14.152-14.156](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-152)).
The search should be directed to that aspect of the claim which is
supported by the description and which it is thought likely will form
the subject of the claim if it is amended in response to an objection to
its undue breadth. For example if an electrical servosystem is described
and the claims also embrace a fluid pressure system, but it is not
apparent from the description how a non-electrical system would be made,
extension of the search beyond the electrical field would not normally
be justified. If it is not possible to predict what is likely to be
claimed after amendment or if an unduly broad and speculative claim
appears to be merely a device for giving an impression of unity of
invention, then the search examiner should use their judgement to decide
the best course of action to follow as set out [in 17.110](#ref17-110).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Acknowledged prior art]{#default-id-330cc385-heading-7 .govuk-accordion__section-button} {#acknowledged-prior-art .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Acknowledged prior art\",\"index_section\":7,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-7 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-7" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Acknowledged prior art\",\"index_section\":7,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.44 {#ref17-44}

If the specification acknowledges specific prior art, identified by
document reference, the search examiner should consider the documents
referred to, preferably before beginning the search, unless it is
reasonably certain from the context that such documents are unlikely to
be relevant to the issues of novelty and inventive step. Such declared
prior art is often the most relevant available and may render the
invention claimed obvious, or even, on occasion, anticipate it. Any such
documents considered to be relevant should be reported in the same way
as documents found as a result of the search. [(See also
17.91)](#ref17-91)

\[Where copies of acknowledged prior art documents are needed, they
should be ordered at the first opportunity. However, where it is clear
that the delay in obtaining the copies is going to be excessive, the
search examiner may perform the search before receiving them. The action
taken by the search examiner with regard to such documents and copies
thereof should be recorded on the internal search report. If a document
subsequently becomes available to the search examiner after issue of the
external search report and proves to be relevant, then the search
examiner should send a copy of it to the applicant together with SL5 or
SL5B, as appropriate.\]

### 17.45 {#section-12}

If the search examiner is unable to obtain through normal channels a
copy of an apparently relevant document they should contact the agent or
applicant and make an informal request for a copy. (In the case of a
reference to a German Gebrauchsmuster, the abstract (available in the
Science Reference Library) should first be consulted). Such a request
should still be made by the search examiner even if the search has
already been done before the normal channels are exhausted or is likely
to have been done before the request is answered. If the agent or
applicant is unwilling to comply, or if to wait for them to do so would
unduly delay s.16 publication, the matter should not be pursued. A
formal request should not be made, nor should a translation of a foreign
language document be asked for (see also
[18.10](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-10))
[89B.11](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-11).
If the document is not available to the search examiner when the
application is due to be sent for publication it may be included in the
search report if the reference in the specification indicates that the
document may be citable.

### 17.46 {#ref17-46}

Documents said to be relevant which are referred to in a letter from the
applicant or agent should be dealt with in the same way as prior art
referred to in the specification.

### 17.47 {#section-13}

Unidentified prior art referred to in the specification which appears to
be relevant to the issues of novelty and inventive step should not be
included in the search report, although it may be noted on the internal
search report and considered at substantive examination ([see
18.67](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-67))
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Using the results of an earlier search]{#default-id-330cc385-heading-8 .govuk-accordion__section-button} {#using-the-results-of-an-earlier-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Using the results of an earlier search\",\"index_section\":8,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-8 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-8" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Using the results of an earlier search\",\"index_section\":8,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.48 {#section-14}

If the application claims priority from an earlier UK application the
report of any search carried out on the earlier application should be
inspected. (Even if it is merely necessary to update the previous search
no refund of the search fee should be made). \[It is necessary for the
search examiner to be aware of any search performed on the earlier
application. Therefore, if such a search has been carried out, the
formalities examiner should annotate Form 1 and send the appropriate
message to the search examiner. The examiner may import the ISR of the
priority case into a dossier. If the earlier ISR is imported, then the
examiner should ensure that the earlier ISR document is clearly
distinguished from that of the application in question by appending the
annotation "ISR of related case". The copy function can be used to copy
it across to the new dossier in which case it will retain its original
document code. If no search has been made on the earlier application
Form 1 should be annotated accordingly.\]

\[Deleted\]

### 17.49 {#section-15}

Likewise in the case of a divisional application the search done on the
parent application should be consulted, [see
15.38](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-38).
(For refunds in such cases, ([see
15.47-49](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-47)).
It may also be useful to inspect the search report of an earlier
pertinent application referred to in the specification or found in the
course of the search.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Performing the search]{#default-id-330cc385-heading-9 .govuk-accordion__section-button} {#performing-the-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Performing the search\",\"index_section\":9,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-9 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-9" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Performing the search\",\"index_section\":9,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.50 {#ref17-50}

When the search examiner is satisfied that they have understood the
essential nature of the invention they should draft a statement of the
critical matter (search statement), any non-essential limitations in the
claims being ignored ([see 17.34](#ref17-34), [17.36-17.37](#ref17-36),
[17.42-17.43](#ref17-42), [17.53](#ref17-53) and [17.57](#ref17-57)).
The statement should as far as practicable be such that if no
anticipation of it is found it can confidently be concluded that what is
claimed is both new and inventive [(see also 17.58)](#ref17-58). That
is, if the critical matter is novel the invention claimed cannot be said
to be obvious.

### 17.51 {#section-16}

The search for the critical matter should then be made. The search
examiner must use their knowledge and experience to formulate a search
strategy, deciding which databases or other sources could contain
citable documents and establishing the order in which these should be
searched, taking into account all available classification systems and
suitable search words. This decision depends not only on the technical
character of the critical matter (whether corresponding to a broad
concept of wide application or whether highly specific in a narrow
technical field), but also on the manner in which the search material is
organised.

### 17.52 {#section-17}

When formulating the search strategy account should be taken both of the
likelihood of search in a given area being fruitful and of the amount of
effort needed to search that area. Thus search having a low (but finite)
chance of success in a particular area may be justified if the search
would be short, but not if a disproportionate amount of time would be
necessary, and may even be done before a considerably longer but
potentially more fruitful search.

### 17.53 {#ref17-53}

If possible, the search statement and search strategy should be
determined provisionally before beginning the search. Since the critical
matter depends not simply on what is disclosed and claimed but also on
its relationship with the state of the art, the search examiner must
always be prepared to revise both during the search, and in some cases a
preliminary search may be necessary before either can be decided upon.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Field of search]{#default-id-330cc385-heading-10 .govuk-accordion__section-button} {#field-of-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Field of search\",\"index_section\":10,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-10 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-10" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Field of search\",\"index_section\":10,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.54 {#ref17-54}

Although any matter (subject to certain specified exceptions), including
prior use, which forms part of the state of the art (see s.2) may be
cited in support of an argument that an invention is not new or does not
involve an inventive step, for the purposes of the investigation under
s.17(4) the search examiner is only required to consider documentary
evidence. There are no limits as to where the prior disclosure was
published. Publication anywhere in the world becomes effective on the
date on which it takes place. This publication date must (except in the
case of an application forming part of the state of the art by virtue of
s.2(3)) be earlier than the priority date of the invention being
searched. For search purposes, the priority date is that indicated in
accordance [with 17.74](#ref17-74).

In considering internet disclosures, the search examiner should cite any
documents which are considered to be highly relevant, even if no
publication date can be established or there is a possibility that the
document was published later than the priority date of the invention
being searched. Such documents should be cited under category X or Y
with an accompanying note in the covering letter to say that a
'publication' date cannot be established but that the disclosure appears
highly relevant and there may well be a related patent application which
does form part of the state of the art ([see 17.83](#ref17-83)). When
updating the search, the examiner should search for such corresponding
patent applications. Where the application in suit has not been
published the examiner should not contact the owner or author of any
such prior art in an attempt to establish a publication date. Following
publication of the application in suit examiners may contact a third
party author if they feel such action is appropriate, however
consideration should be given to the efficient processing of the
application and unnecessary investigation should be avoided.

The documentary evidence may also include descriptive matter, published
subsequently to the relevant priority date, but indicating that the
invention was known before that date. Thus the search report may include
a document containing a description of the invention together with a
statement that it was used publicly before its priority date or that an
oral description thereof was made public, e.g. in a lecture before a
learned society, before that date ([see
2.27](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-27)).
Non-documentary evidence of prior use should not formally be cited at
this stage. However, if the search examiner is aware, either from
personal experience or from information from another examiner or from a
member of the public ([see
21.18-21.19](/guidance/manual-of-patent-practice-mopp/section-21-observations-by-third-party-on-patentability/#ref21-18)),
of an instance of prior use which is apparently destructive of the
novelty or inventive step of the invention claimed, all the relevant
facts should be communicated to the applicant - provided sufficient
circumstantial details of the prior use are available to enable the
applicant to make their own independent enquiries. Therefore a vague or
anecdotal allegation of prior use should not be pursued. ([See also
18.24](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-24))
for the procedure at examination stage).

\[The letter accompanying the search report should be used for bringing
an instance of prior use to the applicant's attention. In no
circumstances should the search examiner offer to make the anticipatory
matter (or a drawing or photograph of it) available to the applicant or
to third parties, unless provided by a member of the public. This is in
order to avoid the involvement of the examiner in their personal, as
opposed to their official, capacity in the patent proceedings.\]

### 17.55 {#ref17-55}

The search examiner consults documents available through online
databases and may supplement this search with an internet search and/or
a search through paper documents held in the Office. Online databases
include patent databases (such as the European Patent Office
Documentation (EPODOC) and Derwent World Patents Index (WPI) databases)
and non-patent literature databases, which often cover specialist areas
of technology. The choice of databases and tools used to perform the
search will depend on the subject matter of the application, and should
be based upon the examiner's judgement of where relevant prior art is
likely to be found.

\[The search should not rely exclusively on CPC classification symbols
as there can be a delay between publication of non-EP/US patent
documentation and its classification in CPC and not all national
document collections are classified to that scheme (for example many JP,
KR and CN patent publications are not classified to the CPC). Therefore,
unless the search is deliberately curtailed ([see
17.83(d)](#ref17-83-d), [17.83(e)](#ref17-83-e) and
[17.117](#ref17-117)), a search should also cover documents only
classified to IPC (either by use of IPC symbols or by word-only
searching). The examiner should also consider whether a FICLA and/or FT
search would be appropriate\].

### 17.55.1 {#section-18}

Where a patent document is found during the search which is relevant for
novelty and/or obviousness, the examiner should consider whether to
perform a citation search on that document, to identify documents cited
against it, and to identify other patent documents against which it has
been cited. While such a citation search is normal practice for relevant
patent documents (ie those which are to be cited as "X" or "Y"), this
may depend on the factors such as the overall relevance of the document,
the numbers of documents cited against it and the circumstances of the
case; for example, if there are numerous novelty citations it is
unlikely to be useful to perform citation searches to identify yet
further relevant documents. Similarly, when a published case equivalent
to the application in suit is found, citations on that case should
generally be checked. This is discussed further in the discussion of the
top-up search [see 17.115](#ref17-115) as such equivalents will not
normally be published at the time of the initial search.

\[Citation searches can be carried out using COMBI, and noted in the
internal search report. For citation search against family members of
the application in suit, [see 17.115](#ref17-115)\]

### 17.56 {#section-19}

As a matter of course and as necessary, to establish common knowledge in
an art for the purpose of establishing the presence or absence of an
inventive step the search may extend to other readily accessible
published information e.g. standard text books and technical review
articles. If, in any particular case, the search examiner is aware of a
relevant prior publication, they should include it in the search report.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Search strategy]{#default-id-330cc385-heading-11 .govuk-accordion__section-button} {#search-strategy .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Search strategy\",\"index_section\":11,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-11 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-11" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Search strategy\",\"index_section\":11,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.57 {#ref17-57}

The areas most likely to contain the most relevant documents will
normally be those relating to the technical subject with which the
invention, or specific embodiments of it, is concerned. However if the
critical matter is of broad application but is claimed narrowly, it may
be appropriate first to search disclosures of general applicability, and
it may then be unnecessary to extend the search to areas more
specifically concerned with the particular manufacture claimed. For
example if the claim were directed to a vehicle body or container
comprising a particular structural joint, but the critical matter
appeared to consist of the joint per se then the primary search would be
in the areas relating to such joints. The search examiner should keep a
look-out for any documents disclosing the use of similar joints in
vehicle bodies or containers or in an art close enough for the document
to be used in conjunction with a disclosure of the joint per se to show
that it would not be inventive to use particular joints in this way. If
an anticipation of the critical matter has been found, extension of the
search to those areas concerned with the higher level of organisation
(in this case vehicle bodies or containers) may not be necessary,
particularly if a supporting document of the kind referred to in the
preceding sentence has been found. This would also be the case even if
no anticipation of the critical matter had been found if it could be
said with reasonable certainty that if a document disclosing the joint
per se did exist it would be found there.

### 17.58 {#ref17-58}

If the invention is a prima facie new organisation for which documents
would need to be found showing it to be known or obvious as a whole if
an objection were to be sustainable, then a single search in the fields
most relevant to such an organisation will generally suffice, and there
will not normally be any point in searching at different levels of
organisation. If the invention appears to be a non-inventive
combination, for example a particular element in a particular machine, a
search to establish whether the combination is known should be made as a
matter of course, unless in the search examiner's opinion the chances of
finding an anticipation are remote. If no such anticipation is found or
searched for, search should be considered for the element and the
machine per se, and/or for documents which could be used to argue the
non-inventiveness of the combination by showing that the particular
element is a conventional option in the relevant technical field. For
example if a central heating boiler having a particular fuel spraying
nozzle were claimed, and a disclosure of the same nozzle in an engine
were found, a further document describing similar nozzles in a way which
implied that they could be used in both boilers and engines would be
highly relevant. Separate search statements should be written for the
respective searches, which may need to be carried out in different
headings. If both the element and the machine are sufficiently well
known for this to be asserted without documentary evidence, and the
obviousness of making the combination can be argued from common general
knowledge, then the novelty search may suffice.

### 17.59 {#section-20}

In some circumstances it may only be feasible to make a novelty search,
ie a search limited to the claimed context even though the
characterising part of what is claimed is not peculiar to that context.
This could arise if the claim were directed to the use of a generally
applicable technique in a specific context but where the available
classification and search words do not provide a way of feasibly
searching for that technique in general and the searcher is unable
readily to identify the various contexts in which it may find use.

### 17.60 {#section-21}

When the search examiner considers that a search might usefully be made
in an area which is the field of expertise of another examiner, they
should consult and take advice on where to search and the likelihood of
finding relevant documents. The results of these consultations should be
recorded on the ISR. Consultation should be restricted to whatever is
necessary for the other examiner to understand what the search need is,
and a second analysis of the case by the other examiner should be
avoided unless the latter has reason to believe (eg because of their
greater experience) that it would be useful. The primary examiner should
normally make the whole of the search themselves. Exceptionally the
application may be sent to another examining group for search, eg where
the examiner consulted thinks that it would be more economical or
convenient for them to do the search themselves. It must however always
be remembered that avoidance of delay is of the utmost importance and
cases should be sent to another examining group for search instead of
being done by the primary search examiner themselves only when the time
allowable for completion of the search is adequate.

### 17.61 {#section-22}

The search examiner should beware of an ex post facto approach to the
subject of obviousness when deciding to extend the search to arts not
mentioned in the application. The question to be answered is not, given
the invention, in what fields might it be applied, but, given the
problem to be solved by the invention, in what analogous arts would it
be reasonable for the skilled person to seek the solution (see also
s.3).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Reconsidering strategy during the search]{#default-id-330cc385-heading-12 .govuk-accordion__section-button} {#reconsidering-strategy-during-the-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Reconsidering strategy during the search\",\"index_section\":12,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-12 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-12" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Reconsidering strategy during the search\",\"index_section\":12,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.62 {#section-23}

While searching for the critical matter the search examiner should keep
in mind the nature of any evidence which would be needed, in conjunction
with an anticipation of the critical matter, to support an argument of
lack of inventive step. If an anticipation of the critical matter, but
not of every feature of the claimed invention, has been found, the
search examiner should consider whether to search for supporting
evidence elsewhere.

### 17.63 {#section-24}

If the novelty and inventiveness of the main claim cannot be impugned
there is no point in making a special search for the subject matter of
dependent claims. It is however as well to note any disclosure of the
subject matter of a dependent claim which is found while searching the
main claim, since this could become useful if material citable against
the main claim were either found later on in the same search or came to
light subsequently.

### 17.64 {#section-25}

The search strategy should be reviewed on finding documents which
demonstrate either that the invention of the main claim lacks novelty or
is obvious or, when there is more than one independent claim, that the
claims relate to more than one invention ([see 17.88](#ref17-88)). It
should be borne in mind that while several documents may produce a
stronger argument that something forms part of the common general
knowledge (for assessing inventiveness) than a single document, in the
case of an objection of lack of novelty a multiplicity of citations may
be merely redundant.

### 17.65 {#section-26}

The search examiner should attempt to anticipate in what way the claim
is likely to be amended. Often amendments will take the form of
combining one or more of the dependent claims with the main claim; in
other cases the search examiner may be able to tell, from their
knowledge of the art and the results of their search, that a feature
described but not brought out in the claims is likely to be used on
amendment to characterise the invention. If the direction of amendment
can be predicted with any reasonable confidence, the search should be
focussed on finding documents of potential relevance to possible amended
claims. However, no search is necessary for claims which merely add
features which are trivial or conventional.

### 17.66 {#ref17-66}

If it is not possible to predict what is likely to be claimed after
amendment, the search examiner should use their judgement to decide the
best course to follow. The aim should be to minimise the need for
searching at the substantive examination stage, but there is no point in
performing at the search stage any search for which there is a strong
possibility that the effort will prove to have been misdirected. If the
invention of the main claim is shown to be not novel or inventive, and a
complete search in respect of it is not practicable, and the dependent
claims diverge from it, for example in order to embrace different
embodiments, so that while in form the claims relate to a single
invention, in practice they do not, several courses are possible: [see
17.110](#ref17-110) and
[14.164](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-164)
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Other documents found]{#default-id-330cc385-heading-13 .govuk-accordion__section-button} {#other-documents-found .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Other documents found\",\"index_section\":13,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-13 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-13" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Other documents found\",\"index_section\":13,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.67 {#ref17-67}

The search examiner should also, while searching for documents which
bear on the questions of novelty and inventive step, note any documents
which may be relevant for other reasons, for example conflicting UK or
European (UK) applications ([see
18.91-18.97.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-91))
[73.05-73.12](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-05)
or documents which support a view that the invention is nonpatentable on
other grounds or cast doubt on the validity of the claim to priority
(for example suggesting that the priority document was not the first
relevant application), or which illustrate the technical background or
contribute to a better understanding of the invention. Such documents
should be cited as "A" unless they are also relevant for the novelty or
obviousness of the searched invention ([see 17.80](#ref17-80)). No
special search should however be made for such documents unless there is
a good reason in a particular case for doing so.

  -----------------------------------------------------------------------
   

  **Section 17(5)**

  On any such search the examiner shall determine whether or not the
  search would serve any useful purpose on the application as for the
  time being constituted and\
  (a) if he determines that it would serve such a purpose in relation to
  the whole or part of the application, he shall proceed to conduct the
  search so far as it would serve such a purpose and shall report on the
  results of the search to the comptroller; and\
  (b) if he determines that the search would not serve such a purpose in
  relation to the whole or part of the application, he shall report
  accordingly to the comptroller;\
  and in either event the applicant shall be informed of the examiner's
  report.
  -----------------------------------------------------------------------

### The search report

### 17.68 {#ref17-68}

In the normal case, where the examiner has been able to carry out a
search, a report identifying the relevant documents and indicating why
they have been cited is sent to the applicant. The documents are also
listed on the front page of the published A document which also includes
a copy of the external search report(s).

\[ That information which is to constitute the search report to the
applicant ([see 17.69­](#ref17-69) [17.82](#ref17-82)) should be
recorded within the upper and lower tables of the internal search
report. Information which is intended solely for the substantive
examiner ([see 17.86](#ref17-86)­ and [17.93](#ref17-93)) should be
recorded outside these tables. The search report to the applicant should
not include an excessive number of citations. Where a very large number
of items has been found, the search examiner is expected to use their
judgement and select the most relevant. If it is difficult to determine
which are the most relevant, normally only examples should be cited and
the applicant so informed by adding a passage to the search report
letter before the paragraph about other search results ([see
17.83](#ref17-83)).The number of items cited should not in general
exceed twelve unless there is a special reason for more, although a
larger number can of course be noted outside the tables of the internal
search report.\]

\[ The Group Head or subclass examiner, after due consultation, should
arrange for an appropriate early response to issue following enquiries
from the agent or private applicant regarding the date of issue of the
search report. Any letter confined to such an enquiry is forwarded by
the formalities examiner direct to the relevant Group Head. The method
and substance of the response should be determined according to the
particular circumstances: in general, the response should be by letter
rather than telephone (plus telephone report), and may be issued by the
appropriate Formalities Manager. When it is possible to make a
relatively accurate estimate the response should specify the anticipated
month of issue. In other circumstances it may be appropriate to be less
precise eg to indicate that the report is not likely to issue before a
specified date (month). In any event, the response should indicate the
telephone number of a named contact.\]

\[ In most cases, publication of an external search report does not
require any action on the part of the search examiner. However [see
16.33](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-33)
[and 17.105](#ref17-105) for action to be taken if the search examiner
becomes aware of an error in the external search report or of an
additional citation after the external search report has issued.\]

\[ Search report covering letters SL1, SL1A, SL3, SE1, and SE3 include a
request for disclosure of unpublished search or examination reports from
other patent offices on equivalent applications. The request highlights
that early consideration of such unpublished reports may lead to more
efficient processing of the application.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Private applicants' cases]{#default-id-330cc385-heading-14 .govuk-accordion__section-button} {#private-applicants-cases .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Private applicants’ cases\",\"index_section\":14,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-14 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-14" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Private applicants’ cases\",\"index_section\":14,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
\[ The search examiner should use SL1PA to act as the covering letter
for the external search report. The Preliminary Examination Report
(using LFEPA) should be issued only if there are any formalities
objections. Paragraphs may be added to the search report covering letter
before the paragraph about other search results ([see 17.03](#ref17-03)
[and 17.83](#ref17-83)) and the search examiner should satisfy
themselves that the letter and any enclosures will make a coherent
whole. The procedure for obtaining copies of cited documents for issue
with the external search report is outlined in [17.104.1](#ref17-104-1).
Where there is plurality of invention on a private applicant case, SC13
can be used. Where there is no standard private applicant letter or
clause available, other standard letters and clauses may be used where
appropriate, with modification to suit the individual case if the search
examiner sees fit.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Field of search]{#default-id-330cc385-heading-15 .govuk-accordion__section-button} {#field-of-search-1 .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Field of search\",\"index_section\":15,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-15 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-15" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Field of search\",\"index_section\":15,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.69 {#ref17-69}

The search examiner should identify the field of search by entering the
appropriate details in the "Field of Search" table in the internal
search report. Only current IPC terms and (if used) terms from the final
edition of the UK Key (edition X) can be entered. When a search is made
through documents classified under the IPC, CPC or FICLA, the relevant
IPC sub-class(es) should be indicated in the "IPC" entry - eg G03B,
G06F. In the rare instances that the search was limited only by search
words and was not limited to any IPC or CPC sub-classes, the "IPC
sub-class" box should be left blank. The "UKC" entry should be used if
the search has been conducted through documents classified on the UK Key
(whether GB patent documents or otherwise). Only the heading(s) searched
should be recorded under the "UKC" entry - eg G3N, H2F.

\[Details of the field of search are entered into the "Field of Search"
table by opening a dialogue box while editing the internal search report
in Word; this is done by clicking on the 'Add-Ins' tab and then
selecting 'Maintain FOS'. There are two input fields for Other Field Of
Search: The 'Other Field Of Search for this ISR' is used by PROSE to
record the other field of search specific to the current ISR, while the
'Other Field Of Search total for the case on COPS' is that which is to
be stored on COPS. This is because COPS only has one free text field for
this information and different searches conducted on a case may include
different fields of search. For the first ISR on a case the examiner
should enter the same data in each field. For each subsequent ISR at the
'A' stage, the first field should be filled with the details specific to
that ISR, while the second field should be filled with an accumulation
of the data for all the ISRs up to that point, but should not contain
any duplication. When the examiner enters the details for a second ISR,
for example, the second field will already be populated with the data
recorded for the case from the first ISR and so all the examiner need to
add to the second field are further "other" fields of search, such as
databases not searched in the first search. The examiner should enter
the appropriate text ([see 17.71](#ref17-71)) or right click to display
standard texts for selection.

\[Fuller details on the field of search should be entered outside the
"Field of Search" table - usually under the heading "Other details about
field of search". For the UKC, a heading and any classifying terms
searched in it should be recorded here - eg H2F (F3C, F3D). For a search
of documents under the IPC or CPC, more detail should be given,
including sub-classes, sub-groups and all CPC terms searched - eg E05B
1/00, 1/0007, 1/003, 1/04. Other details given may include any search
using terms from earlier editions of the UKC or IPC, or an explanation
if the CPC classifications have no suitable IPC equivalent at sub-group
level.

\[Deleted\]

### 17.70 {#section-27}

If the search has been stopped at a certain date because the search
examiner considers that no relevant documents would be found earlier
than that date, for example because of the nature of the technology used
in the invention, there is no need to tell the applicant. If however the
search has been discontinued for some other reason (usually because
sufficient relevant documents have already been found) then the
applicant should be informed ([see 17.83](#ref17-83)).

### 17.71 {#ref17-71}

Databases used and document collections searched should be recorded
under the Field of Search entry "Online & other databases". Online
databases should be recorded using the [standard database
names](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/standard-titles-for-online-database-names)
e.g. EPODOC, WPI, INSPEC. When fulltext searching is conducted, this
should be listed as "Patent Fulltext". Those database names which are
also registered trademarks are acknowledged as such in the [standard
database
names](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/standard-titles-for-online-database-names)
document but not in the search report. If an internet search has been
performed, this should be recorded as such in the external search
report, but names of individual search engines that are used for the
search should not be listed unless they have been accessed through a
subscription and are not freely available. If however only one or two
websites have been looked at, then the Internet should not be listed on
the external search report because this may lead to the impression that
a significant internet search has been conducted.

\[Databases should also be recorded using PROSE; the particular
database(s) searched being indicated using the which are listed on. For
further information on internet references, see the Internet Searching
Manual.\]

### 17.72 {#section-28}

\[Deleted\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Claims searched]{#default-id-330cc385-heading-16 .govuk-accordion__section-button} {#claims-searched .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Claims searched\",\"index_section\":16,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-16 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-16" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Claims searched\",\"index_section\":16,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.72.1 {#ref17-72-1}

The search report also includes an indication of the claims in respect
of which the search has been done. In general, all claims dependent on a
main claim which has been searched should be listed by number provided
none falls outside the scope of the main claim (eg by virtue of
specifying omission of some feature(s) of the main claim). Where a
dependent claim introduces a feature which is not trivial or
conventional, this claim should still be listed in the claims searched.
However, if the search examiner considers that further searching in
respect of this feature may be required at a later stage then the
applicant should be warned in the covering letter. Independent claims
and claims dependent upon them should also be listed by number when they
fall within the scope of the search made. If the claims searched have
been amended in wording or numbering from those originally filed, their
date of filing should also be stated in the "Claims searched" box.

\[The indication of claim(s) searched should be consistent with the
indication as to which claim(s) the cited documents are considered
relevant ([see 17.77](#ref17-77)).\]

### 17.72.2 {#ref17-72-2}

Where there is lack of unity, then claims relating wholly to second or
further inventions should not be included in the "claims searched".
Claims relating to a first invention and also dependent upon claims to
subsequent inventions should be listed in the "claims searched" along
with an indication that only a partial search has been performed in
respect of these claims. In addition, independent claims should not be
searched if they relate to subject matter wholly unpatentable under
s.1(2), s.1(3) or s.4a ([see 17.94](#ref17-94) and
[17.107](#ref17-107)). However, claims which are unpatentable under
s.1(2), s.1(3) or s.4A as worded but which could form the basis of a
patentable claim -- for example, method of treatment claims which could
be converted to a valid second medical use claim -- should be searched
if they fall within the first invention. Claims which are completely
unsupported and of indeterminate scope (for example, "reach-through"
claims to compounds identified by an assay or screening method -- [see
14.156.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-156-1)
should not be searched. A brief explanation in the internal search
report and search letter as to why the claim was not searched should be
included in the internal search report and search letter. Occasionally
applications contain totally indeterminate claims, for example, directed
to "any novel matter disclosed". Such claims should be disregarded at
the search stage; no reference need be made to them in either the
external search report or the accompanying letter.

\[In the "claims searched" the examiner should record "(in part)" after
any claims which have been partially searched because they relate to a
first invention and also depend upon independent claims to subsequent
unsearched inventions\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Documents cited]{#default-id-330cc385-heading-17 .govuk-accordion__section-button} {#documents-cited .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Documents cited\",\"index_section\":17,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-17 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-17" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Documents cited\",\"index_section\":17,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.73 {#ref17-73}

The search examiner should report primarily those prior publications ­
including those acknowledged in the application - which could prove
useful to the substantive examiner when constructing an objection based
on lack of novelty or of inventive step. They should be generous in
their assessment of what the substantive examiner might consider
relevant and particularly in the absence of category X or Y documents
should also include documents illustrating background art, [see
17.80](#ref17-80). The examiner should always cite at least one document
in the first search report on an application, unless -- exceptionally --
they consider that the invention relates to a wholly new field of
technology and so no relevant documents of any kind can be found. If a
document of the kind referred to [in 17.67](#ref17-67) is included, the
reason for its inclusion should be referred to in a supplementary report
in the search letter ([see 17.83](#ref17-83)).

\[The first two columns of the lower table of the internal search report
headed "Ref" and "Location" are for the convenience of the search
examiner, who can include in the first of these a private shorthand
reference code to readily identify a citation and, in the second, the
source or location where the cited document is to be obtained or found.
The information in these columns will not be carried over to the
external search report.\]

\[In the exceptional case that no relevant documents have been cited,
"None" should be inputted in the citation field of the ISR\]

### 17.74 {#ref17-74}

Whether or not a given document forms part of the state of the art in
the case of an invention may depend on the priority date of the
invention, and also on that of the matter in the document. The priority
date may differ for different parts of the matter disclosed in an
application or for different aspects of the invention ([see
5.20-5.24](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-20))
The search examiner should however treat the actual filing date of the
application being searched as though it were the priority date of all
the matter present. In this way, if it should transpire that an
invention is not entitled to a priority date which has been declared, or
if the applicant should relinquish their priority date, this will not
cause them to be confronted with a citation of which they have been left
unaware. In order to decide whether a published application should be
included in the search report by virtue of s.2(3), the potential
citation should be treated as though all the matter in it were entitled
to the earliest priority date declared. All this is without prejudice as
to what may be the true priority date of matter in either the
application being searched or in the potential citation. (For the
practice at substantive examination [see 18.14­
18.16](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-14)).

### 17.75 {#ref17-75}

The citations should be listed according to relevance, starting with any
category X documents, followed by category Y documents and lastly
category A documents. The documents within each category should also be
listed with the most relevant (eg the one relevant to most claims)
first. The relevant passages for consideration in each document should
normally be indicated ([see 17.81](#ref17-81)). In the absence of any
significant priority (eg where all the documents are category A), UK
patent specifications should be listed first, followed by EP
specifications, PCT pamphlets, foreign patent specifications in country
code order and finally non-patent literature.

\[Details on [citation
formats](/guidance/manual-of-patent-practice-mopp/citation-formats) in
PROSE can be found online).

### 17.76 \[Deleted\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Further information]{#default-id-330cc385-heading-18 .govuk-accordion__section-button} {#further-information .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Further information\",\"index_section\":18,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-18 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-18" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Further information\",\"index_section\":18,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.77 {#ref17-77}

An indication of why each document has been included in the search
report should be given by assigning a category indicated by a code
letter (eg X, Y or A -- [see 17.78](#ref17-78)-[17.80](#ref17-80)). The
claims to which it is considered relevant should also be indicated, as
should pertinent passages wherever possible ([see 17.81](#ref17-81))).
The relevance of the document to each claim (except for any relating to
a second or subsequent invention where there is not unity of invention,
[see 17.106](#ref17-106) to [17.110](#ref17-110)) should have been
investigated and all of the claims affected should be listed on the
search form, as far as it is reasonably practicable to do so. In cases
which are complex or obscure or have a very large number of claims, it
may not be practicable to consider all of the claims in sufficient
detail for every claim affected to be identified. In any event, at least
those claims (or those of the claims which relate to the first invention
where there is not unity of invention) which are independent or which
appear prima facie to relate to new or important features or the
characterising features of particular embodiments, rather than merely
reciting standard prior art features, should have if possible received
particular attention and be listed if appropriate. While the substantive
examiner will be guided by the search examiner's assessment of the
citations, they are not bound by it, and may take a different view after
their more detailed consideration of the whole specification.

\[All claims listed against cited documents in the search report should
be included in the list of claims searched. A supplementary report
should issue in the form of an appropriately headed passage in the
search letter (before the paragraph about other search results) if only
a partial search was made in respect of any claims so listed ([see
17.72.1](#ref17-72-1)). Examiners should consider and report on the
relevance of citations to appendant claims only to the extent that it is
reasonably practicable so to do. Where it has not been practicable to
consider all of the claims, it is acceptable to qualify listed claims
with "at least". Consideration should then be given to including
comments in the internal search report form (ISRF) and search letter
explaining the extent to which the relevance of the citations to the
claims has been considered. However, a bare reference to "claim 1 at
least" (i.e to one independent claim only) should be avoided in all but
exceptional circumstances e.g. where the scope of the invention is
extremely unclear or there are a very large number of claims.\]

### 17.78 {#ref17-78}

Category X is to be used to indicate a document which is relevant when
taken alone. It may disclose (or imply - [see
18.22](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-22))
all the features specified in a claim, i.e. may demonstrate lack of
novelty. Alternatively, it may of itself indicate a lack of inventive
step - for example, where the difference between what is claimed and the
disclosure of the document is considered to be non-inventive or to be
common general knowledge, or where features of two embodiments can be
combined to show all the features of what is claimed. The essential
point about a category X document is that it can be used without the
support of other documentary evidence to attack patentability. It should
be indicated as relevant to those claims whose novelty or inventive step
is impugned by the document standing alone.

### 17.79 {#section-29}

Category Y is to be used to denote a document which is relevant when
combined with another document and which will therefore form one limb of
an obviousness argument. If a category Y document is listed against a
claim, it follows that at least one other document of this category must
also have been listed against that claim. A document may be assigned
both categories X and Y. In this regard a document may be X category in
respect of one or more claims and Y category in respect of a different
claim or claims; or may be both X and Y category against a single claim
eg because it is relevant to lack of inventive step both as taken alone
(in the light of common general knowledge) and when combined with
another document cited under category Y.

\[Each document should be listed only once on the search report and, if
it is relevant under both categories X and Y, "X, Y" should be indicated
against it. The "Claims" column should be completed by inserting "X: "
and the numbers of the claims affected under that category in one group
and inserting "Y: " and the numbers of the claims affected under that
category in a second group.\]

### 17.80 {#ref17-80}

Category A is for documents indicating technological background and/or
state of the art. No search is made for such documents, and if the
normal search reveals documents in category X and/or Y it will not
usually be necessary to cite background art as well. However, if the
search reveals nothing in category X and/or Y, one or more documents
representing the most relevant background art should normally be cited
under category A, whether these are revealed by the search or otherwise,
eg acknowledged in the application. Documents cited for other purposes
may also be assigned category A but the reason should be given in a
supplementary report in the search letter, [see 17.83](#ref17-83). Such
purposes include those referred to [in 17.67](#ref17-67) (and in
particular include citation to support an objection under s.1(2) which,
prior to the introduction of category A, was assigned category X). The
search examiner may also include any other document which they feel is
relevant to the application and should be brought to the attention of
the applicant, but does not merit category X or Y.

Where a search has been made for the common matter of two or more
independent claims in order to ascertain whether the inventions are
linked to form a single inventive concept, only documents which appear
relevant under s.1(1)(a) or (b) to the (or the first) invention claimed
should be designated X or Y; other documents found may be designated A
([see 17.83](#ref17-83),[17.106](#ref17-106) and [17.109](ref17-109)).
There is no bar on citing category A documents as well as category X or
Y documents although, since category X and category Y can each be taken
to subsume category A for most purposes, it is unlikely to be
appropriate to assign code A in addition to code X or Y to citations of
category X or Y. The listing of claims and pertinent passages against
cited documents may be dispensed with for category A documents but may
be done where the search examiner considers it would be helpful. Only
claims which fall within the scope of the search (at least in part)
should be listed against cited documents, claims relating to second or
subsequent inventions should not be listed ([see 17.72.1](#ref17-72-1)
and [17.77](#ref17-77)).

### 17.80.1 {#section-30}

Category & may be used to indicate cited documents which are members of
the same patent family (eg GB and foreign equivalent) or otherwise
correspond. Corresponding documents should only be listed where their
inclusion enhances the search report, eg by providing the English text,
by bringing matter into the s.2(2) field, or by disclosing or claiming
different aspects of the invention.

### 17.81 {#ref17-81}

Relevant passages of cited documents should always be indicated (other
than for category A documents, where this is optional, [see
17.80](#ref17-80)) except where this is clearly impracticable, for
example where there are many separate and equally relevant passages
throughout a document or where the document is short and its relevance
is self-evident. Such an indication will not in any way restrict the
substantive examiner, who will be free to rely on any part of the
document in order to support an objection.

### 17.82 {#ref17-82}

Citations in the s.2(3) field or whose effectiveness depends on their
priority date and/or that of the application in suit ([see
17.74](#ref17-74)) should be indicated by further code letters. These
are:­

P - document published on or after the priority date but before the
filing date of the application in suit. Such a document would lie in the
s.2(2) field if the invention were not entitled to the priority date
requested. On the other hand, if the claim to priority were sustained,
the document would be either in the s.2(3) field or outside the state of
the art. If a late declaration of priority is filed after search, then
documents cited in the search report may no longer form part of the art
([see
18.14](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-14)).
That is, a citation may become a 'P' document (although this will not be
indicated on the search report) as a result of the late declaration of
priority.

E - UK patent application, or European or international application
designating the UK, which has a filing or priority date earlier than,
but a publication date the same as or later than, the filing date of the
application in suit. Such a document is therefore prima facie in the
s.2(3) field but may be rendered nugatory if the applicant can sustain
their own priority date and/or attack that of the document listed. Such
a document may only give rise to an objection to the novelty of the
invention, since there is no possibility of it being in the s.2(2) field
and documents in the s.2(3) field are not relevant to obviousness.

Since these codes convey only supplementary information a document to
which they are assigned should of course also be categorised as X, Y or
A ([see 17.78-17.80](#ref17-78)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Supplementary report]{#default-id-330cc385-heading-19 .govuk-accordion__section-button} {#supplementary-report .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Supplementary report\",\"index_section\":19,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-19 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-19" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Supplementary report\",\"index_section\":19,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.83 {#ref17-83}

In some cases it may be necessary for the search examiner to add
observations, which should be as concise as possible, to the letter
which accompanies the search report. Thus, a brief explanation should be
added to the letter as a supplementary report:­

\(a\) if the claims lack unity of invention but it is thought that it
would not be self-evident to the applicant why this is so (NB the fact
that the examiner considers that the claims relate to plural inventions
should always be reported where this is the case, whether or not further
explanation is needed -- [see 17.106-17.110](#ref17-106));

\(b\) if the search examiner is not confident that the applicant will be
able to deduce the way in which cited Y category documents are to be
combined merely from information (eg identified relevant passages)
provided in the search report;

\(c\) if a document is cited which does not bear on novelty or
obviousness or does not represent the most relevant background art ([see
17.73](#ref17-73) and [17.80](#ref17-80));

\(d\) if the search examiner has conducted only a partial search or
truncated, for example because documents found in the search demonstrate
that the invention of the main claim lacks novelty and inventive step,
and it is not apparent in what way the applicant might amend, or because
the claims are insufficient due to excessive claim breadth (NB in the
case of a truncated search PROSE clause SC7 should be used in the letter
or report) -- see also
[14.79-14.82](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-79),
[17.83(e)](#ref17-83-e) and [17.117](#ref17-117);

\(e\) if the documents cited are examples only of relevant prior art, in
which case PROSE clause SC7 should be used (as appropriate) to give a
warning that a further search may be necessary at a later stage In some
circumstances it may be useful to include an explanation as to why
particular documents have been cited, for example because they show a
particular embodiment or demonstrate the wide breadth of a speculative
claim;

\(f\) why an independent claim has not been searched when it relates to
an excluded invention under s.1(2);

\(g\) where the categories X or Y have to be omitted from the search
form because an application is so obscure;

\(h\) where documents which may be relevant have been revealed by an
online database search but only an abstract has been seen and there will
be some delay in obtaining the complete document for the search examiner
to inspect. The search report should include an indication of the likely
relevance of the complete document where possible;

r.27(2) is also relevant.

\(i\) where documents which may be relevant have been revealed by an
online database search and the complete document is unavailable, where a
document is available as a library loan only, or where copyright allows
for in-office use only and no copy can therefore be provided to the
applicant;

\(j\) where the search examiner has determined that the claims do not
clearly define the invention, and so has searched what they perceive to
be the invention when taking the application as a whole ([see
17.42](#ref17-42))-- (NB PROSE clause SC7 should be used as appropriate
to highlight that a search is incomplete);

\(k\) where the search examiner considers that the application may
relate to matter excluded under section 1(2) but where action as
described [in 17.83.4](#ref17-83-4)) or [17.94.5-17.101](#ref17-94-5) is
not appropriate ([see 17.94.2-17.94.3](#ref17-94-2));

\(l\) where a 'publication' date for an internet disclosure cannot be
established but the disclosure appears highly relevant ([see
17.54](#ref17-54)).

\[The supplementary report should be one or more appropriately headed
paragraphs added to the search report covering letter before the
paragraph about other search results. In the case of an application from
a private applicant, SC3PA should be added if it is necessary to
indicate the interpretation which has been put upon unclear claims in
order to carry out the search. If the matter cited completely
anticipates the entire subject-matter of an application from a private
applicant SC4PA should be added. SC14 or SC14A should be added to report
documents revealed by an online database search but unseen. Where
partial searches only have been conducted in respect of some claims [see
17.72.1](#ref17-72-1) and [17.77](#ref17-77).

When using standard clause SC7 to report a truncated search or citation
of examples only, examiners should ensure that the clause appears
prominently i.e. on the first page of the letter or report to avoid
being overlooked.\]

### 17.83.1 {#ref17-83-1}

Where the search report has been issued on the basis of an abstract as
in 17.83(h) above, the examiner should analyse the full document when it
becomes available and send a copy or copies to the applicant as soon as
possible (whether it is then considered relevant to the invention or
not). An accompanying letter should confirm, or not, the relevance of
the document and should include any information which would if known
have been included on the search report, or indicate any differences
between what was indicated on the search report and the document's
actual relevance as appropriate ([see also
17.105-17.105.2](#ref17-105)). An amended search report should be issued
if there is a significant difference between the indicated likely
relevance and the actual relevance of the document and the application
has not yet been published. The examiner should enter particulars as
necessary on the internal search report and COPS using PROSE. If copies
of a document prove to be unobtainable, the applicant should also be
informed.

\[The application should be diarised for one month to safeguard against
the order for references going astray. This should be done by creating
an entry in the PD electronic diary system and adding the diary action
to the dossier in PDAX. This diary entry should state the examining
group after the application number in the format GB0000000.0 -- EX00.
SL14 should be issued when a document previously cited on the basis of
an abstract is considered not relevant to the claims after the full
document is available.\]

### 17.83.2 {#ref17-83-2}

If search in respect of later-filed original claims is restricted in the
circumstances discussed in [17.34.1](#ref17-34-1), then the applicant
should be advised accordingly in a supplementary report in the letter
which accompanies the search report and informed that amendment to the
claims will be necessary in due course.

\[Where the claims contain additional features not in the original
description, clause SC5 (at search stage) or SC6 (at CS&E) should be
used. Private applicant versions of both these clauses are available
(SC5PA and SC6PA).\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Examination Opinion]{#default-id-330cc385-heading-20 .govuk-accordion__section-button} {#examination-opinion .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Examination Opinion\",\"index_section\":20,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-20 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-20" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Examination Opinion\",\"index_section\":20,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.83.3 {#ref17-83-3}

While conducting the search, if major issues are identified in the
application that will require considerable amendment if the application
proceeds to substantive examination, the issue of an examination opinion
should be considered by the search examiner where combined search and
examination is not being conducted. This examination opinion is
non-statutory, but may form the basis of the first examination report
under s.18(3) at substantive examination if no response is received
([see
18.47.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-47-1)).

As such, when choosing to issue an examination opinion, the search
examiner should endeavour to ensure that the examination opinion can be
readily adopted as the first report under s.18(3) by the substantive
examiner. Typically, this will involve detailing all major objections
but in as concise a fashion as the search examiner sees fit.

### 17.83.4 {#ref17-83-4}

Examples of situations where the issue of an examination opinion will be
appropriate include complete or extensive anticipation of the subject
matter disclosed, broad speculative claims where there is no clear
indication of how the applicant may wish to proceed, complex independent
claims with overlapping scope relating to a number of separate
inventions, and inventions falling within excluded categories. However,
in the case of plurality of a more limited extent, an examination
opinion should not be issued for this reason alone. Furthermore, under
some circumstances (e.g. excluded inventions), action prior to search,
or issue of a report under s.17(5)(b) in addition to the examination
opinion may be more appropriate ([see 17.94-17.101](#ref17-94)).

\[The search examiner should use search letter SL1A as a covering letter
for the search report and accompanying examination opinion. This letter
invites amendment before substantive examination and informs the
applicant that the examination opinion will form the first report under
section 18(3) if the points in the opinion are not addressed.\]

\[The examination opinion should include clauses selected from EC1 to
EC8, which may be supplemented by further explanation if necessary or
for clarification. Where plurality of invention is present, in addition
to using clause EC6 (where plurality alone would justify an examination
opinion) or EC8 (where the examination opinion is being issued for
another reason), clause SC13 should be used in the search letter ([see
17.108](#ref17-108)).\]

\[For private applicant cases major issues such as those exemplified
above may be detailed in a supplementary report in the search letter
rather than an examination opinion ([see also 17.83](#ref17-83)).
Alternatively an examination opinion may be issued to a private
applicant if the examiner feels it is appropriate.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [International Exhibitions]{#default-id-330cc385-heading-21 .govuk-accordion__section-button} {#international-exhibitions .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"International Exhibitions\",\"index_section\":21,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-21 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-21" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"International Exhibitions\",\"index_section\":21,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.84 {#ref17-84}

If an applicant, at the time of filing an application, purports to
inform the comptroller under r.5(1) of the display of the invention at
an international exhibition the search examiner should ascertain whether
the exhibition cited by the applicant is an international exhibition
within the meaning of s.2(4)(c). If the exhibition cited does comply
with the statutory requirement ([see
2.41](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-41))
the search examiner need take no further action except to note the fact
on the internal search report. If it does not, a paragraph should be
added to the letter accompanying the search report informing the
applicant that the exhibition cited by them is not an international
exhibition within the meaning of s.2(4)(c), and that the admitted
display of the invention will be taken into consideration by the
substantive examiner during substantive examination in due course ([see
18.23](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-23)).
No reference to the display should be entered on the external search
report, but again a note of it should be made on the internal search
report.

\[Deleted\]

### 17.85 \[Moved to [17.72.2](#ref17-72-2)\] {#moved-to-17722ref17-72-2}
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The internal search report]{#default-id-330cc385-heading-22 .govuk-accordion__section-button} {#the-internal-search-report .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The internal search report\",\"index_section\":22,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-22 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-22" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The internal search report\",\"index_section\":22,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.86 {#ref17-86}

The search examiner should record on the internal search report any
information which is likely to be of use to the substantive examiner.
This should not include matters which are self-evident from the
specification or otherwise, but should provide a record of the thoughts
and strategy of the search examiner which would not be apparent to
another, or even to themselves at a later date. This should as a minimum
include a statement of the critical matter ([see 17.50](#ref17-50)),
details of the classification terms searched (including CPC and IPC
subgroups), and a list of the documents found. If comments such as
"further search may be necessary" are made, then informative reasons
should be given. Full details of the online search strategy should also
be recorded, either in the internal search report or a stand-alone
document. The way in which each document is relevant under an identified
strategy should be clear: this is particularly important with regard to
documents listed as examples. If comments such as "best of many" are
made, then a clear indication of the criterion used in the selection
should be given. If on the other hand documents cited are examples only,
then this should be clearly stated. Multiple citations, showing more or
less the same features, should not be made. If the reasons for the
particular form of the search statement are not readily apparent from
the claims searched, for example if the search has been cast more
broadly or has been restricted, for the reasons discussed in
[17.36-17.37](#ref17-36) and [17.42-17.43](#ref17-42), then a suitable
explanation should be given. Where there is more than one search
statement or strategy, it should be made clear which one produced each
of the documents listed. If any part of the search has been deferred the
reasons for this should be clearly explained. If some thought has been
given as to whether to search using a particular heading, or to consult
the examiner in charge of another heading, and it has been decided not
to do so, then this should also be noted.

\[The online search strategy may be recorded either in the internal
search report, or in an OSS (Online Search Strategy) document on PROSE.
This may be done using a history print (from the HI command on EPOQUE)
or a print-out of the full strategy, but however it is recorded it
should be clear what has been searched and which documents have been
viewed.\]

\[It is not necessary for the search examiner to make comments in every
case in respect of:

-   plural invention **if** there is only one independent claim

-   technical subject problem(s) addressed

-   any subject-matter assumed, without search, to be common general
    knowledge

-   documents found

-   further search.

\[Where a search reveals a large number of citable documents and
examples (whether "best of many" or "examples only") are cited, not only
should this be recorded on the internal search report for the benefit of
the substantive examiner but also the applicant's attention should be
drawn to the situation in a supplementary report in the search letter
([see 17.83](#ref17-83))\]

### 17.87 \[Deleted\]

### 17.88 {#ref17-88}

If the specification contains obscurities and it has been necessary for
these to be interpreted in a particular way in order for the search to
be performed, this should be made clear on the internal search report
(as well as in a supplementary report in the search letter, [see
17.83](#ref17-83).

### 17.88.1 {#section-31}

If the application contains more than one independent claim, a comment
on plurality should always be included on the internal search report,
even if it is considered that the claims relate to a single inventive
concept. If the search examiner considers that the claims lack unity of
invention then this should be recorded, together with an explanation if
necessary, and it should be made clear to which invention the search
relates. If it has been decided to treat a group of inventions as being
so linked as to form a single inventive concept this should be noted,
together with the reason if this is not self-evident.

### 17.89 {#section-32}

The substantive examiner is entitled to assume, in the absence of any
indication to the contrary, that all documents found which fall within
the terms of the search statement and within the databases searched have
been recorded. If many such documents were found and only a selection
have been cited, this should be made clear by answering Yes or No to the
internal reminder prompt ''Are the citations examples only?'' present in
each newly-generated ISR (see 17.83(e) and 17.86). In such cases,
documents which were not selected for citing in the external search
report should be listed in the ISR and accompanied by an explanation of
why they are relevant.

\[No document should be cited which cannot properly be justified.
However, since it is for the substantive examiner to decide whether
claims are novel and inventive, doubt at the search stage should be
resolved in favour of citing.\]

### 17.90 {#ref17-90}

If a specification published after the declared priority date of the
application in suit has been noted, the search examiner should, unless
they have grounds for believing that the exercise would be fruitless,
determine whether or not there is a foreign or European equivalent with
an earlier publication date.

### 17.91 {#ref17-91}

Any other document found in the course of the search which is likely to
prove useful to the substantive examiner, for example a document
relating to a subordinate claim for which no special search has been
made ([see 17.63](#ref17.63)) or relevant for reasons referred to in
[17.67](#ref17-67), should also be noted, together with the reason, for
consideration by the substantive examiner. No document lying outside the
scope of the search statement should be listed without explanation,
since this would leave uncertain the true extent of the search
performed. Where any prior art specifically referred to by the applicant
has been inspected, the result should be noted. Finally, any relevant
instance of prior use which is known to the search examiner should also
be noted ([see 17.54](#ref17-54)).

### 17.92 \[Deleted\]

### 17.93 {#ref17-93}

While the search examiner should not spend time delving into matters in
the specification which are not necessary for the performance of the
search and ancillary duties ([see 17.102](#ref17-102)) any such matters
which are noticed and are likely to be of relevance to the substantive
examiner should be briefly noted.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Search would not serve a useful purpose]{#default-id-330cc385-heading-23 .govuk-accordion__section-button} {#search-would-not-serve-a-useful-purpose .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Search would not serve a useful purpose\",\"index_section\":23,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-23 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-23" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Search would not serve a useful purpose\",\"index_section\":23,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.94 {#ref17-94}

Section 17(5)(b) directs the examiner to determine whether or not a
search on an application would "serve any useful purpose", and if it
would not, to report accordingly. This situation may arise if the
specification is so unclear that a meaningful search is simply
impossible. Alternatively, although a search might be possible, it may
be considered that there is no prospect of obtaining a valid patent and
so the search would not serve a useful purpose. For example, this might
be the case if the specification was considered to be classically
insufficient under s.14(3), and/or because it is not patentable by
virtue of s.1(1)(c) or (d). This will also normally be the case if the
applicant requests; i) a search of subject matter not claimed and found
solely within the description, or, ii) a further search in relation to
deleted claims -- as the claims have been deleted from the application
there is no patent currently being sought in respect of them ([see
17.114.1](#ref17-114-1)).

### 17.94.1 {#section-33}

However, the effect of section 17(5) as a whole is that if a search
would serve a "useful purpose" in relation to any part of the
application, then one should be carried out, at least in relation to
that part. Since the aim must always be to keep to a minimum any
searching done subsequent to the initial search stage, cases in which a
report is made under s.17(5)(b) will be relatively rare (unless the
claimed invention is clearly not patentable by virtue of s.1(1)(c) or
(d) -- [see 17.98](#ref17-98)). In many instances where a search could
be performed more efficiently if the scope of the invention were made
clearer, it nevertheless remains practicable for a search to be
performed without amendment of the claims. In such a case the search
examiner should do their best to glean from the claims and description
what appears to be the invention, at least to a sufficient extent to
enable a partial search to be performed, as provided for by s.17(5)(a).
In any such instance the search letter should include a supplementary
report explaining the scope of the search, and giving reasons -- see
[17.72.2](#ref17-72-2) and [17.83](#ref17-83).

### 17.94.2 {#ref17-94-2}

s.4A(1) is also relevant.

If there is plurality and the first claimed invention appears to be
wholly excluded, but one or more subsequent inventions are not excluded
under s.1(2), then the search should be directed to the first
non-excluded invention -- [see 17.107](#ref17-107). If there are
multiple independent claims that form a single inventive concept and
claim 1 relates to subject matter excluded under s.1(2), the examiner is
able to search any matter which could form the basis of a non-excluded
claim.

Where it is considered that the or each claimed invention is excluded
from patentability, but there is a possibility that the claims may be
amended to define a non-excluded invention supported by the disclosure
and it is readily apparent what this invention would be, a search should
be directed to this invention and the applicant should be informed of
the search examiner's views (in a supplementary report in the search
letter, see [see 17.83](#ref17-83); see [also 17.94.8](#ref17-94-8). The
examiner should only seek to identify such a "searchable" invention as
far as it is efficient and practical to do so. Where a "searchable"
invention cannot be readily identified and no saving amendment is
obvious the examiner should consider the strategies discussed below [see
17.94.8](#ref17-94-8) and [17.98](#ref17-98) in particular). For
example, if the claims all relate to a method of performing a surgical
operation but the application as a whole clearly discloses a potentially
patentable surgical instrument for use in such a method, then the
surgical instrument may be searched. If on the other hand the method is
performed using entirely conventional instruments, then there would
appear to be no saving amendment possible and so a search would serve no
useful purpose.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Strategies at search stage where it appears that a search would serve no useful purpose]{#default-id-330cc385-heading-24 .govuk-accordion__section-button} {#strategies-at-search-stage-where-it-appears-that-a-search-would-serve-no-useful-purpose .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Strategies at search stage where it appears that a search would serve no useful purpose\",\"index_section\":24,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-24 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-24" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Strategies at search stage where it appears that a search would serve no useful purpose\",\"index_section\":24,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.94.3 {#section-34}

Section 17(5) states that the examiner should determine whether or not a
search on an application would serve any useful purpose, and if it does
not, to report accordingly. However, a formal report under s.17(5)(b) is
not the only option available at search stage for applications where
there appear to be significant patentability issues, and it may not be
the most appropriate option, at least initially. If the claimed
invention appears to be unpatentable under s.1(1)(c) or (d), but it can
be searched and the examiner considers that the objection might be
successfully disputed or avoided by amendment, the applicant should
simply be informed when the search report is issued that the potential
objection will need to be fully considered if and when substantive
examination is performed.

### 17.94.4 {#ref17-94-4}

However, if the search examiner considers that the entire application
clearly has no patentable content, then they should consider the most
effective way to deal with the application. This will depend, in most
applications, on the nature of the potential objection(s) and whether
the applicant is professionally-represented or not. The search examiner
can consider several options:

a\) action before search (ABS -- sometimes also referred to as
"amendment before search");

b\) issue of a report under s.17(5)(b);

c\) issue of a substantive examination report under s.18(3) before
search (the "Rohde & Schwartz" procedure -- but this should only be done
in very limited circumstances).

These options are not mutually exclusive; it is possible to use the ABS
procedure prior to issuing either a s.17(5)(b) report or s.18(3) report.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [a) Action Before Search (ABS)]{#default-id-330cc385-heading-25 .govuk-accordion__section-button} {#a-action-before-search-abs .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"a) Action Before Search (ABS)\",\"index_section\":25,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-25 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-25" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"a) Action Before Search (ABS)\",\"index_section\":25,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.94.5 {#ref17-94-5}

If, in the examiner's opinion, the application has no patentable
content, and it is considered likely that the applicant would wish to
simply withdraw the application and receive a refund of the search fee
once that opinion is communicated to them, then the best option is
likely to be issue of a letter suggesting action before search (ABS),
or, if appropriate, action before combined search and examination
(ABCSE). However, the search examiner should not respond to a request
for a further search under s.17(6) by issuing an ABS letter ([see
17.111](#ref17-111)). An ABS or ABCSE letter should state the examiner's
view that there is no likelihood of a patent being granted, and clearly
and simply set out the reasons why the examiner considers that the
invention is not patentable, or is classically insufficient [see
14.67-14.75](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-67),
or lacks novelty and/or inventive step, together with any documents
illustrating this. The letter should also:

a\) Set a period (usually one month, but can be longer if appropriate)
for response;

b\) advise the applicant that they can either:

\(i\) withdraw the application, in which case the search fee will be
refunded. The letter should inform the applicant that in order to
withdraw the application the applicant should write to the Office
stating clearly their wish to withdraw the application; or

\(ii\) request that the application proceeds.

and;

c\) clearly state what will happen if no response is received from the
applicant within the specified period. This should make it clear to the
applicant that a search report will be issued after the end of the
specified period if no response is received from the applicant, and the
application is not withdrawn. If it is considered likely that this
report will be a report under s.17(5)(b) stating that a search would
serve no useful purpose, then the letter should clearly warn the
applicant of that likelihood. The letter should also make it clear that
once a search report is issued there will be no refund of the search
fee.

Where the application is considered to lack sufficiency under s.14(3) or
to be unpatentable under s.1(1)(c) or (d), the ABS letter should
normally be followed by a report under s.17(5)(b) if no response or
withdrawal request is received from the applicant.

\[If an ABS or ABCSE letter is to be issued, the examiner should amend
the ESO checklist to ensure that the ESOs: a) set the OPTICS processing
status to "2 - Out for ABS", and, b) change the PAFS action to ABS (or
ABCSE as appropriate) but do not book the case out on PAFS. An
appropriate diary entry should also be created in the PD electronic
diary system and a diary action added to the dossier in PDAX (taking
into account any as-of-right extension to the reply period that might be
requested). The diary entry should state the examining group after the
application number in the format GB0000000.0 - EX00\]

\[If the application is from a private applicant, then it will normally
be considered by the Private Applicant Unit (PAU) -- [see
17.03](#ref17-03). If the PAU examiner considers that the application is
clearly insufficient or unpatentable, or clearly lacks any novelty, then
an ABS or ABCSE letter should be sent to the applicant as outlined
above. Only one ABS or ABCSE letter should ever be sent to an applicant,
and so if the applicant has either not responded, or responded with an
indication that they wish to proceed with the application, then the
applicant should not be sent a further ABS/ABCSE letter. \]

### 17.94.6 {#section-35}

In response to the letter, the applicant may provide amendments and/or
arguments to address the issues raised in the ABS/ABCSE letter. Where
such arguments and/or amendments are received the search examiner should
carefully consider them and decide whether to issue a search report
under s.17(5)(a) (or combined search and examination report), or issue a
report under s.17(5)(b) that a search would serve no useful purpose. The
examiner should not issue another letter requesting action before
search.

### 17.94.7 {#section-36}

This procedure may also be useful for applications which are very
clearly lacking in novelty and/or inventive step in their entire
content. In such cases, it may be helpful to perform a very rudimentary
search to identify a small number of documents which illustrate this,
and a copy of each of the documents should be sent with the letter.
Again the letter should set out the options for the applicant, including
withdrawal with a refund of the search fee, set a period for response,
and state what action will be taken if no response is received. If the
search examiner considers that the invention lacks novelty and/or
inventive step but is otherwise patentable and could be searched, then
if the application is not withdrawn the ABS/ABCSE letter should be
followed up with a search report under s.17(5)(a) (or combined search
and examination report) rather than a s.17(5)(b) report (other than in
the very rare situation described in [17.101](#ref17-101)).

### 17.94.8 {#ref17-94-8}

A similar procedure may also be used if it is considered that, while the
claims as drafted are not patentable, there is a possibility that a
claim defining a patentable invention could be supported by the
disclosure, but it is not clear what this would be. In this case, a
letter requesting amendment before the search is carried out may be sent
to the applicant. Again, the letter should set a date for response and
should clearly set out what the search examiner's action will be if no
satisfactory response is received.

### 17.94.9 {#ref17-94-9}

The examiner may refer to the Code of Practice in a letter requesting
action or amendment before search. However, it should be noted that
non-compliance with the Code of Practice is not in itself justification
for a report under s.17(5)(b). \[SC16 should be used when referring to
the Code of Practice at the search stage, inserting the number of the
relevant point of the Code\].

\[If the application is withdrawn and a refund of the search fee is
requested, the request should be referred to the examiner by
formalities. The request for a refund should be accepted unless the
period specified for response to the ABS/ABCSE letter has expired and
the examiner has already initiated work on a search report (under either
s.17(5)(a) or (b)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [b) Report under s.17(5)(b) that a search would serve no useful purpose]{#default-id-330cc385-heading-26 .govuk-accordion__section-button} {#b-report-under-s175b-that-a-search-would-serve-no-useful-purpose .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"b) Report under s.17(5)(b) that a search would serve no useful purpose\",\"index_section\":26,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-26 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-26" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"b) Report under s.17(5)(b) that a search would serve no useful purpose\",\"index_section\":26,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.94.10 {#ref17-94-10}

If the examiner has determined that a search would clearly serve no
useful purpose in relation to making an application for a UK patent, and
either an ABS letter is not considered appropriate, or a letter has been
issued and no satisfactory response has been received, then (unless the
Rohde & Schwartz procedure is adopted) they should report this in a
letter to the applicant under s.17(5)(b). The letter should be
accompanied by an examination opinion (see 17.83.3) (for a search) or an
abbreviated exam report (for a CSE -- see square brackets of 17.94.11
and 18.47.2), outlining the reasons why the search would serve no useful
purpose. If, having considered the facts of the case, the examiner feels
that issue of an examination opinion is inappropriate, the reasons for
not performing a search should instead be detailed in the search letter.
For private applicants either an examination opinion should issue or
alternatively the letter under 17(5)(b) should provide a brief
explanation of the reasons why the search would serve no useful purpose
(reports under 17(5)(b) to private applicants will be relatively rare as
ABS or ABCSE action will usually be most appropriate in the first
instance, [see 17.94.5-17.94.9](#ref17-94-5)). In all cases the
applicant should be informed that if amended claims are submitted, then
the examiner will reconsider whether a search can be made.

### 17.94.11 {#section-37}

If, following amendment, the search examiner still considers that a
search would serve no useful purpose, then they should avoid entering
into lengthy correspondence at this stage and should suggest that the
matter is dealt with at substantive examination or the applicant should
be offered a hearing. If the application is unsearchable because it
lacks patentability, the hearing may deal with that substantive matter
too - [see 17.98-17.98.1](#ref17-98).

\[When no search has been made because one would have served no useful
purpose, this should be recorded on the internal search report through
the Field of Search entry on PROSE, which will transmit this information
into COPS.\]

\[The report under 17(5)(b) should take the form of letter SL2 and be
accompanied by an OPINION document (In private applicant cases, SL2PA
should be used and the OPINION document is optional. If an OPINION
document is issued, the SL2PA letter and checklist should be modified to
refer to the OPINION document. If no OPINION document is issued, an
explanation why the search was not completed should be included in the
SL2PA). In combined search and examination cases SE2 (SE2PA) should be
issued along with an abbreviated examination report including a brief
explanation of the reasons why no search has been performed. These
letters offer the applicant the opportunity to submit amended claims.
For further details on the procedure where combined search and
examination is requested but the search would serve no useful purpose,
[see
18.47.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-47-2)
\]

\[No external search report should issue, not least so that Formalities
can rely on the absence of an external search report on file as a signal
that any new or amended claims should be referred to the search examiner
([see
16.18](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-18)).
Insofar as no search report will have issued no search report should be
included with the published A - document.\]

\[For a search or CSE on a case which has not been published, a response
to a report under 17(5)(b) would preferably be considered within one
month. This is because if the examiner decides to carry out a search as
a result of the response it is preferable that the application is
published with the search report. For a CSE on a case which has been
published, a response should be considered within the usual amendment
target; if a search is required, it should also be completed within this
timescale and preferably at the same time as the consideration of the
response.\]

\[For a search, if a response to a report under 17(5)(b) does not
convince the examiner that the claims are now searchable, the examiner
can respond by way of the SL6 letter. The SL6 can be edited to include
brief reasons, although lengthy correspondence should be avoided. If a
hearing takes place at this stage, a s.18(3) report can be issued as a
pre-hearing report (see 18.79-18.80.1). If such a response is received
on a CSE then it should be processed in the same way as a normal
amendment, i.e. with further examination reports issued, culminating in
the offer of a hearing if necessary.\]

### 17.95 {#section-38}

If, following a report that a search would not serve a useful purpose,
searchable amended claims, or observations which convince the search
examiner that a meaningful search is possible in respect of the original
claims, are received before the application has entered the actual
publication cycle under s.16, then the search should be carried out. If
no response to such a report has been received by the time the
application is due to be sent for publication by the search examiner
([see
16.31](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-31))
then, provided the application has not been refused following a
preliminary objection ([see 17.96.1-17.96.4](#ref17-96-1)) it should be
sent for publication in the usual way. The front page of the published
document will indicate, in the place provided for documents cited, that
no search was possible. If a response enabling a meaningful search to be
made is received after the application has been sent for publication,
the search should be carried out as soon as practicable in order to give
the applicant as much time as possible to decide on their subsequent
course of action. If the application has been sent for publication but
it has not entered the actual publication cycle then a search may be
carried out as normal.

\[The amendments should be added to the dossier by Index and Scanning
and annotated appropriately, and a PDAX message sent to the examiner.\]

\[If a search is subsequently performed the search examiner should
create a fresh internal search report and an external search report. A
report of a search in respect of amended claims should be accompanied by
letter SL3 (to which SC13 should be added if there is plurality of
invention) together with a copy of any non-patent literature citations
or English abstracts of any cited foreign language patent document or
NPL citations ([see 17.104.1](#ref17-104-1)). The search should normally
be carried out as soon as possible; if the search is carried out before
the case is picked for publication, the Examination Support Officer will
add a "P" to the external search report in the table of contents.
However if COPS has selected the application for publication from those
in the A-publication queue when the amended claims are placed on the
file, the search on the amended claims should be deferred until after
publication. In these circumstances the amended claims should normally
be put on the file at Publishing Section by the publication liaison
officer, and will be published if the preparations for publication are
not complete (as determined by
[16.02](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-02)).\]

### 17.96 {#ref17-96}

No refund is made following issue of a report under s.17(5)(b)
indicating that no search has been performed on the grounds that it
would not serve a useful purpose ([see also 17.06](#ref17-06)). As
discussed above, if it is considered likely that the applicant will wish
to withdraw and seek a refund once they become aware of the search
examiner's opinion, then the search examiner should consider sending an
ABS letter instead. However, if it appears that the applicant would be
fully aware of the possible objections, then there is little to be
gained by adding an extra round of correspondence.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [c) Report under s.18(3) before search ("Rohde & Schwartz")]{#default-id-330cc385-heading-27 .govuk-accordion__section-button} {#c-report-under-s183-before-search-rohde--schwartz .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"c) Report under s.18(3) before search (“Rohde \\u0026 Schwartz”)\",\"index_section\":27,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-27 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-27" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"c) Report under s.18(3) before search (“Rohde \\u0026 Schwartz”)\",\"index_section\":27,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.96.1 {#ref17-96-1}

Issuing an examination report under s.18(3) before a search is conducted
is very rarely considered to be appropriate, because this option will
normally result in a hearing officer having to issue a decision, but it
may be done in exceptional circumstances. This approach may only be
considered where the search examiner is in no doubt before a search is
commenced that the, or each, invention claimed is not patentable by
virtue of s.1(1)(c) or (d), s.1(3) or s.14(3), and is equally convinced
that the disclosure is such that it could not possibly support any
allowable claim. A report under s.18(3) may be issued even though no
request for substantive examination has been filed and regardless of
whether the other requirements of s.18(1) are met - see [Rohde and
Schwarz's Application \[1980\] RPC
155](http://rpc.oxfordjournals.org/content/97/5/155.abstract?sid=f03f8516-abe2-4453-8958-f94883c4a382){rel="external"}.

### 17.96.2 {#ref17-96-2}

Where such action is taken, no report should be made under s.17(5).
Instead, a reasoned report under s.18(3) should be made giving the
search examiner's opinion that the application should be refused. This
report can be brief in stating the reason why the application should be
refused. For example, it is not necessary to write a detailed argument
on why an invention that is contrary to physical laws will not work when
this is clear from reading the application. The report should add that
if the application is withdrawn a refund of the search fee will be made
(see 17.06). If the matter is not resolved (either because the applicant
submits arguments which do not dispose of the matter or because there is
no response) then a hearing should be offered and a hearing officer's
decision issued in the usual way. In order to efficiently resolve the
objection resulting in issue of the s.18(3) report, examiners should
avoid entering into protracted correspondence with the applicant.

\[ The examiner should consult their group head before issuing a report
under s.18(3) before search. Where an examiner issues a report under
s.18(3) before search, a period of four months should be set for reply
in the first instance. A "Diary" entry should be created in the PD
electronic diary so that the application will return to the examiner in
6 months (to take into account a possible two month extension of the
reply period). A diary entry should also be created for any subsequent
exam reports. Each diary entry should state the examining group after
the application number in the format GB0000000.0 -- EX00, and a
corresponding diary 'action' should also be added to the PDAX dossier.
\]

\[ At the appropriate time, in order to avoid protracted correspondence,
a letter should be issued by the examiner saying that they are minded to
refuse the application and offering a hearing. The applicant should be
given one month to indicate whether they wish to be heard in the matter,
a diary entry should again be created so that the application returns to
the examiner even if no response is received. If the applicant does not
wish to heard (or does not respond), then a hearing officer will decide
the matter on the papers.\]

\[ It should be noted that if, after consulting a group head, an
ex-officio objection is made under s.18(3) in the circumstances
mentioned above, the report containing that objection is considered to
be the first report under s.18(3) for the purposes of determining the
compliance period. If the application does proceed to normal substantive
examination, every effort should be made to ensure that substantive
examination is not unduly delayed since the applicant is deprived of the
potential benefit of r.30(2)(b). \]

### 17.96.3 {#section-39}

An ABS letter may be sent to an applicant which effectively gives the
applicant the choice of withdrawal with a refund of the search fee, a
report under s.17(5)(b) or a s.18(3) report before search. In this case,
the search examiner may inform the applicant before a report under
s.17(5) is issued that whilst the application would seem to give rise to
an objection under s.1(1)(c) or (d), or s.14(3), the matter will not be
pursued prior to substantive examination unless within two months the
applicant indicates a wish to have the matter resolved immediately. If
the applicant so indicates, the objection should be pursued under s.18
as indicated in 17.96.1. If the applicant does not take the opportunity
either to have the matter settled immediately or to withdraw the
application, a report under s.17(5)(b) should then be issued indicating
that because no patentable subject matter is disclosed it is not
apparent what could be usefully searched, and the application sent to
await A-publication in the usual way. There is no entitlement to a
refund of the search fee after issue of the search report.

### 17.96.4 {#ref17-96-4}

Another situation where an application may be refused without performing
a search is on the grounds of estoppel by record where the application
is for the same invention as an earlier application which has been
refused by the courts or the comptroller, as occurred in Ward's
Applications [BL
O/143/02](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F143%2F02&submit=Go+%BB){rel="external"}
and
[BLO/055/06](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F055%2F06&submit=Go+%BB){rel="external"}.

\[ A letter should be issued by the examiner offering the applicant the
opportunity to withdraw and have their search fee (and, if paid, the
substantive examination fee) refunded ([see 17.06](#ref17-06)). The
letter should state that they are minded to refuse the application if no
withdrawal is made, and a hearing should be offered. The applicant
should be given one month to indicate whether they wish to be heard in
the matter. If they do not wish to heard (or does not respond), then a
hearing officer will decide the matter on the papers. \]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [No useful search possible: reasons]{#default-id-330cc385-heading-28 .govuk-accordion__section-button} {#no-useful-search-possible-reasons .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"No useful search possible: reasons\",\"index_section\":28,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-28 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-28" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"No useful search possible: reasons\",\"index_section\":28,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### a) No useful search possible because the invention is obscure

### 17.97 {#section-40}

A report under s.17(5)(b) (preceded, if appropriate, by an ABS/ABCSE
letter) should generally be made if obscurities in the specification are
such as to render a meaningful search impossible, or to make the scope
of the invention so uncertain that the search is likely to be
incomplete. The search examiner should however avoid entering into
dialogue with the applicant or agent regarding possible amendments or
about other matters which should be dealt with at the substantive
examination stage. No report should be made directing attention to
alleged defects or obscurities if, despite these, the search examiner
has been able to perform what is reasonably likely to be a complete
search.

### b) No useful search possible because the invention is not patentable

### 17.98 {#ref17-98}

A report under s.17(5)(b) (preceded, if appropriate, by an ABS/ABCSE
letter), or an examination report under s.18(3) before search, should
generally be made if the, or each, invention claimed is clearly not
patentable by virtue of s.1(1)(c) or (d) (for example if the invention
relates to a pure business method), or by virtue of s.14(3) (for
example, in the circumstances set out [in
4.05](/guidance/manual-of-patent-practice-mopp/section-4-industrial-application/#ref4-05)
and
[14.79](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-79).
In [CFPH LLC's Application \[2006\] RPC
5](http://rpc.oxfordjournals.org/content/123/8/259.abstract){rel="external"}.
Peter Prescott QC (sitting as a Deputy Judge in the Patents Court)
confirmed at paragraph 96 that it will not always be necessary for an
examiner to carry out a search before he or she can determine whether
the invention is new and not obvious (and susceptible of industrial
application) under the description "an invention" (in the sense of
Article 52): "In order to identify what is the advance in the art that
is said to be new and nonobvious the Patent Office may rely on prior art
searches. But in my judgment it is not invariably bound to do so. It
will often be possible to take judicial notice of what was already
known. Patent Office examiners are appointed because they have a
professional scientific or technical training. They are entitled to make
use of their specialist knowledge. Of course the letter of objection
will state the examiner's understanding of the technical facts in that
regard, and thus the applicant will have the opportunity to refute it in
case there has been a mistake."

Furthermore, in [Shopalotto.com Ltd's Application \[2006\] RPC
7](http://rpc.oxfordjournals.org/content/123/9/293.abstract){rel="external"}
(see 1.22), it was held that the question of whether the claimed
invention includes a contribution outside the list of excluded subject
matter may be answered notwithstanding the fact that there has been no
novelty search in relation to the invention. Pumfrey J pointed out the
distinction between the scope of contribution and the area in which the
contribution is made and added that there comes a point where the
relevant matters are so notorious that a formal search is neither
necessary nor desirable and the Comptroller is entitled to use common
sense and experience.

### 17.98.1 {#section-41}

It should be noted that "useful purpose" means serving a useful purpose
in relation to making an application for a UK patent. A search does not
serve a "useful purpose" within the meaning of s.17(5)(b) merely because
it may help the applicant with a decision on whether to file in a
country in which the equivalent application is not excluded from
patentability.

### 17.99-17.99.2 \[Moved to [17.96.1-17.96.3](#ref17-96-1)\] {#moved-to-17961-17963ref17-96-1}

### 17.99.3 \[Deleted\]

### 17.99.4 \[Moved to [17.96.4](#ref17-96-4)\] {#moved-to-17964ref17-96-4}

### 17.100 \[Moved to [17.94.2](#ref17-94-2)\] {#moved-to-17942ref17-94-2}

### c) No useful search possible when the invention is not novel or is obvious

### 17.101 {#ref17-101}

Whenever the search examiner takes the view that an invention claimed is
not novel or is obvious, documents demonstrating this should be selected
and cited if this is at all possible. If, exceptionally, this view can
be supported from common general knowledge but the search examiner
considers that it would be impossible, or that a disproportionate amount
of time would be necessary, to find such documents, a report to this
effect should be made under s.17(5)(b), preceded, if appropriate, by an
ABS letter. In general, such a report will not be appropriate in respect
of any claim when a search has been made on another claim and the claims
are considered to relate to a single inventive concept. Thus, for
example, independent claims and claims dependent upon them should be
listed as having been searched when they fall within the scope of the
search made ([see 17.72.1](#ref17-72-1)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Final Procedure]{#default-id-330cc385-heading-29 .govuk-accordion__section-button} {#final-procedure .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Final Procedure\",\"index_section\":29,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-29 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-29" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Final Procedure\",\"index_section\":29,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.102 {#ref17-102}

The search examiner should also check, and if necessary reframe, the
abstract (see
[14.169-14.191](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-169)
and classify the disclosure on both the IPC and CPC (even if an abstract
has not been filed). The search examiner must therefore study the
specification in whatever detail is necessary to carry out these tasks,
in order to minimise errors on the published document, even though this
may be time-consuming, particularly where relatively deep indexing
systems are in use. The classification and/or index should normally be
assigned in the light of the search, and classification of what is
obviously old should be avoided. All significant technical disclosure
should be classified and/or indexed, including any second or subsequent
inventions, even though it may not have been the subject of a search and
even if the same disclosure in a co-pending application is being given
the same classification/index. The search examiner must see that any
subclass examiner who may need to assign a supplementary classification
has been consulted, and should make a record of such consultation by the
addition of a minute to the PDAX dossier.

\[The abstract title should be entered into the 'Abstract Title' field
in the Details area on PROSE, and the figure to be used with the
abstract should be entered into the 'Fig. Ref.' field as well as on the
Abstract itself. ClassTool should be used to enter IPC and CPC
classification data.\]

### 17.103 {#section-42}

While the search examiner should not check those matters which are the
responsibility of the formalities examiner, if they come across an
apparent omission or error on the part of the formalities examiner they
should draw it to their attention.

### 17.104 {#ref17-104}

When the search examiner has completed their report on the search and
their other duties ([see 17.102](#ref17-102)) the application should be
sent for issue of the search report and search letter ([see
17.83](#ref17-83)).

\[After preparation of the report, a message should be sent to the
Examination Support Officer from the examiner, either directly or via a
message to their Revising Officer, depending on whether the examiner is
revised or not. The Examination Support Officer will then ensure that
the relevant documents are imported into the PDAX dossier. The reports
and covering letters will then be issued automatically and
electronically to the applicant via email.\]

\[Deleted\]

\[ (For the issue of the search report on a divisional application, [see
15.41-42](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-41)
.\]

### 17.104.1 {#ref17-104-1}

r.27(2) is also relevant.

Issuing copies of citations is at the discretion of the Comptroller (see
rule 27(2)). For searches and examinations where the first search was
requested on or after 8 March 2020, the office will no longer issue
paper copies of cited patent documents and customers are directed to
obtain such documents from
[Espacenet](https://worldwide.espacenet.com/){rel="external"}. Search
reports issued electronically by the Office will be accompanied by a PDF
copy of any non-patent literature (NPL) cited and a digital copy of any
English language abstracts of foreign language patent documents or NPL
citations. In citing an NPL disclosure which cannot be obtained in time,
the search examiner may initially rely on an abstract only. However a
copy of the source document should be obtained and sent to the applicant
as soon as possible. Search reports which cite foreign language
documents do not necessarily need to be accompanied by a digital copy of
an English language abstract. The decision whether to refer to/send
abstracts to applicants is at the discretion of individual examiners who
should first consider: a) whether a publicly available machine
translation can be obtained (e.g. through the website of another patent
office) that the applicant can be referred to by URL, and/or, b) whether
it would otherwise be useful for the applicant to receive an abstract
(in which case one may be sent). If however an examiner chooses to refer
to the content of an abstract in their search report, then they must
arrange for that abstract to be sent to the applicant regardless of
whether a free machine translation of the source document is also
available (see second square bracket below 17.104.1. Where a cited
document is not in English, machine translations of the cited document
should not normally be issued to the applicant because they cannot be
relied upon for accuracy and (depending on the source) there may be
contractual and copyright restrictions. If a free translation can be
obtained (e.g. through the website of another patent office), reference
may be made to this source e.g. by URL. If the examiner feels it is
nevertheless necessary to send a machine translation (and there are no
copyright or contractual restrictions to doing so) a clear explanation
of the status of the translation should be provided. In particular, it
should be explained that the translation is machine-generated and
therefore should not be relied upon as being definitive, but is being
provided to be helpful for the applicant. Conversely, the search or
substantive examiner may use a machine translation which they are unable
(e.g. for contractual or copyright restrictions) to share with the
applicant. In such cases, if the applicant disagrees with the examiner's
interpretation of the disclosure then it is ultimately the applicant's
responsibility to provide an explanation of their view. It should be
remembered that it is the foreign language disclosure itself which forms
the citation, not the translation. Translations are merely used to
assist in understanding the prior art.

\[The situation regarding abstracts is as follows:

\(a\) if the search examiner does not have a copy of the NPL source
document, (whether because it is not needed or it is unavailable), then
the abstract alone issues to the applicant;

\(b\) if the search examiner has a copy of the NPL source document as
well as the abstract and the source document is not in English, then
both the source document and the abstract are copied to the applicant.

\(c\) abstracts of foreign language patent documents only need to be
issued to applicants if the examiner chooses to refer to the abstract in
their search report or otherwise deems it would be useful for the
applicant to receive the abstract (see 17.104.1 above).\]

\[ ESOs are no longer responsible for issuing copies of document
abstracts, this process occurs automatically and electronically and is
initiated by the examiner. Specifically, examiners should use the CITES
SUPP PROSE document to add the text of the abstract to the search or
examination bundle with which it is to be issued.\]

\[If requested, multiple paper copies of patent document citations will
be provided for applications where the first search was requested before
27 September 2014. Single copies of patent document citations will be
provided for applications where the first search was requested before 8
March 2020, otherwise no copies of patent document citations will be
provided.\]

\[If a machine translation is to be issued to the applicant the
copyright notice from the provider should be included. K-PION
translations should not be provided; however a reference to the online
KIPRIS service -- which is free for use by the public - may be added to
the search report (e.g. by URL).\]

### 17.104.2 {#section-43}

The application will then proceed to publication in due course [see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29)
[16.32](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-32)
unless in the meantime it is withdrawn or refused.

\[The search examiner should record the appropriate COPS processing
status in PROSE ([see
16.30](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-30)).\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Documents found or considered in full after issue of the search]{#default-id-330cc385-heading-30 .govuk-accordion__section-button} {#documents-found-or-considered-in-full-after-issue-of-the-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Documents found or considered in full after issue of the search\",\"index_section\":30,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-30 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-30" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Documents found or considered in full after issue of the search\",\"index_section\":30,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.105 {#ref17-105}

The applicant should be sent a copy (or copies) of any non-patent
literature (NPL) which the search/substantive examiner has become aware
of after issue of the search report and which would have been included
in the report had it been found during the search, or considered in full
after citing the abstract in the search report ([see also
17.83.1](#ref17-83-1). If the document is a patent document, the
applicant should be informed of the citation. If the search examiner
becomes aware of or considers the document before substantive
examination, then the applicant should be immediately notified of the
document so that they may decide whether to proceed with substantive
examination, although this will not normally be done if it is a newly
published document added to the files since the date of the search. A
document found after issue of the search report and after the filing of
amended claims and which anticipates the original rather than amended
claims should nevertheless be communicated to the applicant. If the date
of completion of preparations for publication has not passed, an amended
external search report should be issued and the new document will be
included on the front page of the published application. If the date of
completion of preparations for publication has passed, an amended search
report is not issued and the new document will only be mentioned on the
front page of the specification published after grant ([see also
18.85](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-85)).

\[When a search examiner becomes aware of a citation after the external
search report has issued, the publication status of the application
should first be checked on COPS using the "Display Processing Status"
(DIS PRO) function. If the application is not yet ready for A
publication, the search examiner should prepare amended internal and
external search reports, and enter the new citation using PROSE.
Depending on the circumstances in which the citations came to the notice
of the search examiner, the Field of Search may, of course, also require
updating on the internal and external search reports. The amended
external search report should be issued under cover of SL5B, in which
the new citation is identified. The amended search report should also be
identified as such. The date of search on the amended report should
correspond with that on the original report, but a suitable
qualification may be added if necessary eg the date of any additional
search. The Examination Support Officer should remove "P" from the
original search report in the PDAX dossier in question but leave as open
to public inspection, and mark the new search report as "P" allowing it
to be published.

\[When a search examiner's attention is drawn to an error in an external
search report after the report has issued but before preparations for
A-publication are complete (eg a citation has been wrongly identified),
a similar procedure to that outlined above should be followed, except
that the aim is to produce a corrected report for issue to the
applicant, under cover of SL5C and for publication with the A-document,
and to correct any corresponding error in the internal search report
form.

\[If it is too late for a corrected external search report to be
published with the A-document then SL5D should be issued, and the
formalities examiner should be advised to issue an erratum in respect of
both the corrected external search report and any corresponding errors
on the A front page ([see also
16.33](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-33)).
After the application has been A-published, the citations should be
corrected in COPS (using the "Record Citations" (REC CIT) function).

\[If an applicant or agent is warned by telephone that an amended or
corrected report is being issued it is not necessary to send a telephone
report as well as the appropriate letter. See
\[[18.11](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-11)
for the procedure adopted if the substantive examiner becomes aware of a
citable document when substantive examination is due.\]

### 17.105.1 {#ref17-105-1}

Where the search examiner is made aware by the applicant of a document
that has been cited in a search report for a patent application on the
same invention made in another patent office, consideration of the
documents cited should be deferred until substantive examination. An
amended search report should not be issued ([see
18.64](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-64)
for when substantive examination has already been performed).

\[The foreign search report or letter received from the applicant
mentioning the cited documents should be placed on the PDAX dossier. A
minute should then be added to the PDAX dossier and the appropriate PDAX
message sent to the substantive examiner.\]

### 17.105.2 {#ref17-105-2}

Documents cited by the IPO after issue of the search report are copied
to the applicant where appropriate ([see 17.104.1](#ref17-104-1)). In
the case of international applications under s.89, copies of additional
documents cited by the UK search/ substantive examiner are provided but
not copies of the documents cited in the International Search Report,
Supplementary International Search Reports (if any), or the
International Preliminary Report on Patentability.

\[It is the examiner's responsibility to ensure that copies of newly
cited documents are issued to applicants: where NPL documents are cited
for the first time at the substantive examination stage; where patent
documents are cited for the first time at the substantive examination
stage and the applicant previously requested that they be sent paper
copies of cited patent documents by ticking the box on Form 9A (and the
first search was requested before 8 March 2020, see 17.104.1); where a
source document for a cited abstract has been obtained; where the wrong
document has previously issued; where on an international application
under s.89, documents are cited by the UK examiner which were not
included in the international search report.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Plurality of invention]{#default-id-330cc385-heading-31 .govuk-accordion__section-button} {#plurality-of-invention .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Plurality of invention\",\"index_section\":31,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-31 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-31" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Plurality of invention\",\"index_section\":31,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 17(6)**
  If it appears to the examiner, either before or on conducting a search under this section, that an application relates to two or more inventions, but that they are not so linked as to form a single inventive concept, he shall initially only conduct a search in relation to the first invention specified in the claims of the application, but may proceed to conduct a search in relation to another invention so specified if the applicant pays the search fee in respect of the application so far as it relates to that other invention.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 17.106 {#ref17-106}

The question as to whether there is unity of invention ([see 14.157 to
14.168](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-157))
should always be considered whenever there are two or more independent
claims in addition to any omnibus claim (but see 14.164); the answer to
this question will usually be quickly apparent and should always be
recorded on the internal search report (see 17.88.1). However plurality
may also arise in situations with only one independent claim. When it is
clear that the claims relate to more than one invention or inventive
concept then the search must be directed to the first invention
specified in the claims, which will generally be the invention of claim
1 or of a group of claims whose scope embraces claim 1 ([see also
17.37](#ref17-37)). Section 17(6) allows no discretion in this matter.
The search must be directed to the first invention encountered when the
claims are read in numerical sequence ([see Hollister Inc's Application
\[1983\] RPC
10](http://rpc.oxfordjournals.org/content/100/1/10.abstract){rel="external"}),
even if the claims are filed out of sequence. If it only becomes
apparent after a partial or full search that unity of invention does not
exist, this lack of discretion prevents the acknowledgement, in the
"claims searched" or in the citation table, of any search other than
that relating to the first invention. Nevertheless, prior art common to
the first invention and any other invention(s) can be cited as
background art relevant to the first invention if it is considered
desirable in establishing the grounds for noting a plurality of
inventions ([see 17.80](#ref17-80)). The examiner may advise the
applicant of prior art relevant to further inventions in a supplementary
report in the search letter, but should make clear to the applicant that
the further invention has not been searched. If the first invention of a
divisional application is the same invention as in the parent, [see
15.38](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-38).

\[The extent and results of a search, as they relate to invention(s)
beyond the first, should be recorded on the internal search report for
the benefit of any search examiner called on to search the further
invention(s) ([see 17.86](#ref17-86)).\]

### 17.107 {#ref17-107}

As noted above, s.17(6) requires the examiner to search this first
invention. However if the first invention claimed is excluded under
s.1(2) but later claims define one or more not excluded inventions, the
examiner is able to search the first of these (see 17.94.2). This is
because s.1(2) is clear that matter which is excluded under that
provision is not considered an "invention" at all. If it is clear that
the first invention is excluded under s.1(2), this should be reported to
the applicant in the search letter, exam opinion or CSE examination
report as appropriate.

### 17.108 {#ref17-108}

r.27(3), COP is also relevant.

When the search examiner has reported that the claims relate to two or
more inventions not linked by a single inventive concept, the
comptroller must notify the applicant of this fact. Those claims or
groups of claims which are considered to constitute separate inventions
should be identified in an accompanying letter or the examination report
if the search and substantive examination are combined. In the latter
case the report should make it clear that the notification of more than
one invention is made under s.17 to keep open the opportunity for the
second and any further inventions to be searched under s.17 ([see
18.41](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-41)).
The letter or report should also indicate that the search has been
directed to the first of the inventions. If it is not possible to state
precisely how many inventions are specified in the claims, but it is
nonetheless clear that unity of invention is lacking, for example in a
case with a large number of overlapping in dependent claims, or where
there are obscurities in the claims, or where a lengthy analysis of the
claims, more appropriately done at substantive examination, would be
needed, a report to this effect may be made. Where it is not practicable
to identify all the claims encompassed by the first inventive concept,
claim 1 of the application may be searched by the search examiner. Where
excessive amounts of time would be incurred to identify the exact number
of inventive concepts in an application, a clause may be inserted into
the search letter, asking the applicant to identify the separate
inventive concepts when requesting a further search under s.17(6).

\[Plurality of invention should be indicated by the addition of SC13 to
SL1 before the paragraph about other search results or by adding RC6 and
RC6A to a combined search and examination report issued with SE1 or SE4
([see
15.41](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-41)).\]

### 17.108.1 {#ref17-108-1}

If there is doubt as to whether plurality is present, it is at the
examiner's discretion as to how to proceed, but the benefit of the doubt
should be given to the applicant
([14.159.1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-159-1)).
It is acceptable to defer plurality as the issue may be more of a
support or clarity problem. The decision must be clearly communicated to
the applicant. When deferring an assessment of plurality, it is
important to ensure that all the independent claims reported as searched
have been fully covered by the search. The substantive examiner cannot
ask for further Forms 9A for claims which the search examiner has
reported as searched (see [17.120](#ref17-120). In extremely unclear
cases, it may be appropriate to issue an Action Before Search/Action
Before CSE letter asking the applicant to clarify the claims prior to
searching. This is done by identifying as far as possible what the
invention(s) are and informing the applicant what will be searched if
there is no response to the ABS/ABCSE letter. If the claims are unclear
or overlapping in scope, but have been treated as unified, this should
be communicated to the applicant in the search letter, indicating the
scope of the search which has been conducted. If it is a CSE, a
clarity/conciseness objection should be raised in the examination
report. It may be appropriate to warn the applicant that plurality may
be raised later.

\[The ABS/ABCSE should be a MISC LETTER in PROSE, with a reply date set
(1 month is usually appropriate). When making the checklist, the
application status should be set to 2 -- Out for ABS.\]

### 17.108.2 {#ref17-108-2}

If the examiner decides that there is plurality and a formal objection
is warranted (as opposed to a clarity/conciseness objection -- see
17.108.1 and second square bracket below
[18.38.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-38-1)),
this must be clearly reported to the applicant. In order to ensure
consistency in the reporting of plurality objections to applicants, all
formal plurality objections must contain:

a\) an introduction to the objection setting out the legal basis for it
and identifying the different groups of inventions, including where
possible an indication of the claims belonging to each group;

b\) the grounds for the objection including identifying the common
subject matter between the different groups of inventions or, if
appropriate, a statement on the lack thereof;

c\) if common subject matter has been identified a comparison with the
prior art/common general knowledge which explains why the features
identified as common subject matter are known or obvious (N.B. if prior
art is relied upon it should be clearly identified). If the claims do
not share common subject matter, this should be communicated to the
applicant;

d\) a concluding statement explaining that a lack of unity has been
found.

Standard PROSE clauses SC13 and RC6 have been designed to provide a
template for a formal plurality objection fulfilling these criteria and
should be used in full whenever a formal plurality objection is raised.
It should be noted that there is no automatic concluding statement
provided in SC13 and RC6 and examiners should ensure that a concluding
statement appropriate to the application before them is provided in
their own words. As noted above, this may include explaining that there
is not share any common subject matter, as may be the case in, for
example, unsuccessful 'plug-and-socket' claims (see 14.161). Examiners
should not use SC13 or RC6 when objecting to clarity/conciseness issues
which create uncertainty as to whether there is plurality or not, they
should instead draft an objection in their own words.

### 17.109 {#ref17-109}

\[Deleted\]

### 17.110 {#ref17-110}

If a group of claims formally has unity of invention because one of them
embraces in scope all the others, and it transpires that the invention
of this claim is not novel or inventive, the search examiner should if
practicable complete the search on this claim. If such a course is
clearly not reasonable, for example if this claim is unduly broad and
speculative and is apparently merely a device for giving an impression
of unity of invention, then the search examiner should decide what
appears to be the most efficient course. The search examiner may decide
to read the main claim as notionally amended by combination with each of
these dependent claims and search the invention which is then
encountered first; in such a case the search examiner should indicate in
the letter accompanying the search report that there is plurality of
invention in line with the guidance set out above at
[17.108.2](#ref17-108-2) and add a brief explanation of the reason for
this ([see also
14.164](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-164)).
Alternatively they may perform a partial search in respect of the main
claim, or, if the substance of the main claim is so blatantly well-known
or is so nebulous that search for it would serve no useful purpose and
the claim is clearly an artificial attempt to give an impression of
unity to differing inventions, they may defer the search altogether
([see 17.94.5-17.94.9](#ref17-94-5)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Further search]{#default-id-330cc385-heading-32 .govuk-accordion__section-button} {#further-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Further search\",\"index_section\":32,\"index_section_count\":32}"}
:::

::: {#default-id-330cc385-content-32 .govuk-accordion__section-content aria-labelledby="default-id-330cc385-heading-32" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Further search\",\"index_section\":32,\"index_section_count\":32}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 17.111 {#ref17-111}

r27(4), r27(4), r27(4), r27(4), r.108(1) is also relevant.

If the search examiner has reported plurality of invention, a further
search may be requested in respect of a second or subsequent invention
which was present in the claims at the time of the main search. However,
further searches under s.17(6) are carried out at the comptroller's
discretion. A request for a single further search should normally be
fulfilled [see also
18.41](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-41).
However, where multiple further searches are requested, the examiner
should use their judgement to decide how many to perform. The filing of
a large number of further searches is undesirable -- for efficiency and
fairness the examiner should consider carrying out fewer searches than
requested. (Further searches are not to be confused with supplementary
searches under s.17(8) of an application which has been amended or
corrected, see 17.120-17.123). A further Form 9A (or Form 9 see 17.02)
and search fee must be filed for each such search requested, and this
must be done at least three months before the end of the compliance
period (including any extensions). A form 9A filed for a further search
request is not subject to additional excess claims fees. The period for
filing the form and fee may be extended at the comptroller's discretion.

\[Where fewer searches have been carried out than have been requested,
this should be noted in a minute. The applicant should be made aware
that the examiner has not carried out (all of) the requested further
searches, and that a refund will be issued on any unsearched Form 9A.
The examiner should send a minute to Formalities asking them to refund
the fee.\]

\[A further search should normally be carried out concurrently with
"primary" searches received in the group at the same time, whether or
not the application has been published. During the 5-week period of the
publication cycle, COPS is locked and so won't accept any changes to
bibliographic data or processing status. It is therefore not possible to
generate an internal or external search report in the usual way (as
nothing can be entered into the "citation" and "field of search"
tables), and so if the further search is likely to be due during the
publication cycle it is advisable to issue it before it enters the
cycle. The initial search letter (eg SL1 or SE1) provides (under the
heading "publication") an estimated date after which the preparations
for publication will be completed -- the case will generally enter the
publication cycle shortly after that date, and so this may be used to
estimate the date from which COPS will be locked. Of course, the actual
searching can be done during the publication cycle, and the case remains
available on PDAX. If the search is urgent (for example if it has been
accelerated) and so has to be done during the publication cycle, then
the search results will need to be communicated to the applicant without
completing a conventional search report. This may be done by creating a
miscellaneous letter giving information about the claims searched, the
citations and the field of search as free text, and enclosing copies of
any citations. where appropriate (see 17.104.1). The case should then be
diarised to a date after publication and a full search report created at
this time in the usual way.\]

\[A report of a further search should be accompanied by letter SL4 and a
copy of any additional citations ([see 17.104.1](#ref17-104-1)). When
further search is carried out before Apublication the resulting external
search report will be published together with the original external
search report. To ensure that this happens, the Examination Support
Officer imports the later search report and marks it as "P" in the PDAX
dossier allowing it to be published. The Examination Support Officer
will then retrieve the sub-file and add a copy of the later search
report, again marking the report as "P" allowing it to be published.\]

\[The search examiner should not respond to a request for a further
search under s.17(6) by issuing an ABS letter ([see
17.94.4-17.94.9](#ref17-94-4)) requesting amendment or other action
before search\]

### 17.112 {#section-44}

If carrying out searches on more than one invention simultaneously the
results of all the searches may be reported on a single search report
providing it has been made clear in a letter or report what the
different inventions are and the search report makes clear to which
claims each document is relevant.

### 17.113 {#ref17-113}

The practice of filing more than one Form 9A and search fee before the
initial search report has issued is undesirable, since it amounts to an
admission that the claims have been filed in the knowledge that they do
not comply with s.14(5)(d). In this situation the normal course of
action should be to search only the first invention. In which case, for
efficiency, the search examiner may also decide to assess plurality only
to the extent needed to search the first invention. However, the search
examiner has discretion to carry out all searches and fully assess
plurality. Any assessment of plurality should be made without reference
to the applicant's actions. If multiple searches are carried out the
search letter heading should be modified to refer to both section 17(5)
and 17(6). If after carrying out a full assessment of plurality it turns
out that the claims have unity of invention, a single search should be
made and the fees for the superfluous Form(s) 9A should be refunded.

\[Where the search examiner has carried out fewer searches than have
been requested, this should be noted in a minute. The applicant should
be made aware that not all requested searches have been completed, that
unsearched Form 9As will be refunded, and (where applicable) that a full
assessment of plurality has been deferred. The examiner should send a
minute to Formalities asking them to refund any unsearched Form 9A
fees.\]

### 17.114 {#section-45}

When Patents Form 9A (or Form 9, [see 17.02](#ref17-02) is filed
requesting a further search and the relevant subject-matter is dealt
with by an examining group other than that in which the original search
was performed, the further search can be carried out in the other group
if desirable for quality and/or efficiency.

### 17.114.1 {#ref17-114-1}

Requests from applicants to search deleted claims or unclaimed subject
matter in the description should be refused by the examiner under s.
17(5)(b) PA 1977 ([see 17.94](#ref17-94)), on the basis that the search
would serve no useful purpose on the application for the time being
constituted - the claims having been deleted no patent is currently
being sought in respect of them. Applicants are free to reinstate
previously deleted claims or to add new claims by amendment and ask for
a search to be performed, or, file the new/previously-deleted claims as
a divisional application and request it be searched in the normal way.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 17(7)**
  After a search has been requested under this section for an application the comptroller may at any time refer the application to an examiner for a supplementary search, and subsections (4) and (5) above shall apply in relation to a supplementary search as they apply in relation to any other search under this section.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 17.115 {#ref17-115}

A supplementary search will generally be necessary as part of the
substantive examination process in order to detect any relevant
documents which have been published since the original search (although
see
[89B.12-89B.12.3](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-12)
for specific points relating to s.89 cases). This "top-up" search will
also normally include a check for published equivalent cases, both to
identify any application which is potentially in conflict with the
application in suit (and which might therefore give rise to action under
s.18(5) or s.73(2) -- [see
18.91-18.97.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-91)
and
[73.05-73.12](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-05),
and to identify any further citations raised against these equivalents
[see
18.10.1-18.10.4](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-10)).
The examiner may take into account how closely the claims of the
equivalent application resemble those of the application in suit, and
the apparent relevance (from the category, if stated) of the
equivalent's citations. A supplementary search of this type may not be
necessary where the initial search is performed at such a time as to be
effectively a "top-up" search, for example where the first search on a
divisional is performed later than 21 months from the priority date.\
\
Whilst the application of CPC terms to EPODOC abstracts may occur
several months later than 21 months from the priority date ([see also
17.118](#ref17-118)), databases should be sufficiently up to date at
about 21 months to conduct a search for prior art falling in the s.2(3)
field provided that the search relies on IPC terms. Note that in any
case, a search cannot be considered complete unless it involves search
of both CPC and IPC ([see 17.55](#ref17-55)). Where a priority claim is
invalid or relinquished, a top-up search before 21 months from filing
may not uncover all relevant s.2(3) prior art because some documents may
not yet be published.\
\
Online databases should also be up to date three months from the
priority or filing date of an application for prior art in the s.2(2)
field. A top-up search should cover the period of at least 6 months
before the original search date, to account for the time taken to
classify published documents to CPC collections. If a document found
lies in the field defined by s.2(3), consideration should be given as to
whether there may be an earlier-published equivalent ([see also
17.90](#ref17-90)). The examiner should ensure that a complete search in
respect of at least s.2(2) art has been made before grant because only
applications forming part of the s.2(3) field can be cited under s.73(1)
after grant.

\[If a priority date has already been determined as a result of the
practice described in 18.15, then the examiner should take this into
account when deciding when to carry out a top-up search for s.2(3) art.
That is, if a priority claim is valid, then databases should be
sufficiently up to date at about 21 months after the priority date. If
the priority claim is invalid then the top-up for s.2(3) art should be
delayed until at least 21 months after the filing date. If the priority
date has not been determined, then the examiner should bear in mind that
the priority claim may be invalid when carrying out a top-up search for
s.2(3) art. The examiner should then decide the most effective course of
action, which may be for example to: delay the top-up until 21 months
from filing, check the priority claim, or continue on the basis that a
top-up 21 months from the declared priority is appropriate. \]

\[A top-up internal search report should be generated on PROSE to record
details of the updated search. It should normally be sufficient to
update a search once only, unless that top-up search occurs very early
(e.g. as a result of combined search and examination or a request for
accelerated examination). The search statement used, or a comment that
the search statement was the same as that for the original search,
should be recorded. The subclasses searched need only be recorded in the
Field of Search if they were not searched in the search being updated.

However, a record should always be made of the search strategy used,
even if it is the same as the strategy used in the original search. This
can either be recorded in the top-up search report or in an OSS (Online
Search Strategy) document in PROSE. Any newly-cited documents should be
recorded on the report and the Examination Support Officer instructed to
issue them [see 17.104.1, 17.105.2](#ref17-104-1). If the search is
further updated before grant, a further top-up internal search report
should be produced, and the Examination Support Officer should be
instructed as above as to which documents are to issue if further
citations are found. A further updating search is necessary only to the
extent that the first updating search was incomplete.\]

\[When conducting a top-up search the examiner should take care to
identify equivalents and consider any documents cited against them. In
addition, online file inspection may provide further details of the
significance of a citation; [see
18.10.1-18.10.4](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-10).
A note should be made on the top-up search reporting whether or not such
citations have been checked and -- if not -- the reasoning why not.\]

\[Examiners should check the CPC symbols which have been applied to the
application in suit and other family members. If this check reveals CPC
symbols that were not searched during the original search, these should
be recorded in the internal top-up search report and the examiner should
consider whether the search needs to be extended accordingly. If the
examiner considers extension of the search is not necessary, they should
include a brief explanation on the top-up search report.\]

\[Patent examiners should only use the top-up internal search report for
notes relating to the search. Any other notes, for example recording the
examiner's decision to drop an objection, should be added as a minute to
the PDAX dossier.\]

\[If a top-up search is carried out at the same time as finding the
application in order for grant, the examiner should create an internal
search report (ISR-TOP) and a bundle consisting of the associate top up
search checklist and a grant form. The examiner should complete the
grant form [see
18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent#ref18-86)
and send a "ISSUE RELEVANT INTENTION TO GRANT LETTER" message to EA
GRANT mailbox as described in [18.03.6, 18.81 and
18.86.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent#ref18-03-6).\]

### 17.116 {#section-46}

This supplementary search may be deferred if thought appropriate. Indeed
the examiner should use their judgement to time the top-up search so
that it is as efficient as possible, for example deferral is likely to
be appropriate if much relevant prior art has already been cited and no
amendment has yet been made, if the first examination report is only a
preliminary one (see 18.48), or if the claims are unclear; a note to
this effect should be made on the internal search report and the
applicant informed accordingly.

### 17.117 {#ref17-117}

It will also be necessary to perform a search under s.17(7) if the
search has been wholly or partially deferred under s.17(5), or if it is
discovered on examining the specification that the search already
performed under s.17(4) was deficient, for example if it transpires that
the true scope of the invention is broader than had been thought, or if
the specification has been amended in a way not anticipated by the
original search strategy (see also [17.120-123](#ref17-120) regarding
the possibility of a fee for such a supplementary search). The course
and results of such a search should be recorded on a top-up internal
search report.

\[The Examination Support Officer should be instructed to issue any
newly-cited documents ([see 17.104.1, 17.105.2](#ref17-104-1).

\[For CSE's, in the event that additional searching is conducted before
the case enters the A-publication cycle (for example if the case is
accelerated, or the first search was truncated and the applicant
responds with early amendments), examiners should ensure that an
external search report (ESR) is manually added to the ISR-TOP bundle,
with the ERC2P checklist edited to instruct the Examination Support
Officer to issue the ESR and import it into PDAX. This ESR will be
included as part of the published A-specification (see also
[18.12](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-12)).\]

### 17.118 {#ref17-118}

When an application is sent to grant before 21 months or the substantive
examiner is not otherwise satisfied that the supplementary search is
complete for art in the s.2(3) field, the application should be diarised
for return at some appropriate date after grant for the search to be
completed and the applicant informed accordingly. If a document forming
part of the state of the art by virtue of s.2(3) comes to the notice of
the substantive examiner after the issue of a report under s.18(4),
action may be taken under s.73(1) after grant ([see
73.02-73.03](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-02)).

To ensure the applicant is informed, the examiner should indicate that
RC46 clause is needed in the intentioin to grant letter by checking that
box in the grant form ([see
18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent#ref18-86)).\]

\[The examiner should create an entry in the PD electronic diary system
and add the diary action to the dossier in PDAX and add the date to the
grant form. The diary entry should state the examining group after the
application number in the format GB0000000.0 -- EX00.\]

\[Online databases will usually be sufficiently up to date in respect of
s.2(3) art in addition to s.2(2) art at about 21 months from the
priority date of the application. Examiners should use their judgment as
to whether the top-up search made is likely to be complete.\]

\[A top-up internal search report (ISR-TOP) should be generated on PROSE
to record details of the updated search. Any additional fields of search
or documents should be recorded in the free-text areas of the ISR-TOP. A
checklist for the post grant top-up search report should be generated
instructing the ESO to import the ISR-TOP.\]

### 17.119 \[deleted\]

  -----------------------------------------------------------------------
   

  **Section 17(8)**

  A reference for a supplementary search in consequence of\
  (a) an amendment of the application made by the applicant under section
  18(3) or 19(1) below, or\
  (b) a correction of the application, or of a document filed in
  connection with the application, under section 117 below,\
  shall be made only on payment of the prescribed fee, unless the
  comptroller directs otherwise.
  -----------------------------------------------------------------------

### 17.120 {#ref17-120}

Subsection (8) was added by the CDP Act along with s.18(1A). Where,
before or during substantive examination, the substantive examiner
considers that a supplementary search is necessitated by an amendment or
correction in respect of an application, an extra fee is payable under
s.17(8) and s.18(1A). This is not to be confused with a further search
under s.17(6) at the request of the applicant for a second or subsequent
invention which was claimed in the application as filed and identified
in the search report, [17.111-114](#ref17-111). Section 17(8) applies to
amendments made either in response to the substantive examiner's report
o r the applicant's volition, as well as to corrections.

### 17.121 {#section-47}

If a supplementary search is required a request for a further search fee
may be considered in the following situations:\
\
i) A correction is made that alters what has to be searched and so
necessitates a change to the search statement;\
\
ii) An amendment shifts the scope of the main claim to include features
of the description which were justifiably not searched. For example,
where searching these features would not have been practical within a
single search, or would have required undue additional searching
([bearing in mind 17.32](#ref17.32)), if the search examiner
communicated the scope of search clearly to the applicant, a further
search fee request may be considered if the amended claims are no longer
within the scope of the search. If the amendment could have readily been
predicted then a request is unlikely to be justified;\
\

iii\) Where a plurality objection was raised in the initial search
report, international preliminary search report or written opinion and
the applicant deletes the first claimed invention, resulting in claim 1
becoming an unsearched invention. If multiple inventions remain in the
application, any searches of later inventions should be dealt with in
accordance with s.17(6).

### 17.122 {#ref17-122}

A request for a further search fee is not appropriate in the following
circumstances:\
\
i) To overcome an unfortunate choice of search areas or search statement
by the search examiner;\
\
ii) When the amended claim remains within the scope of the original
search statement;\
\
iii) When the sole reason for the further search fee is that the
original search was truncated;\
\
iv) When the sole reason for the further search is a decision by the
Examiner to extend the search to more relevant areas. For example, where
classification scheme revision has created a highly relevant place not
available at the search stage, or other classification symbols have been
applied to the published document by the EPO;\
\
v) Where the applicant broadens a claim to achieve consistency with the
description (for example so that an embodiment falls within the scope of
the claim) but there was no comment from the search examiner that the
original search was not widened to take account of the description ([see
17.37](#ref17-37)) and the applicant could have reasonably expected such
a comment.

### 17.123 {#section-48}

r.27(6) is also relevant.

Where action is taken under s.18(1A), \[see
18.03.2-03.6\](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref(18-03-2),
and a fee for a supplementary search is paid, it should be accompanied
by a fresh Form 9A (or Form 9, [see 17.02](#ref17-02) completed
accordingly. A form 9A filed for a supplementary search request is not
subject to additional excess claims fees. A search report is in due
course issued reporting the result of the supplementary search.

\[The Examination Support Officer should be instructed to issue any
newly-cited documents ([see 17.104.1, 17.105.2](#ref17-104-1)).\]
:::
:::
:::
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
