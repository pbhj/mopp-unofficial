::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 276: Persons entitled to describe themselves as patent agents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (276.01 - 276.07) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 276.01

The main effect of this section is to reserve the use of the title
"patent attorney" and patent agent to individuals who are registered
patent attorneys, and partnerships and bodies corporate in which
registered patent attorneys are involved. Use of the title by any person
not authorised to do so is a criminal offence.

  -----------------------------------------------------------------------
   

  **Section 276(1)**

  An individual who is not a registered patent attorney shall not ­\
  (a) carry on a business (otherwise than in partnership) under any name
  or other description which contains the words "patent agent" or "patent
  attorney"; or\
  (b) in the course of a business otherwise describe themselves, or
  permit themselves to be described, as a "patent agent" or "patent
  attorney".
  -----------------------------------------------------------------------

### 276.02

Subsection (1) relates to individuals who are not registered patent
attorneys. Paragraph (a) provides that such a person may not carry on a
business under a name or other description including the words "patent
agent" or "patent attorney". This paragraph does not extend to
partnerships, as these are dealt with in subsection (2). Paragraph (b)
extends the prohibition to otherwise describing himself, or permitting
himself to be described, as a "patent agent" or "patent attorney".

  -----------------------------------------------------------------------
   

  **Section 276(2)**

  A partnership or other unincorporated body shall not ­\
  (a) carry on a business under any name or other description which
  contains the words "patent agent" or "patent attorney"; or\
  (b) in the course of a business otherwise describe itself, or permit
  itself to be described as, a firm of "patent agents" or "patent
  attorneys", unless the partnership or other body is registered in the
  register kept under section 275.
  -----------------------------------------------------------------------

### 276.03 {#ref276-03}

Subsection (2) relates to partnerships and other unincorporated bodies.
It stipulates that for a partnership or other such body to be able to
use the title "patent agent" or "patent attorney", it must be registered
in the register kept under section 275 of the CDP Act. Paragraphs (a)
and (b) are analagous to paragraphs (a) and (b) of subsection (1). Under
s.276(3) similar provision is made in respect of a body corporate and
its directors.

### 276.03.1 {#ref276-03-1}

Previously, if all the partners were not registered patent attorneys,
certain prescribed conditions had to be met for a partnership to use the
title "patent agent" or "patent attorney". The conditions were
prescribed in rules made under section 279 of the CDP Act, namely the
Patent Agents (Mixed Partnerships and Bodies Corporate) Rules 1994 (SI
1994 No. 362) which came into force on 24 March 1994. These Rules set
out the conditions to be satisfied. However, on 1 January 2010 section
185(4) of the Legal Services Act 2007 amended this section of the CDP
Act to change the prescribed condition to that detailed in subsection
(3) -- [see paragraph 276.03](#ref276-03) above. The Patents Agents
(Mixed Partnerships and Bodies Corporate) Rules 1994 no longer apply as
section 279 of the CDP Act was repealed under s.185(5) of and Sch.23 to
the Legal Services Act 2007.

  -----------------------------------------------------------------------
   

  **Section 276(3)**

  A body corporate shall not ­\
  (a) carry on a business (otherwise than in partnership) under any name
  or other description which contains the words "patent agent" or "patent
  attorney"; or\
  (b) in the course of a business otherwise describe itself, or permit
  itself to be described as, a "patent agent" or "patent attorney",
  unless the body corporate is registered in the register kept under
  section 275.
  -----------------------------------------------------------------------

### 276.04 {#section-2}

Subsection (3) relates to bodies corporate entitled to use the title
"patent agent" or "patent attorney" and makes similar provisions to
those of subsection (2) in respect of partnerships, with the requirement
for the body corporate to be registered in the register kept under
section 275 of the CDP Act.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 276(4)**
  Subsection (3) does not apply to a company which began to carry on business as a patent agent before 17th November 1917 if the name of a director or the manager of the company who is a registered patent attorney is mentioned as being so registered in all professional advertisements, circulars or letters issued by or with the company's consent on which its name appears.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 276.05 {#section-3}

There is a special saving for certain companies which began carrying on
the business of a patent attorney before 17th November 1917. This
re-enacts section 114(2)(a) of the Patents Act 1977 (which in turn
re-enacted earlier provisions).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 276(5)**
  Where this section would be contravened by the use of the words "patent agent" or "patent attorney" in reference to an individual, partnership or body corporate, it is equally contravened by the use of other expressions in reference to that person, or his business or place of business, which are likely to be understood as indicating that he is entitled to be described as a "patent agent" or "patent attorney".
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 276.06 {#section-4}

The prohibition on use of the descriptions "patent agent" and "patent
attorney" extends to other expressions which are likely to be understood
as indicating entitlement to such a description, for example "patent
agency".

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 276(6)**
  A person who contravenes this section commits an offence and is liable on summary conviction to a fine not exceeding level 5 on the standard scale; and proceedings for such an offence may be begun at any time within a year from the date of the offence.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 276.07 {#section-5}

Contravention of the section is an offence punishable on summary
conviction by a fine not exceeding level 5 on the standard scale.
Proceedings may be begun within one year of the date of an offence.
These provisions echo the repealed section 114(3) and (4) of the Patents
Act 1977.

  -----------------------------------------------------------------------
   

  **Section 276(7)**

  This section has effect subject to ­\
  (a) section 277 (persons entitled to describe themselves as European
  patent attorneys, &c), and\
  (b) section 278(1) (use of term "patent attorney" in reference to
  solicitors).
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
