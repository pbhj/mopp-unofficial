::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 18: Substantive examination and grant or refusal of patent {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (18.01 - 18.99) last updated: April 2024.
:::

::: govuk-grid-column-two-thirds
::: {#default-id-6d9c2c45 .gem-c-accordion .govuk-accordion .govuk-!-margin-bottom-6 module="govuk-accordion gem-accordion ga4-event-tracker" ga4-expandable="" anchor-navigation="true" show-text="Show" hide-text="Hide" show-all-text="Show all sections" hide-all-text="Hide all sections" this-section-visually-hidden=" this section"}
::: govuk-accordion__section
::: govuk-accordion__section-header
## [Section 18: Substantive examination and grant or refusal of patent]{#default-id-6d9c2c45-heading-1 .govuk-accordion__section-button} {#section-18-substantive-examination-and-grant-or-refusal-of-patent .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Section 18: Substantive examination and grant or refusal of patent\",\"index_section\":1,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-1 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-1" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Section 18: Substantive examination and grant or refusal of patent\",\"index_section\":1,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.01

This section sets out the conditions necessary for an application to
proceed to substantive examination, makes provision for carrying out the
examination and reporting the result, provides for the applicant making
observations on the report and if necessary amending the application,
specifies that a patent may then be granted or refused, and finally
deals with the case of conflicting applications from the same applicant.
Time limits and other provisions relating to these matters are
prescribed in rr.28, 29 and 30.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Information security: a reminder]{#default-id-6d9c2c45-heading-2 .govuk-accordion__section-button} {#information-security-a-reminder .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Information security: a reminder\",\"index_section\":2,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-2 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-2" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Information security: a reminder\",\"index_section\":2,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.01.1

s.118(2) is also relevant

Combined search and examination actions are generally carried out before
A publication, and accelerated examinations may also be performed before
A publication. The guidance in this section therefore relates in part to
pre-publication actions. Before A publication, the application, the
search, examination or combined search and examination report, and any
other documents or information concerning the content of the application
(other than that prescribed under s.118(3)) must be protected and must
not be communicated to anyone outside the Office other than the
applicant or their designated representative -- [see 118.16 to
118.16.1](/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-16)
for further guidance on these issues.

### 18.01.2

More generally, in all cases, whether pre-or post-publication,
formalities examiners should ensure that any communications (including
telephone conversations) are directed to the intended recipient.

\[Where correspondence from the Office is reported as never having
arrived at its intended destination, or is reported as being misdirected
or delayed, this fact should be recorded by sending a minute to the
relevant formalities group with any relevant details. Use of r.111 to
extend a deadline may only be authorised by the relevant Head of
Administration ([see
123.47.1](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-47-1).\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Code of Practice]{#default-id-6d9c2c45-heading-3 .govuk-accordion__section-button} {#code-of-practice .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Code of Practice\",\"index_section\":3,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-3 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-3" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Code of Practice\",\"index_section\":3,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.01.3 {#ref18-3-1}

The Code of Practice (the second part of which can be found below)
identifies best practice points for patent applicants and agents, which
if followed widely will lead to savings and efficiencies in the Office,
and consequently to better service and better value. Examiners and other
Office officials may on occasion draw the Code to the attention of the
applicant or agent, and may ask for it to be complied with before the
case is processed further if that would be more efficient.

### 18.01.4 {#section-3}

However, it is not to be expected that best practice can always be
adhered to. The Office has no right to demand compliance with the Code
of Practice because the Code is advisory only and has no legal force.

### 18.01.5 {#section-4}

If an application does not comply with one of the points of the Code of
Practice then this does not necessarily justify an objection under the
Patents Act or Rules. For example, an application having more than one
independent claim in one category would not comply with code point 1e,
but this would not usually justify an objection under s.14(5)(b). When
drawing attention to non-compliance with the Code of Practice, examiners
should therefore make it clear whether they are also raising a formal
objection under the Act or Rules.

### \[Code of practice for applicants and agents\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Prosecuting patent applications]{#default-id-6d9c2c45-heading-4 .govuk-accordion__section-button} {#prosecuting-patent-applications .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Prosecuting patent applications\",\"index_section\":4,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-4 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-4" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Prosecuting patent applications\",\"index_section\":4,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
This part of the Code of Practice relates to the prosecution of patent
applications. The process of examination and amendment of patent
applications ideally proceeds by eliminating objections until the
application can be granted. To expedite the granting of an application,
the substantive examiner will attempt to indicate all objections in the
first report issued under s.18(3). The main exception to this will occur
where only major objections (such as patentability issues) are raised
and minor matters are deferred until the major points have been settled.
This process of eliminating objections minimises the time taken
conducting a comprehensive review of the specification as it will
generally only be necessary to decide whether or not the amendments or
arguments advanced meet the objections raised. However, for this process
to be efficient for both sides it is necessary that the following points
are observed.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Point 6: Responding to the examiner's objections]{#default-id-6d9c2c45-heading-5 .govuk-accordion__section-button} {#point-6-responding-to-the-examiners-objections .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Point 6: Responding to the examiner’s objections\",\"index_section\":5,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-5 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-5" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Point 6: Responding to the examiner’s objections\",\"index_section\":5,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
-   a\) a full response should be made to each and every objection
    raised in an examination report through amendment and/or argument so
    as to progress the application towards grant [see
    18.63-18.63.3](#ref18-63)
-   b\) when objections are to be met by amendment, an appropriate
    explanation should be provided, in particular, as to how the
    amendment meets the objection and how it is supported by the
    specification as originally filed \[see [18.63-18.63.3](#ref18-63)
-   c\) amendments must neither add new technical disclosure, nor
    broaden the scope of claims beyond the disclosure as originally
    filed ([see 76.04 to
    76.23](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-04))
-   d\) pages showing the amendments, in manuscript or distinctive type,
    should be filed in addition to the clean replacement pages for the
    specification ([see 18.61](#ref18-61))
-   e\) consequential amendment of the description may be deferred until
    the independent claims have been agreed upon ([see
    18.52](#ref18-52))
-   f\) the Office recommends the use of electronic filing when
    submitting amended pages or arguments in response to the examiner's
    objections, whether the original application was filed
    electronically or not. See [Apply for a
    patent](https://www.gov.uk/apply-for-a-patent)
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Point 7: Time issues in patent processing]{#default-id-6d9c2c45-heading-6 .govuk-accordion__section-button} {#point-7-time-issues-in-patent-processing .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Point 7: Time issues in patent processing\",\"index_section\":6,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-6 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-6" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Point 7: Time issues in patent processing\",\"index_section\":6,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
The efficiency of the patent granting process depends in no small part
on the timeliness of the Office as well as of applicants and agents. The
Office will set a timetable for the various stages in processing a
patent application to grant, by setting periods for responding to
various Office letters and reports. Applicants and agents should use
their best endeavours to act within the periods for response that will
be specified. Requests for extensions of those periods for response will
be considered if made in the recommended way and in accordance with the
legislation relating to extensions of time. Requests for processing to
be accelerated will also considered if made in the recommended way.

-   a\) action should be taken within a period specified for response;
    and not delayed until the last day if that is avoidable
-   b\) a request for extension of a period for response should be made
    in a timely manner. The request may be e-mailed to the dedicated
    <pateot@ipo.gov.uk> email address (see Requesting an extension of
    time by e-mail) in which case, an automatic acknowledgement of
    receipt will be issued. Alternatively, the extension may be
    requested in the response letter itself. Where an as-of-right
    extension is not available, the request should be supported by an
    adequate reason that is peculiar to the circumstances of the case
    (see [18.53](#ref18-53) - [18.60](#ref18-60), particularly
    [18.57.1](#ref18-57-1))
-   c\) a request for accelerated processing of search or examination
    should be supported by an adequate reason that is peculiar to the
    circumstances of the case (see
    [17.05.1-2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-05-1)
    and [18.07](#ref18-07) - [18.07.3](#ref18-07-3)).
-   d\) when time is short, the covering letters accompanying responses
    to Office actions should be marked "urgent" (see
    [18.72.1](#ref18-72-1))
-   e\) notification of withdrawal of an application must reach us
    before the date on which preparations for publication are completed
    if it is wished to prevent publication: the publication process does
    not permit later withdrawals ([see
    14.205](/guidance/manual-of-patent-practice-mopp/section-14-the-application#ref14-205)).
-   f\) voluntary amendments should be filed early ([see
    19.13](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-13))
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Point 8: Filing divisional applications]{#default-id-6d9c2c45-heading-7 .govuk-accordion__section-button} {#point-8-filing-divisional-applications .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Point 8: Filing divisional applications\",\"index_section\":7,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-7 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-7" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Point 8: Filing divisional applications\",\"index_section\":7,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
-   a\) divisional applications should be filed early as time is limited
    under rule 30 ([see
    15.20.2](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-20-2)
-   b\) a divisional application should have a single claimed invention
    (or inventive concept) that is distinct from that of the parent
    application (see
    [15.20.2](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-20-2)
    and
    [15.29](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-29))
-   c\) the description and drawings of the divisional application
    should be provided in a form appropriate to the invention of the
    divisional application, and irrelevant material excised ([see
    15.32](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-32))
-   d\) amendments already known to be required at the date of filing of
    the divisional application should be made ([see
    15.32](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-32))
-   e\) a cover letter should be included with the filing of a
    divisional application indicating where the claims derive support
    from in the parent application.

If the above points are not met on filing a divisional application,
suitable amendments should be filed as soon as possible ([see
15.36](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-36)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Point 9: Forms and formalities after filing]{#default-id-6d9c2c45-heading-8 .govuk-accordion__section-button} {#point-9-forms-and-formalities-after-filing .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Point 9: Forms and formalities after filing\",\"index_section\":8,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-8 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-8" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Point 9: Forms and formalities after filing\",\"index_section\":8,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
-   a\) information about changes should be notified promptly and by the
    recommended method. Global changes affecting more than one patent
    application should be notified by one communication, with a list of
    the patent application numbers (or patents) appended. The
    distinction between amendment and correction should be appreciated
    ([see
    19.03](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-03)),
    and where a correction is proposed, sufficient evidence
    demonstrating that the correction was the applicant's intention at
    the time of filing should be provided ([see
    117.19](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-19)).
    Supporting proof where needed should be adequate to establish and
    verify the circumstances.

Change of address (only): a letter will suffice for this, whether it is
amendment or correction ([see
19.05](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-05),
[117.03](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-03)).

Change of agent: Form 51 is used for this amendment ([see
14.04.12](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-04-12)).

Appointment of an agent for the first time (after the filing date): Form
51 is used

Amendment or correction of other data on the application form: Form 20
is used for correction of an applicant's name, and may require
supporting proof. All other amendments or corrections should be made in
writing (see [19.05 to
19.12](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-05)
and [117.17 to
117.21](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-17)).

Amendment of name (ownership) in the Register: Form 20 is used and may
require supporting proof ([see
32.06](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-06)).

  -----------------------------------------------------------------------
   

  **Section 18(1)**

  Where the conditions imposed by section 17(1) above for the comptroller
  to refer an application to an examiner for a search are satisfied and
  at the time of the request under that subsection or within the
  prescribed period\
  ­(a) a request is made by the applicant to the Patent Office in the
  prescribed form for a substantive examination; and\
  (b) the prescribed fee is paid for the examination; the comptroller
  shall refer the application to an examiner for a substantive
  examination; and if no such request is made or the prescribed fee is
  not paid within that period, the application shall be treated as having
  been withdrawn at the end of that period.
  -----------------------------------------------------------------------

### 18.02 {#ref18-02}

(r.28(1) and r.28(2) are also relevant)

The request for substantive examination must be made on Patents Form 10.
This, together with the prescribed fee, including any excess pages fees,
must be filed within six months of the date of publication of the
application under s.16, unless the application falls into one of the
categories referred to in (a)-(d) below:­

r.28(5) is also relevant

\(a\) for an application which claims an earlier date of filing under
s.8(3), 12(6), 15(9) or 37(4), Form 10 and the fee must be filed within
two months of filing the application or, if it expires later, within two
years of the declared priority date, or, where there is no declared
priority date, within two years of the date treated as the date of
filing;

r.28(3) is also relevant

\(b\) for an application not falling within (a) above whose publication
under s.16 has been prohibited or delayed because of a direction made
under s.22(1) or (2), Form 10 and the fee must be filed within two years
of the declared priority date, or, where there is no declared priority
date, of the date of filing;

\(c\) for an application which has been converted from one for a
European patent (UK) the period is governed by r.60, ([see
81.17](/guidance/manual-of-patent-practice-mopp/section-81-conversion-of-european-patent-applications/#ref81-17));

\(d\) for an international application for a patent (UK), the period is
governed by r.68(4), [see
89B.09](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-09).

In the case of an application in category (a), (b), (c) or (d), the
period prescribed for filing Form 10 and the fee can be extended in
accordance with r.108(2) or (3) together with r.108(5) to (7), [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).

\[ The COPS records are combed weekly. Applications which have between
two weeks and three weeks remaining of the prescribed six month period
for filing Form 10 are identified on the "Form 10 Picklist". (The "Form
10 Picklist" cannot identify applications which fall into categories
(a) - (d) listed in 18.02). Any applications on which Form 10 and fee
are not filed are selected by COPS on the "Form 10 Time Limit Report"
one month and seven weeks after the expiry of the prescribed period.
Such applications should be checked by the relevant Formalities Manager
to confirm termination. (For termination procedure, [see
14.199](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-199)).
For applications not identified by the "Form 10 Picklist" but on which
Form 10 has yet to be filed, the formalities examiner should identify
those where the period for filing Form 10 has not expired. The file
should then be diarised using the PD diary. If Form 10 is not
subsequently filed in time, the application should be passed to the
relevant Formalities Manager to confirm termination. \]

### 18.03 {#ref18-03}

Form 10 and the examination fee, including any excess pages fees, can be
filed at any time before the end of the prescribed period; it may be
filed at the time of making the application if the applicant so wishes.
Applications on which Forms 9 or 9A ([see
17.02](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-02))
and 10 have been filed on the same date (apart from PCT s.89 cases where
an international search report has been communicated to the Office) will
receive combined search and examination before publication under s.16,
unless the applicant has explicitly stated in writing that this is not
wanted. If an applicant has initially filed only Form 9 or Form 9A later
files Form 10 before the search has commenced, the application will
automatically be subject to Combined Search and Examination (CS&E),
unless the applicant has specifically stated that they do not want CS&E
(N.B. It is the responsibility of formalities examiners to the check the
Form 10 and any accompanying documents for an indication that CS&E is
not required). However, in such cases the search and examination can
only be combined if the examiner is aware of the request before
beginning the search. A request for a refund of the examination fee and
also of the search fee for combined search and examination cases is
normally acceded to if it is received before issue of the substantive
examiner's report. However, the examination fee is not refunded if
following combined search and examination a search report under s.17(5)
has issued and the examination has been performed but no report under
s.18 has issued ([see 18.84.1](#ref18-84-1-18-84-3)). In all cases such
refunds are a matter of discretion, not a right.

\[ A COPS flag for combined search and examination will be set
automatically to 'yes' if Forms 9 or 9A and 10 are logged with the same
filing date. The formalities examiner should identify those applications
on which both Form 9 or 9A and Form 10 have been filed on the same date
(other than PCT s.89 cases where an international search report has been
communicated to the Office). The combined search and examination will be
identifiable by the appropriate cover label and PDAX message. However,
it will not be necessary to label the dossier cover if the applicant has
stated either on Form 10 or in a separate letter that combined search
and examination is not wanted. In this case, the formalities examiner
should set the combined search and examination flag to 'no', and add a
minute to the PDAX dossier explaining the situation to the examiner.

\[ In the event of Form 10 being filed after Form 9 or 9A, the
formalities examiner should check whether the search report has issued.
If it has already issued, the applicant should be informed, as indicated
later in this paragraph, that the request was received too late. On the
other hand, if the search report has not issued, the formalities
examiner should arrange without delay for the Form 10 to be put on file
and for a minute to be added to the PDAX dossier. The formalities
examiner should also place the appropriate label on the dossier cover
and send the appropriate PDAX message to the substantive examiner to
make them aware that combined search and examination has been requested.
The combined search and examination COPS flag should also be set to
'yes'. On receipt of a labelled file in the examining group, the
Examination Support Officer should check with the search examiner to see
whether the search has been started. If it has not, the examiner in due
course should carry out the examination at the same time as the search.
In the remote event of the Form 10 and the written request being
received during a search, the search examiner can opt, depending on the
stage reached, to carry out the examination or defer it until the normal
examination stage. Whenever the examination is not carried out the
applicant should be informed by the formalities examiner or the search
examiner, as appropriate, that the request for substantive examination
was received too late to allow combined search and examination and that
substantive examination will occur in due course after s.16 publication.
The applicant should also be informed that accelerated examination will
be considered if requested and a reason for the request is given in
writing. If necessary, the combined search and examination flag on COPS
should also be changed from 'yes' to 'no'. The request for a refund
should be dealt with by the appropriate formalities group with the
examining group being informed immediately. An examination report or
combined search and examination reports should not be issued after
receipt of a request for a refund, even if the report or reports have
already been prepared.\]

  -----------------------------------------------------------------------
   

  **Section 18(1A)**

  If the examiner forms the view that a supplementary search under
  section 17 above is required for which a fee is payable, he shall
  inform the comptroller, who may decide that the substantive examination
  should not proceed until the fee is paid; and if he so decides, then
  unless within such period as he may allow\
  (a) the fee is paid, or\
  (b) the application is amended so as to render the supplementary search
  unnecessary,\
  he may refuse the application.
  -----------------------------------------------------------------------

### 18.03.1 {#ref18-03-1-18-03-6}

Subsection (1A) gives powers to the comptroller where an applicant's
action makes a supplementary search necessary at the substantive
examination stage. Section 17(8) provides for the payment of a fee for
such a search ([see
17.120-123](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-120)).

### 18.03.2 {#ref18-03-2}

If, before or during substantive examination, the substantive examiner
reports that a supplementary search is necessitated by an amendment or
correction as referred to in
[17.120-123](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-120),
the applicant may be so informed in a letter giving a specified period
to pay the fee or amend the application so as to remove the need for the
search or submit observations, and warning that the application may
otherwise be refused. Where such an amendment is submitted but fails to
remove the need for the search, the matter is pursued until either a
satisfactory amendment is filed or the fee is paid, or it becomes
evident that the dispute cannot be resolved.

\[ Senior Examiners may decide whether to take action under ss.17(8) and
18(1A), without reference to their Group Head; Examiners should consult
their senior officer as to the appropriate decision. Such action should
be initiated by issuing EL27 in which the period specified for response
should normally be two months. This period may need to be reduced if the
application is approaching the end of the compliance period; if there is
less than 6 months remaining of the compliance period the reply period
should not be set at longer than 1 month. Appropriate timeframes should
be decided by the examiner if less than 3 months of the compliance
period remain. When issuing EL27, the case examiner should create a
"Diary" entry in the PD electronic diary for return of the file to them
on a specified date which should be 5 months from the date on which they
write the instructions. The diary entry should state the examining group
after the application number in the format GB0000000.0 -- EX00. A diary
action should also be added to the PDAX dossier. Instructions should be
given for the diary entry to be cancelled if a response is received
before the "bring forward" date. If amendments are unsatisfactory or the
applicant disputes the need for the search, or search fee, the matter
should be pursued by letter or telephone (confirmed in writing) with the
aim of swift resolution so that the normal examination process can
commence or continue without undue delay. If no response is received the
examiner should write or telephone (confirming in writing) to enquire if
it is the applicant's intention not to proceed, emphasising that if no
action is taken the application will be treated as refused at the end of
the compliance period. It should be made clear that genuine reasons for
the late-filing should accompany any subsequent response. The case
examiner should be named and a contact telephone number provided in all
letters to the applicant or their agent.\]

### 18.03.3 {#ref18-03-3}

s.17(6) and s.17(8) is also relevant

Where a further search is performed, at the request of the applicant,
for a second or subsequent invention which was claimed in the
application as filed and which was identified in the search report,
section 17(6) rather than section 17(8) applies and the procedure is
that set out in
[17.111](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-111),
etc. If, however, amendment to remove plurality of invention results in
the deletion of claims to an invention which was searched at the search
stage in favour of an invention which was present at that stage but
which was not so searched, a search fee under section 17(8) should be
required if a further search is then necessary at substantive
examination ([see 18.41](#ref18-41).

\[ Although no refund is given, an additional search fee is not required
for a second invention in a divisional application when a search report
has issued in respect of the first invention in the divisional
application, where this first invention was already searched in
connection with the parent application ([see
15.38](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-38)),
and then a further search becomes necessary for the second, previously
unsearched invention in the divisional application sometime later due to
the deletion of the first (and previously searched) invention.\]

### 18.03.4 {#section-5}

s17(8) is also relevant

If an applicant files another search fee in anticipation of the
amendments made necessitating a further search under section 17(8) and
the substantive examiner does not consider such a search to be
necessary, then a refund of the fee paid should be made, even if no
specific request therefor has been made by the applicant.

### 18.03.5 {#section-6}

If, following the issue of the letter referred to in
[18.03.2](#ref18-03-2), there is such an unresolved dispute and/or if
the fee is not paid or any other requirement of this provision is not
met, a hearing is offered. If the hearing officer decides that
examination should not proceed until the fee is paid or the application
amended, they should specify a period (normally two months) for payment
or appropriate amendment. If neither action is taken within the
specified period they may refuse the application.

\[ A hearing may be offered by Senior Examiners but Examiners should
consult their senior officer as to the appropriate decision. The hearing
should be taken by a Deputy Director [(see also
18.03.2)](#ref18-03-2).\]

### 18.03.6 {#section-7}

When the appropriate fee has been filed, together with a Form 9 or 9A,
the substantive examiner performs the supplementary search. A search
report is issued; this may accompany a s.18 examination report.

\[ If the search report is issued with a s.18 report, an ISR-SUPP bundle
with SE6 or SL7 should be used as appropriate; SL7 should otherwise be
used (in an ISR-SL7 bundle). Any newly-cited documents should be issued
at the same time ([see 17.104.1 to
17.105.2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-104-1)).\]

\[ The SL7 letter should be used where a supplementary search is carried
out at the same time as a substantive examination which finds no
objections. The SL7 is to be issued at the same time as the intention to
grant letter. The examiner should create a bundle including an SL7 and
associated checklist and sign this off. The examiner should then
complete a grant form ([see 18.86](#ref18-86)) noting the SL7 and send a
standard PDAX message to the EA with the text "ISSUE RELEVANT INTENTION
TO GRANT LETTER".The EA will then issue an intention to grant letter and
send a message to the ESO as set out in [18.86.2](#ref18-86-2).\]

### Section 18(2)

On a substantive examination of an application the examiner shall
investigate, to such extent as he considers necessary in view of any
examination carried out under section 15A above and search carried out
under section 17 above, whether the application complies with the
requirements of this Act and the rules and shall determine that question
and report his determination to the comptroller.

### 18.04 {#ref18-04}

After Form 10 has been filed but before sending the application to an
examining group for substantive examination, the formalities examiner
should examine any new pages for compliance with r.14 and Schedule 2.
Any objections should be reported in a form suitable for inclusion in
the letter which constitutes the examiner's report under s.18(3) ([see
18.47](#ref18-47)).

### 18.05 {#section-8}

\[Deleted\]

### 18.06 {#ref18-06}

The application should be forwarded to the examining group responsible
for the classification heading where the primary search had been
performed or if the application is destined for combined search and
examination, the file should be sent for allocation to the appropriate
group as indicated in
[17.03](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-03).
It may be transferred by mutual agreement to a different heading before
substantive examination if, for example, it had transpired during the
search that the other heading would be more suitable, or if amendment of
the specification has rendered transfer appropriate, or if
subject-matter has been transferred on reclassification. The substantive
examiner should aim to deal with cases allocated to them within 18
months of their s.16 publication date but they should normally examine
these cases in order of their priority dates, unless the application
qualifies for combined search and examination, in which case the search
and examination should be done so that the search report and any report
under s.18(3) can issue within six months of the date of filing of the
Form 9 or 9A (for divisional applications [see
15.41-15.42](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-41)).

\[When an application which qualifies for combined search and
examination is received in an examining group, it will take its normal
place amongst pending searches (unless accelerated search has been
requested -
[17.05.1-2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-05-1)).\]

\[Examiners in particular should be aware of the different procedures
which apply when processing combined search and examination cases.
Reference should be made to the examiners' aide-memoire for combined
search and examination cases, which is issued to all examiners and is
reproduced in [18.98](#ref18-98).\]

### 18.07 {#ref18-07}

A request for accelerated substantive examination of an application,
which the applicant should mark appropriately to ensure prompt action,
may be allowed provided that the request:

a\) gives adequate reasons why acceleration is needed ([see
17.05.1-2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-05-1)
or paragraph 6.2 of the [Patents Fast Grant
Guidance](https://www.gov.uk/government/publications/patents-fast-grant);
or

b\) meets the requirements for acceleration through the "Green Channel"
([see
17.05.1-2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-05-1)
or paragraph 6.2 of the [Patents Fast Grant
Guidance](https://www.gov.uk/government/publications/patents-fast-grant));
or

c\) meets the requirements for acceleration under the PCT(UK) Fast Track
([see
89B.17](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application#ref89B-07));
or

d\) meets the requirements for acceleration under the Patent Prosecution
Highway (PPH) ([see 18.07.1](#ref18-07-1)).

Requests for acceleration should be in writing, and may be made
electronically using the Office's [online patent filing
services](https://www.gov.uk/file-documents-pending-patent) (as a
covering letter). It is possible to exercise discretion to accept an
acceleration request by email; however this practice should not be
encouraged. If a request for acceleration is sent direct to an examiner
then they should forward it to the examiner assistants, unless it is an
acceleration request received after issuance of a first examination
report, or, an acceleration request made at the time of filing a
divisional application, in which case it is the responsibility of the
examiner (see square brackets below). If the request is made before
publication, the applicant should indicate whether or not publication
before the end of the 18 month period prescribed under s.16 is also
requested. If, prior to publication, an applicant makes a request for
accelerated substantive examination but does not indicate explicitly
that accelerated publication is required, then it will be assumed that
accelerated publication is not needed and formalities should make no
effort to contact the applicant regarding accelerated publication. If,
however, there is uncertainty over whether a request is for accelerated
substantive examination and/or accelerated publication (e.g. the
applicant simply requests ''accelerated processing'') then formalities
should contact the applicant to clarify their intention. Requests giving
no, or inadequate reasons should be refused.([See also
18.87](#ref18-87)).

\[ On receipt of a request for accelerated examination, Formalities
should add the Accelerated Examination label to the cover of the dossier
and send the message Acc Exam to the group ESO. A GREEN CHANNEL label
should also be added to the dossier cover where the acceleration request
is under the [Green
Channel](https://www.gov.uk/patents-accelerated-processing), and this
should be recorded on COPS using the function Record Green Channel
Patent Application (REC GRE).\]

\[ Accelerated examination requests are dealt with by examiner
assistants (see sixth square bracket of 18.07 onwards) except in the
following two circumstances where the acceleration request is the
responsibility of the examiner; i) the acceleration request is received
after issuance of a first examination report i.e. it is a request to
accelerate processing of amendments (see square bracket below for
detailed guidance), or, ii) the acceleration request is made at the time
of filing a divisional application (see fifth square bracket of 18.07
for detailed guidance). For guidance on the processing of accelerated
further searches, which is also the responsibility of patent examiners
and not examiner assistants, see the first square bracket of
[17.05.1](guidance/manual-of-patent-practice-mopp/section-17-search/ref17-105-1).\]

\[ If an acceleration request received after issuance of a first
examination report is accepted by an examiner, then this should be
communicated as follows:

a\) In the case where further objections are identified, the examiner
should insert clause ELC30 (deleting the second paragraph of the clause
as appropriate), into the examination letter accompanying the latest
examination report. Examiners should ensure that the clause appears
prominently i.e. on the first page of the letter to avoid being
overlooked;

b\) If the application has already been published and is found to be in
order at the next examination round, then a sentence acknowledging
acceptance of the acceleration request should be included in the EL34.
Examiners can arrange for this to happen by using the ''Additional text
for letter'' section of the Grant Form ([see 18.86](#ref18-66)). In the
case of CSEs, if the examiner has no further objections but 3 months
have not elapsed since A-publication then the examiner should issue an
EL32 in the normal way, appropriately edited to acknowledge acceptance
of the acceleration request;

c\) In the event that an applicant files a lone request (i.e. not
accompanying amendments) for accelerated examination after a first
examination report has issued, then the examiner should issue a modified
EL30 letter (edited as appropriate to delete the reference to
''substantive examination of your application will begin shortly'' and
to delete the 2nd paragraph of the letter).\]

\[ If an acceleration request is to be refused by the examiner, then
they should issue an EL29 letter (modified as appropriate to explain the
reason for refusal). Note that where an acceleration request is allowed,
the examiner should edit the PDAX message to indicate the case is
accelerated (i.e. use ''ACC'' in the PDAX message title) and ensure that
the ''Accelerated Examination'' marker on the cover of the PDAX dossier
is ticked.\]

\[ For accelerated examination requests made at the time of filing a
divisional application if the acceleration request is accepted by the
examiner then they should issue an EL30 letter (edited as appropriate to
suit the circumstances). If the acceleration request is refused by the
examiner, then they should issue an EL29 letter (edited as appropriate
to explain the reason for refusal).\]

\[ If the acceleration request is the responsibility of an examiner
assistant, then on receipt of an Accelerated Exam message the examiner
assistant should consider the request as a matter of urgency, and report
to the applicant/agent whether it is allowable. This should be done
either by letter or by telephone (followed up by a report). In the event
that the request is allowed, an indication should be given, with
reference to any existing Agency target, of when the examination report
can expect to be issued. If the application is in the publication cycle
an examination report can still issue -- see [18.64](#ref18-64) \]

\[ EL29, modified as appropriate, may be used to communicate a refusal
of accelerated examination. EL30 or SC19 (when issuing a search report),
modified as appropriate, can be used to inform the applicant that the
request for accelerated substantive examination has been allowed and
that substantive examination will take place immediately but that no
report under s.18(4) will normally issue until at least three months
after the application has been published under s.16. EL33 should then be
sent if the substantive examination has been carried out and reveals no
objection. When the substantive examiner has done the substantive
examination and identified objections, and if EL30 or SC19 has not
issued previously, ELC30 should be included in their report to confirm
allowance of the request. See [18.07.2](#ref18-07-2) for the situation
where an examination report is likely to be issued earlier than three
months after publication. In the rare event that the examination report
itself is issued immediately, then it will of course not be necessary to
issue a separate letter indicating allowance of the acceleration
request, however an indication should be included in the covering letter
to confirm that processing has been accelerated.

\[ At the point when the request is allowed or disallowed, the examiner
assistant should ensure that PAFS records the correct action type
against the case and that the appropriate PDAX message exists for the
case.

\[ Where an acceleration request is allowed, the PAFS action should be
correct, but the examiner assistant should always confirm what PAFS
actions are open and, if they are not correct, include in the checklist
accompanying the letter or report appropriate instructions to the ESO.
The examiner should ascertain whether there is a "normal" Exam message
in the appropriate heading team mailbox, and if there is close and
delete it. The examiner assistant should then send the message ACC
\[HEADING\] EXAM, setting the relevant date, to the heading mailbox. The
relevant date should be that of the date stamp on the applicant or
agent's letter requesting acceleration, the examination being due two
months from this date.

\[ Where an acceleration request is not allowed, the PAFS action will
need to be changed, and the ESO should be asked to do this in the
checklist accompanying the letter or report issued to the applicant or
agent. Typically the checklist should include 'Please change the PAFS
booking from Accelerated Examination to Examination with a significant
date equal to the earliest date of this application'. The EA will then
edit the PDAX message and return it to the mailbox as a "normal"
unaccelerated exam.\]

### 18.07.1 {#ref18-07-1}

A request for accelerated processing examination of an application under
the Patent Prosecution Highway (PPH) will be allowed providing the
relevant PPH requirements are met. These requirements are available
online from the [Patent Prosecution
Highway](https://www.gov.uk/government/publications/patent-prosecution-highway)
web pages. The examiner should consider the work of the Office of First
Filing/Examination but is not bound to follow the actions of that
Office. Any objections under UK patent law should be raised as normal.

\[On receipt of a request for accelerated processing under the PPH,
formalities should add the Accelerated Examination label and the PPH
label to the cover of the dossier. Once all normal Formalities actions
have been carried out, the ESO should send a PDAX message ('PPH
REQUEST') to the PPH mailbox instead of the relevant heading mailbox.

\[An examiner assistant will determine whether the request for PPH
processing is allowable and record this in a minute for the heading
examiner, consulting the PPH Administration Team where necessary. The
examiner assistant will inform the applicant of the allowability of the
PPH request. Once the examiner assistant has placed the minute on the
PDAX dossier and recorded the PPH request they will send a suitably
annotated PDAX message to the appropriate PDAX heading mailbox.

\[When starting work on an application which has been accelerated under
the PPH, the examiner should refer to the guidance provided in the
minute. If there is no minute but a PPH request has been made the PPH
Administration Team should be contacted immediately by emailing [email:
PPH@ipo.gov.uk](mailto:PPH@ipo.gov.uk).

### 18.07.2 {#ref18-07-2}

s.89A is also relevant

A report should issue confirming whether the request is allowable or not
([see 18.07](#ref18-07) and [18.07.1](#ref18-07-1)). If a request for
accelerated substantive examination is allowed, the report should state
that substantive examination will proceed as quickly as possible, but
that no report under s.18(4) will normally issue until at least three
months after the application has been published under s.16 so as to
allow time for third parties to file observations under s.21 and for
updating the search. see
[18.85](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent#ref18-85) -
2nd square bracket. However, this so-called "3-month rule" does not
necessarily apply to divisional applications where the invention claimed
was clearly claimed in the published parent application, such that
parties have already had at least three months to file observations
under s.21 in respect of that invention. In this situation, if the only
outstanding matter preventing grant of the divisional is the three-month
period for s.21 observations, then the examiner should waive this
three-month period (see square brackets below 18.07.2) and send the
divisional application to grant. If the invention claimed in the
divisional application was clearly claimed in the published parent, but
the parent has not been published for at least three months, then the
examiner should delay sending the divisional application to grant until
the parent has been published for at least three months. Regardless of
whether the invention of the divisional was clearly claimed in the
published parent application or not, the issuing of an intention grant
letter cannot be delayed beyond the end of the compliance period merely
to allow for the expiry of the three-month period. In the situation
where less than three months is to elapse between publication of an
application and the compliance date, the examiner should delay the issue
of an intention to grant letter to allow as much time as possible for
s.21 observations to be filed, without delaying beyond the end of the
compliance period. This situation most commonly arises when divisional
applications are filed very close to the end of the compliance period.
Note that the only time where grant will be delayed beyond the
compliance date is where s.16 publication is due to take place after the
compliance date. For further information on this and other aspects of
handling divisional applications including which report/letter examiners
should issue under which circumstance, see the square brackets below
[15.41](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-41).
In the case of s.89 applications, if the corresponding international
application has been published by WIPO ([see
89B.04](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application#ref89B-04)),
then a report under s.18(4) may not be issued until at least 2 months
after the application has been republished so as to allow adequate time
for third parties to file observations under s.21 after the application
has entered the national phase ([National Starch and Chemical Investment
Corporation's Application BL
O/34/96](https://www.gov.uk/government/publications/patent-decision-o03496)).
Where the s.89 application has entered the national phase early and
accelerated publication under s.16 has occurred ([see
89A.20.1](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-20-1)),
the usual "3-month rule" (from the date published by the IPO) applies.
For further details on accelerated examination of s.89 applications,
[see
89A.21](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-21).
For accelerated processing under the [PCT(UK) Fast
Track](https://www.gov.uk/guidance/patents-accelerated-processing#patent-cooperation-treaty-pct-uk-fast-track),
[see
89B.17](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application#ref89B-17).
Where an acceleration request is accepted on a PCT application, the
substantive examiner (rather than the examiner assistant) is required to
carry out reclassification procedures (see
[89A.14](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-14)
and
[89A.20.1](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-20-1)).\
\
\[ Where the 3-month rule has been waived (or reduced) in relation to a
divisional application, the examiner should clearly record this on the
grant form for the benefit of the EA, or alternatively a minute should
be put on file.\]\
\
\[ Where there is insufficient time remaining to allow the full 3-month
delay after publication for s.21 observations, and so a shorter delay is
used, a diary entry should be created for return of the file before the
end of the compliance period and, if the delay is more than one month, a
modified SE3 or EL32 if a s.18 report has previously been issued, should
be issued. The SE3/EL32 should be modified as required, for example: to
note that the delay will be less than three months after publication and
(if appropriate) to remove any reference to updating the search. \]

### 18.07.3 {#ref18-07-3}

Any request for confidentiality relating to accelerated prosecution, for
instance where such prosecution has been requested because of alleged
infringement, should be considered in the light of the general guidance
given in
[118.13](/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-13),
but the request itself and the general reason for it should be on the
open file.

### 18.08 {#ref18-08}

r.31(5)(a) is also relevant

Substantive examination should be carried out using the main copy of the
specification. When substantive examination occurs after s.16
publication, any amendments filed before substantive examination should
be incorporated into the specification, and the examination be conducted
on the basis of the amended specification (cf
[18.63](#ref18-63)-[18.70](#ref18-70)). The amendments should be
incorporated into the description, claims and/or drawings as
appropriate, and each of these new documents annotated as "working
copy". For applications undergoing combined search and examination
before s.16 publication, the examiner doing the search and examination
should consider whether any amendments filed before issue of the search
report may be allowed. If they are allowable the search and examination
should proceed on the basis of the application as amended
([17.35](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-35),
[19.19.1](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-19-1)).
Amended or new claims should be included in the published application as
normal
([16.16-16.19](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-16)).
Applications where the original claims were filed after the filing date
should be treated on combined search and examination as outlined in
[17.34](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-34).
If formal drawings were filed later than the application date they may
nevertheless be used for the examination, but if a detail of a drawing
becomes a point of issue the informal drawings should be consulted since
any discrepancies of detail may not have been detected in the
preliminary examination ([see
15A.22](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-22)).

\[The substantive examiner should, at each action, scan all
correspondence from applicants or agents to ensure that all matters
raised have been dealt with bearing in mind the substantive examiner's
overall responsibility for the application. If, when proposed new claims
are filed, it is not clear which previous claims (if any) have been
superseded the agent (or applicant) may be telephoned to ascertain the
position before examination is made. \[Amendments allowed before the
issue of the search report should be acknowledged at the time of
combined search and examination using the appropriate version of SC1.

\[The examiner should also pay particular attention to documents sent to
Index and Scanning for importing onto the open part of a dossier.
Although the substantive examiner generally has to identify and explain
their objections without reference to any annotations they may have
made, they may where they find it difficult to do otherwise, explain
their objections more fully by annotating a photocopy of selected pages
from the specification which they can attach to the report which is to
be issued, though this practice should be regarded as exceptional. A
second photocopy, similarly annotated, must be associated with the file
copy of the report. The substantive examiner should send the annotated
photocopied page with a completed proforma to Index and Scanning who
will import the page into the dossier and send the requested message to
the examiner.\]

### 18.09 {#section-9}

s.1(1), s.14(3) and s.14(5) are also relevant

In particular the substantive examiner must establish whether the
application relates to a patentable invention (see ss.1-4), whether the
invention is disclosed in a manner which is clear and complete enough to
enable it to be performed ([see
14.58­](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-58)
[14.90](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-90)),
and whether the claims define the invention and are clear ([see 14.108
to
14.139](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-108)),
are concise ([see 14.140 to
14.141](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-140)),
are supported by the description ([see 14.142 to
14.156](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-142))
and relate to a single invention or inventively-linked group of
inventions ([see 14.157 to
14.168](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-157)).
The substantive examiner should therefore study the specification to
whatever extent is necessary to see whether these requirements have been
met.

### 18.09.1 {#ref18-09-1}

In [Macrossan's Patent Application \[2006\] EWHC 705
(Ch)](http://www.bailii.org/cgi-bin/markup.cgi?doc=/ew/cases/EWHC/Patents/2006/705.html&query=Macrossan%20s&method=boolean){rel="external"},
Mann J held that each patent application must be measured against the
requirements of the law and on the basis of its own facts; any
particular application stands and falls on its own merits. Earlier
patents can be used to gain some assistance on a particular case, but to
rely on previous patents as some sort of benchmark would be to give the
earlier patent in time a primacy which the legislation does not warrant.
In deciding whether or not an application complies with the requirements
of the Act and Rules, the facts of the case should be decided on the
balance of the evidence available.

### 18.09.2 {#ref18-09-2}

As [Mann J indicated in Macrossan's Patent Application \[2006\] EWHC 705
(Ch)](http://www.bailii.org/cgi-bin/markup.cgi?doc=/ew/cases/EWHC/Patents/2006/705.html&query=Macrossan%20s&method=boolean){rel="external"},
any doubt should be resolved in the applicant's favour only if the doubt
is substantial. This could arise if the examiner's assertions as to the
common general knowledge have been challenged and expert evidence would
be needed to establish the position, or if the date of a prior
disclosure has been challenged and the examiner does not have access to
material that would confirm the date. Certainly the examiner is not
required to meet the criminal burden-of-proof standard in raising and
pursuing an objection. If the examiner is minded to maintain an
objection, but there may be substantial doubt on an issue of fact which
would determine patentability, the test applied in [Blacklight Power
Inc. v The Comptroller-General of Patents \[2009\] RPC
6](http://rpc.oxfordjournals.org/content/126/3/173.abstract?sid=18b4798f-f885-458c-a002-a39406a1d03f){rel="external"}
should be used ([see
4.05.1](/guidance/manual-of-patent-practice-mopp/section-4-industrial-application/#ref4-05-1)).
In this case, the judge held that the examiner should consider whether
the evidence provided by the applicant gives rise to a reasonable
prospect that, if the issue were to be fully investigated at trial with
the benefit of expert evidence, it would be resolved in the applicant's
favour. This case concerned industrial applicability and sufficiency
where the claimed invention relied on a scientific theory of doubtful
validity. However, the test could also apply to other questions of doubt
which would be addressed in court by the use of expert evidence. This
could include the determination of inventive step where a prima facie
objection of obviousness is contested on technical grounds ([see
3.67-3.69](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-67)),
and the determination of the publication date of an internet citation
(see 18.09.3-18.09.5 below).

### 18.09.3 {#section-10}

When assessing the relevance of an internet disclosure at the
substantive examination stage, a document should be cited at first
instance unless the examiner is certain that it falls outside the state
of the art. If the applicant contests the publication date the examiner
should decide the matter on the balance of probabilities based on the
evidence available. Such evidence may include a publication date
provided on the webpage itself, the cached date from a search engine
(which provides a date by which the webpage must have been published),
and the "WayBackMachine" on archive.org. Evidence from sources such as
archive.org, while not conclusive, may provide justification for an
examiner's view that there is little doubt as to the date of disclosure
([see also
17.54](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-54)).

### 18.09.4 {#ref18-09-4}

The standard of proof required for assessing the publication date for
internet citations has been considered both in EPO Board of Appeal
decisions and in Office decisions. Although the Board of Appeal in T
1134/06 Konami Corp. decided that for an internet disclosure to be cited
as prior art, its date of publication would need to be proved "beyond
any reasonable doubt", in [HSBC France's Application BL
O/180/09](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F180%2F09&submit=Go+%BB){rel="external"},
the hearing officer held that the date of an internet disclosure should
be decided on the balance of probabilities. This followed from previous
UK case law on prior use (e.g. Kavanagh Balloons Pty Ltd v Cameron
Balloons Ltd \[2004\] RPC 5 -- [see
2.29.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-29-1)):

> As the EPO's approach to prior use forms the basis of the Technical
> Board of Appeal's decision in T 1134/06, I do not consider that I am
> bound to follow the EPO's reasoning in this case. Rather it seems to
> me more appropriate, in line with UK cases in this general area such
> as Kavanagh Balloons, that the date and contents of internet
> disclosures be assessed on the balance of probabilities, just as it is
> for cases of alleged prior use or, for that matter, dating any other
> categories of prior disclosure.

### 18.09.5 {#ref18-09-5}

In [Ranger Services Application BL
O/362/09](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F362%2F09&submit=Go+%BB){rel="external"}
it was held that a publication date established on archive.org was
sufficient to demonstrate, on the balance of probabilities, a date of
publication before the priority date of the application. In addition,
EPO guidance issued in 2009 [(Official Journal EPO 8/9
2009)](http://www.epo.org/patents/law/legal-texts/journal/2009/08-09.html){rel="external"}
states:

> ...the balance of probabilities will be used as the standard of proof,
> as generally applied by the boards of appeal. According to this
> standard, it is not sufficient that the alleged fact (e.g. the
> publication date) is merely probable; the examining division must be
> convinced that it is correct. It does mean, however, that proof beyond
> reasonable doubt ("up to the hilt") of the alleged fact is not
> required.

It further notes that the fact that archive.org is not a complete
archive does not detract from its credibility and the legal disclaimers
contained in the site should not be taken to reflect negatively on the
accuracy of the service. This guidance reduces the burden of proof
required by the EPO and brings its practice more into line with that of
the Office.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Considering the search report]{#default-id-6d9c2c45-heading-9 .govuk-accordion__section-button} {#considering-the-search-report .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Considering the search report\",\"index_section\":9,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-9 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-9" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Considering the search report\",\"index_section\":9,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.10 {#ref18-10}

r.113(5)-(6) is also relevant

The substantive examiner should then study the search report, including
any document noted but not cited. Although any views expressed by the
search examiner, either concerned with novelty or obviousness or with
any other subject, should be carefully considered, the substantive
examiner is not bound by these views and must make up their own mind as
to what objections, if any, should be raised. While the search examiner
will have resolved any doubt about the relevance of a document in favour
of citing it, the substantive examiner should only cite if the document
supports at least a prima facie objection. On the other hand the
examiner may decide to cite a document noted by the search examiner but
not included in the search report under s.17(5), for example where the
documents cited were merely examples selected from a large number found,
or any other document which has come to their notice ([but see
18.11](#ref18-11)). Where the examiner has become aware of an apparently
relevant document in a foreign language, they should check whether there
is an English language equivalent. If not, the substantive examiner
should use their judgement regarding the necessity for a text in
English, making use of what they can glean from the foreign language
document including any drawings and an English language abstract if
available. The applicant can be asked informally to provide a copy of
the text of the document in English if they have it, but a formal
request for a translation should not be made except possibly in the case
of a document cited in an international search report, international
preliminary report on patentabilty or international preliminary
examination report (see
[89B.11](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-11)
and rule 113(5)-(6)). Conversely the examiner may exceptionally provide
a copy of a translation of a foreign language document to the applicant
but is not obliged to do so (as per
[17.104.1](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-104-1)).

\[If the substantive examiner decides not to cite a category X and/or Y
document which had been included in the search report, a minute should
be added to the PDAX dossier to record their decision. Such a minute is
unnecessary in respect of A category documents included in the search
report.

\[The substantive examiner should ensure that any newly-cited documents
are issued as appropriate ([see
17.104.1](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-104-1),
[17.105.1](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-105-1)).

\[If a substantive examiner requires a translation of a foreign language
document which they have been unable to obtain by other means,
consideration may be given to procuring one. This may be arranged by an
Examination Support Officer. This should rarely be necessary and if it
is it should be possible to identify key passages for translation
instead of the whole document.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Family matching, checking equivalents, and online file inspection]{#default-id-6d9c2c45-heading-10 .govuk-accordion__section-button} {#family-matching-checking-equivalents-and-online-file-inspection .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Family matching, checking equivalents, and online file inspection\",\"index_section\":10,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-10 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-10" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Family matching, checking equivalents, and online file inspection\",\"index_section\":10,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.10.1 {#ref18-10-1}

There are often reports and citations associated with equivalent
applications. At any stage, when equivalents are found, examiners should
use their judgement to decide which of these reports and citations to
consider. Online file inspection can be used to find these reports and
may provide further details of the significance of a document cited
against an equivalent ([see 18.10.4](#ref18-10-4)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [First substantive examination]{#default-id-6d9c2c45-heading-11 .govuk-accordion__section-button} {#first-substantive-examination .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"First substantive examination\",\"index_section\":11,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-11 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-11" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"First substantive examination\",\"index_section\":11,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.10.2 {#section-11}

When first examining a UK application or PCT application, the
substantive examiner should identify any equivalent applications and
list these on the internal search report ([as per
17.115](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-115)).
The claims of EP, WO and GB equivalents should be considered in order to
assess whether conflict arises ([see
17.115](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-115))
and ([18.91-18.97.1](#ref18-91)). When there is an EP or WO equivalent,
the examiner should also complete a search comparison form.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Subsequent examinations, and before grant]{#default-id-6d9c2c45-heading-12 .govuk-accordion__section-button} {#subsequent-examinations-and-before-grant .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Subsequent examinations, and before grant\",\"index_section\":12,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-12 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-12" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Subsequent examinations, and before grant\",\"index_section\":12,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.10.3 {#ref18-10-3}

The composition of a patent family may change over time. Therefore
examiners should check if any further equivalents have come to light
since the check made at first substantive examination. As a minimum,
this check should be made just before issuing an intention to grant
letter, but may also be useful when issuing a second (or later)
examination report. The examiner should also consider checking the
online file, and legal status (eg whether granted or not), of any
equivalent applications ([see 18.10.4](#ref18-10-4)). At this stage it
is possible that new documents have been cited against an equivalent, or
an equivalent may have been granted with narrower claims. In addition,
the first examination report for an equivalent may have become
available, if it was not previously. It is often useful to carry out a
final check, before grant, of the legal status and online file of any EP
and US equivalents.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Online file inspection]{#default-id-6d9c2c45-heading-13 .govuk-accordion__section-button} {#online-file-inspection .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Online file inspection\",\"index_section\":13,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-13 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-13" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Online file inspection\",\"index_section\":13,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.10.4 {#ref18-10-4}

Reports and citations associated with equivalents, and the legal status
of equivalents, can often be found using online file inspection.\
\
\[ For EP equivalent cases, the [European Patent
Register](https://register.epo.org/regviewer){rel="external"} online
file inspection facility may provide further details of the significance
of a document cited against the equivalent. For US equivalents, the
[USPTO's PAIR](http://portal.uspto.gov/pair/PublicPair){rel="external"}
online file inspection facility may be used. Other offices are
increasingly introducing online file inspection and online machine
translation. Where reports on AU and CA equivalents are considered by
the examiner the applicant should be informed that work from either the
Australian or Canadian office has been considered. When checking an
equivalent granted US patent, the substantive examiner should also
consider whether the granted claims have been significantly restricted
or clarified in comparison to the claims of the application in suit.
Where it appears that the granted claims, if made to the application in
suit, would distinguish the invention from the prior art and be prima
facie allowable, RC 13 should be used to mention this to the applicant.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Uncovering new citations after search]{#default-id-6d9c2c45-heading-14 .govuk-accordion__section-button} {#uncovering-new-citations-after-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Uncovering new citations after search\",\"index_section\":14,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-14 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-14" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Uncovering new citations after search\",\"index_section\":14,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.11 {#ref18-11}

Ideally an applicant should not be confronted during substantive
examination with a document which could have been included in the search
report but was not. Nevertheless the overriding consideration is the
validity of an eventual patent, and if such a document demonstrates that
the invention claimed is not novel or is obvious it must be cited.

\[Whenever a new document is cited, even if only referred to as an
example of common general knowledge, it should be included in an
internal search report.\]

\[ Whenever a document which could and should have been cited earlier is
cited for the first time in a s.18 report, an apology should be made on
behalf of the Office. In general no reason for the oversight should be
offered, though if the document was cited against a corresponding
application or patent of the same patent family or if it has come to
light as a result of reclassification this should be mentioned. In no
circumstances should the blame for the mistake be attributed. On the
other hand, when citing for the first time a document which was not
previously relevant but has become so as a result of amendment of the
specification, this fact may be pointed out. If this citation is made as
a result of a partial search, and further searching is likely to be
necessary, this should also be mentioned. Furthermore, if a citable
document is found as a result of updating search to comply with s.2(3)
then this should be mentioned in the s.18 report.

\[ If the substantive examiner discovers such a citable document and
considers it so relevant that the applicant may not proceed with the
application, there being nothing to indicate that the applicant must
already be aware of the document, they may defer issue of the s.18
report and instead cite the document using SL5, adding the relevant
information regarding the document ie category and relevant
passages/claims. The optional last paragraph of SL5 should be included
if appropriate. This gives the applicant two months in which to withdraw
the application before issue of the report. For all cases, the examiner
may wish to set a diary reminder in the PD electronic diary for two
months after the issue of SL5. This entry should state the examining
group after the application number in the format GB0000000.0 -- EX00. A
diary action should also be added to the PDAX dossier.

\[ The applicant should be provided with a copy of any non-patent
literature (NPL), English language abstracts of foreign language patent
documents and, where these have been requested by ticking the box on
Form 9A, any patent documents cited after issue of the search report
([see
17.104.1](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-104-1),
[17.105.2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-105-2)).
Furthermore, a copy of the source document (where appropriate) should be
similarly provided for the applicant whenever it is substituted for an
abstract at the substantive examination stage.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Further search]{#default-id-6d9c2c45-heading-15 .govuk-accordion__section-button} {#further-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Further search\",\"index_section\":15,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-15 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-15" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Further search\",\"index_section\":15,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
([see also 18.03.1 to 18.03.6](#ref18-03-1-18-03-6) and
[18.41](#ref18-41))

### 18.12 {#ref18-12}

Normally no further searching should be necessary, beyond updating the
original search ([17.115 to
17.117](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-115);
for s.89 cases, see [89B.12 to
89B.12.3](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-12)).
The search examiner will have set out a statement of the critical matter
and indicated in which databases and classification areas the search has
been carried out. Even if the substantive examiner is surprised by the
fact that few (or no) documents have been listed, they should assume (in
the absence of any indication to the contrary) that the report of the
search includes all anticipations of the critical matter present in the
areas searched, although if they know of a particular relevant document
omitted from the report they may cite it. Nor should the search be
extended to other databases or classification areas unless there is very
good reason for doing so. On the other hand the substantive examiner
will have studied the specification more closely than the search
examiner and if it transpires that the statement of critical matter was
inadequate, or if any of the circumstances referred to in paragraph
[17.117](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-117)
applies, then a further search may be necessary. This could also be the
case if classification scheme revision has created a highly relevant
place, not available at the search stage and likely to contain documents
not covered by the original search, to search for the particular
critical matter. If classification symbols have been applied to the
family by the European Patent Office or other offices, it will normally
be desirable to ensure that the field of search includes at least those
symbols which apparently relate to the critical subject matter (as
opposed to supplementary material which might also have been
classified). The FAMI+REFI tool (or the dedicated Fami/Refi CLASS
function) will identify all CPC symbols that have been applied to the
family by the EPO/USPTO and/or other offices, providing the 'Show EP
confirmed classes only' box is unchecked. When the scope of the search
has been extended beyond that indicated in the original search report(s)
the applicant should be informed, with an indication of the new field(s)
searched.

\[ Any additional fields of search or citations arising from "top-up" or
further searches should be recorded on a top-up internal search report
(ISR-TOP) in PROSE; with any newly-cited documents being issued (as
appropriate) by the Examination Support Officer together with the
examination report ([see
17.104.1](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-104-1),
[17.105.2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-105-2)).
For CSE's, in the event that further searching is conducted before the
case enters the A-publication cycle, examiners should ensure that an
external search report (ESR) is manually added to the ISR-TOP bundle,
with the ERC2P checklist edited to instruct the Examination Support
Officer to issue the ESR and import it into PDAX. This ESR will be
included as part of the published A-specification (see also
[17.117](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-117)).

\[ Unless action has been taken under ss17(8) and 18(1A), a further Form
9 (or Form 9A [see
17.02](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-02))
and search fee can only be accepted if the search examiner has reported
that the application relates to two or more inventions, and then only in
respect of claims present in the application at the time of the main
search. Following a search report on the first of two inventions
identified by the search examiner on an application, the applicant may
cancel the searched claims and file a Form 10 together with a further
Form 9 or Form 9A in respect of the remaining claims. In such a case the
fee paid on the further Form 9 or Form 9A is not refunded; however, the
further search can be carried out in advance of substantive examination.
If, in such a case, Form 9 or Form 9A is not filed, it may be requested
under ss.17(8) and 18(1A) at the substantive examination stage [see
18.03.3](#ref18-03-3). When a search report has been issued in respect
of the first invention in a divisional application, despite a report in
respect of this invention having already issued in connection with the
parent application - see [18.03.3](#ref18-03-3).\]

### 18.13 {#section-12}

In situations such as where documents have been cited at the search
stage and no amendment has been made or the scope of the claims remains
unclear, the substantive examiner should consider whether deferral of
any necessary further searching would be the most efficient course of
action. If any necessary further search is deferred, the applicant
should be informed.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Priority dates: Section 2(3)]{#default-id-6d9c2c45-heading-16 .govuk-accordion__section-button} {#priority-dates-section-23 .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Priority dates: Section 2(3)\",\"index_section\":16,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-16 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-16" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Priority dates: Section 2(3)\",\"index_section\":16,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.14 {#ref18-14}

As a result of the practice referred to in paragraph
[17.74](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-74)
the search report may include documents which may or may not in fact
form part of the state of the art, depending on what is the priority
date of the invention under examination and/or of the potentially
citable matter. Further such documents may be found in the course of
updating the search. In addition, if the applicant files a late
declaration of priority ([see
5.26.1](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-26-1))
after search, the search report may resultingly include documents which
no longer form part of the state of the art ([see
17.82](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-82)).
The examiner should therefore consider whether the documents in the
search report still form part of the state of the art. A document cannot
be disregarded as a citation simply because the application in suit
claims priority from it ([see
5.25.2](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-25)
and
[5.26](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-26)
and
[15.22](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-22)).
At examination the practice set out in paragraphs [18.15 to
18.20](#ref18-15) should be followed when a potential citation was
published on or after the declared priority date or filing date of the
application being examined.

\[Examiners should be aware that there is no legal requirement for the
earliest declared priority date to support the claimed invention. Lack
of such support is not grounds on which to raise an objection. The
validity of the support provided by the declared priority documents only
becomes relevant when it impacts potential citations against novelty or
inventive step, as discussed in [15.15 to
15.20](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-15).
See also
[5.15](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-15).\]

### 18.15 {#ref18-15}

If the potential citation is a UK patent application, or a European or
international application designating the UK, and demonstrates lack of
novelty, a prima facie determination of the priority date of the
invention under examination ([see
5.20-5.25.2](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-20))
should be performed if possible. Normally only a cursory examination of
any priority documents will be necessary. If any priority documents (or
verified translations or declarations - [see
5.11](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-11))
have not yet been filed then the matter should provisionally be resolved
against the applicant. Similarly, if there is any other doubt about the
matter, either because it can be argued that the invention is not
supported by the disclosure in the priority document, or because the
priority document contains a reference which suggests that it might not
be the earliest relevant application (for example if it is a US
continuation-in-part or if it contains a reference to an earlier
application), then the matter should provisionally be resolved against
the applicant. If a prima facie priority date cannot be determined or if
the prima facie priority date is later than the earliest declared
priority date (or, where there is none, the filing date) of the
potential citation, then the latter should be formally cited. No attempt
should be made at this stage to establish the priority date of the
invention in the citation. If the dates are such that there is a
possibility that the applicant may be able to show that the priority
date of their invention is not later than that of the cited matter by
establishing or arguing for their own declared date and/or by attacking
that of the citation, they should be invited to do so; if they do, then
the substantive examiner will have to determine any priority date in
question. If the applicant amends to remove the objection of lack of
novelty, and the citation could be used to demonstrate the absence of an
inventive step, the matter should then be considered as in paragraph
[18.16](#ref18-16).

Because of the provisions of s.78(5A) an application for a European
patent (UK) remains citable under s.2(3) even if it, or the designation
of the UK in it, is withdrawn or deemed to be withdrawn. Under the EPC
all European patent applications filed on or after 13 December 2007
designate all Contracting States (including the UK) automatically at the
date of filing. Every European patent application will therefore become
a European patent (UK) application. Section 78(5A) further provides that
removal of the UK designation before publication of the European patent
(UK) application does not prevent the matter contained in the European
patent (UK) application becoming part of the state of the art by virtue
of section 2(3). Every European patent application filed after 13
December 2007 will therefore enter the state of the art by virtue of
section 2(3) once published. For European patent applications filed
prior to 13 December 2007, removal of the UK designation before
publication will continue to prevent the matter contained in those
applications from forming part of the state of the art by virtue of
section 2(3).

r.9(4) is also relevant

\[RC1 should be used for formally making the citation [see
18.47](#ref18-47). If the dates are such that the applicant may be able
to argue that the priority date of their invention is earlier than that
of the cited matter, RC1A should be added. If there is a possibility
that the applicant will be able to show that the priority date of their
invention is earlier than that of the cited matter by filing a
translation of their priority document (or an equivalent declaration -
[see
5.11](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-11)
then RC1C should be added instead.\]

### 18.16 {#ref18-16}

If the potential novelty citation is a document other than a UK patent
application or a European or international application designating the
UK then it should be formally cited provided that its publication date
is earlier than the prima facie priority date (determined as in 18.15
above) of the invention under examination. Similarly, if the citation is
a UK patent application or a European or international application
designating the UK, but is to be used to argue lack of inventive step
but not lack of novelty, then it should also be formally cited provided
that its publication date is earlier than the prima facie priority date
(determined as in 18.15 above) of the invention under examination. If
the applicant in their response asserts that the priority date of their
invention is in fact not later than the publication date of the citation
the substantive examiner will have to determine the question.

r.9(4) is also relevant

\[Formal citation should be made using RC1 or RC3 as appropriate (see
18.47), and the appropriate one of RC1B or RC3A should be added. If
there is a possibility that the applicant will be able to show that the
priority date of their invention is earlier than the publication date of
the cited matter by filing a translation of their priority document (or
an equivalent declaration - [see
5.11](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-11)
then RC1D or RC3B should be added instead.\]

### 18.17 {#section-13}

r.8(1) and (2) 18.17 are also relevant

If, in order to determine the priority date of the invention under
examination, the substantive examiner needs to consult a document
referred to in the priority document, they should ask the applicant to
provide a copy, with a translation if necessary. If it transpires that
the document in question is in the office on the file of another
application, the examiner should consult that file. If the priority
application number and document itself if required ([see 5.08 to
5.10](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-08))
has not been provided and there is still time to do so, confirmation of
the priority date must be deferred until the document is provided.

### 18.18 {#ref18-18}

If it becomes necessary to consult the priority documents of a cited
European (UK) application and, exceptionally, it proves impossible to
obtain a copy before the end of the compliance period, the applicant
should be informed that the examiner does not have immediate access to
the priority documents and may have to resume action after grant.
Similarly, the applicant should be informed of possible action after
grant if a translation of a priority document or declaration needed to
establish the priority date of a cited EP(UK) application has not yet
been filed ([see
73.02](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-02)).

\[Copies of priority documents (other than UK applications) of cited EP
(UK) applications and of translations, if any, of the priority documents
into one of the three official languages of the EPO can be obtained from
the [European Patent
Register](https://register.epo.org/espacenet/regviewer){rel="external"}
online file inspection service (queries about the [European Patent
Register](https://register.epo.org/espacenet/regviewer){rel="external"}
should be directed to CIMS (Document Supply) on ext 4329). When the
priority date of the EP (UK) application cannot be established in time,
and the applicant is so informed at the same time as citation is made,
RC22 should be used (or RC22A if there are other objections
outstanding); if citation has already been made, EL22 should be used.
RC22A should also be used when there should be plenty of time left for
obtaining copies of the priority documents from the EPO.\]

### 18.19 {#section-14}

When it appears to the substantive examiner that the application under
examination should be cited against the potential citation, they should
take steps to ensure that the matter is taken into account by the
relevant examiner.

### 18.20 {#ref18-20}

ss.89(1) and 89B(2) r.66(1), (2) are also relevant

An international application can form part of the state of the art by
virtue of s.2(3) only if it has fulfilled the conditions for entry into
the national phase (see
[2.30](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-30)
and
[89B.04](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-04))
or if s.79(2) is satisfied in the case of an international application
treated under the EPC as an application for a European patent (UK) ([see
79.03](/guidance/manual-of-patent-practice-mopp/section-79-operation-of-s-78-in-relation-to-certain-european-patent-applications/#ref79-03)).
If at the time the potential citation is under consideration it is not
possible to establish whether these conditions have been met, the
potential objection should nonetheless be brought to the applicant's
attention. However, no formal objection can be raised until it has
entered the national or regional phase designating GB. In the event that
no objections remain save that a PCT application would anticipate one or
more claims but that it has not yet entered the national or regional
phase, the application may be sent to grant and action taken under
section 73(1) if and when it does so.

\[ The [European Patent
Register](https://register.epo.org/espacenet/regviewer){rel="external"}
or [WIPO
Patentscope](https://patentscope.wipo.int/search/en/search.jsf){rel="external"}
can be used to determine whether an international application has
entered the regional phase. The latest date by which an international
application would normally be recorded on the [European Patent
Register](https://register.epo.org/espacenet/regviewer){rel="external"}
as having entered the national or regional phase is 40 months after the
priority date or, if none, the filing date.\]

\[COPS can be used to determine whether an international application has
entered the national phase. The necessary information can be accessed
using the DIS EQU function and entering the WO number. The resulting
display of a UK number indicates that the international application has
entered the national phase. The [European Patent
Register](https://register.epo.org/espacenet/regviewer){rel="external"}
can similarly be used to determine whether an international application
has entered the regional phase. The latest dates by which an
international application would normally be recorded on the relevant
database as having entered the national or regional phase are as
follows:

COPS -- UK designated: 34 months after the priority date or, if none,
the filing date

European Patent Register -- EP(UK) designated: 40 months after the
priority date or, if none, the filing date.

\[ If the international application has not entered the national or
regional phase at the time of writing the examination report, RC43
should be used to warn the applicant of the potential citation. The
PROSE system will then generate a CAV2C caveat form to monitor the
progress of the potential citation. At the appropriate time, the
examiner will be sent the completed CAV2C form, which will indicate
either that the international application is now citable under s.2(3)
(having entered the national or regional phase) or that it is not
citable (having never entered either the national or regional phase).
The examiner can then take the appropriate action, which may be after
the grant of the patent - [see
73.02-73.04](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-02).
Where the application is a PDAX dossier the examiner should record the
details as an action. The Examination Support Officer will print out the
caveat form and send it to the Caveat section. When the caveat form is
returned to the examiner, the outcome should be recorded as a further
action in PDAX. If further details are required they can be added to a
minute and/or a copy of the actual caveat form and the form sent to
Index and Scanning with a request to import the caveat into the
dossier.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Common general knowledge; non-documentary disclosure]{#default-id-6d9c2c45-heading-17 .govuk-accordion__section-button} {#common-general-knowledge-non-documentary-disclosure .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Common general knowledge; non-documentary disclosure\",\"index_section\":17,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-17 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-17" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Common general knowledge; non-documentary disclosure\",\"index_section\":17,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.21 {#section-15}

The search report is concerned solely with documentary evidence, though
in some cases the substantive examiner may be able to argue (at least in
the first instance) that an invention claimed is not novel or is obvious
on the basis of common general knowledge which is sufficiently
well-known not to need documentary support, either on its own or in
combination with any documents cited ([see
3.45](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-45)
and
[3.62](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-62)).

### 18.22 {#ref18-22}

Common general knowledge can in some cases be regarded as implicit in
the disclosure of a cited document, so that while certain essentials of
a claim being considered may not be explicitly disclosed in the
citation, it may be possible to argue that an earlier invention would,
as a matter of normal practice, be performed in a way falling within the
scope of the claim under consideration. This would have the effect that
the prior document may then be cited on the grounds of lack of novelty
rather than in support of an obviousness objection (though it may be
advisable to explain the reasoning behind such an objection). It also
renders citable such a document which forms part of the state of the art
by virtue of s.2(3).

### 18.23 {#ref18-23}

If it is found that an admitted display of the invention not within
s.2(4)(c) has occurred or that in respect of a display falling within
s.2(4)(c), no written evidence under r.5(4) and (5) has been filed
within the prescribed period ([see
2.40](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-40)),
and the matter displayed thus appears to fall within the state of the
art as defined in s.2(2), objection should be raised under s.1(1)(a) or
(b) as appropriate.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Prior use]{#default-id-6d9c2c45-heading-18 .govuk-accordion__section-button} {#prior-use .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Prior use\",\"index_section\":18,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-18 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-18" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Prior use\",\"index_section\":18,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.24 {#ref18-24}

If the substantive examiner is aware, either from personal experience or
from information from another examiner ([see
17.54](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-54)
and
[17.91](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-91))
or from a member of the public ([see
21.18-21.19](/guidance/manual-of-patent-practice-mopp/section-21-observations-by-third-party-on-patentability/#ref21-18)),
of an instance of prior use which is apparently destructive of the
novelty or inventive step of the invention claimed, all the relevant
facts should be communicated to the applicant, provided sufficient
circumstantial details of the prior use are available to enable the
applicant to make their own independent enquiries. For example it might
be reported that an article having particular features was produced by a
named manufacturer, under a particular name or identifying reference, or
that such an article was purchased by a named person at an identified
shop on or about a specified date. If prima facie evidence of prior use
is put to the applicant they must respond ([Zannetos's Application, BL
O/119/96](https://www.gov.uk/government/publications/patent-decision-o11996)).
A vague or anecdotal allegation of prior use should however not be
pursued. (For details of the standard of proof required in cases of
alleged prior use, [see
2.29.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-29-1)).

\[ If the applicant does not admit prior use and is unwilling to amend
it will not normally be possible to pursue the matter. In no
circumstances should the substantive examiner offer to make the
anticipatory matter (or a drawing or photograph of it) available to the
applicant or to third parties, unless provided by a member of the
public. This is in order to avoid the involvement of the examiner in
their personal, as opposed to their official, capacity in the patent
proceedings. Any request or demand that an examiner should give evidence
as to prior use, whether orally or by affidavit or statutory
declaration, should be referred to the Director of Patents. \]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Examination for inventive step]{#default-id-6d9c2c45-heading-19 .govuk-accordion__section-button} {#examination-for-inventive-step .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Examination for inventive step\",\"index_section\":19,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-19 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-19" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Examination for inventive step\",\"index_section\":19,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.25 \[moved to [3.48](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-48)\] {#moved-to-348guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-48}

### 18.26 \[moved to [3.49](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-49), [3.64 to 3.65](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-64)\] {#moved-to-349guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-49-364-to-365guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-64}

### 18.27 \[moved to [3.66](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-66)\] {#moved-to-366guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-66}

### 18.28 \[moved to [3.54](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-54)\] {#moved-to-354guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-54}

### 18.29 \[moved to [3.50](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-50), [3.60](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-60)\] {#moved-to-350guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-50-360guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-60}

### 18.30 \[moved to [3.62](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-62)\] {#moved-to-362guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-62}

### 18.31 \[moved to [3.63](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-63)\] {#moved-to-363guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-63}

### 18.32 \[moved to [3.70](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-70)\] {#moved-to-370guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-70}

### 18.33 \[moved to [3.71](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-71)\] {#moved-to-371guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-71}

### 18.34 \[moved to [3.55](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-55), [3.69](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-69)\] {#moved-to-355guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-55-369guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-69}

### 18.35 \[moved to [3.57](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-57), [3.64](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-64)\] {#moved-to-357guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-57-364guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-64}

### 18.36 \[moved to [3.68](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-68)\] {#moved-to-368guidancemanual-of-patent-practice-moppsection-3-inventive-stepref3-68}
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Plurality of invention]{#default-id-6d9c2c45-heading-20 .govuk-accordion__section-button} {#plurality-of-invention .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Plurality of invention\",\"index_section\":20,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-20 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-20" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Plurality of invention\",\"index_section\":20,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.37 {#ref18-37}

The substantive examiner must consider whether the claims relate to a
single invention or group of inventively-linked inventions ([see 14.157
to
14.168](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-157)).
They should take into account any views expressed by the search examiner
([see 17.106 to
17.110](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-106))
but are not bound by these and must make up their own mind on the
subject; they should hesitate however before raising an objection if the
search examiner has considered the question and decided that the claims
do have unity of invention.

\[The substantive examiner should consider offering an apology on behalf
of the Office when raising an objection to plurality of invention if the
search examiner decided the same claim represented a single invention.\]

### 18.38 {#section-16}

When there is plurality of invention, the substantive examiner should
decide which of three courses of action to take at the first action
under s.18. The course of action adopted should be that which is likely
to be the most efficient in the particular circumstances of the case.
Firstly, the examiner may decide to perform the full substantive
examination at that stage. If substantive examination is performed an
objection to the lack of unity of invention should be incorporated in
the report. Secondly, the examiner may decide to defer consideration of
claims relating to a second or subsequent invention. If so, the
applicant should be informed accordingly. If no search has yet been
performed in respect of the second or subsequent invention, the
applicant should be reminded of this fact. Thirdly, substantive
examination may be deferred entirely and a preliminary report issued
under s.18(3) - [see 18.39 below](#ref18-39).

### 18.38.1 {#ref18-38-1}

If the examiner decides to perform substantive examination, an objection
to the lack of unity of invention should be incorporated into the report
using PROSE clause RC6. In line with the guidance set out at
[17.108.2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-108-2)
on the content of plurality objections at search stage, plurality
objections raised at substantive examination must contain:

a\) an introduction to the objection setting out the legal basis for it
and identifying the different groups of inventions, including where
possible an indication of the claims belonging to each group;

b\) the grounds for the objection including identifying the common
subject matter between the different groups of inventions or, if
appropriate, a statement on the lack thereof;

c\) if common subject matter has been identified a comparison with the
prior art/common general knowledge which explains why the features
identified as common subject matter are known or obvious (N.B. if prior
art is relied upon it should be clearly identified). If the claims do
not share common subject matter, this should be communicated to the
applicant;

d\) a concluding statement explaining that a lack of unity has been
found.

Standard clause RC6 has been drafted to provide a template in line with
these criteria, but examiners should note that it does not contain a
concluding statement and they should draft one in their own words
appropriate to the application. For the drafting of plurality objections
at search stage see 17.108.2.

\[ The aim should be to perform the full substantive examination if it
is practicable to do so. Consideration of the novelty or obviousness of
claims relating to a second or subsequent invention should be deferred
if the claims in question have not been searched. However consideration
in other respects should be deferred only if the amount of time or the
difficulty involved in considering all of the claims is likely to be
excessive. \]

\[ Objection to plurality of invention should be made formally using RC6
(or RC6PA for private applicants). If however the fault appears to lie
merely in unfortunate drafting of the claims, and the specification does
not appear to disclose separate inventions, RC6 should not be used and
the examiner should instead draft an objection in their own words. \]

\[ When using RC6 the reference to division should be omitted if less
than three months of the compliance period remains and so the deadline
for filing a divisional application under r.19 has passed. RC7PA should
be added to RC6PA if division is to be suggested to private applicants.
RC6A should be added if only the first invention has been searched. \]

### 18.39 {#ref18-39}

CoP is also relevant

The examiner may consider deferring substantive examination entirely
when more than one invention has been searched and it is not clear which
invention the applicant is likely to pursue, or when none of the
inventions has been searched, or when claims are of an unduly complex
nature not meeting the points laid out in the Code of Practice. Where a
formal objection arises under s.14(5)(d), a preliminary report should be
issued to this effect.

This report will be a report under s.18(3) and the compliance date
should be set as in [s18.47](#ref18-47) if the report is issued more
than three years and six months after the earliest date of the
application.

RC16 may also be added to this preliminary report to refer to points in
the Code of Practice. The applicant should be informed that further
substantive examination is deferred until the application has been
amended to meet this objection. Where no formal objection arises under
s.14(5)(d), RC16 should be used to refer to points in the Code of
Practice. The period specified for response under s.18(3) should be four
months unless the substantive examination has been combined with the
search, when the period for response should be set to expire two years
after the priority date, or if none, the filing date. The period
specified for the next report under s.18(3) should be two months or set
to expire two years after the priority date, or if none, the filing
date, if this is later for combined search and examination cases. If the
application is in order as a result of the response to the preliminary
report, and the applicant has indicated a possible intention to file a
divisional application, the applicant should be given an opportunity to
file the divisional application before the "parent" is granted (see
15.46). In any event, except for applications where the "3 month rule"
can be waived [(see 18.07.2)](#ref18-07-2), no s.18(4) report should be
made until 3 months after s.16 publication.

\[ Deferred examination should arise only if consideration of all of the
claims would involve an inordinate amount of work. If the application is
in order as a result of amendment in response to a preliminary report of
plurality of invention, EL3 should not be issued, since the preliminary
report constitutes a report under s.18(3). ([See also
18.47](#ref18-47)).

\[If the claims are amended in response to a plurality objection so that
they no longer relate to plural inventions, objections are outstanding
and no divisional application has been filed, PROSE clause RC26 may be
inserted into the subsequent s.18(3) report. It is irrelevant whether
the applicant has foreshadowed a divisional application or not, see [see
15.46](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-46).\]

### 18.40 {#section-17}

For a combined search and examination case, in the situation where it
would be possible to perform the search but the examination is to be
deferred for the reasons set out above, then both the search and
examination should be deferred.

\[The procedure set out in
[17.94.10](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94-10)
should be followed, with a suitably modified version of SE2 (SE2PA)
being sent to the applicant.\]

### 18.41 {#ref18-41}

(r.27(3)-(6) is also relevant)

A further Patents Form 9 (or Form 9A [see
17.02](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-02))
and search fee can only be accepted if the search examiner has reported
that the application relates to two or more inventions, and then only in
respect of claims present in the application at the time of the main
search ([see
17.111](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-111)).
If plurality is first reported in a report under s.18(3), issued after
the search report under s.17, a further Form 9 or Form 9A and search fee
should not be accepted even if the claims now sought to be searched were
in the application as filed; at this stage the applicant should be asked
to amend to overcome the objection. However, a further Form 9 or Form 9A
and search fee can be accepted when the search and examination are
combined and it is made clear in the substantive examination report,
issuing with the search report, that plurality is notified under s.17
([17.108](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-108)).
In such a situation, if a response to the examination report has been
received then the examiner should conduct a re-examination taking into
account the results of the further search. If no response to the
examination report has been received then it is open to the examiner to
decide whether it would be most efficient to conduct a re-examination in
light of the further search results, or to await a response to the
outstanding examination report. Re-examination at the time of conducting
the further search may be particularly appropriate if the compliance
date is close.

If the search examiner has reported plurality of invention and the
applicant has filed one or more further Forms 9 or Form 9A, but the
substantive examiner decides not to raise, or, having raised, decides to
withdraw, a plurality objection in respect of claims which the search
examiner has indicated related to more than one invention, a request for
a refund of the fee paid on the further form(s) should be acceded to.
Such a refund is made as a matter of discretion and not as a
rectification of an irregularity under r.107; it should not be given
where withdrawal of the objection is due to amendment of the claims. If,
on the other hand, amendment to remove plurality of invention involves
deletion of claims to an invention which was searched at the search
stage in favour of an invention present at that stage but not searched,
a search fee under s.17(8) should be required if a further search is
needed at substantive examination, see [18.03.3](#ref18-03-3). See also
[18.12](#ref18-12).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Other aspects of examination]{#default-id-6d9c2c45-heading-21 .govuk-accordion__section-button} {#other-aspects-of-examination .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Other aspects of examination\",\"index_section\":21,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-21 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-21" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Other aspects of examination\",\"index_section\":21,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.42 {#ref18-42}

r.12 are relevant

The specification should commence with a short and appropriate title and
should continue with the description (including a list of any drawings)
and the claims, in that order ([see 14.48 to
14.51](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-48)).
The substantive examiner should also consider whether those requirements
of r.14 and Schedule 2 which are not designated as formal requirements
([see
14.54](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-54),
[14.57](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-57))
have been complied with. Beyond that, the form of the specification is a
matter for the applicant, provided that s.14(3) and (5) are complied
with.

\[Examiners should only object to the title of the invention in the case
where the title does not indicate the matter to which the invention
relates. For example where the title relates to entirely different
matter. RC10 may be used to object to an inappropriate title. If the
grant title exceeds 158 characters, the title will be amended
accordingly because COPS cannot accept longer titles (see
[14.49](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-49),
[18.86](#ref18-86) and [2.40 of the Formalities
Manual](https://www.gov.uk/guidance/formalities-manual-online-version/chapter-2-request-for-grant-of-a-patent-form-1#ref2-40)).\]

### 18.43 {#ref18-43}

Objection should only be raised under s.14(5)(a), (b) or (c) if the
claims contain obscurities or ambiguities or are not clearly consistent
with the description and this causes genuine difficulty in determining
the scope of the claims. Attention should be focused principally upon
the main claim(s). The extent to which appendant claims are scrutinised
at substantive examination is a matter for the substantive examiner's
judgement; however the following principles are given for guidance:

\(a\) If a main claim appears to define an allowable invention all that
is generally required with regard to its appendant claims is to check
that they are supported by the description (particularly where they were
filed later than the application - see
[14.145](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-145)).
Objection to clarity in the appendant claims should only be raised when
the issue casts genuine doubt on the scope of the main claim or misleads
the skilled person who is trying to understand the invention as defined
in the main claim. Minor matters which would not lead a skilled reader
to misconstrue a claim should not be raised.

These include typographical errors, numbered clauses at the end of the
description, lack of antecedence and dependency numbering errors.

\(b\) When objection is to be raised to the main claim on grounds of
lack of novelty, inventive step, excluded matter, clarity, support etc.
and

\(i\) the substantive examiner can conclude, after considering the
search report (and any other prior art known to them), that a novelty,
inventive step, excluded matter, clarity, support etc. objection also
arises in respect of some or all of the appendant claims, they should
report accordingly;

\(ii\) the substantive examiner readily identifies an appendant claim on
which they have found no objection on this account, they should include
the fact, explicitly or by implication, in their report, without
committing the Office for the future since there may be pertinent prior
art not known to the examiner;

\(iii\) it is not clear to the substantive examiner what amendment of
the main claim is likely to be made to meet the objection, examination
of some or all of the appendant claims (and any further searching) may
be deferred.

\(c\) The applicant should always be able to deduce from the substantive
examiner's report which claims have been considered by the examiner and
with what result, and of which claims consideration has been deferred.

It should be remembered that an omnibus claim may be an independent
claim and must be considered separately ([see 14.124 to
14.125](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-124)).

\[Time should not be wasted on detailed checking of appendant claims;
for example, the effect of complex appendancies does not need to be
established. A cursory inspection of claims filed originally with the
application will often suffice if the main claims are acceptable.\]

### 18.43.1 {#ref18-43-1}

CoP is also relevant

In instances where the claims do not meet the points laid out in the
Code of Practice, the substantive examiner may consider deferring full
substantive examination. Where a formal objection arises, a preliminary
report under s.18(3) should be issued objecting under s.14(5)(a), (b) or
(c). RC16 may also be added to this preliminary report to refer to
points in the Code of Practice. The applicant should be informed that
further substantive examination is deferred until the application has
been amended, and/or comments have been made by the applicant, to meet
the objections raised. The procedure is otherwise as in
[18.39](#ref18-39).

### 18.44 {#section-18}

s.76(2) is also relevant

Objection should be raised to any obscurities which hinder the
understanding of the invention or cast genuine doubt upon the scope of
the claims. Objection should be made under s.14(5) in preference to
s.14(3) where this is possible (see [14.102-14.105 and
14.144](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-144).
Where appropriate a warning should be added that any amendment to meet
the objection must not add subject-matter. Minor matters which would not
lead a skilled reader to misconstrue a document or which would not be
pursued if no amendment were forthcoming should not be raised.

(For references in the specification to other documents, [see 14.93 to
14.96](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-93);
to prior art, [see 14.91 to
14.92](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-91);
to Trade Marks, [see
14.97-14.101](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-97),
[14.137](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-137).

\[No more of the description should be read in detail than is necessary,
for example to establish sufficiency and support for the claims.
Objection should not be made to consistory clauses merely because they
are not in literal agreement with the main claims. If the examiner
considers it helpful to suggest what form of amendment would be regarded
as satisfactorily meeting an Official objection, they should think
carefully about the suggestion before making it. Examples of minor
matters which should not be raised are inconsequential incorrect or
missing reference numerals or leading lines, missing words, repeated
phrases, and spelling errors numbered clauses at the end of the
description, lack of antecedence and dependency numbering errors.\]

s.76(2) s.14(5)(c) and s.76(2) are also relevant

### 18.45 {#section-19}

Objection must be raised if amendments to the specification result in
the application disclosing matter which extends beyond that disclosed in
the application as filed ([see 76.03 to
76.23](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-03)),
or if amended claims are not supported by the description as filed ([see
14.142-14.156](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-142)).
Original claims filed after the date of filing the application should be
treated in this latter way ([see
14.145](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-145));
in either case a warning should be given against adding subject-matter
to the description.

\[Added matter introduced by amendments submitted before, or in response
to, the substantive examiner's first report may be objected to using
RC9.\]

### 18.46 {#section-20}

As far as substantive examination is concerned, the claim to priority
should not be questioned unless it becomes necessary to do so in
considering the viability of a potential citation ([see 18.14 to
18.16](#ref18-14)).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 18(3)**
  If the examiner reports that any of those requirements are not complied with, the comptroller shall give the applicant an opportunity within a specified period to make observations on the report and to amend the application so as to comply with those requirements (subject, however, to section 76 below), and if the applicant fails to satisfy the comptroller that those requirements are complied with, or to amend the application so as to comply with them, the comptroller may refuse the application.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 18.47 {#ref18-47}

If it is found, either at the first substantive examination or following
subsequent amendment, that there are objections outstanding, the
examiner's report to the comptroller is issued to the applicant and
should specify the period within which a response should be made. The
report should usually deal first with fundamental matters such as
patentability (other than questions of novelty or obviousness) and unity
of invention ([see 18.37 to 18.40](#ref18-37)), if they arise, then
raise formally any objections based on lack of novelty or inventive step
([see 3.48 to
3.71](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-48)),
and thereafter deal with lack of clarity and inaccuracies and
inconsistencies in the claims and description in order of importance
([see 18.42-18.45](#ref18-42-18-45)). The object should be to present
the objections in a logical order. The report should also include any
report from the formalities examiner ([see 18.04](#ref18-04)). The
specification is not returned to the applicant with the report.

\[ When the application leaves the examining group, the appropriate
processing status should be recorded on PROSE.\]

The most common COPS processing statuses are listed below for
convenience:

2: Out for ABS should be used when search has been requested but is
being deferred awaiting action from the applicant ("Action Before
Search") - see MoPP
[17.03](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-03)
& [17.94.5 to
9](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94-5).
It should only be recorded provided no formal search has been carried
out (and no decision that no search is possible has been made).

3: Searched - Do not s.16 publish yet should be used when a formal
search or CS&E has been carried out but for some reason the examiner
needs to see the file again before it is sent for publication. This is
most often used when no abstract has yet been filed.

4: No search possible applies when the formal search report is to the
effect that no search was performed.

5: May be s.16 published will apply to the majority of completed
searches and CS&Es (including divisionals). It applies when the
application will be in order for A-publication with respect to the
examiner's requirements once the formal search report is ready to be
issued. It also applies to PCT national phase applications which have
been classified and are ready for republication. This status should also
be used when an application is in order for publication having
previously had a status recorded as in 2 or 3 above.

8: Out for ABE should be used when examination has been requested but is
being deferred awaiting action from the applicant ("Action Before
Examination").

9: Disposed of - In Order should be used when an intention to grant
letter has issued, including when the application is in order at first
examination. This status should not be used until after "A"-publication
is complete.

10: Disposed of - Not in Order should be used where the application is
not in order at first examination. This status should not be used until
after "A"-publication is complete.

11: Awaiting applicant's response generally applies when amendments
which do not put the application "in order" have been received and so a
further examination report is issued to the applicant. This status
should not be used if status 9 or 10 has not been previously set.

12: Ready for Grant should be recorded once the application is finally
in order and all "B" stage revision of the COPS data has been completed,
and the date given in the intention to grant letter has passed. (This
status also applies to an application which has been recalled from grant
(see below) but is again in order for grant.)

13: Not Ready for Grant is used when an application which has been
recorded as Ready for Grant needs to be recalled. It is essential to act
promptly when using this status, otherwise publication may have gone too
far to allow automatic intervention. This status may be recorded by the
Publication Liaison Officer in Formalities.

\[ Unless the search and examination have been combined or an
abbreviated examination report ([see 18.47.1](#ref18-47-1)) is issued,
the first report under s.18(3) should be issued under cover of
examination letter EL1. If the report is issued 3½ years or more after
the earliest date of the application, the letter should normally specify
a period for reply of two months. For reports issued less than 3½ years
after the earliest date, the letter should normally specify a reply
period of four months [(see 18.49](#ref18-49)). Subsequent s.18(3)
reports should be issued using EL2, specifying a period of two months or
less for reply ([see 18.49](#ref18-49)) and making reference to the date
of the agent's letter replying to the previous report. For combined
search and examination, the report should issue with SE1 or SE4.
Furthermore, if the examination letter is issued within three months of
the end of the unextended compliance period, ELC1 should be added. If it
is issued in the two months extension obtained by filing Form 52, ELC2
should be added. If it is issued within two months after the end of the
compliance period so that filing of Form 52 is necessary, add ELC3. If
the examination letter is issued after the end of the extended
compliance period letter EL5 should issue ([see also
20.06](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-06)).\]

R.30(2) is also relevant

\[ When the first report (or in the case of a divisional application,
the first report on the earliest 'parent' application) is issued later
than three years and six months from the earliest date, then ELC4 (or
ELC5 for the divisional) should be added to EL1. This reminds the
applicant that the unextended period for getting the application in
order will expire twelve months from the date of issue of the first
report (or, in the case of a divisional application, twelve months from
the date of issue of the first report on the earliest 'parent'
application). Provided that the relevant checkbox is ticked confirming
that this is the first examination report for the case, Prose will add
the ELC4 letter clause automatically during creation of EL1 letters
created more than 3 years and 6 months from the earliest date. As
discussed above, for such letters, the period for response should
normally be set to the Prose default period of 2 months. If ELC4 is not
added during creation of the letter it can still be added to the letter
after creation. If ELC4 was omitted from a first examination letter
issued later than three years and six months from the earliest date,
then the applicant or agent should be informed of the correct compliance
date at the earliest possible opportunity, and the examiner should
arrange for the compliance date to be set correctly on COPS. If this
mistake becomes apparent at the second examination, then a clause should
be added to the EL2 letter to inform the applicant that the normal
unextended period allowed for complying fully with the requirements of
the Act will end 12 months after the date that the first substantive
examination report was sent.\]

\[ Whenever the specification includes amendments not previously
reported on, RC8 or SC1, as and when appropriate ([see
18.08](#ref18-08)), should usually be the first item in the report,
indicating the date of the letter which accompanied the amendments. When
documents are formally cited in support of an objection of lack of
novelty or inventive step, RC1 or RC3 should be used (or RC5 if the
claims are obscure), with RC1A, RC1B, RC1C, RC1D, RC3A or RC3B added if
appropriate ([see 18.15 to 18.16](#ref18-15)).\]

\[ Documents cited or otherwise referred to in a s.18(3) report should
be itemised by number etc, and passages considered to be relevant by the
substantive examiner should also be identified, so that the report is
complete without recourse to the search report under s.17. References to
published or reported judgments and decisions should fully and clearly
identify the report etc in question in the normal way, such as in the
Table of Cases of this Manual.\]

\[ The examiner should produce the examination (covering) letter and the
s.18(3) report using numbered paragraphs and descriptive sub-headings as
appropriate. Examination letter clauses should be inserted in the
examination letter after the paragraph relating to the need to file
amendments or make observations on the report.\]

\[ The examination report should not refer (except in certain limited
cases relating to divisional applications) to objections which might
arise if a presently unpublished application by the same applicant (or
otherwise) that the examiner may be aware of were to be published. In
these circumstances the examiner should create a minute and raise the
objection if and when the other application is published. [See
118.16](/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-16).\]

\[ After preparation of the s.18(3) report and examination (covering)
letter, the substantive examiner should send the appropriate PDAX
message to their revising officer, Group Head or Examination Support
Officer, as appropriate. Any additional instructions to the Examination
Support Officer should be written on the checklist in Prose. The
Examination Support Officer should then import the examination report(s)
and covering letter(s) into the dossier and issue the required copies of
each to the applicant.\]

\[ When a substantive examiner forwards a case for issue of a
preliminary report (see [18.39](#ref18-39), [18.43.1](#ref18-43-1), and
[18.48](#ref18-48)), they should send the appropriate PDAX message to
their Group Head.\]

\[ When, during substantive examination, a letter is to issue which does
not embody a report under s.18 (eg an answer to an agent's enquiry, or
the offer of a hearing), the substantive examiner should ensure that the
letter is headed "Patents Act 1977", the application number is
identified and, if appropriate, the latest date for reply is
specified.\]

### 18.47.1 {#ref18-47-1}

If an examination opinion identifying major defects (see 17.83.3) has
been issued at search stage and no response has been filed, the first
report under s.18(3) should take the form of an abbreviated examination
report (AER) which essentially reproduces the examination opinion. For
international applications entering the national phase where the
International Search Report or International Preliminary Report on
Patentability (IPRP) indicate that major amendment is required, if the
examiner is in agreement, an AER based upon information in these reports
should be issued when national phase examination takes place ([see
89B.15.1](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-15-1).
Where an AER is issued, the top-up search should be deferred (if
required -- [see
89B.12.1](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-12-1))
until the application has been amended.

\[Letters EL1A or EL1B should be used to accompany an abbreviated
examination report (AER), while letter EL1C itself acts as an AER. The
reply period is usually two months for EL1A (AER issued following no
response to an examination opinion), and four months for EL1B (AER
issued for PCT applications without an international preliminary report)
and EL1C (AER based upon an international preliminary report on
patentability). A shorter reply period for EL1B and EL1C may be
specified by the examiner if this is considered more appropriate. Where
the EL1C letter is issued more than 3 years and 6 months from priority,
the reply period should be two months. The AER should use clauses
selected from EC1 to EC8. If an AER based on an examination opinion or
an International Preliminary Examination Report is issued more than 3
years and 6 months from the priority or filing date, ELC4 should be
added to the EL1A or EL1C letter to set the correct compliance period;
Prose will do this automatically if the relevant checkbox is ticked.\]

\[As the first action under s.18(3), the issue of an abbreviated
examination report (AER) should be booked out as a completed examination
([see
89A.14.2](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-14-2)).\]

### 18.47.2 {#ref18-47-2}

For applications that qualify for combined search and examination but
for which search would not serve a useful purpose ([see
17.94-17.101](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94)),
an abbreviated examination report should be issued. This combined report
under s.17(5)(b) and s.18(3) should inform the applicant of the reasons
why the application has not been searched. Once the report is issued
there will be no refund of the search or examination fee, therefore
consideration should be given as to whether action before combined
search and examination or action under s.17(5)(b) is most appropriate
([see
17.94.5](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94-5)).

\[Letter SE2 should accompany the AER. The period specified for reply
should be set to expire two years after the priority date, or if there
is none, the filing date. The heading of the AER should be edited to
read "Combined Search Report under Section 17(5)(b) and Abbreviated
Examination Report under Section 18(3)".\]

### 18.48 {#ref18-48}

Occasionally the substantive examiner may decide that the specification
is so obscure that useful examination is impossible. In such a case,
they should report to this effect under s.18(3), stating briefly the
nature and extent of the obscurity and saying that further examination
is deferred pending amendment. In such a case the period specified for
reply should be four months or for combined search and examination cases
set to expire two years after the priority date, or if there is none,
the filing date (see 18.49 below).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Period specified for response]{#default-id-6d9c2c45-heading-22 .govuk-accordion__section-button} {#period-specified-for-response .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Period specified for response\",\"index_section\":22,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-22 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-22" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Period specified for response\",\"index_section\":22,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.49 {#ref18-49}

Under s.18(3) the comptroller has power to refuse an application where
the applicant fails to file a satisfactory response to an official
report within the period specified therein. This period is set at the
examiner's discretion, but there are certain standard periods which
should normally be set unless the circumstances dictate otherwise. For
first examination reports (other than combined search and examinations)
issued less than 3½ years after the earliest date of the application,
the standard period for response is four months from issue of the
examination report. Where the first examination report is issued 3½
years or more after the earliest date of the application, the standard
period for response is two months from the date of issue of the
examination report. The reason for this shorter period for response is
that in these cases the applicant has only 12 months to get the
application in order, and so a longer response period is likely to lead
to a very compressed timetable in subsequent actions, particularly as
such periods are extendible as-of-right by 2 months -- see
[18.53](#ref18-53). These norms of four months and two months are not
rigid ones, and longer or shorter periods may be set if the
circumstances warrant it; for example, first examinations issued very
close to 3½ years after the earliest date also clearly give rise to the
same risk of problems with a compressed timetable for getting the case
in order.

Second or subsequent examination reports have a standard response period
of two months, although again this should be altered if the
circumstances require it. Thus the period of two months could be
increased to three months if a major objection was inadvertently not
previously raised, or if a document is cited for the first time.
Alternatively a shorter period may be set if only minor objections are
made in a second or later examination report. It is imperative that the
remaining compliance period is considered when setting response periods
for further examination reports. When the unexpired portion of the
normal compliance period is less than twice the appropriate s.18(3)
period, only one half of the remaining time should be specified rounded
down to the nearest month or, when less than six weeks remain, down to
the nearest week. In this latter case the period should be specified in
weeks and not in fractions of a month. When less than six weeks remain,
the substantive examiner should use their discretion.

For combined search and examination cases (other than divisional
applications -- [see
15.46](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-46))
the period for response to a first report under s.18(3) should be set to
expire two years after the priority date, or if none, the filing date.
The periods set for response to any further reports under s.18(3) should
normally be two months (following the approach described above), unless
any such period would expire before the date previously set for reply to
the first report. In that case the period once again should be set to
expire two years from the priority date, or if none, the filing date. It
is possible for a late declaration of priority to be made ([see
5.26](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-26))
after a combined search and examination report has issued, in which case
the examiner should reissue the report under s.18(3) bearing a reply-by
date calculated from the newly declared priority date. The relevance of
citations should not be re-assessed at this point but will need to be
considered when a response to the report is received. Where s.21
observations are filed near to the end of the compliance period, that
period may be extended as described in
[20.02.1](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02-1)
and, if so, the report should inform the applicant of the extension and
the period for response should be set accordingly.

\[When the specified period is to differ from the default period set by
PROSE, the substantive examiner should determine the correct period and
adjust the latest date set for reply accordingly.\]

### 18.50 {#section-21}

Occasionally a further report under s.18(3) may be sent raising
additional points before a reply to an earlier report is received. This
may happen if an agent's letter crosses with the report or if a further
citation is found. When this occurs the applicant should not only be
informed of the period for reply to the further report but also be
reminded of the period for reply to the earlier report. When the two
reports are on the same or related matters it is preferable that the two
periods should be arranged to expire at the same time. Thus, if there is
sufficient of the period already specified remaining, the period for
reply to the further report may be set to expire at the end of the
period already specified for the earlier report. Alternatively, in
appropriate cases, the first period may be extended to the end of the
period specified for the further action. Only exceptionally should the
period specified in respect of the further action expire before the end
of the period specified for the earlier action.

### 18.51 {#section-22}

\[Deleted\]

### 18.52 {#ref18-52}

CoP is also relevant

Although s.18(3) gives the comptroller the power to refuse the
application if they are not satisfied that any objections have been
overcome, this power will not be exercised where the applicant has made,
within the specified period, an attempt to advance the case towards a
final decision as to its allowability; this attempt can be argument,
amendment, a request for hearing or a request for interview. Merely
filing requests for further searches under s.17(6) does not address
objections raised under s.18, and therefore is not an attempt to advance
the case. Similarly where a report under s.18(3) is accompanied by the
offer of a hearing, a response which merely declines the offer is not an
attempt to advance the case ([see also 18.80.1](#ref18-80-1)). Normally
a proposal to amend the claims only (leaving consequential amendment of
the description and drawings to be done when the claims are settled)
would be regarded as such an attempt. A request to defer amendment of
the description until the independent claims have been agreed upon
should therefore be considered favourably. If however the response to
the examination report is such that it cannot be regarded as such an
attempt the applicant should be warned in writing that refusal is
contemplated unless the outstanding matters are addressed within two
months of the expiry of the period for reply originally specified (or
the compliance date if that is sooner). If this fails to elicit a
satisfactory response, they should be formally advised that it is
proposed to refuse the application as provided by s.18(3), but that
before this is done they will be given an opportunity to be heard in the
matter, and a hearing may be appointed ([see 18.79-18.80](#ref18-79) for
the relevant guidance). If no reply is received to the offer of a
hearing the application may be allowed to lapse at the compliance date
(see [18.79](#ref18-79) and
[20.03](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-03)
for further details).

### 18.52.1 {#section-23}

If a third party requests in writing that an application be refused
because the applicant has failed to file a satisfactory response to an
official report within the period specified therein (taking into account
all possible extensions of time), the applicant should be advised that
since they have failed to respond within the period for response the
comptroller proposes to refuse the application. The third party's letter
should also be drawn to the applicant's attention. A final decision to
refuse an application should be made with regard to all the facts of the
case, and the applicant should therefore be offered the opportunity to
provide reasons for their lack of response and/or request a Hearing. See
procedure in [18.54](#ref18-54).

### 18.52.2 {#section-24}

If, however, an enquiry is received from a third party, merely asking
whether the application will be refused, the comptroller should respond
to the third party to inform them of the compliance date of the
application in question and that our normal practice would be to treat
the application as refused at the compliance date if no response has
been received from the applicant.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Extension of the period (an aide-memoire is provided in Annex B)]{#default-id-6d9c2c45-heading-23 .govuk-accordion__section-button} {#extension-of-the-period-an-aide-memoire-is-provided-in-annex-b .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Extension of the period (an aide-memoire is provided in Annex B)\",\"index_section\":23,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-23 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-23" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Extension of the period (an aide-memoire is provided in Annex B)\",\"index_section\":23,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.53 {#ref18-53}

r.109(2), s.117B(3), r.109(1), s.117B(4) and s.123(3A) are also relevant

An automatic extension of two months (or to the end of the compliance
period, as prescribed by rule 30 for the purposes of section 20, if this
expires sooner) to the period set in an official report can be obtained
by requesting it in writing ([see also 18.53.1](#ref18-53-1)). The
request must be received before the end of the period as extended. Only
one extension of this type is available. Further extensions may be
available at the examiner's discretion if an automatic two month
extension has already been granted. Any request for a further extension
must be made before the end of the period as already extended and an
adequate reason must be given ([see 18.56 to 18.57.1](#ref18-56)). Under
Rule 109 of the Patents Rules 2007, there is no longer a requirement for
a request for a further extension under s.117B(4)(b) to be made in
writing. However this may be required if the examiner feels that a
written request is appropriate in the circumstances. A discretionary
extension of one month (in addition to the automatic two months) may be
granted readily however any longer or further extensions should be
accompanied by a very good reason. Evidence to substantiate any reason
given can always be requested if considered necessary (see also
[123.36.8](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36-8)
and
[123.37](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123/37)).

\[ When an automatic extension of two months is requested the
formalities examiner should create a minute noting that a request has
been made, and add it to the dossier. If the request is filed within the
two month period the application will be processed in the normal way.
Where a response to an examination report is made outside the extended
period the application should be referred to the substantive examiner
for consideration. They may exercise the discretion provided by section
18(3) to accept the late response rather than moving to refuse the
application.\]

\[ A request for a further extension is referred to the substantive
examiner, who should not hesitate to consult their Group Head. In
reaching a decision whether or not to exercise discretion to extend the
period, each case should be considered on its own merits and the
contents of [18.54-18.57.1](#ref18-54) should be regarded as guidelines
rather than rigid rules. The request should normally be answered by
telephone or by electronic mail and a minute (or the telephone report or
email) added to the dossier. A concise report of the telephone
conversation should always be sent and a copy placed on the open part of
the file. A report of an email correspondence need not be sent unless
specifically requested or if it is necessary to clarify the reasons for
refusing an extension, but a record of the email exchange should be
placed on the open part of the file. The report should clearly state the
extension allowed or the reason(s) for refusal which should be carefully
worded bearing in mind the possibility of appeal against a decision to
refuse. If the report is necessarily lengthy it is preferable to issue a
letter instead of a telephone or email report. In all cases, the reason
for the request should appear on the open file, eg in the applicant's or
agent's letter or s.18 report or telephone or email report. When, after
a period for reply under s.18(3) has been specified, the criteria used
for determining the period are changed (for example the compliance
period is extended or one of the norms set out in paragraph
[18.49](#ref18-49) is altered) such that if the new criteria has been
applied a longer period would have been specified, a request by the
applicant for an extension of time to make the specified period up to
the longer period should be allowed.\]

### 18.53.1 {#ref18-53-1}

Requests for extensions of time may be made by email and the Comptroller
has directed that such requests be made to the email address
<pateot@ipo.gov.uk>. The Office does not guarantee to recognise requests
sent to any other email address.

\[All requests sent to the 'pateot' email address will receive an
automated acknowledgment confirming receipt: requests for an automatic
extension will receive no further response because the act of validly
requesting the extension is all that is necessary for it to be obtained;
when a request for a discretionary extension is received, the
Examination Support Officer will send the appropriate PDAX message to
the substantive examiner for consideration. Email requests will be noted
and imported into the dossier by the Examination Support Officer. They
will also check to see if the request has been made on time and that it
appears to come from the applicant or appointed agent. If a request is
made out of time or apparently not by the applicant or their
representative then the procedure of the following paragraph should be
followed.\]

### 18.54 {#ref18-54}

s.101 and s.20A is relevant

When a reply is received after the expiry of the specified period and
the automatic extension period of two months has passed, the reason, if
not already given, should be asked for. If no reason is forthcoming the
late response cannot be accepted and a report under s.18(3) should issue
informing the applicant that the application will be refused unless
observations are forthcoming, or a hearing is requested. Where a reason
is provided, the examiner may exercise discretion under s.18(3) to
accept the late response, even though no extension to the specified
period can be granted. Discretion should be exercised favourably,
particularly if; i) the extension period has not been exceeded by more
than a de minimis period, and/or, ii) the examiner is satisfied that the
failure to respond was unintentional at the time that the specified
period expired. Point ii) is consistent with the statutory test that
applies to requests for reinstatement under s.20A ([see 20A.13 to
16](/guidance/manual-of-patent-practice-mopp/section-20a-reinstatement-of-applications/#ref20A-13))
for guidance on the meaning of unintentional). However, there is no
statutory requirement that the failure to respond must have been
unintentional in order for the late response to be accepted, and thus
the discretion accorded by s.18(3) may be exercised in appropriate
circumstances even if this criterion is not met.

### 18.54.1 {#section-25}

After considering the matter ([see 18.56-18.57.1](#ref18-56)) the
substantive examiner should report either that the application should be
allowed to proceed or that the application should be refused for
non-compliance with s.18(3) within the specified period. If the
substantive examiner is minded to refuse the application, the applicant
should be forthwith informed of the fact and told that, if they wish, an
opportunity will be given to hear them in the matter. A month should be
given for reply. If no reply is received the application will be treated
as refused under s.18(3) at the end of the compliance period. Where
however the applicant asks to be heard, a Deputy Director will take the
hearing and may decide either to refuse the application or to allow it
to proceed subject to such conditions as they think fit. Where an
application is refused, and a hearing has taken place, the applicant
should be notified in a formal written decision signed by the Deputy
Director acting for the comptroller.

\[ When a reply is received outside the automatic two month extension
period and no request for extension is received, the formalities
examiner should add a minute to the dossier and send the appropriate
PDAX message to the substantive examiner, who should if necessary
consult their group head. In the interests of uniform practice,
consultation with the Divisional Director should be freely resorted
to.\]

### 18.55 {#ref18-55}

It should be borne in mind that the periods normally specified for
response to the first s.18(3) report were determined having regard to
all normal conditions, including the availability of an automatic
two-month extension. While every case must be decided on its merits, the
decision in [Jaskowski's Application, \[1981\] RPC
197](http://rpc.oxfordjournals.org/content/98/13/197.abstract?sid=b3c017d3-f2fa-4b57-805d-e8fe6bb52f87){rel="external"},
furnishes some guidance in this matter. In that case the applicant's
agent sought an extension on the grounds that delays were inevitably
caused by the need to consult US Patent Attorneys who in turn had to
seek instructions from the applicant. The hearing officer, in refusing
the request, stated "s.18(3) clearly gives the comptroller discretion to
extend the specified period but unless a coach and horses is to be
driven through the subsection they must have some adequate reason for
exercising that discretion which is peculiar to the particular applicant
or application in suit. I can see nothing abnormal in the chain of
communications in this case... which could be regarded as an adequate
reason for extending the specified period".

### 18.56 {#ref18-56}

r.109(2) is also relevant

It follows that factors which may be considered normal in relation to
all or particular categories of application, eg, the distance of
applicant's location from the UK, the complexity of the subject matter
of the application or objections thereto, absence on business or
holiday, and a preference of the applicant to defer response until
reports of parallel applications abroad have been received do not
constitute good grounds for an extension of the specified period; on the
other hand, extreme complexity or remoteness, such incidents as illness
of or serious accident to applicant or agent, also fire and explosion,
wars, revolutions, etc, and natural calamities, which destroy documents
or dislocate normal operations, may do so. Further, when it is necessary
for the applicant to adduce technical evidence an extension may be
allowed in appropriate cases. In [McDonald's Application (BL
O/71/96)](https://www.gov.uk/government/publications/patent-decision-o07196),
after expiry of the normal period for putting the application in order,
the agent requested allowance of a late response to a first s.18(3)
report on the grounds that the applicant had been awaiting the results
of searches and examinations on corresponding applications filed
elsewhere, that they (the agent) was unfamiliar with examination
procedures before the Office and that there had been delays because of
overseas travel and family illness. In refusing the request the hearing
officer noted that family illness might provide an adequate reason but
took no account of it because this difficulty arose well outside the
period set for response and none of the other reasons put forward for
the delay up to that point were sufficient. Extensions of time to await
the issue of reports on corresponding applications were also refused in
[Smart Card Solutions' Applications \[2004\] RPC
12](http://rpc.oxfordjournals.org/cgi/content/abstract/121/8/273){rel="external"}.
In this case, the hearing officer did not consider that the potential
cost savings to the applicant as a small business in avoiding separate
responses to the UK and PCT applications by deferring response until
issue of the International Search Report an adequate reason for
extension as this was not peculiar to the applicant. The hearing officer
also pointed out that the public interest should be protected "by
ensuring that any uncertainty involving a patent application is resolved
as quickly as possible" by not allowing the examination process to be
drawn out without good reason for the delay. A particular category of
adequate reason is that the applicant is a German firm which has the
inventor as an employee and which does not wish to proceed with the
application but by German law is obliged to give the employee the
opportunity of proceeding with the application on their own behalf,
within a mandatory period before terminating the application.

A further, but restricted, category of adequate reason is that where an
applicant requests an extension of a specified period (possibly of
several months) to carry beyond the date of expiry of the nine months
opposition period running in respect of a granted European Patent (UK)
equivalent and has indicated that they will not proceed with the UK
application if no opposition to the European Patent (UK) equivalent is
entered. Provided that sufficient of the compliance period would still
remain in order to deal with any objections outstanding on the UK
application if it should have to proceed, the request may be allowed to
avoid an unnecessary waste of Office and the applicant's time. So far as
delays in (or failures of) communication services are concerned, some
protection to applicants may be afforded by r.111, under which ad hoc
interruptions and dislocations to a communication service by which
documents may be sent and delivered (including the postal service,
electronic communications, and courier services) may justify an
extension to a period of time specified in the Act or Rules.

\[Where correspondence from the Office is reported as never having
arrived at its intended destination, or is reported as being misdirected
or delayed, this fact should be recorded by sending a minute to the
relevant formalities group with any relevant details. Use of r.111 to
extend a deadline may only be authorised by the relevant Head of
Administration ([see
123.47.1](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-47-1)).\]

### 18.56.1 {#ref18-56-1}

While decisions made in relation to discretionary extensions of the
compliance period under r.108 (see 20.02) are legally distinct from
discretionary extensions to the specified period for response, the
reasoning in such decisions may nonetheless be relevant, as noted in [BL
O/610/22](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/610/22){rel="external"}.

### 18.57 {#ref18-57}

In Jaskowski's Application, ([see 18.55](#ref18-55)) the agent was
seeking an extension without having received instructions from the
applicant. The hearing officer indicated that it is the applicant's
responsibility to respond to the examiner's report. However a failure on
the part of the agent to respond within the specified period may be an
adequate reason if the failure was due to an exceptional factor as
exemplified in paragraph [18.56](#ref18-56). It may also be an adequate
reason if it is established on the evidence filed that the failure to
take appropriate action was due to the fault of the agent.

### 18.57.1 {#ref18-57-1}

It is however recognised that even a well-organised system will break
down occasionally. An isolated slip in office procedure by the
applicant, their agent or their servants may be a good ground as would
unusual congestion of urgent work. Account may also be taken of
temporary difficulties, such as financial problems faced by an
applicant, with a view to avoiding early refusal of an application
against the longer term best interests of the applicant. Nevertheless,
further extensions of time beyond the automatic two months available
([see 18.53](#ref18-53)) should be the exception rather than the rule.
Consequently, the following should be taken as a guideline:

\(a\) A reason should always be required, but a single further extension
of up to a month will generally be given even if the reason is not one
of the sort which would be acceptable according to [18.56](#ref18-56)
above.

\(b\) Additional requests for extensions of time (whether at the same
action or a later one) require a very strong reason and should normally
be no longer than a week or the minimum required to overcome the problem
encountered.

\(c\) Extensions of time should be refused or require evidence where an
agent or applicant consistently requests extensions to the period for
response without good reason being given.

### 18.58 {#section-26}

The length of further extensions in addition to the automatic two month
extension is in many cases indicated by the reason for making the
request; otherwise a month's extension is generally appropriate for a
further extension. The substantive examiner should, of course, always
take into account how much of the compliance period remains.

### 18.59 {#section-27}

s.101 and s.20(1) is also relevant

If a request for an extension is refused, the applicant may request a
hearing. If the hearing officer refuses to allow the extension and the
specified period has expired the application will be refused under
s.18(3). If the period has not expired at the time of the hearing, or if
no hearing is requested, no action should be taken until a response to
the outstanding objections is received ([see 18.53](#ref18-53)). If no
response is received before the compliance date the application will be
treated as having been refused ([see
20.03](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-03))
unless both an extension of the compliance period under r.108(2) or (3)
and (4) to (7) ([see 123.34-
41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34))
and an extension of the s.18(3) reply period are sought and granted.

\[Where extension of the compliance period is sought under r.108(3), the
Deputy Director should decide that question (see 4th paragraph of 20.02)
together with the request for extension of the s.18(3) reply period. The
Deputy Director should feel free to consult the Registered Rights Legal
Team about the decision under r.108(3).\]

### 18.60 {#ref18-60}

In order that applicants who simply respond outside the automatic two
month extended period should not have an advantage over those who
request an extension of time before the end of the extended period, it
is essential that the question of an extension should be treated as
distinct from that of the content of the response and should be settled
before any amendments and/or observations submitted are considered.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Re-examination]{#default-id-6d9c2c45-heading-24 .govuk-accordion__section-button} {#re-examination .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Re-examination\",\"index_section\":24,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-24 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-24" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Re-examination\",\"index_section\":24,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Amendments

### 18.61 {#ref18-61}

CoP is also relevant

Normally the applicant or agent responds to a report under s.18(3) by
filing amendments, and/or observations. Amendments may be filed in
electronic form if they are submitted via the [Office
website](http://www.ipo.gov.uk/p-apply-online-sfd-checklist.htm){rel="external"},
or using the secure online filing system provided by [EPO Online
Services](http://www.ipo.gov.uk/p-apply-online-epoline.htm){rel="external"}
if the filer has completed the enrolment procedure for that service.
Office policy is that amended pages should not be accepted by email.
While it is possible under section 124A(3) to exercise discretion to
accept documents submitted by email, this should only be allowed in
exceptional circumstances and should not be encouraged. The amendments
should normally be in the form of new pages or sheets of drawings to be
incorporated into the specification. It is helpful if a further copy of
the amended pages is provided indicating the changes made either in
manuscript on the original pages or using standard word-processing
features since this aids identification by the examiner of the changes
and where support is provided for them in the description. However minor
amendments may be submitted in manuscript on photocopy pages or may be
requested in a letter and be effected by the substantive examiner,
provided they can be regarded as de minimis ([see
14.37-38](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-37)).
Minor amendments to drawings, eg the insertion of a reference numeral,
which can be regarded as de minimis may also be effected by the
substantive examiner if requested in a letter and in accordance with
[14.28](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-28)
and
[14.40-45](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-40).
Amendments may also be submitted informally, with a view to filing
retyped pages when the allowability of the amendments has been
confirmed; this practice should not normally be continued in subsequent
actions. When no other matters are outstanding and the substantive
examiner asks for new pages to be filed, one month should be specified
for reply (unexpired compliance period permitting).

\[ Care must be taken to ensure that all letters etc are placed on the
correct file. Where there are inter-partes proceedings in relation to a
patent application (principally proceedings under s.8, 10 or 12) there
is a separate Litigation Section file for those proceedings. In such
cases, there should be a warning label on the dossier cover and a
warning sheet in the dossier table of contents.\]

\[ Amended or new claims received before the preparations for
publication are completed are included in the published application
([16.16 to
16.19](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-16),
[19.15 to
19.16](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-15)).
To ensure that the application as filed is published, any amendments of
the description, drawings or claims should be annotated appropriately in
the table of contents. The substantive examiner should not enter
amendments on the original specification before s.16 publication. As
with any application on which amended or new claims are filed before the
completion of preparations for publication, the formalities examiner
should carry out the necessary checks on the new or amended claims. The
amended claims should be annotated and a PDAX message, which includes
the date on which the amendments were received in the Office, sent to
the substantive examiner. The examiner may make manuscript amendments
once the application has been published.\]

\[Amendments containing personal information should only be published or
placed on Ipsum with the explicit, written consent of the applicant.
Extra checks should be conducted before A and B publication to ensure
consent has not been rescinded. The Formalities examiner is responsible
for ensuring these checks are completed and should liaise with
Publishing and the Patent Examiner as necessary. If the amendments
contain sensitive personal information which the applicant subsequently
removes through filing further amendments, the original pages should not
be placed on Ipsum or made OPI, they should be redacted and deleted as
necessary by the Formalities examiner.\]

\[ Any replacement pages filed after publication should be imported into
the dossier by Index and Scanning. The formalities examiner will create
a new description, claims and or drawings, which will be appropriately
labelled, before the case is sent to the substantive examiner. Duplicate
new pages should be kept with the letter which accompanied them.\]

\[ When retyped pages are filed solely to meet the requirements of r.14
and Schedule 2, either at the first response or following deferment
permitted by the substantive examiner, they should be checked by the
formalities examiner for textual consistency with the pages they
replace. If the formalities examiner is in doubt as to the authentic
text against which to check they should refer the file to the
substantive examiner who should either identify the correct text or, if
they prefer, carry out the necessary check themselves. The formalities
examiner will not check the text of the replacement pages when they
incorporate amendments which have not previously been written into the
specification, or when they have been filed in response to a report
containing substantive as well as r.14 objections; in these cases it
will be necessary for the substantive examiner to determine whether or
not there is added matter or errors.\]

\[The substantive examiner should decide whether any discrepancies noted
during checking, either by themselves or the formalities examiner, are
such as to require a further report under s.18(3). If they are merely
omissions to which the de minimis rule can be applied the examiner may
ignore them or correct them with the applicant's agreement. Otherwise,
new pages should be requested.\]

\[ Any incoming correspondence on published applications should be
checked by the formalities examiner, who should redact certain personal
or sensitive information from documents which may be made available for
online inspection via Ipsum (the Office's online patent information and
document inspection service). In addition, if the substantive examiner
becomes aware of any personal information in any incoming correspondence
received after 1 March 2011, they should redact it as necessary. The
Enhance feature on PDAX may be used to redact any such personal
information. Once this has been done, the following steps need to be
taken: "set handle" the redacted version; annotate it as "OLFI", remove
any OLFI annotation from the interim or original version of the letter
(and close any interim version); and send a SET PUBLIC message to the
formalities group team mailbox and save or close the dossier.\]

### 18.62 {#ref18-62}

Where the amendments do not meet the substantive objections, the
substantive examiner should use their discretion in deciding whether to
allow compliance with r.14 to be deferred further, having regard to the
nature and extent of the amendments required and the unexpired
compliance period remaining. If further deferment is allowed, the
examiner should include in their next s.18(3) report a reminder that the
r.14 objections are outstanding and that compliance will be required as
soon as the substantive objections have been met. If, on the other hand,
no further deferment is to be permitted, the examiner should state in
their s.18(3) report that all further amendments must be filed on new
pages complying with r.14.

\[ In the case of private applicant cases, if after a request the
applicant fails to submit pages complying with r.14 and Schedule 2 and
the application is otherwise in order for grant, the application should
be referred to the Formalities Manager to approve any necessary retyping
in the Office. The applicant should be advised of such retyping and
informed that the application will proceed to grant unless an objection
to what has been done is lodged within four weeks.\]

### 18.63 {#ref18-63}

s.76(2) is also relevant

The form of the amendments is entirely a matter for the applicant, so
long CoP as they do not add subject-matter. However, a full response
should be made to each and every objection raised in an examination
report through amendment and/or argument so as to progress the
application towards grant. (For amendment other than in response to an
examiner's objections, and the timing of such amendments, [see 19.13 to
19.22](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-13)).

### 18.63.1 {#section-28}

CoP is also relevant

Where objections are to be met by amendment, an appropriate explanation
should be provided, in particular, how the amendment meets the
objection, and how it is supported by the specification as originally
filed. The covering letter accompanying the amendments should explain
how each objection has been overcome. Where major objections related to
novelty, inventive step and other patentability issues have been raised,
the letter should point out where support for the amendments lies and
give reasons why the amended claims overcome the objections, for
example, by stating what is considered to constitute the inventive step
in the independent claims.

### 18.63.2 {#section-29}

CoP is also relevant

If the applicant considers an objection in an examination report to be
unfounded, detailed reasons which adequately address the issue should be
provided in the response. In particular, when responding to an objection
to lack of inventive step, the response should assist the examiner in
understanding the perspective of the skilled person at the priority date
of the invention. Rebuttals which include comments such as "the
invention is not obvious because no-one has done it before" and "the
examiner has fallen into the trap of ex post facto analysis" and provide
no technical information to enable the examiner to determine what would
have been obvious to the person skilled in the art will not adequately
address the objection.

### 18.63.3 {#ref18-63-3}

The application may formally contain only one set of claims at any one
time. Applicants sometimes file auxiliary sets of claims, which are
alternative claims filed in addition to the main set of claims, for
consideration in the event that the examiner has objections to the main
claim set. An examiner is under no obligation to consider such auxiliary
claims, but may be prepared to consider one or a small number of
auxiliary claim sets if the examiner considers that they help the
efficient processing of the application towards grant. It should be
borne in mind that these auxiliary claims do not overcome any objections
until the amendment is formally effected. Where an applicant suggests
alternative claim wording without filing formal replacement pages,
amendment will only be effected when replacement pages are formally
filed. Where the applicant provides clear instructions informing the
examiner that an auxiliary set of claims, in the form of replacement
pages, should be treated as formally filed if the main claim set is
found objectionable, the examiner may make the amendment to incorporate
these claims without additional replacement pages being filed, but only
after they have confirmed this course of action with the applicant or
agent eg by telephone. The examiner should record the outcome of any
such telephone conversation with the applicant or agent ([see
18.74-18.78](#ref18-74)). Where more than one claim set is filed close
to the compliance date but not examined until afterwards and the main
claim set is objectionable, the application will not meet the
requirements of the Act, since it is not possible to effect the
amendment once the compliance period has expired [(see Fisher Rosemount
Systems' Application BL
O/238/12)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F238%2F12&submit=Go+%BB){rel="external"}).
In this situation the examiner will only be able to consider such
auxiliary claims if Patents Form 52 is filed to extend the compliance
date.

### 18.64 {#ref18-64}

The examiner should aim to consider a response to a s.18(3) report
within two months of its being filed. That is, within that time, the
examiner should either issue a further report under s.18(3) (or other
correspondence concerning outstanding matters), or, send the application
forward to grant. If a response is filed close to the end of the
compliance period (i.e. with less than four months to go until the end
of the compliance period), then the examiner should aim to consider a
response to a s.18(3) report, within half the time remaining until the
end of the compliance period. For example, if (when the applicant's
response is received in the office) two months remain until the
compliance period ends, then the examiner should aim to re-examine the
application within one month. In technical areas where customer demand
is high it may not be possible to meet these targets, in which case
examiners should give priority to the re-examination of applications
which are nearest the end of their compliance period.

\[ Where an application has undergone accelerated examination or
accelerated CSE, any amendments received by the latest date for response
should be processed as a matter of urgency, and at the latest within one
month of receipt. Such amendments should be sent to the examiner as an
'URGENT AMENDMENT' on PDAX, with the high priority marker set. Where the
as-of-right extension to the latest date for response has been requested
and amendments for an accelerated examination or CSE are received during
the extended period for filing a response (or later) it will be assumed
that the applicant is no longer interested in expediting the prosecution
of their application. In this circumstance amendments will be processed
within the usual unaccelerated timescales.\]

Where, prior to receiving the response to the first s.18(3) report on
the application, the substantive examiner is made aware by the applicant
of any document cited in a search report for a patent application on the
same invention made in another patent office that has not previously
been considered, the document should be considered by the examiner as
soon as possible after it has been received unless the response to the
s.18(3) report is due imminently. An amended s.18(3) report should be
issued if the document would give rise to a further objection. Even when
the response is received before s.16 publication, the formalities
examiner should refer the file to the appropriate examining group.

\[Deleted\]

Whether before or after s.16 publication, if the substantive examiner
finds that the objections have not been met or that there are further
objections, they should write a further report to be issued to the
applicant [(but see 18.73)](#ref18-73).

Consideration should be given as to whether the search previously made
is adequate, and so whether any necessary supplementary search should be
performed ([see
17.115-17.117](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-115)).
The examiner should also consider checking to see if any further useful
equivalent cases have come to light since any previous check, and
consider checking the status of these (and any previously identified
equivalents) using online file inspection ([see
18.10.3-18.10.4](#ref18-10-3)).

If any amendment or correction made by the applicant necessitates a
supplementary search, an additional search fee may be payable under
s.17(8) and s.18(1A) before substantive examination proceeds any
further, see [17.120 to
123](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-120)
and [18.03 to 18.03.4](#ref18-03) If, either at this stage or after a
subsequent action, there are no outstanding objections the application
should be sent for grant provided that the "3 month rule" is satisfied
in appropriate circumstances (see [18.07.2](#ref18-07-2),
[18.85-18.86](#ref18-85)).

\[During the 5-week period of the publication cycle, a COPS is locked
and so won't accept any changes to bibliographic data or processing
status. However, an examination report can still be created and
signed-off during this period; the checklist should set a COPS
processing status of "Do not change processing status". If there are any
additional citations, these should be mentioned in the examination
report, but any copies that are sent with the report will need to be
manually ordered since an internal top-up search report cannot be
generated; the checklist should also alert the ESO to the new citations.
If this situation arises the case should then be diarised to a date
after publication and any additional citations or fields of search
entered directly to Prose and an COPS update then performed.\]

### 18.65 {#ref18-65}

When the applicant has restricted their claims to meet an objection to
lack of novelty or of inventive step then, for the purpose of
determining whether the objection has been met, the new claims must be
looked at in the same way as they would have been if the specification
had originally been filed with those claims. It is not sufficient for
the applicant to add to their anticipated claim a feature disclosed in
an example in the specification but which is no more than one of the
alternative ways which the person skilled in the art would regard as
available to them for carrying out the known concept originally claimed.

### 18.66 {#section-30}

s.125(1) is also relevant

If the applicant argues that there is a distinction between their
invention and the prior art, the substantive examiner should make sure
that this distinction is reflected in the claims. If anything falling
within the scope of a claim is known or obvious, that claim is bad.

### 18.67 {#ref18-67}

s.2(2) is also relevant

An objection of lack of novelty or of inventive step based, wholly or in
part, on a statement of prior art in the description should not be
withdrawn solely because the applicant amends or deletes the statement,
unless an adequate reason is given. For example if an applicant who has
stated that a particular practice is known in the art now wishes to
amend or withdraw that statement because they assert that the practice
was not publicly known and thus did not form part of the state of the
art, then this assertion must be categorical and unambiguous and
supported by evidence.

### 18.68 {#ref18-68}

s.125(1) is also relevant

Objection should not be raised when there are embodiments or statements
in the description which are inconsistent with the claims, unless the
inconsistency causes genuine difficulty in determining the scope of the
claims. If an embodiment is clearly outside the scope of the claims then
a skilled person, reading the claims in the light of the description,
would remain in no doubt as to the scope of the claims and so no
objection should be made. If, however, it is difficult to determine
whether an embodiment or statement is within the scope of the claims
then an objection should be made. This objection may be to the clarity
of the amended claim, the portion of the description or both. The title
should be consistent with the invention claimed. See
[14.144](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-144).

### 18.69 {#ref18-69}

s.14(5)(c) and s.76(2 is also relevant

Consideration should be given as to whether claims which have been
amended are supported by the description ([see
14.142-14.156](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-142))
and whether they entail added subject-matter ([see 76.04 to
76.23](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-04)).
Applicants may seek to restrict the claims of an application having a
proportion or property expressed as a range of values to a sub-range
within the original range. (The same situation arises where the original
range is in a parent application and the sub-range in a divisional). In
assessing amendments which restrict the scope of independent claims,
examiners should be aware of the potentially very serious consequences
for the applicant should such amendments be held -- post-grant -- to add
matter. In this situation s.76(3)(b) would prevent amendment to remove
the additional matter, as this would extend the protection conferred by
the patent; this is potentially an "inescapable trap" (G 01/93 Advanced
Semiconductor Products, OJEPO 8/94 -- [see
76.27](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-27)).

### 18.69.1 {#ref18-69-1}

There have been various approaches to assessing whether a restricting
amendment to a claim is supported or adds matter. At one extreme is the
view that the mention of a range is the implicit disclosure of every
item falling within the range, and accordingly any sub-range can be
claimed if the requirements of Section 1(1) are met since there would be
support for it. At the other extreme is the view that no sub-range can
be claimed unless there was mention in the application as filed of the
values concerned as end points of a preferred range. Pending guidance
from the courts for practice under the 1977 Act, examiners should not
adopt either extreme as an automatic rule of thumb approach suitable for
all cases but should decide each case on its own merits by considering
whether the sub-range claimed is supported by the description and does
not extend the matter disclosed. The view point should be that of a
skilled person reading the document. This will involve considering what
is said about the values; it is not a question of just looking for any
mention of a value but rather of assessing the teaching of the document
about it. It will be necessary to consider the original disclosure
closely to ascertain whether there is a sufficient disclosure to support
values within the sub-range e.g. whether there is reference explicit or
implicit to the special advantages obtained when values are selected in
the sub-range. The above guidance only applies to considerations of
support for an amended claim (or claim of a divisional application). The
requirements of novelty and inventive step will also have to be met. The
very nature of this approach is such that detailed rules to be rigidly
applied in all circumstances cannot be given. Advice should therefore be
freely sought from the relevant DD or head of examining group. In
exceptional cases advice may also be sought from Legal Section.

### 18.70 {#ref18-70}

s.14(5)(d) is also relevant

Objection will have to be raised if, as a result of amendment, the
claims lack unity of invention.

### 18.71 {#section-31}

s.19(2) is also relevant

If, after objection has been raised, the applicant refuses or omits to
acknowledge a Registered Trade Mark ([see
14.97](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-97)),
the substantive examiner should amend the specification to acknowledge
the Mark and inform the applicant that they have done so ([see
19.24](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-24)).
If however the use of the Trade Mark is objectionable ([see
14.137](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-137))
the objection should be maintained.

\[For procedure, [see
19.24](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-24).\]

### 18.72 {#section-32}

The process of examination and re-examination in response to the agent's
or applicant's replies continues until the substantive examiner is
satisfied that the application complies with all requirements of the
Act, or that a point is reached where disagreement between the applicant
and the examiner is such that a hearing must be appointed and the matter
resolved, or the application is withdrawn or refused. In the usual
event, where the substantive examiner (or Deputy Director after a
hearing) is satisfied that no further objections are outstanding, the
grant of a patent follows automatically [(see 18.85-18.86](#ref18-85)).

\[If an amendment or argument is made that means that an objection is no
longer relevant, the examiner should add a minute to the PDAX dossier
explaining why the objection has been dropped.\]

### 18.72.1 {#ref18-72-1}

CoP is also relevant

Letters filed close to the end of the compliance period should be marked
prominently "urgent - compliance period expires on (date)"

\[To identify applications near to the end of the normal compliance
period, a warning label will be displayed on the dossier cover of
applications with four months or less to the end of the compliance
period, and the appropriate PDAX message should be sent to the
substantive examiner.

\[Deleted\]

\[If a substantive examiner requires urgent action on a file not covered
by an Urgent label, then the substantive examiner should ensure that any
PDAX message sent has the appropriate urgent warning at the beginning of
the message.

\[Deleted\]

\[It also sometimes occurs when time is short that a letter is filed
direct with the substantive examiner by the applicant or agent. If the
letter is accompanied by a copy thereof and it is requested that the
copy be endorsed and returned to serve as a receipt, the substantive
examiner should glance at the two letters to check that they are the
same and then endorse the copy by hand before returning it. (Some agents
have adopted the practice of filing copies of letters for use as
receipts; they are normally stamped and returned.) In some instances
where letters are not accompanied by a copy, the agent or applicant
merely asks in the letter for its receipt to be acknowledged. This is
normally done automatically by the formalities examiner and the issue of
the receipt is indicated by a hand-written number at the bottom
right-hand corner of the letter. If this action should ever have been
omitted, the substantive examiner should ask the appropriate formalities
group to arrange for it to be done. Where such a letter is filed direct
with the substantive examiner, they may instead make a photocopy for
endorsement by hand as above. The substantive examiner can send the
letter to formalities who should forward it to Index and Scanning for
incorporation into the dossier.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Interviews and telephone conversations]{#default-id-6d9c2c45-heading-25 .govuk-accordion__section-button} {#interviews-and-telephone-conversations .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Interviews and telephone conversations\",\"index_section\":25,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-25 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-25" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Interviews and telephone conversations\",\"index_section\":25,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.73 {#ref18-73}

Any objections at all which need to be raised at the first substantive
examination should be set out in writing (except where the only
objection is that a Trade Mark has not been acknowledged - see paragraph
18.82). When an objection needs to be raised in a subsequent action
however, the examiner may attempt to resolve the matter either over the
telephone or at an interview. Recourse to the telephone should
particularly be considered if there is something clearly amiss with the
application (e.g. the wrong pages have been filed), the applicant or
agent has particularly welcomed telephone contact, or it seems likely
from the written exchanges that the examiner and the applicant or agent
may be at cross-purposes. A telephone or interview report which sets out
the remaining objections to grant of the patent application, and
specifies a period for response, can itself constitute a report under
section 18(3); there is no requirement to create a separate s.18(3)
report and covering letter. If the telephone or interview report is
intended to be a report under s.18(3) then for the avoidance of doubt
this should be explicitly stated in the report.

\[In the case of a private applicant, an interview may be suggested
using ELC19PA.\]

\[Anyone is entitled under s.22 of the Welsh Language Act 1993 to have
legal proceedings held in Wales conducted in Welsh without advance
notice. If the applicant or their agent insists on conducting an
interview in Welsh the substantive examiner should obtain the services
of an interpreter. ARD hold a list of Office personnel willing and able
to act as interpreters in Welsh.\]

### 18.74 {#ref18-74}

r.51 is also relevant

A record should be made of anything said or agreed during an interview
or telephone discussion which might materially affect or throw light on
the prosecution of the application, and should ideally be brief. It
should not contain non-essential information, or comments which the
applicant or the agent wish to make off the record and which are not
germane to the conclusion reached. The record will be open to public
inspection and should, together with the other papers on the open part
of the file, present a continuous narrative of the proceedings. It
should state the outcome of the discussion - for example, that a
particular amended form of claim was accepted. If thought appropriate
the reasons should be briefly indicated - for instance if it has been
agreed that a particular form of claim is distinguished from the cited
prior art, but it is not self evident why this should be so. When an
objection is withdrawn as a result of fresh or further argument put
forward in the discussion, the record should briefly outline the
argument. If the applicant or agent puts forward an argument but
indicates that they do not wish it to be made public, they must be told
that it must either be recorded or disregarded. Amendments or arguments
put forward during the discussion which are subsequently withdrawn or
become irrelevant during the course of the same discussion need not be
recorded.

\[Comment by the agent which might be interpreted as critical of the
applicant (or vice versa), or anything critical of a third party, should
not be recorded. Time should not be spent in trying to obtain an agreed
record of the Discussion.\]

### 18.75 {#section-33}

Any document cited and withdrawn during the course of a single
discussion should nevertheless be recorded [(see 18.85)](#ref18-85)
unless the discussion revealed its citation to have been misconceived,
eg based on a misunderstanding.

### 18.76 {#section-34}

As discussed above [(18.73)](#ref18-73), a telephone or interview report
can itself constitute a report under section 18(3). Whether or not this
is the case, if further action has been agreed at the end of the
discussion, for example if the agent is to seek the applicant's
agreement to an amended claim, or is to make consequential amendments to
the description, or if the substantive examiner has promised to put an
argument in writing, this should be made clear in the record, as should
any time limit set.

### 18.77 {#section-35}

As a general rule a copy of the record should be sent to the applicant
or agent with whom the discussion was held, who should be informed that
any disagreement they may have should be communicated in writing.

\[It should be apparent from the open part of the file whether or not a
copy of the record has been sent to the applicant. In any event a
reference to the record should be entered as a minute on the dossier.
The substantive examiner should adopt whatever method appears most
suitable for communicating a record of an interview or telephone
conversation to the agent or applicant. In most cases, this will be a
report of the telephone conversation (quoting the agent's file reference
and addressed to the applicant, c/o the agent in the usual way). However
other procedures may sometimes be appropriate; for example a paragraph
indicating that a requested amendment has been made could be added to a
letter or report. Exceptionally in the case of interviews, the need to
communicate a record may be avoided by obtaining the signature of the
agent or applicant to a minute of the interview which is then placed on
the open part of the file.\]

\[If, as in most cases, the examiner chooses to produce a report of the
telephone conversation, they should use a TELREP document. The examiner
should set a reply date if necessary. If the applicant does not require
a copy of the report sent to them, the examiner should select the "No
copy to agent" option. The appropriate checklist should be used to
accompany the TELEREP: either TELMIN if there is no copy to be sent to
the applicant or TELLET if a copy is to be sent. Reports not sent to the
applicant are still available to view online via IPSUM once an
application has been published.\]

### 18.78 {#section-36}

Any doubt about whether to make a record of a discussion should be
resolved in favour of making the record.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Failure to reach agreement]{#default-id-6d9c2c45-heading-26 .govuk-accordion__section-button} {#failure-to-reach-agreement .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Failure to reach agreement\",\"index_section\":26,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-26 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-26" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Failure to reach agreement\",\"index_section\":26,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.79 {#ref18-79}

s.101 is also relevant

Where the substantive examiner is unable to obtain a satisfactory
response, an offer of a hearing should be made. Under most
circumstances, an offer should not be made until an objection is
repeated, or the application is close to the end of the compliance
period with unresolved matters outstanding. Where multiple unresolved
matters are outstanding, it is permissible for examiners (in
consultation with their group head) to offer a hearing where an impasse
has been reached on a single major substantive issue only. Note that it
is desirable for a hearing to take place before the compliance period
ends, see the considerations in the square bracket below. Where the
compliance period has been extended under r.108(2) and a response filed
at or very near the end of the extended period fails to overcome the
outstanding objections, it may be necessary to issue EL5 ([see
20.06](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-06)).
However, for applications relating to methods of doing business with
major patentability objections, a hearing may be offered upon receiving
the first response from the applicant if the examiner remains of the
opinion that there is no prospect of a patent being granted. Since the
comptroller must give to a party to any proceeding before them an
opportunity to be heard before exercising adversely any discretion, the
offer of a hearing is a prerequisite to refusing an application under
s.18(3). If no reply is received to the offer the application may be
allowed to lapse at the compliance date ([see
20.03](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-03)).
(For hearings, [see
101.02-101.07](/guidance/manual-of-patent-practice-mopp/section-101-exercise-of-comptroller-s-discretionary-powers/#ref101-02)
Chapter 4 of the [Tribunal Patents
Manual](https://www.gov.uk/government/publications/tribunal-patents-manual)
and Chapter 6 of the [Patent Hearings
Manual](https://www.gov.uk/government/publications/hearings-manual).

\[Before offering a hearing, the examiner should consider the following
factors: (i) the length of time remaining until the compliance date;
(ii) the number of times an objection has been made; (iii) the total
number of s.18(3) reports issued; (iv) whether genuine progress is being
made towards grant; and (v) whether there is a saving amendment.\]

\[Examiners should consult their group head before offering a hearing.
Normally the offer will be made in writing, if time permits, by issuing
EL35 as the covering letter with the exam report and/or including a
paragraph in the exam report. Where the matter is urgent however the
offer should first be made by telephone and then confirmed in writing
unless the applicant or agent agrees to the immediate appointment of a
hearing. The applicant should be informed that if they choose to respond
to the offer of a hearing with further amendments or arguments but do
not request to be heard then their application may be passed to a
Hearing Officer for a decision on the papers [(see 18.80)](#ref18-80). A
date for response should be set.\]

\[The examiner should include in the checklist accompanying the letter
instructions to the ESO to include a hearings guidance leaflet when
issuing the letter if the hearing is for an unrepresented applicant. A
link to an [online
copy](https://www.gov.uk/government/publications/ex-parte-hearing-procedures)
of the leaflet can be included in the letter if appropriate.\]

### 18.79.1 {#section-37}

After the offer of a hearing has been accepted, the examiner should
issue a final communication to the applicant, otherwise known as a
pre-hearing report. This report should define all the issues to be
considered at the hearing and should introduce no new objections. All
the relevant arguments for each issue should be set out and any
precedent cases should be referred to. It must also adequately brief the
hearing officer and any assistant on the case without the need for any
further report. The report should therefore raise all the outstanding
issues in sufficient detail for the hearing officer to fully appreciate
what is in contention, what needs to be decided and any consequential
matters. Any outstanding matters or actions that have been deferred and
will need to be addressed after the hearing if the application is not
refused should be mentioned in this communication.

\[As well as the content noted above, the pre-hearing report should
include full analysis using the appropriate legal tests, a summary of
the applicant's arguments, how the claim has been construed, any issues
that have been deferred (i.e. whether the search and/or exam is
complete) and any details of the compliance date and extensions if
relevant. If all of the issues have been set out succinctly in the last
substantive examination report then the examiner may consider it
appropriate for the pre-hearing report to simply refer back to this
examination report\]

\[If the applicant requests to be heard the examiner should arrange the
hearing by creating a minute addressed to Litigation Section requesting
that a hearing be appointed and sending a PDAX message to the Hearings
team mailbox entitled "Please see hearing request". Where less than two
months of the unextended compliance period remain, the minute should
instruct the hearings clerk to arrange the hearing as soon as
possible.\]

### 18.80 {#ref18-80}

An applicant, instead of taking up an offer of a hearing may seek to
continue filing observations or amendments which the examiner considers
do not satisfactorily meet the objections. If this occurs near to the
end of the compliance period the examiner may consider it most efficient
to respond with a further report under s.18(3). The accompanying exam
letter should reiterate the offer of a hearing and include appropriate
reminders of the compliance date as detailed in [18.47](#ref18-47). Exam
letter EL5 may be used if the report is issued after the end of the
extended compliance period (see 20.06). If the compliance period has
passed, but has not been extended, an EL5 letter (modified as
appropriate) may be used to tell the applicant that the application will
be treated as having been refused unless they submit observations and/or
requests a hearing to demonstrate that the application was in fact in
order (or requests an extension of the compliance period under r.108).

However, where the offer of a hearing was made early in the examination
process and no saving amendment appears possible, it is desirable to
avoid multiple unproductive amendment rounds which do not progress the
case towards grant. In such instances the applicant should be told in a
letter that the application does not meet the requirements of s.18(3)
and that a formal decision as to whether the application should be
refused will be issued unless a hearing is requested or the application
is withdrawn. The applicant should be informed that the application will
be passed to a Hearing Officer for consideration two weeks after the
date of the letter. The letter should also define all the issues to be
considered in the Hearing Officer's decision, setting out the relevant
arguments on each one. It must also adequately brief the hearing officer
and any assistant on the case without the need for any further
pre-hearing report.

\[In the case where the offer of a hearing was made early in the
examination process, EL36 can be used to notify the applicant that a
formal decision will be taken unless they withdraw the application or
request a hearing. The EL36 should be edited to define all the issues to
be considered, as set out above.\]

\[Once two weeks have passed since the date of the letter, the examiner
should create a minute addressed to Litigation Section requesting that
the application be forwarded to a Hearing Officer for a decision on the
papers and send a PDAX message to the HEARINGS team mailbox entitled
"Please see decision request". \]

### 18.80.1 {#ref18-80-1}

If the offer of a hearing is refused and no further amendment or
argument is provided the response cannot be considered an attempt to
advance the case. The applicant should therefore be warned in writing
that refusal is contemplated unless the outstanding matters are
addressed within two months of the expiry of the period for reply
originally specified (or the compliance date if that is sooner). If
further arguments or amendments are filed in response to this letter and
the applicant continues to decline the offer of a hearing, the procedure
set out in [18.80](#ref18-80) should be followed.

\[EL37 should be used in this situation.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Application in order]{#default-id-6d9c2c45-heading-27 .govuk-accordion__section-button} {#application-in-order .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Application in order\",\"index_section\":27,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-27 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-27" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Application in order\",\"index_section\":27,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 18(4)**
  If the examiner reports that the application, whether as originally filed or as amended in pursuance of section 15A above, this section or section 19 below, complies with those requirements at any time before the end of the prescribed period, the comptroller shall notify the applicant of that fact and, subject to subsection (5) and sections 19 and 22 below and on payment within the prescribed period of any fee prescribed for the grant, grant him a patent.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 18.81 {#ref18-81}

r.31(4)(a), r.19(3)(a) r.108(1) and r.30A are also relevant

Provided that at least a full search under s.2(2) has been made for
documents published before the priority date of the invention and that
an adequate opportunity has been given for s.21 observations [(see
18.07.2](#ref18-07-2)) for discussion of the exceptions to the "3-month
rule"), when it is found at the first substantive examination that the
application, whether as filed or as subsequently amended, complies with
the requirements of the Act and Rules then the applicant must be
informed of this fact. In the same report they are informed that
voluntary amendments and/or a divisional application may be made within
two months of the date of the letter. This letter acts as a notification
of intention to grant and indicates that the application will be sent
for grant shortly after the two-month period has expired. No divisional
application or voluntary amendments can be made once the application has
been granted (and, in addition, after the two-month period has expired
voluntary amendments can only be made with the consent of the
comptroller). The application is not normally sent for grant until the
two month period has expired [(see 18.84)](#ref18-84). However, if in
response to the report the applicant confirms in writing that they wish
to waive the opportunity under rr.19 and 31(4)(a) to file any divisional
applications or amendments, the pre-grant procedure ([see
18.85-86](#ref18-85)) instead commences forthwith. The facility for
expediting grant by waiving the two month period does not extend to
late-filed (eg divisional) applications where grant is necessarily
delayed until after s.16 publication. It is not possible for the
applicant to request a delay to grant.

\[If the application is found to be in order at first examination, the
substantive examiner should carry out the pre-grant steps set out in
18.86 (including completing a grant form) and send a PDAX message with
the text "ISSUE RELEVANT INTENTION TO GRANT LETTER" to the EA GRANT
mailbox, including as a recipient the Examiner Assistant for their
examining group. If the examining group does not have an Examiner
Assistant then the recipient field should be left blank. On receiving
this message, the EA should consider the grant form when issuing an EL3
or EL3F (if excess fees are due).\]

\[Where amendments have been filed, they should be acknowledged.\]

\[ELC4 should be added when a first s.18 examination report is issued
more than 3 years and 6 months after the earliest date; PROSE will add
the ELC4 letter clause during creation of the EL3 or EL7 letter if the
relevant checkbox is ticked, as discussed in [18.47](#ref18-47).
Similarly, ELC5 should be added to a first examination report on a
divisional application when the first report on the parent application
was/is issued later than 3 years 6 months from the parent's earliest
date. The use of these clauses will ensure that the compliance date is
set correctly on COPS and will also inform the applicant of the
compliance date ([see
20.02](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)).\]

\[In the situation where a top-up search is carried out at the same time
as finding the application in order see
[17.115](/guidance/manual-of-patent-practice-mopp/section-17-search#ref17-115).\]

### 18.82 {#ref18-82}

s.19(2) is also relevant

Where the only objection outstanding at the first examination is that a
Registered Trade Mark, while allowable, is unacknowledged, the
substantive examiner should amend the specification to acknowledge the
Trade Mark, inform the applicant that they have done so and (for
non-combined search and examination cases) issue a report as in
paragraph [18.81](#ref18-81).

\[The procedure for both non-CS&E and CS&E cases is set out in
[19.24-19.26](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-24).\]

### 18.83 {#section-38}

If amendments are filed within the prescribed two-month period the
amended specification should be re-examined in the same way as a
specification amended after a report under s.18(3). If as a result of
the amendments the specification no longer complies with the Act and
Rules a report under s.18(3) should be issued, specifying four months
for reply provided sufficient of the compliance period remains. For the
procedure to be followed when amendments are filed, [see
19.18](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-18).

### 18.84 {#ref18-84}

r.108(1) is also relevant

If after the end of the two month period the application has not been
amended, or, if amended, it is still in order, it should be sent for
grant [(see 18.85-18.86.3)](#ref18-85). The two-month period is
prescribed by 31(4)(a) and therefore cannot be automatically extended by
two months under s.117B(3), as that section applies only to periods
specified by the comptroller. However, a request for a discretionary
extension of the two month period may be made under r.108(1) and should
be allowed if accompanied by an adequate reason. This request may be
filed retrospectively. The criterion applied in determining the adequacy
of the reason should be the same as that applied when considering a
request for an extension of the period specified for response to a
report under s.18(3) [(see 18.56-18.57](#ref18-56)). Where a convincing
reason is not provided, or the reason relates to the applicant's desire
for more time to file a divisional application, the extension should be
refused. If such an extension is allowed, grant should be delayed (see
also
[123.36.8](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36-8)
and
[123.37](/guidance/manual-of-patent-practice-mopp/section-123-rules#123-37)).

### 18.84.1 {#ref18-84-1-18-84-3}

The substantive examiner should defer issuing an examination report
under s.18(4) if they find no objections to the application when the
first substantive examination is made before the search under s.2(2) has
been completed for documents published before the priority date of the
application and before an adequate opportunity has been given after s.16
publication for observations under s.21 (see 18.07.2 for situations
where the three month period after publication can be waived). This is
most likely to occur when combined search and examination has been
requested, and the first substantive examination reveals no objections.
The applicant should be informed that while the initial stage of the
s.18 investigation has not revealed any objections, the investigation
cannot be completed and a report under s.18 cannot issue until three
months after publication, when the examiner will have considered the
outcome of a supplementary search under s.17(7) and any third party
observations filed under s.21. The application should not be sent to
grant until after a report under s.18 has issued ([see
18.84.2](#ref18-84-2)). The substantive examiner should diary the file
for return at an estimated date three months after s.16 publication,
using the PD electronic diary. A diary action should also be added to
the PDAX dossier.

\[When no objections are raised at the first examination stage following
combined search and examination, the search report should be issued with
SE3. SE3 doesnot constitute a report to the comptroller or the applicant
under s.18 and does not set any period for response. Any supplementary
report on the search should be added to SE3 under an appropriate
sub-heading but before the passage about publication of the application.
\]

### 18.84.2 {#ref18-84-2}

On return of the file under the diary system the substantive examiner
should update the search and consider any voluntary amendments made by
the applicant and any third party observations. If no objections are
found at this time, a first substantive report under section 18(4)
should be issued following the procedure set out in
[18.81-18.84](#ref18-81). On no account should the application simply be
sent for grant.

### 18.84.3 {#section-39}

On the other hand if there are one or more objections, for example as a
result of amendments or the top-up search, a first substantive report
under s.18(3) should be issued following the procedure set out in
[18.47](#ref18-47) [18.80](#ref18-80), although the period set for reply
to the first or any subsequent reports should not expire earlier than
two years from the priority date, or if none, the filing date. The date
for completing the first substantive examination report may be deferred
at the applicant's request, without a reason being necessary, to
approximately the date on which other applications, which are of the
same date, normally become due for first examination under s.18.

### Final procedure

### 18.85 {#ref18-85}

As soon as there are no objections outstanding, the search is complete
in respect of s.2(2) art and third parties have had an adequate
opportunity to file observations under s.21, the substantive examiner
should ensure that the recorded bibliographic data (classification,
cited documents, field of search) required for publication of the
granted specification are correct and up to date. If less than 21 months
have elapsed from the priority date or the substantive examiner is not
otherwise satisfied that the search is complete in respect of s.2(3)
art, the application should be diarised for return after grant in order
to complete this search and the applicant should be informed accordingly
([see
17.118](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-118)).
An entry should be created in the PD electronic diary system, and the
substantive examiner should remember to add the diary action to the
dossier in PDAX.

\[There is no need for examiners to defer grant procedure merely to
allow the applicant time to file a divisional application; the
appropriate notification of intention to grant letter will provide the
applicant with at least one month's advance notice of grant, during
which time they may file a divisional ([see
15.46](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-46)).
Note that the date given in the EL3/EL34 intention grant letters are not
prescribed or specified periods and are therefore not subject to
extension under r.108 or r.109 respectively.\]

\[Before marking an application in order for grant, the examiner should
check if any further equivalent applications have become available, and
consider checking the status of these (and any previously identified
equivalents) using online file inspection ([see
18.10.3-18.10.4](#ref18-10-3)).

\[Except as noted in [18.07.2](#ref18-07-2) no application should be
marked in order for grant until three months after s.16 publication so
as to allow the opportunity for s.21 observations and a top-up search.
If during this period the substantive examiner is content that previous
objections have been resolved and no new ones arise, they should add a
minute to the dossier accordingly, and create an entry in the PD
electronic diary for return of the file at an estimated date three
months after publication so that the search can be updated. A diary
action should also be added to the PDAX dossier. If the diary entry is
for more than one month from the date of receipt of the last response by
the applicant, the examiner should also issue EL32 to acknowledge the
response. Any third party observations filed in the intervening period
should be copied to the applicant and considered by the examiner without
delay.

\[For updates to classification symbols, see [18.86](#ref18-86). Any
document cited (formally or otherwise) or new field searched which was
not listed on the A document should be recorded on Prose as soon as
possible, for eventual inclusion on the front page of the B document.
For procedure when action deferred to await filing of a divisional
application, [see
15.46](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-46).

### 18.86 {#ref18-86}

s.14(9) and S.19 is also relevant

The examiner should then complete the necessary pre-grant steps before
marking an application in order for grant.

\[ When the substantive examiner is satisfied that there are no
objections outstanding and that the application may go forward to grant,
they should create a grant form in PROSE. The associated checklist
should also be created (unless a top-up search is carried out at the
same time as finding the application in order, in which case see
[17.115](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-115)).
The grant form acts as an instruction from the examiner to the examiner
assistant to issue the relevant intention to grant letter (EL3 or EL34,
see [18.81](#ref18-81) and [18.86.2](#ref18-86-2)). It is the
responsibility of the examiner putting the case forward to grant to
ensure that the content of the EL3 or EL34 is suitable for, and tailored
to, the individual circumstances of the case being granted. This can be
done by using the appropriate tick-box options present on the form
and/or providing free-text for the EAs to insert into the EL3 or EL34
where appropriate. For situations in which intention to grant letters
may need to be amended see
[17.115](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-115),
[17.118](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-118)
and
[73.07](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-07) -
note that examiners should only use clause RC31 in intention to grant
letters to bring to the attention of applicants s.73(2) conflict that
has not previously been raised in a s.18(3) examination report. The
examiner can also use the grant form to explain why major objections
were dropped, if this has not been recorded elsewhere e.g. in a minute
on the PDAX dossier. The grant form can also be used to record other
file note information for the benefit of examiner assistants e.g.
waivers to s.21 periods for divisional applications on the grant form.
(The abstract is not published as part of the granted patent; therefore
no revision or amendment of the abstract by the substantive examiner is
necessary at this stage. Moreover if amendment to the abstract is filed
no action should be taken except as referred to in
[14.191](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-191)).\]

The examiner should enter the grant title and update the field of
search, classification and citations in Prose. The full data for COPS
that has to be entered (or updated) on Prose to allow the case to
proceed to grant and 'B' publication is:

-   grant title
-   applicant's reference
-   field of Search
-   citations
-   IPC and CPC classification

\[ All IPC and CPC classification symbols should be updated to ensure
they are current and appropriate at B stage, due to possible changes to
classification schemes and/or amendments giving rise to changes in the
identified invention information. Where necessary, such updates may
include the entry of new classification symbols or a decision not to
apply a previously-applied symbol at B stage.\]

\[ ClassTool should be used to update/enter IPC and CPC classification
data.\]

\[Field of search and citations might already have been entered into
PROSE and COPS, e.g. if an ISR-TOP has been created and signed off on
PROSE previously. To record that there are no additional fields of
search, the examiner should select the 'Other' field of search tab on
PROSE, double-click in the grey space and select 'Record no additional
fields for B'.\]

\[ The title from the first page of the latest version of the
specification should be entered into the Grant title field in the
Details area on Prose. The applicant's reference field should also be
corrected if it is different from that in the most recent correspondence
from the applicant.\]

\[ The examiner should ensure that the 5 data markers have been
activated at the bottom right of the Prose screen.\]

\[ It is the substantive examiner's responsibility to see that the
documents going forward for grant have been properly assembled. The
specification sent to Publishing Section should contain only those pages
which are to form part of the published B document, the Formalities
Examiner having placed a "P" against the documents ready for B
publication and marked them each as "Working Copy". Any necessary
re-numbering of the pages will be carried out by the formalities
examiner before grant (see 14.34). Should formalities find any issues
with an application before it enters the grant cycle, they should stop
the application from entering the grant cycle and refer the application
back to the examiner or group head if the examiner isn't available. If
one or more, but not all, of the figures on a sheet of drawings have
been cancelled, Publishing Section will blank out the cancelled figures.
In order to ensure that this is done, the substantive examiner should
add a minute to the PDAX dossier highlighting which figures are to be
cancelled. FMLS examiners will automatically remove any information
(apart from date stamps) contained within the margins of pages being put
forward for B publication -- there is no need for examiners to minute
PDAX dossiers for this purpose.\]

\[ Once the substantive examiner is satisfied that all of the above
steps have been completed and the specification has been properly
assembled, they should create and sign-off a grant form bundle in PROSE,
and send an "ISSUE RELEVANT INTENTION TO GRANT LETTER" message to the EA
GRANT mailbox, including as recipient the Examiner Assistant for their
examining group. If the examining group does not have an Examiner
Assistant then the recipient field should be left blank.\]

\[ Where a request for amendment is filed after the case has been sent
for grant but before issue of the grant letter (also known as the
B-publication letter), action should be taken as in
[14.206](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-206)
mutatis mutandis. Grant should if possible be stopped to allow the
request to be dealt with, but if it is too late to prevent production or
issue of the grant letter it may be necessary to rescind grant. The need
to rescind grant should if possible be avoided by dealing with the
request sufficiently quickly for the granted patent (including any
amendment allowed) still to be published on the date foreshadowed in the
grant letter.\]

### 18.86.1 {#ref18-86-1}

If the application was found to be in order for grant at first
examination, and remains in order after the relevant period has passed
([see 18.84](#ref18-84)), the application should be sent for grant.

\[The examiner assistant should check if new documents have been filed,
if not they should set the COPS status to "12 - Ready for Grant" on
PROSE and forward the case onto Formalities for "Grant". If there are
new documents filed, then the examiner assistant will check with the
examiner whether the application can proceed to grant and record the
situation in a minute. However if a divisional is filed, there is no
need to withhold the parent application from proceeding to grant. If an
EL3F was issued, the examiner assistant will also check if the grant fee
has been paid.\]

If the grant fee is not paid by the deadline in the EL3F the Examiner
Assistant should issue a reminder letter to the applicant. This letter
will remind the applicant of the need to pay the grant fee and inform
them that as they have missed the deadline a Form 52, with the
appropriate fee (if any), is now required for them to do so. The 2 month
response date provided in the EL3F letter is a prescribed period set by
rule 30A of the Patents Rules for paying the grant fee. This period can
be extended by 2 months 'as of right' by filing a Form 52 with the
appropriate fee (if any). Further extensions are possible but are
subject to the comptroller's discretion. If the fee remains unpaid and
no further extensions are available the application will be terminated
by formalities once the compliance date has expired. Once terminated the
applicant may apply for reinstatement in the usual way (see section
28).\]

### 18.86.2 {#ref18-86-2}

S r.30A is also relevant

If the application is found to be in order for grant at a later
examination stage, then an intention to grant letter should be issued by
the examiner assistant.

\[In the situation where a top-up search is carried out at the same time
as finding the application in order see
[17.115](/guidance/manual-of-patent-practice-mopp/section-17-search#ref17-115).\]

\[The examiner should complete a grant form ([see 18.86](#ref18-86)) and
send an "ISSUE RELEVANT INTENTION TO GRANT LETTER" message to the EA
GRANT mailbox, including as recipient the Examiner Assistant for their
examining group. If the examining group does not have an Examiner
Assistant then the recipient field should be left blank.\]

\[The examiner assistant will consider a grant form, check whether any
grant fees are due and create either an EL34 or EL34F bundle. This will
include the intention to grant letter and checklist. They should then
send an "Intention to grant checklist" message to the ESO.\]

\[The EL34F bundle should only be created by the Examiner Assistant if
the number of excess pages or claims have increased since the filing of
the Form 9A and Form 10. If this is the case a grant fee will be
requested in the EL34F letter based on the increase in excess claims or
excess pages.\]

\[The ESO should action the checklist to issue the intention to grant
letter and update PAFS and COPS (to status 9: Disposed of -- In
Order).\]

\[The team of Examination Assistants will be provided with a list of
applications on which an EL34 or EL34F letter has issued. Applications
will appear on the EL34 list 5 weeks after the EL34 has issued, whereas
applications requiring a grant fee will appear on the EL34F list 10
weeks after the EL34F has issued, as the applicant has 2 months in which
to pay their grant fee. If no new documents have been filed since the
issue of either the EL34 or the EL34F letters, and the grant fee has
been paid in respect of the latter, the Examiner Assistant should update
COPS to status 12: Ready for Grant and send a "Grant checklist" message
to the relevant Formalities group. If new documents have been filed, the
Examination Assistant will check with the examiner whether the
application can proceed to grant and record the situation in a minute.\]

\[If the grant fee is not paid by the deadline in the EL34F the Examiner
Assistant should issue a reminder letter to the applicant. This letter
will remind the applicant of the need to pay the grant fee and inform
them that as they have missed the deadline a Form 52, with the
appropriate fee (if any), is now required for them to do so. The 2 month
response date provided in the EL34F letter is a prescribed period set by
rule 30A of the Patents Rules for paying the grant fee. This period can
be extended by 2 months 'as of right' by filing a Form 52 with the
appropriate fee (if any). Further extensions are possible but are
subject to the comptroller's discretion. If the fee remains unpaid and
no further extensions are available the application will be terminated
by formalities once the compliance date has expired. Once terminated the
applicant may apply for reinstatement in the usual way (see section
28).\]

\[The EL34 notification of intention to grant is different to the EL34F
in that it does not specify a period in which an action must be taken,
but simply notifies the applicant of the fact grant is going to take
place within a particular timescale. It follows that no as-of-right
extension is available to the date given in these letters.\]

\[If a written request is received from the applicant to send the
application to grant sooner than the date indicated in either the EL34
or EL34F (thus waiving the opportunity to file any divisional
applications or voluntary amendments) and any grant fee due has been
paid, an "Early grant request" message should be sent to the EA GRANT
mailbox. The Examiner Assistant will instigate grant action without
delay by taking the steps outlined above. This action should only be
taken where a request for accelerated grant is received after the
intention to grant letter has been issued. Requests received before the
intention to grant letter has been issued or requests to forgo the issue
of an intention to grant letter must be denied (by an Examiner
Assistant) and the applicant should be advised (by an Examiner
Assistant) that grant can be brought forward only if requested in
writing after the issue of the intention to grant letter.\]

\[If voluntary amendments are received at a later examination stage but
before an EL34 notification of intention to grant letter has been
issued, see 19.20. For situations in which voluntary amendments are
received after an intention to grant letter has been issued, or after
the compliance date has passed, see [18.88-18.89](#ref18-88) and
[19.22-19.22.1](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-22).\]

### 18.86.3 {#ref18-86-3}

A grant letter is then issued informing the applicant under s.18(4) that
the application complies with the requirements of the Act and Rules and
that a patent is therefore granted. The date of this letter is the date
of grant for all provisions of the Act prior to s.25(1); in particular,
the application may not be withdrawn or amended on or after this date
(for amendment of a granted patent, see s.27). In the same letter, the
applicant is informed of the date when notice of the grant will be
published in the Journal. This practice was confirmed by the Hearing
Officer in [ITT Industries Inc's Application \[1984\] RPC
23](http://rpc.oxfordjournals.org/content/101/3/23.abstract?sid=c4f55999-9255-49b9-9b85-1848fc67e665){rel="external"}
(see especially page 27 line 20 onwards).

### 18.86.4 {#ref18-86-4}

No application should be sent to grant without first having a
notification of intention to grant issued. This is true even for
applications which are found in order for grant near or after the
compliance date.

\[The EL34 is the notification of intention to grant letter that is most
likely to be issued near or after the compliance date. There is no need
for examiners to modify the letter purely because it is issued near or
after the compliance date as it is written such that it is correct even
if the compliance date is imminent or has passed. However there are a
variety of circumstances which may necessitate modification of EL34 --
see for example
[17.115](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-115),
[17.118](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-118),
and
[73.07](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-07)
if conflict is found at the point of issuing an intention to grant
letter.\]

\[The date in the EL34 should not be altered from its default of one
month. This can mean that the latest date for reply is after the
compliance date. This is not a problem because an application can be
sent to grant after the compliance date.\]

### 18.86.5 {#ref18-86-5}

r.108(2) and r.108(3) is also relevant

::: call-to-action
Rule 108(2) and rule 108(3) can be used to request extensions to the
compliance period after the intention to grant letter has been issued,
providing the application has not been sent to grant. Therefore it would
be good practice for the applicant to request any extension to the
compliance period before the date set out in the intention to grant
letter. Requests for discretionary extensions under r.108(3) should be
handled in the usual manner ([see
20.02](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)).
If the compliance period is extended in this way, the application will
still be sent to grant in the timescale set out in the notification of
intention to grant letter. Extending the compliance period in this way
may make it possible to file a divisional on time.
:::

### 18.87 {#ref18-87}

All applications when placed in order for grant enter the next grant
cycle within a week. During each grant cycle, which lasts five weeks,
the various grant procedures outlined in [18.86](#ref18-86) and [24.01
to
24.04](/guidance/manual-of-patent-practice-mopp/section-24-publication-and-certificate-of-grant/#ref24-01)
take place. If an applicant has requested accelerated examination and
desires grant for a particular date, and the request for the former is
accepted by the examiner assistant, an effort should be made to achieve
B -publication (see
[24.04](/guidance/manual-of-patent-practice-mopp/section-24-publication-and-certificate-of-grant/#ref24-04)
and
[25.02](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-02))
by the desired date but it will be impossible to guarantee to do so. The
applicant should be told that publication will, if possible, occur by
the requested date and should also be told if it proves impossible to do
so.

\[Any case which has undergone accelerated substantive examination,
should be identified by the appropriate label on the dossier cover. If
grant by a particular date has been requested, a PDAX message should be
sent to the substantive examiner alerting them to the request outlined
in the minute. When the case is in order, after updating the data in
PROSE but before setting the status "Ready for Grant", the examiner
should check with the publication liaison officer for their division
whether that date can be met. If it cannot, it should be noted that
there are no established Office procedures for providing "instant" grant
of a patent, or for shortening the 5week B-publication cycle. To make
the most of the B-publication cycle, Group ESOs and Examination
Assistants need to set "Ready for Grant" by the end of Thursday. DIS PRO
should be checked by the examiner on the following day to make sure the
case has been picked. If "Ready for B picklist" is still displayed,
contact PD/A3 immediately.\]

### Action after issue of a report under S.18(4) {#action-after-issue-of-a-report-under-s184}

### 18.88 {#ref18-88}

As a consequence of the terms of s.18(4), once a notification of
intention to grant has issued [(see 18.81 )](#ref18-81) and
[18.86.2](#ref18-86-2) informing the applicant that their application
complies with the requirements of the Act and Rules then, unless the
application has in the meantime been amended in such a way that it no
longer complies, a patent must be granted. Grant cannot be withheld
because prior art subsequently comes to light (e.g. because of action
within the Office, as the result of observations from a third party, or
where the applicant makes the office aware of documents cited against a
patent application for the same invention made in another patent office)
which casts doubt on patentability. [(Nokia Mobile Phones (UK) Ltd's
Application - \[1996\] RPC
733)](http://rpc.oxfordjournals.org/cgi/content/abstract/113/21/733){rel="external"}.
If there is no doubt that new prior art found by the examiner would have
given rise to a major objection the applicant should be informed of it
since they may wish to amend either before grant (if the grant letter
referred to in [18.86.3](#ref18-86-3) has not yet issued) or after
grant. Likewise any third party observations should be copied to the
applicant ([see 21.20,
21.23](/guidance/manual-of-patent-practice-mopp/section-21-observations-by-third-party-on-patentability/#ref21-20)).
The applicant should be informed that grant will be delayed for two
months (in order to give them time to decide whether to amend) unless
they request earlier grant. If the applicant chooses not to amend, the
new prior art should not be listed on the front page of the B
publication since this might suggest that it had been properly
considered and the application considered acceptable over it. If any of
the new prior art forms part of the state of the art by virtue of s.2(3)
and is citable against lack of novelty, the applicant should be warned
that action may be taken after grant ([see
73.02](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-02)).
If a divisional application is filed on the application in suit, [see
15.46](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-46).
For situations in which voluntary amendments are received after an
intention to grant letter has been issued, [see
19.22-19.22.1](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-22).

\[ Regardless of how new found prior art comes to light before grant but
after an 18(4) report has already issued, i.e. whether it is via the
examiner or a third-party, if the BLET has not yet issued the examiner
should contact the EAs to ensure that grant is delayed for two months,
inform the applicant as above (this can be done by issuing EL23D in the
case of third-party observations, or a manually edited version of EL23D
in the case of examiner late-found prior art) and diary the case to
return.\]

\[ If third-party observations are filed prior to issuance of a BLET but
too late to prevent it's issuance, then in the absence of any
irregularity of office procedure (see 18.89 below) the grant stands and
the procedure for processing third-party observations received after
grant should be followed (see 21.23 onwards).\]

### Limited circumstances in which grant or a s.18(4) report may be rescinded {#limited-circumstances-in-which-grant-or-a-s184-report-may-be-rescinded}

### 18.89 {#ref18-89}

r.107 and r.33(5) are also relevant

The legislation provides no possibility to rescind grant or a s.18(4)
report other than in circumstances where there has been an irregularity
in Office procedure falling within the terms of r.107. Requests from
applicants to rescind grant simply in order to allow the filing of
voluntary amendments or a divisional application (that was not clearly
foreshadowed) cannot therefore be acceded to if there has been no such
irregularity. Applicants should be advised of possible actions open to
them after grant, for example, amendments under section 27.
Irregularities in the grant procedure which may justify rescinding grant
may occur when:

r.33(4) is also relevant

\(i\) third party observations, which the examiner considers give rise
to a wellfounded objection, were received before issue of the intention
to grant letter or s.18(4) report, but too late to prevent its issue; or

\(ii\) a request by the applicant to withdraw the application or to file
voluntary amendments was received in the Office prior to issuing the
grant letter, but too late to prevent its issue; or

\(iii\) the examiner had explicitly agreed with the applicant that
amendment to the description could be attended to at a later date
pending the examiner's acceptance of the claims, but the case was then
inadvertently sent for grant before such amendments were made.

### 18.90 {#section-40}

If it is necessary to rescind grant or a notification of intention to
grant made under s.18(4) was made in error and is rescinded. If the
grant letter has issued ([see 18.86](#ref18-86)) it will also be
necessary to cancel the grant. A report should then be made under
s.18(3) setting out the objection and specifying two months (or six
months if the examiner's report under s.18(4) was the first report under
s.18) for reply, unless the time available makes a shorter period
appropriate; this may be increased to four months if major amendment is
required and sufficient of the compliance period remains. If s.21
observations were filed near to the end of the compliance period, that
period may have been extended as described in
[20.02.1](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02-1).

\[Cancellation of the grant may only be initiated by contacting the
publication liaison officer, who will call a meeting of all the
interested parties ([see
14.206](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-206)).
The letter rescinding the grant letter and/or report made under s.18(4)
should identify the writer and provide a contact telephone number. A
fresh report should be made for communication to the applicant. When the
remaining time is so short as to require urgent action, the applicant
should if possible be given forewarning by telephone to provide
opportunity for the filing of Patents Form 52.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Conflict]{#default-id-6d9c2c45-heading-28 .govuk-accordion__section-button} {#conflict .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Conflict\",\"index_section\":28,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-28 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-28" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Conflict\",\"index_section\":28,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 18(5)**
  Where two or more applications for a patent for the same invention having the same priority date are filed by the same applicant or his successor in title, the comptroller may on that ground refuse to grant a patent in pursuance of more than one of the applications.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 18.91 {#ref18-91}

s.130(1), s.89(1), s.78(2) and s.73(2) are also relevant

For this subsection to have effect the conflicting applications must be
applications for patents under this Act, including international
applications designating the UK which have entered the national phase as
domestic applications under the provisions of s.89. It does not apply to
conflict with an application for a European patent (UK), since this is
not treated as an application under the Act for the purposes of s.18(5);
such conflict can only be dealt with under s.73(2) when each of the
relevant patents have been granted ([see
73.05-73.09](/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-05)).

### 18.92 {#section-41}

The stricture of s.18(5) applies not only when the conflicting
applications have identical applicants or groups of applicants, but also
when the applications have an applicant in common, or when an applicant
in one case derives their right to be granted a patent from a person who
is an applicant in the other case, for example by assignment or by
contract of employment, or in any other way.

### 18.93 {#section-42}

For the grant to be refused on an application which conflicts with
another, the priority dates in question must be the same, including the
case where one application claims priority from the other, or where one
is divided from the other and claims the same priority. It should be
borne in mind that it is the priority dates of the respective inventions
([see
5.20-5.24](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-20))
which must be considered, rather than the declared priority dates of the
applications. (If the priority dates of the respective inventions are
not the same, then one may form part of the state of the art in respect
of the other; if so, appropriate action under s.18(3) or s.73(1) should
be considered).

### 18.94 {#ref18-94}

Conflict between applications should be drawn to the attention of the
applicant as soon as it is detected (but not before the first
substantive examination), since it is desirable that the matter be dealt
with before any of the patents has been granted, so that the applicant
may decide how they wish the conflict to be resolved, for example by
amending to distinguish the inventions from one another or by deciding
which application should proceed. Once a patent has been granted on one
of the applications, for example if one application is accelerated (see
[15.46](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-46)
-- last square bracket), conflict can still be avoided either by
amendment of the outstanding application or by amendment under s.27 in
respect of the granted patent, since an amendment under s.27 has effect
retrospectively from the date of grant. However, the situation cannot be
saved by surrendering the granted patent, since this action is not
retrospective [(IBM Corporation (Barclay and Bigar's) Application
\[1983\] RPC
283)](http://rpc.oxfordjournals.org/cgi/content/abstract/100/18/283){rel="external"}.
There is no provision for dealing with a conflict between patents having
the same priority date and granted to the same applicant, other than in
the particular circumstances dealt with by s.73(2).

### 18.95 {#ref18-95}

The tests for determining under s.18(5) whether two UK applications
relate to the same invention are the same as for deciding under s.73(2)
whether a UK patent conflicts with a European patent (UK). The phrase
"for the same invention" under both s.18(5) and s.73(2) is regarded as
embodying the long-standing principle that the same monopoly should not
be granted twice over. Thus it covers not only the situation where
respective applications contain main claims explicitly including all of
the same features but also where the main claims differ in their wording
but their scope does not differ in substance. The examiner should
examine the independent claims only in their consideration of this
issue; dependent claims do not need to be examined for both s.18(5) and
s.73(2) conflict (see 18.95.1). The degree of overlap which is allowable
must be decided on the facts of the case, having regard to the state of
the art, and the applicant may be able to show that there is a
significant distinction between apparently conflicting claims. A
product-by-process claim should be considered an independent claim for
the purposes of assessing conflict.

### 18.95.1 {#ref18-95-1}

Examiners will only consider conflict in relation to independent claims.
The case law below provides useful guidance in relation to various
issues that underpin the assessment of conflict and is therefore still
of relevance, despite on occasion making references to conflict in
dependent claims.

### 18.96 {#ref18-96}

::: call-to-action
[Maag Gear Wheel and Machine Co Ltd's Patent \[1985\] RPC
572](http://rpc.oxfordjournals.org/cgi/content/abstract/102/24/572){rel="external"}
was a decision of the comptroller which related to an objection under
s.73(2).  In this case, which concerned a pivoted pad journal bearing
for a shaft, the claims of the European Patent (UK) explicitly defined
additional "pad geometry" features not found in the claims of the UK
patent. However, the hearing officer observed that, even though the
claims of the UK patent  were not explicitly limited to the pad geometry
claimed by the EP(UK) patent, that pad geometry was the only
construction disclosed in the UK patent. Therefore, he construed claim 1
of the UK patent as protecting a journal bearing including that pad
geometry and accordingly found the patents to be in conflict.
:::

In [IBM Corporation (Barclay and Bigar's) Application \[1983\] RPC
283](http://rpc.oxfordjournals.org/cgi/content/abstract/100/18/283){rel="external"},
before the comptroller, a claim to a solution containing a compound in a
concentration "less than 0.03M and sufficiently low to give a
perceptively lighter colour .....", and a claim to a solution containing
the same compound in a concentration "less than or equal to 0.02 Molar",
were also held to be for the same invention. In this case, although the
wording of the claims differed, the wording of the claims led to the
same result (because in order for the concentration in question to give
the required perceptively lighter colour it would have to be less than
or equal to 0.02M) and the scope of the claims therefore did not differ
in substance.

In [Marley (UK) Ltd's Patent \[1994\] RPC
231](http://rpc.oxfordjournals.org/cgi/content/abstract/111/6/231){rel="external"}
the Court of Appeal decided that the correct construction of s.73(2) was
the literal one that the UK patent may be revoked if the claims of the
UK and European (UK) patents are for the same invention. The court found
that a claim to a product will conflict with a claim to the same product
as produced by a specific process. In Marley, claim 1 of the UK patent
related to concrete articles made of a particular composition and having
particular qualities whilst claim 8 of the European patent (UK) related
to such concrete articles, but produced by a particular process.
Balcombe LJ held that claim 1 of the UK patent was for the same
invention as claim 8 of the European patent. It is perhaps worth noting
that, after the House of Lords' judgment in [Kirin-Amgen Inc v Hoechst
Marion Roussel Ltd \[2005\] RPC
9](http://rpc.oxfordjournals.org/cgi/content/abstract/122/6/169){rel="external"},
such a "product-by-process" claim would be construed as a claim to the
product ([see
14.120-1](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-120-1)),
thus also leading to the conclusion that the UK and European (UK)
patents related to the same invention. Note, in this case, claim 8 is
considered in effect to be an independent claim.

### 18.97 {#ref18-97}

In [Arrow Electric Switches Ltd's Applications 61 RPC 1
(1944)](http://rpc.oxfordjournals.org/content/61/1/1){rel="external"},
it was decided under the 1949 Act that where the respective UK patent
applications were directed to distinct inventions A and B, it is
allowable for one of the applications to contain a claim to the
combination of A and B. (However, it would not have been allowable for
both applications to contain such a claim.) In this case, one
application was a divisional of the other application, as a consequence
of a plurality objection. Invention A was an electric switch and
invention B was a means for operating an electric switch. The question
of double jeopardy from infringement arising out of a single act was
considered by Morton J. in the Arrow case and they concluded that it
would be unduly hard on inventors if protection for the combination A
and B were denied because its unauthorised use could result in a suit
under both the patents.

In [Kimberley-Clark Worldwide Inc's Patent (BL
O/279/04)](http://www.ipo.gov.uk/patent/p-decisionmaking/p-challenge/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o/279/04&submit=Go+%BB){rel="external"},
in relation to s.73(2), claims comprising feature A in the UK patent
were held not to be the same invention as the claims in the European
patent (UK) comprising features A and B. In the case in question the
hearing officer considered that the inclusion of feature B in the
European patent (UK) lead to a difference in substance between the two
claims, and therefore no revocation action was taken. In [SeeReal
Technologies SA (BL
O/261/12)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F261%2F12&submit=Go+%BB){rel="external"}
the Hearing Officer, when assessing conflict between claim 1 of a
divisional application and claim 1 of the parent application, confirmed
the practice followed in [Kimberley-Clark Worldwide Inc's Patent (BL
O/279/04)](http://www.ipo.gov.uk/patent/p-decisionmaking/p-challenge/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o/279/04&submit=Go+%BB0){rel="external"}.
The decision in [SeeReal Technologies SA (BL O/261/12) (PDF,
85.1KB)](http://www.ipo.gov.uk/pro-types/pro-patent/pro-p-os/o26112.pdf){rel="external"}
also highlighted the importance of claim construction when assessing
conflict, with reference to [Maag Gear Wheel and Machine Co Ltd's Patent
\[1985\] RPC
572](http://rpc.oxfordjournals.org/cgi/content/abstract/102/24/572){rel="external"}.

In [Koninklijke Philips Electronics n.v. v Nintendo of Europe GmbH
\[2014\] EWHC 1959
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2014/1959.htm){rel="external"},
Birss J (as he then was) gave full consideration to the issue of
so-called ''double patenting'' (see also 75.05.1). At paragraph 310 of
his judgment he stated; ''I find that as a matter of UK law a double
patenting objection taken as a ground for refusing a post-grant
amendment to a claim can be taken but should only be taken in the
following circumstances:

i\) The two patents must have the same priority dates and be held by the
same applicant (or its successor in title).

ii\) The two claims must be for the same invention, that is to say they
must be for the same subject matter and by this I mean they must have
the same scope. The scope is considered as a matter of substance.
Trivial differences in wording will not avoid the objection but if one
claim covers embodiments which the other claim does not, then the
objection does not arise.

iii\) The two claims must be independent claims. This necessarily
follows from the rejection of the point on overlapping scope. If two
independent claims have different scope then there is no reason to
object even if the patents contain dependent claims with the same scope.
The point might arise later if an amendment is needed e.g. to deal with
a validity attack but in the case the point can be taken then.

iv\) If the objection arises in the Patents Court in which both patents
are before the court then it can be cured by an amendment or amendments
to either patent.

v\) Even if the objection properly arises in the sense that two relevant
claims have the same scope, if the patentee has a legitimate interest in
maintaining both claims then the amendment should not be refused.''

Although this case dealt with conflict issues in relation to post-grant
amendments, and therefore is not binding practice for the office in
relation to pre-grant conflict, the same considerations are expected to
apply when assessing conflict for pre-grant cases as apply for
post-grant cases following Koninklijke.

### 18.97.1 {#ref18-97-1}

The EPO Technical Boards of Appeal in T 1790/12 and T 879/12 held that a
divisional application with second medical use claims of the form
"Substance X for use in the treatment of disease Y" did not conflict
with a parent application with "Swiss-type" second medical use claims of
the form "The use of substance X in the manufacture of a medicament to
treat disease Y". Although s.18(5) and s.73(2) are not derived from
provisions of the EPC, in view of this EPO case law, no objection should
be raised under s.18(5) or s.73(2) to conflict between applications
including the new form of second medical use claim and granted GB or EP
patents which protect the same medical use solely through Swiss-type
claims ([see
4A.27.1](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-27-1)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Annex A to section 18 of the Manual - Combined Search and Examination]{#default-id-6d9c2c45-heading-29 .govuk-accordion__section-button} {#annex-a-to-section-18-of-the-manual---combined-search-and-examination .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Annex A to section 18 of the Manual - Combined Search and Examination\",\"index_section\":29,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-29 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-29" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Annex A to section 18 of the Manual - Combined Search and Examination\",\"index_section\":29,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.98 {#ref18-98}

Sections 17 and 18 of the Manual refer at various points to the process
of combined search and examination. In particular, references are to be
found at
[17.35](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-35),
[17.94.10](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94-10),
[17.108](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-108),
[17.115](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-115),
[18.03](#ref18-03), [18.06](#ref18-06),
[18.08](#ref18-08),[18.39-41](#ref18-39), [18.47-49](#ref18-47),
[18.82](#ref18-82), [18.84.1-18.84.3](#ref18-84) and
[19.26](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-26).

\[Examiners in particular should be aware of the different procedures
which apply when processing combined search and examination cases. Of
particular help will be the examiners' aide-memoire for combined search
and examination cases, the contents of which are reproduced below.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [\[Combined search and examination (CS&E):Aide-memoire]{#default-id-6d9c2c45-heading-30 .govuk-accordion__section-button} {#combined-search-and-examination-cseaide-memoire .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"[Combined search and examination (CS\\u0026E):Aide-memoire\",\"index_section\":30,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-30 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-30" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"[Combined search and examination (CS\\u0026E):Aide-memoire\",\"index_section\":30,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Identifying CS&E cases

1). The COPS CS&E flag is automatically set to YES when Forms 9 & 10 are
filed on the same day. If Forms 9 and 10 are filed together but the
applicant states that CS&E is not wanted, or if, before the search is
started, Form 10 is filed after Form 9, CHA CSE is used to reset the
flag appropriately. CHA CSE is also used to reset the flag to NO on all
searched PCT cases filed with Forms 9 and 10.

2). All CS&E applications are identified by the appropriate label on the
dossier cover.

3). Where a request for CS&E is made too late the applicant is informed
and if necessary the CS&E flag is reset to NO using CHA CSE.

### Action on arrival in an examining group

4). Combined Search and Exam is recorded on PAFS when a CS&E case is
booked into an examining group but only after consultation with the
examiner when CS&E has been requested late. CS&E cases take their place
with applications awaiting search unless accelerated search and
examination has been requested (see 20, below).

### Search and First Substantive Examination

5). Where examination reveals objections the search and examination
reports are issued together with SE1 (SE1PA) setting a latest date for
reply 2 years from the priority/filing date. Before the case is booked
out the appropriate value is set for the COPS status field (normally
either 5 - "May be s.16 published" or 3 - "Searched -- Do not s.16
publish yet").

6). Where examination does not reveal any objection only the search
report is issued with SE3 (SE3PA). Before the case leaves the group the
appropriate value is set for the COPS status field. The case is then
diarised for return to the examiner 3 months after A-publication.

Annex A to section 18 of the Manual - Combined Search and Examination

7). When the file is diarised back to the examiner three months after
A-publication the search is topped up and any voluntary amendments
considered. A first examination report is then issued under s.18(4)
using EL3 (EL3F) or under s.18(3) using EL1 (EL1PA), with RC8 included
in the examination report to acknowledge any voluntary amendments, if
appropriate, as the case may be. On no account should the application
simply be sent for grant. The latest date for reply is based on the
normal 2 months for EL3 (EL3F) and the later of 4 months from the date
of the letter or 2 years from the priority/filing date for EL1 (EL1PA).

### Acknowledgement of Registered Trade Marks

8). The examiner should check at first examination stage whether any
unacknowledged terms are registered trade marks. If the examination has
not revealed any objections, the examiner should amend the specification
to acknowledge the mark ([see
19.23-19.26](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-23))
and inform the applicant by adding SC2 to SE3 (SE3PA). If other
objections are to be raised, the applicant should be asked to
acknowledge the mark, using RC11 added to the first s.18(3) report.

### Amendments

9). Amendment before the issue of the search report is allowable with
the Comptroller's consent and account should be taken of any such
amendment during CS&E.

10). New or amended claims filed voluntarily or as part of a full
response to the examination report before preparations for publication
are complete are published in the A-document in the normal way.

11). \[Deleted\]

12). A response to an examination report received after A-publication is
considered in the normal way.

13). If a further examination report is issued the period for reply is
set to expire two years from the priority/filing date or after the
normal four or two months, whichever is later.

14). No application is to be marked In Order until three months after
Apublication. If all earlier objections are met before this date, then
the file is diarised for return to the examiner at the end of that
period. EL32 is issued if the diarised date is for more than one month
ahead.

### A-publication

15). On publication the main COPS processing status for CS&E cases is
set automatically to 10 -- "Disposed of - not in order". This is reset
using REC PRO at the next COPS action if it does not reflect the true
status of the case.

### Third party observations

16). Any third party observations are copied to the applicant
immediately. With applications diarised because no objections are
outstanding the examiner also considers them without delay. On
applications where a response to an examination report is awaited the
examiner has the usual option of deferring consideration until the
response is filed.

### Extension of periods specified at CS&E stage or later

17). Requests for extension of the period for reply given in a
substantive examination report should be treated in the normal way. If
requested, on cases where no examination report is issued at CS&E, the
issue of the first s.18 report can be deferred until applications not
subject to CS&E are expected to receive their first s.18 examination.

### Top-up searches

18). Examiners complete the search for s.2(2) art and if possible also
for s.2(3) art as soon as practical and certainly before issue of EL3
(EL3F) or in other circumstances immediately before grant. Online
databases are generally sufficiently up to date 3 months from the
priority/filing date for the s.2(2) field and 21 months from the
priority/filing date for the s.2(3) field.

19). Where an application is sent to grant before 21 months or the
s.2(3) search is otherwise not complete, the examiner diaries it for
return at some appropriate time after grant to complete the search. Any
s.2(3) objection arising is made under s.73(1) in the normal way.

### Accelerated search and examination

20). The normal procedure for accelerated search should be followed but
with the examination done at the same time as the search.

### Early grant

21). This depends on the applicant requesting accelerated publication
(which is handled normally), otherwise the procedure is the same as with
any other CS&E case.

### Divisional applications

22). If a divisional application qualifies for CS&E the search and
examination are done together even if this was not the case for the
parent application and /or the search has effectively been done
following a request on the parent. If the parent application has not
been substantively examined this may be done at the same time as the
divisional (provided Form 10 has been filed) unless the applicant or
agent agrees otherwise. The need to delay grant for 3 months after
publication can be waived, in accordance with normal practice, if the
divisional invention was claimed in a sufficiently earlier published
parent.

23). ELC5 should be added to the covering letter of the first s.18
report on a divisional application where the first report on the parent
application was issued later than three years and six months from the
priority or filing date. This is to remind the applicant that the
unextended period for putting the divisional application in order
expires 12 months from the date of issue of the first report on the
earliest parent application.

24). The need to delay grant for 3 months after publication to allow for
s.21 observations should be waived where the invention claimed in the
divisional was clearly claimed in the parent, and the parent has been
published for at least 3 months.

### Refunds

25). Normally no refunds should be made if the search and examination
reports or just the search report have been issued.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Annex B to section 18 of the Manual - Extensions of time aide-memoire]{#default-id-6d9c2c45-heading-31 .govuk-accordion__section-button} {#annex-b-to-section-18-of-the-manual---extensions-of-time-aide-memoire .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Annex B to section 18 of the Manual - Extensions of time aide-memoire\",\"index_section\":31,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-31 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-31" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Annex B to section 18 of the Manual - Extensions of time aide-memoire\",\"index_section\":31,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.99 {#section-43}

The following aide-memoire for examiners reflects the practice described
in [18.53-18.60](#ref18-53).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Extensions of time: substantive examination reports]{#default-id-6d9c2c45-heading-32 .govuk-accordion__section-button} {#extensions-of-time-substantive-examination-reports .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Extensions of time: substantive examination reports\",\"index_section\":32,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-32 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-32" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Extensions of time: substantive examination reports\",\"index_section\":32,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
An as-of-right extension of two months, or until the compliance date if
sooner, is available to the period for reply specified in s.18
examination reports. The extension:

-   must be requested in writing, preferably in the covering letter
    accompanying the response or, alternatively, by email to
    <pateot@ipo.gov.uk> (telephone requests are not allowable)
-   may be requested retrospectively up to the expiry of the extended
    period
-   is available only once for any given report round
-   must be for two months, or until the compliance date if sooner,
    whether or not it is needed or wanted; extensions for shorter time
    periods are not available.

An extension further to the as-of-right extension may be requested but:

-   is only available if the as-of-right extension was requested
-   the request must be made before the expiry of the period as extended
    by right
-   is discretionary, but one single extension of up to one month on a
    given period is generally allowable.
-   does not usually need to be requested in writing
-   must give an adequate reason; if no reason is given, one should be
    asked for
-   may be requested at the same time as the as-of-right extension
-   must take into account how much time is left of the compliance
    period

Requests for additional further extensions:

-   must be made before expiry of the period as already extended.
-   require a strong reason to be allowed.
-   should be for the minimum required (normally \< 1 month) to fix the
    problem.

Where a late reply is received and no extension is available because the
period for requesting an extension has expired, the reply should be
referred to the examiner:

-   If no reason is given for the late reply, the examiner should ask
    for one; if no reason is forthcoming, the application will be
    refused
-   If a reason is given, the examiner should consider it and decide
    whether to exercise the discretion provided under section 18(3) to
    accept the late reply (despite it falling outside the specified
    period)

Administrative points:

-   requests for all extensions (i.e. as-of-right and discretionary) may
    be emailed to \[pateot@ipo.gov.uk\] -- an automatic reply will be
    sent. We do not guarantee to recognise requests sent to any other
    email address

-   requests for extensions emailed direct to examiners should be
    handled as follows:

email requests for as-of-right extensions should be forwarded to
formalities to be put on file. You have discretion whether or not to
reply but if you do, you should encourage the agent to use the
<pateot@ipo.gov.uk> service.

in the case of a discretionary request, you should consider the request
and respond, ensuring copies of all correspondence are placed on file.

-   no telephone requests for as-of-right extensions are allowable --
    encourage agent/applicant to use above dedicated email address
    rather than write a letter

-   replies to requests for discretionary extensions may be dealt with
    by telephone or in writing but ensure audit trail is complete. In
    all cases, a complete record (letter, telephone or email report) of
    events and reason for the request must be on file.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Annex C to section 18 of the Manual - Efficient processing of applications at substantive examination]{#default-id-6d9c2c45-heading-33 .govuk-accordion__section-button} {#annex-c-to-section-18-of-the-manual---efficient-processing-of-applications-at-substantive-examination .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Annex C to section 18 of the Manual - Efficient processing of applications at substantive examination\",\"index_section\":33,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-33 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-33" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Annex C to section 18 of the Manual - Efficient processing of applications at substantive examination\",\"index_section\":33,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.100 {#ref18-100}

The following aide-memoire for examiners reflects various practices used
to aid efficient processing of applications at substantive examination.

\[Efficient processing of applications: Examiners' aide-memoire\]

### Prior to commencement of examination -- use of examination opinions at search stage {#prior-to-commencement-of-examination--use-of-examination-opinions-at-search-stage}

1\) If major issues are identified at search stage, then an examiner may
issue a non-statutory examination opinion (see MoPP
[17.83.3](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-83-3)).
The purpose of this opinion is to encourage applicants to address major
defects in their application before requesting examination. If following
issuance of an examination opinion the applicant requests substantive
examination but offers no amendment or rebuttal to the opinion, then the
examiner can reissue the examination opinion as the first examination
report under s.18(3) (see MoPP
[18.47.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-47-1)),
in which case the report is known as an abbreviated examination report
(AER).

2\) Examiners are encouraged to make use of any commentary in their ISR
to complete an examination opinion at search stage, where it would be
likely to aid efficient processing in the event the case proceeded to
substantive examination.

### Prior to commencement of examination -- use of examination PEN letters {#prior-to-commencement-of-examination--use-of-examination-pen-letters}

3\) Pre-examination Notification (PEN) letters can be issued if major
issues are identified at search stage before the application is due to
be examined. These letters request that the applicant either amends or
withdraws before the first examination report is issued. PEN letters can
be adapted to refer to the specifics of the application in hand, such as
whether there is an IPRP present, foreign equivalents or grants, or an
exam opinion has been issued. These letters are not reports issued under
the Patents Act 1977, they are merely an attempt to communicate with the
applicant and encourage amendment or withdrawal when major issues are
present - applicants are not obliged to respond.

### Truncated examination reports -- standard clause RC47 {#truncated-examination-reports--standard-clause-rc47}

4\) On cases with major issues, examiners may defer consideration of
minor examination issues in a first s.18(3) report until the identified
major issues are resolved. Examination reports that are limited to major
objections only are known as ''truncated'' examination reports (TERs).
TERs are intended as a time-efficient way to deal with substantive
examinations on both domestic and PCT cases that have major issues.

5\) If examiners wish to truncate an examination report then they should
create a standard s.18(3) examination bundle in PROSE and insert
standard clause RC47 into the report as the first clause RC47 reads:

> Scope of report

This is a truncated examination report limited to major objections. I am
deferring full examination of your application, including updating of
the search, until you have addressed these objections.

6\) Clause RC47 may be used in a CSE examination report, but in this
situation examiners should take care to delete the reference to
''updating of the search''.

7\) When sending out a TER, examiners can either use: i) PROSE clauses
EC1-8, which are a set of standard clauses specifically designed for use
with examination opinions, AERs and TERs, or, ii) standard PROSE RC
clauses, as they see fit.

### Highlighting the existence of equivalents to applicants - standard clause RC48

8\) Standard clause RC48 in PROSE is intended for use when the claims of
the GB application are heavily anticipated, and the substantive examiner
is aware of equivalent applications that reveal additional citations
that may also be relevant to the issue of patentability. Clause RC48
reads:

> Equivalent applications

I am aware of the following patent applications which are equivalents of
this application: {Examiner list equivalent applications here}. I have
not reviewed these equivalent applications in detail at this stage,
however it is likely that any amendment made to these applications will
be required in connection with this application. Therefore, please
consider the citations raised against these equivalent applications when
amending the present application in response to this report.

9\) If an examiner is using RC48 then it means that they consider the
equivalent citations to be prima facie relevant to the issue of
patentability, but they have not considered their content in detail and
they are not formally citing them against the application in suit. It
follows then that documents cited against equivalent applications should
not be recorded as citations in PROSE merely because reference is made
to them when using RC48. Such citations should, however, be formally
recorded in PROSE at a later stage if they are cited as part of a formal
objection against the GB application in a subsequent exam report.

### Making use of international work products -- Standard Letter EL1C {#making-use-of-international-work-products--standard-letter-el1c}

10\) In the case of PCT applications, EL1C serves the function of an AER
and is issued when an examiner is in agreement with the findings of
international work products, i.e. the International Preliminary Report
on Patentability (IPRP) issued under Chapter I by the International
Search Authority (ISA), or Chapter II (also known as the International
Preliminary Examination Report (IPER)) issued by the International
Preliminary Examination Authority (IPEA).

11\) The EL1C is a standard letter in PROSE and can be tailored to
provide further information to the applicant which may assisting in
guiding amendment.

### Making use of EPO work products on cases with EP equivalents -- Standard Letter EL1D {#making-use-of-epo-work-products-on-cases-with-ep-equivalents--standard-letter-el1d}

12\) In the case of domestic GB applications, EL1D is an AER for use
when an examiner wishes to adopt the content of an examination report or
a European search opinion, issued by the European Patent Office (EPO) on
an EP equivalent, as a first examination report on a GB application. In
a manner analogous to the use of EL1C to adopt the content of
international work products, the aim of the EL1D is to reduce the amount
of time spent performing a first examination on an application that has
major issues. Examiners should note that EL1D is not appropriate for
adopting international work products where the EPO was the International
Searching Authority (ISA) - examiners should continue to use EL1C to
adopt international work products in all circumstances.

13\) Use of EL1D should be considered when the claims of the GB
application and the EP equivalent have claims of substantially the same
scope, the same or similar major issues, and, the examiner agrees with
the findings of the EPO examiner. Care however needs to be taken when
issues arise in areas where there is a divergence of practice between
the IPO and the EPO e.g. excluded matter objections and the approach to
inventive step.

14\) EL1D is available for examiners to use in PROSE under the document
category ''Examination Reports''. Several pieces of information must be
manually entered into an EL1D before issue, through replacement or
selective deletion of text highlighted in red. This information includes
whether the EP application whose examination report is being adopted has
been granted, the EP application's publication number, the particular
report(s) being adopted, the specific paragraph numbers of the report(s)
being adopted, and the date of the report(s). All of this information is
available via Espacenet. EL1D also contains a prompt, reminding
examiners to use clause RC31 to warn the applicant of potential conflict
between the EP and GB applications. If an examiner decides to issue an
EL1D, the EPO report(s) being adopted should be imported into PDAX using
the EQUIV document code. The documents should then be annotated with
"Adopted as first exam report".

15\) It should be noted that when using EL1D, the citations present in
the EP report being adopted will need to be manually added into PROSE by
the examiner before the report is issued.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Annex D to section 18 of the Manual -- Dealing with hearing requests]{#default-id-6d9c2c45-heading-34 .govuk-accordion__section-button} {#annex-d-to-section-18-of-the-manual--dealing-with-hearing-requests .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Annex D to section 18 of the Manual – Dealing with hearing requests\",\"index_section\":34,\"index_section_count\":34}"}
:::

::: {#default-id-6d9c2c45-content-34 .govuk-accordion__section-content aria-labelledby="default-id-6d9c2c45-heading-34" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Annex D to section 18 of the Manual – Dealing with hearing requests\",\"index_section\":34,\"index_section_count\":34}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 18.101 {#ref18-101}

A hearing is an opportunity for the applicant to put their case to a
Hearing Officer (normally a Deputy Director) if they cannot reach
agreement with an examiner about an objection raised during examination.
The aim is to avoid multiple unproductive rounds of amendments and to
hold the hearing before the compliance date. The purpose of the hearing
checklist below, is to ensure that examiners follow best practice in
relation to when to offer a hearing, and, the steps that need to be
completed prior to sending any case for a hearing.

\[HEARING CHECKLIST\]

When to offer the hearing? Y/N

-   the length of the compliance period remaining?
-   the number of times the objections have been made?
-   the total number of exam reports (3rd round is a good rule of
    thumb)?
-   is there genuine progress towards grant?
-   is there a saving amendment?
-   is there another way to proceed e.g. an interview?

Offering a hearing Y/N

-   include relevant covering letter (EL35) and a paragraph in exam
    report
-   instruct the ESO to include hearings leaflet if unrepresented
    applicant (see [MoPP
    18.79](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-79))
-   senior examiners can offer hearings, but it is good practice to
    discuss the suitability of a hearing with your Group Head

After the offer of a hearing Y/N

-   applicant accepts, declines, ignores the offer \> write a
    pre-hearing report
-   applicant withdraws the application or does not respond \> do the
    next case
-   are there outstanding procedural issues to deal with before the case
    is sent for the hearing?

Drafting the pre-hearing report

The purpose of the pre-hearing report is to brief the Hearing Officer
and Applicant on the issues to be decided, the objections and reasons
for them and other pertinent matters. A pre-hearing report must:

Y/N

-   set out the issues to be decided by the Hearing Officer N.B. no new
    objections can be raised
-   summarise the objections/examiner's arguments
-   set out full details of the objections (legal basis, citations,
    arguments) and any precedent cases to be referred to
-   include full analysis using appropriate legal tests e.g.
    Windsurfing, Aerotel, as appropriate
-   explain how you have construed/interpreted the claim(s)
-   summarise the applicant's arguments
-   identify any issues being deferred
-   are there minor objections outstanding?
-   is the search complete?
-   is the examination complete?
-   include details of the compliance date and any extensions if
    relevant
-   has your Group Head had the opportunity to review and comment on the
    draft?

Final steps

Y/N

-   add a minute on the PDAX file asking the hearings clerk to arrange a
    hearing
-   send a PDAX message to the HEARINGS team mailbox to notify the
    hearing
-   send a PDAX checklist message to the ESO to issue the pre-hearing
    report (AFS letter)
:::
:::
:::
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
