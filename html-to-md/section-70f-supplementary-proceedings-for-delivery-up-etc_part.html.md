::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 70F: Supplementary: proceedings for delivery up etc {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (70F-01) last published: 2017
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 70F

In section 70(1)(b) the reference to proceedings for infringement of a
patent includes a reference to proceedings for an order under section
61(1)(b) (order to deliver up or destroy patented products etc.)

### 70F.01 {#f01}

Section 70F confirms that a threat of infringement proceedings within
the definition in section 70 includes a threat to bring proceedings for
an order to deliver up or destroy a patented product. ([See also section
61](/guidance/manual-of-patent-practice-mopp/section-61-proceedings-for-infringement-of-patent)).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
