::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 94: Communication of information to the European Patent Office, etc {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (94.01 - 94.03) last updated April 2008.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 94

  -----------------------------------------------------------------------
   

  **Section 94**

  It shall not be unlawful by virtue of any enactment to communicate the
  following information in pursuance of the European Patent Convention to
  the European Patent Office or the competent authority of any country
  which is party to the Convention, that is to say\
  (a) information in the files of the court which, in accordance with the
  rules of court, the court authorises to be so communicated;\
  (b) information in the files of the Patent Office which, in accordance
  with rules under this Act, the comptroller authorises to be so
  communicated.
  -----------------------------------------------------------------------

### 94.01

CPR 63PD para 14.1 and CPR 63PD para 14.2 are also relevant

This section provides that the communication of information in pursuance
of the EPC as set out below is not unlawful.

### 94.02

r.63 and r.51 are also relevant

The Practice Direction supplementing Part 63 of the Civil Procedure
Rules provides that the court may authorise the communication to the EPO
or the competent authority of any country which is a party to the EPC of
any such information in the files of the court as the court thinks fit.
However, before authorising the disclosure of such information, the
court may permit any party who may be affected by the disclosure to make
representations, in writing or otherwise, on the question of whether the
information should be disclosed.

### 94.03

The comptroller may authorise any information in the files of the Office
to be communicated to the EPO or to a competent authority of any country
which is a party to the EPC, except where that information cannot be
communicated under s.118.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
