::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 83: Effect of patent decisions of competent authorities of other states {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (83.01 - 83.05) last updated April 2008.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 83.01 {#ref83-01}

s.82(3) is also relevant

Section 83 relates, like section 82, to questions arising before the
grant of a European patent whether a person has a right to be granted a
European patent or a share therein. Section 83 concerns the effect in
the UK of determinations of such questions by authorities of states
other than the UK which are party to the EPC, whereas section 82 lays
down the jurisdiction of the court and the comptroller to determine such
questions.

### 83.02 {#section}

s.130(7) is also relevant

Both sections 82 and 83 are so framed as to have, as nearly as
practicable, the same effects in the UK as the corresponding provisions
of the EPC, CPC and PCT.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 83(1)**
  A determination of a question to which section 82 above applies by the competent authority of a relevant contracting state other than the United Kingdom shall, if no appeal lies from the determination or the time for appealing has expired, be recognised in the United Kingdom as if it had been made by the court or the comptroller unless the court or he refuses to recognise it under subsection (2) below.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 83.03 {#ref83-03}

s.82(9) is also relevant

A determination of such a question ([see 83.01](#ref83-01)) by the
competent authority of a state other than the UK (which state is a party
to the EPC) is recognised in the UK as if the court or the comptroller
had made it, unless recognition is refused by the comptroller or court,
[see 83.05](#ref83-05). Recognition is subject to no appeal lying from
the determination or the time for appealing having expired.

### 83.04 {#section-1}

r.61 is also relevant

Any person seeking recognition in proceedings before the comptroller of
such a determination should furnish the comptroller with a copy thereof
certified as a true copy by an official of the competent authority in
question.

  -----------------------------------------------------------------------
   

  **Section 83(2)**

  The court or the comptroller may refuse to recognise any such
  determination that the applicant for a European patent had no right to
  be granted the patent, or any share in it, if either -\
  \
  (a) the applicant did not contest the proceedings in question because
  he was not notified of them at all or in the proper manner or was not
  notified of them in time for him to contest the proceedings; or\
  \
  (b) the determination in the proceedings in question conflicts with the
  determination of the competent authority of any relevant contracting
  state in proceedings instituted earlier between the same parties as in
  the proceedings in question.
  -----------------------------------------------------------------------

### 83.05 {#ref83-05}

The court or the comptroller may in certain circumstances refuse
recognition of a determination by such an authority that the applicant
for a European patent had no right to be granted the patent, or any
share in it. Those circumstances are that either the applicant was not
given a proper opportunity to contest the proceedings, as detailed in
sub-section (2)(a), or the determination conflicts with an earlier one
by a competent authority, as detailed in sub-section (2)(b).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
