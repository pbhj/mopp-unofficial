::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 43: Supplementary {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (43.01 - 43.03) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 43.01

This is the fifth and last of the group of sections relating to
inventions made by employees. Its provisions supplement those of ss.39
to 42 and relate to the applicability and construction of those
sections. The various provisions of s.43 are referred to where
appropriate in the chapters on ss.39 to 42.

  ----------------------------------------------------------------------------------------
   
  **Section 43(1)**
  Sections 39 to 42 above shall not apply to an invention made before the appointed day.
  ----------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 43(2)**

  Sections 39 to 42 above shall not apply to an invention made by an
  employee unless at the time he made the invention one of the following
  conditions was satisfied in his case, that is to say -\
  (a) he was mainly employed in the United Kingdom; or-\
  (b) he was not mainly employed anywhere or his place of employment
  could not be determined, but his employer had a place of business in
  the United Kingdom to which the employee was attached, whether or not
  he was also attached elsewhere.
  -----------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 43(3)**
  In sections 39 to 42 above and this section, except so far as the context otherwise requires, references to the making of an invention by an employee are references to his making it alone or jointly with any other person, but do not include references to his merely contributing advice or other assistance in the making of an invention by another employee.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 43(4)**
  Any references in sections 39 to 42 above to a patent and to a patent being granted are respectively references to a patent or other protection and to its being granted whether under the law of the United Kingdom or the law in force in any other country or under any treaty or international convention.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 43(5)**
  For the purposes of sections 40 and 41 above the benefit derived or expected to be derived by an employer from an invention or patent shall, where he dies before any award is made under section 40 above in respect of it, include any benefit derived or expected to be derived from it by his personal representatives or by any person in whom it was vested by their assent.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 43(5A)**
  For the purposes of sections 40 and 41 above the benefit derived or expected to be derived by an employer from an invention shall not include any benefit derived or expected to be derived from the invention after the patent for it has expired or has been surrendered or revoked.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 43.02

Section 43(5A) was added by the Patents Act 2004. This subsection
ensures that when assessing benefits under section 40(1) or (2) in
relation to inventions made by an employee and calculating the amount of
compensation under section 41, benefits that arise after the relevant
patent has ceased cannot be taken into account.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 43(6)**
  Where an employee dies before an award is made under section 40 above in respect of a patented invention made by him, his personal representatives or their successors in title may exercise his right to make or proceed with an application for compensation under subsection (1) or (2) of that section.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------
   
  **Section 43(7)**
  In sections 40 and 41 above and this section "benefit" means benefit in money or money's worth.
  -------------------------------------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 43(8)**
  Section 533 of the Income and Corporation Taxes Act 1970 (definition of connected persons) shall apply for determining for the purposes of section 41(2) above whether one person is connected with another as it applies for determining that question for the purposes of the Tax Acts.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 43.03

S.43(8) refers to s.533 of the Income and Corporation Taxes Act 1970
which was repealed and replaced by s.839 of the Income and Corporation
Taxes Act 1988. No consequential amendment was made to s.43(8).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
