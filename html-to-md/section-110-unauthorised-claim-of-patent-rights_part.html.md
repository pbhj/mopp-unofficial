::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 110: Unauthorised claim of patent rights {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (110.01 - 110.08) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 110.01

s\. 77(1) is also relevant

This section relates to false representation that a product is patented,
and the circumstances in which this constitutes an offence, and applies
in relation to both patents granted under the 1977 Act and European
patents (UK).

### 110.02

Section 111 makes similar provision with regard to unauthorised claims
to the existence of a patent application in respect of an article.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 110(1)**
  If a person falsely represents that anything disposed of by him for value is a patented product he shall, subject to the following provisions of this section, be liable on summary conviction to a fine not exceeding level 3 on the standard scale.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 110.03

s\. 130(1) is also relevant.

It is an offence, except as provided for in subsections (3) and (4),
[see 110.07 and 110.08](#ref110-07-110-08), for a person to falsely
represent that anything disposed of by them for value is a patented
product. By a "patented product" is meant a product which is a patented
invention or, in relation to a patented process, a product obtained
directly by means of the process or to which the process has been
applied. A representation that something is a patented product is false
if a patent has not yet been granted or is no longer in force.

\[ Any queries relating to offences under this section should be
referred to Patents Legal Section. \]

### 110.04

The maximum fine was converted to a level on the standard scale by the
Criminal Justice Act 1982.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 110(2)**
  For the purposes of subsection (1) above a person who for value disposes of an article having stamped, engraved or impressed on it or otherwise applied to it the word "patent" or "patented" or anything expressing or implying that the article is a patented product, shall be taken to represent that the article is a patented product.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 110.05 {#ref110-05}

Marking of an article to the effect that it is patented, eg by use of
the word "patent" or "patented", is taken (for the purposes of
subsection (1)) to represent that the article is a patented product. In
Cassidy v Eisenmann & Co Ltd \[1980\] FSR 381 (a private prosecution in
a Magistrates' Court), the defendants were found to have contravened
s.110 by selling an article marked "Brevettato" (the English translation
of which is "patented") followed by a list of countries including "Great
Britain" at a time when a UK patent had not been granted.

### 110.06 {#section-4}

s.62(1) is also relevant.

In order to be effective in providing protection against infringement,
however, such marking must be accompanied by the number of the patent or
marked with a relevant web address [see
62.04](/guidance/manual-of-patent-practice-mopp/section-62-restrictions-on-recovery-of-damages-for-infringement/#ref62-04).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 110(3)**
  Subsection (1) above does not apply where the representation is made in respect of a product after the patent for that product or, as the case may be, the process in question has expired or been revoked and before the end of a period which is reasonably sufficient to enable the accused to take steps to ensure that the representation is not made (or does not continue to be made).
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 110.07 {#ref110-07-110-08}

There is a period of grace following the expiry or revocation of the
relevant patent during which representation as a patented produced does
not constitute an offence under this section. That period is of a length
regarded as "reasonably sufficient" for the person in question to
prevent or cease such representation.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 110(4)**
  In proceedings for an offence under this section it shall be a defence for the accused to prove that he used due diligence to prevent the commission of the offence.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 110.08 {#section-5}

It is a defence for a person accused of an offence under this section to
have used due diligence to prevent its commission, but the onus is on
that person so to prove.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
