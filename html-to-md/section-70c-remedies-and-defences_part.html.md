::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 70C: Remedies and defences {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (70C.01-70C.04) last published: September 2017.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 70C(1)

Proceedings in respect of an actionable threat may be brought against
the person who made the threat for--- (a) a declaration that the threat
is unjustified; (b) an injunction against the continuance of the threat;
(c) damages in respect of any loss sustained by the aggrieved person by
reason of the threat.

### Section 70C(2)

In the application of subsection (1) to Scotland-\
(a) "declaration" means "declarator", and\
(b) "injunction" means "interdict".\

### 70C.01 {#c01}

Section 70C(1) sets out the remedies that a person aggrieved by the
threat can obtain when bringing legal action against a person who has
made the threat. An injunction can be obtained as an interim or final
remedy. The damages that can be awarded must relate to a loss caused by
the threat that is a natural and reasonable consequence of the threat
being made.

### Section 70C(3)

It is a defence for the person who made the threat to show that the act
in respect of which proceedings were threatened constitutes (or if done
would constitute) an infringement of the patent.

### 70C.02 {#ref70C-02}

Section 70C(3) sets out that, if the person who has made the threat can
show the act in question is an infringing act, or if done would be an
infringing act, then they have a defence in any threats proceedings.
However, the validity of the patent can be put in issue in proceedings
under the unjustified threats provisions ([see
74.03](/guidance/manual-of-patent-practice-mopp/section-74-proceedings-in-which-validity-of-patent-may-be-put-in-issue/#ref74-03)).
Therefore, even if the act in question was found to fall within the
scope of the patent claims, if the person aggrieved by the threat
successfully shows that the patent is invalid, then no infringement can
have occurred. In this case, the section 70C(3) defence that
infringement occurred must fail.

### Section 70C(4)

It is a defence for the person who made the threat to show-\
(a) that, despite having taken reasonable steps, the person has not
identified anyone who has done an act mentioned in section 70A(2)(a) or
(b) in relation to the product or the use of a process which is the
subject of the threat, and\
(b) that the person notified the recipient, before or at the time of
making the threat, of the steps taken.

### 70C.03 {#c03}

Section 70C(4) provides a defence which allows alleged secondary
infringers to be threatened in the circumstances where a primary actor
cannot be found. This defence applies the principle that a patent holder
must first try to direct any threats towards an alleged primary actor.
If the steps taken identify a primary actor, then this defence cannot be
relied upon even if it appears likely that there are multiple primary
actors, not all of whom have been found.

### 70C.04 {#c04}

Whether the requirement of "reasonable steps" is satisfied depends on
the facts of the case. A reasonable step can include the use of the
permitted communications exception (see
[70B.02-70B.03](/guidance/manual-of-patent-practice-mopp/section-74-proceedings-in-which-validity-of-patent-may-be-put-in-issue/#ref70B-02))
to determine whether a primary act has been committed and by whom. In
some circumstances, it is reasonable to pursue all possible lines of
enquiry, however in other circumstances it may be reasonable to do less.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
