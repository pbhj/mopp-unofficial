::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 1: Patentability {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (1.01 - 1.47) last updated: January2024.
:::

::: govuk-grid-column-two-thirds
::: {#default-id-4bae80f7 .gem-c-accordion .govuk-accordion .govuk-!-margin-bottom-6 module="govuk-accordion gem-accordion ga4-event-tracker" ga4-expandable="" anchor-navigation="true" show-text="Show" hide-text="Hide" show-all-text="Show all sections" hide-all-text="Hide all sections" this-section-visually-hidden=" this section"}
::: govuk-accordion__section
::: govuk-accordion__section-header
## [Section 1: Patentable inventions]{#default-id-4bae80f7-heading-1 .govuk-accordion__section-button} {#section-1-patentable-inventions .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Section 1: Patentable inventions\",\"index_section\":1,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-1 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-1" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Section 1: Patentable inventions\",\"index_section\":1,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.01 {#ref1-01}

The Patents Act 1977 sets out for the first time to codify what is meant
by a patentable invention. Previous legislation up to and including the
1949 Act had merely repeated the stipulation, originally set out in the
Statute of Monopolies of 1623, that a patent may be granted only for a
manner of new manufacture.

### 1.02 {#ref1-02}

\[Deleted\]

  ----------------------------------- -----------------------------------
                                       

  **Section 1(1)**                     

  A patent may be granted only for an .
  invention in respect of which the   
  following conditions are satisfied, 
  that is to say\                     
  (a) the invention is new;\          
  (b) it involves an inventive step;\ 
  (c) it is capable of industrial     
  application;\                       
  (d) the grant of a patent for it is 
  not excluded by subsections (2) and 
  (3) or section 4A below; and        
  references in this Act to a         
  patentable invention shall be       
  construed accordingly               
  ----------------------------------- -----------------------------------

### 1.03 {#ref1-03}

A patent may only be granted if the invention meets the above
conditions. The fact that an invention meets the requirements of s.1(1)
does not however mean that a patent must be granted, since there are
other requirements, as set out in later sections, which must also be
complied with.

### 1.04 {#ref1-04}

s.125(1) is also relevant

"Invention" in this context means that which is specified in a claim.
Although the term has a meaning in ordinary speech, in [Biogen Inc v
Medeva plc \[1997\] RPC
1](https://doi.org/10.1093/rpc/1997rpc1){rel="external"}) Lord Hoffmann
declined to attempt to define the term more closely, saying that judges
"would be well advised to put on one side their intuitive sense of what
constitutes an invention until they have considered the questions of
novelty, inventiveness and so forth". It is possible for a specification
to contain claims which relate to patentable inventions as well as
claims which define inventions which are not patentable or matters which
are not inventions. In such a case amendment is necessary, since a
patent should be granted only when each claim defines a patentable
invention. A claim will generally be held to be bad if anything falling
within its scope is not patentable.

### 1.05 {#ref1-05}

The term "a patentable invention" is defined by setting out four
conditions, all of which must be satisfied in order for an invention to
qualify for the grant of a patent. Since they are expressed as positive
requirements the onus is upon an applicant to demonstrate compliance
when faced with a reasonable challenge. The manner in which each of the
conditions (a), (b) and (c) is to be assessed is set forth in ss.2, 3 &
4 respectively. The fourth condition involves certain things which, for
purposes of the Act, are not to be regarded as inventions [(see 1.07 to
1.40.4)](#ref1-07) and certain inventions for which a patent will not be
granted [see 1.41-1.46](#ref1-41-1-46)

### 1.06 {#ref1-06}

s.130(7)-s.91 is also relevant

The tests set out in s.1(1) and further elaborated in s.1(2)-(4) and
ss.2-4 are so framed as to have, as nearly as practicable, the same
effects in the UK as the corresponding provisions of the EPC; these are
Articles 52-57. (More particularly, EPC aa.52(1) to (3) and 53
correspond to s.1(1) to (4)). Hence, although not binding on the Office,
decisions on patentability given by EPO Boards of Appeal are of
persuasive value in interpreting ss.1-4 ([see
0.07-09](https://draft-origin.publishing.service.gov.uk/guidance/manual-of-patent-practice-mopp/manual-of-patent-practice-introduction/#ref0-07){rel="external"},
[1.09](#ref1-09) and
[130.30-33](/guidance/manual-of-patent-practice-mopp/section-130-interpretation/#ref130-30)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Excluded subject matter]{#default-id-4bae80f7-heading-2 .govuk-accordion__section-button} {#excluded-subject-matter .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Excluded subject matter\",\"index_section\":2,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-2 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-2" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Excluded subject matter\",\"index_section\":2,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -----------------------------------------------------------------------
   

  **Section 1(2)**

  It is hereby declared that the following (among other things) are not
  inventions for the purposes of this Act, that is to say, anything which
  consists of -\
  (a) a discovery, scientific theory or mathematical method;\
  (b) a literary, dramatic, musical or artistic work or any other
  aesthetic creation whatsoever;\
  (c) a scheme, rule or method for performing a mental act, playing a
  game or doing business, or a program for a computer;\
  (d) the presentation of information;\
  but the foregoing provision shall prevent anything from being treated
  as an invention for the purposes of this Act only to the extent that a
  patent or application for a patent relates to that thing as such.
  -----------------------------------------------------------------------

### 1.07 {#ref1-07}

Section 1(2) of the Act defines certain categories of "things which are
not to be regarded as inventions", as Jacob LJ puts it in paragraph 12
of [Aerotel Ltd v Telco Holdings Ltd & Ors Rev 1 \[2007\] RPC
7](http://www.bailii.org/ew/cases/EWCA/Civ/2006/1371.html){rel="external"}
(Aerotel/Macrossan). The normal meaning of "invention" is therefore
qualified by the removal of these categories. Jacob LJ also considered
that there is no overarching principle behind the inclusion of these
particular categories, and no guidance on whether they are to be
construed broadly or restrictively. In particular, they noted that, as
the exclusions are not expressed as exceptions, the general principle
that exceptions should be interpreted restrictively does not apply to
them.

### 1.08 {#ref1-08}

The current practice in dealing with excluded matter under s. 1(2) is
derived from the judgment of the Court of Appeal in Aerotel/Macrossan
(see [1.09.1](#ref1-09-1), [1.18](#ref1-18), [1.20](#ref1-20),
[1.33-1.33.1](#ref1-33), [1.34.1](#i)). This judgment considered all
previous authorities on the matter, and -- having been endorsed in
numerous subsequent decisions -- is to be treated as a definitive
statement of how the law on patentable subject matter is to be applied
in the UK.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Relationship with the EPC and decisions of the EPO]{#default-id-4bae80f7-heading-3 .govuk-accordion__section-button} {#test .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Relationship with the EPC and decisions of the EPO\",\"index_section\":3,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-3 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-3" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Relationship with the EPC and decisions of the EPO\",\"index_section\":3,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.09 {#ref1-09}

As discussed above [(see 1.06)](#ref1-06) decisions of the EPO Boards of
Appeal are of persuasive value; furthermore where the EPO Boards of
Appeal have formed a settled view of European patent law, this will
generally be followed by the UK courts ([see
0.08](/guidance/manual-of-patent-practice-mopp/manual-of-patent-practice-introduction/#ref0-08))
and
[130.30-31](/guidance/manual-of-patent-practice-mopp/section-130-interpretation/#ref130-30)).

### 1.09.1 {#ref1-09-1}

However in
[Aerotel/Macrossan](http://rpc.oxfordjournals.org/content/124/4/117.abstract){rel="external"},
the Court of Appeal analysed EPO Boards of Appeal decisions on the
question of excluded matter and concluded that practice was not yet
sufficiently settled to enable the Court to depart from precedent UK
case-law (see 1.11). This was reiterated in [Symbian Ltd's Application
\[2009\] RPC 1](https://doi.org/10.1093/rpc/rcn033){rel="external"}
(Symbian) (see paragraph 46) and HTC v Apple (see [1.09.2](#k)), and it
remains the case until the UK courts conclude otherwise.

### 1.09.2 {#ref1-09-2}

In an attempt to address whether case-law concerning excluded matter is
settled, and derive uniformity of application of European patent law,
the President of the EPO referred four questions on the patentability of
computer programs to the Enlarged Board of Appeal in October 2008
[(G3/08) (PDF,
247KB)](http://www.epo.org/law-practice/case-law-appeals/pdf/g080003ex1.pdf){rel="external"}.
However, the Board concluded that the referral was inadmissible because
the decisions referred to were not considered to be "divergent", and
declined to answer the questions beyond determining their admissibility.
This led to the Court of Appeal reaffirming its view that practice was
not yet settled in [HTC Europe Co Ltd v Apple Inc \[2013\] EWCA Civ
451](http://www.bailii.org/ew/cases/EWCA/Civ/2013/451.html){rel="external"}
at paragraph 44. The assessment of whether an invention relates to an
excluded category as such should therefore continue to follow the test
set out in the decision in Aerotel/Macrossan.

### 1.09.3 {#ref1-09-3}

As the Office is bound by the judgments in Aerotel/Macrossan and [HTC v
Apple](http://www.bailii.org/ew/cases/EWCA/Civ/2013/451.html){rel="external"},
it cannot choose to depart from the Aerotel/Macrossan test in order to
adopt EPO practice even if that practice becomes settled -- such a
departure can only be made by the courts. This is emphasised in [Dell
Products LP's Application (BL
O/321/10)](https://www.ipo.gov.uk/pro-types/pro-patent/pro-p-os/p-challenge-decision-results-bl?BL_Number=O/321/10){rel="external"},
amongst others.

### 1.09.4 {#ref1-09-4}

However, as Birss J stated in paragraph 9 of [Lenovo (Singapore) PTE Ltd
v Comptroller General of Patents \[2020\] EWHC 1706
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2020/1706.html){rel="external"},
"although in methodological terms the approach in the UK and the
approach in the EPO may look different, in practice they reach the same
result, at least usually." This reinforces what was previously expressed
at paragraph 11 of [Symbian Ltd's Application \[2009\] RPC
1](https://doi.org/10.1093/rpc/rcn033){rel="external"}, that "as a
matter of broad principle, it seems to us that the approaches in \[...\]
the great majority of cases in this jurisdiction and in the EPO, are, on
a fair analysis, capable of reconciliation". Furthermore in [HTC v
Apple](http://www.bailii.org/ew/cases/EWCA/Civ/2013/451.html){rel="external"},
Kitchin LJ, at paragraph 41, stated that "...in terms of result \[...\]
it seems to me that whichever route is followed, one ought to end up at
the same destination". The result of EPO Boards of Appeal decisions on
cases with similar facts may therefore be persuasive, even though the
approach taken by the EPO should not itself be followed (see
[130.31](/guidance/manual-of-patent-practice-mopp/section-130-interpretation/#ref130-31)).
A number of older decisions of the EPO Boards of Appeal -- including
Vicom Systems Inc T0208/84 \[1987\], IBM/Data processing network
T0006/83 \[1990\] and IBM/Computer-related invention T0115/85 \[1990\]
-- were used to inform the Aerotel test, as well as other later guidance
such as the AT&T signposts ([see 1.36](#ref1-36) and [1.37](#ref1-37)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [General Principles]{#default-id-4bae80f7-heading-4 .govuk-accordion__section-button} {#general-principles .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"General Principles\",\"index_section\":4,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-4 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-4" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"General Principles\",\"index_section\":4,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Balance of probabilities

### 1.10 {#ref1-10}

The Court of Appeal, in paragraph 5 of [Aerotel Ltd v Telco Holdings Ltd
& Ors Rev 1 \[2007\] RPC
7](http://rpc.oxfordjournals.org/content/124/4/117.abstract){rel="external"}
(Aerotel/Macrossan), made it clear that assessing excluded matter
involves a question of law which should be decided during prosecution of
the patent application. The position is therefore assessed fully by
patent examiners before grant, and objections are not to be dropped
simply because the applicant asserts that the invention relates to
non-excluded subject matter. The question of excluded matter is decided
on the balance of probabilities, taking into account all of the evidence
available. However, as it is a question of law, it is not something on
which applicants are entitled to the benefit of the doubt, in the way
they would be in relation to questions of pure fact (such as the date of
a particular disclosure, or the scope of the common general knowledge).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Using past decisions]{#default-id-4bae80f7-heading-5 .govuk-accordion__section-button} {#using-past-decisions .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Using past decisions\",\"index_section\":5,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-5 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-5" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Using past decisions\",\"index_section\":5,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.11 {#ref1-11}

The Office is bound to follow the decisions of the UK courts, per the
English common law doctrine of binding precedent, which states that
courts and tribunals must abide by the decisions of higher courts.
Therefore, the Office must follow the interpretation of the Act and the
practice set out in such judgments, and apply the legal principles
arrived at by the judges in those cases.

### 1.12 {#ref1-12}

However, examiners must exercise caution in relying on the specific
facts of any court judgment as support for an objection, as each case
must be judged on its merits, and the facts of the individual case are
certain to be different. As Pumfrey J stated in paragraph 186 of
[Research In Motion UK Ltd. v Inpro Licensing SARL \[2006\] EWHC 70
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2006/70.html){rel="external"},
"The test is a case-by-case test, and little or no benefit is to be
gained by drawing analogies with other cases decided on different facts
in relation to different inventions." Although the test being referenced
was not the Aerotel test, the principle remains the same. Birss J, in
paragraph 17 of [Lantana v Comptroller-General of Patents \[2013\] EWHC
2673
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2013/2673.html){rel="external"},
noted that "\[s\]imply because it is possible to construct a generalised
category which includes both the claimed invention \[...\] and a
previous decision in which a claim was held to be patentable, does not
help. It shows that such things can be patentable in some cases but does
not show that the invention in this case is patentable."

### 1.12.1 {#ref1-12-1}

The Office is not bound to follow the practice of the EPO Boards of
Appeal for the reasons detailed above [(see 1.09.1)](#test)

### 1.12.2. {#ref1-12-2}

Hearing decisions issued by the Office are non-binding, but can be
persuasive in establishing an argument.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Substance over form]{#default-id-4bae80f7-heading-6 .govuk-accordion__section-button} {#substance-over-form .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Substance over form\",\"index_section\":6,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-6 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-6" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Substance over form\",\"index_section\":6,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.13 {#ref1-13}

When determining if an invention falls foul of the exclusions, it is
critical that the examiner consider the substance of the invention
rather than the form of claim provided, by looking beyond the strict
literal wording of the claims. For example, when a claim is directed to
a computer program, the examiner must look at what the computer program
will do when run, as established in paragraph 49 of [Astron Clinica Ltd
& Ors v The Comptroller General of Patents, Designs and Trade Marks
\[2008\] EWHC
85](http://www.bailii.org/ew/cases/EWHC/Patents/2008/85.html){rel="external"}
[RPC
14](http://rpc.oxfordjournals.org/content/125/4/339){rel="external"}.

### 1.14 {#ref1-14}

If the substance of an invention falls within one of the excluded
categories, however, then no form of claim will circumvent an objection.
If a claim is directed to a computer, but the substance of the invention
is a business method running on that computer, it will still be
excluded. In paragraph 44 of
[Aerotel/Macrossan](http://www.bailii.org/ew/cases/EWCA/Civ/2006/1371.html){rel="external"},
Jacob LJ noted that "If an inventor claims a computer when programmed
with his new program, it will not assist him if he alleges wrongly that
he has invented the computer itself, even if he specifies all the
detailed elements of a computer in his claim."

### 1.15 {#ref1-15}

It is not the nature of a single embodiment of an invention which is
important when determining whether it is excluded, but the nature of the
central idea or invention which is embodied in the claims. To determine
this, the invention claimed should be assessed and construed as a whole
to see whether it comprises an advance that lies in a non-excluded
field. However, as Floyd J observed in paragraph 23 of [Kapur v
Comptroller-General of Patents \[2008\] EWHC 649
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2008/649.html){rel="external"},
if there are embodiments of a claim that fall within excluded subject
matter, the fact that the claim is wide enough to encompass embodiments
that are not excluded under s. 1(2) will not be sufficient to save it.
The exclusion "will still bite to the extent that excluded subject
matter is claimed".

### 1.15.1 {#ref1-15-1}

In his judgment in [Astron Clinica Ltd & Ors v The Comptroller General
of Patents, Designs and Trade Marks \[2008\], RPC
14](https://www.bailii.org/ew/cases/EWHC/Patents/2008/85.html){rel="external"},
Kitchin J. held (at paragraph 51) that in a case where claims to a
method performed by running a suitably programmed computer or to a
computer programmed to carry out the method are allowable, then, in
principle, a claim to the program itself should also be allowable.
However, they added that the program claim must be drawn to reflect the
features of the invention which would ensure the patentability of the
method which the program is intended to carry out when it is run. Thus,
following the conclusions of Astron Clinica, claims to a computer
program or a program on a carrier are allowable where corresponding
method and apparatus claims (or notional such claims if none exist)
are/would be allowable on application of the Aerotel/Macrossan test. (A
claim to a computer program stored on a carrier such as a compact disc
is sometimes referred to in the US as a Beauregard claim.) However,
merely including claims to a program or a program on a carrier will not
make an otherwise excluded invention patentable. As Fox LJ said in
[Merrill Lynch's Application \[1989\] RPC
561](https://doi.org/10.1093/rpc/1989rpc561){rel="external"} (page
569):\
\
"it cannot be permissible to patent an item excluded by section 1(2)
under the guise of an article which contains that item - that is to say,
in the case of a computer program, the patenting of a conventional
computer containing that program. Something further is necessary."

In [Macrossan's Patent Application \[2006\] EWHC 705
(Ch)](http://www.bailii.org/ew/cases/EWHC/Patents/2006/705.html){rel="external"},
Mann J (at paragraph 42) rejected submissions by the applicant that
adding a product step onto an otherwise excluded claim was enough to
render it patentable. This was upheld by the Court of Appeal in
Aerotel/Macrossan. Similarly, in [Bloomberg LLP and Cappellini's
Applications \[2007\] EWHC 476
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2007/476.html){rel="external"},
Pumfrey J stated (at paragraph 9) that\
"\[a\] claim to a programmed computer as amatter of substance is just a
claim to the program on a kind of carrier. A program on a kind of
carrier, which, if run, performs a business method adds nothing to the
art that does not lie in excluded subject matter".

Claims to a computer program for generating a system or for producing a
product (for example claims of the form: 'a computer program comprising
computer-readable code for generating a system as claimed in X' or
'storage medium having encoded thereon code for making a system as
claimed in Y' or 'machine readable medium having software thereon to
produce the item of claim Z on a 3D printer') may avoid exclusion if,
following the guidance of [Astron
Clinica](http://www.bailii.org/ew/cases/EWHC/Patents/2008/85.html){rel="external"},
the computer program would cause an otherwise patentable process to be
performed when run. In any case, care should be taken to ensure that
such claims comply with other sections of the act including clarity,
support, and sufficiency [see
s14](/guidance/manual-of-patent-practice-mopp/section-14-the-application).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Interaction between the exclusions]{#default-id-4bae80f7-heading-7 .govuk-accordion__section-button} {#interaction-between-the-exclusions .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Interaction between the exclusions\",\"index_section\":7,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-7 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-7" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Interaction between the exclusions\",\"index_section\":7,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.16 {#ref1-16}

The contribution made by an invention does not need to fall solely
within a single excluded category for the invention to be excluded. As
stated in paragraph 34 of [Raytheon Company v Comptroller General of
Patents, Designs and Trade Marks \[2007\] EWHC 1230
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2007/1230.html){rel="external"}
, where a contribution falls wholly within two or more exclusions,
invention would still be excluded. In paragraph 27 of [Symbian Ltd's
Application \[2009\] RPC
1](https://doi.org/10.1093/rpc/rcn033){rel="external"}, the Court of
Appeal remarked obiter that one effect of the computer program exclusion
is to prevent other excluded material becoming patentable merely by use
of a computer in its implementation (although [see 1.31](#n) for the
interaction with the mental act exclusion). Thus, for example, a
business method implemented on a conventional computer system or network
would be excluded as both a method for doing business and a computer
program as such. Birss J expressed a similar view in paragraphs 32-36 of
[Halliburton Energy Services Inc's Applications \[2012\] RPC
129](https://doi.org/10.1093/rpc/rcs019){rel="external"}.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Not an exhaustive list]{#default-id-4bae80f7-heading-8 .govuk-accordion__section-button} {#not-an-exhaustive-list .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Not an exhaustive list\",\"index_section\":8,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-8 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-8" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Not an exhaustive list\",\"index_section\":8,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.17 {#ref1-17}

The phrase "among other things" suggests that the list of excluded
fields is not a complete list of exclusions; however, to date, the
courts have only given one example of matter which would also be
excluded. In [Lux Traffic Controls Ltd v Pike Signals Ltd and Faronwise
Ltd \[1993\] RPC
107](https://doi.org/10.1093/rpc/1993rpc107){rel="external"}, Aldous J
observed that a method of controlling traffic as such was not
patentable, regardless of whether it also fell within the business
method exclusion; claims to apparatus for controlling traffic flows were
allowed on the basis that the particular apparatus involved a
contribution in a technical (i.e. non-excluded) field. The hearing
officer in [Kostuj's Application (BL
O/028/12)](https://www.ipo.gov.uk/pro-types/pro-patent/pro-p-os/p-challenge-decision-results-bl?BL_Number=O/028/12){rel="external"}
concluded that a method of developing a golf swing without a club was
caught within the scope of the "among other things" phrasing, even if it
did not fall squarely within the other exclusions.

### 1.17.1 {#ref1-17-1}

Therefore, the exclusion may also apply to other matters which are
essentially abstract or intellectual, but which do not fall clearly into
one of the categories specifically listed. It is possible, although
untested in the Courts, that such other matters would fall foul of the
fourth step of the Aerotel test (see [1.23](#ref1-23)) as not being
technical in nature.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Relationship of excluded matter to novelty and inventive step]{#default-id-4bae80f7-heading-9 .govuk-accordion__section-button} {#relationship-of-excluded-matter-to-novelty-and-inventive-step .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Relationship of excluded matter to novelty and inventive step\",\"index_section\":9,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-9 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-9" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Relationship of excluded matter to novelty and inventive step\",\"index_section\":9,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.17.2 {#ref1-17-2}

In [Lantana v Comptroller-General of Patents \[2014\] EWCA Civ
1463](http://www.bailii.org/ew/cases/EWCA/Civ/2014/1463.html){rel="external"}
the Court of Appeal reiterated that the requirements of s.1(1) and
s.1(2) are separate, and that claims to novel and inventive subject
matter may still be excluded. In paragraph 19 of the judgment Arden LJ
held: "I see no mandate in section 1 of the PA 77 for holding that it is
sufficient that there is an inventive step. It is deliberate legislative
policy to exclude certain matters from patentability even if they would
otherwise be patentable". This was reaffirmed in paragraph 70 by Kitchin
LJ, who stated that "\[t\]here is no inconsistency between an acceptance
that an invention \[...\] is new and inventive and a finding that the
contribution it makes falls solely within excluded subject matter". The
judge continued by remarking that "the former requires \[...\] an
assessment of whether it forms part of the state of the art or is merely
an obvious step away from it", while exclusion relates to assessing
whether the contribution falls solely within excluded categories.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The Aerotel/Macrossan test]{#default-id-4bae80f7-heading-10 .govuk-accordion__section-button} {#the-aerotelmacrossan-test .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The Aerotel/Macrossan test\",\"index_section\":10,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-10 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-10" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The Aerotel/Macrossan test\",\"index_section\":10,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.18 {#ref1-18}

The Aerotel/Macrossan test (often referred to as the 'Aerotel test') is
set out in paragraph 40 of the judgment in [Aerotel Ltd v Telco Holdings
Ltd & Ors Rev 1 2007 RPC
7](http://rpc.oxfordjournals.org/content/124/4/117.abstract){rel="external"}
(Aerotel/Macrossan). It provides a framework for the examiner to assess,
and decide upon, the issue of excluded matter. As stated in
[1.08](#ref1-08), Aerotel/Macrossan is considered to be the definitive
statement of how the law relating to excluded matter is to be applied.
The Aerotel test is therefore the principal tool to be used when dealing
with excluded matter cases, and encompasses all previous tests within
it.

The test comprises four steps, which are as follows:

\(1\) Properly construe the claim;

\(2\) identify the actual contribution;

\(3\) ask whether it falls solely within the excluded subject matter;

\(4\) check whether the actual or alleged contribution is actually
technical in nature.

### 1.18.1 {#ref1-18-1}

It was stated by Jacob LJ in paragraph 47 of Aerotel/Macrossan that the
test is a re-formulation of, and is consistent with, the previous
"technical effect approach with rider" test established in [Merrill
Lynch's Application \[1989\] RPC
561](https://doi.org/10.1093/rpc/1989rpc561){rel="external"} and
followed by [Gale's Application \[1991\] RPC
305](https://doi.org/10.1093/rpc/1991rpc305){rel="external"} and
[Fujitsu's Application \[1997\] RPC
608](https://doi.org/10.1093/rpc/1997rpc608){rel="external"}. The Court
considered it, in paragraph 48, to be "a structured and more helpful way
of re-formulating the statutory test." Therefore, as Kitchin LJ notes in
paragraph 44 of [HTC v Apple \[2013\] EWCA Civ
451](http://www.bailii.org/ew/cases/EWCA/Civ/2013/451.html){rel="external"}
the structured approach of the Aerotel test is followed in order to
address whether the invention makes a technical contribution to the art,
with the rider that novel or inventive purely excluded matter does not
count as a "technical contribution".
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Step 1 -- construing the claim]{#default-id-4bae80f7-heading-11 .govuk-accordion__section-button} {#step-1--construing-the-claim .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Step 1 – construing the claim\",\"index_section\":11,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-11 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-11" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Step 1 – construing the claim\",\"index_section\":11,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.19 {#ref1-19}

As the court stated in
[Aerotel/Macrossan](http://www.bailii.org/ew/cases/EWCA/Civ/2006/1371.html){rel="external"},
the examiner must first "decide what the monopoly is before going on to
the question of whether it is excluded". There are no special
considerations with regard to claim construction; the normal principles
apply ([see
14.111-14.120](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-111),
[125](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125))
are applied. In construing that claim examiners should be mindful of all
that falls within the scope of the claim since it is possible for a
claim to encompass both an embodiment that makes a technical
contribution and an embodiment that is excluded ([see 1.15]((#ref1-15))
in which case the claim will fall as being excluded.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Step 2 -- identifying the contribution]{#default-id-4bae80f7-heading-12 .govuk-accordion__section-button} {#step-2--identifying-the-contribution .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Step 2 – identifying the contribution\",\"index_section\":12,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-12 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-12" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Step 2 – identifying the contribution\",\"index_section\":12,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.20 {#ref1-20}

Jacob LJ outlined the considerations to be applied when identifying the
contribution made by the claims in paragraph 43 of Aerotel/Macrossan --
the critical factors for the examiner to consider are emphasised:

> The second step -- identify the contribution - is said to be more
> problematical. How do you assess the contribution? Mr Birss submits
> the test is workable -- it is an exercise in judgment probably
> involving the problem said to be solved, how the invention works, what
> its advantages are. What has the inventor really added to human
> knowledge perhaps best sums up the exercise. The formulation involves
> looking at substance not form -- which is surely what the legislator
> intended.

Paragraph 44 states that, at application stage, the contribution may be
taken to be that alleged by the inventor, although this cannot be
conclusive; as Jacob LJ states, "\[i\]n the end the test must be what
contribution has actually been made, not what the inventor says they
have made". Additional guidance regarding the interpretation of this
paragraph was provided in [IGT/Acres Gaming Inc, Re \[2008\] EWHC 568
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2008/568.html){rel="external"}
paragraphs 23-24, which states that the examiner is not bound to accept
what the applicant says, and is entitled to determine whether the
alleged contribution is known or obvious, typically by performing a
search.

### 1.20.1 {#ref1-20-1}

Therefore, knowledge of the prior art will play a role in assessing the
contribution; as Lewison J noted in paragraph 8 of [AT&T Knowledge
Ventures/Cvon Innovations v Comptroller General of Patents \[2009\] EWHC
343
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2009/343.html){rel="external"},
the examiner should have "some notion of the state of the art". This
does not necessarily mean however that the contribution is defined by
what is new and inventive in the claim [(see 1.21)](#ref1-21). That
proper consideration of the prior art can make a difference to the
outcome is best illustrated by [Aerotel Ltd v Wavecrest Group
Enterprises Ltd & Ors \[2008\] EWHC 1180
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2008/1180.html){rel="external"}.
This case involved the same patent as Aerotel/Macrossan; however,
Wavecrest was able to show that the contribution argued in the earlier
case was known, by providing evidence that the 'special exchange' which
formed the essential part of the new arrangement of hardware ([see
1.34.1](#ref1-34-1)) was actually within the common general knowledge.
The Patents Court therefore reassessed the contribution and found that
it related to how the special exchange was used and programmed, so that
it fell solely within the business method and computer program
exclusions.

### 1.20.2 {#ref1-20-2}

When assessing the contribution, examiners may consider prior art
falling in the section 2(3) field, following the judgment of the Patents
Court in [Nokia v Oppo \[2023\] EHWC 23 (Pat) (see §
278)](https://www.bailii.org/ew/cases/EWHC/Patents/2023/23.html){rel="external"}.

### 1.20.3 {#ref1-23}

A formal search is not required for the examiner to be able to assess
the nature of the contribution. As stated by [Birss J in paragraph 17 of
Lenovo (Singapore) PTE Ltd v Comptroller General of Patents \[2020\]
EWHC 1706
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2020/1706.html){rel="external"},
"it cannot be formally necessary to conduct a prior art search before
taking this sort of point \[on excluded matter\]. It would not be wrong
to do that, but the Comptroller need not do so. Some cases may be such
that no reference to prior art at all is needed to see that the subject
matter is unpatentable and in that kind of case, the extra cost and
trouble of a search would be wasted. Others, like this one, can be
addressed based on prior art which the examiner was aware of without
having to undertake a formal search." Peter Prescott QC (sitting as a
Deputy Judge) in [CFPH LLC \[2005\] EWHC 1589
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2005/1589.html){rel="external"}
at paragraph 96 stated that examiners are "entitled to make use of their
specialist knowledge" to identify the contribution and are not bound to
rely on prior art searches. Pumfrey J stated in [Shopalotto.com Ltd's
Application \[2006\] RPC
7](https://doi.org/10.1093/rpc/2006rpc7){rel="external"} at paragraph 12
that certain features may be "so notorious that a formal search is
neither necessary nor desirable", and that examiners are "entitled to
use common sense and experience" to make that determination. Examiners
should therefore use their own judgement to determine whether to perform
a search, and may accordingly use the provisions of section 17(5)(b) if
appropriate ([see
17.94-17.101](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94))

### What has been added to human knowledge

### 1.21 {#ref1-21}

As Jacob LJ stated in
[Aerotel/Macrossan](http://www.bailii.org/ew/cases/EWCA/Civ/2006/1371.html){rel="external"}
this is the summation of what the contribution is, and all of the other
factors [(see 1.21.2-1.21.4)](#ref1-21) weigh in to making this
determination. The starting point for that assessment is the claims. It
may be helpful to consider what makes the invention novel ([see
1.20.1](#ref1-20-1)); however, it is then necessary to place that in its
proper context and ensure that the effects of the invention are taken
into account. It is not correct to eliminate everything in the claim
that is known to arrive at that which is unknown, and then to conclude
that the unknown part must be the contribution; i.e., as the Court of
Appeal in [Genentech \[1989\] RPC
205](https://doi.org/10.1093/rpc/1989rpc205){rel="external"} put it, it
is not the case that "an invention is unpatentable if the inventiveness
was contributed only by matters excluded under section 1(2)". This
approach -- which is sometimes referred to as "Falconer reasoning", from
its originator; or, less formally, "salami-slicing" -- was expressly
rejected by the Court of Appeal in that decision. Birss J, in [Lenovo
(Singapore) PTE Ltd v Comptroller General of Patents \[2020\] EWHC 1706
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2020/1706.html){rel="external"}
at paragraph 16, summarised matters thus; "invention can lie in a new
combination of old features and so, while identifying an individual
feature as disclosed in prior art is a relevant thing to do, it will
always be necessary to consider it in the context of the invention as a
whole before reaching a conclusion." In [Lantana v Comptroller-General
of Patents \[2014\] EWCA Civ
1463](http://www.bailii.org/ew/cases/EWCA/Civ/2014/1463.html){rel="external"}
paragraph 64, Kitchin LJ set out the importance of considering the
proper context of an invention when assessing the contribution,
accepting "\[a\] submission that it is the claim as a whole which must
be considered when assessing the contribution which the invention has
made, and that it is not permissible simply to cut the claim into pieces
and then consider those pieces separately and without regard to the way
they interact with each other". However, at paragraph 65, Kitchin LJ
qualified this by observing: "\[n\]evertheless, I also have no doubt
that, approached in this way, it is the actual contribution to the art
which the invention has made which must be considered."

### 1.21.1 {#ref1-21-1}

The courts have consistently found that, where claims recite standard
hardware, such conventional apparatus does not form part of the
contribution. This is often the case in computer program inventions --
an application relating to a computer program cannot be saved simply by
claiming conventional computer hardware programmed in a particular way.
Jacob LJ remarked on this in paragraph 44 of
[Aerotel/Macrossan](http://www.bailii.org/ew/cases/EWCA/Civ/2006/1371.html){rel="external"},
and specifically rejected the use of standard hardware when determining
the contribution of the Macrossan application in paragraph 73.

### 1.21.2 {#ref1-21-2}

What constitutes a technical contribution can change over time.
Therefore the view that, once a technical contribution is made, that
same contribution will always be technical may not apply. In paragraph
16 of [Lantana v Comptroller-General of Patents \[2013\] EWHC 2673
(Pat)](http://www.bailii.org/ew/cases/EWCA/Civ/2014/1463.html){rel="external"},
Birss J addressed this point, noting that "\[t\]he fact that in the IBM
case \[T6/83\] the method of communication between programs and files
\[...\] was held patentable in 1988 does not mean that any method of
communicating between programs and files \[...\] necessarily involves a
technical contribution today". This was endorsed by Arden LJ in
paragraph 43 of the Court of Appeal judgment [Lantana v
Comptroller-General of Patents \[2014\] EWCA Civ
1463](http://www.bailii.org/ew/cases/EWCA/Civ/2014/1463.html){rel="external"}.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Substance not form]{#default-id-4bae80f7-heading-13 .govuk-accordion__section-button} {#substance-not-form .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Substance not form\",\"index_section\":13,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-13 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-13" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Substance not form\",\"index_section\":13,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.21.3 {#ref1-21-3}

This is the application of the general principle ([see
1.13-1.15.1](#ref1-13)) -- the examiner must look beyond the literal
wording of the claims to identify the contribution, and consider the
central idea embodied in the claims. Recasting the claim in a different
way, e.g. by claiming apparatus instead of method steps, will not
usually alter the contribution.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Additional factors]{#default-id-4bae80f7-heading-14 .govuk-accordion__section-button} {#additional-factors .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Additional factors\",\"index_section\":14,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-14 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-14" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Additional factors\",\"index_section\":14,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.21.4 {#ref1-21-4}

Considering the additional factors that Jacob LJ in Aerotel/Macrossan
thought relevant to identifying the contribution:

-   the problem said to be solved: an invention usually solves a
    problem; the problem and its solution are almost always relevant to
    the assessment of the contribution; an explicit statement may be
    included in the description, but not always -- it may simply be
    derived from the nature of the invention

-   how the invention works may be simply the definition of the features
    of the invention in the claims, but it is often more useful to
    consider in terms of what the invention does as a matter of
    practical reality

-   finally, the advantages of the invention are normally closely linked
    to the problem being addressed, in the sense that the advantage is
    often that the problem is resolved. All of these factors may assist
    in identifying the contribution, rather than defining it themselves
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Step 3 -- ask whether the contribution falls solely within the excluded matter]{#default-id-4bae80f7-heading-15 .govuk-accordion__section-button} {#step-3--ask-whether-the-contribution-falls-solely-within-the-excluded-matter .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Step 3 – ask whether the contribution falls solely within the excluded matter\",\"index_section\":15,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-15 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-15" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Step 3 – ask whether the contribution falls solely within the excluded matter\",\"index_section\":15,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.22 {#ref1-22}

The assessment carried out at step 3 is a critical part of the Aerotel
test. How that assessment is made, and whether a particular contribution
falls within one of the exclusions, will depend on the individual
exclusion, and so is covered in greater detail under the specific
categories in [1.25-1.40.4](#ref125). In general, examiners should bear
in mind that the third step does not require determining whether the
contribution falls solely within the excluded subject matter categories
as they are listed in section 1(2), but rather whether it falls solely
within excluded subject matter as such. The "as such" qualification
therefore narrows what is excluded -- inventions may appear to fall
solely with the excluded categories, but are not excluded as such. This
is particularly true in the case of computer programs and discoveries,
and this was recognised in [Merrill Lynch's Application \[1989\] RPC
561](https://doi.org/10.1093/rpc/1989rpc561){rel="external"}.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Step 4 -- check if the contribution is technical]{#default-id-4bae80f7-heading-16 .govuk-accordion__section-button} {#step-4--check-if-the-contribution-is-technical .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Step 4 – check if the contribution is technical\",\"index_section\":16,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-16 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-16" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Step 4 – check if the contribution is technical\",\"index_section\":16,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.23 {#ref1-23}

The fourth step is a necessary step, as Jacob LJ stated in paragraph 46
of
[Aerotel/Macrossan](http://rpc.oxfordjournals.org/cgi/content/abstract/124/4/117){rel="external"},
and was included in the test on the basis that it was required, in light
of the earlier decision of the Court of Appeal in [Merrill Lynch's
Application \[1989\] RPC
561](https://doi.org/10.1093/rpc/1989rpc561){rel="external"}. In Merrill
Lynch (page 569, lines 2-10), the necessity of making this assessment
was characterised as follows:

> ...it cannot be permissible to patent an item excluded by section 1(2)
> under the guise of an article which contains that item -- that is to
> say, in the case of a computer program, the patenting of a
> conventional computer containing that program. Something further is
> necessary. The nature of that addition is, I think, to be found in the
> Vicom case where it is stated: "Decisive is what technical
> contribution the invention makes to the known art". There must, I
> think, be some technical advance on the prior art in the form of a new
> result (e.g. a substantial increase in processing speed as in Vicom)

### 1.23.1 {#ref1-23-1}

\[deleted\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Interaction between steps 3 and 4]{#default-id-4bae80f7-heading-17 .govuk-accordion__section-button} {#interaction-between-steps-3-and-4 .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Interaction between steps 3 and 4\",\"index_section\":17,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-17 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-17" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Interaction between steps 3 and 4\",\"index_section\":17,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.24 {#ref1-24}

Whilst the steps will often be considered separately, the examiner may
already have addressed the question posed in step 4 in answering step 3,
as determining whether the contribution falls solely within excluded
matter may require assessing whether there is a relevant technical
contribution, particularly with regard to the computer program
exclusion. The Court of Appeal in [Symbian Ltd v Comptroller General of
Patents \[2009\] RPC
1](https://doi.org/10.1093/rpc/rcn033){rel="external"} ruled that the
question of whether the invention makes a technical contribution has to
be addressed when considering the computer program exclusion, although
whether that takes place at step 3 or 4 is not critical.

\[When applying the Aerotel/Macrossan test in an examination report,
examiners may find it useful to use PROSE clause RC4.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Applying the Aerotel/Macrossan test to specific to exclusions]{#default-id-4bae80f7-heading-18 .govuk-accordion__section-button} {#applying-the-aerotelmacrossan-test-to-specific-to-exclusions .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Applying the Aerotel/Macrossan test to specific to exclusions\",\"index_section\":18,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-18 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-18" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Applying the Aerotel/Macrossan test to specific to exclusions\",\"index_section\":18,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.25 {#ref1-25}

In all cases of excluded matter, the test in [Aerotel Ltd v Telco
Holdings Ltd & Ors Rev 1 \[2007\] RPC
7](http://rpc.oxfordjournals.org/content/124/4/117.abstract){rel="external"}
(Aerotel/Macrossan) is the starting point, no matter which exclusion is
being relied upon. However, when applying the test, the individual
exclusions have specific principles which derive from case law both
before and after Aerotel/Macrossan. These can inform how the examiner
assesses one or more of the steps of the test; however, they should
always bear in mind that the case should be judged on its own merits
([see 1.12](#ref1-12)).

### 1.25.1 {#ref1-25-1}

That a contribution may fall within the scope of more than one exclusion
and remain excluded [see 1.16](#q) should also be kept in mind.
Objections may be raised covering multiple exclusions, and examiners are
encouraged to raise all relevant exclusions, so that the applicant is
not surprised by an additional objection at a later stage.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Section (a) - Discoveries, scientific theories and mathematical methods]{#default-id-4bae80f7-heading-19 .govuk-accordion__section-button} {#section-a---discoveries-scientific-theories-and-mathematical-methods .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Section (a) - Discoveries, scientific theories and mathematical methods\",\"index_section\":19,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-19 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-19" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Section (a) - Discoveries, scientific theories and mathematical methods\",\"index_section\":19,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.26 {#ref1-26}

When considering the patentability of biotechnological inventions with
regard to these particular exclusions, section 76A(1) should also be
consulted (for discoveries in particular, [see
76A.06](/guidance/manual-of-patent-practice-mopp/section-76a-biotechnological-inventions/#ref76A-06))
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Discoveries & scientific theories]{#default-id-4bae80f7-heading-20 .govuk-accordion__section-button} {#discoveries--scientific-theories .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Discoveries \\u0026 scientific theories\",\"index_section\":20,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-20 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-20" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Discoveries \\u0026 scientific theories\",\"index_section\":20,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.27 {#ref1-27}

The established view on when this exception applies was summarised by
Peter Prescott QC (sitting as a Deputy Judge) in paragraph 34 of [CFPH
LLC \[2005\] EWHC 1589
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2005/1589.html){rel="external"}:

> It is well settled law that, although you cannot patent a discovery,
> you can patent a useful artefact or process that you were able to
> devise once you had made your discovery. This is so even where it was
> perfectly obvious how to devise that artefact or process, once you had
> made the discovery \[... The law\] objects only when you try to
> monopolise your discovery for all purposes i.e. divorced from your new
> artefact or process. For that would enable you to stifle the creation
> of further artefacts or processes which you yourself were not able to
> think of.

This line of reasoning, stemming from, amongst others, the decision in
[Hickton's Patent Syndicate v Patents & Machine Improvements Co Ltd
(1909) 26 RPC
339](https://doi.org/10.1093/rpc/26.13.339){rel="external"}, was used by
Lewison J in [Tate & Lyle Technology v Roquette Frères \[2009\] EWHC
1312
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2009/1312.html){rel="external"}
, where a claim, whose contribution was found to be the explanation of
the underlying workings of a previously known method of manufacturing a
sugar substitute, was refused as a discovery as such (see also
[2.14.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-14-1)).

### 1.27.1 {#ref1-27-1}

In [Genentech Inc's Patent \[1989\] RPC
147](https://doi.org/10.1093/rpc/1989rpc205){rel="external"}, the Court
of Appeal held that the discovery of an amino acid sequence for the
substance tPA led to a valid claim when incorporated into a conventional
manufacturing process for the manufacture of tPA. Similarly, in
[Kirin-Amgen v Hoechst Marion Roussel \[2005\] RPC
9](https://doi.org/10.1093/rpc/2005rpc9){rel="external"}, the House of
Lords held that a DNA sequence of a gene was not an invention on its
own, but if it were necessary to isolate and extract it, then a process
developed for this purpose and the material when obtained by this
process could both be patentable ([see also
76A.05](/guidance/manual-of-patent-practice-mopp/section-76a-biotechnological-inventions/#ref76A-05)).

### 1.28 {#ref1-28}

This principle also applies to scientific theories -- practical
applications which rely on the theory may be patentable; the theory
itself would not be. For example, in the two related cases of
[Blacklight Power, BL
O/076/08](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F076%2F08&submit=Go+%BB){rel="external"}
saw an application for modelling atoms based on a new theory of electron
states refused (in part) as relating to a scientific theory, whereas in
[BL
O/114/08](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F114%2F08&submit=Go+%BB){rel="external"},
an application for a practical application of that theory (a plasma
reactor and a laser utilising the effects derived from the theory) was
not considered to be excluded (although it was refused for lack of
industrial applicability).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Mathematical methods]{#default-id-4bae80f7-heading-21 .govuk-accordion__section-button} {#mathematical-methods .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Mathematical methods\",\"index_section\":21,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-21 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-21" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Mathematical methods\",\"index_section\":21,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.29 {#ref1-29}

In Vicom Systems Inc T0208/84 \[1987\], the EPO Board of Appeal defined
a mathematical method as one which "is carried out on numbers and
provides a result in numerical form". A mathematical method was
therefore merely an abstract concept prescribing how to operate on the
numbers, and a claim directed solely to the mathematical method was
excluded from patentability as a result of its abstract nature.

### 1.29.1 {#ref1-29-1}

However, if an invention provides a technical contribution, it may avoid
exclusion even though the underlying idea may reside in a mathematical
method; a claim to a method of enhancing digital images by software
processing that implemented a mathematical method was considered to
provide such a contribution in Vicom and allowed. As Lewison J observed
in paragraph 17 of [AT&T Knowledge Ventures/Cvon Innovations v
Comptroller General of Patents \[2009\] EWHC 343
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2009/343.html){rel="external"}
when the board in Vicom considered whether the claim was excluded as
being a mathematical method, they "concluded that it was not because the
mathematical method which underlay the invention was being used in a
technical process which was carried out on a physical entity by
technical means." Thus, where the contribution made by an invention
relates to the practical application of the mathematical method to a
technical process, it will not be considered a mathematical method as
such. In [Halliburton Energy Services Inc's Applications \[2012\] RPC
129](https://doi.org/10.1093/rpc/rcs019){rel="external"} both a method
of designing a drill bit using computer simulation and a method of
simulating drilling a directional wellbore were found not excluded
although each involved mathematics. In paragraph 72 Birss J held:
"Although obviously some mathematics is involved, the contribution is
not solely a mathematical method (on top of being a computer program)
because the data on which the mathematics is performed has been
specified in the claim in such a way as to represent something concrete
(a drill bit design etc.)".

The outcome in Halliburton is consistent with the EPO Enlarged Board of
Appeal's decision in [G01/19 (Pedestrian Simulation) OJ EPO 2021
A31](https://www.epo.org/en/boards-of-appeal/decisions/g190001ex1){rel="external"},
which was concerned with the operation of the EPO's problem-solution
approach for assessing the inventive step of computer-implemented
simulations.

### 1.29.2 {#ref1-29-2}

The presence of real data taken from a physical entity in a claim
relating to a mathematical method, for example a method of modelling,
simulation or prediction of a real world system, does not necessarily
imply that a corresponding invention will avoid exclusion. [WesternGeco
Ltd 's Application (BL
O/135/07)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F135%2F07&submit=Go+%BB){rel="external"},
a method for processing real-world geophysical data by mere abstract
manipulation of the data was held by the hearing officer to be a
mathematical method as such; however, it was felt that the application
of this processed data to determine parameters relating to physical
properties of the Earth's interior, so that an improved seismic image
could be produced, did not fall wholly within the exclusion as a result
of the decision in Vicom.

### 1.29.3 {#ref1-29-3}

Such inventions might therefore avoid exclusion if they contribute to
non- excluded fields. For example, whilst a claim directed to monitoring
and identifying the position of people and packages in a transportation
network was refused as a mathematical method (as well as a computer
program and method for doing business) in [Innovation Sciences Pty
Limited (BL
O/315/12)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F315%2F12&submit=Go+%BB){rel="external"}
; in [Waters Investments Limited 's Application (BL
O/146/07)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F146%2F07&submit=Go+%BB){rel="external"},
a contribution comprising a method of analyzing samples by an analytical
technique which uses chromatography and spectrometry followed by a
sequence of data analysis techniques (i.e. a mathematical method) to
give particular results, although including some steps that could be
said to fall within excluded matter, was found to lie in the technical
field of sample analysis using chromatography and spectrometric
techniques, and so was not excluded.

### 1.29.4 {#ref1-29-4}

In [Gale's Application \[1991\] RPC
305](http://rpc.oxfordjournals.org/content/108/13/305.abstract){rel="external"}the
applicant had devised a new and better algorithm for calculating square
roots and claimed it on a read only memory (ROM). In holding the
invention to be excluded, Nicholls LJ stated on page 328: "Mr. Gale has
devised an improvement in programming. What his instructions do, but it
is all they do, is to prescribe for the CPU in a conventional computer a
different set of calculations from those normally prescribed when the
user wants a square root". Nicholls LJ also held that the instructions
did not embody a technical process and did not solve a technical problem
within a computer. Thus, carrying out a mathematical method by a program
will not, in general, be enough to avoid exclusion. Similarly, an
improvement in programming or an improved algorithm will not generally
be enough to avoid exclusion unless there is something more to it in the
form of a technical contribution ([see 1.35-1.39.2](#ref1-135)).

### 1.29.5 {#ref1-29-5}

Many computer-implemented inventions and particularly inventions
involving computer simulation and artificial intelligence ("AI") relate
in some way to a mathematical method. If a computer-implemented
mathematical method is applied to a technical process or if it solves a
technical problem within a computer, it will likely not be excluded (see
section 1.35-1.39.2 for more detail on assessment of computer
implemented inventions).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Section (b) -- aesthetic creations]{#default-id-4bae80f7-heading-22 .govuk-accordion__section-button} {#section-b--aesthetic-creations .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Section (b) – aesthetic creations\",\"index_section\":22,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-22 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-22" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Section (b) – aesthetic creations\",\"index_section\":22,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.30 {#ref1-30}

If an article is distinguished from the prior art solely by its design,
ornamentation or colour, then it will be excluded if this has a purely
aesthetic function, but if the distinction has a practical effect then
this could save it from the exclusion. For example, if a serving tray
were characterised solely by having a particular embossed pattern on its
surface, then it would fall within the exclusion, but if it were found
that this particular pattern had non-slip attributes unexpectedly
superior to those normally associated with embossment, this could
provide a patentable feature.

### 1.30.1 {#ref1-30-1}

Evidence may be necessary where the advantage conferred by the
apparently aesthetic distinction is not clear from the disclosure. In
[I.T.S. Rubber Ltd's Application \[1979\] RPC
318](https://doi.org/10.1093/rpc/1979rpc318){rel="external"}, decided
under the 1949 Act, a claim to a squash ball characterised by its blue
colour was allowed because evidence showed that it had surprisingly
enhanced visibility during play.

### 1.30.2 {#ref1-30-1}

The means of obtaining a purely aesthetic effect may be patentable if it
is characterised by non-excluded features, such as the structure of an
article or the steps in a process. For example, a fabric may be provided
with an attractive appearance by means of a layered structure not
previously used for this purpose, in which case a fabric incorporating
such a structure might be patentable.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Section (c) -- mental act, playing a game, business methods, and computer programs]{#default-id-4bae80f7-heading-23 .govuk-accordion__section-button} {#section-c--mental-act-playing-a-game-business-methods-and-computer-programs .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Section (c) – mental act, playing a game, business methods, and computer programs\",\"index_section\":23,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-23 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-23" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Section (c) – mental act, playing a game, business methods, and computer programs\",\"index_section\":23,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Mental act

### 1.31 {#ref1-31}

The decision in [Halliburton Energy Services Inc's Applications \[2012\]
RPC 129](https://doi.org/10.1093/rpc/rcs019){rel="external"} confirmed
(in paragraphs 57 and 63) that the mental act exclusion is to be
interpreted narrowly -- it only covers acts that are carried out by
"purely mental means", and does not extend to those which are merely
capable of being performed mentally. HHJ Birss QC (sitting as a Judge of
the High Court) considered that the aim of the exclusion was to prevent
patents being granted which could be infringed "by thought alone".

### 1.31.1 {#ref1-31-1}

In the same judgment (in paragraph 43), HHJ Birss specifically outlined
that, with this interpretation, a claim carried out on a computer could
not be excluded as a mental act -- this was adopted in the Practice
Notice Patents Act 1977: Patentability of Mental Acts. Therefore, if a
computer, or any other hardware, is involved in the invention, it will
not be excluded as a mental act. However, the claim could still fall
within the computer program exclusion.

### 1.31.2 {#ref1-31-2}

Following the Halliburton decision, examples such as a method of
learning a language, of playing chess, or teaching reading may still be
excluded as methods for performing a mental act, provided they fall
within the narrow interpretation. Pure design methods may also still
fall within this exclusion; for example, in [Halliburton Energy Services
Inc v Smith International (North Sea) Ltd \[2006\] RPC
2](https://doi.org/10.1093/rpc/2006rpc2){rel="external"}, the Patents
Court held that a method for creating a design of rock drilling bit
amounted to a mental act. This decision was expressly endorsed in the
later Halliburton case (in paragraph 49), where the Court held that the
claims in the earlier case were not limited solely to a computer
implementation, and were therefore directed to "the purely intellectual
content of the design process". As a result, the claims encompassed both
excluded and non-excluded embodiments (see [1.15](#ref1-15)). In
contrast, the claims in the later case were restricted to a computer
implementation of the design method, and therefore fell outside the
narrow interpretation of the mental act exclusion.

### 1.31.3 {#ref1-31-3}

In Halliburton v Smith, Pumfrey J indicated (at paragraphs 215-218) that
a claim could avoid the mental act exclusion if it was "tethered" to a
step of manufacturing the designed product, even if it was not solely
limited to a computer implementation. The method in the specification
had a "potential technical effect", but as the claims were so broad as
to encompass activities outside that method, they needed to be
restricted to the technical field in which the method operated to
eliminate those activities from the scope of the claim. Approaching this
from the perspective of the later Halliburton case, this is another
example of a non-mental limitation which, if present in a claim, would
mean that the exclusion will not apply.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Games]{#default-id-4bae80f7-heading-24 .govuk-accordion__section-button} {#games .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Games\",\"index_section\":24,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-24 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-24" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Games\",\"index_section\":24,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.32 {#ref1-32}

The patentability of schemes, rules or methods for playing a game was
considered by the Patents Court in [Shopalotto.com Ltd's Application
\[2006\] RPC 7](https://doi.org/10.1093/rpc/2006rpc7){rel="external"},
where the application related to a computer lottery game played over the
internet. The Court held that the patentability of games should be
assessed using the general approach for excluded matter, putting it in
line with the other exclusions. Following Aerotel/Macrossan, this means
that the patentability of games should be assessed using the Aerotel
test. Patentability of games was also considered in [IGT v The
Comptroller-General of Patents \[2007\] EWHC 1341
(Ch)](http://www.bailii.org/ew/cases/EWHC/Patents/2007/1341.html){rel="external"}
which concerned four applications relating to gaming apparatus,
specifically casino-based systems using slot-machines. In each case,
Warren J held that the claims as construed related to known apparatus
upon which a game was played. The contribution in each case therefore
lay in the rules or methods used to play the game, and fell solely
within the excluded category. See also [1.16](#ref1-16) and
[1.25.1](#ref1-25-1) for discussion of the interaction between the
different exclusions.

### 1.32.1 {#ref1-32-1}

In relation to non-computer based games, apparatus which involves a
combination of standard game items, such as a board, playing pieces, a
random outcome generator (e.g. dice), chance cards and the like, which
are united by the nature of the game played will usually be excluded,
since the actual contribution lies in the game that the apparatus is
used to play. One example is [Anderson 's Application (BL
O/112/12)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F112%2F12&submit=Go+%BB){rel="external"},
in which the Hearing Officer held that the contribution lay in a scheme,
rule or method for playing a game, and that amending to include the use
of known apparatus through which the game could be played did not change
the contribution in substance. The application was therefore refused.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Business methods]{#default-id-4bae80f7-heading-25 .govuk-accordion__section-button} {#business-methods .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Business methods\",\"index_section\":25,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-25 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-25" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Business methods\",\"index_section\":25,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.33 {#ref1-33}

The Court of Appeal in
[Aerotel/Macrossan](http://www.bailii.org/ew/cases/EWCA/Civ/2006/1371.html){rel="external"}
expressly rejected the interpretation placed upon this exclusion by Mann
J in [Macrossan's Patent Application\[2006\] EWHC
705](http://www.bailii.org/cgi-bin/markup.cgi?doc=/ew/cases/EWHC/Patents/2006/705.html&query=title+(+macrossan+)&method=boolean){rel="external"} -
that a method of doing business should be a way of conducting an entire
business, rather than a tool to facilitate business transactions or
procedural steps having administrative or financial character - and
instead took a wider view of what constitutes a business method. It
found that there was no reason to limit the exclusion in the way Mann J
had, and confirmed this by looking at the French and German versions of
EPC Article 52(2), which are not limited to methods of conducting entire
businesses. (The French refers to "economic activities" while the German
refers to "business activities".)

### 1.33.1 {#ref1-33-1}

Therefore, the exclusion is to be interpreted as encompassing such tools
or steps, and not merely abstract matters or completed transactions. As
the Court in Aerotel/Macrossan noted: "whether as an abstract or
generalised activity or as a very specific activity, if it is a method
of doing business as such it is excluded" (paragraph 68). Double entry
bookkeeping was cited in the judgment as an example of a concept that
did not involve conducting an entire business or a completed
transaction, but nevertheless was clearly a method of doing business.

### 1.33.2 {#ref1-33-2}

The expression "doing business" is also not restricted to financial or
commercial activities, but embraces administrative, organisational and
managerial activities. In
[Aerotel/Macrossan](http://www.bailii.org/ew/cases/EWCA/Civ/2006/1371.html){rel="external"},
it was noted that the idea of having three document trays - "in", "out"
and "too difficult" - was a way of conducting business and no more. In
Melia's Application (BL O/153/92), the Hearing Officer refused a scheme
under which prisoners could exchange all or part of a prison sentence
for corporal punishment as a business method - the business in question
being that of administering punishment. In [Cummins-Allison Corp 's
Application (BL
O/361/12)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F361%2F12&submit=Go+%BB){rel="external"},
the Hearing Officer refused a method of conducting blind balancing on
documents associated with a deposit transaction, in which the deposit
was displayed only if the received and calculated amounts match, as an
administrative process falling solely within the exclusion. [Wills'
Application (BL
O/089/99)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F089%2F99&submit=Go+%BB){rel="external"},
relating to the provision of cards to be held by a school and the
parents or grandparents of a child so as to provide an immediate source
of accurate, up-to-date information in the event that the child goes
missing, and [John Lahiri Khan 's Appn (BL
O/356/06)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F356%2F06&submit=Go+%BB){rel="external"},
a method of effecting introductions between people wearing a designated
device, such as a ring, were both refused under this exclusion. In
[Hewlett-Packard Development Company 's Application (BL
O/441/12)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F441%2F12&submit=Go+%BB){rel="external"},
a method for calculating and modifying the characteristics of an item,
such as a magazine, before it is printed -- to produce a printed item
with an optimised postage weight -- was considered to be no more than a
method of managing postal changes, and therefore a business method.

### 1.34 {#ref1-34}

The business method exclusion is generic, as discussed at page 569 of
[Merrill Lynch's Application \[1989\] RPC
561](https://doi.org/10.1093/rpc/1989rpc561){rel="external"} where Fox
LJ rejected a claim to a computerised system for making a trading
market:

> Now let it be supposed that claim 1 can be regarded as producing a new
> result in the form of a technical contribution to the prior art. That
> result, whatever the technical advance may be, is simply the
> production of a trading system. It is a data-processing system for
> doing a specific business, that is to say, making a trading market in
> securities. The end result, therefore, is simply "a method of doing
> business", and is excluded by section 1(2)(c). The fact that the
> method of doing business may be an improvement on previous methods of
> doing business does not seem to me to be material. The prohibition in
> section 1(2)(c) is generic; qualitative considerations do not enter
> into the matter. The section draws no distinction between the method
> by which the mode of doing business is achieved. If what is produced
> in the end is itself an item excluded from patentability by section
> 1(2), the matter can go no further.

Therefore, the fact that an application may provide a better way of
conducting business is not relevant. In [Halliburton Energy Services
Inc's Applications \[2012\] RPC
129](https://doi.org/10.1093/rpc/rcs019){rel="external"}, HHJ Birss QC
at paragraph 35 noted that the use of a computer to implement a better
business method did not confer patentability:

> The business method cases can be tricky to analyse by just asking
> whether the invention has a technical effect or makes a technical
> contribution. The reason is that computers are self evidently
> technical in nature. Thus when a business method is implemented on a
> computer, the patentee has a rich vein of arguments to deploy in
> seeking to contend that his invention gives rise to a technical effect
> or makes a technical contribution. For example the computer is said to
> be a faster, more efficient computerized book keeper than before and
> surely, says the patentee, that is a technical effect or technical
> advance. And so it is, in a way, but the law has resolutely sought to
> hold the line at excluding such things from patents.

### 1.34.1 {#ref1-34-1}

Inventions may, however, relate in some way to a business method and yet
avoid exclusion. Following the approach of Birss J in [Lenovo
(Singapore) PTE Ltd v Comptroller General of Patents \[2020\] EWHC 1706
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2020/1706.html){rel="external"},
"it is useful to ask: what more is the ... invention than a method of
doing business?" If the answer to this question is nothing more than a
computer program running on a conventional computer system, as was the
case in Merrill Lynch discussed above, then the invention is excluded.
In Lenovo the invention (which concerned a contactless card payment
system) was found to be more than a business method as such. Birss J
held that the invention involved "a different physical interaction with
the world outside of the computer" that was "technical in character and,
in the context of the invention as a whole" it was "not just one of the
normal incidents of a conventional computer" (see also 1.38.1).
Similarly, Jacob LJ when considering the invention of Aerotel in
paragraph 53 of his judgment, considered the then invention to be "more
than just a method of doing business as such." Considering the
contribution of Aerotel's claimed telephone system, they noted that "the
\[telephone\] system as a whole is new. And it is new in itself not
merely because it is to be used for the business of selling phone
calls." Even though the telephone system of Aerotel could have been
"implemented using conventional computers" Jacob LJ held that "the
contribution is a new \[telephone\] system" because it was "a new
combination of hardware" in the form of a telephone system including a
"special exchange". Notably the High Court in [Aerotel Ltd v Wavecrest
Group Enterprises Ltd & Ors \[2008\] EWHC 1180
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2008/1180.html){rel="external"}
later found the Aerotel patent to be excluded when this "special
exchange" was shown to be conventional but for its particular payment
scheme (see 1.20.1). Therefore, when assessing whether a particular
invention relates to a new system or a 'new arrangement of hardware', it
should be asked whether the system is new in itself or whether the
system is only new due to the business method it performs.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Computer programs]{#default-id-4bae80f7-heading-26 .govuk-accordion__section-button} {#computer-programs .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Computer programs\",\"index_section\":26,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-26 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-26" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Computer programs\",\"index_section\":26,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.35 {#ref1-35}

Where a claim involves the use of a computer program, it does not
naturally follow that the claim must be excluded. For example, as stated
by Neuberger LJ in his judgment in [Symbian Ltd's Application \[2009\]
RPC 1](https://doi.org/10.1093/rpc/rcn033){rel="external"} at paragraph
48:

> The mere fact that what is sought to be registered is a computer
> program is plainly not determinative. Given that the Application seeks
> to register a computer program, the issue has to be resolved by
> answering the question whether it reveals a "technical" contribution
> to the state of the Art.

Instead, the contribution of a claim to a computer program must be
assessed by reference to the process the program will cause a computer
to perform, because the assessment is based on the substance of the
invention, as stated in [Astron Clinica Ltd & Ors v The Comptroller
General of Patents, Designs and Trade Marks \[2008\] RPC
14](http://www.bailii.org/ew/cases/EWHC/Patents/2008/85.html){rel="external"}
([see 1.13](#ref1-13)). In [Halliburton Energy Services Inc's
Applications \[2012\] RPC
129](https://doi.org/10.1093/rpc/rcs019){rel="external"}, HHJ Birss QC
emphasised that "\[a\] computer programmed to perform a task which makes
a contribution to the art which is technical in nature is a patentable
invention and may be claimed as such." Therefore, a computer program
that provides a technical contribution will not fall under the
exclusion, as it is more than a computer program as such. Although an
invention involving a computer is undoubtedly "technical", the mere
presence of conventional computing hardware does not of itself mean the
invention makes a technical contribution (and so avoids the computer
program exclusion) as such hardware will typically not form part of the
contribution ([see 1.21.1](#ref1-21-1)).

### 1.36 {#ref1-36}

Following the development of the Aerotel test, [Symbian Ltd's
Application \[2009\] RPC
1](https://doi.org/10.1093/rpc/rcn033){rel="external"} more closely
examined the question of "technical contribution" as it related to
computer programs ([see 1.24](#ref1-24)). In examining the case law, the
Court of Appeal concluded that creating a precise test for determining
whether a computer program was excluded was difficult, and to suggest
that such a clear rule existed was perhaps dangerous when considering
the pace of technological development. It emphasised at paragraph 52 the
need to determine each case by reference to its particular facts and
features ([see 1.12](#ref1-12)) bearing in mind the "most reliable
guidance" of the decisions they had set out earlier at paragraph 49:

> In deciding whether the Application reveals a "technical"
> contribution, it seems to us that the most reliable guidance is to be
> found in the Board's analysis in Vicom and the two IBM Corp. decisions
> \[T 0006/83 and T 0115/85\], and in what this court said in Merrill
> Lynch and Gale. Those cases involve a consistent analysis, which
> should therefore be followed unless there is a very strong reason not
> to do so.

### 1.36.1 {#ref1-36-1}

The court held that the contribution made by the invention in Symbian
was not a computer program as such because "it has the knock-on effect
of the computer working better as a matter of practical reality".
Paragraphs 54-56 emphasised the need to look at the practical reality of
what the program achieved, and to ask whether there was something more
than just a "better program". An invention which either solves a
technical problem external to the computer or solves one within the
computer was not considered to fall under the computer program
exclusion. The particular invention involved improving the operation of
a computer by solving a problem arising from the way the computer was
programmed (in that case, a tendency to crash due to conflicting library
program calls). The court considered that this could be regarded as
solving a technical problem within the computer, if it leads to a more
reliable computer. Thus, a program that results in a computer running
faster or more reliably may be considered to provide a technical
contribution, even if the invention addresses a problem in the
programming of the computer. The court concluded that such a technical
contribution rendered the claim in this case patentable.

### 1.36.2 {#ref1-36-2}

The matter of assessing the technical merit of the claimed invention was
also considered in [HTC v Apple \[2013\] EWCA Civ
451](http://www.bailii.org/ew/cases/EWCA/Civ/2013/451.html){rel="external"}.
In paragraph 49, Kitchin LJ reiterated the point from Symbian that a
solution to a technical problem would be a relevant technical effect and
would not be excluded, as technical character is provided from the
problem itself (see also [1.38.5](#ref1-38-5)). In paragraph 152,
Lewison LJ considered the scope of the exclusion, and remarked that:

> Since each case must be determined by reference to its particular
> facts and features (Symbian at \[52\]) we need to begin by asking:
> what does the computer program in this case actually contribute?

In this instance, they concluded that the program made it easier to
write programs for applications to be run on the device that contains
it, and that writing software in this instance did not fall within the
scope of the exclusion, although the ultimate product (i.e. the software
itself) may still do so.

### 1.36.3 {#ref1-36-3}

In paragraphs 45-49 of [HTC v
Apple](http://www.bailii.org/ew/cases/EWCA/Civ/2013/451.html){rel="external"},
Kitchin LJ sets out a summary of how to determine if an invention has
made a technical contribution to the art:

"How then is it to be determined whether an invention has made a
technical contribution to the art? A number of points emerge from the
decision in Symbian and the earlier authorities to which it refers.
First, it is not possible to define a clear rule to determine whether or
not a program is excluded, and each case must be determined on its own
facts bearing in mind the guidance given by the Court of Appeal in
Merrill Lynch and Gale and by the Boards of Appeal in Case T 0208/84,
Vicom Systems Inc/Computer-related invention \[1987\] OJ EPO 14,
\[1987\] 2 E.P.O.R. 74 ; Case T 06/83, IBM Corporation/Data processing
network \[1990\] OJ EPO 5, \[1990\] E.P.O.R.91 and Case T 115/85, IBM
Corporation/Computer-related invention \[1990\] E.P.O.R. 107.

Second, the fact that improvements are made to the software programmed
into the computer rather than hardware forming part of the computer does
not make a difference. As I have said, the analysis must be carried out
as a matter of substance not form.

Third, the exclusions operate cumulatively. So, for example, the
invention in Gale related to a new way of calculating a square root of a
number with the aid of a computer and Mr Gale sought to claim it as a
ROM in which his program was stored. This was not permissible. The
incorporation of the program in a ROM did not alter its nature: it was
still a computer program (excluded matter) incorporating a mathematical
method (also excluded matter). So also the invention in Macrossan
related to a way of making company formation documents and Mr Macrossan
sought to claim it as a method using a data processing system. This was
not permissible either: it was a computer program (excluded matter) for
carrying out a method for doing business (also excluded matter).

Fourth, it follows that it is helpful to ask: what does the invention
contribute to the art as a matter of practical reality over and above
the fact that it relates to a program for a computer? If the only
contribution lies in excluded matter then it is not patentable.

Fifth, and conversely, it is also helpful to consider whether the
invention may be regarded as solving a problem which is essentially
technical, and that is so whether that problem lies inside or outside
the computer. An invention which solves a technical problem within the
computer will have a relevant technical effect in that it will make the
computer, as a computer, an improved device, for example by increasing
its speed. An invention which solves a technical problem outside the
computer will also have a relevant technical effect, for example by
controlling an improved technical process. In either case it will not be
excluded by art.52 as relating to a computer program as such."

The AT&T signposts

### 1.37 {#ref1-37}

Although the Aerotel test involves the examiner checking "whether the
actual or alleged contribution is actually technical in nature" - and
[Symbian](https://doi.org/10.1093/rpc/rcn033){rel="external"} actively
requires the question "does the invention make a technical contribution"
to be asked when considering the computer program exclusion - the
judgment in Aerotel/Macrossan does not provide detailed guidance on how
this is to be carried out in practice. Examiners should remember that
there is no single or clear rule for determining whether a technical
contribution is revealed (see [1.36](#ref1-36). However, in paragraph 40
of [AT&T Knowledge Ventures/Cvon Innovations v Comptroller General of
Patents \[2009\] EWHC 343
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2009/343.html){rel="external"}
(AT&T/CVON), Lewison J (as they then was) considered the decisions
forming the "most reliable guidance" of Symbian (s1.36), tried to distil
the essence of what they revealed, and set out five signposts that they
considered to be helpful when considering whether a computer program
makes a relevant technical contribution. In [HTC v Apple \[2013\] EWCA
Civ
451](http://www.bailii.org/ew/cases/EWCA/Civ/2013/451.html){rel="external"},
Lewison LJ reconsidered the signposts they had suggested in AT&T/CVON
and, in light of the decision in [Gemstar-TV Guide International Inc v
Virgin Media Ltd \[2010\] RPC
10](http://rpc.oxfordjournals.org/content/127/4/257.abstract){rel="external"}
(see [1.37.1](#ref1-37-1) and [1.40.4](#ref1-40-4)), felt that the
fourth signpost had been expressed too restrictively.

### 1.37.1 {#ref1-37-1}

Drawing on the cases identified as the "most reliable guidance" in
Symbian (see [1.36](#ref1-36)) as key to understanding whether an
invention reveals a relevant technical contribution, the signposts are
therefore a distillation of the reasoning and rationale of this previous
case law. (The decisions that each signpost derives from can be found in
brackets following each signpost below).

The signposts are:-

(i). whether the claimed technical effect has a technical effect on a
process which is carried on outside the computer (from Vicom)

(ii). whether the claimed technical effect operates at the level of the
architecture of the computer; that is to say whether the effect is
produced irrespective of the data being processed or the applications
being run (from IBM T 0006/83, IBM T 0115/85, Merrill Lynch, Symbian)

(iii). whether the claimed technical effect results in the computer
being made to operate in a new way (from Gale)

(iv). whether the program makes the computer a better computer in the
sense of running more efficiently and effectively as a computer (from
Vicom, [Symbian](https://doi.org/10.1093/rpc/rcn033){rel="external"}; as
reworded in HTC v Apple)

(v). whether the perceived problem is overcome by the claimed invention
as opposed to merely being circumvented (from Hitachi T 0258/03)

In paragraph 41 of AT&T/CVON, Lewison J added a caveat to the signposts.
"If there is a technical effect in this sense, it is still necessary to
consider whether the claimed technical effect lies solely in excluded
matter." It can be seen that the signposts and Lewison J's caveat
reflect both steps 3 and 4 of the Aerotel test (albeit in reverse
order).

### 1.37.2 {#ref1-37-2}

It should be clear that the signposts are merely guidelines; although
they provide a useful aid for the examiner in assessing the technical
character of a claimed invention, they were not intended to provide a
definitive test (as Lewison LJ's obiter remarks in paragraph 149 of [HTC
v
Apple](http://www.bailii.org/ew/cases/EWCA/Civ/2013/451.html){rel="external"}
make clear). Several judgments have emphasised this point - John Baldwin
QC (sitting as a Deputy Judge) in [Really Virtual Co Ltd v UK
Intellectual Property Office \[2013\] RPC
3](https://doi.org/10.1093/rpc/rcs057){rel="external"} noted that the
signposts, although useful, are no more than signposts and that there
will be some cases in which they are more helpful than in others.
Kitchin LJ made similar remarks in paragraph 51 of HTC v Apple -- their
usefulness does not mean they will be determinative in every case.
However, if the claimed invention fails all of the signposts, it will
likely be a good indicator that it may be nothing more than a computer
program as such.

-   \[If referring to the AT&T signposts in an examination report,
    examiners may find it useful to use PROSE clause RC4A.\]

### 1.37.3 {#ref1-37-3}

Even though the signposts must be treated as guidelines, examiners
should remember that each case must be determined on its own facts
bearing mind the case law forming the "most reliable guidance"
identified in Symbian (see 1.36) from which the signposts are distilled.
For example, in allowing Fisher-Rosemount Systems, Inc.'s Application
[BL
O/346/19](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/346/19){rel="external"}
the Hearing Officer accepted an argument that the invention was not
excluded based on the guidance of the decision of the board of appeal in
IBM Corp. T 0115/85.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Interpretation of the signposts]{#default-id-4bae80f7-heading-27 .govuk-accordion__section-button} {#interpretation-of-the-signposts .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Interpretation of the signposts\",\"index_section\":27,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-27 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-27" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Interpretation of the signposts\",\"index_section\":27,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.38 {#ref1-38}

How these signposts are to be interpreted is often the subject of
discussion in excluded matter cases, and hearing decisions and judgments
have considered the underlying meaning of all of the signposts at one
point or another. The hearing officer in [Commonwealth Scientific and
Industrial Research Organisation's Application (BL
O/367/11)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F367%2F11&submit=Go+%BB){rel="external"}
took the opportunity to consider each signpost in turn (although the
fifth signpost was not addressed in the hearing, and so was only
considered briefly) and explained her view on how each was to be
interpreted in paragraphs 31-42.

### 1.38.1 {#ref1-38-1}

The first signpost asks whether the claimed technical effect has a
technical effect on a process which is carried on outside the computer.
This signpost reflects the essential reasoning of the board of appeal in
paragraph 16 of Vicom:

> Generally speaking, an invention which would be patentable in
> accordance with conventional patentability criteria should not be
> excluded from protection by the mere fact that for its implementation
> modern technical means in the form of a computer program are used.
> Decisive is what technical contribution the invention as defined in
> the claim when considered as a whole makes to the known art.

To meet the first signpost, the process carried out by the program must
be, or must operate on, something external to the computer on which the
program is being run. As Lewison J put it in his summarisation of Vicom
in paragraph 20 of AT&T/CVON, if the process would be patentable if it
were not operated by means of a computer program, the fact that it is
operated by means of a computer program does not render it excluded:

> What the Board are saying in this paragraph is, I think, that you
> assess the patentability of a claimed invention ignoring the fact that
> it operates through a computer program. If, ignoring the computer
> program, it would be patentable, then the fact that a computer drives
> the invention does not deprive it of patentability:

An often-used, if limited, example is that of a computer-controlled car
braking system -- the braking system is external to the computer, and
the programming has an effect on that system. In [Gemstar--TV Guide
International Inc v Virgin Media Limited \[2010\] RPC
10](http://rpc.oxfordjournals.org/content/127/4/257.abstract?sid=d46cf02e-1c1b-4516-91f6-0206745be31a){rel="external"},
Mann J held that Gemstar's "transfer patent" provided an effect in the
outside world. The patent related to a method of using an interactive
program guide to cause a television program to be recorded on a digital
storage device and to initiate its transfer to a secondary storage
device. Mann J held (at 234) that the initiation of movement of the
recorded data from one disk to another was a physical effect that was
enough to prevent the invention from being excluded as a computer
program as such.

Physical interactions may give rise to technical effects upon processes
''outside the computer'' in accordance with signpost one. In \[Lenovo
(Singapore) v. Comptroller General of Patents \[2020\] EWHC 1706
(Pat)\[(https://www.bailii.org/ew/cases/EWHC/Patents/2020/1706.html)
Birss J identified, that signpost one was relevant (paragraph 23) and
that the key question was whether Lenovo's invention involved a
different physical interaction with the world outside the computer, as
compared to what had gone before (paragraph 36). In Lenovo's invention,
the choice of contactless payment cards for splitting a payment between
them was handled automatically at a point of sale terminal, based on a
user's preferences that had already been acquired and stored elsewhere.
Birss J held that as a result of this automatic feature, a card clash
problem experienced with contactless payment cards was solved without
the user having to take any extra physical step at the point they use
their contactless cards. Birss J held that this different physical
interaction was an effect that was not a computer program (nor a
business method) and was technical in nature.

However, as stated by Arden LJ in her judgment of the Court of Appeal in
[Lantana v Comptroller-General of Patents \[2014\] EWCA Civ
1463](http://www.bailii.org/ew/cases/EWCA/Civ/2014/1463.html){rel="external"}
at paragraph 47, the first signpost will not be answered simply by
referencing "the fact that a program will have a practical effect
outside of a computer". One should consider all the requirements for
there to be a technical contribution when considering signpost one.

Signpost one makes reference to 'the computer'', but this does not mean
that any effect taking place outside a single computing device meets the
signpost -- systems operating as a network can be considered as 'the
computer' for the purposes of this signpost, as emphasised by Birss J in
paragraph 30 of [Lantana v Comptroller-General of Patents \[2013\] EWHC
2673
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2013/2673.html){rel="external"}.
Thus 'the computer' of signpost one may be read as a computer system or,
by extension, a system of computers.

### 1.38.2 {#ref1-38-2}

The second signpost asks whether the claimed technical effect operates
at the architectural level of the computer. In practice, this means in
the sense of the operation of the internal workings of a computer: the
processor, the cache memory, or other internal components of the
computer. The "architecture" can be thought of as the combination of
these components, which operate in the same way regardless of the
application being run. If the effect being produced would provide a
benefit to any software program which runs on the system, it is likely
to meet this signpost. If the effect being produced is specific to a
particular data set, type of data, or benefits only particular
applications, it is likely it will fail to meet this signpost. The
hearing officer in [Intuit Inc's Application (BL
O/347/10)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F347%2F10&submit=Go+%BB){rel="external"}
considered that an application programming interface operating between
layers of an application model did not meet the signpost, as it did not
provide internal control of the architectural components or operate at a
sufficiently high level of generality within the computer.

### 1.38.3 {#ref1-38-3}

The third signpost emphasises that the effect must be more than just the
running of a program or application on a general purpose computer -- the
computer itself must operate differently than it did before as a result
of the program being run. As Lewison J effectively noted in paragraph 31
of AT&T this signpost "points towards some generally applicable method
of operating a computer rather than a way of handling particular types
of information".

### 1.38.4 {#ref1-38-4}

The fourth signpost was reframed in HTC v Apple, and it reflects the
essential reasoning of
[Symbian.](https://doi.org/10.1093/rpc/rcn033){rel="external"}. As
Lewison J stated in paragraph 34 of AT&T/CVON:

> In Symbian itself, the invention was patentable because it resulted in
> a faster and more reliable computer. The increase in speed and
> reliability was not, as I understand the invention, dependent of the
> type of data being processed or the particular application being used
> to do the processing. The invention operated at a much higher level of
> generality within the computer.

For the fourth signpost to be met the computer must operate more
efficiently and effectively as a result of running the program. Again,
this must be the computer as a whole, rather than the individual
program. In several cases, such as [Q Software Global Ltd 's Application
(BL
O/120/11)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F120%2F11&submit=Go+%BB){rel="external"}
and [JDA Software Group Inc 's Application (BL
O/386/12)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F386%2F12&submit=Go+%BB){rel="external"},
it was argued that the program required less processing power to run, or
operated faster, and the system was therefore more efficient. This was
not considered to meet the signpost, as the system itself remained
unchanged -- the computer processed the data in the same way as it did
before, the program merely made more efficient use of the hardware.

### 1.38.5 {#ref1-38-5}

The fifth signpost looks at the technical character of an alleged
invention by means of the problem addressed. When the problem is a
technical one, the alleged invention can be considered to have a
technical nature leading to it falling outside the exclusion if (but not
only if) it solves the problem.\
In [Lantana Ltd v The Comptroller-General of Patents, Designs and Trade
Marks \[2013\] EWHC 2673
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2013/2673.html){rel="external"}
Birss J considered the fifth signpost, and stated that "\[i\]t makes
sense to think of something which is a solution to a technical problem
as itself having technical character because it takes that character
from the technical nature of the problem to be solved. But if a thing is
not solving the technical problem but only circumventing it, then that
thing cannot be said to have taken any technical character from the
problem".\
Circumventing a technical problem does not automatically imply that an
alleged invention is excluded, but indicates that one cannot rely on the
addressed problem to deduce its technical character. At paragraph 51 of
the subsequent Court of Appeal decision, [Lantana v Comptroller-General
of Patents \[2014\] EWCA Civ
1463](http://www.bailii.org/ew/cases/EWCA/Civ/2014/1463.html){rel="external"},
Arden LJ noted that "\[c\]ircumvention may be the result of truly
original linear thinking and may lead to patentability in an appropriate
case". This does not happen when circumvention consists of conventional
means, as reaffirmed by Kitchin LJ at paragraphs 68 and 70 of the same
judgment: "\[o\]verall, the invention avoids the problem...but it does
so by using a conventional technique \[... i\]n other words it does not
solve those problems but circumvents them".\
Similarly, if the problem to be solved is not a technical problem, the
solution cannot take technical character from the problem, although it
may have some other technical effect. In the EPO board of appeal
decision T 0258/03 Hitachi/Auction Method, it was argued that a
technical effect resided in overcoming the technical problem of delays
in propagating information in an online auction system. Yet the board
held that the solution of adapting a known auction method such that it
could performed automatically was not a technical solution to this delay
problem. In paragraph 35 of AT&T, Lewison J thought it useful to refer
to the board's decision in Hitachi. Whilst acknowledging the board's
decision did not follow the approach of Aerotel, he believed their
decision shed some light on what the board considers to be a technical
effect. In doing so, Lewison J quoted from the board's decision in
Hitachi, adding his own emphasis:

> Method steps consisting of modifications to a business scheme and
> aimed at circumventing a technical problem rather than solving it by
> technical means cannot contribute to the technical character of the
> subject-matter claimed.

The fifth signpost appears to reflect the essential reasoning of Lewison
J's quotation from Hitachi. Other examples where circumvention also
lacks technical character are given in [Direct TV Pty's Application (BL
O/150/11)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F150%2F11&submit=Go+%BB){rel="external"},
(paragraphs 32-33) and [Apple Inc 's Application (BL
O/244/13)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F244%2F13&submit=Go+%BB){rel="external"}
, paragraphs 38-39), amongst others, where it was argued that the
problem of bandwidth limitations in transmitting data across a network
was solved by the invention. In each case, this was achieved by reducing
the amount of data transmitted. The Hearing Officers considered that to
circumvent the problem, as there was no change to the way in which the
data was transmitted, merely the volume.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Assessing "technical effect" using the AT&T signposts]{#default-id-4bae80f7-heading-28 .govuk-accordion__section-button} {#assessing-technical-effect-using-the-att-signposts .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Assessing “technical effect” using the AT\\u0026T signposts\",\"index_section\":28,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-28 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-28" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Assessing “technical effect” using the AT\\u0026T signposts\",\"index_section\":28,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.39 {#ref1-39}

Determining whether a computer program provides a technical effect such
that it does not fall within the exclusion usually (but not always; [see
1.37.2](#ref1-37-2)) involves assessing the contribution against the
AT&T signposts (in the form stated in HTC v Apple). Decisions since AT&T
have often used the signposts when answering step 3 of the Aerotel test.

### 1.39 {#ref1-39-1}

In [Protecting Kids the World Over (PKTWO) Ltd's Patent Application
\[2012\] RPC 13](https://doi.org/10.1093/rpc/rcs022){rel="external"},
the invention was found to solve a technical problem lying outside the
computer, namely how to improve the generation of an alarm in response
to inappropriate communication, and therefore was not excluded from
patentability.

### 1.39.2 {#ref1-39-2}

In [HTC v Apple \[2013\] EWCA Civ
451](http://www.bailii.org/ew/cases/EWCA/Civ/2013/451.html){rel="external"},
a method of handling the recognition of single- and multi-touch events
in devices programmed to do so had the advantage that application
programmers could more easily write software to accommodate such
recognition. Although considering that the contribution fell outside the
scope of the exclusion in any event (see [1.36.2](#ref1-36-2)), Lewison
LJ went on (in paragraph 154) to assess the contribution against the
AT&T signposts, and found that it met all but the third signpost, which
pointed to the program providing a technical effect in itself. Kitchin
LJ similarly found, in paragraph 57, that the recognition handling
operated at the architectural level, and caused the devices to operate
in a new way. The application was found to fall outside excluded matter.

### Artificial intelligence

### 1.39.3 {#ref1-39-3}

Inventions involving artificial intelligence ("AI") are generally
computer-implemented, so their assessment in respect of excluded matter
will therefore mirror that of any computer-implemented invention. For
example, if the invention uses conventional hardware (see
[1.21.1](#ref1-21-1)) the assessment will focus on the process that the
hardware is programmed to perform considering all the guidance found
above in [1.35-1.39.2](#ref1-35). Inventions involving AI may also be
viewed as involving a mathematical method or algorithm so the guidance
under [1.29-1.29.5](#ref1-29) should also be considered. If an AI
invention is directed to a specific technical process outside of a
computer or if it forms part of the internal workings of a computer,
then it will likely not be excluded. However, where an AI invention is
directed to an excluded process (e.g. a business method) and there is
nothing more to it, it will likely be excluded. Likewise, if an AI
invention is directed to a non-specific purpose it may also fail by way
of encompassing excluded processes [(see 1.15 & 1.19](#ref1-15)). AI
inventions or algorithms that provide an improvement in programming will
likely be excluded following Gale unless they make a technical
contribution [(see e.g. 1.29.4](#ref1-29-4)).

### 1.39.4 {#ref1-39-4}

In [Emotional Perception AI Ltd v Comptroller-General of Patents,
Designs and Trade Marks \[2023\] EWHC 2948
(Ch)](https://www.bailii.org/ew/cases/EWHC/Ch/2023/2948.html){rel="external"},
the High Court held that an artificial neural network (ANN), whether
implemented in hardware or software, was not a program for a computer
(see \[58\]), so the claimed invention involving a particular method of
training and using an ANN did not invoke the computer program exclusion
(see \[61\]). Nonetheless, the judge also considered whether the trained
ANN was capable of being an external technical effect and accepted that
it is capable. This prevented the computer program exclusion from
applying to the remainder of the invention (see \[78\]). Following the
Emotional Perception judgment, examiners should not use the program for
a computer exclusion to object to inventions involving ANNs.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Section (d) -- Presentation of information]{#default-id-4bae80f7-heading-29 .govuk-accordion__section-button} {#section-d--presentation-of-information .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Section (d) – Presentation of information\",\"index_section\":29,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-29 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-29" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Section (d) – Presentation of information\",\"index_section\":29,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.40 {#ref1-40}

Any manner, means or method of expressing information which is
characterised solely by the content of the information is excluded,
regardless of how it is represented. The fact that physical apparatus
may be involved in the presentation will not usually be enough to avoid
the exclusion. In [Townsend 's Application \[2004\] EWHC 482
(Pat)](http://www.bailii.org/cgi-bin/markup.cgi?doc=/ew/cases/EWHC/Patents/2004/482.html){rel="external"},
Laddie J held that the exclusion encompassed both the provision of
information and its expression. In [Autonomy Corp Ltd v Comptroller
General of Patents, Trade Marks & Designs \[2008\] EWHC
146(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2008/146.html){rel="external"},
the court held that choosing where and how to display information was
still the presentation of information, as it is part of the decision as
to how to present the information.

### 1.40.1 {#ref1-40-1}

If the invention relates to the technical means by which the information
is presented, rather than the presentation itself, then it may not fall
within the exclusion, as per [BBC/Colour television signal \[1990\] EPOR
599 (T
0163/85)](http://www.epo.org/law-practice/case-law-appeals/recent/t850163ep1.html){rel="external"},
where the EPO Technical Board of Appeal held that a colour television
signal, characterised by the technical features of the television system
in which the signal occurs, was not excluded under presentation of
information. However, the Board distinguished this from a television
system which was defined solely by the information (e.g. moving
pictures) being transmitted using a standard television signal, which
they felt may fall within the exclusion.

### 1.40.2 {#ref1-40-2}

In other words, if there is a technical effect or a contribution in a
non- excluded field, the invention will not lie solely within excluded
matter. In Townsend, an application for an invention relating to an
advent calendar with an additional indicator (such as a word, picture,
colour etc.) on each door was refused by the Hearing Officer in [BL
O/266/03](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F266%2F03&submit=Go+%BB){rel="external"}
(and, on appeal, the Patents Court) as relating to the presentation of
information which served no technical purpose and included no technical
advance. [TDK Electronics Co Ltd's Application (BL
O/097/83)](https://www.gov.uk/government/publications/patent-decision-009783)
held that a claim to a tape cassette of conventional construction, but
with differentially coloured poles, was excluded because it encompassed
a cassette where the poles were coloured subsequent to assembly, and
thus did not serve any function in its assembly or use. A gaming machine
in which logos or brand or product names were substituted for the
conventional symbols normally depicted on the reels of a fruit machine
was also found to fall within the exclusion in Ebrahim Shahin's
Application (BL O/149/95).

### 1.40.3 {#ref1-40-3}

A claim to a conventional package containing a known product,
characterised solely by the instructions on the package, will not
generally be allowed, since the contribution relates solely to the
presentation of information. Several cases along these lines were
rejected under the 1949 Act, including [Dow Corning Corporation
(Bennett's) Application \[1974\] RPC
235](https://doi.org/10.1093/rpc/91.8.235){rel="external"}, and
[Ciba-Geigy AG (Durr's) Application \[1977\] RPC
83](https://doi.org/10.1093/rpc/1977rpc83){rel="external"}.

### 1.40.4 {#ref1-40-4}

[Gemstar--TV Guide International Inc v Virgin Media Limited \[2010\] RPC
10](http://rpc.oxfordjournals.org/content/127/4/257.abstract?sid=d46cf02e-1c1b-4516-91f6-0206745be31a){rel="external"}
considered the question of computerised methods for presenting
information. The case related to three EP(UK) patents covering various
aspects of Electronic Program Guides (EPGs). In his judgment, Mann J
emphasised that the exclusion was not solely confined to the content of
information, and that, in order for the exclusion not to apply, there
must be some technical effect beyond the information being presented.
The court also stated that if the contribution is defined only in terms
of the information to be presented, then that is a presentation of
information - the presence of a display does not change this. In
particular, a better (or new) user interface was not considered to be a
relevant technical effect -- the rearrangement of information was
nothing more than the presentation of information; simply having
something different displayed was not sufficient to overcome the
exclusion. Two of the three patents were found to be excluded as they
lacked the required technical effect; the third presented a physical
effect, namely the movement of data between hard disks, such that there
was more than merely the presentation of information.

### Exploitation contrary to public policy or morality

  -------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 1(3)**
  A patent shall not be granted for an invention the commercial exploitation of which would be contrary to public policy or morality.
  -------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 1(4)**
  For the purposes of subsection (3) above exploitation shall not be regarded as contrary to public policy or morality only because it is prohibited by any law in force in the United Kingdom or any part of it.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 1.41 {#ref1-41}

Sections 1(3) and 1(4) were amended by the Patents Regulations 2000 (SI
2000 No.2037) so that the wording would more closely reflect the wording
of article 27(2) of the TRIPS agreement. Section 1(3)(a) had previously
stated that a patent would not be granted for an invention whose
"publication or exploitation" would "be generally expected to encourage
offensive, immoral or antisocial behaviour". In practical terms, the
effect of s.1(3) remains the same, which is to prevent the grant of
patent rights for inventions which the general public would regard as
abhorrent or from which the public need protection. It provides a
reasonably objective test which has to be applied to each invention and
its particular set of facts and circumstances. Clearly what is to be
regarded as contrary to public policy or morality will vary according to
changes in social attitudes and on no account ought examiners to allow
their own personal and individual beliefs to colour their judgment on
this matter. The decision of Aldous J in the case of [Masterman's Design
\[1991\] RPC 89](https://doi.org/10.1093/rpc/1991rpc89){rel="external"}
under a similar provision of the Registered Designs Act 1949 deals with
issues broadly corresponding with those which may arise under s.1(3).
The Patents Act 1977 (Isle of Man) Order 2003 (SI 2003 No. 1249) amended
sections 1(3) and 1(4) for the Isle of Man.

(Only in the clearest cases should examiners invoke this subsection and
then only following consultation with their Group Head. Any genuine
doubt should be exercised in favour of the applicant with an appropriate
minute being created).

### 1.42 {#ref1-42}

Unlike under the previous s.1(3)(a), the exclusion from patentability is
not activated if mere publication of the invention, as distinct from its
exploitation, would be contrary to morality. If, however, the
specification includes matter the publication or exploitation of which
would generally be expected to encourage offensive, immoral or
antisocial behaviour, then (irrespective of whether the invention itself
is open to objection under s.1(3)) the situation can be dealt with by
excision of the offending matter under s.16(2) - [see
16.34-16.37](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-34)

### 1.43 {#ref1-43}

European Patent Convention section a.53(a) is also relevant

The corresponding provision of the EPC ([see 1.06](#ref1-06)) refers to
"inventions the publication or exploitation of which would be contrary
to 'ordre public' or morality". In the Harvard ["oncomouse" case T
315/03](http://www.epo.org/law-practice/case-law-appeals/recent/t030315ex1.html){rel="external"}
(\[2006\] 1 OJEPO 15, \[2005\] EPOR 31) (see also 76A.02.1 and 76A.05),
the Board of Appeal endorsed the definitions of "ordre public" and
morality provided in [Plant Genetic Systems T 356/93 \[1995\] 8 OJEPO
393](http://www.epo.org/law-practice/case-law-appeals/recent/t930356ex1.html){rel="external"}
and held that the assessment of these concepts should be made as of the
filing or priority date of the application. The concept of "ordre
public" was accepted as covering the protection of public security and
the physical integrity of individuals as part of society, and
encompassed the protection of the environment. In relation to morality,
the Board in T 356/93 held that the culture inherent in European society
and civilisation should be the basis for determining what behaviour is
right and acceptable, and what behaviour is wrong. However, the Board in
oncomouse added that in making an assessment of morality, no single
definition of morality based on e.g. economic or religious principles
represents an accepted standard in European culture, and opinion poll
evidence was of little value. For animal manipulation cases, the Board
of Appeal in T 315/03 endorsed the guidance provided in its earlier
consideration of the Harvard "oncomouse" application (case T 19/90)
\[1990\] 12 OJEPO 476). This case held that the possible detrimental
effects and risks had to be weighed and balanced against the merits and
advantages aimed at. In particular, the basic interest of mankind to
remedy disease had to be set against the protection of the environment
of uncontrolled dissemination of unwanted genes and the avoidance of
suffering to animals, including the possibility of using non-animal
alternatives. In balancing these factors, the Board in T 315/03 allowed
claims covering transgenic "mice", refusing broader claims encompassing
rodents ([see
76A.02.1](/guidance/manual-of-patent-practice-mopp/section-76a-biotechnological-inventions/#ref76A-02))

### 1.44 {#ref1-44}

For biotechnology inventions, in addition to the general exclusion of
s.1(3), Schedule A2 of the Patents Act specifies that certain categories
of invention are not patentable inventions; these are discussed in
[76A.02-76A.06](/guidance/manual-of-patent-practice-mopp/section-76a-biotechnological-inventions/#ref76A-02))
and the [Examination Guidelines for Patent Applications relating to
Biotechnological Inventions in the Intellectual Property
Office](https://www.gov.uk/government/publications/examining-patent-applications-for-biotechnological-inventions).

### 1.45 {#ref1-45}

Section 1(4) is a rider to section 1(3) to make it clear that an act or
action prohibited by a law is not to be considered as necessarily the
same thing as contrary to public policy or morality. (One reason for
this is that a product which could not lawfully be used in the UK may be
manufactured lawfully in the UK for export to countries where its use is
not illegal). However the existence of a law or regulation may be a
material fact to be taken into consideration in determining whether to
refuse an application under s.1(3). The nature and probable uses of the
invention will need to be considered as well as the exact terms of the
prohibition. Thus if the prohibition is directed unconditionally to the
very act which the inventor proposes very careful deliberation must be
given as to whether to invoke s.1(3). In such cases a useful test is to
consider why the prohibition exists. For example it is considered that
the Landmines Act 1998 (implementing the Ottawa Convention) and the
Cluster Munitions (Prohibitions) Act 2010 (implementing the Convention
on Cluster Munitions) were passed because the public in the UK generally
now consider the development, manufacture and use of anti-personnel
mines and cluster munitions to be immoral. In addition, UK is a
signatory to other weapons conventions which prohibit categories of
weapon, including the Chemical Weapons Convention and the Biological and
Toxin Weapons Conventions; these have been implemented in UK law by the
Chemical Weapons Act 1996 and the Biological Weapons Act 1974
respectively. Again, the signing of the conventions and the passage of
the legislation indicate that the general feeling of the public in the
UK is that the production and use of these weapons is immoral. However,
it should be noted that both these Acts recognise that agents capable of
use as a chemical or biological weapon may have legitimate purposes. In
cases in which an invention can be exploited legally albeit in
accordance with stringent regulations, it would be very difficult to
argue that s.1(3) applies and the application for a patent refused.

\[Patent applications which may relate to weapons which are considered
contrary to public policy or morality are dealt with by Security
Section. Any concerns about such applications should be raised with
them.\]

\[Where an application relates to an anti-personnel mine or cluster
munition; anything which is stated to be a part for such a weapon; a
method relating to production, dispersal, concealment or delivery of
such a weapon; or anything which appears to be usable only as a part for
such a weapon (even if not specifically stated) the examiner should
consider whether any patentable matter would remain if all aspects
related to such weapons were deleted. The same question should be
considered for any application which appears to relate to chemical or
biological weapons or their production, dispersal, concealment or
delivery. However, it is important to remember that agents capable of
use as a chemical or biological weapon may have legitimate purposes.
Such agents may therefore be patentable providing they are capable of a
legitimate industrial application, and legitimate uses of such agents
may also be protected. Patent applications relating to protections
against, and treatments to counter, biological or chemical weapons are
not excluded under s.1(3).\]

\[At search stage, if the examiner determines that there is no
potentially patentable matter, they should issue a s.18(3) report
raising an objection under s.1(3) prior to search (see 17.96.1-17.96.4).
If an application has been searched but not granted, the examiner should
raise an objection under s.1(3) at the next opportunity.\]

\[If the application has not yet been published and it contains
potentially patentable matter, the examiner should prepare a copy
suitable for publication from the originally filed documents, deleting
all aspects relating to such prohibited weapons. This matter will not be
open to public inspection following publication and should not be kept
on the open part of the file. The applicant should be informed that this
has been done and that no patent will be granted relating to such
matter. The applicant has no appeal against this deletion but can argue
that it should be restored before grant. See 16.34-16.37.\]

\[As it is an offence under both the Landmines Act 1998 and the Cluster
Munitions (Prohibitions) Act 2010 to "assist, encourage or induce any
other person" to develop, manufacture, use or sell these weapons,
certain requests for administrative actions which will have an effect
outside the Office will be refused. Administrative actions we will
refuse to carry out are: supplying certified office copies, renewing a
previously granted patent, and forwarding EP or PCT applications to the
relevant authorities. However, if staff are unaware of the nature of the
application when they carry out these actions, they are not at risk of
committing an offence. If staff do become aware that a case that they
are handling relates to an anti-personnel landmine or cluster munition,
they should refer the matter to Security Section.\]

\[The Chemical Weapons Act 1996 and the Biological Weapons Act 1974 do
not have equivalent provisions which make it an offence for a person to
"assist, encourage or induce any other person" to develop, manufacture,
use or sell these weapons. It is therefore not considered that
administrative acts (as outlined above) would constitute offences under
these Acts. Nevertheless, concerns that either applications or granted
patents relate to such weapons should be raised with Security Section.\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Plant and animal varieties, and "essentially biological processes]{#default-id-4bae80f7-heading-30 .govuk-accordion__section-button} {#plant-and-animal-varieties-and-essentially-biological-processes .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Plant and animal varieties, and “essentially biological processes\",\"index_section\":30,\"index_section_count\":30}"}
:::

::: {#default-id-4bae80f7-content-30 .govuk-accordion__section-content aria-labelledby="default-id-4bae80f7-heading-30" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Plant and animal varieties, and “essentially biological processes\",\"index_section\":30,\"index_section_count\":30}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 1.46 {#ref1-46}

Prior to the Patents Regulations 2000, s.1(3)(b) set out that a patent
would not be granted for "any variety of animal or plant or any
essentially biological process for the production of animals or plants,
not being a micro-biological process or the product of such a process".
These exclusions remain in place, and are now found, along with others
which relate to biotechnological inventions, in Schedule A2 to the Act,
introduced by the Patents Regulations 2000 and made under section 76A of
the Act, which was also introduced by those Regulations ([see
76A.01-06](/guidance/manual-of-patent-practice-mopp/section-76a-biotechnological-inventions/#ref76A-01))
and the [Examination Guidelines for Patent Applications relating to
Biotechnological Inventions in the Intellectual Property
Office)](https://www.gov.uk/government/publications/examining-patent-applications-for-biotechnological-inventions).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 1(5)**
  The Secretary of State may by order vary the provisions of subsection (2) above for the purpose of maintaining them in conformity with developments in science and technology; and no such order shall be made unless a draft of the order has been laid before, and approved by resolution of, each House of Parliament.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 1.47 {#ref1-47}

The white paper "Patent Law Reform" (Cmnd 6000) noted that the patent
system "must evolve in response to changing conditions". This was done
under previous legislation by continually re-interpreting the
centuries-old definition of invention as "any manner of new
manufacture". The present Act controls what is to be regarded as an
invention for which a patent monopoly may be granted by means of the
definitions set out in the foregoing subsections. The present subsection
gives the necessary measure of flexibility to this control, whilst
reserving to Parliament the authority to approve it. This would be at
the instigation of the Secretary of State, who would normally take such
action following the established consultative processes.
:::
:::
:::
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
