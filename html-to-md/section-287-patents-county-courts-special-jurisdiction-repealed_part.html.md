::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 287: Patents county courts: special jurisdiction \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (287.01 - 287.03) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 287.01

Sections 287 to 292 of the CDP Act introduced a new arena for
proceedings concerning patents or designs and provided for patents cases
to be conducted in a county court, and patent agents to conduct such
litigation. Sections 287 to 292 were brought into force on 1 August
1989. Section 287 was amended by the Constitutional Reform Act 2005 to
provide a role for the Lord Chief Justice of England and Wales or a
judicial office holder appointed by them to exercise functions under the
section.

### 287.02

Section 287 set out the framework for the establishment of a county
court jurisdiction over patents and designs in England and Wales.

### 287.03

Section 287 has been repealed by Schedule 9, paragraph 30 of the Crimes
and Courts Act 2013.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
