::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 49: Provisions about licences under s.48 {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (49.01 - 49.07) last updated: July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 49.01

Section 48 provides for the making of an application to the comptroller,
by any person, for the grant of a compulsory licence under a patent or
the making of a "licences of right" entry in the register in respect of
the patent. Section 49 is a supplementary provision concerning licences
granted by order of the comptroller, or by virtue of such an entry, as a
result of such applications.

### 49.02

These sections are members of a group, 46 to 54, relating to "licences
of right" entries and compulsory licences. [See
46.02-07](/guidance/manual-of-patent-practice-mopp/section-46-patentee-s-application-for-entry-in-register-that-licences-are-available-as-of-right/#ref46-02)
for general discussion of this group.

### 49.03

The discussion in
[48.17-21](/guidance/manual-of-patent-practice-mopp/section-48-compulsory-licences-general/#ref48-17)
concerning s.48 and orders for the grant of licences thereunder also
applies in relation to s.49.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 49(1)**
  Where the comptroller is satisfied, on an application made under section 48 above in respect of a patent, that the manufacture, use or disposal of materials not protected by the patent is unfairly prejudiced by reason of conditions imposed by the proprietor of the patent on the grant of licences under the patent, or on the disposal or use of the patented product or the use of the patented process, he may (subject to the provisions of that section) order the grant of licences under the patent to such customers of the applicant as he thinks fit as well as to the applicant.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 49.04

Subsection (1) empowers the comptroller to order the grant of licences
to customers of the applicant under s.48 where the ground set out in the
first part of s.48A(1)(c) (WTO proprietor - [see
48A.23](/guidance/manual-of-patent-practice-mopp/section-48a-compulsory-licences-wto-proprietors/#ref48a-23))
or s.48B(1)(e) (non-WTO proprietor - [see
48B.16](/guidance/manual-of-patent-practice-mopp/section-48b-compulsory-licences-other-cases/#ref48b-16)
is established. (The terms "patented product" and "patented process"
used in the subsection are defined in s.130(1)).

  -----------------------------------------------------------------------
   

  **Section 49(2)**

  Where an application under section 48 above is made in respect of a
  patent by a person who holds a licence under the patent, the
  comptroller -\
  (a) may, if he orders the grant of a licence to the applicant, order
  the existing licence to be cancelled, or\
  (b) may, instead of ordering the grant of a licence to the applicant,
  order the existing licence to be amended.
  -----------------------------------------------------------------------

### 49.05

Section 48(3) allows an application under s.48 to be made by an existing
licensee under the patent in question. Section 49(2) makes special
provision regarding the grant, amendment or cancellation of licences by
order of the comptroller on such applications.

### \[Section 49(3) Repealed\] {#section-493--repealed}

### 49.06

Prior to the CDP Act, a licence ordered under s.48 could deprive the
patentee of their rights under the patent in question, or revoke all
existing licences. However, the comptroller did not in practice use this
power and it was removed by the CDP Act with effect from 1 August 1989.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 49(4)**
  Section 46(4) and (5) above shall apply to a licence granted in pursuance of an order under section 48 above and to a licence granted by virtue of an entry under that section as it applies to a licence granted by virtue of an entry under section 46 above.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 49.07 {#ref49-07}

The holder of a compulsory licence ordered under s.48 or of a licence
granted by virtue of a "licences of right" entry made under s.48 has the
same rights of suing for infringement as are conferred by s.46(4) and
(5), [see
46.79](/guidance/manual-of-patent-practice-mopp/section-46-patentee-s-application-for-entry-in-register-that-licences-are-available-as-of-right/#ref46-79).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
