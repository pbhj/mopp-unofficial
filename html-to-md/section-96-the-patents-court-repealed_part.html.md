::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 96: The Patents Court \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated: July 2012
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 96.01

Section 96 provided for the establishment of a Patents Court as part of
the High Court of Justice in England and Wales, to replace the old
Patents Appeal Tribunal.

### 96.02

Section 96 has been repealed by the Supreme Court Act 1981 (c.54) but
its contents have been substantially re-enacted by provisions of that
Act, which are quoted below. These provisions were amended by the
Constitutional Reform Act 2005 (c.4) to provide for the Lord Chief
Justice to appoint judges and deputy judges in the High Court.
Proceedings before the Patents Court are regulated by the Civil
Procedure Rules, particularly Part 63 titled "Patents and other
Intellectual Property Claims" (CPR63). This part was introduced into the
Civil Procedure Rules 1998 by the Civil Procedure (Amendment No 2) Rules
2002 (S.I. 2002 No. 3219) and entered into force on 1 April 2003. Part
63, together with the Practice Direction supplementing CPR 63,
superseded Practice Direction 49E. Further information concerning
procedures before the court are detailed in The Patents Court Guide
available through the website of the Court Service
(http://www.hmcourts-service.gov.uk/).

  -----------------------------------------------------------------------
   

  **Supreme Court Act 1981, section 6(1)**

  There shall be -\
  (a) as part of the Chancery Division, a Patents Court; and\
  (b) as parts of the King's Bench Division, an Admiralty Court and a
  Commercial Court.\
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Supreme Court Act 1981, section 62(1)**
  The Patents Court shall take such proceedings relating to patents as are within the jurisdiction conferred to it by the Patents Act 1977, and such other proceedings relating to patents or other matters as may be prescribed.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Function of the Patents Court

### 96.03

s.130(1) is also relevant

The Patents Court is thus part of the Chancery Division of the High
Court. Section 97 of the Patents Act 1977 provides for appeals to the
Patents Court from many decisions of the comptroller under the Act or
rules. Many other sections of the 1977 Act provide for proceedings (eg
concerning alleged infringement of patents) to be brought in the court;
as respects England and Wales, the "court" means the High Court (where
proceedings are taken by the Patents Court) or a Patents County Court
(as set up under ss.287-292 of the CDP Act).

### 96.04

CPR 63.3 is also relevant

Under section 61 of the Supreme Court Act 1981 and Schedule 1 to that
Act, all causes and matters in the High Court relating to patents, trade
marks, registered designs or copyright are assigned to the Chancery
Division. Proceedings in the High Court under the Patents Act 1977, the
Registered Designs Act 1949 and the Defence Contracts Act 1958, and all
proceedings for the determination of a question or the making of a
declaration relating to a patent under the inherent jurisdiction of the
High Court, shall be assigned to the Chancery Division and taken by the
Patents Court.

### Supreme Court Act 1981, section 6(2)

The Judges of the Patents Court, of the Admiralty Court and of the
Commercial Court shall be such of the puisne judges of the High Court as
the Lord Chief Justice may, after consulting the Lord Chancellor, from
time to time nominate to be judges of the Patents Court, Admiralty
Judges and Commercial Judges respectively.

### Judges

### 96.05 {#ref96-05}

s.97(2) is also relevant

The judges of the Patents Court are normally High Court judges specially
nominated by the Lord Chief Justice, after consulting the Lord
Chancellor. They generally sit singly but can, for the purpose of
hearing appeals from decisions of the comptroller, sit en banc.

### 96.06 {#section-4}

Section 63 of the Supreme Court Act 1981 provides that any business
assigned to one or more such specially nominated judges of the High
Court may during vacation, illness or absence or for any other
reasonable cause be dealt with by any judge of the High Court named for
that purpose by the Lord Chief Justice, after consulting the Lord
Chancellor. There is also provision for the appointment of deputy judges
on an ad hoc basis by the Lord Chief Justice, after consulting the Lord
Chancellor under Section 9(4) of the Supreme Court Act 1981.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Supreme Court Act 1981, section 70(3)**
  Rules of court shall make provision for the appointment of scientific advisers to assist the Patents Court in proceedings under the Patents Act 1949 and the Patents Act 1977 and for regulating the functions of such advisers.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Scientific advisers

### 96.07 {#section-5}

CPR 63PD para 4.10 CPR 35.15

The Civil Procedure Rules provide that in any proceedings under the 1977
Act, the Patents Court may direct that an independent scientific adviser
be appointed to assist the court. Rule 35.15 of the Civil Procedure
Rules covers the duties and remuneration of advisers or assessors. The
adviser shall assist the court in dealing with a matter for which the
adviser has skill and experience, and shall take part in the proceedings
as the court may direct. In particular, the court may direct the adviser
to prepare a report for the court on any matter at issue in the
proceedings, and direct the adviser to attend the whole or any part of
the proceedings to advise the court on any such matter. (The Patents
Court may also order the Office to provide reports under s.99A, [see
99A.01-04](/guidance/manual-of-patent-practice-mopp/section-99a-power-of-patents-court-to-order-report/#ref99A-01).

### 96.08 {#section-6}

Sup.Ct. Act81, s.54(9)

The above provisions regarding scientific advisers, and the provisions
below regarding their remuneration, also apply in relation to the Court
of Appeal in proceedings on appeal from decisions in such Patents Court
proceedings.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Supreme Court Act 1981, section 70(4)**
  The remuneration of any such adviser shall be determined by the Lord Chancellor with the concurrence of the Minister for the Civil Service and shall be defrayed out of money provided by Parliament.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
