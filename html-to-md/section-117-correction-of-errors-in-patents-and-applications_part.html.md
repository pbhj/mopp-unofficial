::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 117: Correction of errors in patents and applications {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (117.01 - 117.31) last updated: October 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 117.01 {#ref117-01}

This section is concerned with the correction of errors in
specifications and in other documents filed in connection with patents
and applications. Prescribed conditions are set out in r.105. (For the
distinction between correction and amendment, [see
19.03-19.04](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-03)).
S.117 and r.105 do not cover failure to file a document within a
prescribed period; they are concerned solely with correcting errors in
documents, and not with procedural errors or omissions, which are the
province of r.107 (Klein Schanzlin & Becker AG's Application \[1985\]
RPC 241; Tokan Kogyo KK's Application \[1985\] RPC 244). Subsections (3)
and (4) were added by the Regulatory Reform (Patents) Order 2004, with
effect from 1 January 2005.

### 117.02 {#section}

S.117 does not govern correction of the Register nor of documents filed
in connection with registration, for which [see
32.14](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-14).
If, however, an error in the Register has resulted from an error in a
document which is subsequently corrected under s.117 then the Register
will be corrected accordingly.

\[ A request to correct an error or mistake under this section must be
made in writing and identify the proposed correction.\]

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 117(1)**
  The comptroller may, subject to any provision of rules, correct any error of translation or transcription, clerical error or mistake in any specification of a patent or application for a patent or any document filed in connection with a patent or such an application.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 117.03 {#ref117-03}

r.105(1)-(2), r.49 is also relevant

Except in the case of a name (where correction must be requested on Form
20, [see
16.23](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-23)),
a request to correct a document under this section must be made in
writing clearly identifying the proposed correction. (The comptroller
may require the filing of a copy of the defective document on which the
correction is shown, and may request that it be shown in red ink.) For
correction of an error in an address or address for service, [see also
117.17](#ref117-17). [See
80.03-06](/guidance/manual-of-patent-practice-mopp/section-80-authentic-text-of-european-patents-and-patent-applications/#ref80-03)
with regard to translations corrected under s.80(3).

\[ A request to correct an error in a specification should be referred
by the formalities examiner via the Formalities Manager to the case
examiner. The Formalities Examiner should add a minute to the dossier
for the case examiner and send the appropriate PDAX message to the
examiner. If the request is allowable the correction should be effected;
if this cannot readily be done on the existing documents and
replacements have not been filed they should be asked for. The
Formalities Examiner should create a working copy annotated
appropriately and place a "P" against the documents for publication. If
the application is a paper case, replacement drawings should be stamped
as under
[15A.06](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-06)
and superseded drawings, other than any marked "not to be amended",
should be cancelled. \]

### 117.04 {#section-1}

Although the request will normally be made by the applicant or
proprietor, any person able to give evidence as to the occurrence of an
error may request its correction.

### 117.05 {#section-2}

If an examiner notices an apparent error of significance during
substantive examination it should be drawn to the applicant's attention.
Exceptionally, as the wording of s.117(1) allows the comptroller to make
corrections on their own initiative the examiner may make clearly
allowable corrections of obvious minor errors but should inform the
applicant that they have done so.

### 117.06 {#ref117-06}

Provided the request for correction has been allowed before the
completion of preparations for publication the application published
under s.16 will be published as corrected ([see
16.23](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-23))
and the published document will bear a notice ([see
16.29]((/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29))
that it embodies a correction made under s.117(1).

### 117.06.1 {#ref117-06-1}

In [Genentech Inc v The Comptroller General of Patents \[2020\] EWCA Civ
475](https://www.bailii.org/ew/cases/EWCA/Civ/2020/475.html){rel="external"}
(on appeal from a decision of the Comptroller in BL O/111/20), the
patentee sought to use a correction to enable them to obtain further
years of SPC protection, which they had not requested at the time of
paying the relevant fee. They said that they had intended to pay for the
maximum possible SPC term, and it was an error that the fee-handling
agents acting on their behalf did not pay the correct fees or list the
correct number of years on the Form SP2. Floyd LJ said that paying
further annual fees for SPCs is prohibited by rule 116(5), and this
specific statutory prohibition could not be circumvented by a correction
under s.117. He held that reading "section 117 as permitting the
Comptroller to rectify not only the document, but also the failure to
pay the fees would be to allow section 117, an entirely general
provision about rectifying errors in documents, to nullify the effect of
rule 116(5). The argument offends against the principle that general
provisions should not be construed so as to derogate from more specific
ones".

### Correction of the specification

### Allowability

### 117.07 {#ref117-07}

r.105(3) is also relevant

No correction may be made in a specification unless the correction is
obvious (meaning that it is immediately evident that nothing else could
have been intended in the original specification). This is construed as
imposing a two-fold test:\
(a) is it clear that there is an error, and\
(b) if so, is it clear what is now offered is what was originally
intended?\
\
A very similar two-fold test was at the centre of judicial reviews under
PCT r.91 in R. v the Comptroller-General of Patents ex parte Celltech
Ltd \[1991\] RPC 475 and in Drazil's Application \[1992\] RPC 479. These
judgments may not be fully applicable to corrections under s.117 since
there are differences in the wording between r.105 and PCT r.91 and
s.117 is not one of those sections listed under s.130(7) as having the
same effect as corresponding provisions in the EPC, CPC and PCT. Rule
139 EPC has similar wording to r.105(3) but the Enlarged Board of Appeal
of the EPO has decided in Decision G11/91 (OJEPO 3/93) that no
correction can be allowed which would extend the disclosure. As
indicated in
[19.04](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-04),
that limitation does not arise under the 1977 Act.

r.105(4) is also relevant

\[ However, the test set out above does not apply where the error in the
specification is connected to the delivery of the application in
electronic form or using electronic communications, i.e. where an error
was caused by the conversion from one electronic format to another, a
correction to the specification should be allowed.\]

\[ If it appears to the case examiner that the alteration sought does
not meet the tests for a correction but it appears that it would be
allowable as an amendment under s.19 or s.27 this course may be
suggested to the applicant. \]

### 117.08 {#section-3}

In order to pass the first test it must be apparent on the face of the
documents that something is amiss. This would clearly be the case if a
passage did not read on, or if a page were missing. It is not, however,
necessary that the error be as readily apparent as this; the notional
addressee of the specification is a person who is reading the document
with the intention of extracting all the teaching from it, and who is
aware of everything of common knowledge in the art concerned. For
example, while a casual reader might not realise that the quoted serial
number of another patent is incorrect, the notional reader will turn up
every reference as they come to it, and it will then be apparent (if for
example the patent apparently referred to relates to completely
different subject-matter) that the reference given would not have been
intended. Likewise if an error is made in giving a known physical
parameter, for example a eutectic temperature, the reader may be deemed
to recognise this, even if they have to refresh their memory from a
reference book. Also, a knowledge of all the contents of the open part
of the file of the application should be attributed to the reader, so
that, for example, a discrepancy between the specification and an
otherwise similar priority document should be regarded as enough to put
the reader on enquiry. If, however, the specification makes technical
and linguistic sense, then it is not immediately evident that this would
not have been what was originally intended, so that, irrespective of
what is proposed as the correction, it cannot be said that nothing else
than what is offered would have been intended. In such a case the matter
cannot be dealt with as the correction of an error.

### 117.09 {#ref117-09}

Although it will sometimes be apparent on the face of the documents what
the correction should be, this will not generally be the case if for
example the error lies in an omission or in an incorrect document
reference or numerical data. It is not however necessary that the reader
be able to correct the error unaided; in considering an offered
correction regard must be had to the view which the fully-informed and
inquisitive skilled reader would take of the documents originally filed
and the most likely solution to the difficulty apparent to the skilled
reader from them. Often the correct version will be unique and will be
apparent from the documents filed at the time at which the application
was made. Otherwise evidence will be necessary to establish that the
correction offered is what was originally intended. In Dukhovskoi and
Others Applications \[1985\] RPC 8 where it became accepted during the
proceedings that the error was apparent, the Patents Court held that the
original document which it was asserted has been mistranslated (which
was neither the priority document nor one of the documents available to
the Office at the date of filing) could be considered when the
application for correction was made and, with the aid of a dictionary,
could establish that the correct translation was what was suggested by
the correction. A priority document filed later than the date on which
the declaration of priority was made ([see 5.08 to
5.11](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-08))
may be taken into consideration provided that it can be shown it was
intended at the time the declaration of priority was made to claim
priority from that document rather than some other application. This
will be shown if its file number was included on the date the
declaration of priority was made. If however this number is supplied
later (as under rule 8(1), it may be) and there is a reasonable doubt as
to its veracity, suitable evidence may take the form of a sworn
statement from the applicant or an extract from the official Gazette of
the country of filing indicating that it was the only application filed
by the applicant on the date in question. A later-filed document may
also be used to show that a particular process or construction is
well-known in the art and could be assumed to be known to the skilled
reader. The expression "immediately evident" is however taken as
requiring that, when all the evidence is considered, it is abundantly
clear that nothing else other than what is now offered as the correction
was originally intended. It is not sufficient merely to show that, on
balance of probabilities, the correction offered is the most likely
version.

\[ Borderline cases under r.105(3) and any where the strict application
of the rule would apparently lead to a ridiculous result should be
referred to the Divisional Director. \]

### 117.09.1 {#ref117-09-1}

In BL O/769/18, the hearing officer held that a request to replace a
specification in its entirety with the specification of the priority
document was not allowable as a correction under s.117(1). The hearing
officer said that it is likely that a fully informed and inquisitive
skilled reader would be aware that replacing the description as filed
with the description of the priority application would correct the
error. However it cannot be said with certainty that what is now offered
is what was originally intended.

### 117.09.2 {#ref117-09-2}

In [Sunwave Communications Co. Ltd's application (BL
O/0206/23)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F0206%2F23&submit=Go+%BB){rel="external"}
the hearing officer confirmed that it is not necessary for the proposed
correction to be a unique solution to an apparent error, as mentioned in
117.09. Instead, the hearing officer concluded that the correction was
allowable as it was the most likely solution to the error that would be
identified by the fully-informed and inquisitive skilled reader, in the
sense that it is immediately evident that this was what was originally
intended.

### 117.10 {#ref117-10}

Section 15(8) states that s.15(6) and (7) in relation to "missing parts"
do not affect the comptroller's discretion under s.117(1) to correct an
error or mistake. Thus, if all of the requirements of s.117 and r.105
have been met ([see 117.03-09](#ref117-03)), it is possible for the
comptroller to allow a missing part to be filed as a correction under
s.117(1) without redating. Applicants should be asked to indicate, in
writing, whether any drawing(s) filed with such a request under s.117
is/are to be regarded as a filing where ss.15(5) to (7) apply, should
the s.117 request be disallowed.

### 117.11 {#section-4}

A request to correct an error in the specification of a European patent
(UK) should be dealt with in the same way as in the case of a patent
granted under the Act. However, the comptroller does not entertain a
request for correction of the text of a European patent (UK) under s.117
if the EPO has already allowed the correction. The formal publicising of
such a correction is also solely a matter for the EPO. If a request
under s.117 to correct an error in the specification of a European
patent (UK) is received before the nine months allowed for opposing the
patent has expired or when there is an opposition outstanding, the
possibility exists that the specification may be amended before the EPO.
In such circumstances the agent is given the option of either (a)
staying the request until the opposition period has expired or the
opposition proceedings have been settled or (b) proceeding with the
request to correct under s.117 on the understanding that the desired
correction may be negated as a result of subsequent amendment before the
EPO. If it is not possible to determine the allowability of the
correction solely by reference to the published specification, the
applicant should be required to furnish the necessary documentary
evidence, for example copies of any relevant documents indicating the
"as filed" state of the application.

\[ Where a request under s.117 is received to correct a European patent
(UK), Restoration and Post Grant Section (RAPS) will check on the EPO
online file inspection service in order to ascertain (a) whether the
period allowed for opposing the patent has expired and whether any
opposition was entered and (b) whether any application for central
amendment of the patent has been made and whether any amendment has been
allowed. Where necessary the proprietor or agent will be given the
option of staying the request. If the applicant is to be given the
option of staying the proceedings, the requested correction should be
scrutinised by a Group Head and any prima facie objection to it should
be communicated to the applicant in a letter giving them the choice on
stay. A request to correct a document in a European patent (UK) other
than the specification will be referred by RAPS to the Group Head\].

### Correction of translations of the specifications of European patents (UK)

### 117.11.1 {#section-5}

A request to correct a translation of the specification of a European
patent (UK) filed under s.77(6)(a) should also be dealt with in the same
way as a patent granted under the Act but unlike a request to correct
the text of a European patent (UK), it should never be deferred pending
the expiry of the opposition period. An additional consideration is
whether the correction requested properly comes under s.117 or s.80(3).
If the correction of the translation would seem to broaden the
protection conferred by the patent or application, it should be made
under s.80(3) in accordance with the procedure in
[80.03-06](/guidance/manual-of-patent-practice-mopp/section-80-authentic-text-of-european-patents-and-patent-applications/#ref80-03),
for which purpose a fresh application on Form 54 is necessary.
Correction of the translation will not be appropriate where the error
lies in the specification of the European patent (UK) but the
translation is nevertheless a correct representation of the incorrect
text. In such cases a request should be made to correct the
specification. If this necessitates a fresh verified translation then
such a translation should be filed pursuant to r.113(1).

\[(i) The procedure for correction of such a translation under s.117
generally follows along the same lines as for correction of UK national
patents after grant, as follows.\
\
(ii) After stamping and scanning, the request for correction is passed
to RAPS for the request for correction to be noted in the Register and
advertised in the Journal.\
\
(iii) RAPS will send the file to the Group Head for consideration as to
whether the correction requested properly comes under s.117 or s.80(3)
and whether or not the correction is allowable.\
\
If it is decided that s.117 is appropriate, the considerations as to the
allowability of the correction set out in [117.07-117.10](#ref117-07)
apply. However, if the requested correction would seem to broaden the
protection conferred by the translation the correction should be made
under section 80(3). In this case a minute should be created to that
effect, in a form suitable for communication to the agent/proprietor,
and a PDAX message sent to RAPS for preparation and issue of a letter
embodying the minute. The letter should also invite the agent/proprietor
to initiate action under s.80(3) by filing Form 54 with appropriate fee,
[see
80.03-06](/guidance/manual-of-patent-practice-mopp/section-80-authentic-text-of-european-patents-and-patent-applications/#ref80-03).\
\
(iv) Subsequent procedure is basically as in the second square bracket
paragraph of [117.14](#ref117-14) except that:\
(a) unless the corrections are of minimal extent, RAPS will call upon
the proprietor to provide, within a specified period, a new translation
incorporating the corrections instead of correcting the original copy of
the translation,\
(b) RAPS should ensure that the file copy is updated accordingly and
that a copy of the corrected translation is also sent to the British
Library, and\
(c) since the correction relates to the translation of the
specification, it is not necessary for Publishing Section to issue a C
publication.\]

### Procedure before publication

([See also 117.06](#ref117-06))

### 117.12 {#section-6}

\[deleted\]

### 117.13 {#ref117-13}

If an examiner notices a clerical error which would be likely to impede
the understanding of a specification when published they should inform
the applicant or agent. However, the initiation of a correction is a
matter for the applicant, and if they do not respond the matter should
be deferred until substantive examination. Publication should not be
unduly delayed. Purely formal editorial matters, such as the
re-arrangement and renumbering of accidentally transposed pages, may be
effected by the examiner with the applicant's or agent's agreement,
provided that the nature of the error and its correction are beyond
reasonable doubt.

\[ If a page of text appears to be missing from the specification the
formalities examiner should draw this to the attention of the case
examiner. If the case examiner considers that the matter may be
rectified as a correction they may invite the applicant to make a
request for correction. In no case should action be taken which would be
likely to provoke a request to correct if it appears that such a request
would not be allowable. \]

### Procedure between publication and grant

### 117.13.1 {#section-7}

Any request to correct an error received after the completion of
preparations for publication of an application but before grant should
be considered as soon as possible and not deferred until substantive
examination, for example. An erratum should be issued once the
correction has been allowed. Where there is doubt whether the desired
alteration is intended to be by way of correction or amendment ([see
19.03-19.19](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-03)),
the situation should be clarified with the applicant.

\[ Where such a correction is considered allowable, the examiner should
create a minute addressed to Formalities, noting that a correction has
been allowed under s.117, and requesting that an erratum is issued. If a
correction is not allowable, the examiner should create a minute
addressed to Formalities outlining the objection to the correction.
Formalities will then issue a letter to the applicant.\]

### Procedure after grant

### 117.14 {#ref117-14}

'The Group Head to whom the request is referred, or the examiner to whom
they delegate it, as the case may be, must consider it according to the
tests set out in paragraphs [117.07 to 117.09](#ref117-07). They must
also consider whether it is necessary to advertise the correction ([see
117.23-117.24](#ref117-23)). When allowed, the correction is published
as a C publication. The C publication is a front page either with a
schedule of the corrections on the reverse or associated with a new
corrected specification if one has been filed, the procedure being the
same as for a C publication for amendment of a granted specification,
[see
27.20](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-20).
A C publication is not normally issued for correction of printer's
errors or of errors in the bibliographic information on the front page
of a published specification, these being dealt with by issuing errata
[see also
16.33](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-33).
However, if a C publication is being issued for other reasons, any such
errors are corrected at the same time.

\[ A request received after grant is handled by RAPS who arrange for the
preliminary notice of the request to appear in the Journal ([see
117.23](#ref117-23)). If it relates to an error in a specification it
should be referred to the Group Head of the group dealing with the
relevant subject matter, who may refer it to a member of his group. Any
objections to the proposed correction should be set out in terms
suitable for embodiment in an official letter which is issued by RAPS.
([See also 117.07](#ref117-07)).

\[ When the Group Head is satisfied that the correction is allowable
they should so report and send a PDAX message to RAPS, and instruct them
whether or not the correction should be advertised in the Journal for
opposition. If the correction is not being advertised, or if the period
allowed for opposing the correction ([see 117.25](#ref117-25)) has
expired, RAPS should assemble the specification. The corrections should
be checked in the examining group before the Group Head confirms the
appropriate certificate should be applied. The case should then be
returned to RAPS who should notify the person making the request that it
has been allowed, date the certificate with the date of such
notification, arrange for the outcome of the s.117 proceedings to be
entered in the register and advertised in the Journal, create a minute
and send a PDAX message to Publishing Section for issue of a C
publication (and also an erratum as in [117.06](#ref117-06) if the
correction is applicable to the A publication as well as to the B
publication). Note that If a correction is opposed at any stage, then
the matter must be referred to a Deputy Director and cannot be dealt
with by a Group Head.\]

### 117.15 {#section-8}

If a request to correct is received after the issue of the letter
informing the applicant that a patent has been granted but before the
date of publication of the patent, the applicant should be informed that
consideration of the request is deferred until after publication.
Following publication of the patent, the post-grant procedure as set out
in [117.14](#ref117-14) should then be followed.

### 117.16 {#section-9}

If proceedings to correct a patent are initiated or are outstanding
whilst an action for revocation of the patent is pending the comptroller
may decide whether the s.117 proceedings are to be stayed or resolved.

### Correction of the request for grant

### 117.17 {#ref117-17}

r.49 is also relevant

If the applicant's address or address for service was given incorrectly
on Form 1 then it may be corrected by means of any suitable written
notification. However, a request under r.49(1)(a) to correct a name must
be made on Patents Form 20. If, however, the name was correct at the
time of filing and has changed subsequently then alteration of the form
to reflect this must be effected either as an amendment under s.19 (if
more than just the name requires amendment) or a correction to the
register under s.32(2)(d) (if only the name has changed), [see
19.05-19.07](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-05)
and
[32.06](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-06).
If there are reasonable doubts about whether the correction should be
made, the comptroller should inform the person making the request of the
reason for their doubts and ask that person to file evidence in support
of the request, i.e. proof that the name was entered incorrectly and
that the name that is to replace it is the name that the applicant
always intended entering on the form. If a request to correct an error
or mistake in a name is allowed the change is effected in the same way
as in the case of an amendment ([see
19.07](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-07)).

\[ The procedure is the same as that described for amendments in
paragraphs [19.06
-19.07](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-06).\]

### 117.18 {#section-10}

A request to correct the title of the invention should be considered in
the same way as a proposed correction of the specification ([see
117.07-117.09](#ref117-07)).

### 117.19 {#ref117-19}

r.86, r.87 is also relevant.

A declaration of priority at part 5 of Form 1 (or a claim in part 6 of
that Form, or a declaration in part 6 of Form 3) may be corrected
provided the Office is satisfied that the discrepancy was due to an
error in translation or transcription or a clerical error or mistake. If
the error lies in the details of the priority document or earlier
application or patent then this document itself may be sufficient
evidence. Otherwise, where there is reasonable doubt that the alleged
mistake is in fact a mistake (or where there is reasonable doubt as to
the veracity of any matter contained in the request or of any document
filed in connection with it) evidence may be requested which
demonstrates that what is now proposed was the applicant's intention at
the time of filing. It is, however, a condition of Rule 5 of the Patent
Law Treaty that when such evidence is requested, the nature of the doubt
must be communicated to the applicant, and this practice should always
be observed. A copy of the applicant's instructions to their agent may
suffice; otherwise sworn evidence should be asked for before the request
is given further consideration, and a period of two months should be
specified for reply. Any priority document or translation in support of
a new declaration must be filed within the period prescribed. In Payne's
Application \[1985\] RPC 193 the applicant sought to delete the two
earliest claims to priority by way of correction but, before the matter
was decided, the applicant was informed by the Office that the
application had been treated as withdrawn through failure to file the
request for search. The applicant subsequently argued that the
correction should be allowed (in which case the search request could
have been filed in time) notwithstanding that the application had ceased
to exist. It was held in the Patents Court that section 117 could not be
invoked to overcome this mandatory requirement, [see
15.56](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-56).

\[ A request to correct the declaration of priority should be referred
by the Formalities Examiner via the Formalities Manager to the
divisional Head of Administration, who will, if the request is
allowable, correct Form 1.

PDAX dossiers: Form 1 should be corrected using the Enhance function in
PDAX, and an action added to the dossier. The Head of Administration
should add a minute to the dossier and ensure the appropriate COPS
action is carried out.

Paper cases: Form 1 should be corrected in red ink, initialled and
dated, and the appropriate COPS action carried out. The Formalities
Manager should then correct the priority details on the front of the
shell.

The applicant should be informed of the allowance of the request. If the
request has been received before the completion of preparations for
publication, a minute and the appropriate PDAX message should be sent
via the Examination Support Officer outlining the publication
instructions. If the application is a paper case, an
appropriately-completed "Notices for front page of 'A' document" form
([see 117.06](#ref117-06)) should be placed on file. If the request was
received later than this, arrangements should be made for an erratum to
issue in respect of the A document (and B document if appropriate). \]

### 117.20 {#section-11}

r.6(2) is also relevant

Where the only matter to be corrected is the priority document file
number quoted on Form 1 or Form 3, then no evidence is required provided
the error is notified to the Office within the period which is allowed
for supplying this number. (For the case where the details on Form 1 are
correct but there is a discrepancy in the priority document, see
paragraph
[15A.16](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-16)).

### 117.21 {#section-12}

r.3 is also relevant

Since correction is deemed to have effect ex tunc, a correction of the
declarations of priority may affect the declared priority date even if
the change was effected after the completion of preparations for
publication.

### 117.21.1 {#ref117-21-1}

In line with the ratio of [Genentech Inc v The Comptroller General of
Patents \[2020\] EWCA Civ
475](https://www.bailii.org/ew/cases/EWCA/Civ/2020/475.html){rel="external"}
([see 117.06.1](#ref117-06-1)), in the absence of a definite and clear
expression in an application that it is filed as a divisional, it is the
practice of the office not to allow applicants to correct an omission at
Part 6 of Form 1 under s.117(1) because to do so would circumvent the
mandatory and specific provision of r.19(3), that applications filed
under s.15(9) must be accompanied by such a statement.

### Other corrections

### 117.22 {#ref117-22}

r.49, r.105 is also relevant

An error in Form 7 may be corrected after the end of the period allowed
for filing this form following a request in writing, except for
correction of a name which must be made on Patents Form 20. (For
effecting a change in Form 7 before the end of this period, [see
13.14](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-14)).
If there are reasonable doubts about whether the correction should be
made, the comptroller should inform the person making the request of the
reason for their doubts and ask that person to file evidence in support
of the request. A copy of the corrected Form 7 should be sent to each
inventor. The decision in Payne's Application ([see 117.19](#ref117-19))
means that s.117 cannot be invoked to overcome the mandatory
requirements of s.13(2) ([see
13.14](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-14)).

\[ A request to correct the inventor details on Form 7 should be
referred by the formalities examiner via the Formalities Manager to the
Casework Lead Manager, who will, if the request is allowable correct the
details on Form 7 using the "Enhance" function on PDAX, add a minute to
the dossier and annotate Form 7. If the application is a paper case, the
Casework Lead Manager should, if the request is allowable, correct Form
7 in red ink, initial and date the correction and carry out the
appropriate COPS action. Where an inventor has been added or deleted
after the completion of preparations for publication, the formalities
examiner should send a message to Publishing Section for issue of an
erratum. \]

### 117.22.1 {#ref117-22-1}

s.14(10) is also relevant

An error or mistake in a withdrawal of an application may be corrected
upon request in writing. A written explanation or evidence will be
needed in support of the request.

### Advertising a correction

### 117.23 {#ref117-23}

r.75, r.105(5) is also relevant

Notice of a request for correction of an error or mistake in the
withdrawal of a published patent application will be published by
advertisement in the Journal --[see 117.31](#ref117-31). In all other
circumstances, except where the comptroller determines that no person
could reasonably object to the correction, the fact of a request for
correction and the nature of the proposed correction are advertised in
the Journal. Preliminary notice of a request made post-grant is inserted
in the Journal, and the proposed correction should be advertised if it
would alter the meaning or extend the scope of the specification of a
granted patent, or if the rights of a third party could be adversely
affected. Other corrections sought before grant are not normally
advertised.

\[ For procedure post-grant see under [117.14](#ref117-14). Corrections
pre-grant are not normally advertised and the procedures described under
[117.13 or 117.13.1](#ref117-13) should be followed.\]

### 117.24 {#section-13}

r.75 is also relevant

When it appears to the Office that a correction may be allowed the
applicant should be so informed and if appropriate the correction should
be advertised. If the period allowed for opposing the correction ([see
117.25](#ref117-25)) has expired without any opposition being filed the
correction should then be effected as soon as possible. This must be by
order for certain corrections of withdrawals --[see 117.31](#ref117-31).
On the other hand, if agreement is not reached with the applicant, a
hearing should be offered. If the applicant requests to be heard they
should be informed that an ex parte hearing will be appointed subject to
advertisement of the proposed correction and the absence of opposition.

\[ For procedure [see under 117.14](#ref117-14). \]

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 117(2)**
  Where the comptroller is requested to correct such an error or mistake, any person may in accordance with rules give the comptroller notice of opposition to the request and the comptroller shall determine the matter.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 117.25 {#ref117-25}

r.75, r.76(3), r.108.(1) is also relevant

Any person may oppose a correction; there is no need for an opponent to
show a locus standi. Notice of opposition should be given on Form 15,
which should be filed within four weeks of the date of advertisement of
the correction in the Journal; this period may not be extended. The Form
should be accompanied by a copy thereof and by a statement of grounds
(in duplicate). This starts proceedings before the comptroller, the
procedure for which is discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).

### 117.26 {#section-14}

\[deleted\]

### 117.27 {#section-15}

s.74(2) is also relevant

The notice of opposition and supporting statement must be directed
solely to the allowability of the correction. In particular, the
validity of the patent may not be put at issue.

### 117.28 {#section-16}

r.82 is also relevant

The comptroller may give such directions as they think fit with regard
to the subsequent procedure. If a form of correction acceptable to the
parties and the Office is not arrived at, the matter will need to be
decided at a hearing.

### 117.29 {#section-17}

Where an opposition to the correction has been properly launched by the
filing of Form 15 accompanied by a supporting statement, but the
opponent subsequently withdraws at any stage, the comptroller
nevertheless takes account of matters raised by the opponent in deciding
whether the correction is allowable. If no agreement is reached with the
applicant, an ex parte hearing is held.

\[The procedure is the same as that described in
[27.31](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-31).\]

  -----------------------------------------------------------------------
   

  **Section 117(3)**

  Where the comptroller is requested to correct an error or mistake in a
  withdrawal of an application for a patent, and\
  (a) the application is published under section 16 above; and\
  (b) details of the withdrawal were published by the comptroller;\
  \
  the comptroller shall publish notice of such a request in the
  prescribed manner.
  -----------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 117(4)**
  Where the comptroller publishes a notice under subsection (3) above, the comptroller may only correct an error or mistake under subsection (1) by order.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------

### 117.30 {#section-18}

Subsections (3) and (4) were added by Article 17 of the Regulatory
Reform (Patents) Order 2004. These provisions relate to the power of the
comptroller, alluded to in section 14(10), to correct an error or
mistake in a withdrawal of a patent application ([see
14.209](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-209)).
This is effective from 1 January 2005 and applies to applications
irrespective of whether their filing date is before or after that date.

### 117.31 {#ref117-31}

r.105(6) is also relevant

These provisions ensure that in the case of a published application
whose withdrawal was published in the Journal, the request for
correction of that withdrawal will likewise be published. This will be
done by advertisement in the Journal and an entry in the register. The
advertisement concludes the period referred to in s.117A(4), [see
117A.02](/guidance/manual-of-patent-practice-mopp/section-117a-effect-of-resuscitating-a-withdrawn-application-under-section-117/#ref117A-02).
If the correction is made it must then be effected by order.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
