::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Supplementary Protection Certificates for Medicinal and Plant Protection Products {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated: April 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Introduction

Note: This section relates to Supplementary Protection Certificates for
Medicinal and Plant Protection Products and is divided into three parts,
with the paragraphs numbered using the prefixes below:

  ----- -----------------------------------------------------------------------------------------------------------------------------------------------------------
         
  SP    the general introduction below
  SPM   a discussion of the details of the Medicinal Regulation by Article, including those aspects common to both the Medicinal and Plant Protection Regulations
  SPP   a discussion of the details specific to the Plant Protection Regulation by Article
  ----- -----------------------------------------------------------------------------------------------------------------------------------------------------------

In the margins:

  --------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
             
  ArtM      refers to the relevant Article of Regulation (EC) No 469/2009 of the European Parliament and of the Council (for Medicinal Products) ("the Medicinal Regulation"), as incorporated in domestic law by the European Union (Withdrawal) Act 2018
  ArtP      refers to the relevant Article of Regulation (EC) No 1610/96 of the European Parliament and of the Council (for Plant Protection Products) ("the Plant Protection Regulation" or "the EC Plant Protection Regulation"), as incorporated in domestic law by the European Union (Withdrawal) Act 2018
  HMR       refers to the Human Medicines Regulations 2012 (SI 2012/1916; "the Human Medicines Regulations"); references to "reg" or "sch" alongside this refer to the relevant regulation of the Human Medicines Regulations
  PA 1977   refers to the Patents Act 1977, "s" refers to the relevant section of the Act, "para" and "sch" refer to the relevant paragraphs of the relevant Schedules to the Act.
  reg       on its own refers to the relevant regulation of the Patents (Compulsory Licensing and Supplementary Protection Certificates) Regulations 2007 (SI 2007/3293; the "2007 Regulations").
  r         refers to the relevant rule of the Patents Rules 2007 (SI 2007/3291) (the "2007 Rules") and "part" refers to the relevant part of those Rules
  Fr        refers to the relevant rule of the Patents (Fees) Rules 2007 (SI 2007/3292 the "2007 Fees Rules") and "FSch" refers to the relevant Schedule of those Rules
  --------- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Overview; historical background of UK SPC law

### SP 0.01 {#refSP0-01}

ArtM 22 is also relevant.

Supplementary protection certificates were introduced by Council
Regulation (EEC) No 1768/92 creating a supplementary protection
certificate for medicinal products ("the original Regulation"), which
entered into force on 2 January 1993. This Regulation was later replaced
by the Medicinal Regulation, which codified and consolidated various
amendments to the original Regulation and provided for references to it
to be construed as references to the Medicinal Regulation. The Medicinal
Regulation entered into force on 6 July 2009. Regulation (EC) No 1610/96
of the European Parliament and of the Council creating a Supplementary
Protection Certificate for plant protection products entered into force
on 8 February 1997.

### SP0.01.1 {#refSP0-01-1}

As set out in the recitals to both Regulations, the certificate is
intended to compensate a patentee for the loss of effective protection
arising out of the time taken to obtain regulatory approval to place on
the market as either a medicinal or plant protection product a product
which is protected by a patent ("the basic patent").

### SP 0.02 {#sp-002}

The basic patent may protect the product as such, a process to obtain
the product or an application of the product see [SPM1.05](#refSPM1-05).

### SP 0.03 {#sp-003}

A certificate takes effect at the end of the lawful term of the basic
patent but does not extend the term of the patent itself. It extends the
protection conferred by the patent in respect of the product covered by
the authorisation to place the corresponding medicinal or plant
protection product on the market, and any use of the product as a
medicinal or plant protection product that has been authorized before
expiry of the certificate.

### SP 0.04 {#refSP0-04}

In the UK, the marketing authorisation for medicinal products takes the
form of a Product Licence or Marketing Authorisation granted by the
appropriate authority see [SPM2.01](#refSPM2-01). Details of granted
authorisations, including the product, are listed on the Medicines and
Healthcare products Regulatory Agency (MHRA)
[website](https://www.gov.uk/government/collections/marketing-authorisations-lists-of-granted-licences)
on a monthly basis and on the Veterinary Medicines Directorate (VMD)
[Product Information
Database](https://www.vmd.defra.gov.uk/ProductInformationDatabase/){rel="external"}
(see [SPM1.02](#refSPM1-02)). Marketing authorisation for plant
protection products may be an Approval or Authorisation granted by the
relevant authority ([see
SPP2.01](/guidance/manual-of-patent-practice-mopp/regulation-ec-no-1610-96-of-the-european-parliament-and-of-the-council-plant-protection-products#refSPP2-01))
and details are published in the online [Pesticides
Register](https://secure.pesticides.gov.uk/pestreg/){rel="external"}.

### SP 0.05 {#refSP0-05}

reg 1 is also relevant.

Certificates granted by the Office have effect in Great Britain and
Northern Ireland pursuant to the 2007 Regulations, (although see
[SPM5.05-5.05.4](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refspm5-05)
and
[SPP5.01-5.03](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refspp5-01)
as well as in the Isle of Man, where the provisions of the 2007
Regulations were given effect by the Patents (Isle of Man) Order 2013
(SI 2013/2602) (the previous application of the 1992 and 1996
Regulations to the territory being revoked by the Isle of Man Patents
(Supplementary Protection Certificates) Regulations 2014 (Statutory
Document 2014/0091)). See also paragraph
[128B.03](/guidance/manual-of-patent-practice-mopp/section-128b-supplementary-protection-certificates/#ref128B-03).

### SP 0.06 {#sp-006}

reg 2 is also relevant.

The 2007 Regulations amended the Patents Act 1977 to introduce s.128B
and Schedule 4A. These make clear how the Act applies in relation to
supplementary protection certificates for medicinal products as they
relate to certificates and applications for certificates that exist
under the original Regulation and Medicinal Regulation and to plant
protection products as they relate to certificates and applications for
certificates that exist under the Plant Protection Regulation. The
operation of these provisions is discussed in paragraphs [128B0.1 to
128B.12](/guidance/manual-of-patent-practice-mopp/section-128b-supplementary-protection-certificates/#128b0-1).
The 2007 Rules and 2007 Fees Rules provide specific procedures for
certificates and applications for certificates which differ from patents
and applications for patents, including the payment and amount of fees.

### SP 0.07 {#sp-007}

ArtM 19 ArtP 18 is also relevant . Where neither the Medicinal or Plant
Protection Regulations nor the 2007 Rules lays down a special procedure
for certificates, the provisions of the Patents Act 1977 and respective
Rules apply to certificates and applications for certificates as they do
to patents and applications for patents (subject to modifications
applied by s.128B and Schedule 4A).

### SP 0.08 {#refSP0-08}

The Medicinal and Plant Protection Regulations take account of
authorisations granted in the European Economic Area (EEA). The
territorial scope of the EEA, and how it has changed over time, is
therefore relevant for certificates and applications. The original
Regulation entered into force with effect from 1 July 1994 in those EFTA
States which were at that date party to the European Economic Area
Agreement (Austria, Finland, Iceland, Norway and Sweden). The
application of the original Regulation did not have retrospective effect
on applications for certificates lodged in an existing EU Member State
before 1 July 1994, and this position was not affected by the accession
to the EU of Austria, Finland and Sweden with effect from 1 January
1995. Liechtenstein became a party to the EEA Agreement with effect from
1 May 1995; although it has not adopted the Medicinal Regulation into
its national law and does not grant certificates in its territory,
authorisations granted with effect in the territory are still relevant
([see SP0.09](#refSP0-09)).

### SP 0.08.1 {#refSP0-08-1}

The accession to the EU of ten States (the Czech Republic, the Republic
of Estonia, the Republic of Cyprus, the Republic of Latvia, the Republic
of Lithuania, the Republic of Hungary, the Republic of Malta, the
Republic of Poland, the Republic of Slovenia and the Slovak Republic) on
1 May 2004 also necessitated the amendment of the original Regulation
and the Plant Protection Regulation for the purposes of their
application to those States ([see SPM13.04.2](#refSPM13-04-2)), as did
the accession of Bulgaria and Romania on 1 January 2007. The accession
of Croatia on 1 July 2013 saw the amendment of the Medicinal Regulation
to apply it to this State.

### SP 0.09 {#refSP0-09}

As discussed in SP0.08, references to authorisations in the EEA also
include authorisations with effect in Liechtenstein for applications
lodged on or after 1 May 1995. In practice, this may be an authorisation
granted in Switzerland, even though it is not a member of the EEA and
grants in accordance with its own national legislation. This is because
Swiss authorisations are (normally) automatically effective in
Liechtenstein under the customs union between the two States (see [SPM
13.04](#refSPM13-04)). However, from 1 June 2005, the bilateral
agreement between Switzerland and Liechtenstein was amended so that
Swiss authorisations for medicinal products with new active ingredients
-- that is, those which would typically qualify for SPC protection --
are not recognised automatically on grant. Instead, this recognition
normally takes place 12 months after the grant of the Swiss
authorisation. In addition, an agreement between Liechtenstein and
Austria which came into effect in December 2010 also allows for
authorisations effective in Liechtenstein to be granted by Austria via
the EU regulatory regime, thus avoiding this issue.

### SP 0.10 {#refSP0-10}

For the Plant Protection Regulation, Decision No 59/97 of the EEA Joint
Committee of 31 July 1997 incorporated its provisions into the EEA
Agreement. The decision entered into force on 1 April 1998, although the
Regulation applied in Iceland and Norway from 2 January 1998 following
adoption of its provisions into their national law. Therefore, for
applications for certificates for plant protection products lodged with
the Office on or after those dates, the first authorisation within the
territories of the existing EU Member States and Iceland, Norway and
Liechtenstein constitutes the first authorisation in the EEA for the
purposes of the Plant Protection Regulation [SP0.08](#refSP0-08) and
[SP0.09](#refSP0-09). As with the Medicinal Regulation, Liechtenstein
does not grant certificates under the Plant Protection Regulation, and a
first authorisation in Liechtenstein may in practice be a authorisation
granted in Switzerland [see SP0.09](#refSP0-09).

### SP 0.11 {#refSP0-11}

Regulation (EC) No 1901/2006 of the European Parliament and of the
Council of 12 December 2006 on medicinal products for paediatric use was
published in the Official Journal on 27 December 2006 and entered into
force on 26 January 2007. This provided for a six-month extension of a
SPC where the marketing authorisation contains validated data on the use
of the medicine in children, and made amendments to the original
Regulation to incorporate the extension. This validated data reports the
results of the testing of the medicinal product in the paediatric
population. These changes were subsequently carried across to the
Medicinal Regulation. Following the end of the transition period
provided by the Agreement on the Withdrawal of the United Kingdom from
the European Union (see
[SP0.13](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refsp0-13)),
the provisions setting out the paediatric extension were replaced by
regulation 58A of the Human Medicines Regulations (see
[SPM8.09](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products/#refspm8-09)).

### SP 0.12 {#refSP0-12}

Regulation (EU) 2019/933 of the European Parliament and of the Council
of 20 May 2019, amending Regulation (EC) No 469/2009 concerning the
supplementary protection certificate for medicinal products, came into
force on 1 July 2019. This amended the Medicinal Regulation to create a
"manufacturing waiver" for SPCs for medicinal products ([see
SPM5.05-13](#refspm5-05-13)).

### Retained EU law and EU case law

### SP0.13 {#refSP0-13}

Under the provisions of the European Union (Withdrawal) Act 2018 ("the
Withdrawal Act"), EU-derived domestic legislation and direct EU
legislation in force in the UK at 11pm on 31 December 2020 was saved or
incorporated into domestic law. This included Regulation 1610/96 and
Regulation 469/2009, as well as their implementation through the 2007
Regulations and others. This legislation was incorporated in the form it
was in effect at the time, and so include amendments made up to, and
including, Regulation (EU) 2019/933 (see [SP 0.12](#refsp0-12)).

By way of section 5 of the Retained EU Law (Revocation and Reform) Act
2023, this body of law is known as "assimilated law".

### SP0.13.1 {#refSP0-13-1}

A number of changes were made to the SPC legislation upon its
incorporation into domestic law. The relevant instruments are: the
[Patents (Amendment) (EU Exit) Regulations
2019](https://www.legislation.gov.uk/uksi/2019/801/contents/made){rel="external"}
(SI 2019/801), the [Intellectual Property (Amendment etc.) (EU Exit)
Regulations
2020](https://www.legislation.gov.uk/uksi/2020/1050/contents/made){rel="external"}
(SI 2020/1050), and the [Supplementary Protection Certificates
(Amendment) (EU Exit) Regulations
2020](https://www.legislation.gov.uk/uksi/2020/1471/contents/made){rel="external"}
(SI 2020/1471). References to "the assimilated law" in the Manual should
be read as referring to the two Regulations as amended by this
legislation.

### SP0.13.2 {#refSP0-13-2}

The changes made by the above legislation apply to new applications
filed after 1 January 2021, as well as SPCs already granted before that
date. A transitional arrangement applies to applications for SPCs filed
before, but still pending on, 1 January 2021, where the amendments made
by SI 2020/1471 do not apply whilst the application remains pending.
Texts of the amended Regulations (in consolidated and marked-up forms)
and the transitional version of the Regulations can be found at
<https://www.gov.uk/guidance/spcs-and-the-northern-ireland-protocol#more-information>.

### SP0.14 {#refSP0-14}

For interpretation of assimilated law, case law of the Court of Justice
of the European Union (CJEU) issued before 31 December 2020 continues to
apply as "assimilated case law" under section 6 of the Withdrawal Act.
Courts and tribunals at the level of the Court of Appeal and above are
not bound to follow this case law and may choose to depart from it,
applying the same test followed by the Supreme Court when deciding
whether to depart from its own case law. However, courts and tribunals
below that level remain bound to follow the principles and
interpretations provided by assimilated case law; these are also binding
on the Office.

### SP0.14.1 {#refSP0-14-1}

Where changes have been made to assimilated law since its incorporation
into domestic law (see SP0.13.1-SP0.13.2 above), assimilated case law
may still be used to interpret it, so long as doing so is consistent
with the intention of the changes. In relation to SPCs, such case law
therefore continues to be relevant to the interpretation of the amended
legislation; in particular, as it relates to the processing and
examination of SPC applications at the Office. References in the Manual
to judgments of the CJEU should be taken as references to assimilated
case law as applicable under the Withdrawal Act.

### SP0.14.2 {#refSP0-14-2}

Courts and tribunals are not bound by judgments of the CJEU issued after
31 December 2020, but may have regard to such judgments in the same way
as any other foreign court.

### The Northern Ireland Protocol

### SP0.15 {#refSP0-15}

A part of the Agreement on the Withdrawal of the United Kingdom from the
European Union, the Protocol on Ireland/Northern Ireland ("the Northern
Ireland Protocol") governs how trade and other cross-border functions
operate on the island of Ireland, to avoid a hard border. This includes
setting out the requirements which apply to goods being placed on the
market in Northern Ireland.

### SP0.15.1 {#refSP0-15-1}

As a result, certain provisions of EU law continue to apply to Northern
Ireland under the Northern Ireland Protocol, including those governing
the regulation of human and veterinary medicines and plant protection
products. Marketing authorisations which cover Northern Ireland are
therefore granted by the relevant UK authority under the applicable EU
law. In addition, authorisations granted by the European Medicines
Agency for EU and EEA member States also have effect in Northern
Ireland.

### SP0.15.2 {#refSP0-15-2}

Although Regulation (EC) 1901/2006 (see SP0.11) is one of the pieces of
legislation which applies via the Northern Ireland Protocol, Article 36
-- which provides for the six-month SPC extension -- is specifically
excluded. The provision of the extension is therefore a matter for UK
law alone, although some of the requirements for an extension, such as
the paediatric investigation plan, may still be dependent on EU law and
processes (see [SPM8.09](#refSPM8-09)).

### Regulation (EC) No 469/2009 of the European Parliament and of the Council (Medicinal Products)

### Recital to the Regulation

### The European Parliament and the Council of the European Union

  -----------------------------------------------------------------------
   

  Having regard to the Treaty establishing the European Community, and in
  particular Article 95 thereof,\
  Having regard to the proposal from the Commission,\
  Having regard to the opinion of the European Economic and Social
  Committee (OJ C 77, 31.3.2009, p 42),\
  Acting in accordance with the procedure laid down in Article 251 of the
  Treaty (Opinion of the European Parliament of 21 October 2008 (not yet
  published in the Official Journal) and Council Decision of 6 April
  2009),\
  Whereas\
  (1) Council Regulation (EEC) No 1768/92 of 18 June 1992 concerning the
  creation of a supplementary protection certificate for medicinal
  products (OJ L 182, 2.7.1992, p. 1) has been substantially amended
  several times. In the interests of clarity and rationality the said
  Regulation should be codified.\
  (2) Pharmaceutical research plays a decisive role in the continuing
  improvement in public health.\
  (3) Medicinal products, especially those that are the result of long,
  costly research will not continue to be developed in the Community and
  in Europe unless they are covered by favourable rules that provide for
  sufficient protection to encourage such research.\
  (4) At the moment, the period that elapses between the filing of an
  application for a patent for a new medicinal product and authorisation
  to place the medicinal product on the market makes the period of
  effective protection under the patent insufficient to cover the
  investment put into the research.\
  (5) This situation leads to a lack of protection which penalizes
  pharmaceutical research.\
  (6) There exists a risk of research centres situated in the Member
  States relocating to countries that offer greater protection.\
  (7) A uniform solution at Community level should be provided for,
  thereby preventing the heterogeneous development of national laws
  leading to further disparities which would be likely to create
  obstacles to the free movement of medicinal products within the
  Community and thus directly affect the establishment and the
  functioning of the internal market.\
  (8) Therefore, the provision of a supplementary protection certificate
  granted, under the same conditions, by each of the Member States at the
  request of the holder of a national or European patent relating to a
  medicinal product for which marketing authorisation has been granted is
  necessary. A Regulation is therefore the most appropriate legal
  instrument.\
  (9) The duration of the protection granted by the certificate should be
  such as to provide adequate effective protection. For this purpose, the
  holder of both a patent and a certificate should be able to enjoy an
  overall maximum of fifteen years of exclusivity from the time the
  medicinal product in question first obtains authorization to be placed
  on the market in the Community.\
  (10) All the interests at stake, including those of public health, in a
  sector as complex and sensitive as the pharmaceutical sector should
  nevertheless be taken into account. For this purpose, the certificate
  cannot be granted for a period exceeding five years. The protection
  granted should furthermore be strictly confined to the product which
  obtained authorization to be placed on the market as a medicinal
  product.\
  (11) Provision should be made for appropriate limitation of the
  duration of the certificate in the special case where a patent term has
  already been extended under a specific national law,
  -----------------------------------------------------------------------

### Have adopted this regulation

### SPM 0.01 {#refSPM0-01}

As is stated in Halsbury's Laws of England (4th Edition, vol 51, pages
346-348), reference may be made to the recitals in the preamble of a
measure in order to confirm the interpretations to be given to a
provision of EU law. This may be necessary in order to arrive at the
clear, as opposed to the literal meaning of a provision: literal
analysis of a text is not always appropriate when regard is had to the
nature and scheme of a measure, or the circumstances in which a
provision was adopted.

### SPM 0.01.1 {#refSPM0-01-1}

Under section 6 of the Withdrawal Act, assimilated law is to be
interpreted in accordance with "principles laid down by \[...\] the
CJEU" before 31 December 2020; this includes the use of recitals as an
interpretative tool. The recitals therefore remain relevant for
interpretation of the Medicinal and Plant Protection Regulations as
incorporated into domestic law.

### SPM0.01.2 {#refSPM0-01-2}

In [Research Corp's SPC (\[1994\] RPC
667](http://rpc.oxfordjournals.org/content/111/20/667.abstract){rel="external"}),
the Patents Court, upholding a decision of the hearing officer (\[1994\]
RPC 387), found that Article 5 of the Regulation was clear in the
context of Community law as a whole: neither the recitals nor an
explanatory memorandum issued by the Commission in 1990 contained
anything to suggest that the phrase "same limitations" should exclude
endorsement licences of right of a "new existing patent" under the
Patents Act 1977. see also [SPM5.01](#refSPM5-01). The court found the
matter acte claire and declined to refer the construction of Article 5
to the European Court of Justice. Similarly, in Draco AB's SPC
Application \[1996\] RPC 417 the Patents Court declined to refer the
decision of the hearing officer see [SPM1.04](#refSPM1-04) to the
European Court. However, in Re Yamanouchi Pharmaceuticals Co. Ltd
(unreported judgment of 31 October 1994) the Patents Court decided to
refer the construction of the transitional provisions of Article 19 of
Regulation (EEC) No 1768/92 to the European Court see
[SPM19.02](#refSPM19-02)and Yamanouchi Pharmaceuticals Co. Ltd v
Comptroller-General \[1997\] RPC 844). The Patents Court has also
referred Novartis AG and University College London & Novartis AG and
Institute of Microbiology and Epidemiology SPC Applications [BL
O/044/03](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/044/03){rel="external"}
and \[2005\] RPC 33 see [SPM8.02](#refSPM8-02) and
[SPM13.04](#refSPM13-04) and Yissum Research and Development Company of
the Hebrew University of Jerusalem
[BLO/222/04](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/222/04){rel="external"}
see [SPM1.02](#refSPM1-02), [SPM1.04](#refSPM1-04) and,
[SPM3.05](#refSPM3-05) to the Court of Justice; in the latter case, on
the basis that parallel reference in other jurisdictions indicated that
the matter was not acte clair.

### SPM 0.02 {#refSPM0-02}

In Draco AB's SPC Application, the Patents Court also refused leave to
appeal to the Court of Appeal, on the basis that it should follow
logically from the court considering the matter to be acte clair. The
court also observed obiter that where a point turned on material leading
to the enactment of a European instrument (the travaux preparatoires),
it was unlikely to be acte clair.

### SPM 0.03 {#spm-003}

The Patents Court in Draco AB's SPC Application, considering the
recitals to the original Regulation, held that the scheme of the
Regulation was not for the general protection of the fruits of research.
It was to compensate for lost time in the exploitation of inventions
which were patented. The CJEU has made similar remarks in relation to
the Medicinal Regulation, in particular in [Teva & ors v Gilead C-121/17
(at
\[39\]-\[41\])](https://eur-lex.europa.eu/legal-content/en/TXT/?uri=CELEX:62017CJ0121){rel="external"}.

### SPM 0.04 {#refSPM0-04}

Recital 10 of the Medicinal Regulation (i.e. "Whereas all the interests
at stake...") is, following the entry into force of the Plant Protection
Regulation, to be interpreted as directed in recital (17) of the latter
regulation which states:

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  \(17\) Whereas the detailed rules in recitals 12, 13 and 14 and in Articles 3(2), 4, 8(1)(c) and 17(2) of this Regulation are also valid, mutatis mutandis, for the interpretation in particular of recital 9 and Articles 3, 4, 8(1)(c) and 17 of Council Regulation (EEC) No 1768/92.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

In accordance with Article 22 of the Medicinal Regulation, reference to
recital 9 of Regulation (EEC) No 1768/92 is to be read as reference to
recital 10 of the Medicinal Regulation. Recitals 12, 13 and 14 of the
Plant Protection Regulation are as follows:

  -----------------------------------------------------------------------
   

  \(12\) Whereas all the interests at stake in a sector as complex and
  sensitive as plant protection must nevertheless be taken into account;
  whereas, for this purpose, the certificate cannot be granted for a
  period exceeding five years;\
  (13) Whereas the certificate confers the same rights as those conferred
  on the basic patent; whereas, consequently, where the basic patent
  covers an active substance and its various derivatives (salts and
  esters), the certificate confers the same protection;\
  (14) Whereas the issue of a certificate for a product consisting of an
  active substance does not prejudice the issue of other certificates for
  derivatives (salts and esters) of the substance, provided that the
  derivatives are the subject of patents specifically covering them;
  -----------------------------------------------------------------------

This amplification of recital 10 is consistent with the approach that
had already been adopted by the UK Office with regard to the inclusion
of salts and esters in the definition of the product see
[SPM1.03](#refSPM1-03), [SPM2.03](#refSPM2-03), [SPM2.04](#refSPM2-04)
and
[SPP1.03-04](/guidance/manual-of-patent-practice-mopp/regulation-ec-no-1610-96-of-the-european-parliament-and-of-the-council-plant-protection-products/#refSPP1-03).
The cross-reference between the Medicinal and Plant Protection
Regulations remains applicable for interpretation of the assimilated
law.

  -----------------------------------------------------------------------
   

  **Article 1: Definitions**

  For the purposes of this Regulation, the following definitions shall
  apply:\
  (a) 'medicinal product' means any substance or combination of
  substances presented for treating or preventing disease in human beings
  or animals and any substance or combination of substances which may be
  administered to human beings or animals with a view to making a
  medicinal diagnosis or to restoring, correcting or modifying
  physiological functions in humans or in animals;\
  (b) 'product' means the active ingredient or combination of active
  ingredients of a medicinal product;\
  (c) 'basic patent' means a patent which protects a product as such, a
  process to obtain a product or an application of a product, and which
  is designated by its holder for the purpose of the procedure for grant
  of a certificate;\
  (d) 'certificate' means the supplementary protection certificate.\
  (e)'application for an extension of the duration' means an application
  for an extension of the duration of the certificate pursuant to Article
  13(3) of this Regulation and regulation 58A(3) of the Human Medicines
  Regulations 2012;\
  (f) 'comptroller' means the Comptroller-General of Patents, Designs and
  Trade Marks;\
  (g)'court means-\
  (i) as respects England and Wales, the High Court;\
  (ii) as respects Scotland, the Court of Session; and\
  (iii) as respects Northern Ireland, the High Court in Northern
  Ireland;\
  (h) "EEA authorisation" means an authorisation to place a medicinal
  product on the market which has effect in an EEA state in accordance
  with Directive 2001/83/EC or Directive 2001/82/EC;\
  (i)'patent' means a patent which has effect in the United Kingdom;\
  (j) 'UK authorisation' means, in relation to a product, an
  authorisation to place that product on the market in the United Kingdom
  as a medicinal product granted or having effect as if granted in
  accordance with-\
  (i) Part 5 of the Human Medicines Regulations 2012; or\
  (ii) regulation 4(3) of, and Schedule 1 to, the Veterinary Medicines
  Regulations 2013.\
  (ja) "GB authorisation" means, in relation to a product, an
  authorisation to place that product on the market in England and Wales
  and Scotland as a medicinal product granted or having effect as if
  granted in accordance with-\
  (i) Part 5 of the Human Medicines Regulations 2012; or\
  (ii) regulation 4(3) of, and Schedule 1 to, the Veterinary Medicines
  Regulations 2013 as they have effect in England and Wales and
  Scotland;\
  (jb) "NI authorisation" means, in relation to a product, an
  authorisation to place that product on the market in Northern Ireland
  as a medicinal product granted or having effect as if granted in
  accordance with Directive 2001/83/EC or Directive 2001/82/EC as they
  have effect by virtue of the Protocol on Ireland/Northern Ireland in
  the EU withdrawal agreement;\
  (k) 'maker' means the person, established in the United Kingdom, on
  whose behalf the making of a product, or a medicinal product containing
  that product, for the purpose of export to countries outside the United
  Kingdom, the Isle of Man, and the Member States of the European Union
  or for the purpose of storing, is carried out.\
  (l) "prescribed" means prescribed by rules under section 123 of the
  Patents Act 1977.
  -----------------------------------------------------------------------

### Product and Medicinal Product

### SPM 1.01 {#refSPM1-01}

Article 1 distinguishes between the terms "medicinal product" and
"product". Although the Medicinal Regulation creates a certificate for
medicinal products, it is the product - defined by Article 1(b) as the
active ingredient or combination of active ingredients - which is the
subject of the certificate pursuant to Article 2.

### SPM 1.02 {#refSPM1-02}

These definitions do not always correspond to the terminology used in UK
Product Licences and Marketing Authorisations, or the details published
in the official Gazettes see [SP0.04](#refSP0-04). Thus, the product
specified in a Product Licence or Marketing Authorisation is generally
broadly equivalent to the "medicinal product" as defined by Article
1(a), and the "active constituent(s)" or "active ingredient(s)" are
generally broadly equivalent to the "product" as defined by Article
1(b). However, as the hearing officer determined in Yissum Research &
Development Company of the Hebrew University of Jerusalem [BL
O/222/04](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/222/04){rel="external"},
such definitions need not restrict the definition of active ingredient
in accordance with Article 1(b) see [SPM1.04.1](#refSPM1-04-1)

### SPM 1.03 {#refSPM1-03}

In view of the imposition of the terms of recitals (14)-(17) of the
Plant Protection Regulation in the interpretation of recital 10 of the
Medicinal Regulation, the term "active ingredient" in Article 1(b) is
generally interpreted as including any closely related derivative, in
particular a salt or ester, which has obtained an authorisation to be
placed on the market and is protected by the basic patent, unless the
derivative in question can be regarded as a new active ingredient. See
also [SPM0.04](#refSPM0-04) and [SPM2.03-04](#refSPM2-03).

### SPM 1.04 {#refSPM1-04}

In the case of [Draco AB's SPC Application \[1996\] RPC
417](http://rpc.oxfordjournals.org/content/113/14/417.abstract){rel="external"},
the applicants applied for a certificate based on a product licence for
an unpressurised asthma inhaler containing the corticosteroid budesonide
in the form of agglomerated micronised particles. However, two earlier
product licences had been granted for inhalers containing budesonide in
the form of micronised particles together with a propellant and
surfactant as "other constituents". The hearing officer found that the
"product" as defined in Article 1(b) was "budesonide" in the case of all
three inhalers and thus rejected the applicant's submission that it was
"additive free budesonide in the form of agglomerated micronised
particles" in the case of the later inhaler and a combination of
"budesonide, a propellant and a surfactant" in the two earlier inhalers.
The applicant's alternative submission that the agglomerated form of
budesonide was a different "product" from the non-agglomerated form used
in the earlier inhalers was similarly rejected. The Patents Court upheld
the decision, holding that the scope of protection was strictly confined
to the active ingredient in view of the definitions in Articles 1(a) and
1(b) See also [SPM4.02](#refSPM4-02). The applicants obtained leave to
appeal to the Court of Appeal but withdrew their appeal before being
heard.

### SPM 1.04.1 {#refSPM1-04-1}

In Yissum Research & Development Company of the Hebrew University of
Jerusalem [BL
O/222/04](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/222/04){rel="external"}
the hearing officer considered that the definition of active ingredients
found in a marketing authorisation should not necessarily be used to
restrict the definition of product in accordance with Article 1(b). The
product "calcitriol" had been the subject of previous marketing
authorisations and for the purposes of Article 3(d) it was not possible
to distinguish these marketing authorisations on the basis of a
different medical application. However, the hearing officer found that
the active ingredients which define the product are those protected by
the basic patent when strictly confined to the corresponding ingredients
of the authorized medicinal product. Thus it was possible for a
certificate to be granted for calcitriol in combination with the
specific ointment base found in the approved medicinal product. On
appeal the Patents Court \[2004\] EWHC 2880, referred this issue to the
European Court of Justice (C-202/05), which found that Article 1(b) is
interpreted as meaning that where a basic patent protects a second
medical use of an active ingredient, that use does not form an integral
part of the definition of the product [SPM0.01](#refSPM0-01),
[SPM1.02](#refSPM1-02) and [SPM3.05](#refSPM3-05). Questions of a
similar nature regarding the interpretation of the term "combination of
active ingredients of a medicinal product" were submitted by the German
Federal Court to the ECJ in Massachusetts Institute of Technology (ECJ
case C-431/04). The Court ruled that the interpretation of Article 1(b)
does not include within the concept of this term a combination of two
substances wherein only one substance has a therapeutic effect and the
other substance enables a pharmaceutical form of the medicinal product
which is necessary for the therapeutic efficacy of the first substance.
Following Massachusetts Institute of Technology C-431/04 in the decision
Abraxis BioScience LLC BL O/410/16 it was concluded that the product
"paclitaxel formulated as albumin bound nanoparticles" comprised albumin
acting as a carrier and that the product did not qualify as an active
ingredient in its own right or as a combination of active ingredients.
This view was upheld on appeal in Abraxis Bioscience LLC v Comptroller
General of Patents \[2017\] EWHC 14 (Pat), where Arnold J referred
questions to the CJEU (C-443/17) in this regard but nonetheless opined
that the ECJ has held that Article 1(b) should be strictly interpreted
and that it would be inconsistent with such an interpretation for
Article 3(d) to permit SPCs to be obtained for new formulations.

The CJEU has reaffirmed that the meaning of the term "product" is
interpreted without regard to therapeutic use in Santen SAS v Directeur
général de l'Institut national de la propriété industrielle C-673/18.
The Court stated at paragraph 46 of this decision that "product" is
defined "...by reference to an active ingredient or combination of
active ingredients and not by reference to the therapeutic application
of an active ingredient protected by the basic patent or a combination
of active ingredients protected by that patent."

The meaning of the term "product" has also been considered in
GlaxoSmithKline Biologicals S.A. [BL
O/506/12](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/410/16){rel="external"},
wherein the Hearing Officer rejected applications comprising an
adjuvant. On appeal to the patents court GlaxoSmithKline Biologicals
S.A. v Comptroller General of Patents \[2013\] EWHC 619 Pat questions
were referred to the CJEU, at the CJEU in case C-210/13 it was
determined that an adjuvant does not fall within the definition of
"product", when considered either alone or in combination with an active
ingredient. In Forsgren v Österreichisches Patentamt C-631/13 it was
determined that a product, which is identified as a "carrier protein" in
a MA, and is covalently bonded to other active ingredients, can itself
be the subject of a SPC if it produces a "pharmacological, immunological
or metabolic action of its own which is covered by the therapeutic
indications of the marketing authorisation".

### SPM1.04.2 {#spm1042}

In order to determine which components of a medicinal product are active
ingredients and which are not this Office may refer to the summary of
product characteristics and (if available and applicable) an European
public assessment reports (EPAR) as well as other evidence complied by
the applicant. This practice was approved of in [Abraxis Bioscience LLC
v Comptroller General of Patents \[2017\] EWHC 14
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2017/14.html){rel="external"}
and followed in Ethicon, Inc., and Omrix Biopharmaceuticals, Inc. (BL
O/136/22). In this latter case it was determined that an oxidised
regenerated cellulose component in a pharmaceutical composition was a
physical haemostat, and as such, had no "pharmacological action of its
own".

### Basic Patent

### SPM 1.05 {#refSPM1-05}

The basic patent may be either a UK patent or a European patent (UK),
and may protect the product as such, a process to obtain the product or
an application of the product. However, a process for obtaining a known
product may not give rise to a new product. The term "basic" does not
mean that the patent must be the first patent to protect the product: it
is open to a patent holder to designate any patent fulfilling the
criteria of Article 1(c) as the basic patent.

### Maker

### SPM 1.06 {#refSPM1-06}

Article 1(f) defines the "maker" for the purposes of the provisions in
Article 5 that relate to the "manufacturing waiver" (see SPM5.05-09).
The "maker" is defined in Art. 1(f) as the person, established in the
UK, on whose behalf this making takes place; Recital 14 of Regulation
(EU) 2019/933, which introduced the definition, makes it clear that the
maker can directly carry out the making. Article 5 places certain
responsibilities on the maker in order for them to take advantage of the
waiver as discussed in SPM5.06-12.

### UK, GB and NI Authorisations

### SPM 1.07 {#refSPM1-07}

As introduced in the Supplementary Protection Certificates (Amendment)
(EU Exit) Regulations 2020 (see [SP0.13.1](#refSPSP0-13-1)), Article
1(j), 1(ja), and 1(jb) defines:

a "UK authorisation" as an authorisation placing a product on the market
in the United Kingdom in accordance with Part 5 of the Human Medicines
Regulations 2012 or regulation 4(3) of, and Schedule 1 to, the
Veterinary Medicines Regulation 2013;

a "GB authorisation" as an authorisation placing a product on the market
in England and Wales and Scotland in accordance with Part 5 of the Human
Medicines Regulations 2012 or regulation 4(3) of, and Schedule 1 to, the
Veterinary Medicines Regulation 2013; and

an "NI authorisation" as an authorisation placing a product on the
market in Northern Ireland in accordance with Directive 2001/83/EC or
Directive 2001/82/EC as they have effect by virtue of the Protocol on
Ireland/Northern Ireland in the EU withdrawal agreement.

### SPM1.07.1 {#refSPM1 .07 .1}

This accounts for the fact that, under the Northern Ireland Protocol
(see SP0.15), marketing authorisations are governed by different
provisions for Great Britain and Northern Ireland, and the relevant
authorities grant in accordance with those provisions. "NI
authorisation" therefore includes centralised authorisations granted by
the European Medicines Agency, which are granted in accordance with the
listed Directives and have effect in Northern Ireland due to the
application of Regulation (EC) No 726/2004 via the Northern Ireland
Protocol.

### SPM1.07.2 {#refSPM1-07-2}

HMR reg 49, Sch 33A

Under Schedule 33A of the Human Medicines Regulations, a centralised
authorisation granted by the European Medicines Agency which was in
force in Great Britain at 11pm on 31 December 2020 continues to have
effect after that time as a UK marketing authorisation for Great Britain
only (the Northern Ireland element remains part of the centralised
authorisation under Regulation 726/2004). This "converted EU marketing
authorisation", as the Schedule defines it, is treated as if it had been
granted by MHRA under regulation 49(1) of the Human Medicines
Regulations on the date that the centralised authorisation took effect.
This applies even if that authorisation was suspended at the time of the
conversion. Since regulation 49(1) falls within Part 5 of the Human
Medicines Regulations, all converted EU marketing authorisations
therefore fall within the definition of "GB authorisation" for the
purposes of the Medicinal Regulation, and remain valid.

  -----------------------------------------------------------------------
   

  **Article 2: Scope**

  A product may, under the terms and conditions provided for in this
  Regulation, be the subject of a certificate if it is-\
  (a) protected by a patent; and\
  (b) the subject of a UK, GB or NI authorisation prior to being placed
  on the market as a medicinal product.
  -----------------------------------------------------------------------

### SPM 2.01 {#refSPM2-01}

ArtM 1(b) is also relevant.

A certificate can thus be granted in the UK for a product which has
received a UK, GB, or NI authorisation, as defined by Article 1 (see
SPM1.07-1.07.2). The authorisation may take the form of a Product
Licence (formerly issued under the Medicines Act 1968) or a Marketing
Authorisation issued under the Human Medicines Regulations 2012 or the
Veterinary Medicines Regulations 2013 ([SI
2013/2033](https://www.legislation.gov.uk/uksi/2013/2033/contents){rel="external"}).
The authorisation may be granted either by the Medicines and Healthcare
Products Regulatory Agency (MHRA) of the Department of Health and Social
Care (DHSC) for a pharmaceutical product, or by the Veterinary Medicines
Directorate (VMD) of the Department for Environment, Food and Rural
Affairs (DEFRA) for a veterinary product. For Northern Ireland, an
authorisation may also be provided through the centralised system for
granting EU-wide marketing authorisations for medicinal products for
human and veterinary use, governed by Regulation (EC) No 726/2004. This
type of authorisation is granted by a decision of the European
Commission following a favorable opinion from the European Medicines
Agency (EMA) and is accepted by the Office as an NI authorisation. Such
authorisations have no direct application to Great Britain, but may be
used as the basis for a GB authorisation through certain application
routes.

### SPM 2.01.1 {#refSPM2-01-1}

The status of documents provided as equivalent to the authorisation
documents indicated in Article 2 (reference to which is now incorporated
into the definitions in Article 1(j), (ja), and (jb)) have been
considered in the following. In Generics (UK) Ltd v Synaptech Inc
(C-427/09) and Synthon BV v Merz Pharma Gmbh & Co KG (C-195/09) the
Court of Justice of the European Union held that Article 2 should be
interpreted as meaning that a product which was placed on the market in
the European Community as a medicinal product for human use before
obtaining a marketing authorisation in accordance with Council Directive
65/65/EEC (now Directive 2001/83/EC) and, in particular, without
undergoing safety and efficacy testing, is not within the scope of the
Regulation, and may not be the subject of a supplementary protection
certificate. It went on to confirm that any SPC granted for a product
which was outside the scope of the Regulation was invalid (see also
[SPM13.04.2](#refSPM13-04-2)). In Leibniz-Institut für Neue Materialien
Gemeinnützige GmbH BL O/328/14, Cerus Corporation BL O/141/14 and
Angiotech Pharmaceuticals Inc. and University of British Columbia BL
O/466/15 the hearing officer found that each of the applications for the
identified device was out of scope of the Medicinal Regulation. In each
case, the device was the subject of an EC Design Examination Certificate
under Directive 93/42/EEC as opposed to a marketing authorization under
Directive 2001/83/EC. In Boston Scientific Ltd v Deutsches Patent-und
Markenamt (C-527/17) the Court of Justice of the European Union
confirmed that an authorization procedure under Directive 93/42/EEC for
a device incorporating, as an integral part, a substance which if used
separately may be considered a medicinal product, cannot be treated as a
marketing authorization granted under Directive 2001/83, see also
[SPM3.03.2](#refSPM3-03-2). In Erber Aktiengesellschaft BL O/610/20, an
approval granted under Regulation (EC) 1831/2003 relating to the use of
animal feeds did not meet the requirement under Article 2 that the
product had been the subject of an "administrative procedure as laid
down in Directive 2001/82/EC" concerning veterinary medicinal products.
As a result, the hearing officer refused the SPC application relying on
this approval, (see
[SPP2.02](/guidance/manual-of-patent-practice-mopp/regulation-ec-no-1610-96-of-the-european-parliament-and-of-the-council-plant-protection-products/#refSPP2-02)).

### SPM 2.02 {#spm-202}

A certificate is granted for a product which constitutes the active
ingredient or combination of active ingredients of a medicinal product
see [SPM1.01](#refSPM1-01).

### SPM 2.03 {#refSPM2-03}

A certificate can only cover a single product, i.e. a single active
ingredient or combination of active ingredients. Different products will
need to be the subject of different certificates, even if they are
protected by the same basic patent. Whether a new certificate is
required for a derivative (eg a salt or ester) of a product which has
already been granted a certificate depends on whether or not the
derivative can be regarded as a new active ingredient see
[SPM0.04](#refSPM0-04) and [SPM1.03](#refSPM1-03). A new certificate may
only be granted for a combination of (a) an active ingredient for which
a certificate has already been granted with (b) one or more other active
ingredients, if the combinations are themselves the subjects of separate
patents are specifically identifiable from the patent as viewed by the
person skilled in the art at the priority or filing date using the test
set out in Teva (C-121/17). A further certificate will not be granted
for the same active ingredient notwithstanding any changes to the
physical form of that ingredient or to other features of the medicinal
product (eg use of a different excipient or different pharmaceutical
presentation). See also [SPM1.04](#refSPM1-04) and
[SPM3.02.6.1](#refspm3-02-6-1).

### SPM 2.04 {#refSPM2-04}

ArtM 3(a) and (ArtP 3(1)(a)) are also relevant.

A certificate may be granted for a compound optionally in derivative
form to the extent that derivatives are protected by the basic patent
see [SPM0.04](#refSPM0-04). Examples of wording which have been accepted
are:

  -------------------------------------------------------------------------------------------
   
  X optionally in the form of the hydrochloride;
  X optionally in the form of a pharmaceutically acceptable salt such as the hydrochloride;
  X optionally in the form of a pharmaceutically acceptable salt.
  -------------------------------------------------------------------------------------------

see also [SPM3.02](#refSPM3-02) and [SPM3.02.6.3](#refSPM3-02-6-3).

  -----------------------------------------------------------------------
  **Article 3: Conditions for obtaining a certificate**

  Where an application is submitted under Article 7, a certificate shall
  be granted if, and at the date of submission of that application:\
  (a) the product is protected by a basic patent in force;\
  (b) there is a valid UK, GB or NI authorisation to place the product on
  the market;\
  (c) the product has not already been the subject of a certificate;\
  (d) the authorization referred to in point (b) is the first UK, GB or
  NI authorization to place the product on the market as a medicinal
  product in the territory of the United Kingdom, the territory of
  England and Wales and Scotland or the territory of Northern Ireland as
  the case may be.
  -----------------------------------------------------------------------

### SPM 3.01 {#refSPM3-01}

The conditions of Article 3 must be satisfied at the date of making an
application. Thus, at that date:\
the basic patent protecting the product must be in force in the UK;\
the product must not previously have been the subject of a certificate
in the UK;\
a valid UK, GB or NI authorization to place the product on the market
must have been granted see [SPM2.01](#refSPM2-01);\
this authorisation must be the first authorisation to place the product
on the market as a medicinal product in the relevant part of the United
Kingdom.

### SPM 3.01.1 {#refSPM3-01-1}

For SPC applications pending as of 1 January 2021, a transitional form
of Article 3 applies, which states:

  -----------------------------------------------------------------------
   

  Where an application is submitted under Article 7, a certificate shall
  be granted if, at the date of submission of that application-\
  (a) the product is protected by a basic patent in force;\
  (b) there is a valid UK authorisation to place the product on the
  market;\
  (c) the product has not already been the subject of a certificate;\
  (d) the authorisation referred to in point (b) is the first UK
  authorisation to place the product on the market as a medicinal
  product.
  -----------------------------------------------------------------------

Under this transitional form of the Article, a 'whole UK' approach
applies to the assessment of Article 3. The authorisation to place the
product on the market must be a UK authorisation - as defined in Article
1(j) - and this authorisation must be the first to place the product on
the market in the UK as a whole. This principle follows through to the
rest of the Regulation in its transitional form. Where a UK
authorisation was replaced by a GB and NI authorisation at the end of
the transition period, regulation 7(7) of the Supplementary Protection
Certificate (EU Exit) Regulations 2020 applies; the combination of the
two authorisations is treated as equivalent to a UK authorisation for
the purposes of the transitional form of the Article.

\[The examiner on the case may request details of the replacement
authorisations for recording on the Register.\]

### SPM 3.01.2 {#refSPM3-01-2}

Applications filed after 1 January 2021 are examined in accordance with
Article 3 as set out above SPM3.01, whether relying on authorisations
granted before or after the end of the transitional period. Additional
transitional provisions apply depending on when and under what legal
basis the authorisation is granted; these are set out in regulation 7 of
the Supplementary Protection Certificate (EU Exit) Regulations 2020.

### Article 3(a)

### SPM 3.02 {#refSPM3-02}

The question of whether the product is protected by the basic patent is
determined in accordance with the usual canons of construction of patent
claims, as was confirmed by the European Court of Justice in Farmitalia
Carlo Erba S.r.l.'s SPC Application (2) (\[2000\] RPC 580 - ECJ Case
C-392/97). In Takeda Chemical Industry's Application (unreported oral
decision on application No SPC/GB93/017), the hearing officer held that
a product comprising the acetate salt of a peptide was protected by a
basic patent, even though the claims did not on a literal construction
include derivatives of the polypeptide, on the grounds that the
description made it clear that the polypeptide could be obtained in the
form of the acetate. In [Centocor Inc's Application \[1996\] RPC
118](http://rpc.oxfordjournals.org/content/113/4/118.full.pdf+html){rel="external"},
the hearing officer held that a product consisting of a monoclonal
antibody was not protected by claims to a combined preparation of a
monoclonal antibody and an anti-microbial agent. In Takeda Chemical
Industries Ltd's Applications \[2004\] RPC 1 (upheld on appeal to the
Patents Court \[2004\] RPC 3) the hearing officer held that products
comprising a combination of ingredients were not protected by patents
which related to only one of the ingredients. The basic patents
contained no reference to the combinations specified in the SPC
applications. The Court of Appeal in Generics (UK) Ltd v Daiichi
Pharmaceutical Co Ltd \[2009\] EWCA Civ 646, \[2009\] RPC 23 upheld the
earlier decision of the Patents Court (\[2008\] EWHC 2413 (Pat),
\[2009\] RPC 4) confirming this interpretation. The Court found the SPC
for levofloxacin, the (-) enantiomer of the racemic mixture, ofloxacin,
to have been properly granted despite the existence of earlier marketing
authorisations to this racemic mixture. In Gilead Sciences, Inc. [BL
O/006/08](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/006/08){rel="external"}
the hearing officer held that a basic patent which protected a specific
active compound and which claimed a combination of the active substance
optionally in combination with other (unspecified) therapeutic
ingredients did not protect the combination of the active compound with
another specific active compound which was the subject of the SPC
application. The claim and a corresponding reference in the description
were the only indication of a combination of active ingredients in the
basic patent. On appeal the Patents Court held (in Re Council Regulation
(EEC) No 1768/92 \[2008\] EWHC 1902 (Pat)) that although the specific
combination was not disclosed in the specification of the basic patent
such a claim did protect the combination within the meaning of Articles
1(c) and 3(a) The court determined that protection was conferred through
inclusion of the extending phrase in the claims "comprising optionally
other active ingredients" and not by reference to the notion of
protection by infringement - the so-called infringement test. In
Astellas Pharma Inc. [BL
O/052/09](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/052/09){rel="external"}
the hearing officer similarly refused the SPC appliaction on the basic
patent either specifically nor generically protected, a combination of
active ingredients was not a basic patent which protected a combination
of active ingredients for the purposes of Article 3(a). On appeal to the
Patents Court (Astellas Pharma Inc v Comptroller-General of Patents
\[2009\] EWHC 1916 (Pat)), Arnold J upheld the decision of the hearing
officer after applying the test articulated in Gilead. Arnold J
distinguished the facts of Astellas from Gilead because, unlike in
Gilead, the basic patent did not specifically disclose and claim a
combination of active ingredients. Analysis of the claims in the way
proposed in Gilead and Astellas as a way to determine compliance with
Article 3(a) has long since been superseded, [see
SPM3.02.6.1](#refSPM3-02-6-1).

### SPM 3.02.1 {#refSPM3-02-1}

\[Deleted\]

### SPM 3.02.2 {#refSPM3-02-2}

\[Deleted\]. Incorporated into SPM3.03.1

### SPM 3.02.3 {#spm-3023}

\[Deleted\]. Incorporated into SPM3.03.2

### SPM 3.02.4 {#refSPM3-02-4}

The criteria by which claims are judged to comply with Articles 3(a) and
3(b) of the Regulation have also been considered in a number of cases
relating to medicinal products in the vaccine field. In Medeva BV's SPC
Applications (BL O/357/09) the hearing officer, in following the
Astellas and Gilead decisions, found that a basic patent protecting only
two particular active ingredients did not protect, for the purposes of
Article 3(a), a product containing these two active ingredients in
combination with other active ingredients. On appeal to the Patents
Court (Medeva BV v The Comptroller General of Patents, \[2010\] EWHC 68
(Pat), \[2010\] RPC 20), Kitchin J upheld the decision of the hearing
officer and dismissed the appeal, noting that "...It is plain that
"product" must have the same meaning in Article 1(b) and Article 3(a).
If the product of Article 1(b) is the whole combination of active
ingredients then so it remains for the purposes of Article 3(a)...".
Ultimately in Medeva v Comptroller-General of Patents C-322/10 the CJEU
ruled that, for the purposes of Article 3(a), in order to be protected
active ingredients needed to be 'specified' in the wording of the claims
of the basic patent filed in support of the SPC application, thereby
rejecting the so-called 'infringement test'. In light of the answers
from the CJEU, the Court of Appeal confirmed that Medeva's applications
did not meet these requirements and dismissed their appeal (Medeva BV v
Comptroller General of Patents \[2012\] EWCA Civ 523).

### SPM 3.02.5 {#refSPM3-02-5}

\[Deleted\]

### SPM 3.02.6 {#refSPM3-02-6}

In Novartis Pharmaceuticals UK Limited and Medimmune Limited/Medical
Research Council \[2012\] EWHC 181 (Pat), Arnold J commented at
paragraph 53 that "the test laid down by the Court of Justice in Medeva
and its progeny is unclear save in its rejection of the infringement
test in combination cases. In particular, it is unclear precisely what
is meant by "specified (or identified) in the wording of the claims".
However, after interpreting and applying the Medeva C 322/10 decision
(along with the decisions C 422/10 Georgetown University and Others, C
630/10 University of Queensland and CSL, C 6/11 Daiichi Sankyo, and
C-518/10 Yeda Research and Development) he went on to find that a claim
to a general method of producing a molecule with binding specificity for
a particular target did not adequately specify or identify the specific
antibody ranibizumab for the purposes of Article 3(a). In Actavis Group
PTC EHF / Actavis UK Limited and Sanofi and Sanofi Pharma Bristol-Myers
Squibb SNC \[2012\] EWHC 2545 Arnold J reiterated his doubt surrounding
what is meant by "specified (or identified) in the wording of the
claims" and that the preceding references did not provide a clear test
which could be applied to cases such as the present. Accordingly, he
referred a further question on the interpretation of Article 3(a) to the
Court of Justice of the European Union for a preliminary ruling
(C-443/12). The CJEU commented that the aim of the SPC system is to
compensate for delay in marketing "the core inventive advance" of a
patent, but did not find it necessary to address the Art. 3(a) question.
Further questions were referred to the CJEU for a preliminary ruling
(C-493/12) from the Patents Court in Eli Lilly & Company v Human Genome
Sciences Inc \[2012\] EWHC 2290 (Pat); the court determined that a
functional definition may suffice for a product to be protected by a
basic patent if:

> the claims relate, implicitly but necessarily and specifically to the
> active ingredient in question.

On coming back before the high court the judgment in [Eli Lilly &
Company v Human Genome Sciences Inc \[2014\] EWHC 2404
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2014/2404.html){rel="external"}
held that the question of compliance with Article 3(a) is simply 'does
the product fall within the scope of the claims', but that in the case
of combination products a proviso exists, in that a product is not
protected solely by wording such as "comprises" that extends the claim
beyond its principle scope.

### SPM3.02.6.1 {#refSPM3-02-6-1}

Arnold J in Teva UK & Ors v Gilead \[2017\] EWHC 13 (Pat) ("Teva I")
reviewed the judgments of the CJEU in respect of how combination
products at least may satisfy Article 3(a) and again referred to the
CJEU (C-121/17) the question "what are the criteria for deciding whether
'the product is protected by a basic patent in force' in Article 3(a)".
The Grand Chamber of the CJEU in C-121/17 has ruled that a product is
protected by a basic patent if the claims relate necessarily and
specifically to that product, and that this is to be judged from the
point of view of a person skilled in the art on the basis of the prior
art at the priority date of the basic patent. The Court effectively
provided a two step test:

i\) the product must necessarily, in light of the description and
drawings fall under the invention of the basic patent and

ii\) the ingredients must be specifically identifiable in light of all
the information disclosed.

In Teva & Ors v Gilead Sciences Inc \[2018\] EWHC 2416 (Pat) ("Teva
II"), Arnold J applied the judgment of the CJEU in C-121/17 as requiring
both steps in a two-step test to be satisfied. Firstly, having regard to
Eli Lilly C 493/12, that the product "must be one that the skilled
person would understand, on the basis of the description and drawings
and their common general knowledge, to embody the technical contribution
made by the patent"; And in a second step, that "the product must be
specifically identifiable by the person skilled in the art in the light
of the description and drawings and the prior art, which must mean their
common general knowledge, as at the filing date or priority date of the
patent, and not merely in the light of information which becomes
available later.". Thus, as Arnold J confirmed in Eli Lilly and Company
v Genentech \[2019\] EWHC 388 (Pat), it is not a question of whether or
not the product was or was not created before the priority date of the
patent, but whether the skilled person, armed with the common general
knowledge available at the priority or filing date, would consider the
product specifically identifiable from the patent. However, whereas
Arnold J concluded the first step of the test required that the product
embodies the technical contribution made by the patent, the Court of
Appeal in Teva & Ors v Gilead \[2019\] EWCA Civ 2272 concluded that the
correct interpretation of the first step is merely an elaborate
exposition of the "necessarily" part of the test in Eli Lilly C-493/12,
so that in a combination product each component must be required by the
claim. The Court of Appeal held that the reference in the test to the
common general knowledge of the skilled person is the means by which the
skilled person decides what the claims relate to, rather than as a means
to formulate the technical contribution.

### Article 3(a) - Process claims

### SPM3.02.6.2 {#ref3-02-6-2}

In C-630/10, University of Queensland, CSL Ltd v Comptroller-General of
Patents, Designs and Trade Marks the CJEU provided its decision by
reasoned order with reference to C-322/10 (Medeva) and C-422/10
(Georgetown), reiterating the answers therefrom. The CJEU further
clarified that if a basic patent relates to a process by which a product
is obtained Article 3(a) only allows an SPC to be granted for a product
which is identified in the wording of the claims of the patent as the
product deriving from the process in question. The Court also stated
that whether it is possible to obtain the product directly as a result
of the process is irrelevant in that regard. Applying the CJEU's
decision, the Patents Court determined the compliance or otherwise of
the applications before it (University of Queensland and CSL limited v
Comptroller General of Patents, \[2012\] EWHC 223 (Pat)). This reasoning
was also applied in Icahn School of Medicine at Mount Sinai BL O/552/14
which determined that Article 3(a) is complied with if the product
identified in the patent claims is the product deriving from the process
protected by that patent, but that it is not also necessary to establish
that the product is produced by the method of the basic patent.

### Article 3(a) - Determining compliance

SPM3.02.6.3

The Office will apply the two-part test set out by the CJEU in Teva to
determine compliance with Article 3(a) (See SPM 3.02.6.1). Therefore, in
order to establish that a product is specifically identifiable in the
wording of the claims of the basic patent filed in support of the SPC,
having regard to the normal canons of claim interpretation, it has to be
confirmed that the product is:\
i) indicated in a claim;\
ii) encompassed by a Markush formula;\
iii) shown to result from the process protected by the basic patent; or\
iv) encompassed by a functional definition.

In respect of (iii) and (iv) it may be necessary to ask the applicant to
provide evidence that this is the case, for example, in the form of a
witness statement from a suitable patent addressee. Markush formulae
claims have in general been accepted as the basis for an SPC - in Sandoz
Ltd & Anor v G.D. Searle & Anor \[2017\] EWHC 987 (Pat), the Patents
Court held that Article 3(a) is complied with if the product falls
within the scope of a Markush formula. However, if the product is not
expressly mentioned in the claims, it will also be necessary to consider
if the skilled person in the art can identify the product specifically
on the basis of the common general knowledge in the art, and the prior
art at the priority date of the basic patent, following Teva C-121/17
(see SPM3.02.6.1). Furthermore, having regard to Royalty Pharma
Collection Trust v Deutsches Patent und Markenamt C-650/17, it may also
be necessary to show that the product was not developed after the filing
date of the patent, following an independent inventive step. Therefore,
in relation to Markush formulae (ii) or functional definition claims
(iv), it is necessary to consider whether the product was identifiable
at the priority date of the basic patent, using the test set out in Teva
(C-121/17) as informed by Royalty Pharma (C-650/17). In order to meet
the requirement that the product is "...in the wording of a claim" an
applicant may have recourse to section 27 of the Patents Act to amend
the basic patent accordingly. Questions concerning the practice of
amending a basic patent to render an application compliant with Article
3(a) were referred to the CJEU in Actavis Group and Actavis UK v
Boehringer Ingelheim Pharma \[2013\] EWHC 2927 (Pat) however in C-577/13
Actavis Group PTC EHF, Actavis UK Ltd v Boehringer Ingelheim Pharma GmbH
& Co. KG these questions were not answered (see also
[SPM3.04](#refSPM3-04), [SPM10.14.1](#ref10-14-1)).

### SPM 3.02.7 {#spm-3027}

\[Deleted\]

### Article 3(b)

### SPM 3.03 {#spm-303}

In C-322/10, Medeva v Comptroller-General of Patents the CJEU ruled
that, for the purposes of Article 3(b), SPCs can be granted for a
combination of active ingredients specified in the wording of the claims
of the basic patent relied on where the medicinal product of the
marketing authorisation submitted in the SPC application also contains
other active ingredients in addition to the combination specified in the
basic patent. Similarly, in Georgetown University, University of
Rochester, Loyola University of Chicago v Comptroller-General of
Patents, C-422/10; when the case ultimately came before the CJEU the
question was the same as that referred in Medeva in respect of Article
3(b). Consistent with the approach taken in C-322/10, the CJEU ruled
that SPCs can be granted for an active ingredient specified in the
wording of the claims of the basic patent where the marketing
authorisation relied upon in the SPC application is for a product that
contains other active ingredients in addition to the active ingredient
specified in the basic patent.

### SPM 3.03.1 {#refSPM3-03-1}

In Imclone Systems Inc. Ltd & Aventis Holdings Inc. (BL O/066/10), the
hearing officer found that a marketing authorisation for a single active
ingredient which additionally specified the clinical use of that active
in conjunction with another active ingredient was not, for the purposes
of Article 3(b) of the Regulation, a valid authorisation to place a
combination product on the market as a medicinal product. The MA
referred to how the active ingredient, an antibody known as cetuximab
(medicinal product name Erbitux®), which had cytostatic properties,
could be used clinically in conjunction with another active ingredient,
an anti-cancer drug irinotecan, with cytotoxic properties, to treat
certain types of cancer and that the presence of the cetuximab reduced
the amount of irinotecan required to achieve a clinical effect. However,
the hearing officer found that the MA has to be considered in its
entirety and not just in respect of the clinical particulars when
deciding what is the authorised medicinal product, and hence what is the
product that can be protected by an SPC. Similarly, the authorisation
used to support an SPC application for a so called "loose combination"
was found not to comply with Article 3(b), in Roche Glycart AG [BL
O/711/22](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O71122){rel="external"}).
Where the MA stipulated that obinutuzumab was to be used in combination
with the anti-neoplastic agent bendamustine, the hearing officer found
that restrictions had been placed on the marketing of obinutuzumab, and
the MA did not constitute placing bendamustine on the market. The MA
provided was therefore found to relate to the antibody obinutuzumab
alone.

::: call-to-action
This approach to "loose combinations" has since been upheld on appeal of
hearing decision BL O/1053/22. The hearing officer found that an
authorisation for the medicinal product XADAGO (RTM) was for safinamide
alone and not combinations comprising safinamide with levodopa and a
peripheral decarboxylase inhibitor (PDI). This was upheld before the
High Court in Newron Pharmaceutical SPA v The Comptroller-General of
Patents, Trade Marks and Designs \[2023\] EWHC 1471 (Ch), and the Court
of Appeal \[2024\] EWCA Civ 128.  Having regard to Santen the courts
emphasised the importance of determining what the product is, for the
purpose of determining compliance with Article 3(b), this being what the
applicant can place on the market, rather than the use or uses of the
product - the combination being an aspect of how safinamide is used. In
the Court of Appeal judgment LJ Birss also opined that it should be
unnecessary to supply expert evidence to assess the scope of an
authorisation.
:::

### SPM 3.03.2 {#refSPM3-03-2}

In Farmitalia Carlo Erba S.r.l's SPC Application (2) the Court also
ruled that where an active ingredient is referred to in the marketing
authorisation in the form of an individual salt the certificate is
capable of covering the active ingredient both as referred to and in its
derived forms such as salts and esters as medicinal products, provided
that the derived forms also enjoy the protection of the basic patent.

### SPM 3.03.3 {#refSPM3-03-3}

Although Article 3(b) requires a valid authorisation to have been
granted, there appears to be no requirement that the authorisation
should still be in force at the date of making the application for a
certificate (e.g. it may be withdrawn or have lapsed before the date of
the application for the certificate, such an SPC may however ultimately
lapse, see SPM14.02-05). In Merck Sharp & Dohme Corporation v
Comptroller General of Patents \[2016\] EWHC 1896 (Pat), Arnold J
proposed (on appeal from the decision of the hearing officer in BL
O/117/16) that the absence of a granted marketing authorisation is not
an irregularity that may be cured after the date of application; he
nonetheless referred a question on this point to the CJEU. In C-567/16
Merck Sharp & Dohme Corporation v Comptroller General of Patents, the
court confirmed the views expressed by Arnold J that an end of procedure
notice, issued under Article 28(4) of Directive 2001/83 by the reference
Member State (in this case, Germany) as part of the EU decentralised
procedure, was not a valid authorisation for the purposes of Article
3(b).

### SPM 3.03.4 {#refSPM3-03-4}

In British Technology Group Ltd's SPC Application \[1997\] RPC 118 the
hearing officer found that a letter from the Medicines Control Agency,
granting permission for a product to be supplied for a proposed clinical
trial, was not an acceptable market authorisation in that it was not
issued in accordance with Directive 65/65/EEC or Directive 81/851/EEC
(later replaced by Directives 2001/83/EC and 2001/82/EC respectively),
nor did it provide a summary of product characteristics as required by
Article 8(1)(b) (see SPM2.01, SPM10.17.1 and SPP3.02).

### SPM 3.03.5 {#refSPM3-03-5}

In Cerus Corporation BL O/141/14, Leibniz-Institut für Neue Materialien
Gemeinnützige GmbH BL O/328/14, and Angiotech Pharmaceuticals Inc. and
University of British Columbia BL O/466/15, the hearing officer found
that EC design examination certificates relating to medical devices did
not meet the requirements of Article 3(b). The EC design examination
certificates were issued having regard to Article 1(4) of Directive
93/42/EEC because the devices incorporate as an integral part, a
substance which, if used separately, may be considered a medicinal
product and which acts upon the body with action ancillary to that of
the device. Such "class III" devices (as defined in Article 9 and Annex
IX of Directive 93/42/EEC) require the safety quality and usefulness of
the substance to be assessed, in an analogous way to authorisation under
Directive 2001/83/EC. However, this assessment process was not found to
be the same or equivalent to the process carried out to authorise a
medicinal product for human use in accordance with Directive 2001/83/EC.
The CJEU in Boston Scientific Ltd v Deutsches patetn-und Markenamt
(C-527/17) confirmed that the authorization procedure under Directive
93/42 cannot be treated as an authorization granted under Directive
2001/83/EC, [see also SPM 2.01](#refSPM2-01).

### SPM3.03.6 {#spm3036}

In Forsgren v Österreichisches Patentamt C-631/13, it was determined
that an SPC is precluded for an active ingredient whose effect does not
fall within the therapeutic indications covered by the wording of the
marketing authorisation.

### Article 3(c)

### SPM 3.04 {#refSPM3-04}

SPM3.04 Article 3(c) precludes the grant of a second certificate for a
product where the first certificate has been granted before the date of
application for the second certificate. However, in Chiron Corporation
and Novo Nordisk A/S \[2005\] RPC 24 the hearing officer concluded that
the grant of a supplementary protection certificate for a product to one
holder of a basic patent before an application is lodged in relation to
the same product by a different holder of a different basic patent on
the basis of a common marketing authorisation does not provide a ground
for rejecting the later application under Article 3(c) of the
Regulation. The European Court of Justice in AHP Manufacturing BV v
Bureau voor de Industriële Eigendom (C-482/07) held that Article 3(c) of
the Regulation does not prevent the grant of a certificate to the holder
of a basic patent for a product if, at the time of the submission of the
application for a certificate, one or more SPCs have already been
granted to one or more holders of one or more other basic patents. Thus
it may be possible in specific circumstances for a further certificate
to be granted when a certificate already exists. In Takeda Chemical
Industries Ltd's Applications \[2004\] RPC 2, the hearing officer held
that products comprising a combination of ingredients were not precluded
from grant of an SPC because one of the ingredients had already been
granted an SPC. However, in light of the comment made in Medeva that
"only one certificate may be granted for the basic patent", Arnold J in
Actavis Group PTC EHF / Actavis UK Limited and Sanofi and Sanofi Pharma
Bristol-Myers Squibb SNC \[2012\] EWHC 2545 determined that the correct
interpretation of Article 3(c) was not clear and referred the following
question to the CJEU for preliminary ruling (C-443/12), "In a situation
in which multiple products are protected by a basic patent in force,
does the Regulation, and in particular Article 3(c), preclude the
proprietor of the patent being issued a certificate for each of the
products protected?" A similar question was referred from the district
court of The Hague in Georgetown University and Octrooicentrum Nederland
C-484/12. In the resulting decisions, the CJEU determined that it is
possible, on the basis of a patent which protects several different
products, to obtain several SPCs in relation to each of those products
provided that each of those products is protected by the basic patent,
(C-484/12, paragraph 30), but that Article 3(c) prohibits successive
SPCs based on a single patent for the same active in combination with
another active not itself protected by the patent (C-443/12, paragraph
30). Commenting on Art 3(c) in C-577/13 Actavis Group PTC EHF, Actavis
UK Ltd v Boehringer Ingelheim Pharma GmbH & Co. KG (paragraph 39), the
CJEU held that, where an SPC has already been granted relating to an
active ingredient which constitutes the sole subject matter of the
invention, the patent holder is precluded from obtaining an SPC for a
combination product claimed in a subsequent claim of the same patent
comprising that active ingredient and another substance not constituting
the subject matter of the invention. In reference to this judgment,
Arnold J in Teva UK Limited & Ors v Merck Sharp & Dohme Corporation
\[2017\] EWHC 539 (Pat) held that, if the combination represents a
distinct invention protected by the patent, it should not matter whether
it is protected by the same patent or by a different patent. In other
words, it is the active ingredients found to represent the subject
matter of the invention that are critical in determining what the
product is, and not if aspects of the subject matter of the invention
are found in one or more patents.

### SPM 3.04.1 {#spm-3041}

The effect of recital (17) of the Plant Protection Regulation ([see
SPM0.04](#refSPM0-04)) must also be considered in respect of Article
3(c), as Article 3 is to be interpreted in accordance with Article 3(2)
of the Plant Protection Regulation which states:

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Article 3(2) \[Plant Protection Regulation\]**
  The holder of more than one patent for the same product shall not be granted more than one certificate for that product. However, where two or more applications concerning the same product and emanating from two or more holders of different patents are pending, one certificate for this product may be issued to each of these holders.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### SPM 3.04.2 {#ref3-04-2}

Consequently, the grant of one certificate to each patent holder in
respect of the same product on two or more applications each specifying
a different basic patent protecting the product is now allowed. The
hearing officer in Takeda Chemical Industries Ltd's Applications
\[2004\] RPC 2 found that only one certificate for a product should be
granted to the same applicant, having also considered the European Court
of Justice decision in Biogen Inc. v Smithkline Beecham Biologicals SA
(\[1997\] RPC 23, C-181/95). C-354/19 Novartis AG v Patent-och
registreringsverket (PRV) may also bear on how Article 3(2) of the Plant
Protection Regulation is applied. See also
[SPP3.03](/guidance/manual-of-patent-practice-mopp/regulation-ec-no-1610-96-of-the-european-parliament-and-of-the-council-plant-protection-products/#refspp3-03).

### Article 3(d)

### SPM 3.05 {#refSPM3-05}

Which marketing authorisation is considered the first, having regard to
Article 3(d), is determined regardless of the use or indication of the
product authorised. In Santen SAS v Directeur général de l'Institut
national de la propriété industrielle C-673/18, the CJEU ruled (at
\[60\]) that "...an MA for a therapeutic application of a product cannot
be regarded as the first MA for that product as a medicinal product, for
the purpose of Article 3(d) of Regulation No 469/2009, where another MA
was granted previously for a different therapeutic application of the
same product. The fact that the most recent MA is the first MA to fall
within the limits of the protection of the basic patent relied on in
support of the SPC application cannot call that interpretation into
question." This judgment overturned Neurim Pharmaceuticals (1991) Ltd v
Comptroller General of Patents C-130/11, which had established that an
authorisation for a new therapeutic application of an existing product
could be regarded as the first MA for the purposes of Article 3(d).
Santen reverts the interpretation of Article 3(d) to that which predated
Neurim, and as such accords with the judgment in Yissum Research and
Development Company of the Hebrew University of Jerusalem v
Comptroller-General of Patents C-202/05 which states that when a basic
patent protects a second medical use of an active ingredient, that use
does not form an integral part of the definition of the product (see
[SPM0.01](#refSPM0-01), [SPM1.02](#refSPM1-02),
[SPM1.04-1](#refSPM1-04-1)).

### SPM 3.05.1 {#refSPM3-05-1}

Having regard to the Withdrawal Act (including amendments made to it by
the European Union (Withdrawal Agreement) Act 2020), the Hearing Officer
in [Janssen Biotech, Inc BL
O/242/22](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/242/22){rel="external"}
found that the IPO was bound by retained EU case law (as it was then
known), including the judgment in Santen, and thereby refused an SPC for
a different therapeutic application of the product golimumab which had
already been the subject of an SPC.

::: call-to-action
In BL O/0484/23 the Hearing Officer concluded that Santen applied
retroactively "there being no temporal restriction in the Santen
Judgment, and no previous case law in relation to SPCs establishing
one". This interpretation of the temporal effect of Santen was confirmed
on appeal to the Patents Court Merck Serono S.A v The
Comptroller-General of Patents, Trade Marks and Designs \[2023\] EWHC
3240 (Ch).
:::

### SPM 3.05.2 {#refSPM3-05-2}

\[Deleted\]

### SPM 3.06 {#refSPM3-06}

It is considered that a process for obtaining a known product already
covered by a certificate may not give rise to a new certificate see
[SPM1.05](#refSPM1-05).

### SPM 3.07 {#refSPM3-07}

In order for the protection conferred by a certificate to apply in a
particular territory, the relevant authorisation must be the first to
place the product on the market in that territory (see
SPM5.05.1-5.05.2). This means that each of the territories in which an
authorisation can be granted are considered separately for the purposes
of Article 3(d). The existence of a GB authorisation does therefore not
affect the assessment of an NI authorisation against Article 3(d) in
terms of determining the first authorisation in Northern Ireland.
Equally, an NI authorisation does not affect the assessment of a GB
authorisation as the first authorsation in Great Britain. However, a UK
authorisation may have an effect in both instances.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Article 4: subject-matter of protection**
  Within the limits of the protection conferred by the basic patent, the protection conferred by a certificate shall extend only to the product covered by the UK, GB or NI authorization to place the corresponding medicinal product on the market and for any use of the product as a medicinal product that has been authorized in the United Kingdom before the expiry of the certificate.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### SPM 4.01 {#spm-401}

A certificate extends the protection conferred by the basic patent
beyond the term of that patent but only in respect of the product
covered by the authorisation to place the corresponding medicinal
product on the market in the relevant territory and any use of the
product as a medicinal product that has been authorised within the UK
before expiry of the certificate. It does not, however, extend the term
of the patent itself.

### SPM 4.01.1 {#spm-4011}

Article 4 of the Medicinal Regulation is to be interpreted in the same
way as Article 4 of the Plant Protection Regulation (in view of recital
(17) of the latter) which states:

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Article 4 \[Plant Protection Regulation\]**
  **Subject-matter of protection**
  Within the limits of the protection conferred by the basic patent, the protection conferred by a certificate shall extend only to the product covered by the authorizations to place the corresponding plant protection product on the market and for any use of the product as a plant protection product that has been authorized before expiry of the certificate.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### SPM 4.01.2 {#spm-4012}

The plural "authorizations" makes clear that one certificate will
suffice for the first and subsequent authorizations.

### SPM 4.02 {#refSPM4-02}

In [Draco AB's SPC Application \[1996\] RPC
417](http://rpc.oxfordjournals.org/content/113/14/417.full.pdf+html){rel="external"}
the Patents Court held that Article 4 was the operative Article to
confer protection on the product, which was the active ingredient as
defined in Article 1(b).

### SPM 4.03 {#refSPM4-03}

In Novartis AG v Actavis UK Limited C-442/11, questions concerning the
interpretation of Article 4 (and Article 5) of the Regulation were
referred to the Court of Justice of the European Union . The Court of
Justice provided its decision by reasoned order with reference to its
decisions in C 322/10 Medeva, C 422/10 Georgetown University and Others,
C 630/10 University of Queensland and CSL, and C 6/11 Daiichi SANKYO
(see above), confirming that SPCs provide patent-like infringement
protection for the duration of the SPC also see [SPM5.03](#refSPM5-03).

  -----------------------------------------------------------------------
   

  **Article 5: Effects of the certificate**

  1\. Subject to the provisions of Article 4 and paragraphs 1a and 1b,
  the certificate shall confer the same rights as conferred by the basic
  patent and shall be subject to the same limitations and the same
  obligations.\
  1a. The protection conferred by a certificate in accordance with
  paragraph 1 shall extend only to the territory in respect of which a
  valid, UK, GB or NI authorisation has been issued and where the
  authorisation-\
  (a) is the first authorisation for the product in the territory in
  accordance with Article 3(b) and (d), and\
  (b) has been issued before the certificate takes effect in accordance
  with Article 13(1).\
  1b. Where after the submission of an application for a certificate in
  accordance with Article 7(1) or (2) and before the certificate takes
  effect in accordance with Article 13(1), a GB or NI authorisation is
  granted in respect of the same product and the authorisation would have
  met the requirements of Article 3(b) and (d) had it been granted on the
  date of submission of the application, the protection conferred by a
  certificate in accordance with paragraph 1 shall extend to the
  territory of England and Wales and Scotland or the territory of
  Northern Ireland as the case may be.\
  2. By way of derogation from paragraph 1, the certificate referred to
  in paragraph 1 shall not confer protection against certain acts which
  would otherwise require the consent of the holder of the certificate
  ("the certificate holder"), if the following conditions are met:\
  (a) the acts comprise:\
  (i) the making of a product, or a medicinal product containing that
  product, for the purpose of export to countries outside the United
  Kingdom, the Isle of Man and the Member States of the European Union;
  or\
  (ii) any related act that is strictly necessary for the making, in the
  United Kingdom, referred to in point (i), or for the actual export; or\
  (iii) the making, no earlier than six months before the expiry of the
  certificate, of a product, or a medicinal product containing that
  product, for the purpose of storing it in the United Kingdom, in order
  to place that product, or a medicinal product containing that product,
  on the market of the United Kingdom, the Isle of Man or one or more
  Member States of the European Union after the expiry of the
  corresponding certificate; or\
  (iv) any related act that is strictly necessary for the making, in the
  United Kingdom, referred to in point (iii), or for the actual storing,
  provided that such related act is carried out no earlier than six
  months before the expiry of the certificate.\
  \
  (b) the maker, through appropriate and documented means, notifies the
  comptroller, and informs the certificate holder, of the information
  listed in paragraph 5 of this Article no later than three months before
  the start date of the making in the United Kingdom, or no later than
  three months before the first related act, prior to that making, that
  would otherwise be prohibited by the protection conferred by that
  certificate, whichever is the earlier;\
  (c) if the information listed in paragraph 5 of this Article changes,
  the maker notifies the comptroller and informs the certificate holder,
  before those changes take effect;\
  (d) in the case of products, or medicinal products containing those
  products, made for the purpose of export to countries outside the
  United Kingdom, the Isle of Man and the Member States of the European
  Union, the maker ensures that the words 'UK export' are affixed so as
  to be sufficiently clear and visible to the naked eye to the outer
  packaging of the product, or the medicinal product containing that
  product, referred to in point (a)(i) of this paragraph, and, where
  feasible, to its immediate packaging;\
  (e) the maker complies with paragraph 9 of this Article.\
  3. The exception referred to in paragraph 2 shall not apply to any act
  or activity carried out for the import of products, or medicinal
  products containing those products, into the United Kingdom merely for
  the purpose of repackaging, re-exporting or storing.\
  4. The information provided to the certificate holder for the purposes
  of points (b) and (c) of paragraph 2 shall be used exclusively for the
  purposes of verifying whether the requirements of this Regulation have
  been met and, where applicable, initiating legal proceedings for
  non-compliance.\
  5. The information to be provided by the maker for the purposes of
  point (b) of paragraph 2 shall be as follows:\
  (a) the name and address of the maker;\
  (b) an indication of whether the making is for the purpose of export,
  for the purpose of storing, or for the purpose of both export and
  storing;\
  (d) the number of the certificate; and\
  (e) for medicinal products to be exported to countries outside the
  United Kingdom, the Isle of Man and the Member States of the European
  Union, the reference number of the marketing authorisation, or the
  equivalent of such authorisation, in each country of export, as soon as
  it is publicly available.\
  6. For the purposes of notification to the comptroller under points (b)
  and (c) of paragraph 2, the maker shall use the standard prescribed
  form.\
  7. Failure to comply with the requirements of point (e) of paragraph 5
  with regard to a country outside the United Kingdom, the Isle of Man
  and the Member States of the European Union shall only affect exports
  to that country, and those exports shall, therefore, not benefit from
  the exception.\
  9. The maker shall ensure, through appropriate and documented means,
  that any person in a contractual relationship with the maker who
  performs acts falling under point (a) of paragraph 2 is fully informed
  and aware of the following:\
  (a) that those acts are subject to paragraph 2;\
  (b) that the placing on the market, import or re-import of the product,
  or the medicinal product containing that product, referred to in point
  (a)(i) of paragraph 2 or the placing on the market of the product, or
  the medicinal product containing that product, referred to in point
  (a)(iii) of paragraph 2 could infringe the certificate referred to in
  paragraph 2 where, and for as long as, that certificate applies.\
  10. Paragraph 2 shall apply to certificates that are applied for on or
  after 1 July 2019.\
  Paragraph 2 shall also apply to certificates that have been applied for
  before 1 July 2019 and that take effect on or after that date.
  Paragraph 2 shall only apply to such certificates from 2 July 2022.\
  Paragraph 2 shall not apply to certificates that take effect before 1
  July 2019.\
  11. The Secretary of State may by regulations make further provision as
  to the manner and form (including design and colour) of affixing the
  words "UK export" to the outer packaging of the product, or the
  medicinal product containing that product, referred to in paragraph
  2(a)(i) of this Article, and, where feasible, to its immediate
  packaging.\
  12. Those regulations are to be made by statutory instrument which is
  subject to annulment pursuant to a resolution of either House of
  Parliament.
  -----------------------------------------------------------------------

### SPM 5.01 {#refSPM5-01}

Article 5(1) states that subject to Article 4, and the provisions on
territorial scope found in Articles 5(1a) and (1b) (see SPM5.05), a
certificate confers the same rights as the basic patent and is subject
to the same limitations and obligations. Provisions under national law
relating to such matters as infringement therefore apply equally to a
certificate. In certain circumstances, the Office will provide a
non-binding opinion as to whether or not an SPC is infringed, see
Section 74A.

### SPM 5.02 {#spm-502}

In [Research Corp's SPC (\[1994\] RPC
667)](http://rpc.oxfordjournals.org/content/111/20/667.full.pdf+html){rel="external"},
see also [SPM0.01.2](#refSPM0-01-2), the Patents Court, upholding a
decision of the hearing officer reported at \[1994\] RPC 387, held that
the endorsement of the basic patent as licences of right under the
provisions of paragraph 4 of Schedule 1 of the 1977 Act was a limitation
within the meaning of Article 5. The comptroller therefore had
jurisdiction to entertain an application to settle the terms of licence
of right under a certificate, pursuant to s.46(3) of the Patents Act
1977 and Article 5 of the Patents (Supplementary Protection Certificate
for Medicinal Products) Regulations 1992 (which applied provisions of
the Patents Act to SPCs; now dealt with under Schedule 4A of the Patents
Act).

### SPM 5.03 {#refSPM5-03}

In Novartis AG and Actavis UK Limited, questions concerning the
interpretation of Article 5 (and Article 4) of the Regulation were
referred to the Court of Justice of the European Union (C-442/11). The
Court of Justice provided its decision by reasoned order with reference
to its decisions in C-322/10 Medeva, C-422/10 Georgetown University and
Others, C-630/10 University of Queensland and CSL, and C-6/11 Daiichi
Sankyo (see above), confirming that SPCs provide patent-like
infringement protection for the duration of the SPC also see
[SPM4.03](#refSPM4-03).

### SPM5.04 {#spm504}

To assist in determining if particular compositions or acts infringe an
SPC it is possible to request that the Office gives a non-binding
opinion on infringement, See [Section
74A](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-74a-opinions-on-matters-prescribed-in-the-rules)
or [Opinions: resolving patent
disputes](https://www.gov.uk/guidance/opinions-resolving-patent-disputes).

### Territorial scope

### SPM 5.05 {#refSPM5-05}

Articles 5(1a) and 5(1b) were introduced by the Supplementary Protection
Certificates (Amendment) (EU Exit) Regulations 2020. They establish that
a certificate will only confer protection within the territory in which
relevant marketing authorisations allow the product to be placed on the
market. These authorisations must meet the requirements of Article 3(b)
and (d), and must have been issued before the certificate takes effect.

### SPM 5.05.1 {#refSPM5-05-1}

Article 5(1b) allows for the protection provided by a certificate to
extend to additional territory if a marketing authorisation is granted
in that territory after the application for the certificate has been
filed. The Article ensures that, if the authorisation would have met the
requirements of Articles 3(b) and 3(d) if it had been granted when the
application was filed -- that is, if it is valid to place the product on
the market in that territory, and is the first such authorisation for
that territory -- the certificate will provide protection in that
territory as well. Because Article 3(b) and (d) are assessed at the date
of filing the application, such authorisations would not otherwise
fulfil the conditions of Article 5(1a).

### SPM 5.05.2 {#refSPM5-05-2}

Therefore, if a certificate is applied for, and granted, based on a GB
authorisation only, and no NI authorisation is issued and notified (see
SPM13A.02) before the basic patent expires, the certificate will only
provide protection in Great Britain when it takes effect. The same
applies vice versa for a certificate based on an NI authorisation. If a
UK authorisation is used as the basis for a certificate, or if both GB
and NI authorisations are issued and notified before the basic patent
expires, the protection will extend to the whole of the UK.

### SPM 5.05.3 {#refSPM5-05-3}

It is not possible to extend the territorial scope (by notification of
additional authorisations) once a certificate takes effect (see
SPM13A.02). If an authorisation is withdrawn, the effect on the
territorial scope of the certificate will depend on the protection
conferred by the certificate, as per the provisions on lapse (see
SPM14.01.3-14.01.5).

### SPM 5.05.4 {#refSPM5-05-4}

The territorial scope of a paediatric extension is determined by Article
13(5) and regulation 58A(4A) and (4B) of the Human Medicines Regulations
2012; the protection conferred by the extension cannot exceed that of
the certificate, and can only extend to territory where the conditions
for the extension have been met (but see SPM13B.04 where the territory
of both may be extended at the same time).

### Manufacturing waiver

### SPM 5.06 {#refSPM5-06}

Articles 5(2)-(10) were introduced by Regulation (EU) 2019/933 and
establish a "manufacturing waiver" for SPCs for medicinal products; the
recitals of Regulation (EU) 2019/933 provide context for the
interpretation of these provisions. The waiver confers an additional
exception to the rights provided under Art. 5(1) and allows
manufacturers of generic or biosimilar versions of SPC-protected
medicines to carry out specified acts that would otherwise require the
permission of the SPC holder.

### SPM 5.07 {#refSPM5-07}

The waiver permits the making of SPC-protected medicines for export to
countries outside the UK or the EU, as set out in Article 5(2)(a)(i).
The recitals of Regulation (EU) 2019/933 make it clear that this is
intended to allow manufacturing for export to countries where protection
does not exist or has expired; recital 18 states that it is the
responsibility of the maker to verify that protection does not exist or
has expired in a country of export.

### SPM 5.08 {#refSPM5-08}

The waiver also permits, under Article 5(2)(a)(iii), the making of
SPC-protected medicines, in the last six months of the SPC term, for
storing in the UK, ready for sale in the UK or the EU when SPC
protection expires (sometimes referred to as "stockpiling").

### SPM 5.09 {#refSPM5-09}

In addition, the waiver permits any "related act" that is strictly
necessary for the making, export or storing of the medicine under either
category and would otherwise require the permission of the SPC holder to
be done in the UK. Any related acts strictly necessary for making for
the purpose of stockpiling, or for the actual stockpiling, can only take
place in the final six months of the SPC term. Recital 9 of Regulation
2019/933 provides examples of "related acts"; including possessing,
offering to supply, supplying, importing, using or synthesising an
active ingredient for the purpose of making a medicinal product, or the
temporary storage or advertising for the exclusive purpose of export.

### SPM 5.10 {#refSPM5-10}

Article 5(3) makes clear that the acts permitted by the waiver do not
include the importation of active ingredients or medicinal products into
the UK merely for the purpose of repackaging, re-exporting or storing.
Recital 11 of Regulation (EU) 2019/933 also makes it clear that the
waiver does not permit sale of medicines made under these provisions in
the country where an SPC is in force, and nor does it allow
re-importation of medicines made for export into the country where an
SPC is in force. The waiver does not permit storage of active
ingredients or medicinal products for any reasons other than those set
out in Art. 5(2)(a). The maker is obliged to ensure, through appropriate
and documented means, that anyone performing any of the permitted acts
under the waiver under a contractual relationship with the maker is
aware of that these acts are subject to the provisions of Art. 5(2) and
that sale, import or re-import of products or medicinal products in
breach of those provisions could infringe the relevant SPC for as long
as it is in effect (Art. 5(9)).

### SPM 5.11 {#refSPM5-11}

Articles 5(2)(b) and (c), along with Articles 5(4) to 5(7), set out the
notification requirements that the maker must comply with to make use of
the waiver. At least three months before any making or related acts, the
maker must provide certain information to both the SPC holder and the
Office. As set out in Art. 5(5), this information is the name and
address of the maker; an indication of whether the manufacture is for
export, stockpiling or both; and the relevant SPC number. Finally, for
medicinal products produced for export, the maker must provide the
reference number of the marketing authorisation, or its equivalent, in
each country of export, as soon as it is publicly available. Failure to
provide this last item in respect of any country of export will only
invalidate the waiver in respect of exports to that country, as provided
by Article. 5(7). The maker must inform both the Office and the SPC
holder of any changes to this information in accordance with Article
5(2)(c), whilst Article 5(4) makes clear the SPC holder must only use
this information for the purposes of verifying whether the requirements
of the waiver have been met and, where applicable, initiating legal
proceedings for non-compliance. Recital 15 of Regulation (EU) 2019/933
states that the information should not include confidential or
commercially sensitive information and, as discussed in SPM11.04, the
Office will publish the information as soon as possible after receiving
it.

### SPM 5.12 {#refSPM5-12}

The maker must notify the Office of this information using Patents Form
SP5; this standard form is to be used for both the initial notification
and for notifying any changes to the information. The same form may be
used for notifying the SPC holder but this is not compulsory.

### SPM 5.13 {#refSPM5-13}

Products and medicinal products made for the purpose of export under the
waiver must carry the words "UK export" as required by Art. 5(2)(d) in
both the amended and transitional forms of the Medicinal Regulation (see
SP0.13.1). The words must be sufficiently clear and visible to the naked
eye, and must be affixed to the outer packaging and, where feasible, the
immediate packaging of the medicine.

### SPM 5.14 {#refSPM5-14}

Article 5(10) provides for transitional arrangements. Under these, the
manufacturing waiver initially applies only to SPCs applied for on or
after 1 July 2019; the date on which Regulation (EU) 2019/933 came into
force. From 2 July 2022, the waiver also applies to SPCs applied for
before 1 July 2019, but only if the basic patent had not expired before
that date and so the SPC had not come into effect. The waiver does not
apply to SPCs that were already in effect before 1 July 2019.

  -----------------------------------------------------------------------------------------------
   
  **Article 6: Effects of the certificate**
  The certificate shall be granted to the holder of the basic patent or his successor in title.
  -----------------------------------------------------------------------------------------------

### SPM 6.01 {#spm-601}

Article 6 does not prevent an application from being lodged by a person
other than the proprietor of the basic patent. However, irrespective of
who lodged the application, the certificate can be granted only to the
person registered at the time of grant as the proprietor of the basic
patent.

### SPM 6.02 {#refSPM6-02}

The question of whether, having regard to the wording of Article 6, the
holder of a marketing authorisation may refuse to give a copy to the
holder of the basic patent or their successor in title where it is
required by Article 8(1)(b) in order to complete the application was
referred to the European Court of Justice by the [Tribunal de Commerce,
Nivelles, Belgium in Biogen Inc v SmithKline Beecham Biologicals SA
\[1997\] RPC
833](http://rpc.oxfordjournals.org/content/114/23/833.full.pdf+html){rel="external"}.
The Court ruled that the Regulation does not require the holder of the
marketing authorisation to provide a copy to the patent holder. Such an
obligation may, however, be deemed to be inherent in the contractual
relationship between the parties. ([See also SPM8.04.1](#refSPM8-04-1).)

### SPM 6.03 {#spm-603}

In Eli Lilly & Company v Human Genome Sciences Inc \[2012\] EWHC 2290
(Pat) Warren J held "that the holder of a basic patent can make an
application for an SPC in reliance on an MA granted to a third party
having no connection of any sort with that holder.". However, in Eli
Lilly and Company v Genentech Inc \[2019\] EWHC 388 (Pat), Arnold J
concluded that the issue was not acte clair and referred a question to
the CJEU to determine if the Regulation precludes the grant of an SPC to
the proprietor of a basic patent in respect of a product which is the
subject of a marketing authorisation held by a third party without that
party's consent. However, the CJEU declined to address the matter
([C-239/19](https://eur-lex.europa.eu/legal-content/en/TXT/?uri=CELEX:62019CO0239){rel="external"}),
considering that the reference was inadmissable as a hypothetical
question (in part because the Patents Court had found the basic patent
invalid).

  -----------------------------------------------------------------------
   

  **Article 7: Application for a certificate or an extension of a
  Certificate**

  1\. The application for a certificate shall be lodged within six months
  of the date on which the UK, GB or NI authorisation referred to in
  Article 3(b) and (d) to place the product on the market as a medicinal
  product was granted. Where more than one such authorisation is granted
  before the application for a certificate is lodged, the application
  shall be lodged within six months of the date of grant of the earliest
  of such authorisations.\
  2. Notwithstanding paragraph 1, where the authorization to place the
  product on the market is granted before the basic patent is granted,
  the application for a certificate shall be lodged within six months of
  the date on which the patent is granted.\
  3. The application for an extension of the duration may be made when
  lodging the application for a certificate or when the application for
  the certificate is pending and the appropriate requirements of Article
  8(1)(d) or Article 8(2), respectively, are fulfilled.\
  4. The application for an extension of the duration of a certificate
  already granted shall be lodged not later than two years before the
  expiry of the certificate.\
  5. Notwithstanding paragraph 4, for five years following the entry into
  force of Regulation (EC) No 1901/2006, the application for an extension
  of the duration of a certificate already granted shall be lodged not
  later than six months before the expiry of the certificate.
  -----------------------------------------------------------------------

### SPM 7.01 {#refSPM7-01}

The application must be lodged within six months of the date of grant of
either the first authorisation that applies in any part of the UK or the
basic patent, whichever is later. In the case of the former, this can be
a UK, GB, or NI authorisation, and so would include authorisations
granted by the European Medicines Agency. As Article 7(1) makes clear,
if more than one authorisation covering different parts of the UK has
been granted when filing the application, it must be filed within six
months of the earliest of those authorisations (see also SPM8.06.1,
8.06.2).

### SPM 7.01.1 {#refSPM7-01-1}

In Abbott Laboratories' SPC Application \[2004\] RPC 20, the Hearing
Officer held that the relevant date in Article 7(1) is the actual date
of grant of the authorisation and not the date of publication of grant
in the relevant Official Gazette. It was also held that the six-month
deadline set out in Article 7 is extendable under r.110(1) of the
Patents Rules 1995 (now rule 108(1) of the 2007 Rules), in accordance
with the provisions of Article 18 of the original Regulation (now
Article 19 of the Medicinal Regulation) (see SPM19.11). In respect of
Article 7(2); in accordance with Article 97(4) of the European Patent
Convention, the date of grant of a European Patent is the date the
European Patent Bulletin mentions grant. For a UK patent, the relevant
date of grant is taken to be the date of publication of the notice of
grant in the Patents Journal under Section 24(1) of the Patents Act
(rather than the date of grant under Section 18(4)).

### SPM 7.02 {#refSPM7-02}

An application for an extension can be lodged when an application for a
certificate is filed or whilst the application for a certificate is
pending or it may be filed after a certificate has been granted. When a
certificate is already granted the application shall be lodged not later
than two years before the expiry of the certificate.

### SPM 7.03 {#spm-703}

The time periods expressed in Article 7 are to be determined in
accordance with [Regulation (EEC, Euratom) No 1182/71 of the Council of
3 June
1971](https://www.legislation.gov.uk/eur/1971/1182){rel="external"}
determining the rules applicable to periods, dates and time limits, as
incorporated into domestic law under the Withdrawal Act.

  -----------------------------------------------------------------------
   

  **Article 8: Content of the application for a certificate**

  1\. The application for a certificate shall contain:\
  (a) a request for the grant of a certificate, stating in particular:\
  (i) the name and address of the applicant;\
  (ii) if the applicant has appointed a representative, the name and
  address of the representative;\
  (iii) the number of the basic patent and the title of the invention;\
  (iv) the number and date of the UK, GB or NI authorisation, or where
  there is more than one such authorisation, of each authorisation as
  referred to in Article 3(b) and (d);\
  (v) the number and date of the earliest of any EEA authorisation, the
  granting of which predates the granting of the UK, GB or NI
  authorisation as referred to in Article 3(b) and (d);\
  (b) a copy of the UK, GB or NI authorisation or, where there is more
  than one such authorisation, of each authorisation to place the product
  on the market, as referred to in Article 3(b) and (d), in which the
  product is identified, containing in particular the number and date of
  the authorization and the summary of the product characteristics listed
  in Article 11 of Directive 2001/83/EC, Article 14 of Directive
  2001/82/EC, Part 2 to Schedule 8 of the Human Medicines Regulations
  2012 or Part 1 of Schedule 1 to the Veterinary Medicines Regulations
  2013;\
  (c) where the product is the subject of one or more EEA authorisations
  granted prior to the UK, GB or NI authorisation referred to in Article
  3(b) and (d), the applicant must provide in relation to the earliest of
  any such EEA authorisations-\
  (i) information regarding the identity of the product thus authorised;\
  (ii) information regarding the legal provision under which the
  authorisation procedure took place; and\
  (iii) a copy of the notice publishing the authorisation in the
  appropriate official publication;\
  (d) where the application for a certificate includes a request for an
  extension of the duration\
  (i) a copy of the statement indicating compliance with an agreed
  completed paediatric investigation plan as referred to in regulation
  58A(2)(a) of the Human Medicines Regulations 2012;\
  (ii) details of the territory in respect of which the statement
  referred to in sub-paragraph (i) has been made.\
  2. Where an application for a certificate is pending, an application
  for an extended duration in accordance with Article 7(3) shall include
  the particulars referred to in paragraph 1(d) of this Article and a
  reference to the application for a certificate already filed.\
  3. The application for an extension of the duration of a certificate
  already granted shall contain the particulars referred to in paragraph
  1(d) and a copy of the certificate already granted.
  -----------------------------------------------------------------------

### SPM 8.01 {#refSPM8-01}

ArtM.8, ArtP.8, r.116(1) FSch 1 is also relevant

An application for a certificate must contain a request for the grant of
a certificate on Form SP1 but see also [SPM19.04](#refSPM19-04)
accompanied by the prescribed application fee (currently £250).

### SPM 8.02 {#refSPM8-02}

This request should specify:

the name and address of the applicant (Section 3 of Form SP1);

the name of the applicant's agent (if any) and the address for service
in the United Kingdom (including the Isle of Man), Gibraltar or the
Channel Islands (Section 4);

the EC Regulation (469/2009 or 1610/96) under which the application is
made (Section 5);

the product in respect of which the certificate is sought (ie the active
ingredient or combination of active ingredients of the medicinal
product) (Section 6);

the number, title, expiry date and (if later than the first UK, GB or NI
authorisation) the date of grant of the basic patent (Section 7);

the number and date of the first UK, GB or NI authorisation (Section
8a);

the number and date of any other UK, GB or NI authorisation which has
been granted ahead of the application being filed (Section 8b)

the State, number and date of the first authorisation in the EEA (where
granted before the date of the first UK, GB or NI authorisation), plus
the identity of the authorised product and the legal provision under
which the authorisation took place (Section 9). The definition of EEA
authorisation in Article 1 requires such a first authorisation to have
been granted in accordance with Directive 2001/83/EC or Directive
2001/82/EC. In addition, in Synthon BV v Merz Pharma Gmbh & Co KG
(C-195/09) and Generics (UK) Ltd v Synaptech Inc (C-427/09) the Court of
Justice of the European Union ruled in relation to Article 2 that a
product which was placed on the market in the European Community as a
medicinal product for human use before obtaining a marketing
authorisation in accordance with Council Directive 65/65/EEC (now
Directive 2001/83/EC) and, in particular, without undergoing safety and
efficacy testing, is not within the scope of the Regulation, and may not
be the subject of a supplementary protection certificate (see also
[SPM2.01](#refSPM2-01), [SPM13.04.2](#refSPM13-04-2)) and Novartis AG
and University College London & Novartis AG and Institute of
Microbiology and Epidemiology SPC Applications [BL
O/044/03](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/044/03){rel="external"}.
In AstraZeneca AB C-617/12 the CJEU confirmed by reasoned order
referring to Novartis and others C-207/03 and C-252/03 that a Swiss
authorisation which has been automatically recognized in Liechtenstein
constituted the first authorisation in the EEA even if that
authorisation was suspended at a later date see also [BL
O/146/12](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/146/12){rel="external"}
and [SP0.08-09](#refSP0-08) and [SPM13.04](#refSPM13-04).

### SPM 8.03 {#spm-803}

Where more than one authorisation for the product was granted on the
date of the first UK, GB, or NI authorisation, or if more than one
authorisation in the EEA shares the same date, details of all of the
relevant authorisations should be given at Sections 8a, 8b and 9 of Form
SP1.

### SPM 8.03.1 {#refSPM8-03-1}

The relevant date having regard to Article 8(1)(a)(iv) or 8(1)(b) will
be the date of grant of the authorisation, unless evidence is provided
which shows a different date of legal effect ([see also
SPM13.05.1](#refSPM13-05-1)). Following decision [BL
O/418/13](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/418/13){rel="external"}
(Genzyme Corporation), where the earliest authorisation is one granted
by a decision of the European Commission following a favourable opinion
from the EMA under Regulation (EC) 726/2004, the date of the
authorisation will be taken to be the date of notification. This date
may be evidenced such as by providing a suitable excerpt from the OJEU,
see [SPM13-05-1](SPM13-05-1), [10.15](#refSPM10-15)). The relevance of
the "date of notification" in these circumstances has been confirmed by
the CJEU in Seattle Genetics Inc. v Österreichisches Patentamt C-471/14.
This remains applicable for centralised authorisations granted by the
EMA (in respect of Northern Ireland) and for GB authorisations converted
from such authorisations (see [SPM1.07.2](#refSPM1-07-2)).

### SPM 8.03.2 {#refSPM8-03-2}

If the application for a certificate is based on a marketing
authorisation granted before 1 January 2021, the applicant must provide
the details of that authorisation. The marketing authorisation will be
treated as though it is a UK, GB or NI authorisation as applicable, and
the application examined in line with the amended legislation.
Transitional provisions in regulation 7 of the Supplementary Protection
Certificates (Amendment) (EU Exit) Regulations 2020 provide the
necessary support for these types of authorisation. However, if the
pre-2021 marketing authorisation has been changed as a result of the
Northern Ireland Protocol, the application must contain details of each
authorisation that relates to the same product (whether GB, NI or UK).
Therefore, if a pre-2021 UK authorisation has become a combination of GB
and NI authorisations, the application for a certificate must include
details of both authorisations, so that it properly reflects the
regulatory basis for the SPC.

### SPM 8.04 {#refSPM8-04}

HMR sch 8

The request should be accompanied by a copy of the or each UK, GB or NI
authorisation. This authorisation should identify the product and
contain the number and date of the authorisation and a summary of the
product characteristics listed in Directive 2001/83/EC or the Human
Medicines Regulations 2012 (for pharmaceutical products), and Directive
2001/82/EC or the Veterinary Medicines Regulations 2013 (for veterinary
products) (see SPM2.01). Thus, in the case of a pharmaceutical product,
it is necessary to file a copy of the marketing authorisation granted by
the Medicines and Healthcare products Regulatory Agency, the Veterinary
Medicines Directorate or the European Commission following a favourable
opinion from the EMA (see SPM2.01). The copy should include any
enclosure or Schedule referred to in the document of grant, such as an
attached authenticated copy of the licence application setting out the
particulars of the product.

### SPM 8.04.1 {#refSPM8-04-1}

The question of the applicant's obligation to provide a copy of the
authorisation was referred to the European Court of Justice by the
Tribunal de Commerce, Nivelles, Belgium in Biogen Inc v SmithKline
Beecham Biologicals SA \[1997\] RPC 833. The Court ruled that, where the
owner of the basic patent and the holder of the marketing authorisation
were different persons and the patent owner was unable to provide a copy
of the authorisation in accordance with Article 8(1)(b) of the
Regulation, the application for the certificate could not be refused on
that ground alone. It was open to the national authority granting the
certificate to obtain a copy of the marketing authorisation from the
national authority which issued it. In the light of the Biogen ruling,
the Office will proceed on the basis that, whilst it cannot reject the
application merely because the copy of the authorisation is provided by
someone other than the applicant, equally it cannot waive at least the
minimum requirements of Art 8(1)(b). Thus it is not sufficient for the
applicant merely to ask the Office to obtain a copy of the authorisation
without first having established their own inability to do so. Also, the
Office will not make good the lack of a copy by referring to or copying
authorisation documents held on other files, such as an SPC application
filed by another patent holder. Accordingly, where an applicant is
unable to obtain a copy of the authorisation from the person holding it,
the Office will first require the applicant to provide evidence of this
and also to provide such information as is available from the authority
issuing the authorisation such information (eg a gazette notice, a
letter or a database printout) as will enable the Office to verify the
identity of the product and the date of the authorisation stated on Form
SP1. The Office will then ask the issuing authority to supply a copy of
the relevant (usually confidential) summary of product characteristics
listed in the relevant regulatory legislation ([see
SPM2.01](#refSPM2-01)). It is important to note that this latter
document may be covered by a request for confidentiality under Rule
53(1) of the Patents Rules 2007 from the authorisation authority and is
then solely for Office use and under no circumstances will be made
available to the applicant or the public. ([See also SPM
6.02](#refSPM6-02)).

\[ Copies of confidential authorisations obtained from the authorisation
authorities invoking the Biogen ruling are filed in a separate envelope
marked "Not open to the Applicant or the Public" \]

### SPM 8.05 {#refSPM8-05}

r.113 is also relevant

In addition, in order to meet the requirements of Article 8(1)(c), where
there is an authorisation to place the product on the market in the EEA
which predates the earliest UK, GB or NI authorisation relied upon in
the application, the application should be accompanied by a copy of the
notice publishing the (or each) earliest such EEA authorisation in the
appropriate official gazette. However, Article 8(1)(c) of the Medicinal
Regulation is to be interpreted in the same manner as Article 8(1)(c) of
the Plant Protection Regulation which reads:

  -----------------------------------------------------------------------
   

  **Article 8 \[Plant Protection Regulation\]**

  Content of the application for a certificate

  1\. (c) where the product is the subject of one or more EEA
  authorizations granted prior to the GB or NI authorization referred to
  in Article 3(1)(b) and (d), the applicant must provide in relation to
  the earliest of any such EEA authorizations-\
  (i) information regarding the identity of the product thus authorised;\
  (ii) information regarding the legal provision under which the
  authorization procedure took place; and\
  (iii) a copy of the notice publishing the authorization in the
  appropriate official publication or, failing such a notice, any other
  document proving that the authorization has been issued, the date on
  which it was issued and the identity of the product authorized.

   
  -----------------------------------------------------------------------

### SPM 8.05.1 {#spm-8051}

Therefore, if no such publication in a gazette has therefore been made,
the copy of the authorisation itself or any other document proving that
the authorisation has been issued, such as a confirmatory letter from
the authorisation authority, should be furnished in lieu. Any document
not in the English language should be accompanied by a translation which
need only be verified if there is reason to doubt the accuracy of the
translation.

### SPM 8.06 {#refSPM8-06}

Except where it is immediately apparent that the product in question is
protected by the basic patent, the applicant should also provide
whatever information is necessary to enable the Comptroller to confirm
that this is so; for example by specifying a claim of the basic patent
which refers to the product or indicating how the product is derived
from a general formula in a claim.

### SPM 8.06.1 {#refSPM8-06-1}

If an authorisation that covers additional territory is granted after
the original application is submitted, Form SP6 should be used to submit
details of the relevant authorisation (see [SPM13A.02](#refSPM13A-02)),
rather than submitting a new Form SP1 or seeking a correction to the
original form.

### For an extension of a certificate

### SPM 8.07 {#refSPM8-07}

The application for an extension must be made on Form SP4 and
accompanied by the prescribed application fee (currently £200).

### SPM 8.08 {#refSPM8-08}

This request should specify:

a granted certificate number or certificate application number if these
exist (Section 2 of Form SP4);

the name and address of the applicant (Section 3);

the name of the applicant applicant's agent (if any) and the address for
service in the United Kingdom (including the Isle of Man), Gibraltar or
the Channel Islands (Section 4);

the product in respect of which the certificate is sought (i.e. the
active ingredient or active substance, or combination thereof, of the
medicinal product) (Section 5);

the number, title and expiry date of the basic patent (Section 6);

the number and date of the authorisation(s) containing the statement of
compliance with an agreed paediatric investigation plan, including the
state if necessary (Sections 7a and 7b);

the territory in which the statement of compliance applies

### SPM 8.09 {#refSPM8-09}

ArtsPd 36(1),(2), ArtPd 28(3) is also relevant

Eligibility for a paediatric extension is governed by regulation 58A of
the Human Medicines Regulations 2012. The conditions under which an
extension can be granted are broadly similar to those in Article 36 of
Regulation 1901/2006 which applied until 31 December 2020. The request
should be accompanied by a copy of the statement indicating compliance
with an agreed paediatric investigation plan. This may be provided by
MHRA in respect of GB and UK authorisations, as well as NI
authorisations granted under Directives 2001/82/EC or 2001/83/EC, or by
the European Medicines Agency for NI authorisations granted under the
centralised procedure of Regulation 726/2004. Regulation 58A(3) states
that either indication qualifies for the paediatric extension in the
relevant territory. Since the other provisions of Regulation 1901/2006
other than Article 36 continue to apply in Northern Ireland, as
indicated in regulation 58A, case law relating to the broader EU
paediatric regime remains relevant for determining whether the
conditions are met.

### SPM 8.09.1 {#refSPM8-09-1}

In Merck & Co., Inc. (BL O/035/09) the hearing officer considered
whether an opinion of the Paediatric Committee of the EMEA indicating
compliance with a PIP was sufficient to meet this requirement. He found
that it was not and that the statement of compliance included in the
marketing authorisation of the medicinal product was the necessary copy
of this statement. The hearing officer considered that the applicant
could rectify this deficiency by filing this document (see SPM19.11). In
E I Du Pont Nemours & Co. v UK Intellectual Property Office \[2009\]
EWCA Civ 966, \[2010\] RPC 6, Jacob LJ determined that an application
which did not contain a copy of the statement of compliance, and
therefore failed to comply with the provisions of Article 8(1)(d) at the
time of submission, was not incurably defective. He concluded that the
failure to submit the document was an irregularity which may be cured
after the date of application under Article 10(3). In Dr Reddy's
Laboratories (UK) Ltd and Dr Reddy's Laboratories Ltd v Warner-Lambert
Company LLC \[2012\] EWHC 3715 (Pat), the meaning of "significant
studies contained in an agreed Paediatric Investigation Plan are
completed after the entry into force of this Regulation" in Article
45(3) of Regulation (EC) No 1901/2006 was considered. It was determined
that studies outside the strict power of the PDCO to demand, but
nonetheless included in the PIP, do not render the PIP unlawful or
invalid, and that the requirement for studies to be "significant" in
Article 45(3) of Regulation (EC) No 1901/2006 was not of general
application but only referred to studies completed before entry into
force of that Regulation. In Otsuka Pharmaceuticals Company Limited. (BL
O/098/15), the hearing officer found the application did not comply with
article 8(1)(d) as it lacked a compliance statement resulting from the
incomplete agreed PIP. In reference to obiter dicta in the Court of
Appeal's judgment in E I Du Pont Nemours & Co., the hearing officer
determined this was an irregularity that could only be corrected before
expiry of the certificate.

### SPM 8.09.2 {#refSPM8-09-2}

Authorisations granted under the centralised procedure of Regulation
726/2004 and converted into a GB authorisation ([see
SPM1.07.2](#refSPM1-07-2)) are not taken to contain a compliance
statement compatible with regulation 58A upon conversion, even if such a
statement was made on the corresponding centralised authorisation before
31 December 2020. Regulation 58A requires such a statement to be made by
"the licensing authority"; i.e, the MHRA. Under paragraph 41(1) of
Schedule 33A to the Human Medicines Regulations 2012/1916 (Transitional
provision in relation to EU Exit), an agreed EU PIP is taken to be
accepted by the MHRA and applied to the converted authorisation;
however, the provision does not include adoption of the compliance
statement itself. Therefore, in order for a paediatric extension to be
granted in respect of the GB jurisdiction relying on a converted EU
marketing authorisation, it will be necessary for the applicant to
demonstrate that a compliance statement has been made on the GB
authorisation by the MHRA following conversion based on an agreed
completed EU PIP.

### SPM 8.10 {#refSPM8-10}

ArtPd 36(3) is also relevant

Where the request for an extension was filed before 1 January 2021, it
should also be accompanied by proof that it has authorisations to place
the product on the market in the United Kingdom and all EEA Member
States as referred to in Art 36(3) of Regulation (EC) No 1901/2006. This
requirement continues to apply to these requests as a result of
transitional provisions found in regulation 69 of the Patents
(Amendment) (EU Exit) Regulations 2019 (SI 2019/801). The Court of
Appeal in E I Du Pont Nemours & Co. (\[2009\] EWCA Civ 966, \[2010\] RPC
6) found that a failure to provide this proof also constituted a
deficiency which can be rectified by the applicant under Article 10(3)
after the date of application for the extension.

### SPM8.10.1 {#refSPM8-10-1}

In Chiesi Farmaceutici S.P.A ([BL
O/019/22](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O01922){rel="external"}),
the applicant had provided the necessary evidence of an updated
marketing authorisation in the UK. However, as the request was filed
before 31 December 2020, they were also required to demonstrate that
they possessed authorisations to place the product on the market in all
EU member states in order to qualify for the extension. The applicant
still lacked authorisations in two member states at the date the SPC was
due to expire and they were only able to cure this defect after the SPC
had nominally expired. The Hearing Officer determined that the
Comptroller had discretion to allow the applicant to rectify the
irregularity until the end of the term of the paediatric extension
applied for; the request was allowed on that basis.

### SPM 8.11 {#refSPM8-11}

When the certificate has been granted the request should not only should
state its number on Form SP4 but must also be accompanied by a copy of
the granted certificate.

### SPM 8.12 {#spm-812}

\[Deleted\]

### SPM 8.12.1 {#refSPM8-12-1}

If the certificate relies on separate authorisations for different
territories, a single extension request may be filed based on both
authorisations, or separate requests based on each one (see
[SPM13B.01-13B.03](#refSPM13B-01)). All such requests must be filed
before the deadline set by Article 7 (see [SPM7.02](#refSPM7-02)).

### SPM 8.13 {#refSPM8-13}

Reg 58A(5) HMR and Reg 58A(4) HMR is also relevant

Regulation 58A of the Human Medicines Regulations 2012 sets out two
circumstances in which an extension to the term of an SPC should not be
granted. These circumstances are where the applicant has already
received either one of the following alternative rewards:\
a. If an orphan marketing authorisation has been granted for the
medicinal product in accordance with regulation 58C of the Human
Medicines Regulations 2012, qualifying it for an additional two years of
marketing protection under regulation 58D(5); or\
b. If the applicant applies for, and obtains, a one-year extension to
the period of marketing protection for the medicinal product, on the
grounds that the new paediatric indication brings a significant clinical
benefit in comparison with existing therapies, in accordance with
regulation 51A(12) of the Human Medicines Regulations 2012.\
\

In [Chugai Seiyaku Kabushiki Gaisha & Tadamitsu Kishimoto (BL
O/321/20)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/321/20){rel="external"},
a granted paediatric extension for an SPC was revoked on the grounds
that the SPC holder had already received a one-year extension to the
period of marketing protection for the medicinal product in accordance
with Art. 14(11) of Regulation 726/2004 (now regulation 51A(12) of the
Human Medicines Regulations 2012).

### SPM 8.14 {#spm-814}

\[Deleted\]

### SPM 8.15 {#spm-815}

\[Deleted\]

### SPM 8.16 {#spm-816}

\[Deleted\]

### SPM 8.17 {#refSPM8-17}

If the requirements for the extension are met for part of the territory
covered by the SPC, the extension will only have effect in that part
(see [SPM13.08](#refSPM13-08)). If the requirements are met in another
territory later, a separate application for the paediatric extension to
apply in that territory may be made by filing another Form SP4 (see
[SPM13B.02](#refSPM13B-02)). Again, such requests must be filed before
the deadline set by Article 7.

### SPM 8.18 {#refSPM8-18}

If the extension request is filed before the SPC takes effect, and the
authorisation(s) also allow the protection provided by the SPC to cover
additional territory, both the SPC and the paediatric extension can
apply in the additional territory (provided that the requirements of
Article 13A and 13B are met).

### SPM 8.19 {#refSPM8-19}

Reg 58A(4A), (4B) HMR is also relevant.

If the extension request is filed while the SPC is in force, the
paediatric extension will only apply in a territory where the SPC
already provides protection, even if the authorisation has effect in
additional territory, as established by Article 13(5) and paragraphs
(4A) and (4B) of regulation 58A of the Human Medicines Regulations 2012.
As the paediatric extension only extends the duration of the SPC, as
established in Genentech (see SPM13.07); it does not allow the
protection provided by the extension to cover additional territory not
covered by the SPC.

  -----------------------------------------------------------------------
   

  **Article 9: Lodging of an application for a certificate**

  1\. An application for a certificate (or an extension of the duration
  of a certificate) shall be lodged with the comptroller.\
  2. Notification of the application for a certificate shall be published
  by the comptroller. The notification shall contain at least the
  following information:\
  (a) the name and address of the applicant;\
  (b) the number of the basic patent\
  (c) the title of the invention;\
  (d) the number and date of the UK, GB or NI authorisation or, where
  there is more than one such authorisation, each authorisation provided
  under Article 8(1)(b), the product identified in the authorisation or
  each authorisation and the territory in respect of which the
  authorisation has been granted or has effect as if granted;\
  (e) where there are authorisations granted in the EEA before any UK, GB
  or NI authorisation provided under Article 8(1)(b), the number and date
  of the earliest EEA authorisation;\
  (f) where applicable, an indication that the application includes an
  application for an extension of the duration.\
  (g) where an indication is given in accordance with sub-paragraph (f),
  details of the territory in respect of which an extension has been
  applied for.\
  3. Paragraph 2 shall apply to the notification of the application for
  an extension of the duration of a certificate already granted or where
  an application for a certificate is pending. The notification shall
  additionally contain an indication of the application for an extended
  duration of the certificate.
  -----------------------------------------------------------------------

### SPM 9.01 {#spm-901}

The application for a certificate or an extension of a certificate must
be lodged with the Office, irrespective of whether the basic patent is a
GB patent or a European Patent (UK).

\[ All new applications for certificates are referred to an examiner.
Upon receipt of an application a filing receipt is issued. \]

### SPM 9.02 {#refSPM9-02}

r.44(7) is also relevant

The information prescribed by Article 9(2) concerning an application for
a certificate is taken from Form SP1, together with the generic name of
the product when this appears in the market authorisation document but
not on Form SP1, and is published in the Patents Journal along with the
date of lodging the application. The application number (see
[SPM9.03](#refSPM9-03)), the product in respect of which protection is
sought (from Form SP1) and the date of lodging the application are also
entered in the register under the entry for the basic patent. However,
no separate publication of the application corresponding to the 'A'
publication of a patent application under the Patents Act 1977 is made.

### SPM 9.02.1 {#spm-9021}

Article 9(3) requires that the information prescribed by Article 9(2) is
also published in the Patents Journal when an application for an
extension is filed. Any additional information not found on Form SP1
that is necessary for the publication will be taken from Form SP4.

\[An examiner arranges the publication in the Journal and the entry in
the register.\]

### SPM9.03 {#refSPM9-03}

Applications are numbered in a yearly sequence, eg SPC/GB93/001. The
granted certificate retains this number, ([see SPM10.19](#refSPM10-19)).
SPC applications filed after 1 January 2021 start from SPC/GB21/009,
rather than 001.

### SPM 9.03.1 {#spm-9031}

Applications for extensions will also use the number of the application
for a certificate or the granted certificate it will extend as
appropriate ([see SPM10.19.1](#refSPM10-19-1)).

  -----------------------------------------------------------------------
   

  **Article 10: Grant of the certificate or rejection of the application
  for a certificate**

  1\. Where the application for a certificate and the product to which it
  relates meet the conditions laid down in this Regulation, the
  comptroller shall grant the certificate.\
  2. The comptroller shall, subject to paragraph 3, reject the
  application for a certificate if the application or the product to
  which it relates does not meet the conditions laid down in this
  Regulation or any prescribed fee is not paid.\
  3. Where the application for a certificate does not meet the conditions
  laid down in Article 8 or the prescribed fee relating to the
  application has not been paid, the comptroller shall ask the applicant
  to rectify the irregularity, or to settle the fee, within a stated
  time.\
  4. If the irregularity is not rectified or the fee is not settled under
  paragraph 3 within the stated time, the comptroller shall reject the
  application.\
  6. Paragraphs 1 to 4 shall apply mutatis mutandis to the application
  for an extension of the duration.
  -----------------------------------------------------------------------

### SPM 10.01 {#spm-1001}

ArtsM 8, 10(3), ArtsP8, 10(3) is also relevant

An initial examination for formal matters is carried out to determine
whether the application is in the required form, (including the
requirements of rule 14 of and Schedule 2 to the Patents Rules 2007 as
to size and presentation of documents), and accompanied by the
prescribed fee ([see SPM8.01](#refSPM8-01)); was lodged within the
period prescribed by Article 7 ([see SPM7.01](#refSPM7-01)); contains
the information prescribed by Article 8(1)(a) ([see
SPM8.02-03.2](#refSPM8-02)); is accompanied by a copy of the (or each)
UK, GB or NI authorisation ([see SPM8.04-04.1](#refSPM8-04); contains,
where appropriate, information regarding an earlier authorisation in the
EEA and a copy of the relevant notice ([see SPM8.05](#refSPM8-05)); and
whether the basic patent was in force and a marketing authorisation was
granted by the date that the SPC application was lodged, it being
confirmed by the CJEU in Merck Sharp & Dohme Corporation v
Comptroller-General of Patents, Designs and Trade Marks (C-567/16) that
the latter requirement is not "an irregularity" that can be cured having
regard to Article 10(3).

\[ This examination is carried out by an Examiner Assistant (EA) from
the EA team in the Business Operations Division (BOD).\]

### SPM 10.02-03 {#spm-1002-03}

\[Deleted\]

### Substantive examination of an application for a certificate

### SPM 10.04 {#refSPM10-04}

ArtM 3(a), (ArtP3(1)(a)), ArtM 3(b) (ArtP3(1)(b)), ArtM 3(c),
(ArtP3(1)(c))is also relevant

A substantive examination is also carried out to determine whether the
following conditions of Article 3 were complied with at the date of the
application: the product is protected by the basic patent;

a valid authorisation to place the product on the market as a medicinal
product has been granted in accordance with the applicable regulatory
legislation for the territory in which the authorisation has effect (see
[SPM2.01](#refSPM2-01));

the product has not already been the subject of a certificate. (The
examiner carries out a search of certificates granted in the UK in order
to establish this.) Generally, the substantive examination is carried
out at the same time as the formalities examination, and all objections
arising are reported to the applicant in a single letter ([see
SPM10.12](#refSPM10-12)). If the basic patent has already expired or is
about to expire, substantive examination should be carried out as a
matter of urgency, in order to avoid delay in the entry into force of
the certificate. Substantive examination may however be deferred in
cases where the examiner considers it likely that the application may
not be able to meet a formal objection.

\[ A substantive examiner carries out the examination. \]

### SPM 10.04.1 {#spm-10041}

PA 1977, s.21 is also relevant

The applicant may request accelerated examination giving a reasoned
statement for the request. If allowed, the applicant is warned that,
even when the application is found to be in order for grant at an
earlier date, grant will not occur until a period of at least three
months has elapsed from the date of publication of the notice of filing
of the application in the Patents Journal to allow for third-party
observations.

### SPM 10.05 {#spm-1005}

ArtsM 3(d),10(5), (ArtsP3(1)(d), 10(5)) is also relevant

Although no search is at present carried out to establish whether the
authorisation specified was the first authorisation to place the product
on the market in the relevant territory as a medicinal product, the
examiner should also consider whether this requirement is met where
there is reason to do so (for example, on the basis of information
supplied by the applicant, observations by a third party ([see
SPM10.06](#refSPM10-06)) or information in another application for the
same product). See also Draco AB's SPC Application \[1996\] RPC 417.

### SPM 10.06 {#refSPM10-06}

PA 1977, s.21, sch 4A, para 4 is also relevant

Any observations by a third party, on the question whether the
application meets the conditions of the Regulation should be considered
by the examiner as in the case of an application for a patent. However,
as in the case of a patent application such observations must be in
writing and must be made before a certificate is granted.

### SPM 10.07 {#spm-1007}

r.82(1)(a) is also relevant

Where the examiner requires further information in order to make any
determination (for example, on how the active ingredient described in
the marketing authorisation relates to the claims of the basic patent),
the applicant should be required to furnish this within a specified
period.

### Formalities examination of an application for an extension

### SPM 10.08 {#spm-1008}

ArtsM 8, 10(6) is also relevant

An initial examination for formal matters is carried out to determine
whether the application:

is in the required form, (including the requirements of rule 14 of and
Schedule 2 to the Patents Rules 2007 as to size and presentation of
documents), and accompanied by the prescribed fee ([see
SPM8.07](#refSPM8-07));

was lodged within the period prescribed by Article 7 ([see
SPM7.02](#refSPM7-02));

contains the information prescribed by Article 8 ([see
SPM8.08](#refSPM8-08));

is accompanied by a copy of the statement indicating compliance with an
agreed paediatric investigation plan as referred to in regulation
58A(2)(a) of the Human Medicines Regulations 2012 as prescribed by
Article 8(1)(d)(i) ([see SPM8.09](#refSPM8-09));

if filed before 1 January 2021, contains proof that the product has been
authorized in all Member States by an authorisation issued by the EMA or
by national authorisations granted by each Member state as prescribed by
Article 8(1)(d)(ii) as it applies under regulation 69 of the Patents
(Amendment) (EU Exit) Regulations 2019 ([see SPM8.10](#refSPM8-10));

where an application for a certificate is pending a reference to the
certificate already filed as prescribed by Article 8(2) ([see
SPM8.08](#refSPM8-08));

where a certificate is granted a copy of the certificate already granted
as prescribed by Article 8(3) ([see SPM8.11](#refSPM8-11));

\[ The formalities examination is carried out by an EA in BOD.\]

### Substantive examination of an application for an extension

### SPM 10.09 {#spm-1009}

reg. 58A(2),(3) HMR and reg. 58A (5),(6) HMR is also relevant

A substantive examination is also carried out to determine whether, at
the date of the application, it entitles the holder of the patent or
certificate to the reward set out in regulation 58A of the Human
Medicines Regulations 2012. The examiner may seek to establish that:

the marketing authorisation(s) identified includes the required
statement indicating the compliance with an agreed paediatric
investigation plan ([see SPM 8.09 - SPM8.09.2](#refSPM8-09));

if the application was filed before 1 January 2021, the product is
authorized in all Member States;

the product has not already been the subject of one of the alternative
rewards set out in regulation 58A of the Human Medicines Regulations
2012 ([see SPM 8.13](#refSPM8-13)).

Generally, the substantive examination is carried out at the same time
as the formalities examination and at the same time as the pending
application for a certificate, if appropriate. All objections arising
are reported to the applicant in a single letter ([see
SPM10.12](#refSPM10-12)).

\[ A substantive examiner carries out the examination. \]

### SPM 10.10 {#refSPM10-10}

PA 1977, s.21, sch 4A, para 4 is also relevant

Any observations by a third party, on the question whether the
application meets the conditions of the Human Medicines Regulations
should be considered by the examiner as in the case of an application
for a patent. However, as in the case of a patent application such
observations must be in writing and must be made before an extension is
granted.

### SPM 10.11 {#spm-1011}

r.82(1)(a) is also relevant

Where the examiner requires further information in order to make any
determination, the applicant should be required to furnish this within a
prescribed period.

### Examination report

### SPM 10.12 {#refSPM10-12}

ArtsM10(2), 10(3), 10(4), (ArtsP 10(2), 10(3), 10(4)), r.108is also
relevant.

Where it appears to the examiner that formal objections arise, and/or
that any of the conditions of Article 3 is not met or that the holder of
the patent or certificate is not entitled to the reward set out in
regulation 58A of the Human Medicines Regulations 2012, the applicant
should be informed accordingly and allowed a specified period for reply
(generally four months for the first report, and two months for any
subsequent report). As in the case of formal or substantive examination
of an application for a patent, this period may be extended at the
request of the applicant. In Medeva BV v The Comptroller General of
Patents (\[2010\] EWHC 68 (Pat), \[2010\] RPC 20), Kitchin J (at \[42\])
confirmed that an examination report in respect of an SPC application is
not a decision against which an appeal can properly be filed under s.97
of the Patents Act 1977.

### SPM 10.12.1 {#spm-10121}

Where an application is not in the required form, does not contain all
of the required particulars and documents and/or is not accompanied by
the prescribed fee, the filing date will not be lost if the applicant
rectifies the irregularity or settles the fee within the specified
period. If the applicant wishes to extend this period, they should
request this in writing before the period expires, otherwise the
application may be rejected under Article 10(4). The request may be made
retrospectively in the covering letter accompanying the response or,
alternatively by email to the <pateot@ipo.gov.uk> in which case an
automatic acknowledgement of receipt will be issued (telephone requests
are not allowable). For an application for an extension of the duration
of a certificate, Jacob LJ directed in E I Du Pont Nemours & Co.
\[2009\] EWCA Civ 966, \[2010\] RPC 6 that failure to comply with the
provisions of Article 8(1)(d) at the time of submitting the application
for an extension is an irregularity which may be cured after the date of
application under Article 10(3).

### Amendment and correction

### SPM 10.13 {#spm-1013}

The details on Form SP1 or SP4 may be amended or corrected in response
to the examination report.

### SPM 10.14 {#spm-1014}

r.105 is also relevant

Where a proposed correction affects the details of application or grant
which have already been published ([see SPM9.02](#refSPM9-02),
[SPM11.01](#refSPM11-01)), the details of the correction will also need
to be published. In such cases, a request to correct should be made in
writing identifying the proposed correction. If the Office considers
that the correction is allowable, details will be advertised in the
Patents Journal.

### SPM 10.14.1 {#ref10-14-1}

Post-grant amendment of an SPC grant certificate may be permitted under
Section 27 of the Patents Act 1977 (which applies by virtue of Schedule
4A to the Patents Act 1977). Questions concerning this practice were
referred to the CJEU in Actavis Group and Actavis UK v Boehringer
Ingelheim Pharma \[2013\] EWHC 2927 (Pat) however in C-577/13 Actavis
Group PTC EHF, Actavis UK Ltd v Boehringer Ingelheim Pharma GmbH & Co.
KG these questions were not answered(see also
[SPM3.02.6.3](#refSPM3-02-1), [SPM3.02.6-3](#refSPM3-02-5)).

### SPM 10.15 {#refSPM10-15}

r.31 is also relevant

Where, before a certificate or extension has been granted, an applicant
desires to amend Form SP1 or SP4 other than in response to an official
objection (for example, to add details of further relevant
authorisations of which they have become aware) the amendment should be
formally requested in writing. This approach should not be used for
notification of an authorisation for the purposes of establishing
protection or extension in an additional territory. Instead, the
applicant should file Form SP6 (see [SPM13A.02](#refSPM13A-02)) for the
certificate and/or a new Form SP4 for the paediatric extension (see
[SPM13B.02](#refSPM13B-02)). For rectification of the duration of an SPC
following decision
[BLO/418/13](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F418%2F13&submit=Go+%BB){rel="external"},
see [SPM 8.03.1](#ref8-03-1), [SPM 13.05.1](#refSPM13-05-1).

### Re-examination

### SPM 10.16 {#spm-1016}

Where the applicant has amended or corrected the application, and/or
made submissions in response to any objection to formal or substantive
matters raised by the examiner, the application should be re-examined as
soon as possible. If formal objections have been met, any deferred
substantive examination should now be carried out. Where the examiner is
still not satisfied that the conditions of the Regulation are met,
either the outstanding objection(s) should be pursued in further
correspondence, by telephone or at an interview, or the rejection
procedure ([see SPM10.17](#refSPM10-17)) should be initiated. Unlike the
case of patent applications, there is no overall period within which an
application for a certificate or certificate must be in order for grant.
However, where the basic patent has already expired or is about to
expire, re-examination should be carried out as a matter of urgency see
[SPM10.04](#refSPM10-04)).

### Rejection of application

### SPM 10.17 {#refSPM10-17}

PA 1977, s97. is also relevant

Where the applicant has not replied to objections raised by the examiner
in respect of formal or substantive matters, or the examiner having
considered any amendments, corrections and/or submissions made by the
applicant in response is still not satisfied that the applicant fully
meets the conditions of the Regulation, the applicant should be informed
in an official letter of the examiner's opinion and the reasons
therefor, and that accordingly, unless the applicant requests to be
heard in the matter, the Comptroller proposes to reject the application
under Article 10(2) and/or 10(4) as appropriate. As in the case of an
application for a patent, any hearing will be taken by a senior officer
of the Office acting for the Comptroller and any adverse decision will
be subject to appeal to the Patents Court.

### SPM 10.17.1 {#refSPM10-17-1}

In British Technology Group Ltd's SPC Application \[1997\] RPC 118,
where it had been found that a valid product licence had not been
granted, the Hearing Officer went on to refuse permission to keep the
application open until the time that the applicants provided a valid
authorisation in accordance with Article 3(b). To do otherwise would put
third parties at a considerable disadvantage. The correct procedure was
to file a fresh application when all the requirements of the Regulation
could be met, particularly the provision of a valid market
authorisation. Article 3(c) is then not contravened because the first
filed application has not been granted ([see SPM3.03.1](#refSPM3-03-1)).

### Withdrawal of application

### SPM 10.18 {#spm-1018}

PA 1977, s.14(9) is also relevant

An applicant may request in writing that their application is to be
withdrawn at any time before a certificate or an extension is granted
([see
14.199-208](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-199)).
Any such withdrawal may not be revoked, although there are provisions in
the Act for correction Whilst there appears to be no bar on an
application being withdrawn before grant and subsequently being refiled
at a later date, grant of such an application would depend upon the time
limits of Article 7 being met.

### Grant of certificate

### SPM 10.19 {#refSPM10-19}

When all requirements are met, a certificate is granted. No letter will
be issued to inform applicants of the intention to grant an SPC (as is
current practice for patent applications). In Merck and Co., Inc. ([BL
O/108/08](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F108%2F08&submit=Go+%BB){rel="external"})
the hearing officer found that, where an application met the
requirements of Article 10, an SPC could be granted even if, by applying
the calculation of Article 13(1), it would never take effect at the end
of the lawful term of the basic patent. The certificate retains the
application number (see [SPM9.03](#refSPM9-03)). It states the date of
expiry of the maximum possible period of its duration and indicates that
entry into force is dependent upon the payment of fees.

### SPM 10.19.1 {#refSPM10-19-1}

When an extension is granted on an application for a certificate or
pending application then the certificate granted will indicate that the
extension has been included in the maximum possible period of its
duration. However, if the extension is granted for an existing
certificate then an amended certificate stating the extended maximum
possible period of duration will be granted.

### SPM 10.19.2 {#spm-10192}

ArtM 3(a) is also relevant

The Medicinal Regulation does not require that grant of the certificate
must occur before the basic patent expires, merely that the latter is in
force on the date of filing. Consequently, any such grant is
retrospective to the day after the basic patent expired (see
[SPM13.01](#refSPM13-01). Therefore, when details of filing of an
application are published in the Patents Journal, the public is put on
notice that grant of the certificate may occur at any time subsequently.

\[An examiner issues the granted certificate\]

### SPM 10.20 {#refSPM10-20}

ArtM 19(2), (ArtP 18(2)) is also relevant

Opposition to the grant of a certificate or an extension is not allowed
(see [SPM19.06](#refSPM19-06); see also [SPM10.06](#refSPM10-06),
[SPM10.10](#refSPM10-10) for procedure where a third party makes
observations in writing).

### SPM 10.21 {#spm-1021}

The Medicinal Regulation does not appear to invest the Office with the
power to refuse to grant a certificate on the grounds that the marketing
authorisation has lapsed or been withdrawn, provided that the
requirement of Article 3(b) has been met. Action may, however, be
considered after grant for declaration of lapse under Article 14(d) when
the certificate has come into force [SPM14.02 to 14.05](#refSPM14-02).

  -----------------------------------------------------------------------
   

  **Article 11: Publication**

  1\. Notification of the fact that a certificate has been granted shall
  be published by the comptroller. The notification shall contain at
  least the following information:\
  (a) the name and address of the holder of the certificate;\
  (b) the number of the basic patent;\
  (c) the title of the invention;\
  (d) the number and date of the UK, GB or NI authorisation or, where
  there is more than one such authorisation, of each authorisation
  provided under Article 8(1)(b) or Article 13A(1), the product
  identified in the authorisation and the territory in respect of which
  the authorisation has been granted or has effect as if granted;\
  (e) where there are EEA authorisations granted before any UK, GB or NI
  authorisation provided under Article 8(1)(b), the number and date of
  the earliest EEA authorisation;\
  (f) the duration of the certificate.\
  2. Notification of the fact that the application for a certificate has
  been rejected shall be published by the comptroller. The notification
  shall contain at least the information listed in Article 9(2).\
  3. Paragraphs 1 and 2 shall apply to the notification of the fact that
  an extension of the duration of a certificate has been granted or of
  the fact that the application for an extension has been rejected.\
  3a. Where notification is made that an extension of the duration of a
  certificate has been granted, the notification shall specify the
  territory in respect of which the extension has been granted.\
  4. The comptroller shall publish, as soon as possible, the information
  listed in Article 5(5), together with the date of notification of that
  information. The comptroller shall also publish, as soon as possible,
  any changes to the information notified in accordance with point (c) of
  Article 5(2).
  -----------------------------------------------------------------------

### SPM 11.01 {#refSPM11-01}

r\. 44(7) is also relevant

The information prescribed by Article 11, including the generic name of
the product when this appears in the market authorisation but not on
original Form SP1 or Form SP4, is published in the Patents Journal,
together with the date of grant or rejection. The certificate number
([see SPM9-03](#refSPM9-03)), product, date of grant or rejection and
duration of a granted certificate and extension are also entered in the
register under the entry for the basic patent.

\[An examiner arranges the publication in the Journal and the entry in
the register. \]

### SPM 11.02 {#spm-1102}

In both the Journal and the register:

the product is identified as that for which the certificate has been
granted, and may differ from that published upon application see
[SPM9.02](#refSPM9-02);

the duration of a granted certificate is identified by the date of
expiry of the maximum period of duration as determined by Article 13 and
if an extension has been granted.

### SPM 11.03 {#spm-1103}

A copy of the certificate of grant is retained on the file of the
application which is open to public inspection. However, no separate
publication of the certificate or an extension of a certificate
corresponding to the 'B' publication of a patent under the Patents Act
1977 is made.

### SPM11.04 {#refSPM11-04}

Article 11(4) was introduced by Regulation (EU) 2019/933 and requires
the Office to publish, as soon as possible, the information provided by
manufacturers intending to make generic or biosimilar versions of
SPC-protected medicines under the manufacturing waiver; the information
to be provided and the form in which it is provided is discussed at
[SPM5.11-12](#ref5-11). The Office will publish such information in the
electronic Patents Journal under the entry for the basic patent. In
addition, this information will also be published under the entry for
the basic patent on Ipsum.

\[An examiner arranges the publication in the Journal and on Ipsum. \]

  -----------------------------
   
  **Article 12: Annual fees**
  \[Omitted\]
  -----------------------------

#### Annual fees

### SPM 12.01 {#spm-1201}

ArtM 1(I), 10(2)- (4), 14(1)(c) is also relevant

Entry into effect of the certificate is subject to the payment of annual
fees in accordance with paragraph 5 of Schedule 4A to the Patents Act
1977 and rule 6 of the 2007 Fees Rules (see [paragraph
128B.10](/guidance/manual-of-patent-practice-mopp/section-128b-supplementary-protection-certificates/#ref128B-10)).
The Medicinal Regulation refers to these as "prescribed" fees, which are
defined as those set in the 2007 Fees Rules.

### SPM 12.01.1 {#spm-12011}

Article 12 of the Medicinal Regulation permitted, but did not require,
the charging of annual fees for certificates. The amendments made to the
assimilated law upon its incorporation into domestic law (see SP0.13.1)
omit Article 12, as the ability of the Office to set fees is established
in section 123 of the Patents Act 1977 (as applied to certificates via
Schedule 4A), which provides the Secretary of State with the power to
set fees on certificates and the processes for paying them. The 2007
Fees Rules were made, and continue to operate, under that power.

### SPM12.01.2 {#ref12-01-2}

As noted in SPM 10.21, the Regulation does not appear to invest the
Office with the power to refuse to allow a certificate to come into
force on the grounds that the marketing authorisation has lapsed or been
withdrawn, provided that the requirement of Article 3(b) had been met.
Action may, however, be considered later for declaration of lapse under
Article 14(d), after the certificate has come into force (see [SPM14.02
to SPM14.05](#refSPM14-02)).

\[An Examiner deals with all matters relating to the payment of annual
fees.\]

### Effective period of the certificate

### SPM 12.02 {#refSPM12-02}

r.116(5) is also relevant

The certificate holder is required to pay annual fees for the effective
period of the certificate. This is the maximum period of duration of the
certificate, as defined by Article 13, less any period for which the
certificate holder does not desire it to have effect and without
reference to any paediatric extension that may have been granted.

### SPM 12.03 {#refSPM12-03}

ArtM 13(1), 13(3), (ArtP13(1)), r.116(5) is also relevant

The effective period must consist of a single period starting the day
after the expiry of the basic patent (the "start date"). Where the
certificate holder opts for an effective period less than the maximum
period of the certificate, this period cannot subsequently be extended.
This follows from rule 116(5), which states that "once the certificate
has taken effect no further fee may be paid to extend the term of the
certificate unless an application for \[a paediatric extension\] is
made...". In Genentech Inc v The Comptroller General of Patents \[2020\]
EWCA Civ 475, it was argued that this rule allows an applicant who has
paid for a period less than the maximum duration to pay additional
annual fees if they file an application for a paediatric extension. The
Court rejected this argument and held that the correct interpretation of
r.116(5) is that, once the certificate has taken effect, no further fee
may be paid to extend the certificate except (if necessary) the
application fee for a paediatric extension.

### Date for payment

### SPM 12.04 {#refSPM12-04}

r.116(2)(a) is also relevant

The date by which the annual fees are payable is normally the start
date. The annual fees may not be paid earlier than three months before
that date.

### SPM 12.05 {#spm-1205}

r.116(2)(b) is also relevant

However, where the certificate is granted later than three months before
the expiry of the basic patent, the date for the payment of annual fees
is three months after the grant date of the certificate.

### Calculation of annual fees

### SPM 12.06 {#spm-1206}

Fr.6(2)is also relevant

An annual fee is payable for each year of the effective period of the
certificate. Any final period of less than 12 months is treated as a
whole year; for example, an effective period of 3 years 6 months will
require the payment of 4 years' annual fees.

### SPM 12.07 {#spm-1207}

Fr.6(2), Fr. 6(3) is also relevant

The annual fees are payable as a single cumulative amount as a condition
of the certificate taking effect. The level of the fees is that applying
on the start date or, if paid earlier, the actual date of payment. No
additional fees are payable for an extension to a certificate to take
effect.

### SPM12.07.1 {#refSPM12-07-1}

Currently, for an effective period of the certificate lasting:\
up to one year from the start date, the total fees payable are £600;\
up to two years from the start date, the total fess payable are £1300;\
up to three years from the start date, the total fees payable are
£2100;\
up to four years from the start date, the total fess payable are £3000;\
up to five years from the start date, the total fees payable are £4000.

### Notification that payment is due

### SPM 12.08 {#spm-1208}

r.116(3) is also relevant

The certificate holder is notified not later than two months beforehand
of the date on which the fees are payable and of the level of the fee
payable in respect of each year. Where the certificate is granted later
than three months before the expiry of the basic patent, this
notification is sent with the granted certificate.

### SPM 12.09 {#spm-1209}

r.116(8) is also relevant

The notification is sent to the address for service provided on Form
SP1, or any address replacing it. It is also sent to the following
address, where different:

\(i\) the address specified for the sending of renewal reminders on
payment of the last renewal fee relating to the basic patent, or any
address replacing it; or

\(ii\) where there is no address under (i), any address for service
entered in the register in respect of the basic patent.

### Procedure for payment of fees

### SPM 12.10 {#refSPM12-10}

r.116(5) is also relevant

The payment of the total sum of the annual fees for the whole effective
period should be accompanied by Form SP2 (but see
[SPM19.04](#refSPM19-04)). The holder of the certificate should state on
this Form the date on which fees are payable (the "due date"), the
desired effective period of the certificate, and the amount of fees paid
in consequence. There is presently no electronic payment system for the
payment of fees for SPCs (BL O/252/11, Tulane Education Fund v
Comptroller General of Patents \[2012\] EWHC 932 (Pat); see also
[SPM12.14.1](#refSPM12-14-1) and [SPM14.01.1](#refSPM14-01-1)).

### SPM12.10.1 {#refSPM12-10-1}

In [Tulane Education Fund v Comptroller General of Patents \[2013\] EWCA
Civ 890, \[2014\] RPC
10](http://www.bailii.org/ew/cases/EWCA/Civ/2013/890.html){rel="external"},
the Court of Appeal rejected the argument that this payment regime was
ultra vires, noting that Article 12 did not impose any restriction on
when or how the fees must be paid.

### SPM12.10.2 {#refSPM12-10-2}

In [Genentech Inc v The Comptroller General of Patents \[2020\] EWCA Civ
475](http://www.bailii.org/ew/cases/EWCA/Civ/2020/475.html){rel="external"},
the Court of Appeal rejected the argument that the applicant should not
be able to choose and pay fees for a shorter period of protection than
the maximum duration provided by Article 13. Instead, the Court held
that the applicant may elect to take the certificate for a shorter
period by paying annual fees for less than the full term if they wished,
but if they did this they would not be able to subsequently top up these
fees to extend the effective period.

### SPM 12.11 {#spm-1211}

The Office confirms the payment of fees and the date of the expiry of
the effective period by sending a certificate of payment to the address
given in Section 6 of Form SP2. If the holder wishes this certificate to
be sent to a different address, they should indicate this at Section 7
of Form SP2 and give the address on a separate sheet.

### Late payment of fees

### SPM 12.12 {#spm-1212}

r.116(6) is also relevant

Where the annual fees are outstanding, the holder of the certificate is
notified within 6 weeks of the due date.

### SPM 12.13 {#refSPM12-13}

Fr.6(4) is also relevant

Annual fees may be paid up to six months after the due date, subject to
a late payment fee of one-half of the amount of the unpaid fees. The
six-month period for payment cannot be extended. Once paid, the annual
fees are treated as having been filed on the due date.

### Non payment of fees

### SPM 12.14 {#refSPM12-14}

ArtM 14(1)(c), (ArtP 14(1)(c)) is also relevant

If the fees are not paid by the due date or in accordance with
[SPM12.13](#refSPM12-13), the certificate is treated as having lapsed on
the date of expiry of the basic patent and so does not take effect. The
holder is notified accordingly.

### SPM 12.14.1 {#refSPM12-14-1}

In [BL
O/252/11](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/252/11){rel="external"}
(The Administrators of the Tulane Education Fund), the hearing officer
confirmed that an SPC could not be brought into effect where the
applicant had failed to pay the prescribed fee within the prescribed
time period or within the six months following the end of the prescribed
period. In dismissing the applicant's appeal at the Patents Court, Roger
Wyland QC (sitting as a Deputy High Court Judge) clarified that neither
rule 107(3) of the Patents Rules 2007 nor Section 28 of the Patents Act
1977 could be used to bring the SPC into effect in such circumstances
(Tulane Education Fund v Comptroller General of Patents \[2012\] EWHC
932 (Pat)); see also [SPM12.10](#refSPM12-10) and
[SPM14.01.1](#refSPM14-01-1).

### Fees for manufacturing waiver notifications

### SPM 12.15 {#refSPM12-15}

Article 12(2) was introduced by Regulation (EU) 2019/933 and allowed the
Office to charge a fee for notifications under Art. 5(2)(b) and (c) (see
[SPM5.11-12](#refSPM5-11)). The Office does not charge a fee for such
notifications at present, although doing so would fall within the power
to set fees provided by section 123 of the Patents Act 1977.

  -----------------------------------------------------------------------
   

  **Article 13: Duration of the certificate**

  1\. The certificate shall take effect at the end of the lawful term of
  the basic patent for a period equal to the period which elapsed between
  the date on which the application for a basic patent was lodged and the
  date of the first authorization to place the product on the market in
  the area comprising the European Economic Area and the United Kingdom,
  reduced by a period of five years.\
  2. Notwithstanding paragraph 1, the duration of the certificate may not
  exceed five years from the date on which it takes effect.\
  3. The periods laid down in paragraphs 1 and 2 shall be extended by six
  months in the case where regulation 58A of the Human Medicines
  Regulations 2012 applies. In that case, the duration of the period laid
  down in paragraph 1 of this Article may be extended only once.\
  4. Where a certificate is granted for a product protected by a patent
  which, before 2 January 1993, had its term extended or for which such
  extension was applied for, under national law, the term of protection
  afforded under this certificate shall be reduced by the number of years
  by which the term of the patent exceeds 20 years.\
  5. An extension of the duration of a certificate in accordance with
  paragraph 3 in respect of-\
  (a) a UK authorisation shall apply in the United Kingdom;\
  (b) a GB authorisation shall apply in only England and Wales and
  Scotland, and\
  (c) a NI authorisation shall apply in Northern Ireland only,\
  on condition that the territorial protection conferred by the extension
  does not exceed that conferred by the certificate.
  -----------------------------------------------------------------------

### SPM 13.01 {#refSPM13-01}

ArtM 12, (ArtP12), PA 1977, Sch4A, para 5 is also relevant

A certificate takes effect at the end of the lawful term of the basic
patent, provided that:

the basic patent has not previously lapsed or been revoked;

the annual fees are paid in time (see [SPM12.04-05](#ref12-04)).

### SPM 13.02 {#refSPM13-02}

Article 13 defines the maximum period of duration of the certificate. As
confirmed by the Court of Appeal in Genentech Inc v The Comptroller
General of Patents \[2020\] EWCA Civ 475, the effective period may
however be less than this maximum period if the certificate holder opts
to pay fees for a lesser period, the effective period may however be
less than this maximum period if the certificate holder opts to pay fees
for a lesser period (see [SPM12.02-03](#refSPM12-02) and
[SPM12.10.2](#refSPM12-10-2)).

### SPM 13.03 {#spm-1303}

r.44(7) is also relevant

The date of entry into force of the certificate and the date of expiry
of the effective period see [SPM12.02](#refSPM12-02) are published in
the Patents Journal and entered in the register under the entry for the
basic patent.

\[An examiner arranges the publication in the Journal and the entry in
the register. \]

### Calculation of the duration of the certificate

### SPM 13.04 {#refSPM13-04}

Since the term of the basic patent is 20 years, the maximum period
defined by Article 13 is either:

a period of 15 years from the date of the first authorisation to place
the product on the market in the UK or EEA; or

a period of 5 years from the date on which it takes effect,

whichever is the lesser (see [SPM18.02](#refSPM18-02)) for appeal where
duration is incorrectly calculated.) The period of 15 years runs from
the date of the first authorisation to place the product on the market
in either the United Kingdom or a State which is a Contracting Party to
the European Economic Area Agreement, whichever is earliest. The
European Court of Justice in Novartis AG & University College London &
Novartis AG and Institute of Microbiology v Comptroller General of
Patents, Designs and Trade Marks for the UK and Ministre de l'Économie v
Millennium Pharmaceuticals Inc. (joined cases C-207/03 and C-252/03)
\[2005\] RPC 33 held that when a Swiss authorisation automatically
recognized in Liechtenstein was the first in the EEA it constituted the
first authorisation for the purposes of Article 13 (see also
[SP0.08-09](#refSP0-08) and [SPM8.02](#refSPM8-02)). In AstraZeneca AB
[BL
O/146/12](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/146/12){rel="external"},
the hearing officer found that when a Swiss authorisation automatically
recognized in Liechtenstein was the first in the EEA it constituted the
first authorisation in the Community for the purposes of Article 13 even
if that authorisation was suspended at a later date. The decision was
appealed and questions referred to the CJEU for a preliminary ruling in
C-617/12. The court determined the questions by reasoned order referring
to Novartis (see also [SPM 8.02](#refSPM8-02)).

### SPM 13.04.1 {#refSPM13-04-1}

The period of 15 years also runs from the first pharmaceutical or
veterinary authorisation for such a State irrespective of whether the
first authorisation under Article 3(b) to place the product on the
market in the UK is pharmaceutical or veterinary. Thus, in Farmitalia
Carlo Erba S.r.l's SPC Application (1) \[1996\] RPC 111 the hearing
officer held that on the plain meaning of Article 13(1), an Italian
veterinary authorisation of 1987 and not a Netherlands pharmaceutical
authorisation of 1992 constituted the first authorisation in the
Community, in a case where the Article 3(b) authorisation was
pharmaceutical. Similarly, in Pharmacia Italia SpA v Deutsches Patentamt
\[2005\] RPC 27 (C-31/03), the authorisation as a veterinary product was
held by the ECJ to be the first market authorisation in the Community
for an SPC application made on the basis of a medicinal product for
human use. The ECJ therefore ruled that the grant of the certificate was
precluded by the veterinary authorisation as this took place before the
date specified under Article 19(1) of the original Regulation.

### SPM 13.04.2 {#refSPM13-04-2}

The question of whether the marketing authorisation which must be
identified under Article 13(1) must be compliant with Council Directive
65/65/EEC or whether any marketing authorisation that enables the
product to be placed on the market in the EEA should count for the
calculation of the duration of the SPC has been considered by the Court
of Justice of the European Union in two separate cases (Generics (UK)
Ltd v Synaptech Inc \[2009\] EWHC 659 (Ch), referred to the CJEU in
Generics (UK) Ltd v Synaptech Inc, C-427/09; and Synthon v Merz Pharma
\[2009\] EWHC 656 (Pat), referred to the CJEU in Synthon BV v Merz
Pharma Gmbh & Co KG, C-195/09). In both cases the court held that a
product which was placed on the market in the European Community as a
medicinal product for human use before obtaining a marketing
authorisation in accordance with Council Directive 65/65/EEC and, in
particular, without undergoing safety and efficacy testing, is not
within the scope of the Regulation, and may not be the subject of a
supplementary protection certificate. It went on to confirm that any SPC
granted for a product which was outside the scope of the Regulation was
invalid (see also [SPM2.01](#refSPM2-01)).

### SPM 13.04.3 {#refSPM13-04-3}

Following the accession of ten new member states to the EU on 1 May
2004, and subsequent enlargements on 1 January 2007 and 1 July 2013, a
national authorisation granted in one of these states from the accession
date is considered to be valid in the EEA. However, such authorisations
would be used to determine the length of a certificate only if no other
marketing authorisation had already been granted in the European
Economic Area (see [SP0.08.1](#refSP0-08-1)).

### SPM 13.05 {#spm-1305}

It follows from Article 13 that a certificate would have no effective
duration in the case in which the date of the first authorisation to
place the product on the market is not more than five years from the
filing date of the basic patent. However, the hearing officer found in
Merck and Co., Inc. (BL O/108/08) that where an application met the
requirements of Article 10, an SPC could be granted even if it would
never take effect and its term could not extend beyond the end of the
lawful term of the basic patent unless it was extended under Article
13(3). An equivalent case arising from the German Patent and Trade Mark
Office was referred to the CJEU (C-125/10 - Merck & Co Inc v Deutsche
Patent- und Markenamt) where it was found that an SPC should be granted
where it would have no positive term unless extended under Article 13(3)
and that the duration of such an extension should start "from the date
determined by deducting from the patent expiry date the difference
between five years and the duration of the period which elapsed between
the lodging of the patent application and the grant of the first
marketing authorisation".

### SPM 13.05.1 {#refSPM13-05-1}

The duration of an SPC will be calculated having regard to the date of
grant of the first authorisation unless evidence is provided to
substantiate a different date of legal effect, [see also
SPM8.03.1](#refSPM8-03-1). Following decision [BLO/418/13 (Genzyme
Corporation)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F418%2F13&submit=Go+%BB){rel="external"}
(Genzyme Corporation), in the situation where the earliest authorisation
is one granted by a decision of the European Commission following a
favourable opinion from the EMA, the office will calculate the duration
of the SPC according to Article 13(1) from the date of notification. A
suitable excerpt from the OJEU may be provided as evidence of the
notification date. (see [SPM 8.03.1](#refSPM8-03-1) and 10.15). The
relevance of the "date of notification" in these circumstances has been
confirmed by the CJEU in Seattle Genetics Inc. v Österreichisches
Patentamt C-471/14. Applicants may apply to rectify the duration of an
SPC, in this regard, at any time before the certificate (or extensions
thereto) expires, in accordance with the practice notice published on 20
November 2013. This practice has been confirmed by the CJEU in Incyte
Corporation v Szellemi Tulajdon Nemzeti Hivatala C-492/16.

### Calculation of maximum duration period

### SPM 13.06 {#refSPM13-06}

The Office will generally invite the applicant to confirm agreement with
the maximum expiry date calculated by the Examiner on the basis of the
facts presented on Form SP1 (see
[SPP17.02](/guidance/manual-of-patent-practice-mopp/regulation-ec-no-1610-96-of-the-european-parliament-and-of-the-council-plant-protection-products/#refSPP17-02)).

### Duration of an extension

### SPM 13.07 {#refSPM13-07}

An extension of a certificate increases the period of duration of the
certificate by six months. However, as held by the Court of Appeal in
Genentech Inc v The Comptroller General of Patents \[2020\] EWCA Civ
475, this extension applies to the maximum duration of the SPC as laid
down in paragraphs (1) and (2) of Article 13. If the applicant has
chosen a shorter effective period by paying fees for only part of the
maximum duration, this shorter period cannot be extended.

### SPM13.08 {#refSPM13-08}

Reg 58A(4A), (4B) HMR is also relevant.

Article 13(5) sets out the territorial extent of the extension,
according to the territory in which the authorisation(s) containing the
statement of compliance have effect (see [SPM8.09](#refSPM8-09)).
Paragraphs (4A) and (4B) of regulation 58A of the Human Medicines
Regulations 2012 mirror this, establishing that if the requirements for
the extension are met for only part of the territory covered by the
certificate, the extension will only have effect in that part. Equally,
the extension will not apply where the territory in which that
authorisation has effect does not cover territory in which protection is
conferred by the certificate.

  -----------------------------------------------------------------------
   

  **13A: Authorisation granted after submission of an application for a
  certificate**

  1\. Where after the date of submission of an application under Article
  7(1) or (2), but before the grant of a certificate under Article 10(1)
  in relation to a NI authorisation, a valid UK or GB authorisation is
  granted which, at its date of grant, is the first authorisation to
  place the product on the market as a medicinal product in the territory
  of the United Kingdom or the territory of England and Wales and
  Scotland as the case may be, the applicant shall notify the comptroller
  of the grant of the authorisation, within six months of its date of
  grant and before the certificate takes effect under Article 13(1), and
  provide the details set out in Article 8(1)(a)(iv) and (b) on the
  prescribed form.\
  2. Where after the submission of an application under Article 7(1) or
  (2), but before the grant of a certificate under Article 10(1) in
  relation to a UK or GB authorisation, a valid NI authorisation is
  granted which, at its date of grant, is the first authorisation to
  place the product on the market as a medicinal product in the territory
  of Northern Ireland, the applicant shall notify the comptroller of the
  grant of the authorisation, within six months of its date of grant and
  before the certificate takes effect under Article 13(1), and provide
  the details set out in Article 8(1)(a)(iv) and (b) on the prescribed
  form.\
  3. Where after the grant of a certificate under Article 10(1) in
  relation to a UK or GB authorisation, but before expiry of the basic
  patent, a valid NI authorisation is granted which, at its date of
  grant, is the first authorisation to place the product on the market as
  a medicinal product in the territory of Northern Ireland, the
  certificate holder shall notify the comptroller of the grant of the
  authorisation, within six months of its date of grant and before the
  certificate takes effect under Article 13(1), and provide the details
  set out in Article 8(1)(a)(iv) and (b) on the prescribed form.\
  4. Where after the grant of a certificate under Article 10(1) in
  relation to a NI authorisation, but before expiry of the basic patent,
  a valid UK or GB authorisation is granted which, at its date of grant,
  is the first authorisation to place the product on the market as a
  medicinal product in the territory of the United Kingdom or the
  territory of England and Wales and Scotland as the case may be, the
  certificate holder shall notify the comptroller of the grant of the NI
  authorisation, within six months of its date of grant and before the
  certificate takes effect under Article 13(1), and provide the details
  set out in Article 8(1)(a)(iv) and (b) on the prescribed form.\
  5. If the applicant or the certificate holder fails to notify the
  comptroller of the grant of an authorisation in accordance with
  paragraph 1, 2, 3 or 4 the protection conferred by a certificate
  granted under Article 10 shall not extend to any additional territory
  covered by that authorisation.\
  6. On receipt of a notification under any of paragraphs 1 to 4, the
  comptroller shall publish:\
  (a) the number and date of the authorisation,\
  (b) the product identified in that authorisation, and\
  (c) the territory in respect of which the authorisation has been
  granted or has effect as if granted.
  -----------------------------------------------------------------------

### SPM13A.01 {#refSPM13A-01}

Article 13A was introduced by the [Supplementary Protection Certificates
(Amendment) (EU Exit) Regulations
2020](https://www.legislation.gov.uk/uksi/2020/1471/contents/made){rel="external"}
(SI 2020/1471). It sets out the requirements for notification and
publication of any UK, GB or NI authorisations (which are the first in
their respective territory) granted after an SPC application has been
filed.

### SPM13A.02 {#refSPM13A-02}

The circumstances in which a notification is made will depend on the
stage at which the application or the certificate has reached when the
later marketing authorisation is granted. The effect of the notification
will depend on the type of authorisation filed at application stage and
the type of authorisation notified. However, in all cases, the procedure
is identical. The applicant or certificate holder must notify the Office
of the additional authorisation within six months of it being granted,
and before the certificate takes effect. The notification must be made
on Form SP6, which asks for the information covered by Article
8(1)(a)(iv) and 8(b), as well as the number of the application or
certificate the notification relates to.

### SPM13A.03 {#refSPM13A-03}

Once the Office has been notified of the additional authorisation, and
provided that the authorisation meets the requirements of Article 3(b)
and (d), the protection conferred by the certificate (when it takes
effect) will extend to the territory of the additional authorisation in
accordance with Article 5(1a) and (1b) (see SPM5.05-5.05.2). If the
Office is not notified of the additional authorisation within the time
limits set by Article 13A, the protection will not be extended.

### SPM13A.04 {#refSPM13A-04}

The Office will publish the details of the additional authorisation
following receipt of the form SP6 in the Journal and on the register
entry for the basic patent.

  -----------------------------------------------------------------------
   

  **Article 13B: Extension of the duration of a certificate**

  1\. Where after an application for an extension of the duration of a
  certificate in accordance with Article 7(3) or (4) has been made in
  respect of a GB authorisation, but before the application is granted,
  an application is also made for an extension of the duration of the
  certificate in respect of a NI authorisation in accordance with Article
  7(3) or (4), the duration of the certificate, if the extension is
  granted, shall be extended in accordance with Article 13(3) and (5) to
  include the territory of Northern Ireland.\
  2. Where after an application for an extension of the duration of a
  certificate in accordance with Article 7(3) or (4) has been made in
  respect of a NI authorisation, but before the application is granted,
  an application is also made for an extension of the duration of the
  certificate in respect of a GB authorisation in accordance with Article
  7(3) or (4), the duration of the certificate shall be extended in
  accordance with Article 13(3) and (5) to include the territory of
  England and Wales and Scotland.\
  3. Where after the grant in accordance with Article 10(6) of an
  application for an extension of the duration of a certificate in
  respect of a GB authorisation, an application is made, in accordance
  with Article 7(4), for an extension of the certificate in respect of an
  NI authorisation, the duration of the certificate shall be extended in
  accordance with Article 13(3) and (5) to include the territory of
  Northern Ireland.\
  4. Where after the grant, in accordance with Article 10(6) of an
  application for an extension of the duration of a certificate in
  respect of a NI authorisation, an application is made, in accordance
  with Article 7(4), for an extension of the certificate in relation to a
  GB authorisation, the duration of the certificate shall be extended in
  accordance with Article 13(3) and (5) to include the territory of
  England and Wales and Scotland.
  -----------------------------------------------------------------------

### SPM13B.01 {#refSPM13B-01}

Article 13B was introduced by the [Supplementary Protection Certificates
(Amendment) (EU Exit) Regulations
2020](https://www.legislation.gov.uk/uksi/2020/1471/contents/made){rel="external"}
(SI 2020/1471). It sets out how a request can be made to allow a
paediatric extension (that has already been applied for in respect of
either Great Britain or Northern Ireland) to have effect in the
remaining territory (i.e. Northern Ireland or Great Britain
respectively).

### SPM13B.02 {#refSPM13B-02}

As with Article 13A, although the Article sets out several different
ways the extension can be applied to further territory, depending on
whether the original extension request has been granted and the
authorisation type it was based on, the procedure is the same. In each
instance, to have the extension apply in the remaining territory, a
separate application must be made using Form SP4 based on the additional
authorisation.

### SPM13B.03 {#refSPM13B-03}

Since the Article links back to the requirements of Article 7, the
standard timescale for filing a request for a paediatric extension also
applies to applications made using Article 13B; that is, the application
must be made before the two year deadline set by Article 7(4).
Similarly, the information required by Article 8 must be provided on the
Form SP4.

### SPM13B.04 {#refSPM13B-04}

It is possible for the effects of Articles 13A and 13B to combine. If an
authorisation was granted for the additional territory which included a
statement of compliance, the applicant or certificate holder would be
able to extend both the original duration of the certificate and the
paediatric extension to that additional territory. The requirements of
both Articles (that is, filing the relevant Forms and meeting the
relevant timescales) would need to be met.

  -----------------------------------------------------------------------
   

  **Article 14: Expiry of the certificate**

  1\. The certificate shall lapse:\
  (a) at the end of the period provided for in Article 13;\
  (b) if the certificate holder surrenders it;\
  (c) if the prescribed annual fee is not paid in time;\
  (d) if and as long as the product covered by the certificate may no
  longer be placed on the market following the withdrawal of all UK, GB
  or NI authorisations to place on the market.\
  The comptroller may decide on the lapse of the certificate either of
  the comptroller's own motion or at the request of a third party.\
  2. Where a UK authorisation is withdrawn and replaced simultaneously
  with a GB authorisation and a NI authorisation, the certificate granted
  in respect of the UK authorisation shall not lapse.\
  3. Where a UK, GB or NI authorisation is withdrawn, but one or more
  such authorisations remain valid, the protection conferred by the
  certificate shall, as from the date of withdrawal, no longer extend to
  the territory covered by the authorisation withdrawn but shall continue
  in respect of the territory covered by any remaining authorisation.
  -----------------------------------------------------------------------

### SPM 14.01 {#refSPM14-01}

ArtM 17, (ArtP 16) r.44(7) is also relevant

Notification of lapse is published in the Patents Journal, and is also
entered in the register under the entry for the basic patent. (See also
[SPM17.01](#refSPM17-01)).

\[An examiner arranges the publication in the Journal and the entry in
the register\]

### SPM 14.01.1 {#refSPM14-01-1}

In [BL
O/252/11](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/252/11){rel="external"}
(The Administrators of the Tulane Education Fund) the hearing officer
held that an SPC could not be brought into effect where the applicant
had failed to pay the prescribed fee within the prescribed time period
or within the six months following the end of the prescribed period. In
dismissing the applicant's appeal at the Patents Court, Roger Wyland QC
(sitting as a Deputy Judge of the High Court) clarified that neither
Rule 107(3) of the Patents Rules 2007 or Section 28 of the Patents Act
1977 could be used to bring the SPC into effect (Tulane Education Fund v
Comptroller General of Patents \[2012\] EWHC 932 (Pat)); see also
[SPM12.10](#refSPM12-10) and [SPM12.14.1](#refSPM12-14-1).

### SPM 14.01.2 {#refSPM14-01-2}

If the applicant has chosen to pay annual fees for only part of the
maximum duration and has thereby selected a shorter effective period
([see SPM12.02-03](/#refSPM12-02) and [SPM12.10](#refSPM12-10)), then
the SPC will lapse under Art.14(c) at the end of this period (Genentech
Inc v The Comptroller General of Patents \[2020\] EWCA Civ 475).

### SPM 14.01.3 {#refSPM14-01-3}

Lapse under Article 14(1)(d) will only occur if all authorisations to
place the product on the market across the territories where the
certificate has effect are withdrawn. Article 14(2) and 14(3) set out
what happens to the certificate in the event that not all authorisations
are withdrawn.

### SPM 14.01.4 {#refSPM14-01-4}

If the certificate is based on a UK authorisation which is withdrawn,
but is simultaneously replaced by a combination of GB and NI
authorisations (for example, at the end of the transition period),
Article 14(2) expressly states that no lapse takes place; there is no
discontinuity in the ability to place the product on the market which
would result in lapse.

### SPM 14.01.2 {#refSPM14-01-2}

If the certificate relies on more than one authorisation covering
different parts of the UK, and not all authorisations are withdrawn,
Article 14(3) establishes that the certificate will continue in effect,
but the protection will only extend to the territory where a valid
authorisation exists.

### Declaration of lapse under Article 14(1)(d)

### SPM 14.02 {#refSPM14-02}

PR part 7 is also relevant

The Comptroller may declare that a certificate has lapsed under Article
14(1)(d), either on the application of any person, or on the
Comptroller's own initiative.

### SPM 14.03 {#refSPM14-03}

PR part 7 is also relevant

An application by a third party to the Comptroller for a declaration of
lapse under Article 14(1)(d) should be made on Form SP3 in duplicate
([but see SPM19.04](#refSPM19-04)) with duplicate statement of grounds,
this action carries a fee of £50. This starts proceedings before the
comptroller to determine the matter, the procedure for which is
discussed at paragraphs [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).

\[ Applications on Form SP3 for declaration of lapse are referred to
Tribunal Section. \]

### SPM 14.04 {#spm-1404}

PR part 7 is also relevant

Where the Office becomes aware, other than by an application on Form
SP3, of the withdrawal of the appropriate authorisation(s) to place on
the market a product covered by a certificate, the certificate holder is
informed of the withdrawal in an official letter and that, subject to
any observations which the holder may make within a specified period
(generally two months), the Comptroller proposes to declare that the
certificate has lapsed.

### SPM14.05 {#spm1405}

ArtM18 (ArtP 17) is also relevant

Any decision by the Comptroller, whether on the application of a third
party or on the Comptroller's own initiative, is subject to appeal to
the Patents Court.

### Restoration of certificate after lapse under Article 14(1)(d)

### SPM 14.06 {#spm-1406}

Where a new authorisation to place the product on the market is granted,
a certificate which has lapsed under Article 14(1)(d) automatically
takes effect again from the date of the new authorisation (unless the
certificate has been declared invalid or lapsed on any other ground,
such as surrender).

### SPM 14.07 {#refSPM14-07}

r.44(7) is also relevant

The certificate-holder should advise the Office of the grant of the new
authorisation(s). Notice of the termination of lapse under Article
14(1)(d) is then inserted in the Patents Journal see
[SPM17.01](#refSPM17-01).

### SPM 14.08 {#spm-1408}

PR part 7 is also relevant

Any person may apply to the Comptroller for a declaration that the
ground for lapse under Article 14(1)(d) no longer exists. This starts
proceedings before the comptroller to determine the matter, the
procedure for which is discussed at paragraphs [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).

### Surrender of certificate

### SPM 14.09 {#spm-1409}

r\. 42 is also relevant

Any offer by the holder to surrender a certificate should be made in
writing. No fee is at present required. The offer is examined in
accordance mutatis mutandis with the procedure under s.29 of the Patents
Act 1977 for the surrender of patents ([see
29.02-07](/guidance/manual-of-patent-practice-mopp/section-29-surrender-of-patents/#ref29-02)).

### SPM 14.10 {#refSPM14-10}

r.106(5)(6)(8) is also relevant

If a certificate is surrendered, a remission of annual fees is made for
any complete effective year(s) subsequent to the date of surrender.
Thus, if a certificate having a term of 4 years 3 months (for which five
years' fees would have been paid) is surrendered after 3 years 9 months,
the fifth year's fee is remitted.

### SPM 14.11 {#spm-1411}

No remission is made if a certificate lapses under Article 14(1)(d)
unless the holder first surrenders the certificate. This is because
lapse under Article 14(1)(d) may not be permanent whereas once
surrendered a certificate cannot be re-instated.

### SPM 14.12 {#spm-1412}

The district court of The Hague referred questions concerning whether
surrender of a certificate has retrospective effect in Georgetown
University and Octrooicentrum Nederland (Dutch Patent Office) C-484/12,
but given the responses to other questions referred the questions on
surrender did not need to be answered.

  -----------------------------------------------------------------------
   

  **Article 15: Invalidity of the certificate**

  1\. The certificate shall be invalid if:\
  (a) it was granted contrary to the provisions of Article 3;\
  (b) the basic patent has lapsed before its lawful term expires;\
  (c) the basic patent is revoked or limited to the extent that the
  product for which the certificate was granted would no longer be
  protected by the claims of the basic patent or, after the basic patent
  has expired, grounds for revocation exist which would have justified
  such revocation or limitation.\
  2. Any person may submit an application or bring an action for a
  declaration of invalidity of the certificate before the comptroller or
  the court.
  -----------------------------------------------------------------------

### SPM 15.01 {#refSPM15-01}

ArtM 17, (ArtP16) r.44(7) is also relevant

Notification of invalidity of a certificate is published in the Patents
Journal and is also entered in the register under the entry for the
basic patent ([see SPM17.01](#refSPM17-01)).

\[An examiner arranges the publication in the Journal and the entry in
the register.\]

### SPM 15.02 {#refSPM15-02}

PR part 7 is also relevant

An application for a declaration of invalidity of a certificate may be
made to the Comptroller or the Court as in the case of an application
for revocation of a patent.

### SPM 15.03 {#refSPM15-03}

In Hässle AB v Ratiopharm (C-127/00;\[2003\] ECR I-14781), the European
Court found that the first authorisation referred to in Article 19(1) of
the original Regulation meant the first authorisation required under the
provisions on medicinal products within the meaning of Council Directive
65/65 (now Directive 2001/83/EC, see [SPM3.03.1](#refSPM3-03-1)) and not
to authorisations required for legislation on the pricing or
reimbursement for medicinal products in a Member State. As a result,
where a certificate had been granted contrary to that requirement, the
certificate was invalid under Article 15. The Court held that this was
the case even if it was not possible to infer that the list of grounds
of invalidity of a certificate found in Article 15(1) was not
exhaustive.

### SPM 15.04 {#refSPM15-04}

In Generics (UK) Limited (trading as Mylan) and Novartis AG, \[2011\]
EWHC 2403 (Pat) Floyd J found the claims of the basic patent filed in
support of an SPC to be obvious and invalid, and consequently also found
the associated SPC be invalid.

### Application to the Comptroller

### SPM 15.05 {#spm-1505}

An application to the Comptroller for a declaration of invalidity of a
certificate should be made on Form SP3 but see [SPM19.04](#refSPM19-04).
The procedure is the same as in the case of an application for a
declaration of lapse (see [SPM14.03](#refSPM14-03). As occurs from time
to time, judgments of the Courts or CJEU will result in some granted
SPCs being invalid. The office will not re-examine granted SPCs in the
period between their dates of grant and coming into force, although a
check on the status of the basic patent is made before an SPC comes into
force. An interested party may however seek a declaration of invalidity.
Alternatively a non-binding opinion may be sought in this regard; see
Section 74A and [Opinions: resolving patent
disputes](https://www.gov.uk/guidance/opinions-resolving-patent-disputes).

\[Applications on Form SP3 for declaration of invalidity are referred to
Tribunal Section.\]

  -----------------------------------------------------------------------
   

  **Article 16: Revocation of an extension of the duration**

  1\. The extension of the duration may be revoked if it was granted
  contrary to the provisions of regulation 58A(3) of the Human Medicines
  Regulations 2012.\
  2. Any person may submit an application for revocation of the extension
  of the duration to the comptroller or the court.
  -----------------------------------------------------------------------

### SPM 16.01 {#refSPM16-01}

Procedures relating to revocation of an extension are in accordance with
those for a certificate see [SPM15.01](#refSPM15-01),
[SPM15.02](#refSPM15-02) and [SPM15.04](#refSPM15-04). In Dr Reddy's
Laboratories (UK) Ltd and Dr Reddy's Laboratories Ltd v Warner-Lambert
Company LLC \[2012\] EWHC 3715 (Pat) it was determined that the Court
had the power to revoke a paediatric extension having regard to Article
16(1). In Chugai (BL O/321/20), a granted paediatric extension was
revoked under Article 16 on the grounds that the certificate holder had
already received a 1-year extension to the period of marketing
protection for the medicinal product in accordance with Article 14(11)
of Regulation 726/2004; and so the extension was granted contrary to the
provisions of Article 36 of Regulation (EC) No 1901/2006 (see SPM8.13).
In this case the request for revocation came from the certificate
holder; the Hearing Officer held that "any person" in Article 16(2)
includes the certificate holder. (The equivalent provision in UK
domestic law is regulation 51A(12) of the Human Medicines Regulations
2012.)

  -----------------------------------------------------------------------
   

  **Article 17: Notification of lapse or invalidity**

  1\. If the certificate lapses in accordance with point (b), (c) or (d)
  of Article 14, or is invalid in accordance with Article 15, or if the
  territorial extent of the certificate is limited in accordance with
  Article 14(3), notification thereof shall be published by the
  comptroller.\
  2. If the extension of the duration is revoked in accordance with
  Article 16, notification thereof shall be published by the comptroller.
  -----------------------------------------------------------------------

### SPM 17.01 {#refSPM17-01}

r.44(7), ArtM 14(1)(a), ArtP 14(1)(a) is also relevant

The notifications required by Article 17, and also notification of
termination of lapse under Article 14(1)(d) see [SPM14.07](#refSPM14-07)
and notification of lapse at the end of the effective period of the
certificate see [SPM14.01](#refSPM14-01), are published in the Patents
Journal. These events are also entered in the register under the entry
for the basic patent.

\[An examiner arranges the publication in the Journal and the entry in
the register.\]

  -------------------------
   
  **Article 18: Appeals**
  \[Omitted\]
  -------------------------

### SPM 18.01 {#spm-1801}

PA 1977, s.97 ArtM 13(1) (ArtP 13(1)) is also relevant

Article 18 was omitted from the Regulation by SI 2019/801. As section 97
of the Patents Act applies to decisions taken by the comptroller under
the Medicinal Regulation by way of paragraph 3 of Schedule 4A, these are
open to appeal to the Patents Court in the same manner as decisions
taken in respect of patents. In Medeva BV v The Comptroller General of
Patents \[2010\] EWHC 68 (Pat), \[2010\] RPC 20, Kitchin J confirmed the
procedural details for filing an appeal against an SPC rejection by the
Intellectual Property Office also see [SPM10.12](#refSPM10-12).

\[ Matters within the Office relating to appeals from decisions taken
under the Regulation are dealt with by Tribunal Section.\]

### SPM 18.02 {#refSPM18-02}

ArtsM 13(1),8.1(a)(iv)

Recital (17) of the Plant Protection Regulation see
[SPM0.04](#refSPM0-04) has the effect that, from 8 February 1997 when
said regulation came into force, Article 17 of the original Regulation
(now Article 18 of the Medicinal Regulation) is additionally to be
interpreted in accordance with Article 17(2) of the Plant Protection
Regulation which states:

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Article 17(2) \[EC Plant Protection Regulation\]**
  The decision to grant the certificate shall be open to an appeal aimed at rectifying the duration of the certificate where the date of the first authorization to place the product on the market in the Community, contained in the application for a certificate as provided for in Article 8, is incorrect.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### SPM 18.02.1 {#refSPM18-02-1}

As this paragraph of the Plant Protection Regulation is assimilated law,
the ability to appeal the duration of the certificate remains applicable
to certificates granted under the Medicinal Regulation.

### SPM 18.03 {#spm-1803}

(ArtsP 13(1),8(1)(a)(iv))

Such an appeal may be lodged by the applicant or a third party. If the
appeal results in a corrected maximum expiry date for the granted
certificate the details will be notified to the public in the Patents
Journal.

  -----------------------------------------------------------------------
   

  **Article 19: Procedure**

  1\. In the absence of procedural provisions in this Regulation, the
  procedural provisions applicable to the corresponding basic patent (as
  modified by section 128B of, and Schedule 4A to, the Patents Act 1977)
  shall apply to the certificate.\
  2. Notwithstanding paragraph 1, the procedure for opposition to the
  granting of a certificate shall be excluded.
  -----------------------------------------------------------------------

### SPM 19.01 {#refSPM19-01}

Unless there are procedural provisions in the Medicinal Regulation, the
procedural provisions applicable to the corresponding basic patent apply
to the certificate, as may be modified by section 128B and Schedule 4A.

### SPM 19.02 {#refSPM19-02}

r.116, FSch 1, Fr.6(2), r.106(5),(6),(8), part 7, r.4, r.44(7) is also
relevant.

Section 123 of the Patents Act 1977 (as modified by Schedule 4A) also
permits the setting of rules regulating the business of the Office in
relation to certificates and applications for certificates or
extensions. Therefore, the 2007 Rules and the 2007 Fees Rules
additionally govern:

the application and fee in respect of the application (Articles 8 and
9);

the certificate of grant (Article 10);

annual fees (Article 12);

declaration of lapse or invalidity of the certificate (Articles 14(d)
and 15.1(a) and (c));

forms for use in connection with certificates and applications for
certificates (Article 19.1); and

publication of certain details (Articles 9.2, 11.1, 11.2 and 17).

### SPM 19.03 {#spm-1903}

r.116, Fsch.1, Fr.6(2) is also relevant

In particular the Rules provide for six specific Forms:

SP1 (Request for grant) see [SPM8.01](#refSPM8-01);

SP2 (Payment of annual fees) see [SPM12.10](#refSPM12-10);

SP3 (Application for decision of lapse or declaration of invalidity, or
revocation of a paediatric extension) (see [SPM14.03](#refSPM14-03),
[SPM15.04](#refSPM15-04) and [SPM16.01](#refSPM16-01);

SP4 (Application for an extension to the duration of a certificate)
([see SP0.11](#refSP0-11));

SP5 (Notification of intention to rely on manufacturing waiver) (see
SPM5.11-12)

SP6 (Notification of additional marketing authorisation) (see SPM13A.02)

and prescribe the fees payable thereon.

### SPM 19.04 {#refSPM19-04}

r.4(2) is also relevant

The requirement to use any of these Forms is satisfied by the use of a
form which is acceptable to the Comptroller and contains the required
information (such as a replica or photocopy of an official Form).

### SPM19.05 {#spm1905}

r.4 is also relevant

For actions other than those covered by Forms SP1 to SP6, the relevant
Patent Forms should be used and the same fee (if any) paid.

### SPM 19.06 {#refSPM19-06}

It follows from Article 19(2) that a procedure which provides for
opposition to the grant of a certificate is not allowed
[SPM10.20](#refSPM10-20).

### Requests for information (caveats)

### SPM 19.07 {#spm-1907}

r.54 is also relevant

Insofar as rule 54 of the Patents Rules 2007 is applicable, information
relating to certificates, applications for certificates, extensions of a
certificate and applications for extensions of a certificate is
available upon request, as in the case of patents and applications for
patents. Paragraphs 5(a), 5(b), 6(a) and 6(d) of rule 54 appear to have
no relevance to certificates and applications for certificates.
Paragraph 6(c) appears applicable mutatis mutandis to the provision of
information concerning the payment of annual fees see
[SPM12.10-12.13](#refSPM12-10).

### Documents open to public inspection

### SPM 19.08 {#spm-1908}

r.51(2)(b) is also relevant

Documents are normally made open to public inspection immediately after
they are filed at (or sent to) the Office.

### SPM 19.09 {#spm-1909}

r.53 is also relevant

The person filing or sending a document (other than any of Forms SP1 to
SP6), or any other person, may request within 14 days that the document
be kept confidential (giving reasons). The comptroller may then direct
that the document in question, or part thereof, should be treated as
confidential. The document is not open to public inspection while the
matter is being determined. Where a request is made to keep a document
confidential but no reasons are given the person filing the document is
requested to provide suitable reasons within a period of 14 days.

### SPM 19.10 {#spm-1910}

r.46, 48 is also relevant

Copies of any documents which are not treated as confidential are
available upon request as in the case of documents relating to patents.

### Extensions of time

### SPM 19.11 {#refSPM19-11}

r.108 is also relevant

In Abbott Laboratories' SPC Application \[2004\] RPC 20 it was held that
the six month time limit set out in Article 7 is extendable under
r.110(1) of the Patents Rules 1995 (which is equivalent to r.108(1) of
the Patents Rules 2007). There are no provisions in the Regulation
relating to extension of the Article 7 time limit, and there are no
special provisions for such extensions laid down by national law. Hence
the applicable provision governing any such extension of time is the
appropriate procedural provision under national law corresponding to the
basic patent. In Merck & Co., Inc ([BL
O/035/09](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F035%2F09&submit=Go+%BB){rel="external"}
the hearing officer found that the time limit set by the examiner for
rectifying an irregularity in filing the required documents was
extendable under r.108 ([see SPM8.09.1](#refSPM8-09-1)). The Court of
Appeal in E I Du Pont Nemours & Co. \[2009\] EWCA Civ 966, \[2010\] RPC
6 directed that the time periods for curing irregularities under Article
10(3) in an application for an extension of the duration of a
certificate can be extended ([see SPM8.09.1](#refSPM8-09-1)).

  ------------------------------------------------------------------------------------
   
  **Article 20: Additional provisions relating to the enlargement of the community**
  \[Omitted\]
  ------------------------------------------------------------------------------------

  -----------------------------------------
   
  **Article 21: Transitional provisions**
  \[Omitted\]
  -----------------------------------------

  -----------------------------
   
  **Article 21a: Evaluation**
  \[Omitted\]
  -----------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Article 22: Repeal**
  Regulation (EEC) No 1768/92, as amended by the acts listed in Annex I, is repealed. References to the repealed Regulation shall be construed as references to this Regulation and shall be read in accordance with the correlation table in Annex II.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### SPM22.01 {#refSPM22-01}

The assimilated law continues to abide by this glossing; any references
in UK domestic law to the original Regulation (including in other
provisions of assimilated law under the Withdrawal Act) are to be
construed in line with this Article. Hence references to the original
Regulation in the Plant Protection Regulation, such as in the recitals
(see [SPM0.04](#refSPM0-04)), continue to affect the Medicinal
Regulation.

  ---------------------------------------------------------------------------------------------------------------------------------
   
  **Article 23: Entry into force**
  This Regulation shall enter into force on the 20th day following its publication in the Official Journal of the European Union.
  ---------------------------------------------------------------------------------------------------------------------------------

### SPM23.01 {#spm2301}

The Medicinal Regulation was published in the Official Journal of the
European Union on 16 June 2009. It therefore entered into force on 6
July 2009.

  -----------------------------------------------------------------------
   

  **Annex 1: Repealed regulation with list of its successive amendments
  (referred to in Article 22)**

  Council Regulation (EEC) No 1768/92 (OJ L 182 2.7.1992, p. 1)\
  Annex I, point XI.F.I. of the 1994 Act of Accession (OJ C 241
  29.8.1994, p. 233)\
  Annex II, point 4.C.II, of the 2003 Act of Accession (OJ L 236
  23.9.2003, p. 342)\
  Annex III, point 1.II, of the 2005 Act of Accession (OJ L 157,
  21.6.2005, p. 56)\
  Regulation (EC) No 1901/2006 of the European Parliament and of the
  Council, Only Article 52 (OJ L 378, 27.12.2006, p. 1)
  -----------------------------------------------------------------------

  -------------------------------------------- -----------------------------------------------------------------
                                                
  **Annex II: Correlation table**               
  Regulation (EEC) No 1768/92                  This Regulation
  \-                                           Recital 1
  Recital 1                                    Recital 2
  Recital 2                                    Recital 3
  Recital 3                                    Recital 4
  Recital 4                                    Recital 5
  Recital 5                                    Recital 6
  Recital 6                                    Recital 7
  Recital 7                                    Recital 8
  Recital 8                                    Recital 9
  Recital 9                                    Recital 10
  Recital 10                                   \-
  Recital 11                                   \-
  Recital 12                                   \-
  Recital 13                                   Recital 11
  Article 1                                    Article 1
  Article 2                                    Article 2
  Article 3, introductory wording              Article 3, introductory wording
  Article 3, point (a)                         Article 3, point (a)
  Article 3, point (b), first sentence         Article 3, point (b)
  Article 3, point (b), second sentence        \-
  Article 3, points (c) and (d)                Article 3, points (c) and (d)
  Article 4 to 7                               Article 4 to 7
  Article 8(1)                                 Article 8(1)
  Article 8(1a)                                Article 8(2)
  Article 8(1b)                                Article 8(3)
  Article 8(2)                                 Article 8(4)
  Article 9 to 12                              Article 9 to 12
  Article 13(1), (2) and (3)                   Article 13(1), (2) and (3)
  Article 14 and 15                            Article 14 and 15
  Article 15a                                  Article 16
  Article 16, 17 and 18                        Article 17, 18 and 19
  Article 19                                   \-
  Article 19a, introductory wording            Article 20, introductory wording
  Article 19a, point (a) points (i) and (ii)   Article 20, point (b) introductory wording, points (i) and (ii)
  Article 19a, point (b)                       Article 20, point (c)
  Article 19a, point (c)                       Article 20, point (d)
  Article 19a, point (d)                       Article 20, point (e)
  Article 19a, point (e)                       Article 20, point (f)
  Article 19a, point (f)                       Article 20, point (g)
  Article 19a, point (g)                       Article 20, point (h)
  Article 19a, point (h)                       Article 20, point (i)
  Article 19a, point (i)                       Article 20, point (k)
  Article 19a, point (j)                       Article 20, point (l)
  Article 19a, point (k)                       Article 20, point (a)
  Article 19a, point (l)                       Article 20, point (j)
  Article 20                                   Article 21
  Article 21                                   \-
  Article 22                                   Article 13(4)
  \-                                           Article 22
  Article 23                                   Article 23
  \-                                           Annex I
  \-                                           Annex II
  -------------------------------------------- -----------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
