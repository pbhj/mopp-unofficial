::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 107: Costs and expenses in proceedings before the comptroller {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (107.01 - 107.13) last updated: October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 107.01

This section provides for the award of costs (expenses in Scotland) by
the comptroller in proceedings before them. It also provides for
enforcement of orders made by the comptroller awarding costs, and allows
the comptroller to require security for the costs of certain proceedings
before them.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 107(1)**
  The comptroller may, in proceedings before him under this Act, by order award to any party such costs or, in Scotland, such expenses as he may consider reasonable and direct how and by what parties they are to be paid.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Award of costs by comptroller

### 107.02

The comptroller is empowered to award by order any party in proceedings
before them under the 1977 Act such costs as he may consider reasonable
and direct how and by what parties they are to be paid.

### 107.03

As explained in Tribunal Practice Notice 4/2007 (reproduced in the
"Relevant Official Notices and Directions" section of this Manual),
costs in proceedings before the comptroller are not intended to
compensate parties for the expenses to which they have been put; they
are a contribution to costs in the form of a lump sum.

\[Guidance on the assessment of costs is given in chapter 5 of the
Patent Hearings Manual. \]

### 107.04 to 107.08 {#to-10708}

\[deleted\]

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 107(2)**
  In England and Wales any costs awarded under this section shall, if the county court so orders, be recoverable by execution issued from the county court or otherwise as if they were payable under an order of that court.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 107(3)**
  In Scotland any order under this section for the payment of expenses may be enforced in like manner as an extract registered decree arbitral bearing a warrant for execution issued by the sheriff court of any sheriffdom in Scotland.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Enforcement of order for costs

### 107.09 {#ref107-09}

An award of costs by a hearing officer cannot be enforced by the Office,
but a successful party who is unable to obtain the awarded amount from
the other party is at liberty to seek the assistance of the courts in
enforcing the award, in accordance with subsection (2), (3), (5) or (6)
as appropriate.

  -----------------------------------------------------------------------
   

  **Section 107(4)**

  The comptroller may make an order for security for costs or expenses
  against any party to proceedings before him under this Act if\
  (a) the prescribed conditions are met, and\
  (b) he is satisfied that it is just to make the order, having regard to
  all the circumstances of the case;\
  \
  and in default of the required security being given the comptroller may
  treat the reference, application or notice in question as abandoned.
  -----------------------------------------------------------------------

### Security for costs

### 107.10 {#ref107-10}

r.85 is also relevant

The comptroller may require a party to give security for costs or
expenses in proceedings. The conditions for making an order, which are
similar to those applicable to the courts under rule 25.13(2) of the
Civil Procedure Rules, are prescribed by r.85. These are that the party
against whom the order is made:\
\
(a) is resident outside the UK, but not resident in a Brussels
Contracting State, a Lugano Contracting State, or a Regulation State, as
defined in section 1(3) of the Civil Jurisdiction and Judgments Act
1982;\
(b)is a company or other body (whether incorporated inside or outside
the UK) and there is reason to believe that it will be unable to pay
another party's costs if ordered to do so;\
(c) has changed their address for service with a view to evading the
consequences of the litigation;\
(d) has furnished an incorrect address for service; or\
(e) has taken steps in relation to their assets that would make it
difficult to enforce an order for costs against them.\
\
In practice, the comptroller will only require such security following
successful application from another party to the proceedings.\
\
\[Guidance on security for costs is given in chapter 2 of the Patent
Hearings Manual.\]

### 107.11 {#section-3}

When an application asking the comptroller to require security for costs
has been successful, security for a fixed sum will generally be required
when the proceedings are initiated. If it does not accompany the
reference, application or notice, a letter is issued requiring provision
thereof. It suffices for the person's patent agent in the UK to
guarantee the sum in writing.

\[ The matter of security for costs is dealt with by Tribunal Section.
The letter should identify the writer and provide a contact telephone
number. \]

### 107.12 {#section-4}

If the required security is not given, the reference, application or
notice may be treated as abandoned.

  ---------------------------------------------------------------------------------------------------------------------------
   
  **Section 107(5)**
  In Northern Ireland any order under this section for the payment of costs may be enforced as if it were a money judgment.
  ---------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 107(6)**
  In the Isle of Man, any order under this section for the payment of costs may be enforced as if it were a judgment or order of the court for the payment of money.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 107.13 {#ref107-13}

::: call-to-action
[See 107.09](#ref107-09). Subsection (6) was added by the [Patents Act
1977 (Isle of Man) Order
1978](https://www.legislation.gov.uk/uksi/1978/621/contents/made){rel="external"}
(SI 1978/621), and was restated most recently by the [Patents Act 1977
(Isle of Man) Order
2013](https://www.legislation.gov.uk/uksi/2013/2602/contents/made){rel="external"}
(SI 2013/2602).
:::
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
