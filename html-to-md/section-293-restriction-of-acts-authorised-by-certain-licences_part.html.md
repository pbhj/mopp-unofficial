::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 293: Restriction of acts authorised by certain licences {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (293.01) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 293.01

The second group of sections in Part VI of the CDP Act amended the
provisions for licences of right under certain patents which were
granted under the Patents Act 1949. Section 293 inserted a new paragraph
4A into paragraph 4(2)(c) of Schedule 1 to the Patents Act 1977. As all
1949 Act patents have now expired, the provisions of this section are
now spent.

### 293.02-10

\[deleted\]

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 293**
  In paragraph 4(2)(c) of Schedule 1 to the Patents Act 1977 (licences to be available as of right where term of existing patent extended), at the end insert ", but subject to paragraph 4A below", and after that paragraph insert "
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 4A(1)**
  If the proprietor of a patent for an invention which is a product files a declaration with the Patent Office in accordance with this paragraph, the licences to which persons are entitled by virtue of paragraph 4(2)(c) above shall not extend to a use of the product which is excepted by or under this paragraph.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 4A(2)**

  Pharmaceutical use is excepted, that is -\
  \
  (a) use as a medicinal product within the meaning of the Medicines Act
  1968, and\
  \
  (b) the doing of any other act mentioned in section 60(1)(a) above with
  a view to such use.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 4A(3)**

  The Secretary of State may by order except such other uses as he thinks
  fit; and an order may\
  \
  (a) specify as an excepted use any act mentioned in section 60(1)(a)
  above, and\
  \
  (b) make different provision with respect to acts done in different
  circumstances or for different purposes.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 4A(4)**

  For the purposes of this paragraph the question what uses are excepted,
  so far as that depends on -\
  \
  (a) orders under section 130 of the Medicines Act 1968 (meaning of
  "medicinal product"), or\
  \
  (b) orders under sub-paragraph (3) above,\
  \
  shall be determined in relation to a patent at the beginning of the
  sixteenth year of the patent.
  -----------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 4A(5)**
  A declaration under this paragraph shall be in the prescribed form and shall be filed in the prescribed manner and within the prescribed time limits.
  -------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 4A(6)**

  A declaration may not be filed -\
  \
  (a) in respect of a patent which has at the commencement of section 293
  of the Copyright, Designs and Patents Act 1988 passed the end of its
  fifteenth year; or\
  \
  (b) if at the date of filing there is -\
  \
  (i) an existing licence for any description of excepted use of the
  product, or\
  \
  (ii) an outstanding application under section 46(3)(a) or (b) above for
  the settlement by the comptroller of the terms of a licence for any
  description of excepted use of the product, and, in either case, the
  licence took or is to take effect at or after the end of the sixteenth
  year of the patent.
  -----------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 4A(7)**

  Where a declaration has been filed under this paragraph in respect of a
  patent -\
  \
  (a) section 46(3)(c) above (restriction of remedies for infringement
  where licences available as of right) does not apply to an infringement
  of the patent in so far as it consists of the excepted use of the
  product after the filing of the declaration; and\
  \
  (b) section 46(3)(d) above (abatement of renewal fee if licences
  available as of right) does not apply to the patent".
  -----------------------------------------------------------------------
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
