::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 25: Term of patent {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (25.01 - 25.15) last updated: April 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 25.01

This section specifies when a patent begins to have effect, how long it
may continue in force, and what must be done to keep it in force.
Further requirements concerning renewals are set out in rr.36 to 39 and
41. The ection applies to both patents granted under the 1977 Act and
European patents (UK).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 25(1)**
  A patent granted under this Act shall be treated for the purposes of the following provisions of this Act as having been granted, and shall take effect, on the date on which notice of its grant is published in the journal and, subject to subsection (3) below, shall continue in force until the end of the period of 20 years beginning with the date of filing the application for the patent or with such other date as may be prescribed.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 25.02 {#ref25-02}

s.77(1) is also relevant.

Thus the effective date of grant for the purposes of all provisions of
the Act subsequent to s.25(1) is the date on which the specification of
the patent was published and the notice of grant appeared in the Journal
(or, in the case of a European patent (UK), the grant was mentioned in
the Bulletin).

### 25.03 {#ref25-03}

This date is later than the date on which a patent was granted [see
18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86),
[24.01](/guidance/manual-of-patent-practice-mopp/section-24-publication-and-certificate-of-grant/#ref24-01),
and which is the date effective for all provisions of the Act prior to
s.25(1). This was confirmed by the Hearing Officer in ITT Industries
Inc's Application \[1984\] RPC 23 (see especially page 27 line 20
onwards). There is thus an interval of several weeks during which the
provisions relating to an application have ceased to apply but those
relating to a granted patent do not yet have effect. For example, since
s.19 is a provision prior to s.25(1), an application cannot be amended
once a patent has been granted, but the specification of the patent
cannot be amended until after publication of the notice of grant,
because s.27 is a provision subsequent to s.25(1). Similarly, if a
question of entitlement is referred to the Comptroller between the date
on which the grant letter is issued and the date of publication of the
notice of grant, then no immediate action can take place under either of
sections 8 or 37. Instead, the question is treated as having been
referred under s.37 on the latter date -- [see
8.03.1](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-03-1).

### 25.04 {#section-1}

s.130(1) is also relevant.

The twenty-year term of the patent is measured from the date of filing.
In the case of a patent granted in pursuance of a UK application, this
is the date accorded under s.15. In the case of a European patent (UK)
or a patent granted on an international application, the date of filing
is that accorded under a.80 EPC or a.11 PCT respectively.

### 25.04.1 {#section-2}

A notice in the Journal dated 22 July 1992 makes clear that from the
date of that notice patent expiry dates recorded on the Register,
announced in the Journal and recorded in official renewal information,
would be based on the interpretation of relevant provisions of the
legislation that the full term expires on the day before the anniversary
of the filing date of the application; and that this change in procedure
applies only to the expiry of a patent and not to the lapsing of a
patent due to non-payment of a renewal fee. Thus a 20 year period that
starts on 4 November 1992 ends on 3 November 2012.

### 25.04.2 {#section-3}

Although the term of a patent may not be extended beyond the prescribed
20 year period, a medicinal product or plant protection product
protected by a patent may be protected for a further period of up to
five years from the expiry of the patent at the end of the twenty year
term by the grant of a supplementary protection certificate ([see
supplementary protection certificate
pages](/guidance/manual-of-patent-practice-mopp/supplementary-protection-certificates-for-medicinal-and-plant-protection-products).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 25(2)**
  A rule prescribing any such other date under this section shall not be made unless a draft of the rule has been laid before, and approved by resolution of, each House of Parliament.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 25.05 {#section-4}

Hence although s.25(1) authorises the making of a rule prescribing that
the term of a patent be measured from a date other than the filing date,
any such rule must be made by affirmative resolution. No such resolution
has so far been made.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 25(3)**
  Where any renewal fee in respect of a patent is not paid by the end of the period prescribed for payment (the "prescribed period") the patent shall cease to have effect at the end of such day, in the final month of that period, as may be prescribed.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 25.06 {#ref25-06}

r.37(2), r.38(2) is also relevant.

If it is desired to keep a patent in force after the end of the fourth
year from the date of filing, renewal fees must be paid for each
succeeding year (but see 25.08). Successively increasing amounts are
prescribed for each year after the fourth, and the fee for each year
should normally be paid within the period of three calendar months
ending on the last day of the month in which the renewal date falls. For
example, if a patent is filed on 10 May 2003 and granted on 17 June
2006, renewal fees are due on 31 May each year, starting in 2007, and
can be paid any time in March, April or May of each year (but [see
25.07](#ref25-07), [25.08](#ref25-08)).

### 25.07 {#ref25-07}

r.37(3) is also relevant.

If, in the case of a patent granted under the Act, the notice of grant
has not been published in the Journal by the end of three years and nine
months from the date of filing, then any renewal fees which have become
due (including any fees due in respect of preceding years) can be paid
at any time up to the end of the third calendar month after the month in
which the date of grant falls. For example, if a patent is filed on 20
April 2002 and granted on 16 November 2006, the renewal fee which would
have been due for payment on 30 April 2006 may be paid any time up until
28 February 2007. The fifth anniversary falls on 20 April 2007, so the
renewal fee for the following year would be due on 30 April 2007 and may
be paid any time in February, March or April of that year.

### 25.08 {#ref25-08}

r.37(4) a.86 EPC is also relevant.

Renewal fees to keep a European patent (UK) in force beyond four years
from its date of filing are payable to the Office only in respect of
each year following the year in which mention of the grant was published
in the Bulletin. (Fees in respect of years up to and including the year
in which mention of grant is published are paid direct to the EPO).
Where a renewal fee becomes due within three months after publication of
the mention of grant, it may be paid up to the end of the third calendar
month after the month in which the date of grant falls. If the
comptroller is not notified of an address for service for the proprietor
of a European patent (UK), then the proprietor's address on the register
will be treated as the address for service even if that address is
outside the United Kingdom [see also
77.05](/guidance/manual-of-patent-practice-mopp/section-77-effect-of-european-patent-uk/#ref77-05).

### 25.08.1 {#section-5}

a.112a EPC, s.77(5A), r.41A is also relevant.

Where a European patent (UK) has been revoked by the EPO Board of Appeal
and is subsequently restored by the Enlarged Board of Appeal following a
petition for review under EPC Art.112a, any renewal fees which fell due
whilst the patent was revoked are payable within a two-month period
following the restoration. [See
77.12.1](/guidance/manual-of-patent-practice-mopp/section-77-effect-of-european-patent-uk/#ref77-12-1)
for further details.

### 25.09 {#ref25-09}

r.36(3), r.36(5) is also relevant.

In addition to the renewal fee, Form 12 must also be filed within the
same period (preferably at the same time). Alternatively, a patent can
be renewed online. A certificate of payment will be issued once the fee
is received and/or a report detailing errors if the payment is
incorrect. Details of the renewal arrangements effective from 1 November
1999 were published in a supplement to the Patents and Designs Journal
of 20 October 1999.

### 25.10 {#section-6}

The amount of the fee due is that which is prescribed at the time
payment is made (but [see 25.14](#ref25-14)). As a consequence of the
stipulation that the fee be paid within the period of three calendar
months ending on the last day of the month in which the anniversary of
filing falls, it is not possible to circumvent an increase in the
prescribed fees by earlier payment.

### 25.11 {#section-7}

Anyone may pay the renewal fees on a patent - there is no requirement
that this be done either by the proprietor or with his agreement.

  -----------------------------------------------------------------------
   

  **Section 25(4)**

  If during the period ending with the sixth month after the month in
  which the prescribed period ends the renewal fee and any prescribed
  additional fee are paid, the patent shall be treated for the purposes
  of this Act as if it had never expired, and accordingly ­\
  (a) anything done under or in relation to it during that further period
  shall be valid\
  (b) an act which would constitute an infringement of it if it had not
  expired shall constitute such an infringement; and\
  (c) an act which would constitute the use of the patented invention for
  the services of the Crown if the patent had not expired shall
  constitute that use.
  -----------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 25(5)**
  Rules shall include provision requiring the comptroller to notify the registered proprietor of a patent that a renewal fee has not been received from him in the Patent Office before the end of the prescribed period and before the framing of the notification.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 25.12 {#ref25-12}

r.39(2), r.39(3), r.39(4) is also relevant.

Within six weeks after the end of the period allowed for paying a
renewal fee [see
25.06-25.08](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-06)
the comptroller must, if the fee is still unpaid, send to the proprietor
a notice reminding them that payment is overdue and of the consequences
of non-payment (except the timescales are different where fees are
payable under s.77(5A) - see 25.12.1 below. This notice is sent to the
last address specified for this purpose by the proprietor or, if no such
address was given, to the address for service which is entered in the
register. (Prior to 6 April 2017, this notice was sent to the address
specified on the last Form 12 filed or any address notified by the
proprietor since that Form was filed or, if no such address was given,
to the address for service which was entered on the register.)

### 25.12.1 {#section-8}

r.39(2) is also relevant.

In the situation where a European patent (UK) has been revoked by the
EPO Board of Appeal and is subsequently restored by the Enlarged Board
of Appeal, and renewal fees are payable under s.77(5A), the comptroller
must send such a notice within six weeks of either (i) the end of the
renewal period or (ii) the date on which the Office receives
notification from the EPO of the restoration of the patent, whichever
occurs later. s.25(4) 25.13 The period allowed for payment of a renewal
fee ([see 25.06-25.08](#ref25-06)) may be extended by up to six months
on payment of the fee appropriate for the length of extension sought and
completion of the appropriate part of Form 12. If the renewal fee is
paid within this extended period, the patent is treated as though it had
not expired (but see ss.58(5) and 62(2)). If however the renewal fee is
not paid by the end of six months from the time when it was due, the
patent is regarded as having expired on the anniversary of the filing
date, and is so advertised in the Journal. For example, if a patent is
filed on 10 May 2003 and granted on 17 June 2006, renewal fees are due
on 31 May each year, starting in 2007, but may be paid late any time up
to the end of November (with a late payment penalty). If a fee is not
paid for any given year, the patent will be deemed to cease on 10 May of
that year. The Register is endorsed "Patent ceased".

### 25.13 {#ref25-13}

The period allowed for payment of a renewal fee (see 25.06-25.08) may be
extended by up to six months on payment of the fee appropriate for the
length of extension sought and completion of the appropriate part of
Form 12. From 30 July 2020 to 31 March 2021 no late payment fee is
required when paying during the six-month extension period. If the
renewal fee is paid within this extended period, the patent is treated
as though it had not expired (but see ss.58(5) and 62(2)). If however
the renewal fee is not paid by the end of six months from the time when
it was due, the patent is regarded as having expired on the anniversary
of the filing date, and is so advertised in the Journal. For example, if
a patent is filed on 10 May 2003 and granted on 17 June 2006, renewal
fees are due on 31 May each year, starting in 2007, but may be paid late
any time up to the end of November (with a late payment penalty). If a
fee is not paid for any given year, the patent will be deemed to cease
on 10 May of that year. The Register is endorsed "Patent ceased".

### 25.13.1 {#section-9}

r.36(2) is also relevant.

If outstanding renewal fees due under s.77(5A) are not paid within
either the r.41A two-month period for paying the renewal fee following
the restoration or the six-month extension discussed above, the European
patent (UK) will be deemed to have ceased at the end of the final day of
the two-month period following the restoration by the Enlarged Board of
Appeal.

### 25.14 {#ref25-14}

When the renewal fee is paid in the extended period, the amount due is
that which was prescribed at the end of the normal period. The amount of
the fee for the extension is however that due at the time it is paid.

### 25.15 {#section-10}

r.41(2), r.41(3) is also relevant.

If the renewal fee has not been paid (together with the fee for any
extension) within six months of the end of the normal period allowed,
then within a further six weeks the comptroller must send to the
proprietor a further notice informing them that the patent has now
lapsed and informing them of the possibility of applying for restoration
(see s.28). This notice must be sent to the address specified by rule
39(3) ([see 25.12](#ref25-12)).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
