::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 69: Infringement of rights conferred by publication of application {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (69.01 - 69.08) last updated: July 2022.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 69.01

s.69(2)(a) is also relevant

This section provides that the publication of an application before
grant may give rise to a right to bring proceedings for infringement
though that right cannot be enforced until after grant. For the purposes
of s.69, grant occurs when notice of grant is officially published, [see
25.02](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-02).

### 69.02

s.74(7) is also relevant

Infringement proceedings under s.69 may be before the court or before
the comptroller and should follow the same procedure as for infringement
after grant, see the chapter on s.61. Where proceedings are pending in
the court under any of sections 58, 61, 69, 70-70F, 71 and 72, no
proceedings may be instituted before the comptroller under s.69 without
the leave of the court ([see
74.07](/guidance/manual-of-patent-practice-mopp/section-74-proceedings-in-which-validity-of-patent-may-be-put-in-issue/#ref74-07)).

### 69.03

In s.69, an application means one for a patent granted under the 1977
Act or one that is so treated, i.e. by virtue of s.78(1), s.79 or
s.89(1) in the case of applications for a European patent (UK),
international applications for a European patent (UK) and international
applications for a patent (UK) respectively. Similarly publication is
under s.16 or under the relevant provision of the EPC or PCT so that it
is treated under s.78(3)(d), s.79 or s.89B(2) as publication under s.16,
subject to the language requirements of s.78(7) (if in force), s.79(3)
and s.89B(3).

### 69.04

\[deleted\]

### 69.05

s.130(7) is also relevant

Section 69 is one of the sections so framed as to have, as nearly as
practicable, the same effect in the UK as the corresponding provisions
of the EPC, CPC and PCT. The provisions in question are EPC Article 67,
CPC Article 34 (renumbered as Article 32 \[1989\]) and PCT Article 29.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 69(1)**
  Where an application for a patent for an invention is published, then, subject to subsections (2) and (3) below, the applicant shall have, as from the publication and until the grant of the patent, the same right as he would have had, if the patent had been granted on the date of the publication of the application, to bring proceedings in the court or before the comptroller for damages in respect of any act which would have infringed the patent; and (subject to subsections (2) and (3) below) references in sections 60 to 62 and 66 to 68 above to a patent and the proprietor of a patent shall be respectively construed as including references to any such application and the applicant, and references to a patent being in force, being granted, being valid or existing shall be construed accordingly.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 69.06 {#ref69-06}

s.69(2)(a) is also relevant

From publication of the application until grant, the applicant has the
same right as they would have had, if the patent had been granted on the
day of publication, to sue for damages in respect of any act which would
have infringed the patent. This allows proceedings to be brought (after
grant) to recover damages in respect of such pre-grant infringements,
subject to certain restrictions - see [69.07](#ref69-07) and
[69.08](#ref69-08). In such proceedings, ss.60 to 62 and 66 to 68 apply
to applications as if they were patents. In [Spring Form Inc v Toy
Brokers Ltd & Ors \[2001\] EWHC 535
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2001/535.html){rel="external"}
the court confirmed that, despite the explicit reference only to
damages, in principle an account of profits is available as a remedy in
respect of infringing actions under s.69.

  -----------------------------------------------------------------------
   

  **Section 69(2)**

  The applicant shall be entitled to bring proceedings by virtue of this
  section in respect of any act only -\
  (a) after the patent has been granted; and\
  (b) if the act would, if the patent had been granted on the date of the
  publication of the application, have infringed not only the patent, but
  also the claims (as interpreted by the description and any drawings
  referred to in the description or claims) in the form in which they
  were contained in the application immediately before the preparations
  for its publication were completed by the Patent Office.
  -----------------------------------------------------------------------

### 69.07 {#ref69-07}

The applicant is entitled to bring such proceedings only if the act in
question would have infringed not only the granted patent (if grant had
been on the day of publication of the application) but also the claims
in the form extant immediately before preparations for publication were
completed. For that purpose, those claims are to be construed as
interpreted by the description etc, ie in the same way as under
s.125(1), [see
125](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 69(3)**
  Section 62(2) and (3) above shall not apply to an infringement of the rights conferred by this section, but in considering the amount of any damages for such an infringement the court or the comptroller shall consider whether or not it would have been reasonable to expect from a consideration of the application as published under section 16 above, that a patent would be granted conferring on the proprietor of the patent protection from an act of the same description as that found to infringe those rights, and if the court or the comptroller finds that it would not have been reasonable, it or he shall reduce the damages to such an amount as it or he thinks just.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 69.08 {#ref69-08}

Section 62(2) and (3) impose restrictions on the recovery of damages for
infringement where there is late renewal of the patent or amendment of
the specification of the patent, and are not applicable here. However,
damages are reduced by the court or the comptroller if it is considered
that it would not have been reasonable to expect from the application as
published that a patent conferring protection against the infringing act
would be granted.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
