::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 14: The application {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (14.01 - 14.209) last updated: April 2024.
:::

::: govuk-grid-column-two-thirds
::: {#default-id-c972c5d5 .gem-c-accordion .govuk-accordion .govuk-!-margin-bottom-6 module="govuk-accordion gem-accordion ga4-event-tracker" ga4-expandable="" anchor-navigation="true" show-text="Show" hide-text="Hide" show-all-text="Show all sections" hide-all-text="Hide all sections" this-section-visually-hidden=" this section"}
::: govuk-accordion__section
::: govuk-accordion__section-header
## [Section 14: The application]{#default-id-c972c5d5-heading-1 .govuk-accordion__section-button} {#section-14-the-application .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Section 14: The application\",\"index_section\":1,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-1 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-1" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Section 14: The application\",\"index_section\":1,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.01

This section prescribes the manner of making an application, its form
and content, the requirements which must be fulfilled by the contents,
and provides for withdrawal of the application. Specific provisions
relating to some of these matters are also laid down in rr.12-16.

### The Regulatory Reform (Patents) Order 2004 and Patents (Amendment) Rules 2004: Coming into Force and Transitional provisions

### 14.01.1

The form and contents of an application as set out in s.14(1) and the
conditions governing withdrawal of applications at the request of the
applicant under s.14(9) were deregulated by the Regulatory Reform
(Patents) Order 2004 ("the 2004 Order") in conjunction with the Patents
(Amendment) Rules 2004 ("the 2004 Rules"). The 2004 Order and the 2004
Rules incorporated the principles of Articles 5 and 6 of the Patent Law
Treaty, and both statutory instruments came into force on 1 January
2005. The Patents Rules 1995 as amended have been replaced entirely from
17 December 2007 by the Patents Rules 2007.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Code of practice]{#default-id-c972c5d5-heading-2 .govuk-accordion__section-button} {#code-of-practice .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Code of practice\",\"index_section\":2,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-2 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-2" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Code of practice\",\"index_section\":2,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.01.2

The Code of Practice (the first part of which can be found below)
identifies best practice points for patent applicants and agents, which
if followed widely will lead to savings and efficiencies in the Office,
and consequently to better service and better value. Examiners and other
Office officials may on occasion draw the Code to the attention of the
applicant or agent, and may ask for it to be complied with before the
case is processed further if that would be more efficient.

### 14.01.3

However, it is not to be expected that best practice can always be
adhered to. The Office has no right to demand compliance with the Code
of Practice because the Code is advisory only and has no legal force.

### 14.01.4

If an application does not comply with one of the points of the Code of
Practice then this does not necessarily justify an objection under the
Patents Act or Rules. For example, an application having more than one
independent claim in one category would not comply with code point 1e,
but this would not usually justify an objection under s.14(5)(b). When
drawing attention to non-compliance with the Code of Practice, examiners
should therefore make it clear whether they are also raising a formal
objection under the Act or Rules.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Code of practice for applicants and agents]{#default-id-c972c5d5-heading-3 .govuk-accordion__section-button} {#code-of-practice-for-applicants-and-agents .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Code of practice for applicants and agents\",\"index_section\":3,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-3 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-3" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Code of practice for applicants and agents\",\"index_section\":3,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Drafting and filing patent applications

This part of the Code of Practice for applicants and agents relates to
the drafting and filing of patent applications under the Patents Act
1977. The quality of the application that is received by the Office
determines in large part the effort that has to be expended during the
statutory search and examination, in putting the application in a state
to be granted, and is thus a central factor for the Code. The Code does
not seek to eliminate the need for search and examination, but rather to
optimise drafting towards UK law and practice and to avoid formulations
that are clearly problematic for the UK search and examination. When
drafting specifications for filing at this Office the aim should
therefore be to adhere to the Code points below.

This part of the Code also deals with presentational and procedural
matters that have to be checked by the Office after the initial filing.
The Office tries to keep red tape to a minimum, but some formalities are
necessary for the system to work properly. There is substantial scope
for efficiency savings if the best practices set out in this Code are
known and followed
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Point 1: The claims as filed: structure]{#default-id-c972c5d5-heading-4 .govuk-accordion__section-button} {#point-1-the-claims-as-filed-structure .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Point 1: The claims as filed: structure\",\"index_section\":4,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-4 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-4" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Point 1: The claims as filed: structure\",\"index_section\":4,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
The claims as filed should be structured to have:

-   a\) One independent claim defining all the technical features
    essential to the invention or inventive concept. Inessential or
    optional features should not be included in this claim; consequently
    terms such as "preferably", "for example", or "more particularly"
    should not be included, as the feature being introduced by such
    terms does not restrict the scope of the claim in any way. The
    independent claim should include sufficient details of
    interrelationship, operation or utility of the essential features to
    enable the scope of the claim to be determined [(see
    14.110.1)](#ref14-110); and

-   b\) dependent claims incorporating all the features of the
    independent claim and characterised by additional non-essential
    features [(see 14.134)](#ref14-134).

In addition:

-   c\) further independent claims are only justified where the
    inventive concept covers more than one category, e.g. apparatus,
    use, process, product [(see 14.159 to 14.168)](#ref14-159),
    complementary versions within one category, e.g. plug and socket,
    transmitter and receiver, which work only together [(see
    14.161)](#ref14-161), or distinct medical uses of a substance or
    composition [(see 14.162)](#ref14-162).

Therefore claims as filed should not, where it might have been avoided,
contain:

-   d\) multiple unrelated inventions that would clearly give rise to a
    plurality objection [(see 14.157.1)](#ref14-157)

-   e\) multiple independent claims in any one category, even if only
    one inventive concept is present [(see 14.110.1 and
    14.140)](#ref14-110)

-   f\) claims of a total number or complexity not justified by the
    nature of the invention [(see 14.110.1 and 14.140)](#ref14-110)

-   g\) claims which are in principle unsearchable by reason of the
    number of alternatives embraced, or the choice of characterising
    parameters or desiderata [(see 14.110 and 14.133)](#ref14-110)

-   h\) dependent claims that are not fully limited by the terms of the
    preceding independent claim, e.g. dependent claims which omit,
    modify or substitute a feature of an independent claim [(see
    14.134)](#ref14-134)

If these points are not met on filing, suitable claim amendments should
be filed if the search examiner requests them to enable search ([see
17.94.9](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94-9)
and
[17.108](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-108))
Amendments may be required by the substantive examiner before examining
such claims ([see
18.43.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-43-1)
and
[18.39](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-39)).

The above points also apply at the entry into the UK national phase of
international applications under the Patent Cooperation Treaty.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Point 2: The claims as filed: patentability]{#default-id-c972c5d5-heading-5 .govuk-accordion__section-button} {#point-2-the-claims-as-filed-patentability .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Point 2: The claims as filed: patentability\",\"index_section\":5,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-5 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-5" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Point 2: The claims as filed: patentability\",\"index_section\":5,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
-   a\) the claims as filed should not, where it might have been
    avoided, define an invention which is clearly excluded from being
    patentable under the Act ([see 1.07 to
    1.40.4](/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-07)).

If this point is not met on filing, suitable claim amendments should be
filed if the search examiner requests them to enable search ([see
17.94.9](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94)).
Examiners may either require amendment before searching or examining
such claims, or may issue a report that a search would serve no useful
purpose ([see 17.94-17.94.10 and
17.98](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Point 3: Other aspects of the specification as filed]{#default-id-c972c5d5-heading-6 .govuk-accordion__section-button} {#point-3-other-aspects-of-the-specification-as-filed .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Point 3: Other aspects of the specification as filed\",\"index_section\":6,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-6 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-6" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Point 3: Other aspects of the specification as filed\",\"index_section\":6,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
-   a\) the use of a compact style of consistory clause, which in the
    description defines the invention by reference to the claims, is
    strongly encouraged [(see 14.148)](#ref14-148).

-   b\) trade marks are an indication of the origin rather than the
    composition or content of goods, and should not be used in patent
    applications where a generic term can be used instead. Trade marks
    are only permitted in claims where it can be shown that their use is
    unavoidable and does not introduce ambiguity [(see
    14.137)](#ref14-137). Where marks that are registered are mentioned,
    they should be acknowledged as such [(see 14.100 and
    14.101)](#ref14-100). If a trade mark is not registered, its owner
    should be indicated [(see 14.100)](#ref14-100)

-   c\) passages which confuse the scope of the invention [(see
    14.139.1)](#ref14-139) or claims that are unspecific (e.g. those
    claiming "Any novel matter..." )[(see 14.139)](#ref14-139) should
    not be filed.

-   d\) the specification should be clear and precise. It should not
    repeat matter unnecessarily, nor should it contain matter which is
    irrelevant to the invention - for example the complete details of
    well-known ancillary features need not be given [(see
    14.74)](#ref14-74)

-   e\) only the most relevant prior art should be discussed [(see 14.91
    and 14.92)](#ref14-91). For example, a few documents which
    illustrate how a problem has previously been approached could be
    discussed with a view to distinguishing the invention in suit from
    them or illustrating its advantages.

-   f\) If the specification has been drafted abroad then it should be
    adapted to comply with sections 14(3) and 14(5) of the Patents Act
    1977 and the Code points before it is filed here. Action should be
    taken to ensure that it is written in a reasonable standard of
    English before it is filed.

-   g\) the patent specification is intended to describe the invention
    precisely and it is vital that the meaning of all abbreviations and
    words used is clear and unambiguous. Words, abbreviations or
    acronyms used in the specification that are new, may be considered
    to be jargon, or may be at risk of being ambiguous (for example
    because their meaning is not yet generally settled in the field in
    question) should not be used unless unavoidable, in which case a
    definition should be given in the specification.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Point 4: The abstract]{#default-id-c972c5d5-heading-7 .govuk-accordion__section-button} {#point-4-the-abstract .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Point 4: The abstract\",\"index_section\":7,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-7 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-7" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Point 4: The abstract\",\"index_section\":7,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
-   a\) the abstract and abstract title should meet the requirements of
    rule 15 of the Patents Rules 2007, concisely summarising the matter
    contained in the specification, including the technical field,
    distinctive technical features, and principal use of the invention
    [(see 14.173 to 14.184)](#ref14-173)

-   b\) an abstract having wording that merely reproduces an independent
    claim will not comply with this Code and may be rejected on the
    ground of non-compliance with the Rules [(see 14.184)](#ref14-184)
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Point 5: Forms and formalities on filing]{#default-id-c972c5d5-heading-8 .govuk-accordion__section-button} {#point-5-forms-and-formalities-on-filing .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Point 5: Forms and formalities on filing\",\"index_section\":8,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-8 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-8" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Point 5: Forms and formalities on filing\",\"index_section\":8,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
-   a\) the presentation of the application (including drawings) should
    meet the requirements of rule 14 of the Patents Rules 2007 ([see
    14.26 - 14.57](#ref14-26)). For example, documents must be legible
    and capable of reproduction, and the margins should be kept clear
    and free from headers or footers denoting references or other matter
    [(see 14.28)](#ref14-28).Shading is allowed in drawings so long as
    it does not obscure any detail in the drawings. In addition, a
    specification may include photographs if they are clear enough to be
    reproducible [(see 14.41)](#ref14-41).

-   b\) an official form should be used where required by the Rules, and
    it should be completed fully and accurately ([see 14.04](#ref14-04).
    for the application form).

-   c\) supporting documents should also be provided where appropriate;
    for example a covering letter where some explanation is needed.

-   d\) to allow us to direct incoming items quickly to the right place
    all appropriate identifiers should be given:

    -   form 1 should give ADP number(s) and customer reference;

    -   forms and letters filed after Form 1 should give application
        number and (optionally) customer reference;

    -   the payment page incorporated into each fee-bearing form should
        give contact details and indicate the method of payment;

    -   bank transfers should state the application or patent number,
        or, if not known, the reason for payment e.g. search fee.

In correspondence with applicants and their representatives, the
Intellectual Property Office will use customer references if given, and
will tell agents the applicant's ADP numbers ([see 14.04.1 and
14.04.5](#ref14-04)).

-   e\) if it is critical that an application be filed within a certain
    time window such as the twelve months of the Paris Convention, the
    application should be filed with sufficient time remaining for any
    fatal defects to be detected and put right.

-   f\) \[Deleted\].

-   g\) electronic filing of applications is strongly encouraged;
    official fees are reduced for certain electronically-filed
    documents. See [Apply for a UK
    patent](https://www.gov.uk/apply-for-a-patent).

\[Deleted\]

  -----------------------------------------------------------------------
   

  **Section 14(1)**

  Every application for a patent -\
  (a) shall be made in the prescribed form and shall be filed at the
  Patent Office in the prescribed manner;\
  (b) \[repealed\]
  -----------------------------------------------------------------------

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 14(1A)**
  Where an application for a patent is made, the fee prescribed for the purposes of this subsection ("the application fee") shall be paid not later than the end of the period prescribed for the purposes of section 15(10)(c) below
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

s.119 CoP is also relevant

### 14.02 {#ref14-02}

The prescribed form of the application is laid down in the rules. The
filing fee was abolished by the 2004 Order, which replaced it with the
application fee specified in s.14(1A). An application may be filed by
hand, by post, or by email ([to
forms@ipo.gov.uk](mailto:forms@ipo.gov.uk)). As of 31st October 2020 the
office can no longer accept documents sent by facsimile (fax)..
Alternatively, an application may be submitted online via the Office
website, or using the secure online filing system provided by EPO Online
Services if the filer has completed the enrolment procedure for that
service.. The filing date accorded to an application is the date of
receipt, if the Office is open to receive that class of document; if
not, it is the next working day (see [also 14.29](#ref14-29)).
Confirmation copies are only required if requested by the Office e.g. if
there is a problem with the clarity/completeness of the faxed document
received. Confirmatory copies should be prominently marked "In
confirmation of fax". [See also
22.07](/guidance/manual-of-patent-practice-mopp/section-22-information-prejudicial-to-national-security-or-safety-of-public/#ref22-07))
for filing considerations relating to documents which might include
information of relevance to national defence or security. The hours and
days when filings may be made are governed by s.120. Under r.97 of the
Patents Rules 1995, an application filed by post was deemed to have been
filed when the letter containing it would be delivered in the ordinary
course of post. However, the Patents Rules 2007 do not include a rule
relating to postal deeming and therefore any document posted on or after
17 December 2007 will be accorded the date of receipt it is actually
received in the Office.

  -----------------------------------------------------------------------
   

  **Section 14(2)**

  Every application for a patent shall contain -\
  (a) a request for the grant of a patent;\
  (b) a specification containing a description of the invention, a claim
  or claims and any drawing referred to in the description or any claim;
  and\
  (c) an abstract;\
  but the foregoing provision shall not prevent an application being
  initiated by documents complying with section 15(1) below.
  -----------------------------------------------------------------------

### 14.03 {#section-5}

While an application must eventually contain all the items referred to
in s.14(1A) and this subsection, the final proviso of s.14(2) makes
clear that for an application to be initiated and given a filing date it
is sufficient for it to comply with the conditions of s.15(1), ([see
15.02-15.06](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-02)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Request for the Grant of a Patent]{#default-id-c972c5d5-heading-9 .govuk-accordion__section-button} {#request-for-the-grant-of-a-patent .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Request for the Grant of a Patent\",\"index_section\":9,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-9 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-9" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Request for the Grant of a Patent\",\"index_section\":9,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04 {#ref14-04}

r.12(1) and r.25(1) is also relevant

The request must be made on Patents Form 1; this is designated a formal
requirement (see 17.07).

Considering each part of the form:-
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [1. Reference Number]{#default-id-c972c5d5-heading-10 .govuk-accordion__section-button} {#reference-number .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"1. Reference Number\",\"index_section\":10,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-10 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-10" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"1. Reference Number\",\"index_section\":10,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### CoP

### 14.04.1 {#ref14-04-1}

Although there is no obligation to provide such a reference, it is
clearly in the applicant's interest to do so, since until an application
number has been accorded by the Office there is no reliable way of
identifying a particular application. It may also be useful should any
confusion arise over the application numbers accorded to several cases
filed together. If provided, the Office will use the applicant's
reference number in correspondence related to the application.

### 14.04.2 \[deleted\]

### 14.04.3 {#ref14-04-3}

\[deleted\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [2. Applicant's Details]{#default-id-c972c5d5-heading-11 .govuk-accordion__section-button} {#applicants-details .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"2. Applicant’s Details\",\"index_section\":11,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-11 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-11" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"2. Applicant’s Details\",\"index_section\":11,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### CoP

### 14.04.4 {#section-6}

r.12(2) and r.12(3) is also relevant

The applicant's name and address should be entered in Part 2 of the Form
([see
15.03](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-03)),
but failing that the Form must contain sufficient information to enable
the applicant to be contacted. Where a date of filing is accorded to an
application which does not give the applicant's name and address, the
applicant must be notified of the failure, and the comptroller may
refuse the application if the applicant fails to file their name and
address within two months of such a notification. Under r.108(1)
together with r.108(5) and (7), the two-month period for filing the
applicant's name and address may be extended at the comptroller's
discretion in tranches of 2 months, but no extension may be granted
after two months following the expiry of the period as prescribed or
previously extended - see 123.36.10-12. Where Parts 2 and 4 of the Form
do not, between them, contain enough information to enable the applicant
to be contacted, the contact information in Part 12 may suffice. \[If
contact details are provided but establishing contact is difficult,
advice should be sought from Legal Section.\]

### 14.04.5 {#ref14-04-5}

s.7(1) is also relevant

An application may be made by any person either alone or jointly with
another. ("Person" means either an individual or a corporate body, see
7.02). If the inventor is not also an applicant they should not be named
on Form 1.

### 14.04.6 {#section-7}

An applicant who is an individual should apply in their true name.
Exceptionally, a pseudonym may be used if it is well established and is
customarily used by the individual for banking and other business
purposes. The name must be given in full, the surname or family name
being underlined. Letters or statements denoting academic or
professional qualifications may appear after the name. A statement of
nationality or occupation is not required and should not be given. Once
a particular established name and signature has been used subsequent
business should not be effected by the same individual using a different
name or signature unless the name has changed, eg due to marriage. A
corporate body should be designated by its legal name. In the case of
either an individual or a corporate body, a business name or trading
style, eg "trading as XYZ", or a former name is not required and should
not be given. If known, the automatic data processing (ADP) number of
the applicant should be given for identification of the applicant; this
number will supplied to an applicant's agents.

### 14.04.7 {#section-8}

If an application is made on behalf of a Government Department, the
applicant should be stated to be the Secretary of State or Minister
responsible for the department, and not the individual holding such
office. Applications from police forces should be made by the relevant
Police Authority.

### 14.04.8 {#section-9}

Each applicant must give, in full, a permanent address, which may be
either a private or a business address. A c/o is not acceptable unless
it can be shown that it is a permanent address for the applicant which
the Office can rely on for communicating with the applicant, e.g. if, in
the case of a company, it is registered with Companies House as the
company's address. It is in the applicant's best interests to provide
the Office with a secure and reliable address. Standard abbreviations,
e.g. Rd, USA, are allowable.

### 14.04.9 {#section-10}

Since the United Kingdom does not accord diplomatic recognition to
Taiwan the statement "Republic of China" for the country of
incorporation or address of an applicant located in Taiwan cannot be
accepted and should be objected to. If the applicant disagrees they
should be advised that the onus is on them to ascertain from the Foreign
Office what is considered to be an acceptable designation.

### 14.04.10 {#ref14-04-10}

Where there is not enough space on the form for the details of more than
one applicant, any further names and addresses should be given on a
continuation sheet and the use of a continuation sheet should be
indicated in parts 9 of the form.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [3. Title of Invention]{#default-id-c972c5d5-heading-12 .govuk-accordion__section-button} {#title-of-invention .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"3. Title of Invention\",\"index_section\":12,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-12 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-12" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"3. Title of Invention\",\"index_section\":12,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
r.44(1)(d) is also relevant

### 14.04.11 {#section-11}

This is the title entered in the register and in the list of new
applications in the Journal. By virtue of s.14(1)(a) and r.12(1), a
title is a formal requirement, thus if there is no title given on Form 1
the applicant should be required to provide one, or to confirm that the
title, if any, on the specification is to be used on Form 1. (See also
[14.49- 51](#ref14-49) and
[19.24](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-24)).
The title should avoid disclosing the invention, since it is published
in the Journal prior to publication of the application under s.16(1).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [4. Address for Service Details]{#default-id-c972c5d5-heading-13 .govuk-accordion__section-button} {#address-for-service-details .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"4. Address for Service Details\",\"index_section\":13,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-13 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-13" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"4. Address for Service Details\",\"index_section\":13,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04.12 {#ref14-04-12}

If the applicant has appointed an agent their name should be given in
this part of Form 1. The agent need not be registered ([see
274.03](/guidance/manual-of-patent-practice-mopp/section-274-persons-permitted-to-carry-on-business-of-a-patent-agent/#ref274-03)).

\[If the agent's name is missing from Form 1, but it is clear from the
information supplied who the agent is, the agent's details should be
inserted on COPS by the Formalities Examiner, and Form 1 annotated
appropriately.\]

\[Deleted\]

### 14.04.13 {#ref14-04-13}

CoP and r.101(1) is also relevant

An agent filing an application is assumed to be duly authorised to act
for the applicant, and in general no specific evidence to this effect is
required. (The comptroller has the power to require an agent to produce
evidence, at any stage in proceedings, that they are authorised. This
will be done only if there is some reason for doubting the authority or
for requiring it to be confirmed). If however, subsequent to the filing
of Form 1, the applicant wishes to change their agent or an agent is
appointed for the first time, the incoming agent must file Patents Form
51. Notification on Form 51 should not be made if the (new) agent will
only perform administrative tasks such as paying renewal fees or
registering an assignment. If an applicant who has been represented by
an agent decides to dispense with the services of the agent and
represent themselves as a private applicant, then they should be advised
to provide written confirmation of this change. A copy of the
applicant's letter will then be sent to the former agent.

\[The Office is no longer prepared to retain general authorisations; if
one is filed it should be returned to the agent. If a specific
authorisation is filed it will, although redundant, be allowed to remain
on the file. \]

### 14.04.14 {#ref14-04-14}

r.103, r.104, CDPA s.281(5), PR Sch. 4 Parts 2 & 3 is also relevant

Every application must include an address for service, which must be in
the United Kingdom (including the Isle of Man), Gibraltar or the Channel
Islands. An address for service of an agent cannot be accepted in the
Channel Islands or Gibraltar unless they also have a residence or place
of business in the UK, or Isle of Man, ([see
281.04](/guidance/manual-of-patent-practice-mopp/section-281-power-of-comptroller-to-refuse-to-deal-with-certain-agents/#ref281-04)).
The address for service is treated for all purposes as the address of
the person concerned with the proceedings and all communications must be
addressed to the applicant at or c/o this address. Communication with
the applicant can only be through this address; if an agent has been
appointed and the applicant contacts the office direct they must be
advised to communicate through their agent. If there is no acceptable
address for service on an application and there is sufficient
information to contact the applicant, they should be contacted and be
directed to file an acceptable address for service within 2 months (this
period can be extended using Rule 108(2) and 108(3) -- [see
123.36.5](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36-5).
If an acceptable address is not provided within this time period, or
there is insufficient information to make contact with anyone to provide
such an address, the application is treated as withdrawn.

\[Where the address for service is not in the UK, Gibraltar or the
Channel Islands, a request for compliance with this requirement within 2
months should be made, with a warning that failure to comply will lead
to the application being withdrawn under r.104(4)(a)\]

\[If no address has been given at part 5, but it is clear from the
information given that an agent is acting for the applicant, the agent's
address will be entered on COPS by the formalities examiner, and Form 1
annotated appropriately. Likewise if no agent has apparently been
appointed and an address given in part 3 is within the United Kingdom,
Gibraltar or the Channel Islands, this will be entered as the address
for service on COPS.\]

\[Deleted\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [5. Declaration of Priority]{#default-id-c972c5d5-heading-14 .govuk-accordion__section-button} {#declaration-of-priority .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"5. Declaration of Priority\",\"index_section\":14,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-14 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-14" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"5. Declaration of Priority\",\"index_section\":14,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04.15 {#ref14-04-15}

r.6(1) & (4) is also relevant

If priority is claimed from an earlier application (see s.5), the date
and country of filing of the earlier application should normally be
declared at the time of filing the application in suit. The file number
of the earlier application should preferably also be stated if it is
available. A copy of the earlier application and any necessary
translation do not however need to be filed at this stage.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [6. Claiming an Earlier Application Date]{#default-id-c972c5d5-heading-15 .govuk-accordion__section-button} {#claiming-an-earlier-application-date .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"6. Claiming an Earlier Application Date\",\"index_section\":15,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-15 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-15" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"6. Claiming an Earlier Application Date\",\"index_section\":15,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04.16 {#section-12}

s.15(9) is also relevant

The number of the earlier application and its filing date should be
given. In the case of an application under s.15(9) (a divisional
application) the request for an earlier filing date must be made at the
time of filing the application in suit, ([see
15.18](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-18)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [7. Inventorship]{#default-id-c972c5d5-heading-16 .govuk-accordion__section-button} {#inventorship .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"7. Inventorship\",\"index_section\":16,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-16 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-16" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"7. Inventorship\",\"index_section\":16,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04.17 {#ref14-04-17}

s.13(2) is also relevant

Where all the applicants named in Part 2 of the Form 1 are also the
inventors, a mark should be made in the top "Yes" box. If one or more of
the applicants is not an inventor, the top "No" box should be marked
instead. Where there is one or more inventor whose name is not given as
an applicant in Part 2 of the Form, a mark should be placed the lower
"Yes" box in part 7 of the Form. It follows that the lower "No" box in
part 7 of the Form should only be marked in the case where all inventors
are named applicants and all named applicants are inventors.

### 14.04.17.1 {#section-13}

s.7(3) is also relevant

Since "inventor" means the actual deviser of the invention a corporate
body cannot claim to be an inventor. Where an applicant named in Part 2
of the Form is a corporate body and the top "Yes" has been marked in
Part 7, an objection should be raised.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [8. Application Fee]{#default-id-c972c5d5-heading-17 .govuk-accordion__section-button} {#application-fee .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"8. Application Fee\",\"index_section\":17,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-17 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-17" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"8. Application Fee\",\"index_section\":17,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04.18 {#section-14}

s.15(10)(c), r.22(2) and r.22(7) is also relevant

While the application fee may be paid at any time up to the expiry of
the period prescribed by r.22(7), nevertheless when it is paid at the
time of filing this fact should be indicated by placing a mark in the
"Yes" box in part 8 of the Form 1, and where it is not paid at the time
of filing it follows that the "No" box should be marked. If the
application fee is paid at a later date, a surcharge will be payable
(unless it is paid in the period 30 July 2020 to 31 March 2021).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [9. Documents making up the application]{#default-id-c972c5d5-heading-18 .govuk-accordion__section-button} {#documents-making-up-the-application .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"9. Documents making up the application\",\"index_section\":18,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-18 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-18" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"9. Documents making up the application\",\"index_section\":18,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04.19 {#section-15}

It should be noted that the information in the check list relates to the
application as filed; pages filed subsequently should not be indicated
in this check list.

### 14.04.19.1 {#section-16}

s.15(1)(c)(ii) and s.15(10)(b), r.22 is also relevant

Where the application is not accompanied by a description, and a
reference to an earlier application under s.15(1)(c)(ii) is relied upon
for qualifying for a date of filing, the application number, country of
filing and date of filing of that earlier application should be entered
in Part 9 of the Form. A reference must be substantiated by the filing
of a certified copy within the time specified by rule 22(3), which is
four months from the date of filing, and a description must be filed
within the time specified by rule 22(1), which is the same period as for
the claims and abstract and is the later of twelve months from the
earliest date or two months from the date of filing. Where both a
description is filed and the details of an earlier application are given
in Part 10 of the Form, the applicant (or their agent) should be
contacted to clarify under which of the (mutually exclusive) options of
s.15(1)(c) the application is made.

\[If the check list has not been completed, or if it has been completed
in a manner which is not consistent with what has been filed, the
formalities examiner should raise the matter with the agent or
applicant\].

### 10. Documents accompanying the application

### 14.04.20 {#ref14-04-20}

Documents or forms filed subsequently should not be indicated in this
check list.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [11. Request]{#default-id-c972c5d5-heading-19 .govuk-accordion__section-button} {#request .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"11. Request\",\"index_section\":19,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-19 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-19" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"11. Request\",\"index_section\":19,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04.21 {#section-17}

The form must be signed at this part either by the or each applicant or
by the agent who has filed the application. Where a signature is
missing, the formalities examiner should raise an objection requiring a
duly signed form to be submitted ([see
15A.20](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-20)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [12. Person to contact]{#default-id-c972c5d5-heading-20 .govuk-accordion__section-button} {#person-to-contact .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"12. Person to contact\",\"index_section\":20,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-20 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-20" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"12. Person to contact\",\"index_section\":20,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04.22 {#section-18}

If a contact name and telephone number is given any telephone enquiry
about how the form has been filled in should be made to the person
named.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Application of Rule 4(2)]{#default-id-c972c5d5-heading-21 .govuk-accordion__section-button} {#application-of-rule-42 .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Application of Rule 4(2)\",\"index_section\":21,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-21 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-21" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Application of Rule 4(2)\",\"index_section\":21,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.04.23 {#section-19}

r.4(2) is also relevant

Where an earlier version of Form 1 is used, and where the information on
that Form is sufficient for the application in question, no objection
should be raised and the Form should be regarded as a Form acceptable to
the comptroller and containing the information required.

\[ 14.05-14.24 Deleted \]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Amendment or correction of Form 1]{#default-id-c972c5d5-heading-22 .govuk-accordion__section-button} {#amendment-or-correction-of-form-1 .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Amendment or correction of Form 1\",\"index_section\":22,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-22 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-22" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Amendment or correction of Form 1\",\"index_section\":22,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.25 {#section-20}

r.49 is also relevant

If the applicant wishes to correct his or her name, then Form 20 is
required. However, correction of any of the other information given on
Form 1 merely requires notification in writing. ([See
19.05-19.12](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-05)
[32.06](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-06)
and
[117.17-117.21](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-17)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Form and presentation of documents]{#default-id-c972c5d5-heading-23 .govuk-accordion__section-button} {#form-and-presentation-of-documents .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Form and presentation of documents\",\"index_section\":23,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-23 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-23" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Form and presentation of documents\",\"index_section\":23,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.26 {#ref14-26}

r.25(1) and r.25(2) is also relevant

The size and presentation of the documents (including drawings) making
up an application are governed by r.14. The requirements of r.14(1), (2)
and (3) are designated as formal requirements (except for where an
application is delivered in electronic form or using electronic
communications, in which case only the requirements of rule 14(1) are
formal requirements); those that are not so designated are identified in
paragraphs [14.54](#ref14-54), [14.55](#ref14-55) and
[14.57](#ref14-57). Some general provisions regarding the content of the
specification are laid down in r.12(4)-(7); these are not designated
formal requirements.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Size and presentation]{#default-id-c972c5d5-heading-24 .govuk-accordion__section-button} {#size-and-presentation .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Size and presentation\",\"index_section\":24,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-24 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-24" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Size and presentation\",\"index_section\":24,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.27 {#ref14-27}

PR Sch. 2 part 1, PR Sch. 2 parts 1-3, CoP is also relevant

All documents making up an application must be on A4 matt white paper.

### 14.28 {#ref14-28}

For drawings, the minimum margins must be 20mm at the top and the left
hand side, 15mm at the right hand side and 10mm at the bottom. There
must not be any frames (lines surrounding matter). For documents other
than drawings the minimum margins must be 20mm at the top, right, left
and bottom. The margins of all documents should be completely blank,
even from case reference details; line numbers and page numbers are
regarded as being part of the text rather than being in the margin.

\[It will not be necessary to object to de minimis deviations from the
prescribed minima if it is certain that printing and binding of the
document will not be affected\].

### 14.29 {#ref14-29}

PR Sch. 2 part 1 para. 2 is also relevant

The contents of all documents, including drawings, making up an
application or replacing such documents must be suitable for
reproduction.

\[Deleted\]

### 14.30 {#ref14-30}

PR Sch. 2 parts 1-3 is also relevant

The description, claims and abstract must use at least 1½ line spacing
and the capital letters in any typeface or font used must be more than
2mm high.

\[If the specification of a private applicant's case does not meet the
r.14 requirements the applicant should be given the opportunity to
overcome any deficiency. In the event that the applicant does not comply
with an initial request and providing that there are no other
outstanding requirements, the specification should be retyped in the
Office with the approval of the Formalities Manager. The retyped
application should be accompanied by a suitable instruction for
publication. The applicant should also be advised in writing that their
application has been retyped and given four weeks from the date of the
letter being issued in which to object. If there is no objection, the
application can be allowed to proceed to A-publication.\]

### 14.31 \[deleted\] {#ref14-31}

### 14.32 {#section-21}

PR 1995, r.20(2) is also relevant

The specification (including drawings) and abstract, and any replacement
sheets, must be filed in duplicate for cases filed before 26 June 2006,
but there is no need to do so for cases filed after this date. Carbon
copies are acceptable provided that the requirements set out in Schedule
2 are met

### 14.33 {#ref14-33}

PR Sch. 2 paras. 8 & 13 is also relevant

The request for grant (Form 1), the description, the claims, the
drawings and the abstract must each begin on a new sheet. The pages of
the description, claims and abstract are normally secured, eg by
stapling, in the top left-hand corner; the request for grant and the
drawings normally remain as separate sheets.

### 14.34 {#ref14-34}

PR Sch. 2 paras.4, 5, 12 & 14 is also relevant

The pages of the description and claims must be numbered consecutively
in a single series. This is so even when the claims and/or the abstract
are filed later than the description. The sheets of drawings must also
be numbered consecutively in a single series, and the drawings
themselves must be numbered consecutively in a single series. Where a
sequence listing is set out at the end of the application, it must be
numbered consecutively in a separate series.

\[When re-numbering of pages is required as a consequence of addition or
excision of pages, this will be carried out by the formalities examiner
before grant. Replacement pages therefore should not be required.
(Numbering with a letter suffix (eg page 2a) is not satisfactory and
should be corrected (together with deletion of any explanatory note such
as "page 2a follows" on a preceding page) before an application is sent
for grant). The formal re-numbering will then be effected in Publishing
Section prior to B publication. No objection should be raised to the
absence of line numbering.\]

### 14.35 {#section-22}

PR Sch. 2 para 6 is also relevant

For documents other than drawings, page numbers must be located at the
top or bottom of the page (but not in the margin) in the centre; the
numbers are regarded as part of the text rather than as being in the
margin.

\[If page numbers are in the wrong place and there is a space in the
right place, the amendment should be made by the formalities examiner by
deleting and re- entering without making an objection. Line numbering
may be ignored when measuring margins. \]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Alterations]{#default-id-c972c5d5-heading-25 .govuk-accordion__section-button} {#alterations .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Alterations\",\"index_section\":25,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-25 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-25" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Alterations\",\"index_section\":25,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.36 {#section-23}

PR Sch. 2 part 1 is also relevant

All documents, including drawings, making up an application or replacing
such documents must be suitable for reproduction and must not include
frames (lines surrounding matter).

### 14.37 {#ref14-37}

Alterations may thus be allowed if they can be regarded as de minimis.
Any such alterations in manuscript should be neat and printed rather
than written. They must not extend into the minimum margins and they
must not be so placed as to hinder the normal scanning of the page by
the reader. They must also be in black ink - blue ink is not acceptable
as it cannot be reproduced reliably.

\[In applying the de minimis rule the overriding consideration is that
the resulting document must, in all respects, be suitable for direct
reproduction and that the legibility of the text must not be impaired by
any alterations. Further, the number of alterations and the manner of
their execution should not give the page an untidy appearance. The
determination of where to draw the line must be left to good sense. As a
guide it may be taken that if, say, one or two words of a claim are
changed, the consequential manuscript amendment of these words whenever
they occur throughout the description could be permitted. On the other
hand, a manuscript amendment of several lines should not be regarded as
de minimis. Manuscript insertions made by the examiner should be
printed, but objections should not be raised if the applicant or agent
writes rather than prints provided the writing is neat and clearly
legible. \]

### 14.38 {#section-24}

The question as to what is the number and nature of deletions that would
be considered reasonable is a matter for judgement; it should be borne
in mind that the aim is to obtain a document which is reasonably free of
deletions. It may be reasonable to delete the same word or part of a
word throughout the specification, or to delete a few words, all or part
of a sentence or a short paragraph on a single page. A single deletion
comprising most of a page is not considered reasonable unless it is done
in a manner leaving the deleted area blank. Nor is it considered
reasonable to have many deletions on a single page, or deletions which
break up the text in a manner making reading awkward. No objection is
raised to otherwise reasonable deletions occurring on several pages so
long as the total number remains moderate having regard to the length of
the specification. While the extent of deletions may not be such as to
require objection the subsequent inclusion of further deletions may not
be allowable.

### 14.39 {#ref14-39}

Whilst the overriding consideration is that the specification shall be
capable of being directly reproduced, there is a further consideration
that any deletions should not either by their number or the manner of
their execution spoil the appearance of the finished document. Deletions
should preferably be effected by erasure or obliteration - visible or
"invisible". Striking-out should be clear and neat; a straight-edge
should be used, except for crossing out single letters. Deletions should
be made in black ink and be such as to ensure satisfactory reproduction.
There must be no uncertainty as to the extent of the deletion.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Further requirements for drawings]{#default-id-c972c5d5-heading-26 .govuk-accordion__section-button} {#further-requirements-for-drawings .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Further requirements for drawings\",\"index_section\":26,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-26 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-26" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Further requirements for drawings\",\"index_section\":26,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.40 {#ref14-40}

PR Sch. 2 para.18 is also relevant

Drawings must not only be such as to permit direct reproduction, as laid
down in paragraph 2 of Schedule 2, but the scale of the drawings and the
distinctness of their graphic execution must be such that it would still
be clear if it were reduced by linear reduction to two thirds of its
original size. Since the drawings when reproduced may be of a different
size than when filed, no scale should be specified in words and no
dimensions should be given in the drawings. If it is considered
desirable, a scale or other reference for making reference may be
included, but it must be represented diagrammatically.

### 14.41 {#ref14-41}

PR Sch.2 part 3 is also relevant

Drawings should comprise black lines, but may also include shading so
long as the shading does not obscure detail within the drawings.
Cross-sections should be indicated by hatching. A specification may
include photographs, however as with all parts of the specification,
they must be clear enough for reproduction. Colour drawings or
photographs are not allowable.

### 14.42 {#section-25}

PR Sch.2 para.12 is also relevant

Figures of the drawings should be numbered consecutively in a single
series, independently of the numbering of the sheets.

### 14.43 {#section-26}

PR Sch. 2 para. 20 is also relevant

All numbers, letters and reference signs on the drawings should be
simple and clear; brackets, circles or inverted commas should not be
used in association with numbers and letters. The capital letters in any
typeface or font used in any drawing must be more than 3mm high. The
Latin, and, where customary, the Greek alphabet should be used.

### 14.44 {#ref14-44}

The drawings should not contain extensive textual matter. A few words
may be allowed as aiding understanding of the drawings. In the case of
electrical circuits and block schematic or flow sheet diagrams, a few
short catchwords are allowable and may be desirable. If extensive text
is present, such that the sheet may be considered to constitute part of
the description, then objection should be raised under r.14(3) together
with paragraph 19 of schedule 2, which states that a drawing must not be
included in the description.

### 14.45 {#section-27}

The title of the invention, and the applicant's or agent's name, should
not appear on the drawings.

### 14.46 Flow sheets and diagrams are considered to be drawings. {#flow-sheets-and-diagrams-are-considered-to-be-drawings}
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Language]{#default-id-c972c5d5-heading-27 .govuk-accordion__section-button} {#language .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Language\",\"index_section\":27,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-27 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-27" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Language\",\"index_section\":27,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.47 {#section-28}

r.14(1) r.113(1) see is relevant

Apart from the certified copies of priority applications required by
r.8, all documents (including drawings) making up an application, or
replacing such documents, must be in English or Welsh. Where the
original document is not in English or Welsh this requirement can be met
by the foreign-language document being accompanied by a translation into
English.

### 14.47.1 {#section-29}

r.12(8) r.12(9) is also relevant

Not withstanding the requirement to file an application in English, an
application can qualify for a date of filing if the description (or what
appears to be the description) is in a foreign language, providing that
the indication that a patent is sought and the identity of the applicant
(or the means of contacting them) are in English. Where a date of filing
is accorded to an application comprising a foreign-language description,
the applicant must be notified of the failure to comply with r.14(1).
The comptroller may refuse the application if the applicant fails to
file a description in English or Welsh within two months of such a
notification (extendable at the discretion of the comptroller under
r.108(1) together with r.108(5)-(7)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Content of the application]{#default-id-c972c5d5-heading-28 .govuk-accordion__section-button} {#content-of-the-application .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Content of the application\",\"index_section\":28,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-28 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-28" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Content of the application\",\"index_section\":28,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.48 {#ref14-48}

r.12(4) r.12(7) is also relevant

The specification should state the title of the invention and should
continue with the description, claim or claims and any drawing referred
to in the description or any claim, in that order. Where the
specification includes drawings, the description must include a list of
drawings briefly describing each of them. The claims should be headed in
such a way that the commencement of the claims is clearly identifiable,
eg by the title "Claims". In [R v Comptroller-General of Patents, ex
parte Penife International Ltd \[2004\] RPC
37](https://doi.org/10.1093/rpc/2004rpc37){rel="external"} which
considered the requirement of Art.11(1) of the PCT to file claims with
the application, it was held that a consistory clause setting out the
scope of the invention was part of the description and was not a claim.

\[There is no prescribed preamble to the claims. Although the simple
title "Claims" is preferred, no objection should be raised to any title
which serves the required purpose. The title "What I (or we) claim is",
(as used under the 1949 Act) is allowable. \]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Title]{#default-id-c972c5d5-heading-29 .govuk-accordion__section-button} {#title .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Title\",\"index_section\":29,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-29 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-29" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Title\",\"index_section\":29,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.49 {#ref14-49}

r.12(6) is also relevant

The title must be short and indicate the matter to which the invention
relates. If it is not in agreement with that given on Form 1, (see
14.04.11), or even if there is no title on the specification, the
specification as filed should be published under s.16(1).

\[The maximum length of a title permitted by COPS on the computerised
register is 158 characters . If a title to be entered on the Register
exceeds this the Office will amend the title accordingly (see [also
18.42](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-42)
[18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86)
and [2.40 of the Formalities
Manual](https://www.gov.uk/guidance/formalities-manual-online-version/chapter-2-request-for-grant-of-a-patent-form-1#ref2-40)
\].

### 14.50 {#section-30}

The title should not contain a fancy or trade name, a person's name, the
word "patent" or the abbreviation "etc" - whatever is intended to be
covered by this later term should be indicated more explicitly, or the
words "and the like" may, if appropriate and clear in context, be used.

### 14.51 {#ref14-51}

\[deleted\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Some requirements for the technical content]{#default-id-c972c5d5-heading-30 .govuk-accordion__section-button} {#some-requirements-for-the-technical-content .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Some requirements for the technical content\",\"index_section\":30,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-30 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-30" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Some requirements for the technical content\",\"index_section\":30,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.52 {#section-31}

PR Sch. 2 para.19 is also relevant

The request for grant, the description, the claims and the abstract must
not contain drawings; any drawings forming part of the specification
must be on sheets which are separate from the text and numbered as a
separate series.

### 14.53 {#section-32}

r.13(7), PR Sch .2 para. 5 is also relevant

The description, the claims and the abstract may contain chemical or
mathematical formulae. If it is considered necessary an applicant may be
required to file a copy of such formulae prepared in the manner
prescribed for drawings. For applications which include a sequence
listing, this listing is considered to form part of the description
(rather than the drawings) and therefore may be inserted at any point in
the description. These would then be subject to excess pages fees as
they are part of the description. It is usual, however, for the sequence
listing to be placed at the end of the description and before the
claims. Alternatively, a sequence listing may be set out at the end of
the application, in which case it must be numbered consecutively in a
separate series to the description and claims, and rule 12(4) does not
apply. In this case, the sequence listing would not be subject to excess
pages fees. See
[15A.04](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-04)
for practice at preliminary examination stage when a sequence listing
has not been filed.

\[Rule 13(6) requires that, if it is reasonably possible, a sequence
listing shall be delivered in electronic form or using electronic
communications, even where the patent application is not delivered in
electronic form or using electronic communications.\]

### 14.54 {#ref14-54}

PR Sch. 2 para. 22, r.25(1) r.14 is also relevant

The description and the abstract may contain tables. The claims may
contain tables of information only if the comptroller agrees. This is
one of the requirements set out in part 4 of Schedule 2 and is therefore
not a formal requirement. The question as to whether tables in a claim
are allowable is therefore a matter for the substantive examiner, and no
objection should be raised at an earlier stage.

### 14.55 {#ref14-55}

PR Sch. 2 part 4 r.25(1) r.14 is also relevant

The terminology and any references used must be consistent throughout
the application for a patent. Only technical terms, signs and symbols
which are generally accepted in the field in question may be used. The
same features should be denoted by the same reference sign throughout
the application. References must only be included in the drawing(s)
where they are mentioned in either the description or the claims. These
are not formal requirements; hence amendment to meet them should not be
required at the preliminary examination stage.

### 14.56 {#section-33}

PR Sch.2 para. 24-25 is also relevant

Where units of measurement used in the application are not standard
international units of measurement, the equivalent standard
international units of measurement must be provided, and where no
international standard exists, units must be used which are generally
accepted in the field. Temperatures should be given in degrees Celsius
(centigrade), except that as a matter of practice, degrees Kelvin are
acceptable to express cryogenic or colour temperatures. There is,
however, no objection to non- standard units used in a statement of
prior art by way of direct quotation.

::: call-to-action
\[Council Directive 80/181/EEC (as amended by Council Directive
89/617/EEC and Directive 1999/103/EC of the European Parliament and of
the Council) was implemented into UK law by the Weights and Measures Act
1985 and the Units of Measurement Regulations 1986 and continue to have
effect in UK law. These Acts prescribe use of SI units. While applicants
should be advised to use such units, there is no sanction against the
use of non-SI metric-based units (eg Angstrom units), and they should
not be objected to. \]
:::

### 14.57 {#ref14-57}

r.25(1) is also relevant

::: call-to-action
The EC Units of Measurement Directive (Council Directive 2009/3/EC)
requires the use of metric units in documents (such as patent
specifications) sent out by any part of government and was implemented
into UK law by the Weights and Measures Act 1985 and the Units of
Measurement Regulations 1986. Non-metric units may be used but only in
conjunction with metric ones and then only as supplementary indications
with metric units predominating. Examiners should ensure that the
requirements of paragraph 24 of Schedule 2 are met in any specification
sent for grant. No amendment should be required at the preliminary
examination stage to secure compliance with that rule, since it does not
define a formal requirement nor is it specified in rule 23 as being one
of the rules relevant to preliminary examination. Applications which do
not comply with paragraph 24 of Schedule 2 (or the Directive) are
nevertheless published under s.16 as filed.
:::

\[RC35 should be used to object to the absence of metric or Celsius
units. When both non-metric and metric values are given, the examiner
should not check that the conversions are accurate for consistency's
sake, but objection should be raised in the case of manifest error. \]

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 14(3)**
  The specification of an application shall disclose the invention in a manner which is clear enough and complete enough for the invention to be performed by a person skilled in the art.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 14.58 {#ref14-58}

s.130(7) is also relevant

Section14(3) is intended to have, as nearly as practicable, the same
effect as the corresponding provisions of the EPC, PCT and CPC. Article
83 EPC and Article 5 PCT require the invention to be disclosed "in a
manner sufficiently clear and complete for it to be carried out by a
person skilled in the art". An objection under this section of Art is
often said to relate to "sufficiency of disclosure" or "sufficiency".
The pre-grant provision under s.14(3) accords directly with s.72(1)(c)
which sets out the same requirement for the validity of the granted
patent. Whilst the bulk of the case law discussed below relates to
proceedings under s.72, the principles set out in these cases are
pertinent both to s.14(3) and s.72(1)(c).

### 14.59 {#ref14-59}

s76(2) is also relevant

It is the responsibility of the applicant to ensure that, at the time of
filing the application, the disclosure is clear and complete in respect
of the invention claimed in each of the claims. If it is not, then
either the application must be refused or, if it is possible to do so,
the claims must be restricted to that matter which has been adequately
disclosed i.e. that for which there is an enabling disclosure ([see
14.67](#ref14-67)) relating to enablement). Deficiencies in the
disclosure cannot be rectified by adding matter subsequent to filing
without falling foul of s.76(2).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Sufficiency: summary of general principles]{#default-id-c972c5d5-heading-31 .govuk-accordion__section-button} {#sufficiency-summary-of-general-principles .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Sufficiency: summary of general principles\",\"index_section\":31,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-31 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-31" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Sufficiency: summary of general principles\",\"index_section\":31,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.60 {#ref14-60}

In [Eli Lilly v Human Genome Sciences \[2008\] RPC
29](https://doi.org/10.1093/rpc/rcn027){rel="external"} at \[239\]
Kitchin J gave the following summary of the relevant principles, to be
applied when assessing whether an application satisfies this section of
the Act:

> The specification must disclose the invention clearly and completely
> enough for it to be performed by a person skilled in the art. The key
> elements of this requirement which bear on the present case are these:
> (i) the first step is to identify the invention and that is to be done
> by reading and construing the claims; (ii) in the case of a product
> claim that means making or otherwise obtaining the product; (iii) in
> the case of a process claim, it means working the process; (iv)
> sufficiency of the disclosure must be assessed on the basis of the
> specification as a whole including the description and the claims; (v)
> the disclosure is aimed at the skilled person who may use his common
> general knowledge to supplement the information contained in the
> specification; (vi) the specification must be sufficient to allow the
> invention to be performed over the whole scope of the claim; (vii) the
> specification must be sufficient to allow the invention to be so
> performed without undue burden.

Similar summaries have also been utilised in a number of court decisions
(see e.g. [Wobben v Vestas-Celtic Wind Technology Ltd \[2007\] EWHC 2636
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2007/2636.html){rel="external"}).

The purpose of the requirements imposed by s.14(3) and s.72(1)(c) is to
prevent a patentee laying claim to products or processes which the
teaching of the patent does not enable the skilled addressee to perform
(\[Zipher Ltd v Markem Systems Ltd \[2009\]
FSR1\]http://www.bailii.org/ew/cases/EWHC/Patents/2008/1379.html). Thus,
all consideration of sufficiency in essence deals with the extent to
which the applicant has provided an enabling disclosure for their
invention (see also
[2.10](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-10)
and
[72.03](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-03)).

### 14.61 {#ref14-61}

Whilst there is only one provision under the Act, it is now settled law
that sufficiency in terms of the disclosure being clear and complete
enough for the invention to be performed by the person skilled in the
art is approachable in three different ways:

\(1\) Classical insufficiency (2) Insufficiency by ambiguity/uncertainty
(3) Insufficiency by excessive claim breadth

A summary of what should be understood by each of these approaches to
sufficiency is provided in [Zipher Ltd v Markem Systems Ltd \[2009\] FSR
1](http://www.bailii.org/ew/cases/EWHC/Patents/2008/1379.html){rel="external"},
with the first two also being set out in [Medimmune Ltd v Novartis
Pharmaceuticals UK Ltd, Medical Research Council \[2011\] EWHC 1669
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2011/1669.html){rel="external"}.
These three approaches will be considered in more detail in 14.67-14.82
below, however first it is necessary to set out some general points
common to all three.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Claim construction]{#default-id-c972c5d5-heading-32 .govuk-accordion__section-button} {#claim-construction .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Claim construction\",\"index_section\":32,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-32 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-32" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Claim construction\",\"index_section\":32,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.62 {#ref14-62 .105}

Construction of the claims should be approached in the usual fashion
(see s.125). This involves putting a purposive construction on the
claims, interpreting them in the light of the description and drawings
as set out in Section 125(1)
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Date at which sufficiency is judged]{#default-id-c972c5d5-heading-33 .govuk-accordion__section-button} {#date-at-which-sufficiency-is-judged .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Date at which sufficiency is judged\",\"index_section\":33,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-33 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-33" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Date at which sufficiency is judged\",\"index_section\":33,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.63 {#ref14-63}

In [Biogen Inc. v Medeva plc \[1997\] RPC
1](https://doi.org/10.1093/rpc/1997rpc1){rel="external"} the House of
Lords held that sufficiency should be decided at the date of filing of
the application (rather than the date of publication as had been the
case under the 1949 Act). This being the case because

> it would be illogical if a patent which ought to have been rejected
> under section 14(3) is rendered immune from revocation under section
> 72(1)(c) by advances in the art between the date of application and
> the publication of the specification.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The person skilled in the art]{#default-id-c972c5d5-heading-34 .govuk-accordion__section-button} {#the-person-skilled-in-the-art .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The person skilled in the art\",\"index_section\":34,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-34 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-34" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The person skilled in the art\",\"index_section\":34,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.64 {#ref14-64}

The concept of the skilled person is that of the uninventive, but
technically competent person (or team) who is considered for the purpose
of assessing inventive step ([see
3.26-3.28.2](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-26)).
As stated by Aldous J in [Mentor Corporation v Hollister Inc. \[1993\]
RPC 7](https://doi.org/10.1093/rpc/1993rpc7){rel="external"} (at page
14):

> The section requires that the skilled man be able to perform the
> invention. Such a man is the ordinary addressee of the patent. He must
> be assumed to be possessed of the common general knowledge in the art
> and the necessary skill and expertise to apply that knowledge. He is
> the man of average skill and intelligence, but is not expected to be
> able to exercise any invention. In some arts he may have a degree, in
> others he will be a man with practical experience only. Further, in
> circumstances where the art encompasses more than one technology, the
> notional skilled addressee will be possessed of those technologies
> which may mean that he will have the knowledge of more than one
> person.

However, although the phrase "person skilled in the art" is construed in
the same way when considering sufficiency and inventive step, for the
purposes of s.14(3) the skilled person is seeking to make the patent
work and does so with the common general knowledge at the time the
patent was filed. In contrast to the situation for inventive step
purposes, the skilled worker has the patent in front of them, and thus
is "trying to carry out the invention and achieve success,...not
searching for a solution in ignorance of it." (see [Zipher Ltd v Markem
Systems Ltd \[2009\] FSR
1](http://www.bailii.org/ew/cases/EWHC/Patents/2008/1379.html){rel="external"}
at page 50). This can be significant in determining the nature and
skills of the skilled person (or team), as they need not be the same for
both inventive step and sufficiency purposes (See [Schlumberger Holdings
Ltd v Electromagnetic Geoservices AS \[2010\] RPC
33](https://doi.org/10.1093/rpc/rcq044){rel="external"}) at paragraphs
40 and 61-64, and MoPP 3.28-3.28.2).

### 14.65 {#ref14-65}

In [Halliburton Energy Services Inc v Smith International (North Sea)
Ltd \[2006\] RPC
2](https://doi.org/10.1093/rpc/2006rpc21){rel="external"}, the Patents
Court held that where a programmer formed part of the addressee team of
a computer-based invention, it was essential to form a view of their
capabilities. It was mentioned that the writing of anything other than a
trivial program required substantial effort in writing and debugging
even though much programming required no creative thought and a
competent programmer would have substantial experience in their area of
expertise.

### 14.66 {#ref14-66}

The court in [Kirin-Amgen Inc. v Roche Diagnostics GmbH \[2002\] RPC
1](https://doi.org/10.1093/rpc/2002rpc1){rel="external"} concluded that,
once the addressee's skill and knowledge has been assessed, it may
nevertheless be appropriate to consider that the skilled person might
consult someone else on a certain point when trying to implement the
teaching of the patent. Since a patent is a document which is intended
to be practical, rather than theoretical, in nature - and its teaching
is there for the purpose of being worked - it would be unrealistic to
adopt too rigid an approach to the question of the knowledge of the
skilled addressee.

### 14.66.1 {#ref14-66-1}

In Regeneron Pharmaceuticals v Kymab Ltd \[2018\] EWCA Civ 671, it was
held that the skilled person is not bound to carry out the invention
precisely as described and can use the common general knowledge to
perform the invention and make any obvious changes that may be
necessary, provided of course that any work involved is not undue. See
[14.85-14.88](#ref14-85) for detail on undue burden).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Classical Insufficiency]{#default-id-c972c5d5-heading-35 .govuk-accordion__section-button} {#classical-insufficiency .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Classical Insufficiency\",\"index_section\":35,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-35 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-35" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Classical Insufficiency\",\"index_section\":35,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.67 {#ref14-67}

This is the name now commonly given to what was previously understood
simply as 'insufficiency'. It relates to the situation where there is no
enabling disclosure. In [Zipher Ltd v Markem Systems Ltd \[2009\] FSR
1](http://www.bailii.org/ew/cases/EWHC/Patents/2008/1379.html){rel="external"}
Floyd J offers the following useful summation of the objection:

> Classical insufficiency arises where the express teaching of the
> patent does not enable skilled addressee to perform the invention.
> This type of insufficiency requires an assessment ...of the steps to
> which it would be necessary for the skilled reader or team to take in
> following the teaching of the specification and in order to arrive
> within the claim. Plainly the steps should not include inventive ones.
> But a patent can also be found insufficient if the steps can be
> characterised as prolonged research, enquiry or experiment.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Degree of sufficiency required]{#default-id-c972c5d5-heading-36 .govuk-accordion__section-button} {#degree-of-sufficiency-required .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Degree of sufficiency required\",\"index_section\":36,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-36 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-36" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Degree of sufficiency required\",\"index_section\":36,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.67.1 {#ref14-67-1}

In order to be sufficient the application must include at a minimum
something amounting to one embodiment or example that can be put into
effect. However this should not be equated with one embodiment being
enough to necessarily render the application sufficient as was regarded
as the situation under some previous authorities (Chiron Corp. v Organon
Teknika Ltd (No.3) \[1994\] FSR 202 at 241, [Mölnlycke AB v Procter
Gamble (no.5)\[1994\] RPC
49](https://doi.org/10.1093/rpc/1994rpc49){rel="external"}. Indeed
[Generics (UK) Limited and others v H Lundbeck A/S \[2009\] RPC
13](https://doi.org/10.1093/rpc/rcp023){rel="external"} illustrates how
even when a claim defines only a single discrete product, in that case a
single chemical compound, there can still be argument over whether that
is sufficiently enabled. Instead the application must be sufficient to
enable the whole breadth of the claim to be worked. This is discussed
further under 'Insufficiency by excessive claim breadth' below
[(14.79-14.82)](#ref14-79). However in all situations sufficiency is a
question of fact -- does the patent enable the invention to be worked
across the breadth of the claim? The courts have sought to provide some
guidance in answering this question, but have warned "one must be on
one's guard against formulations that gloss the statutory requirement as
there is always a risk that they will end up being substituted for it"
([Halliburton Energy Services Inc v Smith International (North Sea)
\[2006\] RPC 2  at
para.133](https://doi.org/10.1093/rpc/2006rpc21){rel="external"}. As
noted in [Kirin-Amgen Inc v Hoechst Marion Roussel \[2005\] RPC
9](https://doi.org/10.1093/rpc/2005rpc9){rel="external"} "Whether the
specification is sufficient or not is highly sensitive to the nature of
the invention. The first step is to identify the invention and decide
what it claims to enable the skilled man to do. Then one can ask whether
the specification enables him to do it."

### 14.68 {#ref14-68}

There has been much discussion regarding what should be expected of the
skilled worker in establishing this enablement. However, a useful test,
given in [Edison and Swan Electric Light Co v Holland 6 RPC
243](https://doi.org/10.1093/rpc/6.19.243){rel="external"} at page 282,
consists in asking whether anything new has to be found out by a person
of reasonably competent skill following the directions in the
specification in order to succeed; if the answer is yes, the disclosure
is not complete enough. Similarly, the classic statement of the test for
insufficiency in [Valensi v British Radio Corporation \[1973\] RPC
337](https://doi.org/10.1093/rpc/90.12.337){rel="external"} at 377 (CA)
is useful:

> We think the effect of these cases as a whole is to show that the
> hypothetical addressee is not a person of exceptional skill and
> knowledge that he is not to be expected to exercise any invention nor
> any prolonged research, inquiry or experiment. He must, however, be
> prepared to display a reasonable degree of skill and common knowledge
> of the art in making trials and to correct obvious errors in the
> specification, if a means of correcting them can readily be found.

### 14.69 {#ref14-69}

This negative test, the absence of prolonged research, was approved by
the Court of Appeal in [Mentor Corporation v Hollister Inc \[1993\] RPC
7](https://doi.org/10.1093/rpc/1993rpc7){rel="external"} as was the
judge in the lower court's statement regarding the positive test that
only the performance of "routine trials" should be required:

> When, a little later, Aldous J came to apply the law to the facts of
> this case, he refers to "routine trials" and "normal routine matters
> that the skilled man would seek to do and would be able to do". Mr.
> Thorley criticises the use of the word "routine". To require the
> performance of routine trials is, he said, to ask too much of the
> addressee. I do not agree. "Routine" is just the word I would have
> chosen myself to describe the sort of trial and error which has always
> been regarded as acceptable; and "routine trials" has the further
> advantage that it is a positive concept, which is easily understood
> and applied. In practice, therefore, it may provide a surer test of
> what is meant by "clearly enough and completely enough" in section
> 72(1) of the Act than the negative test proposed in Valensi, namely
> the absence of prolonged research, enquiry and experiment. If the
> trials are unusually arduous or prolonged, they would hardly be
> described as routine.

### 14.70 {#ref14-70}

However, it was stressed in [Halliburton Energy Services Inc v Smith
International (North Sea) Ltd \[2006\] EWCA Civ
1715](http://www.bailii.org/ew/cases/EWCA/Civ/2006/1715.html){rel="external"}
that not only did the setting of a gigantic project, even if merely
routine, not satisfy the test, but also that no analogy should be drawn
with genetic engineering and pharmaceutical inventions, where much
routine work is involved in implementation, as in such cases the work
that goes into bringing them to market relates to testing efficacy and
safety --- not in actually making the invented product.

### 14.70.1 {#ref14-70-1}

\[Deleted\]
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Quality of product enabled]{#default-id-c972c5d5-heading-37 .govuk-accordion__section-button} {#quality-of-product-enabled .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Quality of product enabled\",\"index_section\":37,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-37 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-37" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Quality of product enabled\",\"index_section\":37,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.71 {#ref14-71}

In [Mentor Corporation v Hollister Inc \[1993\] RPC
7](https://doi.org/10.1093/rpc/1993rpc7){rel="external"} (at page 17
lines 4-14) it was accepted that it was enough that the patent allowed a
"workable prototype" to be arrived at with comparative ease (see
reference to routine trials above at 14.69) and the requirement was not
to produce a "successful commercial product". Similarly, the fact that a
specification does not refer to a step which may well be useful for the
purpose of being able to reproduce consistently reliable products of
commercial quality and range does not render the disclosure incomplete
provided that the directions in the specification lead to a product
which has "patent utility", i.e. is suitable for and fulfils the purpose
for which the specification states it is intended. For instance, a
useful perfecting step in making a compound does not have to be
disclosed if the imperfectly produced compound can still be used for the
application's purpose. ([American Cyanamid v Ethicon \[1979\] RPC
215](https://doi.org/10.1093/rpc/1979rpc215){rel="external"} at page
265). Thus the applicant also does not have to disclose preferred
embodiments.

In addition, the applicant is not required to disclose the best method
of performing the invention which they are entitled to claim. (Rule
5.1(a)(v) of the PCT specifies that the best mode should be described,
but goes on to say that where the national law of a designated state
does not require description of the best mode (as in the UK) failure to
describe the best mode shall have no effect in that State).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Functional claims]{#default-id-c972c5d5-heading-38 .govuk-accordion__section-button} {#functional-claims .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Functional claims\",\"index_section\":38,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-38 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-38" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Functional claims\",\"index_section\":38,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.72 {#ref14-72}

The criteria for adequacy of disclosure are the same whatever the form
of the claim and are not stricter when the claim is of functional form,
that is when it is limited by result, e.g. is of the "No-Fume" type [see
14.120](#ref14-120), ([International Business Machines Corporation's
Application \[1970\] RPC
542](https://doi.org/10.1093/rpc/87.18.533){rel="external"}). However,
where claims are defined by functional features or desirable results,
the specification does need to provide enough instruction for the
skilled person to be able to achieve the desired result without
embarking on a research programme [(see 14.69](#ref14-69),
[14.82](#ref14-82) and [14.87](#ref14-87). In order to be sufficient,
functional or mechanistic claims must also not be so ambiguous as to be
unworkable [see 14.76- 14.78](#ref14-76).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Medical use claims]{#default-id-c972c5d5-heading-39 .govuk-accordion__section-button} {#medical-use-claims .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Medical use claims\",\"index_section\":39,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-39 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-39" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Medical use claims\",\"index_section\":39,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.72.1 {#ref14-72-1}

For applications or patents relating to a second or further medical use
of a known substance or composition, the courts have held that the
specification as filed must make it plausible that the substance or
composition will be effective for the claimed use or uses; if not, the
patent will be insufficient. This requirement for plausibility was
confirmed by the Supreme Court in [Warner-Lambert Company LLC v Generics
(UK) Ltd (t.a. Mylan) & Anor. \[2018\] UKSC
56](http://www.bailii.org/uk/cases/UKSC/2018/56.html){rel="external"},
and criteria for determining plausibility were set out. However, in this
decision (and the preceding lower court decisions) it was not suggested
that this requirement extended more generally to the assessment of
sufficiency in other categories of invention, and the criteria set out
by the Supreme Court are specific to inventions concerning new medical
uses. For this reason, the detailed discussion of this issue is provided
in the discussion of second medical use claims at
[4A.29.2-4A.30](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-29-2).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Errors and omissions]{#default-id-c972c5d5-heading-40 .govuk-accordion__section-button} {#errors-and-omissions .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Errors and omissions\",\"index_section\":40,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-40 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-40" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Errors and omissions\",\"index_section\":40,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.73 {#ref14-73}

An error will not cause a patent to be found invalid, whether it is
found in the description or in a drawing, provided that the skilled
worker would both observe it and be in a position to correct it. In
[No-Fume Ltd v Frank Pitchford & Co. Ltd. (1935) 52 RPC
231](https://doi.org/10.1093/rpc/52.7.231){rel="external"} at 243 Romer
LJ stated:

> The test to be applied for the purpose of ascertaining whether a man
> skilled in the art can readily correct the mistake or readily supply
> the omissions, has been stated to be this: Can he rectify the mistakes
> and supply the omissions without the exercise of any inventive
> faculty? If he can, then the description of the specification is
> sufficient. If he cannot, the patent will be void for insufficiency.

### 14.74 {#ref14-74}

It is neither necessary nor desirable that details of well-known
ancillary features should be given, but the specification must disclose
any feature essential for carrying out the invention in sufficient
detail to render it obvious to the skilled person how to put the
invention successfully into practice. In [Badische Anilin and Soda
Fabrik v La Societe etc du Rhone 15 RPC
359](https://doi.org/10.1093/rpc/15.15.359){rel="external"}, a
specification which referred merely to the use of an autoclave of
unstated material, was held to be insufficient. It was necessary for
success that an iron vessel be used, and yet it was shown that other
autoclaves, e.g. enamelled, were frequently used in the trade.

### 14.75 {#ref14-75}

In [Mayne Pharma v Debiopharm and Sanofi-Synthélabo \[2006\] EWHC 1123
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2006/1123.html){rel="external"},
it was held that for a claim defining "a stable oxaliplatin solution
formulation comprising oxaliplatin, an effective stabilising amount of a
buffering agent ... and a pharmaceutically acceptable carrier" to be
sufficient, it must be possible to design a test which can answer the
question: "have I used such an amount or not?". The specification
pointed, however, to the answer "you don't have to add any at all", and
the invention so claimed was therefore insufficiently disclosed.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Insufficiency by ambiguity/uncertainty]{#default-id-c972c5d5-heading-41 .govuk-accordion__section-button} {#insufficiency-by-ambiguityuncertainty .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Insufficiency by ambiguity/uncertainty\",\"index_section\":41,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-41 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-41" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Insufficiency by ambiguity/uncertainty\",\"index_section\":41,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.76 {#ref14-76}

Whilst the skilled person is taken to be trying to make the invention
work, as held by Lord Hoffmann in [Kirin-Amgen Inc v Hoechst Marion
Roussel \[2005\] RPC
9](https://doi.org/10.1093/rpc/2005rpc9){rel="external"}, the second way
in which insufficiency can arise is when the disclosure is so ambiguous
as to make it impossible to know whether one had worked the invention or
not. This Lord Hoffman distinguished from a simple lack of clarity in
Kirin Amgen. The relevant claim required the recombinant erythropoietin
(rEPO) of the invention to have a higher molecular weight compared to
urinary erythropoetin (uEPO). However different uEPOs have different
molecular weights, thus whether or not a product fell within the claim
depended on the choice of uEPO and the specification did not tell the
skilled person how to make this choice. Lord Hoffman elaborated "In the
present case, however, the choice of uEPO has nothing to do with making
the invention work. It is simply a criterion against which one tests
whether rEPO falls within the claims...All the skilled man can do is try
and guess which uEPO the patentee had in mind and if the specification
does not tell him, then it is insufficient." Elsewhere in his judgement
Lord Hoffman provided an artificial example of how lack of clarity can
overstep the mark into insufficiency:

> If the claim says that you must use an acid, and there is nothing in
> the specification or context to tell you which acid, and the invention
> will work with some acids but not with others but finding out which
> ones work will need extensive experiments, then that in my opinion is
> not merely lack of clarity; it is insufficiency. The lack of clarity
> does not merely create a fuzzy boundary between that which will work
> and that which will not. It makes it impossible to work the invention
> at all until one has found out what ingredient is needed.

In [Anan Kasei v Neo Chemicals \[2019\] EWCA Civ
1646](http://www.bailii.org/ew/cases/EWCA/Civ/2019/1646.html){rel="external"},
Floyd LJ noted "that Lord Hoffmann's emphasis was simply intended to
draw attention to the distance between the judge's finding and a case
which presented doubtful cases at the edge of a claim. For my part, I do
not agree that the objection of uncertainty is answered simply because
there is something within the claim which is clear, if there is a large
territory (more than a fuzzy boundary) where the claim is uncertain". In
this decision Lewison LJ suggested that 'uncertainty' was more
appropriate than 'ambiguity' because something is ambiguous when it is
capable of having two (or more) meanings, and ultimately the court will
be able to decide which of them is the correct meaning, whilst the issue
at hand was that of uncertainty. If the court cannot ascertain the
boundary, having used all the interpretative tools at its disposal, it
must conclude that the specification does not disclose the invention
clearly enough and completely enough for it to be performed by a person
skilled in the art.

Similarly in [Sandvik Intellectual Property AB v Kennametal UK Ltd
\[2011\] EWHC
3311(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2011/3311.html){rel="external"}
(at paragraph 164), a claim to a coating having a particular texture
coefficient on a cutting tool was insufficient for ambiguity because
calculating the value of the coefficient required reference to a value
measured against a standard and there was no disclosure of which of the
two widely used common standards was used by the patentee. In this case,
this ambiguity only made a difference between infringement and
non-infringement at the extremities of this one parameter, nonetheless
in those circumstances it was impossible to say whether the product fell
within the claim or not, because it was uncertain what the correct test
was. In [Zipher Ltd v Markem Systems Ltd \[2009\] FSR
1](http://www.bailii.org/ew/cases/EWHC/Patents/2008/1379.html){rel="external"}
(at paragraph 374) Floyd J cautioned that "If the skilled person cannot
tell whether he is working the invention or not, the specification is
insufficient. It is not, however, enough to establish this type of
insufficiency to show that there may be a puzzle at the edge of the
claims. It will normally be necessary for the problem to permeate the
whole claim".

### 14.77 {#ref14-77}

Functionally or mechanistically-defined uses are not considered to be
inherently so ambiguous as to be insufficient by the UK courts. In the
context of medical uses, Regeneron Pharmaceuticals v Genentech \[2012\]
EWHC 657 the Patents Court considered whether a claimed use for the
treatment of "a non-neoplastic disease or disorder characterised by
undesirable excessive neovascularisation" was so ambiguous as not to be
defined sufficiently for the skilled person to determine whether or not
the claims are infringed. Floyd J rejected this allegation:

> There was no evidence that the skilled addressee would have any
> difficulty in determining whether a given disease would fall within
> the terms of the claim as I have construed them.

### 14.78 {#ref14-78}

The description is presumed to be addressed to a person skilled in the
art who is doing their best to understand and not to criticise;
technological defects or obscurities which are unimportant and do not
cast doubt on the scope of the invention are not fundamentally
objectionable. Objection should not be raised merely because it is
possible to describe the invention more clearly, provided there is an
invention that is sufficiently described (cf. [Schwarzkopf and Ors'
Application, 31 RPC
437](https://doi.org/10.1093/rpc/31.16.437){rel="external"} at 439).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Insufficiency by excessive claim breadth]{#default-id-c972c5d5-heading-42 .govuk-accordion__section-button} {#insufficiency-by-excessive-claim-breadth .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Insufficiency by excessive claim breadth\",\"index_section\":42,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-42 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-42" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Insufficiency by excessive claim breadth\",\"index_section\":42,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.79 {#ref14-79}

The disclosure of an invention must be sufficient to enable the
invention to be performed to the full extent of the monopoly claimed. In
contrast to the situation where a patent or application is classically
insufficient, there may be an enabling disclosure for some portion of
the invention, but not for the full breadth of the claims. It therefore
follows that restricting the scope of the claims to that which is
enabled can overcome the objection.

The House of Lords in [Biogen Inc v Medeva plc \[1997\] RPC
1](https://doi.org/10.1093/rpc/1997rpc1){rel="external"} held that for
the purposes of s.14(3) and 72(1)(c) the disclosure must be sufficient
to enable the whole width of the claimed invention to be performed, and
the disclosure of a single embodiment will not always satisfy the
requirement regardless of the width of the claim. Insufficiency arising
from a disclosure which does not enable the invention to be performed
across the entire claim width is thus sometimes referred to as "Biogen
insufficiency". This principle is not confined to chemistry and
biotechnology patents. In [Nokia GmbH v IPCom GmbH & Co KG \[2009\] EWHC
3482
(Pat)](http://www.bailii.org/ew/cases/EWHC/Patents/2010/3482.html){rel="external"}
the patent for synchronization of mobile radio telephones was attacked
on the grounds that the whole scope of the claim was not workable. The
initial synchronization required "coarse frequency synchronization at
least if the accuracy of the carrier frequencies is not adequate, in
which case the coarse frequency synchronization operates independently
of bursts and determines whether the frequency of the determined carrier
is within a tolerance band". Nokia's attack was based on the fact that
the coarse frequency synchronization was not enabled while IPCom argued
that if the coarse frequency synchronization did not work then it would
have been possible to obtain an oscillator of sufficient accuracy to
avoid the need for coarse frequency synchronisation at all. In effect
two methods were claimed and IPCom's defence was, that if one didn't
work, the skilled person could still perform the other. Floyd J (as he
then was) rejected the argument and restated the principle set out in
Biogen that the entire scope of the claim must be enabled, not just part
of it. In effect both methods had to be enabled.

### 14.80 {#ref14-80}

Pumfrey J held in [Minnesota Mining & Manufacturing Co's (Suspension
Aerosol Formulation) Patent \[1999\] RPC
135](https://doi.org/10.1093/rpc/1999rpc135){rel="external"} (at
150-151) that a specification is insufficient if it provides no teaching
relating to the criteria according to which the skilled person is taken
to be using the invention. What will suffice to satisfy the criterion
that the disclosure must be sufficient across the whole width of the
claimed invention will vary depending upon the nature of the claim.
Thus, for example, when there is in truth more than one product which is
claimed, the question has to be asked whether the invention of one
product is the invention of the other. Unless it is they are different
inventions and each must be sufficiently described. A similar conclusion
had been reached by the Court of Appeal in that case and [Chiron Corp.
and ors v Murex Diagnostics Ltd and ors \[1996\] RPC
535](https://doi.org/10.1093/rpc/1996rpc535){rel="external"} (pages 612
and 613).

### 14.80.1 {#ref14-80-1}

In [Anan Kasei v Neo Chemicals \[2019\] EWCA Civ
1646](http://www.bailii.org/ew/cases/EWCA/Civ/2019/1646.html){rel="external"},
Floyd LJ considered the implications of Biogen v Medeva plc and
[Generics (UK) Limited and others v H Lundbeck A/S \[2009\] RPC
13](https://doi.org/10.1093/rpc/rcp023){rel="external"} and drew out the
following:

> 1.  The principle in Biogen is concerned with permissible scope of
>     claim in the light of the patentee's contribution to the art.
> 2.  In general, that principle is that the claim must not extend to
>     embodiments which owe nothing to the patentee's contribution to
>     the art.
> 3.  In the case of a claim to a single novel chemical compound \[as in
>     Generics v Lundbeck\], the patentee's technical contribution is
>     that compound. Such a claim will not be insufficient if the single
>     compound is enabled by a method in the specification,
>     notwithstanding the fact that there may be other methods of making
>     it which owe nothing to the disclosed method.
> 4.  The same must be true of a claim to a class of compounds, each of
>     which can be made by the application of a method disclosed in the
>     specification. There is no requirement that the patentee disclose
>     more than one method, where one method will do.
> 5.  This does not mean that all claims to a class of products by
>     definition comply with the Biogen principle. The conclusion in
>     Biogen shows that a claim which is formally to a class of products
>     may cover embodiments which owe nothing to the patentee's
>     technical contribution.
> 6.  The reason why the claim in Biogen offended the principle was not
>     because it had "process components" but because the language of
>     the claim was so generalised (both in relation to the manner in
>     which the product was made and in relation to its function) that
>     it extended to embodiments which owed nothing to the patentee's
>     contribution to the art. A claim to a product defined by its
>     function (e.g. any heavier than air flying machine referred to by
>     Lord Hoffmann at page 52 in Biogen) is capable of extending to
>     subject matter which owes nothing to the patentee's contribution
>     to the art.

### 14.81 {#ref14-81}

The EPO Technical Board of Appeal in EXXON/Fuel oils \[1994\] 9 OJEPO
653 (T409/91) and UNILEVER/Detergents \[1995\] 4 OJEPO 188 (T435/91)
which held that an application should provide enough information to
allow a person skilled in the art to carry out substantially all that
which falls within the ambit of what is claimed. In T435/91 the Board
considered the sufficiency of a 'functional' definition of a component
in a claimed composition and found that the indefinite and abstract host
of possible alternatives must all be available to the skilled person if
the definition is to satisfy the requirement for sufficiency. [Novartis
AG v Johnson & Johnson \[2009\] EWHC (Pat)
1671](http://www.bailii.org/ew/cases/EWHC/Patents/2009/1671.html){rel="external"}
drew on the EPO case law in T 435/91, T 694/92 and (in particular) T
1743/06 when considering the sufficiency of a claim to a contact lens
defined almost entirely by desirable characteristics. Kitchin J (as he
then was) stated:

> ...a claim to a class of products said to possess a useful activity
> must be based upon the identification of a common principle which
> permits a reasonable prediction to be made that substantially all the
> claimed products do indeed share that activity. Further, it is not
> permissible to by-pass that requirement simply by adding a functional
> limitation which restricts the scope of the claim to all products
> which do have the relevant activity, that is to say all those which
> 'work'. In the case of a claim limited by function, it must still be
> possible to perform the invention across the scope of the claim
> without undue effort.

This was upheld at the Court of Appeal in [Novartis AG v Johnson &
Johnson \[2010\] EWCA Civ
1039](http://www.bailii.org/ew/cases/EWCA/Civ/2010/1039.html){rel="external"}.
Jacob LJ observed:

"Generally patents with functional claims give you guidance as to what
to do if you embark on a trial and error process. The reader can learn
from the errors so as to reach something that works. But not here."

The claim was therefore considered to amount to "if you try any pair of
polymers, to see if they work...and find anything that does, we claim
it." Moreover, it was not clear even whether the examples provided in
the patent "worked" according to the parameters defined in the claim.
The patent was therefore revoked on grounds of sufficiency.

### 14.82 {#ref14-82}

In [American Home Products Corp. v Novartis Pharmaceuticals UK Ltd
\[2001\] RPC 8](https://doi.org/10.1093/rpc/2001rpc8){rel="external"}
the invention concerned the use of a known antibiotic (rapamycin) for
the preparation of a medicament for inhibiting organ or tissue
transplant rejection. The Court of Appeal reversed the lower court's
decision and held that the claim only covered rapamycin, but did not
cover derivatives of rapamycin, and was thus sufficient. The court then
observed that, had the claim covered derivatives, the patent would have
been insufficient because there was no disclosure in the description
enabling the skilled person to decide which of the many possible
derivatives would have worked. Although there was a strong possibility
that some of the large number of derivatives would work in the same way
as rapamycin itself, it was impossible to say which would so work,
unless the skilled person undertook the "vast and correspondingly
burdensome" research task necessary. Thus the court distinguished
between a sufficient description, which requires the skilled person to
use their skill to perform the invention, and an insufficient
description, which requires the skilled person to go to the expense and
labour of trying to ascertain which of the products encompassed by the
claim actually has the required properties. Similarly, in [DSM NV's
Patent \[2001\] RPC
35](https://doi.org/10.1093/rpc/2001rpc35){rel="external"} (see
paragraphs 181-194), a claim was insufficient because a skilled worker
seeking to implement the invention over the whole width of the claim
would have been required to depart from the express teaching of the
patent and experiment over a long period of time before possibly
achieving the desired result.

### 14.82.1 {#ref14-82-1}

In [Regeneron Pharmaceuticals Inc v Kymab Ltd \[2020\] UKSC 27 (PDF,
128KB)](https://www.supremecourt.uk/cases/docs/uksc-2018-0131-judgment.pdf){rel="external"}
Lord Briggs (at paragraph 56) summarised the following helpful
principles which should be used when assessing the sufficiency of
product claims that cover a range of embodiments;\
\
"i) The requirement of sufficiency imposed by article 83 of the EPC
exists to ensure that the extent of the monopoly conferred by the patent
corresponds with the extent of the contribution which it makes to the
art.\
\
ii) In the case of a product claim, the contribution to the art is the
ability of the skilled person to make the product itself, rather than
(if different) the invention.\
\
iii) Patentees are free to choose how widely to frame the range of
products for which they claim protection. But they need to ensure that
they make no broader claim than is enabled by their disclosure.\
\
iv) The disclosure required of the patentee is such as will, coupled
with the common general knowledge existing as at the priority date, be
sufficient to enable the skilled person to make substantially all the
types or embodiments of products within the scope of the claim. That is
what, in the context of a product claim, enablement means.\
\
v) A claim which seeks to protect products which cannot be made by the
skilled person using the disclosure in the patent will, subject to de
minimis or wholly irrelevant exceptions, be bound to exceed the
contribution to the art made by the patent, measured as it must be at
the priority date.\
\
vi) This does not mean that the patentee has to demonstrate in the
disclosure that every embodiment within the scope of the claim has been
tried, tested and proved to have been enabled to be made. Patentees may
rely, if they can, upon a principle of general application if it would
appear reasonably likely to enable the whole range of products within
the scope of the claim to be made. But they take the risk, if
challenged, that the supposed general principle will be proved at trial
not in fact to enable a significant, relevant, part of the claimed range
to be made, as at the priority date.\
\
vii) Nor will a claim which in substance passes the sufficiency test be
defeated by dividing the product claim into a range denominated by some
wholly irrelevant factor, such as the length of a mouse's tail. The
requirement to show enablement across the whole scope of the claim
applies only across a relevant range. Put broadly, the range will be
relevant if it is denominated by reference to a variable which
significantly affects the value or utility of the product in achieving
the purpose for which it is to be made."\
\
viii) Enablement across the scope of a product claim is not established
merely by showing that all products within the relevant range will, if
and when they can be made, deliver the same general benefit intended to
be generated by the invention, regardless how valuable and
ground-breaking that invention may prove to be.

### 14.82.2 {#ref14-82-2}

[Regeneron Pharmaceuticals Inc v Kymab Ltd \[2020\] UKSC
27](https://www.supremecourt.uk/cases/docs/uksc-2018-0131-judgment.pdf){rel="external"}
was clarified by Birss J in Illumina Cambridge Ltd v Latvia MGI Tech SIA
& Ors \[2021\] EWHC 57 (Pat). He held that the principles set out by
Lord Briggs at paragraph 56 of Regeneron (see above), are not limited to
product claims and apply equally to method claims. To assist with
transposing the principles in Regeneron to apply them in different
circumstances he reframed principles iv-vii) in general terms as
follows;

'' iv) The disclosure required of the patentee is such as will, coupled
with the common general knowledge existing as at the priority date, be
sufficient to enable the skilled person to perform substantially all the
types or embodiments \[ \] within the scope of the claim. That is what,
\[ \], enablement means. \[...\]

v\) A claim which seeks to protect products or processes which cannot be
performed by the skilled person using the disclosure in the patent will,
subject to de minimis or wholly irrelevant exceptions, be bound to
exceed the contribution to the art made by the patent, measured as it
must be at the priority date.

vi\) This does not mean that the patentee has to demonstrate in the
disclosure that every embodiment within the scope of the claim has been
tried, tested and proved to have been enabled \[...\]. Patentees may
rely, if they can, upon a principle of general application if it would
appear reasonably likely to enable the whole range \[...\] within the
scope of the claim to be performed. But they take the risk, if
challenged, that the supposed general principle will be proved at trial
not in fact to enable a significant, relevant, part of the claimed range
to be performed, as at the priority date.

vii\) Nor will a claim which in substance passes the sufficiency test be
defeated by dividing the \[...\] claim into a range denominated by some
wholly irrelevant factor, such as the length of a mouse's tail. The
requirement to show enablement across the whole scope of the claim
applies only across a relevant range. Put broadly, the range will be
relevant if it is denominated by reference to a variable which
significantly affects the value or utility of the product or process in
achieving the purpose for which it is to be performed.''

In this case, which dealt with DNA sequencing technology, MGI attempted
to argue Illumina's patent was insufficient because, a) the scope of the
claims were broad enough to encompass nucleotides that could not be used
in the claimed sequencing method, and, b) the claims covered methods of
sequencing with unspecified read-lengths, whilst the specification only
disclosed examples of short read-lengths. Birss J emphasised that the
requirement to show enablement across the entire scope of a claim only
applies to a ''relevant range'', and held that because features a) and
b) did not relate to the ''the essence or core of the invention (closely
related to the technical contribution and/or the inventive concept)''
they were not relevant ranges in the Regeneron sense, and therefore
could not render the claim insufficient. This reinforces what Lord
Briggs said at point vii) of paragraph 56 in the original Regeneron
judgment. For further discussion on the concept of relevant ranges refer
to paras 276-277 of the Illumina judgment.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [A principle of general application]{#default-id-c972c5d5-heading-43 .govuk-accordion__section-button} {#a-principle-of-general-application .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"A principle of general application\",\"index_section\":43,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-43 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-43" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"A principle of general application\",\"index_section\":43,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.83 {#ref14-83}

The approach taken in the functionally defined cases above (Novartis,
American Home Products etc.) deals with the question of where the
specification discloses a "principle of general application". In both
Novartis and American Home Products (above), the insufficiency arose in
part because the cases did not contain a "principle of general
application" by which the limited examples could be said to enable the
general terms used in the claims. In [Biogen Inc. v Medeva plc \[1997\]
RPC 1](https://doi.org/10.1093/rpc/1997rpc1){rel="external"} Hoffmann LJ
stated:

> Thus if the patentee has hit upon a new product which has a beneficial
> effect but cannot demonstrate that there is a common principle by
> which that effect will be shared by other products in that class, he
> will be entitled to a patent for that product but not for the class,
> even though some may subsequently turn out to have the same beneficial
> effect... On the other hand, if he has disclosed a beneficial property
> which is common to the class, he will be entitled to a patent for all
> products of that class (assuming them to be new) even though he has
> not himself made more than one or two of them.

### 14.84 {#ref14-84}

Biogen Inc. v Medeva plc (above) was clarified by the Court of Appeal in
[Kirin-Amgen Inc. v Transkaryotic Therapies Inc. \[2003\] RPC
3](https://doi.org/10.1093/rpc/2003rpc3){rel="external"}, and at the
\[House of Lords appeal ([Kirin-Amgen Inc v Hoechst Marion Roussel
\[2005\] RPC 9](https://doi.org/10.1093/rpc/2005rpc9){rel="external"}.
Lord Hoffmann held that "a principle of general application" was simply
an element of the claim stated in general terms. Such a claim was
sufficiently enabled if it could be reasonably expected that the
invention would work with anything falling within the general terms. For
example, a requirement of "connecting means" was enabled if the
invention could reasonably be expected to work with any means of
connection, but it was not necessary for the patentee to have
experimented with all of them.

Similarly in Regeneron Pharmaceuticals Inc v Genentech Inc \[2012\] EW
HC 657 (Pat) (upheld at appeal Regeneron Pharmaceuticals Inc, Bayer
Pharma AG v Genentec Inc \[2013\] EW CA Civ 93) the Patents Court
considered that a second medical use claim relating to "Use of a hVEGF
antagonist in the preparation of a medicament for the treatment of a
non-neoplastic disease or disorder characterised by undesirable
excessive neovascularisation" did relate to a "principle of general
application" and as such a claim in correspondingly broad terms was
acceptable (See also [Examination Guidelines for Patent Applications
relating to Medical Inventions in the Intellectual Property Office
paragraphs 49 and
156-7](https://www.gov.uk/government/uploads/system/uploads/attachment_data/file/315048/medicalguidelines.pdf).
In many of these cases, the extent to which the breadth of the claim is
excessive has to be assessed according to how much work the skilled
person needs to carry out to make the invention work.

The subject of a ''principle of general application'' was revisited in
[Regeneron Pharmaceuticals Inc v Kymab Ltd \[2020\] UKSC
27](https://www.supremecourt.uk/cases/docs/uksc-2018-0131-judgment.pdf){rel="external"},
where the court considered to what extent the ''principle of general
application'' could be used to satisfy the sufficiency requirement for a
product claim where the specification enabled the skilled person to make
some, but not all, of the products in a range. The court held that cases
of this nature inevitably fall into two categories. The first category
is that in which the claim encompasses embodiments unavailable at the
priority date, but nevertheless a sufficient general principle exists
which enables the skilled person, using the disclosure and/or their
common general knowledge, to work the invention across the scope of the
claim and apply the general principle to embodiments as and when they
are discovered -- reference is made to Genentech I/Polypeptide
expression (T 292/85) and UNILEVER/Stable Bleaches (T 226/85) as
examples of cases in this category. The second category is that in which
there is no general principle which would allow the skilled person to
work the claim across its entire range, and it is not possible to rely
on either the disclosure or their common general knowledge to enable the
remaining scope of the claim -- reference is made to EXXON/Fuel oils
(T409/91) and UNILEVER/Detergents (T435/91) as examples of cases in this
category. In the case before the court here, which related to
genetically modified mice, the court found that it fell into the second
category. The skilled person at the time of the earliest date was not
able to insert large sequences of DNA without exercising inventive
ingenuity. Consequently, there was only enablement across a very small
part of the claimed range and the claim was held to be insufficient (see
[14.82.1](#ref14-82-1)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Undue burden]{#default-id-c972c5d5-heading-44 .govuk-accordion__section-button} {#undue-burden .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Undue burden\",\"index_section\":44,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-44 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-44" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Undue burden\",\"index_section\":44,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.85 {#ref14-85}

The specification does not need to disclose all the details of the
operation to be carried out in order to perform the invention since an
enabling disclosure it is to be interpreted by the skilled person, in
light of common general knowledge, who is reasonably expected to carry
out tests. In [Eli Lilly & Co. v Human Genome Sciences, Inc.\[2008\]
EWHC 1903 (Pat) \[2008\] RPC
29](https://doi.org/10.1093/rpc/rcn027){rel="external"}, Kitchin J held
that the specification must be sufficient to allow the invention to be
performed without undue burden, having regard to the fact that the
specification should explain to the skilled person how the invention can
be performed. The question whether a burden is undue must be sensitive
to the nature of the invention, the abilities of the skilled person and
the art in which the invention has been made (at the time of filing).

### 14.86 {#ref14-86}

It should always be remembered that the skilled person is also taken to
be trying to make the invention work, as held by Lord Hoffmann in
[Kirin-Amgen Inc v Hoechst Marion Roussel \[2005\] RPC
9](https://doi.org/10.1093/rpc/2005rpc9){rel="external"}. Thus if the
skilled person would quickly realise that one method would work and
another would fail, the specification is not insufficient because the
claim is expressed in terms broad enough to include both methods.

### 14.87 {#ref14-87}

In Chiron Corp. v Organon Teknika Ltd. \[1994\] FSR 202 where it took
the applicants themselves several years from filing an application,
claiming among other things a vaccine, to successfully produce the
vaccine, the vaccine claim was held to be invalid. However, in [American
Cyanamid v Ethicon \[1979\] RPC 215 at
265](https://doi.org/10.1093/rpc/1979rpc215){rel="external"} at 265, in
which it had been contended that a patent claiming a surgical suture
made of a particular polymer was invalid for, inter alia, insufficiency
in that it did not point out the need for adequately drying the polymer
and freeing it from undesired monomer, it was held that the disclosure
was not insufficient since these were steps which "the instructed reader
desirous of achieving success could be expected, if necessary, to take".
Accordingly the hypothetical addressee had to be prepared to display a
reasonable degree of skill and common knowledge of the art in making
trials and to correct obvious errors and omissions in the specification
if a way of correcting them could readily be found ([Mentor Corporation
v Hollister Inc \[1993\] RPC
7](https://doi.org/10.1093/rpc/1993rpc7){rel="external"}), although is
not expected to exercise any invention or any prolonged research,
inquiry or experiment. Referring to this case, Pumfrey J in [Halliburton
Energy Services Inc v Smith International (North Sea) Ltd \[2006\] RPC
2](https://doi.org/10.1093/rpc/2006rpc2){rel="external"} agreed that the
straightforward test for sufficiency was whether the specification
required the addressee to carry out tests or developments that went
beyond routine trials. Where the specification is very complex and its
development would be expected to be accompanied by a great amount of
work, it is always necessary to keep a balance between the interests of
the public and the interests of the patentee in the sense that it is
necessary to guard against imposing too high a standard of disclosure
merely because the subject matter was inherently complex.

### 14.88 {#ref14-88}

The question of an undue burden was at the heart of the sufficiency of a
claim to a functionally-defined product in T 1743/06 INEOS/Silicas. In
this case, the Board held that, while it was acceptable that the skilled
person would need to use a reasonable amount of trial and error to
select conditions which would achieve the desired result, there must be
adequate instructions in the specification, or on the basis of common
general knowledge, to lead the skilled person towards success, through
evaluation of initial failures.

### 14.88.1 {#ref14-88-1}

Claim breadth and insufficiency in relation to claims with both
structural and functional limitations was considered in [FibroGen Inc. v
Akebia Therapeutics Inc \[2021\] EWCA Civ
1279](https://www.bailii.org/ew/cases/EWCA/Civ/2021/1279.html){rel="external"}.
Birss LJ, following closely the reasoning in [Regeneron Pharmaceuticals
Inc v Genentech Inc \[2012\] EWHC 657
(Pat)](http://www.bailii.org/cgi-bin/markup.cgi?doc=/ew/cases/EWHC/Patents/2012/657.html&query=regeneron&method=boolean){rel="external"},
held that regardless of whether a claim contains functional limitations,
structural limitations or a mixture of both, answering the question of
whether a principle of general application is plausible/credible
requires use of the same three-step test;\
i) Identify what falls within the scope of the claimed class?\
ii) Identify what it means to say that the invention works (in other
words what is the invention for)?\
iii) Determine whether it is possible to make a reasonable prediction
that the invention will work with substantially everything falling
within the scope of the claim?

If it is possible to make the prediction of step (iii) then a claim
cannot be held insufficient merely because the patentee has not
demonstrated the invention works in every case -- if a principle of
general application exists, functional language will invariably cover
compounds which have not been invented yet. What is important is that:
a) the skilled person, without undue burden, is able to identify
compounds beyond those named in the patent that fall within the scope of
the claims, and, b) the skilled person can work substantially anywhere
within the whole claim (e.g. no inventive step can be needed to work in
a part of the claim that was not available in the specification, as was
held to be the case in [Regeneron Pharmaceuticals Inc v Kymab Ltd
\[2020\] UKSC 27 (PDF,
128KB)](https://www.supremecourt.uk/cases/docs/uksc-2018-0131-judgment.pdf){rel="external"}.
In other words, for claims with structural and functional components it
must be possible for the skilled person, given any compound satisfying
the structural requirements of the claim, to apply tests without undue
burden, to see if it satisfies the functional requirements of the claim
and thereby work out if it is a claimed compound. The type of
large-scale work required to screen compounds for their functional
features in this case was found to be routine, with the court holding
that this would not constitute an undue burden for a medicinal chemist.

It is noteworthy that the COA found that the original trial judge had
fallen into error when applying step (i) of the three-step test, by
construing the claims too broadly. Proper account had not been taken of
the limitations as to the scope of the claims imposed by the functional
features present, the correct approach to claim construction being that
the claims were directed towards compounds possessing the relevant
structural properties that also had the relevant functional features.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Considerations during examination and search]{#default-id-c972c5d5-heading-45 .govuk-accordion__section-button} {#considerations-during-examination-and-search .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Considerations during examination and search\",\"index_section\":45,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-45 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-45" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Considerations during examination and search\",\"index_section\":45,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.89 {#section-34}

Care should be taken to make it readily apparent what sort of
sufficiency objection is being made. Therefore a classical insufficiency
objection should be raised only in the clearest cases, when the
disclosure appears inadequate to support a valid claim. However where
the claims are unduly broad and speculative objection may be raised
under s.14(3) and/or under s.14(5) [(see also
14.102-14.104)](#ref14-102). Such an objection may not be overcome by
the addition of further examples or features to the specification since
this is prohibited under s.76(2), however an objection to the excessive
breadth of the claims under either section may be remedied by
restricting the scope of the claims. If made under s.14(3) as an
objection to insufficiency due to excessive claim breadth then some
indication that a portion of the claim(s) is regarded as enabled should
be made clear to the applicant. This is not only an issue to be
considered at substantive examination, but the examiner should consider
the scope of the search to be conducted in light of the enabled
disclosure. If the search has been restricted because only a portion of
the claims appear to be enabled then that should be made clear to the
applicant either in the search letter, exam opinion or examination
report as applicable.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Inventions contrary to well-established laws]{#default-id-c972c5d5-heading-46 .govuk-accordion__section-button} {#inventions-contrary-to-well-established-laws .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Inventions contrary to well-established laws\",\"index_section\":46,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-46 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-46" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Inventions contrary to well-established laws\",\"index_section\":46,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.90 {#ref14-90}

If successful performance of the invention is inherently impossible
because it would be contrary to well-established laws (e.g. where the
alleged invention is a perpetual motion machine) objection may arise
under s.14(3). If the claims are directed to its function and not merely
its structure objection may also arise under s.4(1) - see 4.05 and also
Eastman Kodak Co. v American Photo Booths Inc. [BL
O/457/02](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F457%2F02&submit=Go+%BB){rel="external"},
in which the hearing officer held that the invention could not function
as described and claimed, and so lacked both industrial applicability
and sufficiency of disclosure. Similarly, the hearing officers in
Blacklight Power Inc.'s Application [BL
O/170/09](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F170%2F09&submit=Go+%BB){rel="external"},
and Robinson's Application [BL
O/336/08](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F336%2F08&submit=Go+%BB){rel="external"}
held that the applications in question were both insufficient and
lacking in industrial applicability as the claimed inventions relied on
scientific theories of doubtful validity. The hearing officers in these
cases followed the test set out in the Patents Court judgment in
[Blacklight Power Inc. v The Comptroller-General of Patents \[2009\] RPC
6](https://doi.org/10.1093/rpc/rcn035){rel="external"} and held that
there was not a reasonable prospect that the applicant's theory might
turn out to be valid if it were to be fully investigated at a trial --
see 4.05.1-4.05.2. Regardless of whether objection arises under s.4(1)
or s.14(3), one of the procedures set out in 17.94- 17.96.4 should be
followed at the search stage.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Prior art]{#default-id-c972c5d5-heading-47 .govuk-accordion__section-button} {#prior-art .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Prior art\",\"index_section\":47,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-47 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-47" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Prior art\",\"index_section\":47,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.91 {#ref14-91}

The applicant is not obliged to describe or acknowledge the prior art,
since the reader is presumed to have the general background technical
knowledge appropriate to the art. There may however be instances in
which the absence or inadequacy of a statement of prior art renders it
difficult to understand how the invention is to be performed.
Alternatively, documents found during the search or otherwise, may show
that the statement of prior art is inadequate or misleading. This could
be the case if an ambiguous presentation gave the impression that the
prior art had solved less of the problem than was actually the case.

### 14.92 {#section-35}

A specification may include fair and reasonable comment on prior
inventions with a view to distinguishing the invention in suit from them
or illustrating its advantages, but no statement disparaging a prior
patent or describing it in an unfair or misleading manner should be
included. There is however no objection to a mere statement that a prior
invention is in some respects unsatisfactory.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [References to other documents]{#default-id-c972c5d5-heading-48 .govuk-accordion__section-button} {#references-to-other-documents .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"References to other documents\",\"index_section\":48,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-48 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-48" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"References to other documents\",\"index_section\":48,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.93 {#ref14-93}

An application as filed may contain a reference to another document or
webpage, in which further information is to be found. For example, the
application may refer to another document or webpage "the contents of
which are incorporated herein by reference". The allowability of such a
reference must be considered if the further information to be found in
the other document is essential for there to be a clear and complete
disclosure of the invention ([see 14.94](#ref14-94)). On the other hand,
a reference to a document containing information which is not essential
for sufficiency need not be considered; such references are allowable,
even if the referenced document was only made available after the filing
date of the present application and/or it is a patent document referred
to by its application number.

### 14.93.1 {#ref14-93-1}

Pumfrey J in [Halliburton Energy Services Inc v Smith International
(North Sea) Ltd \[2006\] RPC
2](https://doi.org/10.1093/rpc/2006rpc2){rel="external"}, \[2005\] EWHC
1623\](http://www.bailii.org/ew/cases/EWHC/Patents/2005/1623.html) made
it clear that cross-referencing for the purpose of supplementing a
disclosure is highly undesirable, stating that applications should be
complete in themselves (see paras. 61-62 and 30). Since the date at
which sufficiency has to be judged is the date of filing, not the date
of publication ([see 14.63](#ref14-63)), Pumfrey J also held in the same
case that a document referred to in the specification must have been
published by the date of filing the application for a reference to be
effective.

### 14.93.2 {#ref14-93-2}

In [Akebia Therapeutics Inc v Fibrogen Inc \[2020\] EWHC 866
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2020/866.html){rel="external"},
Arnold LJ held that there is no principle of law that the skilled team
are deemed to read all documents cited in a patent. Whether they would
do so depends on the context and the wording of the specification. In
this case, a large number of documents were referenced in the patent,
and the specification gave little, if any, direction as to which would
be worth following up, and so "the skilled team would not necessarily
follow up any of the cited publications." Therefore it cannot be assumed
(for the purpose of sufficiency) that the skilled person will consider
every document referenced in the patent or application.

### 14.94 {#ref14-94}

If the information contained in a referenced document is necessary for a
person skilled in the art to carry out the invention then the examiner
should confirm that the document was published at the filing date. If
not, then objection should be raised under s.14(3) informing the
applicant that references to documents containing essential information
and published later than the filing date, or not at all (including
applications withdrawn before publication and applications unpublished
at the time of filing of the application in suit) should be deleted.
Supplementing or replacing of the reference by an indication of the
contents of the documents (beyond what is already explicitly contained
in the application as filed) is not allowed. Where the publication date
of a reference containing essential information is unclear the applicant
should be asked to verify the date. For instance, in the case of a
reference to a webpage essential for a complete disclosure of the
invention, a copy of the verifiably-dated webpage showing its contents
prior to the date of filing must be provided. Furthermore, in the
situation where it appears to the examiner that a referenced document
might contain information necessary for sufficiency but the document is
not readily available, the applicant should be asked to file a copy and
verify its publication date.

\[If the entire text of another specification is appended to the
application in suit it should be removed from the copy for publication
but should remain on the open part of the file so that it will
subsequently be published by laying open. The following footnote should
be added to the front page of the published application ([see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29)) -
"A specification referred to in the application and appended to it is
not included in this publication but is available for inspection in
accordance with the provisions of Section 118(1) of the Patents Act
1977". At substantive examination the removal from the specification of
the appended specification should be required. Relevant matter from it
may be added to the description if required for sufficiency. \]

### 14.95 {#section-36}

Provided that the publication requirements set out above are satisfied
and if requested by the applicant, the examiner should allow a reference
to be replaced by the matter referred to, provided that, when the
reference is to another application, the matter was present in that
application as filed. Where the language of the documents referred to is
other than English and the disclosure of the invention would not be
clear and complete enough without the reference, such replacement (in
English) should be required. Where the reference is to a document not
readily available to the examiner the applicant should be asked to file
a copy and/or a translation, as necessary.

### 14.96 {#section-37}

There is an indication on the front page of a granted patent
specification if that case relates to a "parent" or divisional
application. There is no reason why applicants should not be encouraged
to include cross-references in other suitable circumstances where the
front page of the granted patent will not give any warning of a related
application or patent, e.g. where another application of the same date
by the same applicant claims matter described in the application in
suit.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Trade marks]{#default-id-c972c5d5-heading-49 .govuk-accordion__section-button} {#trade-marks .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Trade marks\",\"index_section\":49,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-49 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-49" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Trade marks\",\"index_section\":49,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.97 {#ref14-97}

The description should be as clear and straightforward as possible, with
the avoidance of unnecessary technical jargon. Since however it is
addressed to persons skilled in the art to which it relates it is
acceptable, and will often be desirable, for it to use technical terms
which are well known in that art. Little known or specially formulated
technical terms may be used provided they are adequately defined and
that there is no generally recognised equivalent. Foreign terms may be
used where there is no English equivalent. Terms already having an
established meaning should not be used differently if this is likely to
cause confusion, but in some circumstances it may be appropriate for a
term to be borrowed from an analogous art.

### 14.98 {#section-38}

A recognised trade description should not be used in such a way as to
give rise to uncertainty or ambiguity; eg "leather" should not be used
as a general term covering materials resembling leather, unless the
precise meaning given to the word "leather" is defined. If a
specification contains a reference to a proprietary article or specific
product the composition of which is not well known, the description
should state the composition of the article or the way in which it is
prepared. If the applicant maintains that the information is well known
in the art, or if the specification so states, and the examiner is
unable to verify this, evidence in support of the contention may be
required.

### 14.99 {#section-39}

The use of proper names or similar words to refer to materials or
articles is undesirable in so far as such words merely denote origin, or
where they may relate to a range of different products. The product
should be sufficiently identified, without reliance on the word, to
enable the invention to be carried out by the skilled person. Such words
which have generally accepted meanings as standard descriptive terms may
however be used without further explanation; examples are Bowden cable,
Belleville washer, zip fastener.

### 14.100 {#ref14-100}

A trade mark should preferably not be used in a specification since it
is an indication of origin rather than of composition or content and on
that account cannot properly be used to describe an article. (For trade
marks in claims, see 14.137). If a registered trade mark is used it
should generally be accompanied by wording showing that it is a trade
mark, since its use as a descriptive term without acknowledgement may be
prejudicial to the rights of its owner (Official Ruling 1914 (A) 31 RPC
Appendix i). However, a word that has been registered as a trade mark
only requires acknowledgement if it is being used in the specification
with reference to the goods or services for which the trade mark is
registered. Acknowledgement should preferably take the form of "(RTM)"
inserted after the trade mark. However, use of the symbol ® is also
regarded as acceptable. The acknowledging of registered trade marks
should extend to their obvious derivatives. Any word which is a trade
mark should commence with a capital letter. Any statement that a term is
a trade mark or a registered trade mark should not be challenged, nor
should any attempt be made to determine whether such a term is being
used within its registered class; the registered owner may be using the
mark for goods not covered by registration and they may have common law
rights to its use in this way. If a mark is known not to be registered
it is good practice to indicate the name of the owner. The validity of
the trade mark is not material. Community trade marks and international
trade marks registered under the Madrid Protocol (and effective in the
U.K.) should be acknowledged in exactly the same way as trade marks
registered directly with the Office. (For acknowledgement of trade marks
without the applicant's consent, [see
19.24-19.26](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-23).

\[In order to check whether a word used in a specification is a
registered trade mark, the examiner should access the Trade Mark
Database provided on the Office's external website. This contains
details of all trade marks registered or effective in the U.K.,
including Community and international trade marks effective in the U.K.
For advice on how to ensure that trademarks are acknowledged, see
19.24-19.26. [See 14.137](#ref14-137) if a trade mark is used in a
claim\].

### 14.101 {#section-40}

A number of words which are registered trade marks have come into such
general use that the fact that they are trade marks tends to be
overlooked. Examples of such words are 'Bakelite', 'Caterpillar',
'Filofax', 'Frisbee', 'Jacuzzi', 'JCB', 'Kodak', 'Lycra', 'Rollerblade',
'Tabloid', 'Thermos', 'Vaseline', 'Velcro', 'Walkman' and 'Yale'. Such
words should be acknowledged in just the same manner as other less
well-known trade marks.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Relationship of s.14(3) with s.14(5)]{#default-id-c972c5d5-heading-50 .govuk-accordion__section-button} {#relationship-of-s143-with-s145 .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Relationship of s.14(3) with s.14(5)\",\"index_section\":50,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-50 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-50" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Relationship of s.14(3) with s.14(5)\",\"index_section\":50,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
s.125(1) s.76(2) are also relevant

### 14.102 {#ref14-102}

Since "the invention" whose disclosure must be clear and complete is
that defined in the claims, the requirement of s.14(3) may overlap with
those of s.14(5), that the claims define the invention, be clear and be
supported by the description, since all are concerned with the
relationship between the extent of the disclosure and the scope of the
claims.

### 14.103 {#ref14-103}

When considering whether an objection should be made under s.14(3) or
under s.14(5) consideration should first be given as to the extent of
the enabling disclosure. If the objection to be made is clearly that
the, or part of the, claim lacks an enabling disclosure then objection
should be made under s.14(3). If the objection is that the description
casts doubt on the true scope of the invention, in that it causes
genuine difficulty in determining the scope of the claims, then
objection should be made under s.14(5)(c). Many situations will fall
between the two scenarios, but care should always be exercised to ensure
that the invention considered is that of the claims properly construed
rather than simply the embodiment(s). For further information on
embodiments of the invention, [see 14.144](#ref14-144). Also, see
[14.113](#ref14-113). However where the claims are unduly broad and
speculative objection may be raised under s.14(3) and/or under s.14(5)
[(see also 14.79-14.82)](#ref14-79). Such an objection may not be
overcome by the addition of further examples or features to the
specification since this is prohibited under s.76(2), however an
objection to the excessive breadth of the claims under either section
may be remedied by restricting the scope of the claims. When considering
whether an objection should be made under s.14(3) or s.14(5)(b) then
consideration should first be given to whether the unclear word or
phrase creates merely a "fuzzy boundary", as described by Lord Hoffman
in [Kirin-Amgen Inc v Hoechst Marion Roussel \[2005\] RPC
9](https://doi.org/10.1093/rpc/2005rpc9){rel="external"} abstract to the
scope of the claim or whether it renders it impossible to determine what
falls within and without the claim as a whole (see insufficiency by
ambiguity/uncertainty above at [14.79-14.82)](#ref14-79).

### 14.104 {#section-41}

Objection does not arise under s.14(3) merely because particular matter
claimed is absent from the description, since it is the specification
(which includes the claims) which is required to disclose the invention;
objection should in such a case be made under s.14(5)(c), [(see
14.145)](#ref14-145). However while the insertion in the description of
a passage in agreement with an originally unsupported claim may overcome
the objection under s.14(5)(c), objection will remain if this passage is
not in itself a clear and complete enough disclosure of that particular
aspect of the invention, so that the applicant has failed to discharge
their duty under s.14(3) not only to disclose the invention but to do so
in a manner which allows it to be performed.

\[Section 14(4) Repealed\]

### 14.105 {#section-42}

Subsections (4) and (8) of s.14 were concerned with how an invention
which required for its performance the use of a micro-organism could be
disclosed. This is now provided for in s.125A.

  -----------------------------------------------------------------------
   

  **Section 14(5)**

  The claim or claims shall -\
  (a) define the matter for which the applicant seeks protection;\
  (b) be clear and concise;\
  (c) be supported by the description; and\
  (d) relate to one invention or to a group of inventions which are so
  linked as to form a single inventive concept.\
  -----------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 14(6)**
  Without prejudice to the generality of subsection (5)(d) above, rules may provide for treating two or more inventions as being so linked as to form a single inventive concept for the purposes of this Act.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

s.130(7) is also relevant

### 14.106 {#section-43}

S.14(5) and (6) are specified as provisions which are so framed as to
have, as nearly as practicable, the same effect as the corresponding
provisions of the EPC, PCT and CPC; a.84 EPC and a.6 PCT use essentially
the same wording as s.14(5)(a)-(c), and a.82 EPC is essentially the same
as s.14(5)(d). (There is no requirement of unity of invention in the
articles of the PCT, but Rule 13.1 of the Treaty is essentially
equivalent to s.14(5)(d)).

### 14.107 {#section-44}

The provision in the Rules referred to in s.14(6) is to be found in
r.16. There is no article in the EPC or PCT equivalent to s.14(6) but
Rules 44 and 13.2 of the respective treaties use wording similar to that
of r.16. [(See further 14.159)](#ref14-59).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Defining the invention: Clarity]{#default-id-c972c5d5-heading-51 .govuk-accordion__section-button} {#defining-the-invention-clarity .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Defining the invention: Clarity\",\"index_section\":51,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-51 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-51" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Defining the invention: Clarity\",\"index_section\":51,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.108 {#ref14-108}

With the exception of r.16 there is no provision in the Rules regarding
the form and content of claims. Although the EPC and PCT Rules do
contain such provisions, s.130(7) is not interpreted as requiring
compliance with these rules, and indeed some of the provisions of the
EPC and PCT are at variance with UK practice. Rule 43(1) EPC and Rule
6.3 PCT prescribe the so-called Germanic form of claim, in which a
recital of the prior art is followed by a statement of what
characterises the invention. This type of claim is often described as
being in "two-part form", and in the US is sometimes referred to as a
Jepson claim. (The PCT Rule goes on to say that where the national law
of a State does not require this form of claiming, failure to use it
shall have no effect).

### 14.109 {#ref14-109}

In contrast it is established practice in UK law that the form of the
claim is a matter for the applicant, any claim which fulfils the
requirements of the Act being acceptable. Although the Germanic form of
claim may in many cases be the most convenient, particularly for example
when the invention is an improvement in a known type of apparatus, the
applicant cannot be required or urged to present their claim in this
way. In [British United Shoe Machinery Co Ltd v A Fussell and Sons Ltd,
25 RPC](https://doi.org/10.1093/rpc/25.12.368){rel="external"} at page
651, it was stated that "a man must distinguish what is old from what is
new by his claim; but he has not got to distinguish what is old from
what is new in his claim".

### 14.110 {#section-45}

CoP is also relevant

The claims must be drafted in terms of the technical features of the
invention and should not contain any statements relating, for example,
to commercial advantages or other non-technical matters. In addition,
claims should not define the invention over the prior art by unusual,
non-standard or unreasonable parameters against which no comparison with
the prior art can be made, unless the invention does not allow of a
clear alternative definition. Statements of purpose, or reference to
results or desiderata should only be used in claims if they assist in
defining the invention and no better mode of definition is possible. It
is not necessary that every feature should be expressed in terms of a
structural limitation. Functional limitations may be included provided
that a skilled person would have no difficulty in providing some means
of performing this function without exercising inventive skill (see
14.48 for the relation of claims to the description).

### 14.110.1 {#ref14-110-1}

CoP is also relevant

The claims as a whole should aim to define and delimit the features of
the invention, with the independent claims clearly establishing the
essential features of the invention as well as sufficient details of
interrelationship, operation or utility to establish that the invention
achieves the intended objectives. The more independent claims there are,
the more doubt is cast on the essential features of the invention,
particularly where there is more than one independent claim in the same
category, such as multiple independent claims for a product, process,
apparatus, or use. The aim should therefore be to define the essential
features of the invention using a single claim for each category, and to
leave additional non-essential features to dependent claims. Under
certain limited circumstances, this may not be appropriate, for example:
where the invention relates to a number of closely-related products -
such as transmitters and receivers [(see 14.161)](#ref14-161), where
there are different uses of a product or apparatus, or where there are
closely-related alternative solutions to a particular problem. It
follows that claims in a style where many independent claims in one
category are included, all of varying scope and features, are
particularly inappropriate. In addition to lacking clarity, this style
of patent claim may also be considered to lack conciseness [(see
14.140)](#ref14-140) and give rise to plurality of invention [(see
14.157-14.168)](#ref14-157)
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Construction of the claims]{#default-id-c972c5d5-heading-52 .govuk-accordion__section-button} {#construction-of-the-claims .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Construction of the claims\",\"index_section\":52,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-52 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-52" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Construction of the claims\",\"index_section\":52,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
s.125(1) is also relevant

See also [2.11-2.17,
125](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-11),
[125.01-125.24](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-01)
and, with regard to biotechnological inventions,
[76A.07-76A.09](/guidance/manual-of-patent-practice-mopp/section-76a-biotechnological-inventions/#ref76A-07).

### 14.111 {#ref14-111}

The requirement that the claims shall be clear applies to individual
claims and also to the claims as a whole, and is of the utmost
importance in view of the function of the claims in defining the
monopoly sought. Each claim should be read giving the words the meaning
and scope which they normally have in the relevant art. The claim should
also be read with an attempt to make technical sense of it; such a
reading may involve a departure from the strict literal meaning of the
wording of the claim [(see 14.114)](#ref14-114). The claims are to be
interpreted having regard to the description and any drawings ([see also
125](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention)).
If, in a particular case, the description gives the words used in a
claim a special meaning, by explicit definition or otherwise, this
should be clear from a reading of the claim alone; where a special
meaning is given to a term or phrase in a claim by a definition in the
description the use of some such phrase as "as hereinbefore defined"
will reduce the risk of ambiguity. A reference to prior art in the
specification is a factor to be taken into account in interpreting a
patent. In [Ultraframe (UK) Ltd v Eurocell Building Plastics \[2005\]
RPC 7](https://doi.org/10.1093/rpc/2005rpc7){rel="external"} the Patents
Court held that this would depend on the way the prior art was
acknowledged. If the specification identified some particular feature of
the prior patent as disclosing a problem which the inventor claimed to
have overcome, it might be of considerable relevance in interpreting the
width of the claim.

### 14.112 {#ref14-112}

The prima facie meaning of words used in a claim may not be their true
meaning when read in the light either of a definition found elsewhere in
the specification or of technical knowledge possessed by persons skilled
in the art. In these circumstances a claim may bear a meaning different
from that which it would have borne had no such assisting light been
available. Thus, if the draftsperson has specifically indicated
somewhere in the specification what they means by a particular
expression, then that must be taken into account - see [Kirin-Amgen Inc.
v Roche Diagnostics GmbH \[2002\] RPC
1](https://doi.org/10.1093/rpc/2002rpc1){rel="external"}, in which
Neuberger J also cautioned against too heavy a reliance on dictionary
definitions, which are "shorn of any relevant context". The House of
Lords appeal on this case, [Kirin-Amgen Inc v Hoechst Marion Roussel Ltd
\[2005\] RPC 9](https://doi.org/10.1093/rpc/2005rpc9){rel="external"},
confirmed that the meaning of words is what the skilled person would
have understood the author to mean by using these words. Nonetheless,
the starting point for interpreting words -- especially non-technical
words -- in a claim will usually to be their ordinary definition, before
considering their context and use in the specification; as held by the
Court of Appeal in both [Fabio Perini SPA v LPC Group plc and others
\[2010\] EWCA Civ
525](https://www.bailii.org/ew/cases/EWCA/Civ/2010/525.html){rel="external"}
and [Occlutech GMBH and anr v AGA Medical Corp. and anr \[2010\] EWCA
Civ
702](https://www.bailii.org/ew/cases/EWCA/Civ/2010/702.html){rel="external"}.
Further, in [Virgin Atlantic Airways Ltd v Premium Aircraft Interiors UK
Ltd \[2010\] RPC 8](https://doi.org/10.1093/rpc/rcq019){rel="external"},
the Court of Appeal held that the skilled reader is taken to suppose
that the patentee knew some patent law -- that their claim is for the
purpose of defining the monopoly and that it should be for something
new. Knowledge of that may well affect how the claim is construed. For
instance, the patentee would not be expected to have claimed what they
had expressly acknowledged was old.

### 14.113 {#ref14-113}

There is no justification for departing from the unambiguous and
grammatical meaning of a claim and narrowing or extending its scope by
reading into it words which are not in it, or for using stray phrases in
the body of a specification for the purpose of narrowing or widening the
boundaries of the monopoly fixed by the plain words of a claim. For
example, the Court of Appeal in [Fabio Perini SPA v LPC Group plc and
others \[2010\] EWCA Civ
525](https://www.bailii.org/ew/cases/EWCA/Civ/2010/525.html){rel="external"}
rejected an argument that the word "slit" should have a narrowly-defined
meaning based on the exemplification in the patent, rather than the
ordinary meaning of a long, narrow opening. The patentee is under a
statutory obligation to state in the claims what is the invention they
desire to protect. In [Glaverbel S A v British Coal Corporation \[1995\]
RPC 255](https://doi.org/10.1093/rpc/1995rpc255){rel="external"} the
Court of Appeal held that the claims should be read together with the
body of the specification but if a claim is expressed in clear language,
ie the meaning of the claim is clear, the monopoly sought cannot be
extended or cut down by reference to the rest of the specification. This
approach has subsequently been endorsed in, for example, [Lubrizol
Corporation v Esso Petroleum Co. Ltd \[1998\] RPC
727](https://doi.org/10.1093/rpc/1998rpc727){rel="external"} and
[Cartonneries de Thulin SA v CTP White Knight Ltd \[2001\] RPC
6](https://doi.org/10.1093/rpc/2001rpc6){rel="external"}.

### 14.113.1 {#section-46}

In the UK, "consisting of" is generally interpreted to mean "consisting
exclusively of" whilst "comprising" is generally interpreted to mean
"including" (i.e. other integers or features may be present). The
conventional interpretation of "comprising" to mean "including" was
approved of by Kitchin J. in [DLP Ltd's Patent \[2007\] EWHC 2669 (Pat),
\[2008\] RPC 11](https://doi.org/10.1093/rpc/rcn006){rel="external"}.
These terms are considered to be clear unless, on the facts of the
particular case, there is genuine doubt as to their meaning. [See also
14.123.1](#ref14-123-1).

### 14.114 {#ref14-114}

In view of the differences in the scope of protection which may be
attached to various categories of claim (eg directed to a product,
process, apparatus or use), the wording of a claim should leave no doubt
as to its category.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Purposive construction; variation in non-essential features of a claim]{#default-id-c972c5d5-heading-53 .govuk-accordion__section-button} {#purposive-construction-variation-in-non-essential-features-of-a-claim .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Purposive construction; variation in non-essential features of a claim\",\"index_section\":53,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-53 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-53" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Purposive construction; variation in non-essential features of a claim\",\"index_section\":53,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
[see also
125.13-125.14](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-13).

### 14.115 {#ref14-115}

A patent specification should be given a purposive construction rather
than a purely literal one as held by Lord Diplock in [Catnic Components
Ltd and another v Hill and Smith Ltd \[1982\] RPC
183](https://doi.org/10.1093/rpc/1982rpc183){rel="external"} ([see also
125](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125)).
Variation in unessential features of the claimed invention may not be
sufficient to take a product or process outside the protection of the
claim. In Catnic, a claim to a lintel having inter alia a support member
"extending vertically" was held to have been infringed by otherwise
identical lintels in which the support member was 6 o or 8o from
vertical, since this produced a negligible reduction in the vertical
support provided by the member. Another example of purposive
construction was the interpretation of the word "opaque" by Jacob J in
Minnesota Mining and Manufacturing Co. and anr. v Plastus Kreativ AB and
anr. (BL C/64/95; upheld on appeal [\[1997\] RPC
737](https://doi.org/10.1093/rpc/1997rpc737){rel="external"}) where he
construed the term by considering the stated reason for the flap being
opaque, which was to eliminate the disadvantages of the prior art.

However, when considering this case in [Nikken Kosakusho Works v Pioneer
Trading Co. \[2005\] FSR
15](https://uk.westlaw.com/Document/I0D3BAB50E42811DA8FC2A0F0355337E9/View/FullText.html){rel="external"},
Mann J held that although the meaning of a word in a claim can be
qualified or explained by reference to the objective intended to be
realised, this has to be stated clearly enough in the specification;
otherwise the skilled but unimaginative reader, through whose eyes the
patent had to be read, would be confused.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The Protocol to Article 69 of the EPC]{#default-id-c972c5d5-heading-54 .govuk-accordion__section-button} {#the-protocol-to-article-69-of-the-epc .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The Protocol to Article 69 of the EPC\",\"index_section\":54,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-54 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-54" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The Protocol to Article 69 of the EPC\",\"index_section\":54,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
([see also
125.09-125.25](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-09))

### 14.116 {#section-47}

\[deleted\]

### 14.116.1 {#section-48}

For discussion of the Protocol to Article 69 of the EPC, see 125. For a
discussion on Actavis UK Limited and others v Eli Lilly and Company
\[2017\] UKSC 48, see
[125.17.3-125.18.5](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-17-3)
and
[125.26](/guidance/manual-of-patent-practice-mopp/section-125-extent-of-invention/#ref125-26).

### Inventions defined by ranges

### 14.117 {#ref14-117}

In Auchincloss and another v Agricultural & Veterinary Supplies Ltd. and
Others \[1997\] RPC 649, Peter Prescott QC (sitting as a deputy judge)
distinguished (at pages 663-665 and 689) a stated range from the term
"descriptive word or phrase" used by Lord Diplock in Catnic and found
that a departure from this range, however small, is not a variant in the
Catnic sense. He stated (at page 689):-

> The aim of the Catnic line of cases is to ascertain the purpose of the
> patentee, but objectively, that is, through the eyes of the skilled
> reader of the document. Where the patentee has expressed himself in
> terms of a descriptive word or phrase there may be room for supposing
> that he was using language figuratively, and did not intend to
> restrict himself to the purely literal meaning. But where the patentee
> has defined an integer of his claim in terms of a range with specified
> numerical limits at each end, his purpose must be taken to have been
> to claim thus far and no further.

### 14.117.1 {#ref14-117-1}

In Halliburton Energy Services Inc v Smith International (North Sea) Ltd
\[2006\] RPC 2, Pumfrey J held that a claim which defined the axial
force on each cone as being between 31 and 35 per cent of the total
axial force should be construed to mean the specified number to two
significant figures, so including, for example, 30.500 per cent to
35.499 per cent.

### 14.117.2 {#ref14-117-2}

In [Smith & Nephew Plc v Convatec Technologies Inc \[2015\] EWCA Civ
607](http://www.bailii.org/ew/cases/EWCA/Civ/2015/607.html){rel="external"}
Kitchin LJ confirmed that the approach to the interpretation of claims
containing a numerical range is no different from that for any other
claim. That is, the claims are construed to mean what a skilled person
would have understood them to mean. The judge also considered a series
of relevant cases and drew out certain points of particular relevance to
claims which include a numerical range. First, the scope of any such
claim must be exactly the same whether one is considering infringement
or validity. Secondly, there can be no justification for using rounding
or any other kind of approximation to change the disclosure of the prior
art or to modify the alleged infringement. Thirdly, the meaning and
scope of a numerical range in a patent claim must be ascertained in
light of the common general knowledge and in the context of the
specification as a whole. Fourthly, it may be the case that, in light of
the common general knowledge and the teaching of the specification, the
skilled person would understand that the patentee has chosen to express
the numerals in the claim to a particular but limited degree of
precision and so intends the claim to include all values which fall
within the claimed range when stated with the same degree of precision.
Fifthly, whether that is so or not will depend upon all the
circumstances including the number of decimal places or significant
figures to which the numerals in the claim appear to have been
expressed.

### 14.117.3 {#section-49}

The judge went on to determine that in this particular case the claimed
range of "between 1% and 25%" would be understood by a skilled person to
include all values greater than or equal to 0.5% and less than 25.5%.
That is, the claim would include all values which would fall within the
claimed range, once the values are rounded to the nearest whole number.
He found that in the context of the claimed method, the purpose of
expressing numbers to a particular degree of precision was to convey to
the reader the range of permissible values and the accuracy with which
those values need to be determined. The judge dismissed the 'significant
figures' approach (which would have resulted in the claimed range
including all values greater than or equal to 0.95% and less than 25.5%)
in part because this would require values close to the bottom of the
range to be determined with much greater accuracy than those near the
top.

### 14.117.4 {#ref14-117-4}

The above approach was reaffirmed in [Napp Pharmaceutical Holdings Ltd v
Dr Reddy's Laboratories (UK) Ltd \[2016\] EWCA Civ
1053](http://www.bailii.org/ew/cases/EWCA/Civ/2016/1053.html){rel="external"}.
Floyd LJ agreed with the decision in Smith & Nephew Plc, that claims
must be construed to mean what the skilled person would understand them
to mean. All of the figures in the patent were multiples of five. Floyd
LJ stated that even though this is the case, this fact says nothing
about the degree of precision to which these numbers are expressed and
that this does not contradict the skilled person's normal understanding
that numbers written in this way would be treated as expressed to the
nearest whole number. Floyd LJ also stated that the term "about" in a
claim (e.g. about 10%) suggests that something wider was meant than if
the word "about" was omitted and that a more generous degree of
imprecision was claimed.He concluded, agreeing with the decision of the
Patents Court, that "about 10%" in the claim should extend to 9-11%.

### 14.117.5 {#section-50}

For situations in which the application in suit specifies a range which
overlaps with a range disclosed in prior art, see
[2.06.2](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-06-2).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Invention defined by reference to intended use]{#default-id-c972c5d5-heading-55 .govuk-accordion__section-button} {#invention-defined-by-reference-to-intended-use .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Invention defined by reference to intended use\",\"index_section\":55,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-55 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-55" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Invention defined by reference to intended use\",\"index_section\":55,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.118 {#section-51}

A claim to an apparatus or material for a particular purpose is
construed as a claim to any apparatus or material having the features
specified which is suitable for that purpose ([see
2.12-2.14](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-12)).
On the other hand, a claim to something "when used in" a particular
process is regarded as protecting only the use of the invention in this
way ([see
2.15](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-15))
while a claim to "the use of" a material is regarded as equivalent to a
claim to a method of using the material ([see
2.16](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-16)).

### 14.119 {#section-52}

A claim merely directed to "Apparatus for carrying out the method of
.... according to claim X", or some such wording will not normally be
clear in scope. For further discussion of construction of phrases such
as "for", "suitable for" and "adapted to" [see
2.12-2.14.1](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-12)
The claim should normally clearly specify the essential features of the
apparatus unless all the integers which would constitute such apparatus
are clearly implicit in the method claimed, and all such apparatus would
be novel and non-obvious.

### 14.120 {#ref14-120}

The area defined by the claims must be as precise as the invention
allows. As a general rule, claims which attempt to define the invention,
or a feature thereof, by a result to be achieved should not be allowed.
However, they may be allowed if the invention can only be defined in
such terms or cannot be defined more precisely without unduly
restricting the scope of the claims and if the result is one which can
be directly and positively verified by tests or procedures adequately
specified in the description and involving nothing more than trial and
error. In [No-Fume Ltd v Frank Pitchford Co Ltd, 52 RPC 231 (PDF,
2.3MB)](http://rpc.oxfordjournals.org/content/52/7/231.full.pdf+html){rel="external"},
a claim to an ash receptacle for smokers in which the dimensions of
certain parts were such that smoke from objects thrown into the
receptacle did not emanate from the receptacle was allowed on the
grounds that the invention could be realised by dimensions other than
those disclosed, by experiments not involving inventive ingenuity.
However, claims of this kind are generally undesirable and it should be
noted that the No- Fume claim was allowed solely because the invention
did not admit of precise definition independently of the result
achieved. Any claim which includes a subordinate clause prefaced by
words such as "so that" or "the arrangement being such that" requires
special consideration from this point of view. In [BL O/031/17 (PDF,
132KB)](https://www.ipo.gov.uk/p-challenge-decision-results/o03117.pdf){rel="external"},
the hearing officer considered a claim defining a seating assembly for
an aircraft cabin. The claim was defined by the result that a passenger
access path was produced between seats. The hearing officer found it was
possible to define the invention without reference to the result to be
achieved, however defining the invention in any other way would unduly
restrict the scope of the monopoly sought. He also determined that the
claimed result could be easily verified by a person skilled in the art.
Based on these findings, and on the specific facts of the case, he
concluded that the claim was clear despite being defined by result.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Product by Process claims]{#default-id-c972c5d5-heading-56 .govuk-accordion__section-button} {#product-by-process-claims .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Product by Process claims\",\"index_section\":56,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-56 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-56" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Product by Process claims\",\"index_section\":56,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.120.1 {#ref14-120-1}

The House of Lords in [Kirin-Amgen Inc v Hoechst Marion Roussel Ltd
\[2005\] RPC
9](http://rpc.oxfordjournals.org/content/122/6/169.abstract){rel="external"}
held that 'product-by-process' claims (e.g. "Product X obtained by
process Y") should be construed as a claim to the product as such in
line with EPO practice (see Decision [T 150/82 International Flavors and
Fragrances Inc \[1984\] OJEPO
309](http://www.epo.org/law-practice/case-law-appeals/recent/t820150ex1.html){rel="external"};
this is irrespective of whether the term "obtained", "obtainable",
"directly obtained" or an equivalent wording is used. Claims for
products defined in terms of a process of manufacture are allowable only
if the products as such fulfil the requirements for patentability; a
"product-by-process" claim is not rendered novel merely by the fact it
is produced by means of a new process ([see also
2.15](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-15)).
A claim for a patentable product defined by its process of manufacture
is only allowable if the product cannot satisfactorily be characterised
by reference to its structure or composition; if the product can be
defined by other means, an objection under clarity and/or conciseness
should be raised. Product by process claims can be difficult to identify
so care should be taken when assessing claims. If a product claim
includes any method or process steps (even if those steps are not
explicitly defined as 'manufacturing' steps), the claim is a product by
process claim. For example, a claim to "an apparatus comprising features
A and B, where feature A is treated in an oven", is a product by process
claim. In addition, a product by process claim will result if a product
claim incorporates method/process steps as a result of a reference to
another claim.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Chemical cases]{#default-id-c972c5d5-heading-57 .govuk-accordion__section-button} {#chemical-cases .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Chemical cases\",\"index_section\":57,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-57 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-57" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Chemical cases\",\"index_section\":57,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.121 {#ref14-121}

Where the invention relates to a chemical compound it may be
characterised in a claim in various ways, eg by its chemical formula,
or, exceptionally, by its parameters or as a product of a process.
Characterisation of a chemical compound solely by its parameters should,
as a general rule, be allowed only in those cases where the invention
cannot be adequately defined in any other way, for example in the case
of macromolecular chains. In such cases however only parameters usual in
the art should be employed to characterise the compound, since use of
unusual parameters may disguise lack of novelty ([see
2.18-2.20](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-18)
and
[3.88-3.93](/guidance/manual-of-patent-practice-mopp/section-3-inventive-step/#ref3-88)).

### 14.122 {#ref14-122}

Chemical "process" claims should define the starting material, the end
product and also the means adopted for converting the one into the other
(British Celanese Ltd's Application, 51 RPC 192). The definition of a
process in the claims with reference to such "tools of the trade" as
condensation, polymerisation, esterification and sulphonation, or even
by the use of the term "reacting", is permissible provided the
specification contains no reservations affecting the universality of the
process. In Kirin-Amgen Inc v Hoechst Marion Roussel Ltd \[2005\] RPC 9,
the House of Lords held that the protection conferred by a process claim
should extend to products directly obtained by the process in accordance
with EPC Article 64(2) [(see also 14.120.1)](#ref14-120).

### 14.123 {#section-53}

The extent to which the ingredients of a composition need to be
specified in order adequately to define the invention depends greatly on
the subject-matter concerned. Thus a claim to "a pharmaceutical
composition containing compound X together with a diluent or carrier" is
allowable, X being a medically active compound which characterises the
composition, and the diluent or carrier being any material suitable for
the purpose and being choosable by knowledge of the art or by
non-inventive experiment. In the field of alloys, sufficient of the
constituents should be specified such that the claim is not speculative
and is adequately supported by the disclosure.

### 14.123.1 {#ref14-123-1}

In EPO Decision T 589/89, NATIONAL RESEARCH/Polyurethane compositions
(\[1994\] EPOR 17), the use of the word "comprising" was held to mean
that further reactive ingredients may be present in the claimed
composition. However, where the word "consists" is used, the proportions
of the specified ingredients must total 100 per cent (EPO Decision T
711/90, unreported). This is consistent with the long-standing practice
in the UK that "consisting of" is generally interpreted to mean
"consisting exclusively of" whilst "comprising" is generally interpreted
to mean "including" (i.e. other integers or features may be present).
The EPO has interpreted the phrase "consisting essentially of" as
meaning that unspecified components could be present in the claimed
composition if the characteristics of the claimed composition are not
materially affected by the presence of these unspecified components (EPO
Decision T 472/88, GENERAL ELECTRIC/ Thermoplastic resin (\[1991\] EPOR
486), EPO Decision T 340/89, GENERAL FOODS/caffeine (\[1992\] EPOR 199),
EPO Decisions T 522/91 and T 759/91(each unreported)). This settled view
of the EPO is followed in the UK. This has been confirmed by [Anan Kasei
Co Ltd & Rhodia Operations SAS v Molycorp Chemicals & Oxides Ltd
\[2018\] EWHC 843
(Pat)](https://www.bailii.org/ew/cases/EWHC/Patents/2018/843.html){rel="external"}
and the corresponding Court of Appeal decision [Anan Kasei v Neo
Chemicals \[2019\] EWCA Civ
1646](http://www.bailii.org/ew/cases/EWCA/Civ/2019/1646.html){rel="external"}.
Thus a claim to a composition "consisting essentially of X, Y and Z"
could be found to be anticipated if the prior art includes such a
composition which also contains other components which do not appear to
materially affect its characteristics (e.g. its activity or function).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Omnibus claims]{#default-id-c972c5d5-heading-58 .govuk-accordion__section-button} {#omnibus-claims .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Omnibus claims\",\"index_section\":58,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-58 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-58" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Omnibus claims\",\"index_section\":58,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.124 {#ref14-124}

r.12(6A) is also relevant

Claims to the preferred embodiments of the invention which end with some
such words as "substantially as described and shown (or illustrated) in
the accompanying drawings" are limited to the embodiments described and
depicted in the drawings. Such claims fall within the type known as
"omnibus" claims which also include claims referring to examples (eg in
chemical cases) or to tables. As a result of the[Patents (Amendment)
(No.2) Rules 2016 (PDF,
55.7KB)](http://www.legislation.gov.uk/uksi/2016/892/pdfs/uksi_20160892_en.pdf){rel="external"},
it is not possible to include omnibus claims in UK patent applications,
unless this is the only way to define the technical features of the
invention clearly and concisely. If in response to an objection to an
omnibus claim the applicant demonstrates that the invention cannot
otherwise be clearly and concisely defined using words, a mathematical
or chemical formula or any other written means, the examiner should
allow the omnibus claim to remain. An example might be where the
invention involves some peculiar shape, illustrated in the drawings, but
which cannot be clearly defined either in words or by a simple
mathematical formula.

\[Substantive examiners should raise an objection to any omnibus claims
present in a pending patent application, using PROSE clause RC37 unless:

\(a\) A notification of intention to grant has already issued; or\
(b) The examiner is already convinced that the technical features of the
invention cannot otherwise be clearly and concisely defined;

Examiners should note their reasons for not raising an objection in a
minute.\]

### 14.125 {#section-54}

An omnibus claim should not suggest that a drawing, example or table
illustrates or exemplifies the invention if it does not, for example if
it is present for comparison or as prior art, but there is no objection
to referring to the invention "as described with respect to" such
drawings, examples or tables, provided the wording of the claim and of
the description makes the position clear. However the words
"substantially as described" are insufficient by themselves to limit a
claim to the embodiment described, and its scope will be construed to be
as wide as the statement of invention. In such cases care should be
taken to ensure that the invention is set forth in precise terms in the
body of the specification, that ambiguity does not arise [see 14.139.1
and 14.139.2](#ref14-139-1) and that the statement of invention is not
broader than the main claim [see 14.146](#ref14-146). With regard to
omnibus claims of copending applications describing the same apparatus,
[see
18.95](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-95).

### 14.125.1 {#ref14-125-1}

In [Raleigh Cycle Co Ltd and Anr. v Miller and Co Ltd, 65 RPC
141](http://rpc.oxfordjournals.org/content/65/6/141){rel="external"}, an
omnibus claim directed to a generator "constructed, and arranged
substantially as described with reference to and as illustrated in the
accompanying drawings" was construed as a narrow claim, but was held, by
virtue of the qualification "substantially", to have been infringed by a
generator not having stepped stator windings, even though the only
embodiment specifically disclosed did have such windings. In Jansen
Betonwaren B.V. v Ian Robbie Christie ([BL O/496/15 (PDF,
85.5KB)](https://www.ipo.gov.uk/p-challenge-decision-results/o49615.pdf){rel="external"}
the hearing officer considered the validity of an omnibus claim to "A
building block substantially as described with reference to the
drawings." The claim was construed narrowly such that it required the
"four main design features" disclosed in the description and all
features shown in the sole figure. The claim was nevertheless determined
to lack novelty on the basis of prior public use. The hearing officer
also found an even narrower construction of claim 1 was possible. Under
this construction the claim required the building block to be
manufactured using "a mix of concrete sand and cement as well as
elastomer and thermoplastics". The additional limitation rendered the
claim novel over the alleged prior use but resulted in the disclosure
being insufficient.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Disclaimers]{#default-id-c972c5d5-heading-59 .govuk-accordion__section-button} {#disclaimers .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Disclaimers\",\"index_section\":59,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-59 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-59" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Disclaimers\",\"index_section\":59,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.126 {#ref14-126}

A disclaimer is a form of claim limitation. It is an amendment to an
already existing claim comprising the incorporation of a "negative"
technical feature. Typically, this will entail excluding specific
embodiments or areas from a general feature. A disclaimer is allowed if
what remains once the disclaimed material has been subtracted from the
claim is clearly supported; that is, the disclaimer does not add matter
contrary to s.76(2). An amendment to a claim by the introduction of an
"undisclosed" disclaimer, where neither the disclaimer as such nor the
subject matter excluded by it was disclosed in the application as filed,
may be allowable providing certain criteria are met (see 14.127). These
criteria were set out by the EPO Enlarged Board of Appeal in joined
cases G1/03 (Disclaimer/PPG) and G2/03 (Disclaimer/Genetic Systems)
\[2004\] 8-9 OJEPO 413 and \[2004\] EPOR 33. The Board later confirmed
in G2/10 (Disclaimer/SCRIPPS) that if the criteria for allowing an
undisclosed disclaimer were met the subject matter remaining in the
claim after the introduction of the disclaimer would still need to be
disclosed in the application as filed.

### 14.127 {#ref14-127}

The Board held that an undisclosed disclaimer is allowable in the
following circumstances:

a\) Delimiting a claim against an anticipation published after the
priority date. Where a novelty objection is raised under section 2(3),
an undisclosed disclaimer may be allowed to distinguish between an
application and the s.2(3) prior art. The purpose of a disclaimer
excluding a conflicting application is merely to take account of the
fact that different applicants are entitled to patents in respect of
different aspects of inventive subject matter. The disclaimer splits the
invention as a whole into two parts: in respect of the identical part,
it preserves the rights of the first applicant; for the rest, disclosed
for the first time in the later application, it attributes the right to
the second applicant;

b\) Delimiting a claim against an anticipation in an unrelated field
that the skilled person would never take into consideration (i.e. an
accidental anticipation). This occurs when a piece of prior art would be
disregarded by the skilled person, either because it belongs to a
technical field remote from that of the invention, or because its
subject matter suggests it would not help to make the invention. An
undisclosed disclaimer may be allowed to distinguish an application from
prior art such as this, even if cited under s.2(2). Take, for example, a
claimed invention that concerns a large group of chemical compounds with
certain properties which are advantageous for a specific use. One single
compound within that group turns out to be known for a completely
different use and, therefore, only properties irrelevant to the new use
are known. A disclaimer may be used to prevent that one single compound
from presenting a bar to patenting the group. A disclaimer cannot be
used to delimit a claim against an anticipation that is not accidental;

c\) Delimiting a claim against subject matter excluded from
patentability for non-technical reasons, such as methods of treatment of
the human body or inventions contrary to public morality For example, a
method of avoiding offspring of a certain sex would be contrary to
public morality when applied to humans, but not when applied to cows. If
the claims are directed broadly to mammals, an undisclosed disclaimer
may be used to exclude human beings and so avoid objection under s1(3).
A disclaimer that excludes subject matter not eligible for patent
protection may only serve the purpose of removing such specific legal
obstacles. It is unlikely that a disclaimer can be used to disclaim
excluded matter in general in order to meet to the requirements of
novelty, inventive step, clarity and support, which all claims must
satisfy;

Undisclosed disclaimers cannot be used to delimit a claim against non
working embodiments. If a claim is directed to a large number of
alternatives, some of which do not work, then either the specification
will contain sufficient criteria for finding appropriate alternatives
over the claimed range or there are problems relating to the sufficiency
of disclosure of the invention or the level of inventive step. A
disclaimer is inappropriate for dealing with either of these situations.
Furthermore, the disclaimer should not remove more than is necessary
either to restore novelty or disclaim excluded subject matter. A
disclaimer that was relevant to assessment of inventive step or
sufficiency of disclosure would add subject matter and would not be
allowed.

The approach in G1/03 was affirmed by the Board in G1/16. In this
decision, the Board also held that an undisclosed disclaimer "must not
provide a technical contribution to the subject matter disclosed in the
application as filed. In particular, it may not be or become relevant
for the assessment of inventive step or for the question of sufficiency
of disclosure.

The decision of the Board was considered in M-Systems Flash Disk
Pioneers Ltd v Trek Technology (Singapore) Pte Ltd (BL O/318/06), where
amendment by undisclosed disclaimer was held in principle to be
allowable to limit a claim in the light of conflicting prior art under
s. 2(3), although the hearing officer was unable to exercise discretion
to allow the amendment on other grounds. In [Sudarshan Chemical
Industries Ltd v Clariant Produkte (Deutschland) GmbH \[2014\] RPC
6](http://rpc.oxfordjournals.org/content/131/3/171.full.pdf%2Bhtml){rel="external"}
the Court of Appeal upheld the view of the lower court that the
disclaimer resulted in a monopoly of ambiguous and uncertain scope and,
having considered the board decision in G1/03, that it resulted in the
disclosure of added matter. It was held that, far from the prior art
disclosure being in a remote technical field, the prior art was in fact
the most directly relevant earlier disclosure and the starting point
from which the invention of the patent in suit had been made. In other
words the circumstances in this case were not those described in [14.127
b](#ref14-127)) above. The use of disclaimers for an 'accidental
anticipation' (as described in 14.127 b) was further discussed in the
EPO Board of Appeal decision T 1218/14 (Dohler/DuPont Nutrition
Biosciences). It was noted that to allow such a disclaimer the
disclosure must be so unrelated and remote (from a technical point of
view) that the skilled person would never have taken it into
consideration when making or working on the invention. In this case, the
fact that the invention was considered inventive over the disclosure
(which was a secondary document in a mosaic) did not mean the disclosure
was sufficiently remote from the invention that a disclaimer was
allowable.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Clarity]{#default-id-c972c5d5-heading-60 .govuk-accordion__section-button} {#clarity .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Clarity\",\"index_section\":60,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-60 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-60" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Clarity\",\"index_section\":60,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.128 {#ref14-128}

A claim is bad if it contains internal contradictions. The EPO Technical
Board of Appeal has held that a claim to a composition containing, inter
alia, 40-95% of polyamide was not clear since, even if the other
essential constituents were present in the minimum quantities specified
the maximum amount of polyamide that could be present was 89% (Decision
T02/80; OJEPO 10/81).

### 14.129 {#ref14-129}

A claim should not include vague or equivocal forms of wording which
leave the reader in doubt as to the exact scope of a feature. Examples
of this are relative terms such as "thin", "wide", "strong". If such
terms appear in a claim it is usually necessary to have them either
defined or excised. No objection arises, however, if the relative term
has a recognised meaning in the art, eg "high-frequency amplifier", and
this is the meaning intended. The term "predetermined" may need
consideration: it may be meaningful in some contexts, e.g. to
distinguish between fixed and variable values, but meaningless in other
contexts, e.g. where all comparable values are fixed. In Nikken
Kosakusho Works v Pioneer Trading Co. \[2006\] FSR 4, a tool chuck had
"an annular groove of predetermined depth", and it was held that this
phrase was objectionable and in itself implied nothing about either the
criteria for choosing the depth, or the range of values it should have.
On the other hand, in Folding Attic Stairs Ltd v Loft Stairs Co. Ltd.
\[2009\] FSR 24, the Patents Court held that a claim to a manufacturing
process wherein an element was spaced at "a preset distance" from
another element was meaningful in the context of the claim; it meant the
spacing between these elements was selected by the manufacturer with a
specific aim as described in the specification. The term "predetermined
size" in the same claim was also held to be clear in context. Hence, if
it is clear from the description that these factors are essential to the
performance of the invention, they should be made explicit when
clarifying the claims.

### 14.129.1 {#ref14-129-1}

Although the claims are interpreted in the light of the description, it
was held in IGT/Acres Gaming Inc.'s Application \[2008\] EWHC 568 that
if a claim seems to bear a meaning that is, in fact, inconsistent with
its real meaning when read in the context of the whole document, the
claim is obscure and open to objection under s.14(5)(b) because the
reader may be misled as to its scope.

### 14.130 {#section-55}

Generalising expressions such as "substantially" or (applied to
numerical data) "about" should be construed, both as regards the extent
of the monopoly and the relationship between the invention and the prior
art, according to the subject-matter and the context. They may be
allowable if they do not render the scope of the claims indeterminate.
In PLG Research v Ardon, \[1995\] RPC 287 Aldous J. applied the Catnic
principle in holding that "substantially uniplanar" did not exclude an
insubstantial departure from uniplanarity due, for example, to features
inherent in manufacture. "Uniplanarity" should be judged on the basis of
the eye of the skilled addressee, who would judge a departure by its
size and quality. On the other hand, where such generalising expressions
are inappropriate, eg a reference to "an alkyl group containing about
five carbon atoms", objection should be raised.

### 14.131 {#section-56}

The question as to whether such terms as "back", "front", "above",
"upwardly", are allowable or whether they introduce uncertainty into the
claim, must be decided upon the facts of the case. Particular care is
needed when for example the location of a feature of the invention is
defined by reference to apparatus not forming part of the invention
claimed or even by reference to a person using the invention, although
there will be many instances when such references are rendered clear by
the inclusion of expressions such as "when in use", or "when held by an
operator".

### 14.132 {#section-57}

Under certain circumstances, claims may define a limited number of
features of which one or more may be selected ([see
14.123](#ref14-123)). However, where a claim includes features defined
in purely optional terms such as "preferably", "for example", "such as",
or "more particularly" an objection should be raised. Such optional
features do not impose any restrictions on the scope of the claim.
Consequently, such terms are objectionable as they do not serve a
purpose in the claim such that the claim cannot be considered to be
concise ([see 14.140-14.141](#ref14-140)). Where there are multiple
optional features, either in a single claim or across the claim set,
such that there is difficulty in construing the claim or claims, a
clarity objection may also be warranted. Optional features should
ideally be removed from claims and made the subject of new dependent
claims if it is desired to claim the feature(s).

\[ Additional care should be taken when examining an application with a
large number of claims or where it appears that optional features have
been included purely with the objective of minimising excess claims
fees, although there is no objection to be raised under the Act
specifically in relation to the apparent avoidance of such fees.\]

### 14.133 {#ref14-133}

A claim whether independent or dependent, can refer to alternatives
provided that this does not make the claim obscure or difficult to
construe [(see also 14.164)](#ref14-164). However, such claim
formulations should be avoided if, by reason of the large number of
alternatives, the generality of the claim is impossible to search in its
entirety. Markush claims are an example of this type of claim: such
claims set out a number of alternatives (possibly using words such as
"selected from the group consisting of ..."). They are often used in
chemical cases as a way of setting out various functionally- equivalent
alternatives in one or more parts of the chemical compound being
claimed.

### 14.134 {#ref14-134}

A claim whose wording suggests that it is appendant to a preceding
claim, so that it purports to incorporate all the features of the
preceding claim, but which on inspection is found not to be properly so
appendant (since, for example, it states that a feature of the preceding
claim is omitted or replaced by a different feature, so that the claim
is not fully limited by the terms of the preceding claim) may be open to
an objection of inconsistency. However a claim which expressly states
that it is directed to a modification of the subject-matter of an
earlier claim and particularises the modification may be acceptable,
provided the scope of the claim is clear. Care should be taken to ensure
that a search carried out in respect of a claim which is modified in
this way by a later claim has covered the matter claimed in the later
claim. [(See also 14.164)](#ref14-164).

### 14.135 {#ref14-135}

When a claim includes reference letters or numerals used in the
description and drawings these should not influence the construction of
the claim, but should be taken as a helpful identification of features
in the specific embodiment which may help a reader orient themselves at
the stage when they are trying to work out what the patent is about (as
held in [Virgin Atlantic Airways Ltd v Premium Aircraft Interiors UK Ltd
\[2010\] RPC
8](http://rpc.oxfordjournals.org/content/127/3/192.abstract){rel="external"})
and confirmed in [Jarden Consumer Solutions (Europe) Ltd v SEB SA & Anor
\[2014\] EWCA Civ
1629](http://www.bailii.org/ew/cases/EWCA/Civ/2014/1629.html){rel="external"}).
The only restriction brought about by the inclusion of such reference
letters or numerals is that the claim must be interpreted so as to
include the specific example [(Rodi and Wienenberger AG v Henry Showell
Ltd, \[1966\] RPC at page
453)](https://academic.oup.com/rpc/article/83/17/441/1645493){rel="external"}.
No objection should be raised to the presence of the references. (In
contrast applicants for patents under the EPC or the PCT are urged, by
Rules 43(7) and 6.2(b) of the respective treaties, to use references in
their claims. The PCT rule goes on to say that such references may be
removed by a designated Office for the purposes of publication; this is
not the practice of the UK Office).

### 14.136 {#section-58}

Although ideally it should be possible to ascertain the scope of any
claim of a specification without recourse to other documents, (and
certainly without recourse to evidence of what a patentee intended a
claim to mean - see Glaverbel v British Coal Corporation and anr
\[1995\] RPC 255), the inclusion of a reference to an earlier
specification as one of the integers of a claim is not completely
precluded. The overriding consideration is the clarity of the claim. A
reference to a feature "as described in" an earlier document will not
normally be clear in scope. It should also be remembered that if the
reference is to a claim of an earlier patent specification, this may
subsequently be amended, thereby altering the scope of the claim under
consideration. The availability of the other document and inconvenience
to a reader should also be borne in mind; in particular, objection
should always be raised if the document referred to is in a foreign
language. (Whether a reference can be replaced by matter from an earlier
specification depends on its date of publication - [see
14.85)](#ref14-85).

### 14.136.1 {#ref14-136-1}

Claims defined by an industry or technical standard which could change
over time should generally be objected to under clarity. This was
discussed in paragraphs 26-28 of the hearing officer's decision in
Bilgrey Samson Ltd's Application, [BL
O/577/01](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F577%2F01&submit=Go+%BB){rel="external"}.
However, as with the guidance on the use of trade marks in claims (see
14.137 below), references to standards may be allowable in limited
circumstances where the reference would be clear to the skilled person
and there is no danger of confusion for third parties at a later date,
e.g. where the standard is identified with a specific version or edition
number.

### 14.137 {#ref14-137}

Since a trade mark is indicative of the origin of goods rather than of
their content or composition, the use of a Trade Mark in a claim should
generally only be permitted where the applicant is able to show that its
use is unavoidable and does not introduce ambiguity. However, in limited
circumstances such as when trade marks relate to industry or technical
standards which specify the technologies identified by those trade
marks, for example Bluetooth and WiFi, references to these trade marks
may be allowable (see also 14.136.1 above).\
\[If a Trade Mark is used in a claim, RC12 should be added to RC11 [(see
14.100- 14.101](#ref14-100),
[19.25-19.26](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-25)
Senior Examiners may decide to allow a reference to a Trade Mark in a
claim without consulting their Group Head but Examiners should only
allow such references with the concurrence of their senior officer.\]

### 14.138 {#ref14-138}

Claims containing two or more sentences have always been resisted on the
grounds of ambiguity. In Leonard's Application \[1966\] RPC 269, it was
held that claims consisting of disjunctive sentences would of necessity
give rise to uncertainty as to the precise scope of the monopoly sought,
but a claim was eventually allowed directed to "an automatic change
speed transmission having all the following characteristics in
combination ...." followed by numbered paragraphs (each a separate
sentence) setting out the characteristics of the transmission.

### 14.139 {#ref14-139}

CoP is also relevant

Claims directed to "Any novel matter ..." or which are similarly
directed are unacceptable and should not be filed as they serve no
useful purpose: in particular such claims cannot be relied upon to form
the sole support for claiming a broad scope of protection in a
subsequent divisional application. These claims should be ignored at the
search stage and their removal should be required before the application
proceeds to grant.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Impact of the specification on the clarity of the claims]{#default-id-c972c5d5-heading-61 .govuk-accordion__section-button} {#impact-of-the-specification-on-the-clarity-of-the-claims .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Impact of the specification on the clarity of the claims\",\"index_section\":61,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-61 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-61" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Impact of the specification on the clarity of the claims\",\"index_section\":61,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.139.1 {#ref14-139-1}

[Moved to 14.144](#ref14-144)

### 14.139.2 {#section-59}

[Moved to 14.144](#ref14-144)

### 14.139.3 {#ref14-139-3}

[Moved to 14.144.1](#ref14-144-1)
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Conciseness]{#default-id-c972c5d5-heading-62 .govuk-accordion__section-button} {#conciseness .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Conciseness\",\"index_section\":62,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-62 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-62" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Conciseness\",\"index_section\":62,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.140 {#ref14-140}

CoP is also relevant

The requirement that the claims shall be concise refers to the claims in
their entirety as well as to the individual claims. The number of claims
must be considered in relation to the nature of the invention the
applicant seeks to protect. In Contra Vision Ltd's Patent ([BL
O/079/00](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F079%2F00&submit=Go+%BB){rel="external"})
a request to amend the claims was refused partly on the grounds that the
proposed amendments would have resulted in sixty-eight independent
claims and a four-fold increase in the total number of claims, and thus
the claims would not have been concise. A lengthy statement of claim in
which the wording of one claim is repeated to an unnecessary extent is
open to objection (Bancroft's Application 23 RPC 89). It follows that a
statement of claim is not allowable if it is framed on the American
system to include a long series of claims which are independent of each
other yet almost identical in subject-matter. Multiple independent
claims of slightly varying but overlapping scope also open up the need
for analysis of subject matter common to those independent claims, with
the result that plurality of invention may be found [(see
14.157-14.168)](#ref14-157).

### 14.141 {#section-60}

Each claim should cover some area of subject-matter not the subject of
another claim, and objection should be raised when two or more claims
are coterminous. While the Courts will if possible construe the claims
so as to give a different meaning to each one, if little or no
difference can be found between two of the claims, this affords no
ground for departing from the reasonable and natural meaning of the
language.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Support by the description]{#default-id-c972c5d5-heading-63 .govuk-accordion__section-button} {#support-by-the-description .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Support by the description\",\"index_section\":63,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-63 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-63" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Support by the description\",\"index_section\":63,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.142 {#ref14-142}

The words "supported by the description" were new in the 1977 Act,
replacing the 1949 Act requirement that the claims had to be fairly
based on the matter disclosed in the specification. In Schering Biotech
Corp's Application \[1993\] RPC 249 Aldous J felt that it was not right
to rely on cases decided under the "fairly based" requirement and
emphasised the importance of coming to the right decision since a
patent, when granted, cannot be attacked on this ground ([see
14.152](#ref14-152)).

### 14.142.1 {#ref14-142-1}

The view of the EPO Technical Board of Appeal in AgrEvo UK Ltd (T 939/92
OJEPO 6/96) was that "support by the description" means that the
technical features stated in the description as being essential features
of the described invention must be the same as those used to define the
invention in the claims, for otherwise the claims would not be true
definitions but mere descriptions. This view that support is purely a
drafting matter does not clearly correspond, for example, with the view
of Aldous J in Schering Biotech Corp's Application \[1993\] RPC 249
[(see 14.149)](#ref14-149). Thus, until such time as the courts endorse
the EPO's interpretation of support in AgrEvo UK Ltd, the guidance given
below should be followed.

### 14.143 {#section-61}

Most claims are generalisations from one or more particular examples.
The extent of generalisation permissible is a matter which must be
judged in each particular case in the light of the relevant prior art.
Thus an invention which opens up a whole new field is entitled to more
generality in the claims than one which is concerned with advances in a
known technology. A fair statement of claim is one which is not so broad
that it goes beyond the invention nor yet so narrow as to deprive the
applicant of a just reward for the disclosure of their invention. The
applicant should be allowed to cover all obvious modifications,
equivalents to and uses of that which they have described. In
particular, if it is reasonable to predict that all the variants covered
by the claims have the properties or uses the applicant ascribes to them
in the description they should be allowed to draw their claims
accordingly.

### 14.144 {#ref14-144}

Objections should only be raised relating to an inconsistency between
claims and description when the inconsistency causes genuine difficulty
in determining the scope of the claims. Examples where this might (but
not always) occur are:

-   the description may state, or may imply, that a certain technical
    feature not mentioned in the main claim is essential to the
    performance of the invention. In such a case, the claims should
    normally be amended to include this feature. If however the
    applicant can show convincingly that it would be clear to a person
    skilled in the art that the description was incorrect in suggesting
    that the feature in question was essential, and if the main claim
    which implies (by omission) that the feature is not essential was
    present on the date of filing, amendment of the description may be
    allowed instead

-   if it is difficult to determine whether an embodiment falls within
    the scope of the claims, then an objection should be made. However,
    in general, an objection should not be raised if an embodiment falls
    outside the scope of the claims as the claims should clearly define
    the monopoly sought.

### 14.144.1 {#ref14-144-1}

No objection is required if the description contains any general
statement stating that the scope of protection includes equivalents.
[Actavis UK Limited and others v Eli Lilly and Company \[2017\] UKSC
48](http://www.bailii.org/uk/cases/UKSC/2017/48.html){rel="external"}
sets out clear criteria for the courts to determine equivalents.

### 14.145 {#ref14-145}

Where certain subject-matter is clearly disclosed in a claim of the
application as filed, but is not mentioned anywhere in the description,
it is permissible to amend the description, provided the amendment does
not give rise to any other objection, so that it includes this
subject-matter. When however original claims filed later than the filing
date of the application contain matter not present in the description,
such matter will need to be deleted. Objection should be raised under
s.14(5)(c), rather than under s.76(2) since later-filed original claims
do not constitute an amendment of the application (but any
subsequently-filed claims do, see 15.54).

### 14.146 {#ref14-146}

Many applicants choose to include an opening statement or "consistory
clause" or "statement of invention" setting out the nature of the
invention in the description. The applicant however is not required to
include such a statement if the description indicates without ambiguity
the nature of the invention. Sometimes there are inconsistencies between
the claims and statements of invention that are provided. Examiners
should only object to such inconsistencies when they cause genuine
difficulty in determining the scope of the claims. This is expected to
occur only rarely. If, however, the description of the invention
contains generalising statements and the specification contains an
omnibus claim [(see 14.124-14.125)](#ref14-124) not of the narrow form
discussed in Raleigh Cycle Co Ltd and Anr v Miller and Co Ltd, 65 RPC
141 then an opening statement defining the invention is necessary. (In
the Raleigh Cycle specification the omnibus claim was directed to a
generator, "constructed, and arranged substantially as herein described
with reference to and as illustrated in the accompanying drawings").

### 14.147 {#ref14-147}

\[Deleted\]

### 14.148 {#ref14-148}

(CoP is also relevant)

The use of a compact style of consistory clause, which imports a
reference to one or more claims into the statement of invention, is
strongly encouraged since this avoids repetition. When the claims are
read in their context as part of the description the true scope of the
invention should not be in doubt. If such reference includes independent
claims care should be taken that they do not import uncertainty into the
description of the invention (United Shoe Machinery Application 57 RPC
71).

### 14.149 {#ref14-149}

In Glatt's Application \[1983\] RPC 122 [(see 14.151(a)](#ref14-151)) it
was held that if claims are put forward which cover something which
plainly was never within the contemplation of the invention as described
in the specification then they lack support. This view was reinforced by
Aldous J in Schering Biotech Corp's Application ([see
14.142](#ref14-142)) when it was held that the correct approach was to
consider the claims in the specification through the eyes of the skilled
person in the art, to ascertain what is the invention which is specified
in the claims, compare that with the invention described in the
specification and thereafter decide whether the invention in the claims
is supported by the description. Mere mention in the specification of
features appearing in the claim is not necessarily sufficient support.
"The word 'support' means more than that and requires the description to
be the base which can fairly entitle the patentee to a monopoly of the
width claimed." This approach was believed by Aldous J to be consistent
with that of the EPO in Biogen NV v Hoffmann-La Roche & Co. AG (T301/87
OJEPO 8/90). It is also consistent with the finding of the Court of
Appeal in Biogen v Medeva \[1995\] RPC 25 at p 87, regarding the
implications of quoting a claim verbatim in the description.

### 14.150 {#section-62}

If it appears that the description is inadequate to support a broad
claim, it is possible to argue either that the disclosure is not clear
and complete enough or that the claim is not supported by the
description. In general a classical sufficiency objection [(see
14.67-14.75)](#ref14-67) should be raised under s.14(3) only in the
clearest cases, when the disclosure appears inadequate to support a
valid claim. However where the claims are unduly broad and speculative
objection may be raised either under s.14(3) as insufficiency through
excessive claim breadth and/or under s.14(5) ([see also
14.79-14.82](#ref14-79)). Such an objection may not be overcome by the
addition of further examples or features to the specification since this
is prohibited under s.76(2), however an objection to the excessive
breadth of the claims under either section may be remedied by
restricting the scope of the claims [(See also
14.102-14.104)](#ref14-102).

### 14.151 {#ref14-151}

The following are examples of cases where the relationship of the claim
to the description has been considered:-

-   \(a\) In Glatt's Application, \[1983\] RPC 122, an article for
    conditioning fabrics in a laundry dryer and comprising a flexible
    woven or non-woven sheet having on it areas of fabric conditioning
    composition was described in a way which indicated that it was an
    essential feature that the material of the sheet be permeable to
    air. A claim which was silent as to the permeability of the sheet
    was held by the Patents Court to be not supported by the description
    [(see 14.149)](#ref14-149)

-   \(b\) In Universite Rene Descartes ([BL
    O/147/88](https://www.gov.uk/government/publications/patent-decision-o14788))
    it was decided that to insert into a claim a specific numerical
    example, not expressly stated in the description, would result in a
    claim which was not supported by the description. (With regard to
    the narrowing of a claim to a sub-range not specified before, [see
    18.69-
    18.69.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-69).

-   \(c\) In A C Edwards Ltd v Acme Signs & Displays Ltd \[1990\] RPC
    621 and \[1992\] RPC 131 it was held that the invention of claim 1
    (which was substantially amended pre-grant) was not directed to a
    different inventive concept than that disclosed in the original
    application. There was therefore support for the amended claim in
    the description. (In practice this decision may give support, in
    certain circumstances, for an intermediate generalisation - the case
    related to a 7 element digital display in which any required digit
    could be displayed using flaps which cover/uncover the elements. As
    described each flap was attached to a baseboard by two studs. The
    originally filed claim 1 was silent in respect of the studs but the
    amended claim 1 included, for each flap "a stud". It was held that
    this limitation to claim 1 "did not disclose use of a single stud
    any more than did claim 1 of the application". [See also
    76.15-15.1](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-15).

-   \(d\) In Raychem Ltd's Applications \[1986\] RPC 547 the applicant
    sought to amend claim 1 by deleting the final step in a process and
    thus claim an intermediate product. It was held that the amended
    claim would not be supported by the disclosure which clearly
    disclosed the final step as an essential feature of the invention.
    It was held that such a claim must also offend against Section 76
    ([see also
    76.17](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-17)).
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Broad or speculative claims]{#default-id-c972c5d5-heading-64 .govuk-accordion__section-button} {#broad-or-speculative-claims .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Broad or speculative claims\",\"index_section\":64,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-64 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-64" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Broad or speculative claims\",\"index_section\":64,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.152 {#ref14-152}

An applicant does not have to restrict their claims to the specific
embodiment described, but the width of the claims must be properly
supported by the description of the invention in the specification (for
discussion of insufficiency by excessive claim breadth [see
14.79](#ref14-79) onwards). A claim can only be supported by an enabling
disclosure (Asahi Kasei Kogyo KK's Application \[1991\] RPC 485, at page
536). When claims are broad and speculative, in that their scope extends
beyond the description to embrace possibilities the effects of which
cannot readily be predetermined or assessed on the basis of what is
described, and the description gives merely an indication of the full
breadth of scope of the invention but no, or inadequate, directions of
how to put it into practice across the range claimed, objection should
be raised that such claims are not supported by the description. In
Esau's Application, 49 RPC 85 a claim for apparatus for influencing
substances by means of high frequency electrical energy was refused
since it embraced any kind of influence on any kind of substance.
Eventually the law officer allowed the claims to be redrafted to cover a
process for the aggregation of fine particles from gases or liquids for
the purpose of effecting their removal. The law officer's remarks on
page 87, lines 42-47 about broad and indeterminate claims are invoked
again in Shell Development Co's Application, 64 RPC at page 154 and in
General Electric Co Ltd's Application \[1961\] RPC at page 24. In
Schering Biotech Corp's Application (see [14.142](#ref14-142), the judge
rejected the wider of two alternative claims 1 as unsupported: whereas
the invention was the use of a new insert in a vector to produce a
polypeptide having a certain activity, passages suggesting that by using
that new insert other inserts of unknown code and amino-acid sequence
could be found did not provide a description adequate to support a claim
to a monopoly covering the other inserts. It was unjustified to claim
the use of all vectors producing the required result, where only one
vector had been investigated. This followed the judgment in the Court of
Appeal in Genentech Inc's Patent \[1989\] RPC 147 concerned with genetic
engineering, in which Dillon L J observed (at page 236-7) "the Patent
Office ought to have very clearly in mind that it is undesirable to
allow claims the object of which is to cover a wide and unexplored field
or where there is no disclosure in the specification which is in any way
coterminous with the monopoly indicated in the claims." The reasons of
the three judges in Genentech in finding all claims invalid differed,
but they were unanimous that the reason relied on by Whitford J, ie that
the claims were not supported by the description, was not a ground for
revocation under s.72(1). Mustill L J observed (at page 261) that
"grounds of objection have always been prone to overlap, and that the
very same factors may lead to an irredeemable flaw in the patentability
of the supposed invention, and to an impossibility of framing an
application which complies with section 14(5)". Subsequently, in Chiron
Corpn v Organon Teknika \[1994\] FSR 202 Aldous J observed that
s.72(1)(c) could not provide grounds for attacking a claim where in
essence the complaint was one of lack of support; it being important
that the effect of s.72(1) should be the same as the equivalent
provision applied in many other European countries. In the case of
medical inventions having at their heart a medical use ([see
4A.16-4A.31](/guidance/manual-of-patent-practice-mopp/-section-4a-methods-of-treatment-or-diagnosis/#ref4A-16)),
the description should not only identify a condition that may be treated
but also demonstrate by reference to tests that the treatment is a
reality and not just a possibility (Hoerrmann's Application \[1996\] RPC
341, Consultant Suppliers Ltd's Application \[1996\] RPC 348). However,
in Prendergast's Applications (\[2000\] RPC 446) Neuberger J emphasised
that rudimentary tests would suffice and that full, detailed and
rigorous testing of the drug for the proposed condition is not
necessary.

### 14.153 {#ref14-153}

In Pottier's Application \[1967\] RPC 170 a broad claim to: 'A process
for the treatment of hydrated seedlings which comprises subjecting the
seedlings to cold shock at a temperature below 0°C for a period
sufficiently long to affect the size of the resulting plant', was
refused, following Esau's Application [(see 14.152)](#ref14-152) because
the claim was broad and speculative. The treatment of sugar beet
seedlings only was described. Such a claim might be permissible if it
were made clear in the description that the conditions set forth in
relation to that plant applied to other plants generally; but otherwise
the claim would not be regarded as adequately supported unless the
description gave a sufficient range of examples, relating to different
kinds of plants, to enable a horticulturist to deduce how the process
should be applied to virtually any plant. In Amchem's Products Inc's
Patent, \[1978\] RPC 271, where claim 1 was directed to a process for
increasing the resistance of any plant to any disease or internal
malfunction by treating it with one or other of a number of specified
compounds in an amount sufficient to increase the resistance, and the
description referred only to protecting tomatoes, cucumbers and potatoes
against particular diseases, it was held that the invention claimed had
not been sufficiently and fairly described.

### 14.154 {#ref14-154}

Claims directed to all solutions of a problem are not allowable unless
invention lies in the identification of the problem. In N V de
Bataafsche Petroleum Maatschappij's Application, 57 RPC 65, it was an
essential feature of a claim to a process for impermeabilizing and
tightening soils etc, that an aqueous dispersion of a bituminous
substance forming part of a mixture caused to penetrate the soil and
coagulate therein was "suitably stabilized". No general instructions
were given in the specification as to how this was to be done and the
claim was held to include "every method of achieving that suitable
stability by achievement whereof the problem is to be solved" and to be
invalid by the same reasoning as that promulgated by Lord Parker in
British United Shoe Machinery Co Ltd v Simon Collier Ltd 26 RPC pages
48-51. In Chemische Fabrik auf Aktien's Application, 45 RPC 403, a claim
to a chemical process using "a catalyst" and thus embracing any catalyst
which would give the desired result was disallowed. In David Kahn Inc v
Conway Stewart & Co Ltd \[1974\] RPC at pages 319-320 it was stated that
"A patentee may rightly claim a monopoly wider in extent than what he
had invented. If he has discovered a general principle or invented a
general method and discloses one way of carrying it out, he may claim
all ways of carrying it out, but he is not entitled to claim a monopoly
more extensive than is necessary to protect what he has himself said is
his invention. He cannot claim all solutions to a problem unless
invention lies in identification of the problem".

### 14.155 {#section-63}

A claim in generic form ie relating to a whole class eg of products or
machines, may be acceptable even if of broad scope, if there is fair
support in the description, and there is no reason to suppose that the
invention cannot be worked through the whole of the field claimed. Where
the information given appears inadequate to enable the skilled person to
extend the teaching of the description to parts of the field claimed,
but not explicitly described, by using routine methods of
experimentation or analysis, the applicant should be required to show
that the invention can in fact be readily applied on the basis of the
information given, over the whole field claimed, or, failing this, to
restrict the claim to accord with the description. An example of this
might be a claim to a specified method of treating "synthetic resin
mouldings" to obtain certain changes in physical characteristics. If all
of the examples described related to thermoplastic resins and the method
was such as to appear inappropriate to thermosetting resins, then
restriction of the claims to thermoplastic resins might be necessary.

### 14.156 {#section-64}

A claim may broadly define a feature in terms of its function, even
where only one example of the feature has been given in the description,
if the skilled reader would appreciate that other means could be used
for the same function. For example, "terminal position detecting means"
in a claim might be supported by a single example comprising a limit
switch, it being obvious to the skilled person that eg a photoelectric
cell or a strain gauge could be used instead. In general, however, if
the entire contents of the application are such as to convey the
impression that a function is to be carried out in a particular way,
with no intimation that alternative means are envisaged, and a claim is
formulated in such a way as to embrace other means, or all means, of
performing the function, then objection arises. Furthermore, it may not
be sufficient if the description merely states in vague terms that other
means may be adopted, if it is not reasonably clear what they might be
or how they might be used.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Reach-through claims]{#default-id-c972c5d5-heading-65 .govuk-accordion__section-button} {#reach-through-claims .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Reach-through claims\",\"index_section\":65,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-65 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-65" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Reach-through claims\",\"index_section\":65,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.156.1 {#ref14-156-1}

"Reach-through" claims to compounds identified by an assay or screening
method with particular properties, and to the downstream uses of such
compounds are speculative as the claim covers all compounds possessing
these properties when assayed, whether or not they have been
specifically identified in the description of the patent. If the
relationship between the function of the materials and their structural
features is not defined, identification of all compounds from the assay
with the desired properties would require substantial experimentation by
trial and error to screen compounds for the desired activity. Therefore
"reach-through" claims are both insufficient and unsupported by the
description. Furthermore, these claims often give no information about
the chemical and physical characteristics of the material possessing the
desired activity screened for and will also be unclear.
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Unity of Invention]{#default-id-c972c5d5-heading-66 .govuk-accordion__section-button} {#unity-of-invention .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Unity of Invention\",\"index_section\":66,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-66 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-66" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Unity of Invention\",\"index_section\":66,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.157 {#ref14-157}

s.125 is also relevant

Each claim of a specification defines at least one invention. However
the requirements of the Act regarding unity of invention are met if the
claims relate to a group of inventions which are so linked as to form a
single inventive concept. The question as to whether the inventions are
related in this way is not based on rigid rules but rather on broad
considerations as to the degree of interdependence between the
inventions claimed, and as to the state of the art concerned, as
explained in the following paragraphs. The practice to be adopted for
dealing with applications considered to lack unity of invention during
search or substantive examination is discussed in paragraphs
[17.106-17.114](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-106)
and
[18.37-18.41](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-37)
respectively (see
[17.108.2](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-108-2)
and
[18.38.1](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-38-1)
in particular, for the format that should be adopted when reporting
plurality to applicants during search and substantive examination
respectively).

### 14.157.1 {#ref14-157-1}

CoP is also relevant

Multiple inventive concepts should not be included in the same
application if they are unrelated to the extent that they will
inevitably give rise to a plurality objection.

### 14.158 {#section-65}

When considering unity of invention, regard should be had to the
underlying inventive concept of each of the inventions claimed. This
concept may be expressed in different ways [see 14.159](#ref14-159) and
may be implicit rather than explicit. Regard may be had to the purpose
and/or result of each invention. One criterion which would be suitable
for some sets of claims would be to determine whether the common
subject- matter of the claims is novel and involves an inventive step.
Other criteria are referred to in the subsequent paragraphs. The lack of
novelty or the obviousness of the common subject-matter may be
established by citing documentary evidence, but this is not necessary,
at least in the first instance, if the common matter is clearly known or
obvious. Specifications forming part of the state of the art by virtue
of s.2(3) may not be used to demonstrate that common subject-matter is
not new.

### 14.159 {#ref14-159}

r.16 is also relevant

Inventions should be treated as being linked so as to form a single
inventive concept where there exists between the inventions a technical
relationship which involves the same or corresponding "special technical
features", i.e. features which define a contribution which each of the
claimed inventions, when considered as a whole, makes over the prior
art. The inclusion of any one of the following combinations of claims of
different categories in the same application should be permitted :

-   \(a\) in addition to an independent claim for a product, an
    independent claim for a process specially adapted for the
    manufacture of the product, and an independent claim for use of the
    product; or
-   \(b\) in addition to an independent claim for a process, an
    independent claim for an apparatus or means specifically designed
    for carrying out the process; or
-   \(c\) in addition to an independent claim for a product, an
    independent claim for a process specially adapted for the
    manufacture of the product and an independent claim for an apparatus
    or means specifically designed for carrying out the process. This
    list of combinations of claims in different categories (process,
    product, use, apparatus) is not exhaustive, and other combinations
    should be considered on their merits.

### 14.159.1 {#ref14-159-1}

Examiners should use their discretion to determine whether the claims in
question share a single inventive concept, any doubt being resolved in
favour of the applicant. In Dow Chemical Company's Application (BL
O/175/83) the hearing officer concurred with the view expressed in EPO
Decision T110/82 (OJEPO 7/83) that one of the determining factors is the
equitable levying of fees between applicants. However the fact that the
subject-matter of two claims cannot be covered by a single search does
not necessarily demonstrate that unity of invention is lacking. On the
other hand if a single search suffices on a borderline case the benefit
of any doubt can be resolved in favour of the applicant.

### 14.160 {#section-66}

s.72 s.26 is also relevant

Plurality of invention is not a ground on which a patent may be revoked,
nor may any person, in any proceedings, raise such an objection to the
claims of a patent as granted or as amended after grant ([see
26.01](/guidance/manual-of-patent-practice-mopp/section-26-patent-not-to-be-impugned-for-lack-of-unity/#ref26-01)).

### 14.161 {#ref14-161}

Claims to separate articles which are inter-related, for example, by
being characterised in that they are to be used together, may be
regarded as linked to form a single inventive concept. This would be the
case with separate claims to two parts of an electrical or other
coupling, or to a housing and to contacts to be mounted in the housing,
provided they were specifically adapted for one another and have no
further obvious application. In particular separate claims may be
justified to parts which may be manufactured or sold separately, such as
a rupturable container of fuel and a burner adapted to pierce the
container when mounted on it; or a container of chemicals to be sprayed
which is adapted to be mounted on a carrier, and such a carrier
specially adapted for receiving the container; or to a new form of cable
and to a sheath stripper particularly adapted to deal with this cable.
Another example where there is unity of invention would be a transmitter
and receiver which were adapted to be used together, for example by
employing a particular novel method of encoding or modulating the
signal; however a transmitter and a receiver intended for use with it
would be regarded as separate inventions if they could also be used with
known receivers or transmitters.

### 14.162 {#ref14-162}

When a specification discloses a number of distinct surgical,
therapeutic or diagnostic uses for a known substance or composition,
separate claims to the substance or composition for the respective uses
are not, as a general rule, regarded as lacking unity of invention, if
there has not been any previous medical use of the substance or
composition.

### 14.163 {#ref14-163}

The fact that the inventions defined in independent claims may be
directed to solving the same problem or to implementing the same idea,
or that separately claimed processes may lead to the same product, may
not be sufficient in itself to confer unity of invention. In particular
the fact that a class of chemical intermediates has been prepared solely
in order to be converted to particular products may not demonstrate that
there is a single inventive concept linking claims to the intermediates
and the products. The EPO Technical Board of Appeal held in Decision
T35/87 (OJEPO 4/88) that it is necessary for unity of invention between
intermediates and end-products that groups of intermediates prepared and
oriented towards the end-products be technically closely interconnected
with the latter by sharing an essential structural element. Therefore,
if intermediate and final products include a common structure which can
be considered to be novel and involve an inventive step then claims to
the products can relate to a single inventive concept even though
separate searches for the products may be necessary.

### 14.164 {#ref14-164}

There is normally no question of plurality of invention when one claim
is within the scope of another, whether by repeating the wording of it
or by being dependent on it, even though the additional matter in the
narrower claim would have been capable of being claimed as a further
invention. Where however a claim is presented as dependent on another
but in fact is not limited to the invention of the other claim, for
example, by directing a claim to an apparatus as suitable for use in a
claimed system, or by stating that one or more integers of the other
claim are omitted or are replaced by other features, there is the
possibility of plurality of invention. Likewise when alternatives are
specified in a single claim, the claim should be mentally rewritten as a
series of independent claims which can then be assessed for unity of
invention in the usual way. A plurality objection may also be raised if
an independent claim is clearly unduly broad and speculative and not new
or inventive, and is apparently merely a device for giving an impression
of unity of invention between otherwise unrelated dependent claims ([see
17.66](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-66)
and
[17.110](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-110)).

### 14.165-167 \[deleted\] {#ref14-165}

### 14.168 {#ref14-168}

Where an applicant has discovered a useful property in a group of
chemically related compounds, some of which are known, claims to the new
use (subject to the provisions of s.4A(1)), to compositions containing
the compounds for such use, and to any of the compounds that are novel
per se and to their method of preparation, are considered to form a
single inventive concept provided that all of the compounds, whether
novel or known, possess the common characterising property giving rise
to the use.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 14(7)**
  The purpose of the abstract is to give technical information and on publication it shall not form part of the state of the art by virtue of section 2(3) above, and the comptroller may determine whether the abstract adequately fulfils its purpose and, if it does not, may reframe it so that it does.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [The abstract]{#default-id-c972c5d5-heading-67 .govuk-accordion__section-button} {#the-abstract .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"The abstract\",\"index_section\":67,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-67 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-67" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"The abstract\",\"index_section\":67,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.169 {#ref14-169}

s.14(2)(b) and (c) is also relevant

The abstract is not part of the specification, and it is clear from
s.125(1), which refers to the claims being interpreted by the
description and any drawings contained in the specification, that it
cannot be used to give assistance in determining the extent of the
protection conferred by the claims. The form and content of the abstract
are governed by r.15.

### 14.170 {#ref14-170}

s.97(1)(a), s.15(5)(a), s.15A(2)(b), s.15(10)(a) is also relevant

If an abstract fails to meet any requirement of r.15 then it may be
amended by the examiner, using the power given to the comptroller by
s.14(7). Although amendments made by the examiner do not form a part of
the application as filed, it is the amended form of the abstract that is
included in the published 'A' document, [see
16.08](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-08)
(There is no appeal to the Patents Court from a decision of the
comptroller under s.14(7)). If however the abstract as filed clearly
fails to meet the dictionary definition of an abstract (ie a brief
statement of the chief points of a larger work), for example when it is
little more than a title, it is deemed that no abstract has been filed.
Filing of an abstract is required by s.15(10)(a). If it is deemed that
no abstract has been filed, the case should be referred to a Formalities
examiner to issue a (replacement) preliminary examination under
s.15A(2), raising an objection to the failure to file an abstract that
meets the requirements of s.15(10)(a). The applicant should be asked to
remedy this. If the time allowed for filing the abstract ([see
15.50](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-50))
has already expired (including any extension allowed under r.108), the
application will be taken to have been withdrawn ([see
15.55](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-55)).

\[An objection that the applicant has failed to file anything that meets
the dictionary definition of an abstract should only be raised in the
clearest cases. The desirability of giving every help to private
applicants should be borne in mind.\]

### 14.171 {#ref14-171}

r.15 is also relevant

The purpose of the abstract is to provide technical information about
the patent application, and so the abstract should reflect the content
of the specification. However, in reality, matter is sometimes disclosed
in an abstract which is not disclosed in the specification as filed.
Following the decision of the Patents Court in Abbott Laboratories Ltd.
v Medinol Ltd \[2010\] EWHC 2865 (Pat) it is important to note that the
specification cannot be amended to incorporate such matter from the
abstract into the description or claims, whether or not the abstract was
filed on the day of filing. This decision (which overturns previous
Office practice as established in ARMCO Inc's Application [BL
O/84/85](https://www.gov.uk/government/publications/patent-decision-o08485)
-- [see
76.08.2](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-08-02)
has major implications for applications in which the abstract contains
significant material not disclosed elsewhere in the application. In
Abbott Laboratories Ltd. v Medinol Ltd, Arnold J held that s.14(7)
should be interpreted as meaning that the purpose of the abstract is to
give technical information only (by reference to the similar Art.85 EPC
and associated EPO case law), and so is irrelevant for the purpose of
determining the disclosure of the application as filed. The disclosure
of the abstract, even if filed on the date of filing, cannot therefore
be considered for the purpose of determining under s.76 whether an
amendment adds matter extending beyond the disclosure of the application
as filed. It is not possible to incorporate this material into the
description or claims by subsequent amendment, and so it will not be
possible for the applicant to claim this matter, or to rely on this
disclosure to provide support for the claims or to overcome an objection
of insufficiency. The applicant should therefore be informed of this
situation and its implications as soon as possible, so that they may
have the opportunity to withdraw and re-file the application with the
matter in question included in the specification. In addition, Arnold J
said that the purpose of the abstract is to provide a summary of the
disclosure of the specification, and so if it does not mean the same
thing as the specification then it must be assumed to be an inaccurate
summary. Therefore, when re- framing the abstract, the examiner should
delete any material which does not appear elsewhere in the application,
regardless of whether the abstract was filed on the day of filing or
later.

\[Inclusion of material in the abstract but not in the specification is
most likely in applications from private applicants, and so examiners
dealing with applications from private applicants should be particularly
alert to this possibility.\]

\[Applications from private applicants are sent to the Private Applicant
Unit (PAU) [see
17.03](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-03).
PAU examiners will scrutinise abstracts to determine whether they
contain matter not present elsewhere in the application. If the PAU
examiner becomes aware that there is a significant disclosure in the
abstract which is not present elsewhere in the application, then they
may issue a letter to the applicant under the ABS or ABCSE procedure as
set out [in
17.94.5-9](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94)
This letter should give the applicant the options to withdraw the
application and re-file (with a refund of the search fee), or to
continue with the application.\]

\[Where a private applicant case has been sent to an examination group,
this problem is most likely to come to light when the search examiner
scrutinises and if necessary re-frames the abstract. If the search
examiner discovers significant material in the abstract which is not
disclosed elsewhere in the application, they should inform the applicant
of this problem and its implications as soon as possible. This may be
done by including an appropriate warning in the search letter. However,
if the matter disclosed solely in the abstract is likely to be critical
for the grant of a patent, then it may be more appropriate to issue an
ABS or ABCSE letter.\]

### 14.171.1 {#ref14-171-1}

It appears to follow that a consequence of the decision that the
abstract is to be ignored when considering the disclosure of the
application is that an applicant cannot rely on matter contained solely
in an abstract for the purposes of claiming priority. ([See
5.20-5.25](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-20)).

### 14.172 {#section-67}

It follows from s.14(7) that subject-matter in the abstract but not in
the specification can only be cited against an invention which has a
priority date later than the date of publication of the application
containing the abstract.

### 14.173 {#ref14-173}

CoP and r.15(1) is also relevant

The abstract must have a title which encapsulates the invention
disclosed in the specification. The abstract title (along with the title
of the invention) is published on the front page of the A-specification.

\[If there is no abstract title the search examiner should provide one
in PROSE for translation into COPS without raising objection under
r.15(1). It is not necessary to add the abstract title to the page
containing the abstract. No other title should be entered, and because
of restrictions inherent in COPS, the title should not exceed 158
characters (including spaces) or include subscripts, such as present in
chemical formulae. No abstract title (or anything else in lieu of the
title) should be entered in PROSE if an abstract has not been filed. \]

### 14.174 {#section-68}

CoP is also relevant

The abstract title can be different from the title given to the
application on filing. The latter is unlikely to make a suitable
abstract title if, as is generally the case, it is expressed in broad
terms to avoid disclosure of the invention in the Journal before the
application itself is published. If the abstract title is unsuitable
(for example if it is too long or too vague) the examiner should amend
it. Examples of titles which are regarded as unsuitable are

-   \(a\) any title including such expressions as "improvements in or
    relating to" or "and the like"
-   \(b\) titles such as "chemical compound" or "control circuit", which
    give little or no indication of the invention
-   \(c\) over-long titles which are apparently intended merely to
    indicate that the specification contains claims in certain
    categories (process, apparatus etc), eg "Gas-permeable seamless pipe
    structure and method and apparatus for production thereof", or
    "Method of bleeding a hydraulic system and means therefor"
-   \(d\) over-long titles which contain matter, for example relating to
    possible fields of application of the invention, more properly to be
    found in the body of the abstract
-   \(e\) titles including a trademark ([see
    19.24](/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-24)).

### 14.174.1 {#section-69}

When amending an abstract title to make it an effective search tool,
care should be taken to avoid adding matter not in the specification.
Before amending an abstract title, the search examiner should read
enough of the specification to be certain that the alterations are
accurate and that the amended title encapsulates the disclosed
invention.

### 14.175 {#section-70}

r.15 is also relevant

The text of the abstract should comply with r.15(2), (3) and (7), which
read:-

\(2\) The abstract must contain a concise summary of the matter
contained in the specification.

\(3\) That summary must include---

\(a\) an indication of the technical field to which the invention
belongs; (b) a technical explanation of the invention; (c) the principal
use of the invention.

\(7\) The abstract must not contain any statement on the merits or value
of the invention or its speculative application.

### 14.176 {#section-71}

CoP is also relevant

The abstract should be primarily directed to that which is new in the
art to which the invention pertains. Clearly an abstract which describes
only the background or prior art or does not adequately reflect the
technical disclosure in the specification is inappropriate. If the
invention is in the nature of a modification to a known apparatus,
process, product or composition, the abstract should, while making clear
the context of the invention, be directed to the technical features of
the modification. If the invention is of a basic nature the entire
technical disclosure may be new in the art and the abstract should be
concerned with the entire disclosure. In either case it should be clear
from the abstract where the inventive contribution to the technical
field lies by indicating this in the opening sentence(s). The main
specifics about the invention and how it can be practised should then be
set out.

### 14.177 {#section-72}

CoP is also relevant

The abstract should be drafted so that it constitutes an efficient
instrument for the purposes of searching and disclosure in the
particular technical field, in particular by making it possible to
assess whether there is a need to consult the specification itself. The
scope of the abstract, and the words used in it, should be selected to
ensure that retrieval from electronic databases is likely when searching
similar applications at a later date.

### 14.178 {#section-73}

The check list in the "WIPO Standard ST.12/A" provides a useful guide
for the writer or reviser of an abstract. It indicates that, provided
that the specification contains the information, the abstract should
include the following:-

-   \(a\) where the invention is an article, its identity, use,
    construction, organisation and method of manufacture;
-   \(b\) where the invention is a chemical compound, its identity
    (structure if appropriate), method of preparation, properties and
    uses;
-   \(c\) where the invention is a mixture, its nature, properties, use,
    essential ingredients (identity, function), proportions of
    ingredients (if significant), and preparation;
-   \(d\) where the invention is a machine, apparatus or system, its
    nature, use, construction, organisation and operation;
-   \(e\) where the invention is a process or operation, its nature and
    characterising features, material and conditions employed, product
    (if significant), and the nature of a relationship between the
    steps, if more than one.
-   \(f\) where the disclosure involves alternatives, the abstract
    should deal with the preferred alternative and identify the others
    if this can be done succinctly; if this cannot be done, it should
    mention that they exist and whether they differ substantially from
    the preferred alternative.

This checklist is for guidance only. In particular the content of the
abstract should be determined by the new technical disclosure of the
specification rather than by the nature of the claims.

### 14.179 {#ref14-179}

Where features are merely preferred or optional, the abstract should
avoid any implication that they are essential to the invention. If the
disclosure involves alternatives, the abstract should deal with the
preferred alternative and identify the others, if this can be done
succinctly; if this cannot be done, it should mention that they exist
and whether they differ substantially from the preferred alternative.
Where the claims relate to more than one invention, the subject matter
of the further inventions should be included in the abstract, even
though no search may yet have been carried out in respect of these
inventions.

### 14.180 {#section-74}

Where the specification contains extensive numerical data or tables, eg
relating to physical properties or compositions, the presence of such
data, and their nature, should be indicated in the abstract, if this can
be done briefly.

### 14.181 {#ref14-181}

The general nature of a chemical compound or composition should be given
as well as the use thereof, eg "the compounds are of the class of alkyl
benzene sulfonyl ureas, useful as oral anti-diabetics". For processes,
the type of reaction, reagents and process conditions should be stated,
generally illustrated by a single example. Wherever applicable, the
chemical formula should be given which, among all the formulae contained
in the specification, best characterises the invention.

\[When a search examiner wishes the abstract to be accompanied by a
formula (other than one which can be typeset in normal typescript), they
will generally have to provide a copy for use in preparing the front
page of the 'A' publication\].

For this purpose, the examiner should provide a document on the dossier
containing a copy of the formula in question from a suitable page of the
description or claims. This document may be created and imported onto
the dossier electronically; or alternatively, created manually and
subsequently scanned on to the dossier. A minute should also be added to
the dossier giving clear instructions to Formalities concerning the
document's inclusion in the publication sub-file.

\[Deleted\]

The quality of the copy of the formula will be checked in Publishing
Section for suitability for reproduction and a fresh copy will be made
there if necessary. The above action is of course unnecessary if the
abstract already includes an acceptable representation of the formula or
a suitable formula drawing [see
15A.09](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-09)
is available. \]

### 14.182 {#section-75}

r.15(2) is also relevant

An abstract should normally contain not more than 150 words, as it is
unlikely to be considered to be concise if it extends beyond 150 words.
However, if a longer text is considered essential, there is space on the
front page which will accompany the published specification for
approximately 200 words. Abstracts should therefore contain 200 words at
the very most (including any reference numerals). Unnecessary phrases
such as "the invention relates to" or "and the like" may be deleted, and
an unduly long introductory statement which only indicates what is old
and well-known may be curtailed or deleted. While the removal of such
superfluous matter is of particular importance when the abstract is
longer than 150 words, it may also be deleted even if the abstract is
short.

### 14.183 {#section-76}

It is not necessary for the abstract to indicate the kind of protection
sought by the claims. Thus for example if an apparatus has been
described in the abstract and the specification includes claims to a
method of using the apparatus and/or to an article produced by the
apparatus, there is no need for the abstract to indicate this if the
technical features of the method and article are implicit in the
description of the apparatus.

### 14.184 {#ref14-184}

CoP is also relevant

The legal phraseology or the sentence structure used in patent claims,
should be avoided in abstracts. Thus an abstract that is identical to,
or closely resembles, an independent claim should not be filed, nor
should an abstract use words commonly associated with patent claims such
as "said" and "means".

### 14.185 {#ref14-85}

The search examiner must keep in mind that the abstract is an important
search tool and if it is unsatisfactory for that purpose they should
amend it. A critical appraisal of the abstract by the search examiner is
therefore necessary although this should not involve a rigorous analysis
to determine how far r.15 is complied with. They should not attempt
extensively to rewrite it for simply stylistic reasons. Thus if an
abstract does not reflect the main contribution to the art in an
application (eg where the claim is heavily anticipated or badly drafted)
the search examiner should amend the abstract to incorporate that
contribution. It may also be necessary to add to an otherwise adequate
abstract one or two phrases to ensure that it reflects all of the matter
in the application which has been classified including any further
inventions.

\[Any required amendments should made to the copy of the abstract
provided on PROSE.\]

\[Deleted\]

### 14.186 {#section-77}

When reframing an abstract to make it an effective search tool, care
should be taken to avoid adding matter not in the specification (but
[see 14.171](#ref14-171) for the procedure where the abstract as filed
includes matter not present in the specification). Before amending an
abstract, the search examiner should read enough of the description to
be certain that the alterations are accurate. The minimum to be read
depends on what is necessary for the search examiner to obtain a clear
understanding of the essential nature of the invention. Where a
statement of the background of the invention is included in the abstract
it should not normally be deleted, since it will frequently help to
explain the solution provided by the invention.

### 14.187 {#section-78}

The search examiner should resist the temptation to amend an abstract
which adequately fulfils its purpose merely because they would have
written it differently. Nor should subject-matter be deleted merely
because it does not appear to be useful search material in the
heading(s) in which the application is classified. In particular, matter
relating to the use of the invention may appear to be of no relevance to
the s.17 searcher, but may well be useful to other readers.

### 14.188 {#section-79}

r.15(4)-(6) is also relevant

Drawings particularly for the abstract are not required and should not
be filed. The applicant is required to indicate on the abstract which
figure, or, exceptionally, figures of any drawings of the specification
should accompany the abstract when published. If they have not done so
it is up to the search examiner to decide which figure(s) should be used
and to record this as outlined in [14.190](#ref14-190). The search
examiner may decide that one or more figures other than those suggested
by the applicant may be used instead or additionally if they consider
that they better characterise the invention. Normally not more than one
figure should accompany the abstract. Exceptionally two figures may be
used provided that, when sufficiently reduced in size to be accommodated
on the front page of the application, they, together with the reference
characters thereon, would still be readable. \[No objection should be
raised under r.15(4) in the event that no figure has been indicated on
the abstract. In such circumstances the search examiner should decide
upon a suitable figure. \]

### 14.189 {#section-80}

It should be clearly apparent from the abstract what the or each
accompanying figure represents. To aid identification of features
mentioned in the abstract, relevant reference numerals which appear in
the selected figure(s) should be freely used in the abstract. Numerals
which appear only in other drawings should normally not be used,
although exceptionally, a numeral which is considered necessary for an
understanding of the abstract but appears only in these other drawings,
may be referred to. Such reference should be bracketed, eg (29, Fig 16),
without any additional wording such as "see" or "not shown". When this
expedient is adopted it should be ensured that reference numerals which
do appear in the abstract drawing(s) are without brackets.

### 14.190 {#section-81}

The search examiner should revise or check the abstract and record the
number of the figure which is to accompany it (see 14.188) on the
abstract itself as well as in the "Fig. Ref." field on PROSE (see
below). If no figure is to accompany it, they should record this.

\[The figure number should appear separately on the page to the text of
the abstract. It is not necessary to put brackets around either the
figure number or the application number, although this may be done to
provide additional clarity.

\[The figure(s) to accompany the abstract should be recorded using PROSE
by entering the figure number(s), the word "FIG" where it is a single
unnumbered figure, the abbreviation "RTA" (ie 'refer to the abstract')
when special instructions are needed or the word "None" when no figure
is to accompany the abstract.

In some instances (particularly chemical cases), a figure or formula may
be included in the abstract itself, rather than separately within the
drawing pages. In these cases, the abbreviation "RTA" should be recorded
on PROSE, and no further information need be added to the abstract page
itself; in such situations do not write "None" or "No Figure" either in
PROSE or on the abstract text page.

In exceptional cases where part of a figure or a combination of two or
more figures is required for the abstract, a document containing a copy
of the relevant figure(s) should be provided on the dossier and
annotated appropriately. This document may be created and imported onto
the dossier electronically; or alternatively, created manually and
subsequently scanned on to the dossier. A minute should also be added to
the dossier giving clear instructions to Formalities concerning the
document's inclusion in the publication sub-file.\]

\[Deleted\]

### 14.191 {#ref14-191}

Amendments to the abstract submitted before s.16 publication should be
taken into account by the search examiner when deciding on the text to
be published. Since however when the specification of the patent is
published after grant it is not accompanied by an abstract, amendments
to the abstract filed after s.16 publication serve no purpose, although
of course they remain on the file and are open to public inspection. If
however the applicant draws attention to an error in the published
abstract which is such as to render the abstract wrong or misleading in
a material respect, an erratum may be issued provided that the error
arose during publication or during reframing of the abstract by the
search examiner. ([See
16.33](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-33)).

\[Section 14(8) Repealed.\]

### 14.192 {#section-82}

Subsection (8) has been repealed, [see 14.91](#ref14-91).
\[14.193-14.198 Deleted\]

  -----------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 14(9)**
  An application for a patent may be withdrawn at any time before the patent is granted and any withdrawal of such an application may not be revoked.
  -----------------------------------------------------------------------------------------------------------------------------------------------------
:::
:::
:::

::: govuk-accordion__section
::: govuk-accordion__section-header
## [Withdrawal]{#default-id-c972c5d5-heading-68 .govuk-accordion__section-button} {#withdrawal .govuk-accordion__section-heading ga4-event="{\"event_name\":\"select_content\",\"type\":\"accordion\",\"text\":\"Withdrawal\",\"index_section\":68,\"index_section_count\":68}"}
:::

::: {#default-id-c972c5d5-content-68 .govuk-accordion__section-content aria-labelledby="default-id-c972c5d5-heading-68" module="ga4-link-tracker" ga4-track-links-only="" ga4-set-indexes="" ga4-link="{\"event_name\":\"navigation\",\"type\":\"accordion\",\"section\":\"Withdrawal\",\"index_section\":68,\"index_section_count\":68}"}
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 14.199 {#ref14-199}

An intimation of withdrawal will not be acted on by the Office unless it
is clear and unqualified and in writing, and from a person with the
authority to make the withdrawal. The withdrawal request may be by paper
or electronic communication.

### 14.199.1 {#ref14-199-1}

Directions prescribing the form and manner in which email messages
withdrawing applications should be delivered to the Office were
published in the PDJ No.5946 on 7 May 2003 and came into effect from 8
July 2003. These directions are reproduced in full in the "Relevant
Official Notices and Directions" section of this Manual. Email
withdrawal requests should be titled "Withdrawal of patent application
number GBYYXXXXX.X" and sent to <withdraw@ipo.gov.uk> and include in the
main body of the message a clear statement of withdrawal and indication
that the sender is authorised to make the withdrawal. This email address
should not be used for other proceedings under the Act or any other
correspondence relating to business carried out by the Office. The email
should be in plain text (RFC822-compliant); messages in MS-TNEF/RTF or
HTML formats and messages that are encrypted or digitally signed will
not be accepted. If an email request does not comply with the
directions, the Office may treat the message as not having been
delivered. The time and date of receipt of the email message will be
taken as the time/date stamp the message receives when it enters the
Office internal email system, which will not be the exact time/date it
was sent. A return email message will be sent by the Office to confirm
receipt of the withdrawal request; the request cannot be treated as
being delivered unless this acknowledgement has been sent. [(See also
14.205)](#ref14-205)

\[When an application has been withdrawn (or has been refused or treated
as having been withdrawn or refused) the Formalities manager should add
the appropriate label to the cover of the dossier, add "Fmls comp --
Terminated" action to the dossier, and carry out the appropriate COPS
action (so that, regardless of whether or not the application has been
published under s.16, the termination is advertised in the Journal).\]

\[Deleted\]

\[Where on any application for which a valid notification of withdrawal
is received, there is a Form 9 (and fee) already lodged but no copy of
the external search report on file and/or a Form 10 (and fee) already
lodged but no copy of the first substantive examination report on file,
then before taking any other action the formalities examiner should
refer the application to the appropriate Group Head for any action
required in the examining group. The application should then be referred
back to the relevant formalities manager for completion of the
termination action as set out above and refund of the Form 9 and/or form
10 fee(s) in appropriate circumstances [(see 14.207)](#ref14-207)\]

### 14.200 {#ref14-200}

In General Motors Corporation (Longhouse's) Application \[1981\] RPC41 a
letter withdrawing the application was written, on instructions from the
applicant company in the United States, by a technical assistant under
the control of the authorised agent (both being employees of the
applicant). Subsequently, the applicants having changed their minds, the
authorised agent wrote requesting reinstatement of the application.
Refusing the request, the hearing officer held that the technical
assistant had the implied authority, by virtue of his employment, to
make the withdrawal. In Siemens Medical Systems Inc.'s Application ([BL
O/063/00](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=+O06300&submit=Go+%BB){rel="external"})
it was held that withdrawal takes place as of the date on which the
withdrawal request is filed, and not on the date that the Register is
updated to show the withdrawal.

### 14.201 {#section-83}

When there is more than one applicant a request for withdrawal will not
be effective if made by only some of the applicants unless it is clear
that the remaining applicants have given them the express authority to
do so.

### 14.202 {#section-84}

It is not necessary for a request to use the words "withdrawal" or
"withdrawn"; it is sufficient if the statement expresses, in whatever
terms (for example by referring to "abandonment") positive intention to
terminate the application forthwith. However a statement of intention,
such as that the applicant is no longer interested in prosecuting their
application, is not sufficient. For example, an applicant may state that
they do not intend to file a request for substantive examination; such
an indication is not binding on them, and they may change their mind and
file the request at any time within the prescribed period. Similarly an
applicant may indicate that they do not intend to reply to a report
issued under s.18(3). In such cases no specific action should be taken,
the application being left to await further action by the applicant or
until it is, in due course of time, treated as having been withdrawn or
refused.

### 14.203 {#ref14-203}

If the applicant or agent has indicated that they do not intend to
proceed (or some such non-committal wording), but has not unequivocally
and in writing withdrawn the application, and some course of action by
the Office, such as search, A-publication, substantive examination,
hearing, or grant, is pending, they should be asked to indicate in
writing their clear intentions. If no clear withdrawal is forthcoming
the application will proceed. If the applicant or agent indicates over
the telephone that they wish to withdraw an application, they should be
told to express their intention in writing. If no letter or email is
received the request will not be acted on.

### 14.204 {#ref14-204}

If at any time the applicant or agent purports to effect a conditional
withdrawal, they should be requested to file an unqualified statement of
withdrawal, failing which the application is regarded as still in being.

### 14.205 {#ref14-205}

If an application is withdrawn before preparations for its publication
have been completed it will not be published under s.16(1) (see 16.07).
Thus if an applicant wishes to prevent publication of their application
they must unequivocally withdraw it before preparations for publication
have been completed ([see
16.02](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-02)).
The directions prescribing the form and manner of delivery of email
messages to the Office to withdraw applications [(see
14.199.1](#ref14-199) state that if an application is to be withdrawn in
time to prevent publication, the email message must be received by the
Office up to 23.59 on the day before preparations for publication are
complete; if the email messages is received after that time, it will be
too late to prevent publication. If a letter requesting that an
application be withdrawn and not published is received too late to
prevent publication and it appears likely, even if not explicitly
stated, that the applicant intended to withdraw only if publication were
not going to take place, they should be asked to state clearly their
intentions.

\[When a request for withdrawal is received for an application already
in the A- publication cycle, the divisional publication liaison officer
should be contacted immediately. When a request for withdrawal is
received before publication but after preparations for publication are
complete (as determined by
[16.02](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-02)),
the relevant formalities group will issue an appropriate letter
depending on whether or not withdrawal appears to be conditional on
prevention of publication. In each case subsequent action will depend on
any response from the applicant. \]

### 14.206 {#ref14-206}

r.107 is also relevant

Likewise if they wish to withdraw their application before grant they
must do so before the issue of the letter informing them of the grant.
If a written request for withdrawal is received in the Office before the
issue of the grant letter but not in time to prevent issue of the
letter, then the grant may be rescinded ([see
18.89-90](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-89)).

\[When a written request for withdrawal is received for an application
already in the grant cycle, the publication liaison officer for your
division should be enlisted immediately. The publication liaison officer
will call a meeting of all interested parties to decide whether
withdrawal is appropriate, and take the necessary steps to effect
withdrawal, including communication with the applicant to formally
rescind the grant by letter when the grant letter has been sent. \]

### 14.207 {#ref14-207}

If an application in respect of which Form 9A or Form 10 has been filed
is withdrawn before the report under s.17 or s.18 respectively is
issued, the fee paid may be refunded. Such a refund is however a matter
of discretion and not a right.

\[The refund is authorised by the appropriate formalities group. \]

### 14.208 {#section-85}

A withdrawn application may be used for the purposes of claiming
priority for a later application, subject to the provisions of the Act
([see
5.04](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-04)).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 14(10)**
  Subsection (9) above does not affect the power of the comptroller under section 117(1) below to correct an error or mistake in a withdrawal of an application for a patent.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 14.209 {#ref14-209}

This subsection was added by the Regulatory Reform (Patents) Order 2004,
and establishes that, although section 14(9) provides that a withdrawal
may not be revoked, the withdrawal may be corrected under the provisions
of section 117. This provision came into force on 1 January 2005 and
applies to applications filed both before and after this date. If the
application in question had been published under section 16 and the fact
of withdrawal had also been published, special provisions apply
([sections
117(3),(04)](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-3)
and
[117A](/guidance/manual-of-patent-practice-mopp/section-117a-effect-of-resuscitating-a-withdrawn-application-under-section-117/#ref117A)).
:::
:::
:::
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
