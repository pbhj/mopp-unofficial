::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 129: Application of Act to Crown {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (129.01) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 129

This Act does not affect His Majesty in her private capacity, but
subject to that, it binds the Crown.

### 129.01

The Crown (except the Sovereign) is thus bound by the Act. However the
Act gives specific rights to the Crown in respect of use of patented
inventions for the services of the Crown, under ss.55 to 59, and use or
disposal by the Crown of articles forfeited under customs or excise law,
under s.122.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
