::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 9: Determination after grant of questions referred before grant {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (9.01 - 9.04) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 9.01

This section deals with the situation where a reference to the
comptroller made under s.8 has not been disposed of when the patent
application in question is in order for grant. Section 8 provides for
the determination of questions of entitlement to a patent for an
invention prior to the grant thereof but is not applicable once grant
has occurred, when s.37 instead applies to the determination of such
questions.

### 9.2

\[deleted\]

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 9**
  If a question with respect to a patent or application is referred by any person to the comptroller under section 8 above, whether before or after the making of an application for the patent, and is not determined before the time when the application is first in order for a grant of a patent in pursuance of the application, that fact shall not prevent the grant of a patent, but on its grant that person shall be treated as having referred to the comptroller under section 37 below any question mentioned in that section which the comptroller thinks appropriate.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 9.3 {#ref9-03}

When an application is in order for grant, it proceeds to grant in the
normal way notwithstanding the existence of an unresolved entitlement
question under s.8.

### 9.4 {#ref9-04}

On the grant of the patent, the Office sends a letter informing the
claimant that the reference is being treated as made under s.37, and
that this may affect the relief available, but should not otherwise
affect the course of the proceedings. A letter in identical terms is
sent to the defendants. Unless the claimant and defendant respond, it is
assumed that both parties agree that the question of relief should be
deferred until the substantive hearing.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
