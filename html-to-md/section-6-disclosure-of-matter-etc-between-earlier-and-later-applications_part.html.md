::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 6: Disclosure of matter, etc, between earlier and later applications {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (6.01 - 6.03) last updated January 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 6(1)**
  It is hereby declared for the avoidance of doubt that where an application (the application in suit) is made for a patent and a declaration is made in accordance with section 5(2) above in or in connection with that application specifying an earlier relevant application, the application in suit and any patent granted in pursuance of it shall not be invalidated by reason only of relevant intervening acts.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 6(2)**

  In this section:\
  "relevant application" has the same meaning as in section 5 above; and\
  "relevant intervening acts" means acts done in relation to matter
  disclosed in an earlier relevant application between the dates of the
  earlier relevant application and the application in suit, as for
  example, filing another application for the invention for which the
  earlier relevant application was made, making information available to
  the public about that invention or that matter or working that
  invention, but disregarding any application, or the disclosure to the
  public of matter contained in any application, which is itself to be
  disregarded for the purposes of section 5(3) above.
  -----------------------------------------------------------------------

### 6.01

This is an avoidance of doubt section and is based on the wording of
Arts. 4A and 4B of the Paris Convention. The section confirms the
provisions of ss.2, 3 and 5 that if an invention in an application in
suit is entitled for priority to the filing date of an earlier
application specified in a declaration of priority under s.5(2) then any
disclosure or use of matter contained in that earlier application on or
after the filing date of the earlier application cannot invalidate a
claim to that invention.

### 6.02 {#ref6-02}

In [Beloit Technologies Inc v Valmet Paper Machinery Inc \[1995\] RPC
705](https://doi.org/10.1093/rpc/1995rpc705){rel="external"} Jacob J
held that s.6(1) does not carve out from the state of the art matter
made available to the public in the priority interval just because that
matter is in the priority document. Thus, an invention which is not
entitled to the priority date of an earlier application can be
invalidated by the disclosure or use, between the filing dates of the
earlier application and the application in suit, of matter contained in
the earlier application.

### 6.03 {#ref6-03}

In his judgment in [Beloit Technologies Inc v Valmet Paper Machinery Inc
\[1995\] RPC
705](https://doi.org/10.1093/rpc/1995rpc705){rel="external"} Jacob J
reached the same general conclusions as the Enlarged Board of Appeal
(G3/93 OJEPO 1-2/1995) which considered the implications of a document
published during the priority period, the technical content of which
corresponds to that of the priority document, and concluded that the
published document constitutes prior art citable against the application
claiming priority from the priority document to the extent that such
priority is not validly claimed. This also applies if a claim to
priority is invalid due to the fact that the priority document and the
subsequent application do not concern the same invention because the
application claims subject matter not disclosed in the priority
document.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
