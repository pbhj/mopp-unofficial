::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 82: Jurisdiction to determine questions as to right to a patent {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (82.01 - 84.02) last updated October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 82: Jurisdiction to determine questions as to right to a patent {#section-82-jurisdiction-to-determine-questions-as-to-right-to-a-patent}

### 82.01

The words "the law applicable to the contract of employment" were
substituted for the words "the proper law of the contract of employment"
by s.5, Sch 4 of the Contracts (Applicable Law) Act 1990.

### 82.04.1 {#ref82-04-1}

s.82(3) is also relevant

The jurisdiction of the court and the comptroller depends on whether or
not the question to be determined is an "employer-employee question", ie
one between an employer and an employee, or their successors in title,
arising out of an application for a European patent for an invention
made by the employee. If it is an employer-employee question, the
country of employment of the employee (at which they are mainly employed
or, if no such country can be identified, to which they are attached)
decides whether there is jurisdiction, as detailed in sub-section (5);
if not, the country of residence or of the principal place of business
of the parties so decides, as in sub-section (4) (but [see
82.05.1](#ref82-05-1)). In most cases, there is jurisdiction if the
country of employment, residence or business is the UK. Even though
there was no dispute in respect of a reference under s.12 in respect of
Oxford Instruments' Ltd's Application ([BL
O/068/93](https://www.gov.uk/government/publications/patent-decision-o06893))
for a European patent, before determining the substance of the reference
the hearing officer first satisfied themselves that the proceedings fell
to be considered under s.82(4) and that, since the patent applicant's
residence was in the UK, the comptroller had jurisdiction in accordance
with s.82.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 82(6)**
  Without prejudice to subsections (2) to (5) above, the court and the comptroller shall have jurisdiction to determine any question to which this section applies if there is written evidence that the parties have agreed to submit to the jurisdiction of the court or the comptroller, as the case may be, and, in the case of an employer-employee question, the law applicable to the contract of employment recognises the validity of the agreement.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 82.05 {#section-1}

The words "the law applicable to the contract of employment" were
substituted for the words "the proper law of the contract of employment"
by s.5, Sch 4 of the Contracts (Applicable Law) Act 1990.

### 82.05.1 {#ref82-05-1}

s.82(9) is also relevant

The considerations in [82.04.1](#ref82-04-1) are over-ridden if there is
written evidence that the parties (whether employer and employee or
otherwise) have agreed to submit to a particular jurisdiction. In the
case of an employer-employee question, the agreement must be one which
the contract of employment properly recognises as valid. If the agreed
jurisdiction is that of the competent authority of another state which
is a party to the EPC, then the court and the comptroller do not have
jurisdiction. (Determinations by such authorities may be recognised in
the UK, [see
83.03-04](/guidance/manual-of-patent-practice-mopp/section-83-effect-of-patent-decisions-of-competent-authorities-of-other-states/#ref83-03).
The court and the comptroller do however have jurisdiction if so agreed
by the parties.

  -----------------------------------------------------------------------
   

  **Section 82(7)**

  If, after proceedings to determine a question to which this section
  applies have been brought before the competent authority of a relevant
  contracting state other than the United Kingdom, proceedings are begun
  before the court or a reference is made to the comptroller under
  section 12 above to determine that question, the court or the
  comptroller, as the case may be, shall stay or sist the proceedings
  before the court or the comptroller unless or until the competent
  authority of that other state either -\
  \
  (a) determines to decline jurisdiction and no appeal lies from the
  determination or the time for appealing expires, or\
  \
  (b) makes a determination which the court or the comptroller refuses to
  recognise under section 83 below.
  -----------------------------------------------------------------------

### 82.06 {#ref82-06}

s.82(9) is also relevant

Any proceedings begun before the court or references made to the
comptroller under s.12 to determine a relevant question are stayed if
proceedings before the competent authority of another state, which is a
party to the EPC, are already in being. Proceedings before the court or
the comptroller resume only if and when that competent authority either
declines jurisdiction and its decision cannot be reversed, or makes a
determination which the court or the comptroller refuses to recognise
under s.83, [see
83.05](/guidance/manual-of-patent-practice-mopp/section-83-effect-of-patent-decisions-of-competent-authorities-of-other-states/#ref83-05).

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 82(8)**
  References in this section to the determination of a question include respectively references to - (a) the making of a declaration or the grant of a declarator with respect to that question (in the case of the court); and (b) the making of an order under section 12 above in relation to that question (in the case of the court or the comptroller).
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 82(9)**
  In this section and section 83 below "relevant contracting state" means a country which is a party to the European Patent Convention and has not exercised its right under the convention to exclude the application of the protocol to the convention known as the Protocol on Recognition.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 82.07 {#section-2}

All contracting states are now bound by the Protocol on Recognition.
Article 167 EPC \[1973\] which allowed contracting states to enter a
reservation not to be bound by the Protocol, also limited the time that
the reservation was available to ten years (or fifteen years with an
extension) from the date that the Convention entered into force.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
