::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 123: Rules {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section last updated: April 2024.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(1)**
  The Secretary of State may make such rules as he thinks expedient for regulating the business of the Patent Office in relation to patents and applications for patents (including European patents, applications for European patents and international applications for patents) and for regulating all matters placed by this Act under the direction or control of the comptroller; and in this Act, except so far as the context otherwise requires, prescribed" means prescribed by rules and "rules" means rules made under this section.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 123.01 {#ref123-01}

This gives a general power to the Secretary of State to make rules under
the Act, see 0.03 in the "Introduction" section of this Manual. Although
s.123(2), (6) and (7) refer to rules for specific purposes, the
rule-making power is not limited to those purposes. The rules are
applicable to business and other matters arising under the 1977 Act and
also govern the business of the Office in relation to European patents,
applications for European patents and international applications for
patents (rr.58 to 72 being specific to such patents or applications but
many others of the rules are also relevant). There are six schedules to
the rules, [see
0.04](/guidance/manual-of-patent-practice-mopp/manual-of-patent-practice-introduction/#ref0-04)
in the Introduction to this Manual.

### 123.02 {#section}

The Rules thus have the force of statute and where the applicant fails
to comply with them the comptroller has power to refuse grant. However,
the Rules are subordinate to the Act from which they derive their
authority. The Rules therefore must be read together with the Act and
cannot override express provisions in the Act.

### 123.02.1 {#ref123-02-01}

Reg 4(1) of the Patents (Supplementary Protection Certificate for
Medicinal Products) Regulations states that for the purposes of s.123(1)
matters relating to supplementary protection certificates shall be the
business of the Office. Reg 4(2) provides that the power of the
Secretary of State to make rules under s.123 shall include the power to
make rules in respect of such certificates and applications for said
certificates and that ss.123(2)-(7) and 124 shall apply accordingly. The
Patents Rules 2007 and the Patents (Fees) Rules 2007 therefore include
specific provisions for supplementary protection certificates. See the
"Supplementary Protection Certificates" section of this Manual for
further details.

### 123.03 {#ref123-03}

S.123 (except for sub-sections (6) and (7)) is merely an enabling
section allowing the making of rules; it does not follow that a rule
must have been made providing for any particular contingency. In Tokan
Kogyo KK's Application \[1985\] RPC 244 it was held that even if it were
accepted that s.123(2)(b) [see 123.06 to 123.10.2](#ref123-06)
contemplates the making of rules authorising the rectification of errors
and omissions on the part of applicants or their agents, the plain fact
is that the only rule relating to correction of irregularities in
procedure is r.107, the provison to which mentions errors and omissions
on the part of only the Office.

  -----------------------------------------------------------------------
   

  **Section 123(2)**

  Without prejudice to the generality of subsection (1) above, rules may
  make provision­ -\
  (a) prescribing the form and contents of applications for patents and
  other documents which may be filed at the Patent Office and requiring
  copies to be furnished of any such documents;
  -----------------------------------------------------------------------

### Form and contents of documents

### 123.04 {#ref123-04}

The form and presentation of documents forming a 1977 Act application
are governed by the rules, especially rules 12 and 14 (together with
Schedule 2), see [14.26 to
14.57](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-26),
and rule 15 with regard to the abstract, see [14.169 to
14.189](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-169)
. Information concerning biological material should, where applicable,
be included in the application in accordance with rule 13 and Schedule
1, see chapter on s.125A. Rules 76(4), 78(1) and 87 govern the form of
statements of case (as defined in r.73(3)) and evidence filed at the
Office. The content and layout of the Patents Forms is set out by
directions made under section 123(2A) ([see.123.70.1](#ref123-70-1)).
Requirements with regard to translations of European patents (UK) and
applications therefore are given in rules 56 and 57. A number of the
rules require that certain documents should be filed in duplicate.

\[Documents produced on machines which use thermal paper, may fade
within months if not correctly stored. Such material can be recognised
by the waxy feel of the paper and the slightly blurred image. Any such
documents received by staff should be photocopied as soon as possible
after receipt, and both the thermal print and the photocopy should be
placed on file. \]

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(b\) regulating the procedure to be followed in connection with any proceeding or other matter before the comptroller or the Patent Office and authorising the rectification of irregularities of procedure; Proceedings before the comptroller: procedure
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 123.05 {#ref123-05}

Part 7 of the Patents Rules 2007 (see particularly rules 73-88) provides
a general procedural code for the conduct of proceedings heard before
the comptroller. It applies to the applications, requests and references
mentioned in Part 1 of Schedule 3 to the Rules and to the oppositions
mentioned in Part 2 of that schedule (which start proceedings),
including proceedings already started when the Rules entered into force
on 17 December 2007. The procedure is explained in outline below, but a
more detailed explanation of how it applies in practice is given in
Tribunal Practice Notice 6/2007 (which is reprinted in the "Relevant
Official Notices and Directions" section of this Manual) and in the
Patent Hearings Manual and Tribunal Patents Manual. Any requirements
specific to particular sections are discussed in the chapters relating
to such sections. The provision of security for costs or expenses in
proceedings before the comptroller is governed by s.107(4) and r.85
([see
107.10](/guidance/manual-of-patent-practice-mopp/section-107-costs-and-expenses-in-proceedings-before-the-comptroller/#ref107-10)).

### 123.05.1 {#section-1}

r.74 is also relevant.

The procedural code of Part 7 has the overriding objective of enabling
the comptroller to deal with cases justly. The criteria for this are
listed in r.74(2) and are equivalent to those applying to the court
under the Civil Procedure Rules. The comptroller is required to seek to
give effect to the overriding objective when they exercise any power or
interpret any rule in Part 7, and the parties to proceedings are
required to help the comptroller to further the overriding objective.

### 123.05.2 {#ref123-05-02}

Those rules in part 7 which are listed in Part 4 of Schedule 3 (rr.74,
79, 80(2) to (6), 81, 82, 84 and 87) apply to any proceedings, including
ex parte hearings, heard before the comptroller under the Patents Act
1977.

### 123.05.3 {#ref123-05-3}

r.75, r.76(2) is also relevant.

In the case of oppositions, Part 7 draws a distinction between those
which start proceedings and those (in respect of some provisions in ss.
47 and 52 concerning licences) which arise after proceedings have
started. These are listed in Parts 2 and 3 respectively of Schedule 3.
Except in the case mentioned in rule 105(5) (a correction under s.117 to
which no person could reasonably object), the comptroller must advertise
in the journal any event which can give rise to an opposition under any
of the provisions in Parts 2 and 3. For Part 2 oppositions, a person has
a period of four weeks from the date of the advertisement to start
opposition proceedings ([see 123.05.4](#ref123-05-4)), except in the
case of oppositions under s.75(2) to amendment in infringement or
revocation proceedings for which the period is two weeks. For Part 3
oppositions, [see 123.05.6](#ref123-05-6).

### 123.05.04 {#ref123-05-4}

r.44(7), r.76 is also relevant.

A person must start proceedings by filing in duplicate the relevant
Patents Form and a statement of grounds, whereupon they become a
claimant. The relevant form is Form 2 for the applications, requests and
references in Part 1 of Schedule 3 (except those relating to
supplementary protection certificates for medicinal and plant protection
products under Community legislation for which it is Form SP3), or Form
15 for the oppositions in Part 2 of Schedule 3. The fact that such an
application, reference, request or opposition has been made is recorded
in the register. The statement of grounds must include a concise
statement of the facts and grounds on which the claimant relies and
specify the remedy which they seek. In cases involving licences, the
statement must, where appropriate, also include the period or terms of
the licence which the claimant believes to be reasonable.

### 123.05.5 {#ref123-05-5}

r.77 is also relevant.

The comptroller must notify the applicant for, or proprietor of, the
patent in suit that proceedings have started, and may also notify any
other person who appears to the Comptroller likely to have an interest
in the case. Generally the comptroller will notify any person named in
the register of patents or in the statement of grounds and any other
person who appears likely on a case-by-case basis to have an interest.
The notification must be accompanied by Form 2 and the statement of
grounds and must specify a period, which will normally be 6 weeks, for
the persons notified to file a counter-statement; the counter-statement
must be filed in duplicate before the end of that period. Any notified
person who fails to file a counter-statement will be treated as
supporting the claimant's case, but a person who files a
counter-statement becomes a defendant in the proceedings.

### 123.05.6 {#ref123-05-6}

r.77(7), r.77(8), r.77(10) is also relevant.

This procedure does not apply to the oppositions in Part 3 of Schedule 3
where proceedings have already started and have been advertised in the
journal. In these cases, a person has a period of four weeks from the
date of the advertisement to file a counter-statement in duplicate to
become a defendant in the proceedings.

### 123.05.7 {#section-2}

r78 is also relevant.

A counter-statement must state which of the allegations in the statement
of grounds the defendant denies and which of them they admits, and which
of the allegations they are unable to admit or deny but which they
require the claimant to prove. If the defendant denies an allegation
they must state their reasons for doing so, and, if they intend to put
forward a different version of events from that given by the claimant,
they must state their own version. If the defendant fails to deal with
an allegation they will be taken to admit it, except that where they
have set out the nature of their case in relation to the issue to which
the allegation is relevant they shall be taken to require the allegation
to be proved.

### 123.05.8 {#section-3}

r.76(4), r.78(5) r.87(5) is also relevant

In order to enable them to be used in evidence, a statement of grounds
and a counter-statement must be verified by a statement of truth signed
by the party or their legal representative.

### 123.05.9 {#ref123-05-9}

r.80(1), r.80(3) is also relevant.

As soon as practicable after the defendant has filed a counter-statement
the comptroller must send it to the claimant and specify the periods
within which the claimant and defendant may file evidence. Evidence
shall only be considered to be filed when it has been received by the
comptroller and sent to all other parties to the proceedings. (For the
form of evidence and the powers of the comptroller to control the
evidence and to compel attendance of witnesses and production of
documents, [see 123.17-18](#ref123-17)

### 123.05.10 {#ref123-05-10}

r.80(4)r.80(5) is also relevant.

After evidence has been filed, the comptroller must give the parties an
opportunity to be heard and send notice of a date for the hearing where
a party requests to be heard.

### 123.05.11 {#ref123-05-11}

In addition to their powers to control evidence under r.82(2) ([see
123.17](#ref123-17)), the comptroller has powers under r.81 to extend or
shorten (or further extend or shorten) any period of time specified
under the provisions of Part 7 of the Rules; under rule 82(1) to give
such directions as to the management of the proceedings as they think
fit; and, on the application of a party under r.83, to strike out a
statement of case or give summary judgment. The power under r.82(1) is
general, but the rule lists a number of examples of the directions which
may be given. Under r.82(3) the comptroller may make any direction given
under the rules in Part 7 subject to conditions and may specify the
consequences of failure to comply with the directions or a condition.
Hearing officers will use these powers to manage cases actively in order
to meet the overriding objective, with the aim of completing proceedings
within 12 months of the filing of the counter-statement. Accordingly the
hearing officer will review the case after the counter-statement has
been filed to assess how it can best be resolved. The hearing officer
will set a timetable at the outset for the filing of evidence and the
date of the hearing by agreement with (or at least after representations
from) the parties. They will normally order the case to follow one of
three main routes:

-   alternative dispute resolution (ADR), for which a period of 2 months
    should normally suffice

-   standard procedure, for straightforward cases. In general the
    hearing officer will expect the hearing to be set for no later than
    9 months from the filing of the counter-statement. This will provide
    a window for ADR if necessary, sufficient time for the filing of
    three rounds of evidence (evidence in chief from the claimant and
    the defendant and evidence in reply from the claimant) at 6 week
    intervals, and then a period of 2-3 months before the hearing

-   case management conference, usually where the case is complex or the
    issues do not appear to be clear cut, in order to determine how
    events should proceed

### 123.05.12 {#section-4}

r84 is also relevant.

The hearing is to be held in public unless either the comptroller has
granted an application for the hearing or part of it to be held in
private, or it relates to an application for a patent which has not been
published. In the former case the comptroller may grant an application
where they consider there is good reason for it, and all parties have
had an opportunity to be heard on the matter.

### 123.05.13 {#ref123-05-13}

r.80(6) is also relevant.

When the comptroller has decided the matter they must notify all the
parties of their decision and the reasons for making it. The preparation
and issue of decisions, which may be written or oral, is explained in
Chapters 5 and 6 of the Patent Hearings Manual.

### Rectification of irregularities

([see also 123.03](#ref123-03))

### 123.06 {#ref123-06}

The correction of irregularities is governed by rule 107 which reads:

\(1\) Subject to paragraph (3), the comptroller may, if he thinks fit,
authorise the rectification of any irregularity of procedure connected
with any proceeding or other matter before the comptroller, an examiner
or the Patent Office.\
(2) Any rectification made under paragraph (1) shall be made\
(a) after giving the parties such notice; and\
(b) subject to such conditions, as the comptroller may direct.\
(3) A period of time specified in the Act or listed in parts 1 to 3 of
Schedule 4 (whether it has already expired or not) may be extended under
paragraph (1) if, and only if ­\
(a) the irregularity or prospective irregularity is attributable, wholly
or in part, to a default, omission or other error by the comptroller, an
examiner or the Patent Office; and\
(b) it appears to the comptroller that the irregularity should be
rectified.

The comptroller thus has discretion on whether to allow a rectification
of an irregularity of procedure under this rule, and to impose terms
when exercising this discretion favourably. For example, Coal Industry
(Patents) Ltd's Application (\[1986\] RPC 57) concerned an application
on which, following the filing of amendments, an examination report was
prepared in the Office but never issued. The applicants therefore never
responded and the application was treated as refused since it failed to
comply with the Act and Rules. Since this was attributable to an error
in the Office, the hearing officer resuscitated the application under
r.100 of the Patents Rules 1995 (equivalent to r.107 of the Patents
Rules 2007) but imposed terms similar to those employed in restoration
proceedings under s.28 to protect the interests of third parties who may
have made preparations to exploit the invention between advertisement of
the refusal and the decision to resuscitate. The Patents Court upheld
the hearing officer. This can be contrasted with Eveready Battery Co.
Inc.'s Patent \[2000\] RPC 852, in which a renewal fee was correctly
paid on a patent, but not recorded in the Office. The patent was
therefore erroneously recorded as having ceased. The court held that,
since the requirements of the Act and Rules in respect of renewal fee
payment had been met, the patent had not ceased and so there was no
irregularity to correct. It was therefore not open to the Office to
impose third party terms.

\[Advertisement of the initiation of proceedings under r.107 may be
appropriate in some cases (eg where the case has been previously
advertised as terminated); this advertisement will provide a "cut-off"
for any third party terms, [see also 123.38](#ref123-38). \]

### 123.07 {#section-5}

\[deleted\]

### 123.08 {#ref123-08}

In its judgment in Fater's Application \[1979\] FSR 647 the Patents
Court ruled that the comptroller's discretionary power under the
original Rule 100 \[Patents Rules 1978\], which by virtue of Rule
124(1)(d) \[Patents Rules 1978\] applied also to "existing patents and
existing applications", to rectify an irregularity in procedure in or
before the Office is not restricted to irregularities attributable to
the Office, but extends to irregularities in procedure, however caused,
in proceedings of which the Office has seisin. However the House of
Lords in E's Applications \[1983\] RPC 231 overruled the specific
judgment in Fater's Application that (the original) Rule 100 \[Patents
Rules 1978\] could be used to extend a time limit in a manner not
allowed by Rule 110 \[Patents Rules 1978\], since a general provision of
the Rules should not be construed in such a way as to circumvent a
specific provision of the Rules. Nevertheless, the proviso, added to
Rule 100 \[Patents Rules 1978\] after the judgment in Fater, and not in
point in E's Applications, enabled the comptroller to extend a time or
period specified in the Act or prescribed in the Rules to correct an
irregularity in procedure attributable wholly or in part to an error,
default or omission on the part of the Office. Paragraph (3) of rule 107
of the Patents Rules 2007 now gives the power to extend a period of time
specified in the Act or listed in Parts 1 to 3 of Schedule 4 (whether
that period has already expired or not), only if the irregularity or
prospective irregularity is attributable, wholly or in part, to a
default, omission or other error by the comptroller, an examiner, or the
Patent Office and it appears to the comptroller that the irregularity
should be rectified. In Alphaplan Ltd's Application ([BL
O/127/93](https://www.gov.uk/government/publications/patent-decision-o12793)),
an incorrectly (post)dated cheque was not honoured by the agents' bank
which meant that the application was not entitled to the original date
of filing since the filing fee could not be considered to have been paid
on that date ([see
15.06](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-06));
the hearing officer held that rectification under Rule 100(1) \[Patents
Rules 1990\] was not possible; however under Rule 100(2) \[Patents Rules
1990\] he allowed the application to take as its filing date the date by
which the Office cashiers should have picked up the error.

### 123.09 {#ref123-09}

According to the judgment of the Court of Appeal in M's Application
\[1985\] RPC 249 (concerning failure to file Form 10 within the
prescribed period) the proviso to rule 100 \[Patents Rules 1982\] only
came into operation if the applicants showed, firstly, that the Office
was guilty of an error, default or omission (the "omission" being an
omission to do something which it could be said there was some sort of
obligation to do), secondly, that such error, default or omission could
be said to have contributed to the failure to meet the time limit, and
thirdly, that the error, default or omission played an active causative
role in the irregularity which had taken place. It did not have to be
the sole cause but it had to be something more than a mere causa sine
qua non so that it could be said to be a partial cause of the
irregularity in the sense of having actively brought it about. In M's
Application the Patent Agents had filed a letter stating that Form 10
was filed therewith but the fact that neither the form nor the fee was
enclosed with the letter was not detected until some time later.
However, the Court of Appeal held that the Office is not under an
obligation to answer routine letters within any particular time limit,
so the delay in detecting the defect did not constitute such an
"omission" on the part of the Office. The Court also postulated the
possibility that a neglect of some well-established and generally well
known practice on which it is known or may be assumed that all those
dealing with the Office can be said to rely ­even though it may not be
backed up by any statutory or regulatory backing - may constitute an
"error or default or omission", but declined to make a decision to that
effect.

### 123.10 {#ref123-10}

In Mills' Application \[1985\] RPC 339 the Court of Appeal endorsed the
three conditions set out in M's Application for the applicability of the
proviso to rule 100 \[Patents Rules 1982\] subject to the rider that,
with regard to an "omission", the obligation need not necessarily be of
a legally enforceable nature. In Mills, the applicant sought to
attribute his failure to file Form 10 in time at least in part to the
Office's failure (which was accepted on the balance of probabilities) to
provide a free copy of the printed specification as had been promised in
the section 16 publication letter and which, the applicant argued, would
have served as a reminder. In overturning the decision in the Patents
Court, which relied on the fact that the Office's failure was not in
respect of a statutory requirement, the Court of Appeal held that the
applicant could reasonably have expected the Office to fulfil a specific
promise given to the Agents in accordance with a well established
practice.

### 123.10.1 {#ref123-10-1}

Rule 107 may be used to rectify an irregularity where that irregularity
was attributable, at least in part, to a reasonable expectation created
by well established Office practice. As discussed in
[37.20.1](/guidance/manual-of-patent-practice-mopp/section-37-determination-of-right-to-patent-after-grant/#ref37-20-1),
the Hearing Officer in Rigcool Ltd v Optima Solutions UK Ltd BL
[O/149/11](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F149%2F11&submit=Go+%BB){rel="external"}
decided that a "period of two years beginning with the date of the
grant" ended the day before the second anniversary of the date of grant.
However, in a subsequent decision concerning the same dispute
([O/182/11](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F182%2F11&submit=Go+%BB){rel="external"}),
the Hearing Officer extended this period by one day under r.107, as they
accepted that previous Office decisions implied that Office practice was
to accept that the period expired on the second anniversary, and not the
day before.

### 123.10.2 {#ref123-10-2}

Furuno Electric Co. Ltd's Application ([BL
O/208/10](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F208%2F10&submit=Go+%BB){rel="external"})
concerned the use of r.107 to rescind grant of the patent, and thereby
allow a divisional to be filed. In this case it was alleged that a
failure by the examiner to raise objections -- most significantly to a
statement of invention corresponding to a deleted claim -- constituted a
procedural irregularity. The applicant pointed to passages in this
Manual (e.g. at
[18.68](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-68)
and
[14.147](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-147))
concerning inconsistencies between the claims and the description, and
said these passages showed that the examiner had made a clearly wrong
assessment which should be rectified using r.107. The hearing officer
held that the examiner had exercised his judgement in deciding that the
claims were clear and that the remaining deficiencies were of a minor
nature; whether this judgement was correct or not was a separate
question from whether the examiner, having come to that view, followed
the correct procedure. Although the applicant disagreed with the
examiner's judgement of the facts, no procedural irregularity had
occurred. It was also noted that, while the Manual may be referred to in
determining whether a procedural irregularity has occurred, not all the
guidance in the Manual can be regarded as procedural; nor can every
alleged failure to follow that guidance be regarded as a procedural
failing.

### 123.10.3 {#ref123-10-3}

Office guidance is regularly updated, particularly when the meaning of
statute is clarified by the courts, a hearing officer, or another
source. This does not mean that every action the Office made under
earlier guidance amounts to an error which contributed to an
irregularity of procedure. [Cypress Semiconductor Corporation's
Application (BL
O/326/16)](https://www.ipo.gov.uk/p-challenge-decision-results/o32616.pdf){rel="external"}
relates to an application which was reinstated in 2009 following a
failure to respond to an examination report. The Office followed its
practice at the time and did not provide a new period for compliance.
The applicant was not therefore able to amend their application but
could only make observations. In 2010 Office practice concerning
reinstatement following the failure to respond to an examination report
changed to provide an applicant with an opportunity to amend their
application. In 2015 the applicant, noticing that the application had
not been terminated, argued that, given the change in practice, the
period for compliance should be treated as having been extended so that
previously proposed amendments could be considered. The hearing officer
however held that no new period for compliance could be set, either
under the reinstatement provisions of section 20A or under the
provisions of rule 107.

\[ It is possible to utilise Rule 107 to extend a period of time
specified in the Act or listed in Parts 1 to 3 of Schedule 4. However,
the power afforded by Rule 107 should not be applied too readily, but
rather only in cases where the circumstances warrant it and in
accordance with the case-law (referred to above). Whether to exercise
discretion must be decided on a case-by-case basis. Therefore, any
consideration of exercise of discretion under Rule 107 to extend a
period of time should be referred to the Head of the Registered Rights
Legal Team (via the appropriate legal advisor), or, the Head of
Administration, as appropriate.

\[ Whenever a lost or mislaid document comes to light an appropriate
minute setting out the circumstances of its discovery should be made.
Facts should not be withheld to avoid disclosing someone's error, since
such facts may be of the utmost importance in determining whether or not
an irregularity of procedure has occurred to justify exercise of the
Comptroller's discretion should the need subsequently arise.

\[ Formalities should check the address on lost correspondence to ensure
it matches the most recent address for service for the relevant
application. It may also be necessary to look for any unactioned
requests to update the address, for example by form 51 of letter. If
appropriate, formalities should inspect their Post Book to confirm
whether or not the correspondence was actually issued.\]

\[ If the correspondence was never sent or was sent to an incorrect
address, this constitutes an irregularity in Office procedure and may be
dealt with under rule 107. In such cases, the matter should be dealt
with by Formalities, who should contact the intended recipient to
apologise for the error and indicate that an extension of time is being
considered under rule 107. A message should then be sent to the Head of
Administration, who may suggest that the correspondence be reissued with
an amended reply date. The compliance period may also be extended if
this is felt to be necessary. In the event that the correspondence was
accidentally sent to a third party, a copy of the original communication
should still be reissued even if the third party has forwarded the
misdirected mail to the correct address.\]

\[ When a mistake is made in the Office, and particularly when it causes
inconvenience or extra work for an applicant, an apology for the mistake
should be made. If it is felt that the nature or seriousness of the
mistake necessitates a fuller explanation or other action then the Group
Head should be consulted, the matter being referred up if necessary. \]

\[ All lost or misdirected correspondence must be reported to the
Casework Lead Manager. Each instance will be logged and passed to the
Information Security and Customer Insight teams for review.\]

\[Dispensation by comptroller -- deleted\]

### 123.11 {#section-6}

\[deleted\]

### 123.12 {#section-7}

\[deleted\]

### 123.13 {#section-8}

\[deleted\]

### 123.14 {#section-9}

\[deleted\]

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(c\) requiring fees to be paid in connection with any such proceeding or matter or in connection with the provision of any service by the Patent Office and providing for the remission of fees in the prescribed circumstances;
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Fees

### 123.15 {#ref123-15}

According to the Patents (Fees) Rules the fees to be paid in respect of
any matters arising under the Act shall be those specified in those
Rules and the Schedules to those Rules. Those Schedules also give, where
appropriate, the number of the corresponding Patents Form to accompany
the fee. The fees are periodically revised; the current Patents (Fees)
Rules should always be consulted. Where any form specified in the
Schedules to the current Patents (Fees) Rules is required to be filed it
shall be accompanied by the specified fee (if any) or, where payment may
be made within a prescribed period of time after the form has been
filed, the specified fee shall be paid within that period. Under the
Public Offices Fees (Patents, Designs and Trade Marks) Order 1964, all
fees must be paid in money. Cash, money order, postal order, cheque,
banker's draft or credit/debit card are accepted; adhesive stamps are
not. Fees may also be paid by authorising debit of funds from a deposit
account administered by the Office, or by direct credit to the Office's
account, but in that event a fee is not deemed to have been received
until this account is actually credited. Payment for statutory fees is
accepted by credit/debit card, but this cannot be used for adding to a
deposit account, or for international patent fees (other than the
transmittal fee) payable under the PCT.

### 123.16 {#ref123-16}

r.106 is also relevant.

Provision for the remission of fees is made by rule 106. With regard to
the remission of fees under rule 106 in the case of a divisional
application, [see
15.27](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-27)
and
[15.47-49](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-47)
in the case of requests for an opinion under section 74A, [see
74A.04](/guidance/manual-of-patent-practice-mopp/section-74a-opinions-on-matters-prescribed-in-the-rules/#ref74A-04),
and in the case of an international application, [see
89B.08](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-08).
There is no appeal from any decision of the comptroller under rule 106.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(d\) regulating the mode of giving evidence in any such proceeding and empowering the comptroller to compel the attendance of witnesses and the discovery of and production of documents;
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Evidence

### 123.17 {#ref123-17}

r.73(2), r.73(3), r.86, r.87 is also relevant

Under r.80(2) the comptroller may at any time they think fit give leave
to a party to proceedings before the Comptroller to file evidence upon
such terms as the Comptroller thinks fit. Under r.82(2), the Comptroller
may give directions (which may be subject to conditions; see r.87
[123.05.11](#ref123-05-11)) as to the issues on which they require
evidence, the nature of the evidence required and the way in which it is
to be placed before them, and they may use these powers of control to
exclude evidence which would otherwise be admissible. R.87(1) allows
evidence to be given by witness statement, statement of case, affidavit,
statutory declaration or any other form which would be admissible in
proceedings before the court. It follows from rr.82 and 87 that the
comptroller may take oral evidence in lieu of or in addition to written
evidence and in particular may allow any witness to be cross-examined on
their written evidence. In general however, evidence filed for the
purposes of Part 7 of the Rules (Proceedings before the comptroller) is
to be given by witness statement unless the comptroller directs or any
enactment requires otherwise, a witness statement being defined as a
written statement signed by a person and containing evidence which that
person would be allowed to give orally. A witness statement or statement
of case may only be given in evidence if it includes a statement of
truth; for the purposes of Part 7 of the Rules (Proceedings heard before
the comptroller), this must be dated and signed by the person making the
statement in the case of a witness statement, and in other cases by the
party or their legal representative. In relation to the attendance of
witnesses and the discovery and production of documents, the comptroller
has the powers of a judge of the High Court or (in Scotland) the Court
of Session other than the power to punish summarily for contempt of
court.

### 123.18 {#ref123-18}

r.79 r.82(1) is also relevant

Under their powers of case management, the comptroller may direct the
filing of documents, information or evidence as they think fit. Where a
"relevant statement" (defined as a witness statement, statement of case,
affidavit or statutory declaration) refers to another document, a copy
of that document should accompany each copy of the relevant statement
that is filed. This does not apply where the relevant statement is sent
to the comptroller and the document was published by the comptroller or
is kept at the Office. It is desirable as a matter of convenience and
practice that objections to the admissibility of evidence should be
raised as early as possible but, as was noted in Peckitt's Application
\[1999\] RPC 337, the fact that an objection was not raised as early as
it might have been does not make admissible that which was not
admissible. Cross-examination of witnesses and the admission of hearsay
evidence, other than in Scotland, under the Registered Designs Act 1949,
Patents Act 1977, Copyright, Designs and Patents Act 1988 and Trade
Marks Act 1994 are subject to the provisions of the Practice Notice
dated 4 January 1999 which appeared in the Patents and Designs Journal
No 5727 10 February 1999 and \[1999\] RPC 294 (reproduced in the
"Relevant Official Notices and Directions" section of this Manual).

### 123.19 {#section-10}

\[deleted\]

### 123.20 {#section-11}

The mode of giving evidence and related powers of the comptroller are
discussed further in Chapter 3 of the Patent Hearings Manual.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(e\) requiring the comptroller to advertise any proposed amendments of patents and any other prescribed matters, including any prescribed steps in any such proceeding;
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Advertisement

### 123.21 {#section-12}

Certain matters must be advertised by the comptroller, the necessary
advertisements being made in the Patents Journal which the comptroller
is required to publish by rule 117, [see 123.72](#ref123-72). According
to rule 117(a), the Journal should inter alia contain particulars of
applications for and grants of patents and of other proceedings under
the Act.

### 123.22 {#ref123-22}

An application to the comptroller for leave to amend the specification
of a patent is advertised in accordance with rule 75.

### 123.23 {#section-13}

Certain steps in other proceedings under the Act are advertised in
accordance with rules 40(3), 40(9), 43(2), 75 and 105(5)-(7).

### 123.24 {#section-14}

In accordance with rule 45, the comptroller may publish or advertise
such things done under the Act or Rules in relation to the register as
they think fit. The Journal includes a list of those patents and
published applications in relation to which transactions, instruments or
events affecting rights therein or thereunder have been entered in the
register, see section 32. [See also 123.73](#ref123-73).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(f\) requiring the comptroller to hold proceedings in Scotland in such circumstances as may be specified in the rules where there is more than one party to proceedings under section 8, 12, 37, 40(1) or (2), 41(8), 61(3), 71 or 72 above;
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Proceedings in Scotland

### 123.25 {#ref123-25}

r.88(1) r.88(2)is also relevant.

Where there is more than one party to proceedings, any party thereto may
apply to the comptroller to hold proceedings in Scotland, in accordance
with rule 88(1).

### 123.26 {#section-15}

The application will be granted where all the parties consent to holding
proceedings in Scotland, or where the comptroller considers it
appropriate.

### 123.27 {#section-16}

(deleted)

### 123.28 {#section-17}

PR part 7 is also relevant.

Rule 88(1) is mentioned in Part 1 of Schedule 3 to the Patents Rules
2007 and the application must therefore be started by filing Form 2 and
a statement of grounds in duplicate.

### 123.29 {#section-18}

r.88(3) is also relevant

There is no appeal from any decision of the comptroller refusing to
grant an application under r.88(1).

### 123.30 {#section-19}

See [123.17](#ref123-17) with regard to the powers of the comptroller in
proceedings in Scotland (rule 86), and Chapter 8 of the Patent Hearings
Manual for the conduct of proceedings in Scotland.

  ----------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(g\) providing for the appointment of advisers to assist the comptroller in any proceeding before them;
  ----------------------------------------------------------------------------------------------------------

### Advisers

### 123.31 {#section-20}

Rule 102 empowers the comptroller to appoint advisers to assist the
Comptroller in any proceeding before them and to settle any question or
instructions given to them.

### 123.32 {#section-21}

\[deleted\]

### 123.33 {#section-22}

\[deleted\]

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(h\) prescribing time limits for doing anything required to be done in connection with any such proceeding by this Act or the rules and providing for the alteration of any period of time specified in this Act or the rules;
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Time limits

### 123.33.1 {#section-23}

The Patents Rules 2007 prescribe many periods in connection with the
processes for applying for and maintaining patent rights, and for
resolving disputes over those rights. Time periods prescribed in the
Rules are generally calculated using the "exclusive rule", meaning that
the prescribed period does not include the defining date (for example a
period of one month "from 15 April" ends on 15 May). [The Patents
(Amendment) Rules 2011 (SI 2011/2052) (PDF,
49.2KB)](http://www.legislation.gov.uk/uksi/2011/2052/pdfs/uksi_20112052_en.pdf){rel="external"},
which came into force on 1 October 2011, adjusted the wording used to
define various time periods so that there is no doubt that these periods
should be calculated using the exclusive rule. See the [Practice Notice
on time
periods](http://webarchive.nationalarchives.gov.uk/20140603093549/http://www.ipo.gov.uk/p-pn-patentrules2007.htm){rel="external"}
for further details.

### 123.34 {#ref123-34}

r.108 is also relevant

Rule 108 of the Patents Rules 2007 reads- ­\
(1) The comptroller may, if he thinks fit, extend or further extend any
period of time prescribed by these Rules except a period prescribed by
the provisions listed in Parts 1 and 2 of Schedule 4.\
(2) The comptroller shall extend, by a period of two months, any period
of time prescribed by the provisions listed in Part 2 of Schedule 4
where­\
(a) a request is filed on Patents Form 52\
(b) no previous request has been made under this paragraph; and\
(c) that request is filed before the end of the period of two months
beginning immediately after the date on which the relevant period of
time expired.\
(3) The comptroller may, if he thinks fit, extend or further extend any
period of time prescribed by the rules listed in Part 2 of Schedule 4
where­\
(a) a request is filed on Patents Form 52; and\
(b) the person making the request has furnished evidence supporting the
grounds of the request, except where the comptroller otherwise directs.\
(4) Each request under paragraph (2) or (3) for a period of time to be
extended must be made on a separate form unless­\
(a) each of those requests relate to the same patent or application for
a patent; and\
(b) the grant of each of those requests would result in the expiry of
all the extended periods of time on the same date, in which case those
requests may be combined and made on a single form.\
(5) Any extension made under paragraph (1) or (3) shall be made­\
(a) after giving the parties such notice; and\
(b) subject to such conditions,as the comptroller may direct, except
that a period of time prescribed by the rules listed in Part 3 of
Schedule 4 may be extended (or further extended) for a period of two
months only.\
(6) An extension may be granted under paragraph (1) or (3)
notwithstanding the period of time prescribed by the relevant rule has
expired.\
(7) But no extension may be granted in relation to the periods of time
prescribed by the rules listed in Part 3 of Schedule 4 after the end of
the period of two months beginning immediately after the period of time
as prescribed (or previously extended) has expired.

### 123.35 {#section-24}

The rules listed in the above-referenced Parts of Schedule 4 of the
Patents Rules 2007 are given below.

### 123.36 {#ref123-36}

r.108(1) is also relevant.

Times or periods prescribed by the Rules thus fall into four categories
I to IV with regard to extension under rule 108:

### 1. periods which cannot be altered

### 123.36.1 {#ref123-36-1}

PR Sch 4, Part 1 is also relevant

These are the periods prescribed in the following parts of the Patents
Rules 2007: rules 6(2)(b), 7(1), 32(1), 37, 38, 40(1), 43(4), 58(3),
59(1), 66(3), 76(2), 77(8) and (10), 109, 116(2), paragraph 8(5) of
Schedule 1 (new deposits of biological material).

### 123.36.2 {#section-25}

\[deleted\]

### 123.36.3 {#section-26}

\[deleted\]

### 123.36.4 {#section-27}

\[deleted\]

### 2. periods which can be extended (1) by a single period of two months as of right on request and payment of a fee; and (2) by an alternative or additional period(s) at discretion on request and payment of fees

### 123.36.5 {#ref123-36-5}

r.108(2), r.108(3) PR, Sch 4, Part 2 is also relevant

These are the periods prescribed in the following parts of the Patents
Rules 2007: rules 8(1) and (2), 10(3), 18(1), 21, 22(1), (2) and (5),
28(2), (3) and (5), 30, 56(6) and (7), 58(4), 59(3), 60, 66(1) and (2),
68, paragraph 3(2) of Schedule 1 (filing of information in relation to
the deposit of biological matter).

### 123.36.6 {#section-28}

\[deleted\]

### 123.36.7 {#section-29}

A single two-month extension under rule 108(2) is obtainable for these
periods as of right on filing Patents Form 52 together with the
appropriate fee.

### 123.36.8 {#section-30}

r.108(2), r.108(3), r.108(6) is also relevant

The alternative or additional extension available at discretion under
rule 108(3) has to be requested on Patents Form 52 together with the
appropriate fee, and evidence supporting the grounds for the request is
required for the comptroller to consider extension. The request may be
made after expiry of the period to be extended (but see category IV
below). Factors influencing the exercise of discretion are dealt with in
[123.37](#ref123-37).

\[ If the request under r.108(3) concerns formalities matters it is
normally considered by the relevant formalities group, but may only be
allowed by the divisional Head of Admin (or above), provided it is
unopposed. If the request concerns substantive matters it is normally
considered by the relevant examiner, but may only be allowed by the
group head. Furthermore, where extensions of both the s.18(3) reply
period (which can be allowed by the examiner) and, under r.108(3), the
compliance period are required the group head decides both questions.
However, an opposed request can only be decided upon by a Divisional or
Deputy Director or above. If the request to extend the compliance period
or the s18(3) reply period is made whilst an application is awaiting a
hearing or decision, then the relevant hearing officer should consider
the request.\]

### 3. periods (other than in I and II) which can be extended at discretion

### 123.36.9 {#ref123-36-9}

r.108(1) r.108(6) is also relevant

Extension at the discretion of the comptroller under rule 108(1) is
applicable to many times and periods prescribed by the Rules. The
discretionary power under rule 108(1) is a general one to which the only
exceptions are times or periods prescribed by any of the rules specified
in either category I (no alteration possible) or category II (extensible
by two months and/or at the discretion of the comptroller). The request
may be made after expiry of the period to be extended (but see category
IV below). Factors influencing the exercise of discretion are dealt with
[in 123.37](#ref123-37).

### 4. periods in II or III which can be extended subject to further conditions

### 123.36.10 {#ref123-36-10}

r.108(5), (7) PR Sch 4, Part 3 is also relevant.

These are the periods prescribed in the following rules of the Patents
Rules 2007: 10(3), 12(3) and (9), 19, 21(1)(a) and (2)(a), 22, 28, 30,
58(4), 59(3), 60, 66(1) and (2) 68.

### 123.36.11 {#section-31}

\[deleted\]

### 123.36.12 {#section-32}

r.108(5), r.108(7) is also relevant

The further conditions that apply to extension of these periods are
that­

\(a\) any extension must be for two months, and (b) no extension is
possible if two months have elapsed since the last period expired.

These conditions limit the retrospective availability of extension of
these time periods, but reinstatement of the patent application under
section 20A becomes possible in situations where extension is not
available (section 20A(3)(a) - [see 20A.01 to
20A.13](/guidance/manual-of-patent-practice-mopp/section-20a-reinstatement-of-applications/#ref20A-01))

### Exercise of discretion; conditions

### 123.37 {#ref123-37}

s.20A is also relevant

Discretion should be exercised favourably if it is shown that the
failure to meet the time period was unintentional at the time that the
period expired (except where the extension of time is for the filing of
divisional application - [see
15.21](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-21)).
This is consistent with the statutory test that applies to requests for
reinstatement under s.20A ([see
20A.13-16](/guidance/manual-of-patent-practice-mopp/section-20a-reinstatement-of-applications/#ref20A-13)
for guidance on the meaning of unintentional). However, since rule 108
sets out no statutory test for discretionary extensions of time,
discretion may be exercised favourably in appropriate circumstances even
if the unintentional criterion does not appear to have been met. Prior
to the introduction of the reinstatement provisions under s.20A, a
number of cases were decided on the basis that there must have been a
continuing underlying intention to proceed with the application or
patent; a change of mind regarding whether to proceed on the part of
those responsible for its prosecution was held in Heatex Group Ltd's
Application (\[1995\] RPC 546) not to be a legitimate reason for
favourable exercise. In Meunier's International Application ([BL
O/013/01](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F013%2F01&submit=Go+%BB){rel="external"}),
the applicant had chosen to acquire patent protection in the UK via an
EP(GB) designation of his international application, rather than by
continuing with a GB designation and national phase entry. When it was
discovered that, by mistake, EP(GB) had not been designated, a request
for the application to belatedly enter the national phase directly was
refused by the hearing officer, who regarded this as a change of mind,
despite a continuing underlying intention on the part of the applicant
to protect his invention in the UK. In a broadly similar set of
circumstances, the hearing officer in Pilat's International Application
\[2003\] RPC 13 came to the same conclusion. In MacMullen's Application
([BLO/307/03](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F307%2F03&submit=Go+%BB){rel="external"}),
the hearing officer held that in order to demonstrate a continuing
underlying intention to proceed with an application where a form and
required fee had not been filed within the prescribed period due lack of
funds, it was necessary for the applicant to show that they had
insufficient funds to pay the fee and that they made genuine and
continuing efforts to obtain the required sum during this period. These
cases may also be useful in determining whether discretion can be
exercised favourably. However, in order to ensure consistency with the
reinstatement provisions, if the evidence provided shows that the
failure to meet the time period was unintentional, discretion should be
exercised favourably regardless of whether or not there has been a
continual underlying intention to proceed.

### 123.38 {#ref123-38}

r.108(5) is also relevant

The comptroller has discretion to impose conditions when making an
alteration under r.108(1) or r.108(3), after giving the parties such
notice as the Comptroller may direct. For example, in Chitolie's
Application ([BL
O/078/04](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=o%2F078%2F04&submit=Go+%BB){rel="external"},
upheld on appeal \[2004\] EWHC 1549 (Ch)), terms analogous to those
employed in restoration proceedings under s.28 were applied where an
application was resuscitated by the extension after having been
advertised as terminated (much as described in [123.06](#ref123-06)
where the resuscitation was under r.100).

\[ Third party terms will normally be appropriate where the case has
been published and previously advertised as terminated (or void in the
case of a European patent (UK) for which a translation was not filed in
time under s.77(6)). The "cut-off" for such terms will then be the date
on which the filing of Form 52 is advertised in the Journal. \]

### Other matters

### 123.39 {#ref123-39}

Although the wording of rule 108 permits certain of the times or periods
prescribed by the Patents Rules for doing any act or taking any
proceeding thereunder to be extended at the Office discretion, no
provisions are included for extending times or periods prescribed by the
Act itself (confirmed by the hearing officer in ITT Industries Inc's
Application \[1984\] RPC 23).

### 123.40 {#section-33}

Rule 108(4) provides that each request under rule 108(2) or (3) must be
made on a separate Form 52 unless each of those requests relate to the
same patent or application and the extended periods would expire on the
same date.

### 123.41 {#ref123-41}

The relationship of rule 108 with rule 107 is discussed in [123.06 to
123.10](#ref123-06) particularly in connection with the period for
filing Form 10 which is defined by rule 28, 60 or 68 ([see
18.02](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-02))
and extensible under rule 108(2) or (3) to (7).

### 123.42 {#section-34}

\[deleted\]

### 123.43 {#ref123-43}

### Rule 110

Rule 110 of the Patents Rules 2007 reads as follows ­\
(1) The comptroller may certify any day as an interrupted day where ­\
(a) there is an event or circumstance causing an interruption in the
normal operation of the Patent Office; or\
(b) there is a general interruption or subsequent dislocation in the
postal services of the United Kingdom.\
(2) Any certificate of the comptroller given under paragraph (1) shall
be displayed in the Patent Office and advertised in the journal.\
(3) The comptroller shall, where the time for doing anything under the
Act expires on an interrupted day, extend that time to the next
following day not being an interrupted day (or an excluded day).\
(4) In this rule ­"excluded day" means a day specified as an excluded
day in direction given under section 120; and; "interrupted day" means a
day which has been certified as such under paragraph (1).

### 123.44 {#ref123-44}

The comptroller has power under rule 110(1) to certify a day as an
interrupted day where there is an event or circumstances causing an
interruption in the normal operation of the Office, or where there is a
general interruption or subsequent dislocation in UK postal services. A
period of time specified under the Act or Rules for the giving, making
or filing of any notice, application or other document which expires on
a day so certified is extended to the first non-excluded day following
the end of the certified interruption. Rule 110(2) requires any such
certificate to be displayed in the Office. Although the comptroller's
powers under rule 110(1) are couched in discretionary terms, in practice
the Comptroller is under an obligation to issue a certificate where the
circumstances are such as to fall within the words of the rule, and the
effect of such a certificate may be retrospective or prospective or both
as the case may require (judgment of Patents Court in Omron Tateisi
Electronics Co's Application \[1981\] RPC 125).

### 123.45 {#ref123-45}

In Armaturjonsson AB's Application \[1985\] RPC 213, the Patents Court
held that the old form of rule 110 (formerly rule 111) was in no way
concerned with individual difficulty or problems of a purely personal
kind; it was concerned only with a state of affairs where not just one
individual applicant but many applicants or indeed anybody who is under
duty to do some act within some prescribed time may be affected. It
further held that rule 110 was concerned only with special circumstances
which may bring about interruption, which are not going to be catered
for by the excluded days provisions. Excluded days (such as the
Christmas holiday period in the application in suit) do not constitute a
period of general interruption as contemplated within rule 110.

\[To certify that there has been an interruption, the comptroller must
issue a certificate. The certificate need not be signed by the
comptroller themselves. The Director of Patents, Director of IPID and PD
Divisional and Deputy Directors are all authorised to sign on behalf of
the comptroller. Clearly, however, the most senior available officer
should be used. Once the certificate is signed, a copy must be displayed
in the Office (Concept House and 10 Victoria Street). A copy must also
appear as soon as possible in the various official Journals. Although
not a legislative requirement, a prominent website notice is clearly
appropriate too. It is very desirable to issue the certificate on the
interrupted day in question, but it is not essential to do this. It is
possible to certify either before or after the event that the normal
operation of the Office will be, or was, interrupted on a particular
day. The same publication requirements apply. If the certificate says
that a particular day or days were interrupted, then there is no need to
issue a further notice. However, if the certificate is open-ended, then
we must notify the public when the interrupted period has come to an
end. The original certificate may be attached to the notice for
reference. The notice should be publicised in the same way as the
original certificate -- on the website, in Journals and displayed at the
Office. The notice need not be made by the same person who signed the
certificate, but can be anyone (as identified earlier) who can act on
behalf of the comptroller in this respect. It may be helpful to attach
an explanatory note to the notice, so that the effect on users is clear.
\]

\[The certificate applies to time periods set out in the Acts and Rules,
and to periods that have been specified by the comptroller. Thus it does
not apply to time periods set out under the Patent Co-operation Treaty
or the European Patent Convention. Nor does it apply to time periods set
out in international trade mark legislation. Some of these treaties and
other instruments contain provisions which cater for certain types of
disruption in the UK and other states (see, for example, rule 134 EPC).
Such provisions are usually more restrictive in their effect than the UK
certificate of interruption. So where the filing of documents at the
Office is not possible at all, users who wish to meet a deadline under
one of these treaties or other instruments will be advised to try and
file the relevant documents at an alternative location, if the relevant
law allows. There is no simple rule that can be applied, and the
position will be determined on a case-by-case basis.\]

### 123.46 {#ref123-46}

Rule 111, r.110(2)&(4), r.6(1) is also relevant

Rule 111 of the Patents Rules 2007 reads as follows ­\
(1) The comptroller shall extend any period of time specified in the Act
or these Rules where they are satisfied that the failure to do something
under the Act or these Rules was wholly or mainly attributable to a
delay in, or failure of, a communication service.\
(2) Any extension under paragraph (1) shall be made ­\
(a) after giving the parties such notice, and\
(b) subject to such conditions,as the comptroller may direct.\
(3) In this rule "communication service" means a service by which
documents may be sent and delivered and includes post, facsimile, email
and courier.

### 123.47 {#ref123-47}

Rule 111 allows the comptroller to extend time periods if a particular
case of failure to do something under the Act or Rules was wholly or
mainly attributable to a delay in, or failure of, a communication
service involving the sending and delivery of documents. This includes
post, facsimile, email and courier, both inside and outside the UK.
Before allowing any extension under r.111(1), the comptroller is
required to give the parties notice, and an extension may be subject to
conditions as the comptroller may direct. Rule 111 may be used to extend
a prescribed time period regardless of whether or not it has expired.
Details of how to deal with lost post in relation to specified time
periods can be found below in paragraph 123.47.2.

### 123.47.1 {#ref123-47-1}

It is worth noting, that whilst rule 111 allows for relief where an
official communication can, on the balance of probabilities, be
demonstrated to have gone astray in the post, the rule requires that the
failure to meet a deadline is 'wholly or mainly' attributable to the
communication failure. If an application is found not to comply with the
Act and Rules by the end of the compliance period, it may not be
possible to extend the compliance period under rule 111 simply because
an examination report was not received. In [Daihatsu Motor Co Ltd and
others' Patent (BL
O/234/14)](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F234%2F14&submit=Go+%BB){rel="external"}
the hearing officer determined that it was the "lack of systematic
rigour" by the attorney in monitoring the compliance period and tracking
the application status, and not the non-arrival of a report of
re-examination under section 18(3), that was the main reason for the
failure to bring the application in order in time.

\[Where correspondence from the Office is reported as never having
arrived at its intended destination, or is reported as being misdirected
or delayed, this fact should be recorded by sending a minute to the
relevant formalities group with any relevant details. The relevant
Assistant Head of Administration will then decide whether a prescribed
period can be extended under rule 111. The assistant head of
administration will need to be satisfied 'that the failure to do
something under the Act or these Rules was wholly or mainly attributable
to a delay in, or failure of, a communication service'. Duplicate copies
of the original reports should be sent to the address for service, along
with PROSE letter RULE 111. If the application has been published, the
applicant/agent may also be directed to the online IPSUM service, which
contains links to patent citations. When a period of time set out in the
Act or Rules is due to expire imminently, a reminder should also be
included. The RULE 111 letter invites the applicant or attorney to
provide evidence that the delay or failure to respond was wholly or
mainly attributable to a problem with a communication service (as
opposed to a report having been mislaid after it was received). Such
evidence might include a copy of their office records relating to the
application in question and/or a signed statement briefly describing
their usual mail delivery arrangements and any procedures used to record
when a piece of correspondence arrives. Once evidence has been supplied,
Formalities should refer the application to the Assistant Head of
Administration, who will determine whether or not rule 111 may be used
to allow an extension of time. The length of any extension will depend
on the circumstances of the case. Once the Assistant Head of
Administration has determined what action is to be taken, the
formalities manager should ensure that the applicant or attorney is
informed in writing as to the outcome. If an extension under rule 111
has been allowed, the necessary changes should be made to the patents
database and the case dossier. If the applicant or attorney is unhappy
with the Office's decision in the matter, they can request to be heard
on the matter. Under no circumstances should reports be reissued with
new dates unless an extension of time has been agreed by one of the
Heads of Administration. If required, advice should be sought from
Patents Legal Section. \]

::: call-to-action
\[Where correspondence may have been directed to an incorrect postal or
email address or may have been sent to somebody other than the intended
recipient, a breach report should be submitted using the internal
Incident and Breach reporting system.\]
:::

### 123.47.2 {#section-35}

Specified periods of time are not covered by the provisions of rule 111.
Therefore the only option available for the applicant or attorney is to
request an extension of time under section 117B of the Patents Act (see
MoPP [18.53,
18.54](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-53)
and
[15A.23](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-23).
An examiner may consider it necessary to use rule 111 to extend the
compliance period (which is a prescribed period) at the same time. If
this is the case, the patent examiner should liaise with the Assistant
Head of Administration, who will quickly consider whether or not to
allow such an extension.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(i\) giving effect to an inventor's rights to be mentioned conferred by section 13, and providing for an inventor's waiver of any such right to be subject to acceptance by the comptroller;
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Mention of inventor

### 123.48 {#section-36}

Rule 10 sets out the procedure where any person alleges that they ought
to have been mentioned as the inventor or joint inventor of an
invention, [see 13.04 to
13.07](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-04),
or that any person mentioned as sole or joint inventor ought not to have
been so mentioned, [see
13.17](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-17).

### 123.49 {#section-37}

See [13.08 to
13.15](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-08)
for details of the procedure for identifying the inventor or inventors
(and derivation of the right to apply) where the applicant is not the
inventor or sole inventor.

### 123.50 {#section-38}

The procedure for a person named as inventor to waive their right to be
mentioned is set on in rule 11, see [13.03 to
13.03.1](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-03).

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(j\) without prejudice to any other provision of this Act, requiring and regulating the translation of documents in connection with an application for a patent or a European patent or an international application for a patent and the filing and authentication of any such translations;
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Translations

### 123.51 {#section-39}

The general rule regarding translations of documents in connection with
patent applications is rules 113 to 115, but rules 6 to 9, 35, 56 to 59
and 66 and 68 to 70 are also relevant.

### 123.52 {#section-40}

Rule 9 relates to the translation of priority documents, [see 5.11 to
5.13](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-11),
and r.35(4) and (5) concerns the need for translations where it is
desired to amend a patent of which the specification as published was
not in English.

### 123.53 {#section-41}

Rules 56 and 57 relate to translations of European patents (UK) and of
claims of applications for such patents, under ss.77(6) (see
[77.13-25](/guidance/manual-of-patent-practice-mopp/section-77-effect-of-european-patent-uk/#ref77-13))
78(7) ([see
78.11-15](/guidance/manual-of-patent-practice-mopp/section-78-effect-of-filing-an-application-for-a-european-patent-uk/#ref78-11))
and 80(3) (see
[80.02-06](/guidance/manual-of-patent-practice-mopp/section-80-authentic-text-of-european-patents-and-patent-applications/#ref80-02)).
Rules 58(4) and 59(3) relate to a translation of a European patent
application where conversion into an application under the Act has been
requested under section 81 (see
[81.05-06](/guidance/manual-of-patent-practice-mopp/section-81-conversion-of-european-patent-applications/#ref81-05)).

### 123.54 {#section-42}

Rule 69(1)(b) and (5)(b) relate to a translation of information
regarding the deposit of biological material in connection with an
international application (UK), [see
89A.10](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-10).
In addition, rules 69 and 70 relate to the required content of any
translation of an international application (UK) and amendments thereof,
[see
89A.08-09](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-08)
and
[89A.25](/guidance/manual-of-patent-practice-mopp/section-89a-international-and-national-phases-of-application/#ref89A-25).

### 123.55 {#section-43}

\[deleted\]

### 123.56 {#section-44}

According to rule 113(1), any document or part thereof not in English is
required to be accompanied by a translation, unless that document is
listed in r.113(2). Where the document is or forms part of an
application for a patent and no translation is filed [see
15.06.1](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-06-01),
[15.06.2](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-06-02),
and
[16.14](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-14).

### 123.57 {#section-45}

r.115 is also relevant

It is not normally necessary to verify the translation. However, where
the comptroller has reasonable doubts about the accuracy of any
translation of a document, the person supplying the translation is
notified and asked to furnish evidence to establish that the translation
is accurate. If evidence is not provided following this request, the
comptroller has discretion to take no further action in relation to that
document.

### 123.58-59 {#section-46}

\[deleted\]

### 123.60 {#section-47}

r.113(3) is also relevant

Where more than one copy of a document is filed or sent, it should be
accompanied by a corresponding number of copies of the translation.

### 123.61 {#section-48}

r.113(5), r.113(6), r.108(1) is also relevant

Where an International Search Report, an International Preliminary
Report on Patentability or an International Preliminary Examination
Report is filed at the Office in relation to an international
application for a patent (UK) and that report cites or refers to any
document in a language other than English or Welsh, a translation into
English of the document may be requested. It should be filed within two
months (extensible at the comptroller's discretion) of the date of the
request. If it is not filed, the comptroller may, if they think fit,
take no further action in relation to the application. [See also
89B.10-11](/guidance/manual-of-patent-practice-mopp/section-89b-adaptation-of-provisions-in-relation-to-international-application/#ref89B-10).

### 123.62 {#section-49}

r.114 is also relevant

A party who institutes proceedings before the comptroller in relation to
a European patent (UK), the published specification of which is in
French or German, is required to furnish a translation of the
specification unless such a translation has already been filed under
s.77(6) or the comptroller determines that it is not necessary. This
also applies to the making of a request for an opinion under section
74A. A party given leave to amend the patent during such proceedings is
required to furnish a translation of the amendment into the language in
which the specification was published.

### Register of patent agents

### 123.63 {#section-50}

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(k\) related to registration of patent agents. Together with other provisions of the 1977 Act concerning patent agents, it has been repealed by the CDP Act and replaced by Part V of the CDP Act (Patent Agents and Trade Mark Agents), see particularly s.275 thereof.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 123.64-123.69 {#section-51}

\[Deleted\]

  -------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2)**
  \(l\) providing for the publication and sale of documents in the Patent Office and of information about such documents.
  -------------------------------------------------------------------------------------------------------------------------

### Publication and sale of documents

### 123.70 {#section-52}

According to rule 119, the comptroller may arrange for the publication
and sale of copies of documents (in particular, specifications of
patents and applications for patents) in the Office.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(2A)**
  The comptroller may set out in directions any forms the use of which is required by the rules; and any such directions shall be published in the prescribed manner.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 123.70.1 {#ref123-70-1}

Subsection (2A) was inserted by the Patents Act 2004 and enables the
comptroller to specify in directions the content and layout of the
Patents Forms, which previously had to be prescribed by rules.
[Directions given under the
Act](https://www.gov.uk/government/collections/patent-directions) are
available on the IPO's external website. Directions given under
s.123(2A) are also required to be published in the Journal under
r.117(c).

  ---------------------------------------------------------
   
  **Section 123(3)**
  Rules may make different provision for different cases.
  ---------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 123(3A)**

  It is hereby declared that rules\
  (a) authorising the rectification of irregularities of procedure, or\
  (b) providing for the alteration of any period of time, may authorise
  the comptroller to extend or further extend any period notwithstanding
  that the period has already expired.
  -----------------------------------------------------------------------

### 123.70.2 {#section-53}

The CDP Act inserted subsection (3A) into section 123 of the 1977 Act
which is concerned with the power to make rules under that Act. The
rules prescribe various times or periods for performing particular
actions. The rules also empower the comptroller to extend some of those
times or periods. It has always been assumed that such extension can be
made even though the time or period has already expired, and the rules
have always been operated on that basis. Subsection (3A) has been added
as an "avoidance of doubt" provision. This paragraph came into effect on
Royal Assent (15 November 1988). See [123.34 to 123.47](#ref123-34) for
details of such extensions of periods.

### \[Sections 123(4) and (5) Repealed\]

### 123.71 {#section-54}

Subsections (4) and (5) of s.123 required Treasury consent in order to
make rules prescribing fees and to determine the remuneration of
advisers appointed to assist the comptroller in proceedings. These
requirements were removed by the repeal of these subsections by Schedule
2 paragraph 26 of the Patents Act 2004.

### 123.71.1 {#section-55}

\[deleted\]

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(6)**
  Rules shall provide for the publication by the comptroller of a journal (in this Act referred to as "the journal") containing particulars of applications for and grants of patents, and of other proceedings under this Act.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### The Journal

### 123.72 {#ref123-72}

The Patents Journal contains particulars of applications for and grants
of patents and of other proceedings under the Act and any other
information that the comptroller considers to be generally useful or
important, as required by rule 117. It is published weekly, usually on a
Wednesday.

### 123.73 {#ref123-73}

Publication of applications and grant of patents are advertised in the
Journal as required by sections 16 (see
[16.02](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-02)
and 24 (see
[24.01-24.02](/guidance/manual-of-patent-practice-mopp/section-24-publication-and-certificate-of-grant/#ref24-01))
respectively. Other particulars of proceedings under the Act given in
the Journal include those mentioned in [123.22 to 123.24](#ref123-22).

### 123.74 {#section-56}

Certain information about European Patents (UK) is also listed, viz such
patents granted under Article 97 of the European Patent Convention, such
patents revoked under Article 101 of that Convention, such patents
declared void under s.77(7) for failure to file a translation into
English and such patents ceased through non-payment of renewal fees.

### 123.75 {#section-57}

In addition, the Journal includes information about Office publications,
official notices issued by the Office, brief particulars of recent
judgments and decisions, proceedings under the Registered Designs Act
1949 and information concerning the Science Reference Library.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 123(7)**
  Rules shall require or authorise the comptroller to make arrangements for the publication of reports of cases relating to patents, trade marks, registered designs or design right decided by them and of cases relating to patents (whether under this Act or otherwise) trade marks, registered designs, copyright and design right decided by any court or body (whether in the United Kingdom or elsewhere).
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Reports of cases

### 123.76 {#section-58}

The comptroller is required by rule 118 to make arrangements for the
publication of reports of cases relating to patents, trade marks,
registered designs or design right decided by the Comptroller and
reports of cases relating to patents (whether under the Act or
otherwise), trade marks, registered designs, copyright and design right
decided by any court or body (whether in the United Kingdom or
elsewhere). The Office therefore arranges for the publication of the
Reports of Patent, Design and Trade Mark Cases (RPCs).

### 123.77 {#section-59}

The CDP Act amended s.123(7) to cover cases relating to Design Right,
whether decided by the comptroller or by any court or body in the UK or
elsewhere.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
