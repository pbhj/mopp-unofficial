::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 131A: Scotland {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (131A.01) last updated: April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 131A.01 {#a01}

This section was introduced by the Scotland Act 1998 (Consequential
Modifications)(No.2) Order 1999 (SI 1999 No. 1820). It clarifies how
certain references in the Act are to be interpreted in the light of
Scottish devolution and the setting up of the Scottish Parliament.

### Section 131A

In the application of this Act to Scotland -

\(a\) "enactment" includes an enactment comprised in, or in an
instrument made under, an Act of the Scottish Parliament;

\(b\) any reference to a government department includes a reference to
any part of the Scottish Administration; and

\(c\) any reference to the Crown includes a reference to the Crown in
right of the Scottish Administration.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
