::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 19: General power to amend application before grant {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (19.01 - 19.26) last updated: October 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 19.01

Provisions relevant to this subject are laid down in r.31.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 19(1)**
  At any time before a patent is granted in pursuance of an application the applicant may, in accordance with the prescribed conditions and subject to section 76 below, amend the application of his own volition.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 19.02 {#ref19-02}

(r.107 s.25(1) is also relevant)

To be effective a request to amend must be received before the issue of
the letter informing the applicant that a patent has been granted [see
18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent#ref18-86).
If it is received before that date but too late to prevent issue of the
letter the grant may be rescinded; the request should then be considered
as described in paragraphs [19.20-19.22](#ref19-20). If the request is
received on or after that date it is too late to amend the application;
amendment of the specification of the patent may be sought after the
date of publication of the notice of grant in the Journal see
[27.04](/guidance/manual-of-patent-practice-mopp/section-27-general-power-to-amend-specification-after-grant/#ref27-04).
It is additionally necessary for the request to be received while the
application is still in being; an application apparently cannot be
amended after it has been treated as withdrawn through failure to meet a
formal requirement or otherwise terminated, even if allowance of the
amendment would allegedly have the effect of avoiding termination (eg by
relinquishing a claim to priority, [see 19.11](#ref19-11), and thus
apparently giving more time for Form 9A to be filed). This is because
amendment takes effect ex nunc as explained in [19.04](#ref19-04) and,
in addition, it appears that the general power to amend under s.19
cannot be used to circumvent the specific requirements of other sections
of the Act (cf Payne's Application \[1985\] RPC 193), [see
117.19](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-19).

### Amendments and corrections

### 19.03 {#ref19-03}

Although the ordinary dictionary definitions of these words overlap, and
the terms may often be employed interchangeably in normal parlance, they
are used in the Act and Rules in quite distinct and specific senses, and
it is convenient at this point to discuss in general terms the
difference between them. Correction is the alteration of a document so
that it may better express the intention the drafter had at the time of
drafting, including the case where an agent drafting a document has
misconstrued their instructions. If the alteration is sought because the
drafter has become aware of new facts, or because circumstances have
since changed, or because they have changed their mind, then this is not
correction but amendment.

\[ Whenever a request to effect an alteration is made informally in a
letter rather than formally on the appropriate form, it should be borne
in mind that the applicant may not have used the term "amend" or
"correct" in these precise ways. If the applicant's intentions are not
clear both options may if necessary be pointed out and the difference
explained, but care should be exercised against suggesting either option
in a case where it would appear prima facie not to be open. \]

### 19.04 {#ref19-04}

(r.105(3), s.27(3), s.75(3) is also relevant)

Correction of an application or the specification of a patent or of any
document filed in connection therewith is governed by s.117, and takes
effect ex tunc, that is, the document is deemed always to have been in
the state in which it is after the correction. If the correction sought
is in a specification then it is necessary to show that it may properly
be dealt with as a correction, but once this is established there is no
impediment to the making of the correction; in particular the question
of whether subject- matter is added or the protection conferred is
extended by the alteration does not arise (Rock Shing Industrial Ltd v
Braun AG, [BL
O/138/94](https://www.gov.uk/government/publications/patent-decision-o13894)).
In contrast, amendment of an application or of the specification of a
patent must comply with the requirements of s.76(2) or (3), [see
76.04-27](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-04).
Amendment of an application is governed by s.19 and takes effect ex
nunc, that is, from the time the amendment is made; amendment of the
specification of a patent is carried out under s.27 (or under s.75 if
done while there are pending proceedings in which the validity of the
patent may be put in issue) and is deemed to have had effect from the
date of grant.

### Amendment of the request for grant

### 19.05 {#ref19-05}

(r.31(5)(b) r.31(6) r.49 is also relevant)

An application to amend the request for grant (Patents Form 1) must
(except as stated below) be made in writing, giving a reason for the
amendment. This may be filed at any time before the applicant is sent
the letter informing them that a patent has been granted ([see
18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86)),
but must be received before preparations for publishing the application
under s.16 have been completed if the change is to be included in the
published application. In such a case the published application will
carry a notice to the effect that the publication reflects an amendment
to the request for grant effected under r.31. A request to change a
person's address or address for service must be made in writing by that
person, [see
32.06](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-06).

### 19.06 {#ref19-06}

(r.31(5)(b), r.49(2), (4) r.113(1) is also relevant)

Any request to amend a name must be made on a Patents Form 20, as such a
change must be effected under s.32(2)(d) and in accordance with r.49(2).
Patents Form 20 can also be used to notify the IPO of address changes.
However, if only an address has changed, it is allowable for a request
to update the address on record to be made in writing. (Form 20 should
also be used for correction of a clerical error in a name -- see
117.17). Where the comptroller has reasonable doubts about whether the
alteration should be made, the person making the request will be
informed of these doubts and may be required to supply proof to support
their request. In the case of a corporate body any proof required should
be in the form of a document from the appropriate companies registration
authority, such as (for the UK) a certificate from Companies House, or
(for the USA) a State Certificate, or (for Germany) an extract from the
Handelsregister. For France or Belgium a copy of the gazette or
commercial paper in which the change of name was advertised should be
supplied. If a change of name has already been recorded at the EPO, a
copy of EPO Form 2544 (giving notice of the change made in the Register
of European Patents) will be accepted. Whenever the proof is in a
foreign language it should be accompanied by a verified translation. In
the case of a natural person whose name has been changed, eg by Deed
Poll, it is sufficient to give particulars of the issue of the gazette
or newspaper in which the change has been advertised.

\[ Where a change of name is accepted, the name is crossed out using
Enhance and the new name inserted. \]

### 19.07 {#ref19-07}

(r.49(5) is also relevant)

Changes in the applicant's name, address or address for service will be
effected in the Register. If the request to amend is received in time
the amendments will be included in the published application ([see
19.05](#ref19-05)).

\[ Where there is any doubt as to an applicant's name, address or
address for service, COPS and PROSE hold the definitive version of the
Register. \]

### 19.08 {#section-1}

If the change in name or address had taken place at the time the
application for a patent was filed (so that Form 1 was incorrect at the
time it was filed), then alteration of it is not amendment but
correction and must be effected under s.117 [see
117.17](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-17).

### 19.09 {#ref19-09}

Provided that Form 7 has not yet been filed, the addition or deletion of
an applicant or the substitution of one applicant for another, for
example because of assignment of the application or the death of an
applicant, may be effected by a request in writing. (This may also
necessitate an amendment of Part 8 of Form 1 [see
14.04.16-17](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-04).
Reasons for making the amendment should be provided in writing.
Supporting evidence (or sworn statement(s) indicating the consent of the
party or parties affected by the change) is required and the statements
made on a subsequent Form 7 must be consistent with the amendments made
and the reasons given. If however Form 7 has already been filed, any
assignment of the application or other change in ownership must be
notified under s.30.

### 19.10 {#section-2}

A request to amend the title of the invention on Form 1 is generally
allowable. It is acceptable for amendment of the Form 1 title to result
in a discrepancy between the Form 1 title and the title on page 1 of the
specification [see
14.49](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-49).

\[ The formalities examiner should forward such a request to the
relevant examiner, adding a minute to the PDAX dossier, to draw the
examiner's attention to the request. If the examiner allows the request,
they should create a minute and send the appropriate PDAX message to the
formalities examiner who will effect the changes using the "Enhance"
function in PDAX and ensure that the appropriate COPS action is taken.\]

### 19.11 {#ref19-11}

If the application includes a declaration of priority [see
14.04.14](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-04-14)
or claims an earlier date of filing [see
14.04.15](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-04-15)
either of these may be relinquished by a request to amend Part 5 or Part
6 of Form 1. (It should be remembered that abandonment of the claim to
an earlier filing date may necessitate relinquishment of a claim to
priority). The proposed amendment may arise as a consequence of an
objection made under s.18(3), whether to meet that objection or in
anticipation of a further objection which would arise as a result of the
action taken, for example where a declaration of priority is deleted
consequent on division following an objection of plurality of invention.
In all cases, including when a request is made to relinquish divisional
status following a preliminary objection [see
15.34-15.35](/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-34),
the request to amend Form 1 should be made in writing, giving a reason
for the amendment. For making a declaration of priority after filing a
Form 1, [see 5.07 to
5.07.3](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-07).

\[ A request for the deletion of a declaration of priority must be
referred with an explanatory minute to the relevant Casework Lead
Manager via the Formalities Manager. The Casework Lead Manager will
authorise the amendment, alter Form 1 using the "Enhance" function in
PDAX and then update the history action log. \]

### 19.12 {#ref19-12}

The applicant must always be informed whether or not an application to
amend the request for grant has been allowed.

### Amendment of the specification of an application

### 19.13 {#ref19-13}

(s.18(3) r.31, r.66A, CoP s.76(2) is also relevant)

In addition to having the right to amend the specification in order to
overcome an official objection, the applicant may make amendments for
reasons of their own. Such voluntary amendments may however only be made
as provided for by r.31 or r.66A [see 19.15- 19.16](#ref19-15);
[19.20](#ref19-20), and it is preferable for voluntary amendments to be
kept to a minimum and filed as early as possible. No amendment is
allowable which results in the application disclosing any matter which
extends beyond that disclosed in the application as filed [see
76.03-76.23](/guidance/manual-of-patent-practice-mopp/section-76-amendments-of-applications-and-patents-not-to-include-added-matter/#ref76-03).
For the procedure for effecting amendments, [see
18.61-18.62](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-61).

### 19.14 {#section-3}

Deleted

### Amendment as of right

### 19.15 {#ref19-15}

(r.31(3), s.16(1) is also relevant)

Between the dates of issue of the search report and of the first
substantive examination report the applicant may, of their own volition,
amend the specification, for example to take account of documents cited
in the search report. There is no restriction on the number of times
this may be done, and this right is not affected by the issue of an
examination opinion with the search report [see 17.83.3 and
17.83.4](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-83-3).
The amendments are deemed to be effected at the time they are filed, but
are not considered by the substantive examiner, for example to see if
they add subject-matter, until the first substantive examination. Any
amended or new claims subsisting immediately before preparations for
publication have been completed are included in the published
application [see
16.16-16.19](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-16)

### 19.15.1 {#ref19-15-1}

(r.66A is also relevant)

Where an international application under the PCT has entered the UK
national phase and an international search report was issued during the
international phase, the applicant may amend the specification of their
own volition from the date of national phase entry until the date the
first examination report is issued. Where no international search report
has been issued by the time the application enters the UK national
phase, the period within which the applicant may amend voluntarily
starts from the date of issue of the UK search report or the
international search report (whichever is issued first).

### 19.16 {#section-4}

(r.31 is also relevant)

After the issue of the first substantive examination report the
applicant may, in addition to amending to meet any objections raised in
that report, amend the specification once for reasons of their own. If
the first report is made under s.18(3), the voluntary amendments must be
filed at the same time as the applicant replies to that report except
that if the first substantive examination report is issued before the
preparations for publication of the application have been completed, the
applicant may also amend the specification of their own volition before
they replies to the report. Any amended or new claims subsisting
immediately before preparations for publication have been completed are
included in the published application [see
16.16-16.19](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-16).
On the other hand if the first report is made under s.18(4) any
amendments must be filed within two months of the issue of the report
([see 19.18 below](#ref19-18)).

### 19.17 {#ref19-17}

The opportunity to amend the specification as of right following a first
report under s.18(3) is therefore expended when any considered reply is
made, whether consisting of amendments to meet the substantive
examiner's objections, the setting out of proposals to meet those
objections, or the submission of arguments intended to rebut them, and
regardless of whether the reply is complete, or whether it is made
before the end of the period specified in the report. Thus, for example,
if a reply is made proposing amendments to the claims but deferring
consequential amendments, this terminates the period for voluntary
amendments as of right under r.31(4), and any further amendments not
required by the substantive examiner must be made with the consent of
the comptroller under r.31(5)(a), [see 19.20](#ref19-20). However a mere
request or a query in response to the report under s.18(3) does not have
this effect, and the option to file voluntary amendments remains open
until a considered reply is made within the period specified.

### 19.18 {#ref19-18}

If the first examination report is issued under s.18(4) and allowable
amendments are filed before the expiry of the two month period the
amendments should be acknowledged and the application should be sent for
grant in the timescale previously indicated ([see
18.85-18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-85)).
If however, amendments are filed which are not allowable, so that a
report is then issued under s.18(3), this report counts as a second
report, and any subsequent voluntary amendments must be made with the
consent of the comptroller under r.31(5)(a), [see 19.20](#ref19-20). On
the other hand if a first report under s.18(4) is rescinded under r.107
[see
18.89-18.90](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-89),
then the report under s.18(3) replacing it is regarded as the first
report and voluntary amendments as of right are allowed when reply is
made to it, even if voluntary amendments had already been made within
the period allowed by the original s.18(4) report.

\[Voluntary amendments should be processed within the normal timescales
for amendments made in response to an examination report.\]

\[If the examiner determines that amendments filed within the two months
period are allowable, they should send a PDAX message with the text
"ISSUE RELEVANT EL4 LETTER" to the EA GRANT mailbox, including as a
recipient the examiner assistant for their examining group. If the
examining group does not have an examiner assistant then the recipient
field should be left blank. On receiving this message, the EA should
then issue an EL4 or EL4F (if there are outstanding grant fees). If
there are no grant fees outstanding, the EL4 should be accompanied by a
GRANT checklist. The EA should set the application ready for grant and
send a "GRANT CHECKLIST" message to Formalities. If the response date in
the intention to grant letter has not yet expired, the EA should instead
use an EL4P checklist to accompany the EL4. The EA will subsequently set
the application ready for grant and send a "GRANT CHECKLIST" message to
Formalities once the date in the original intention to grant letter has
expired. An EL4F should be accompanied by an EL4P checklist.\]\
\

\[If the amendments have decreased the number of excess pages or excess
claims and consequently the applicant has overpaid, the EA should send a
PDAX message to Formalities instructing them to issue a refund of the
overpayment. The EA should at the same time continue to prepare the
application for grant. The EA should send a "GRANT CHECKLIST" message to
the relevant Formalities group, unless the date given in the original
intention to grant letter has not expired (see above). There is no need
to wait for the refund to be issued before sending the application to
grant.\]\
\

\[If the grant fee is not paid by the deadline in the EL4F the Examiner
Assistant should issue a reminder letter to the applicant. This letter
will remind the applicant of the need to pay the grant fee and inform
them that as they have missed the deadline a Form 52, with the
appropriate fee (if any), is now required for them to do so. The 2 month
response date provided in the EL34F letter is a prescribed period set by
rule 30A of the Patents Rules for paying the grant fee. This period can
be extended by 2 months 'as of right' by filing a Form 52 and the
appropriate fee (if any). Further extensions are possible but are
subject to the comptroller's discretion. If the fee remains unpaid and
no further extensions are available the application will be terminated
by formalities once the compliance date has expired. Once terminated the
applicant may apply for reinstatement in the usual way (see section
28).\]\
\

\[If the voluntary amendments are not allowable, an examination report
under s.18(3) should be issued. Once the objections raised in the
s.18(3) report have been addressed, the examiner should ask the EA to
issue a further intention to grant notification should be issued (an
EL34 or EL34F), see
[18.86.2](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent#ref18-62-2).\]

### 19.19 {#ref19-19}

The comptroller has no power to refuse amendments submitted under
r.31(3) or (4) to be made. However, any amendment which does not comply
with s.76(2) or which gives rise to any other objection under the Act or
Rules should not be allowed to stand. The amended specification should
be considered by the examiner as described in
[18.64-18.70](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-64).
The amendments take effect on filing; hence if no response is received
within the specified period to an objection to such an amendment it is
not permissible to send the unamended application forward for grant.

\[ RC9 may be used to object to added matter in a specification amended
under r.31(3) or (4). \]

### Amendment with the consent of the comptroller

Before issue of the search report

### 19.19.1 {#ref19-19-1}

(s.117(1) is also relevant)

Under rule 31, amendment of the specification before the examiner's
search report under section 17(5) is issued is only allowable if the
comptroller requires or consents to that amendment. Otherwise the
specification will remain in its unamended state. The search examiner
should normally consider the allowability of any amendments proposed
before the search is done, having regard to ss.14(3), 14(5) and 76 [see
17.35](/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-35).
Amendments made to the specification other than corrections under
s.117(1) or to the claims will not be published in the A specification
[see
117.13](/guidance/manual-of-patent-practice-mopp/section-117-correction-of-errors-in-patents-and-applications/#ref117-13).
They will however be incorporated into the specification and become open
to public inspection when the application is published [see
16.20](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-20).

After response to the first report under s.18

### 19.20 {#ref19-20}

(r.31(5), r.31(6) is also relevant)

Once the opportunities for amendment as of right [see
19.15-19.16](#ref19-15) have passed, the applicant may make amendments
which are not in response to an Official objection only with the consent
of the comptroller and provided that they are received before issue of
the letter informing the applicant that a patent has been granted see
[18.86](/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86)
and [19.02](#ref19-02). The application to amend should be made in
writing, giving a reason for the amendment, unless the applicant can
satisfy the substantive examiner that the amendment is calculated to
meet an objection that the examiner should have raised. The
comptroller's consent is not normally withheld if the amendments appear
to be allowable.

### 19.21 {#ref19-21}

Until consent is given to the amendments the specification remains in
its unamended state. Consent is not granted until the amendments are in
an allowable state. The applicant should not be left uncertain whether
the amendments have been received and considered. Accordingly, if the
amendments were filed after a reply to a report under s.18(3) and no
further report under s.18(3) is to issue, and there has been no
communication with the agent or applicant concerning the amendments, the
agent or applicant should be notified of their allowance. On the other
hand, if the specification without the proposed amendments is in order
and the amendments give rise to objection a report to this effect should
be made, and two months be specified for reply. If no response is made
within this time the (unamended) application may be sent for grant.

\[ Allowable amendments filed after a reply to the final report under
s.18(3) should be acknowledged as in paragraph [19.18](#ref19-18). On
the other hand if, while the amendments are allowable, a further report
under s.18(3) is to issue, RC8 should be added to the report. RC9A may
be used to object to additional matter in the proposed amendments when
the application is not otherwise in order, and EL9 may be used where the
unamended application is in order (RC9 should not be used in either of
these circumstances). Where a report solely dealing with objections to
proposed amendments under r.31(5)(a) is to be sent, it should be headed
"Patents Act 1977: Report on amendments" (and not "Patents Act 1977:
Examination report under section 18(3)") and an appropriate period for
reply should be specified. \]

\[The EL9 should have a reply date of 2 months. The examiner should
reconsider any further amendments in response to the EL9 within 3 weeks
of receiving them, this is to ensure the minimum possible delay to
grant. If the applicant responds to the EL9, and the application is
resultantly found to be in order for grant, the examiner should follow
the procedure in [19.22.1](#ref19-22-1) so that the application is sent
on for grant by the examiner assistants. The examiner should also minute
the file for the attention of the examiner assistants stating that the
amendments are now allowable (sending a "PSM" message to the EA GRANT
mailbox). If a response to the EL9 is received and the proposed
amendments are still not allowable, then the examiner should use their
judgement whether to send the application to grant in the unamended
state (following the same procedure as when there is no response -- see
below) or whether they issue a further EL9. If no response is received
within the time period, formalities should be informed to remove the
proposed voluntary amendment from the working copy of the specification
and examiner assistants will prepare for the application to be sent to
grant in its unamended state, checking that any excess fees have been
paid. If any required fees have been paid, the examiner assistants will
then message formalities to perform the grant check. If any fees have
not been paid, the examiner assistant should issue the standard reminder
letter (with a deadline of two months from the original EL9 deadline.\]

\[Where fees are filed in response to an EL9 letter, but no substantive
response has been made, formalities should check the correct amount has
been paid and close the application down. Unless the applicant has
requested early grant Formalities do not need to send a "FORM 34 PAID"
message to the EA in this instance as the application will appear on
their EL9 list after 10 weeks. Once the application appears on the EA's
list they must check whether any grant fee remains outstanding. If no
grant fee is due they should prepare the application to be sent to grant
in its un-amended state. A "GRANT CHECKLIST" message should then be sent
to the relevant formalities group who will carry out the final grant
check.\]

\[If grant fees remain outstanding, the EA should issue a reminder
letter to the applicant. This letter will remind the applicant of the
need to pay the grant fee and inform them that as they have missed the
deadline a Form 52, with the appropriate fee (if any), is now required
for them to do so. The 2 month response date provided in the EL9 letter
is a prescribed period set by rule 30A of the Patents Rules for paying
the grant fee. This period can be extended by 2 months 'as of right' by
filing a

Form 52 with the appropriate fee (if any). Further extensions are
possible but are subject to the comptroller's discretion. If the fee
remains unpaid and no further extensions are available the application
will be terminated by formalities once the compliance date has expired.
Once terminated the applicant may apply for reinstatement in the usual
way (see section 28).\]

### 19.22 {#ref19-22}

If, however, a request to amend is received after the end of the
compliance period (at which time the application must have been reported
as complying with the Act and Rules, otherwise it would have been
treated as having been refused) the amendment should be allowed only if
it does not necessitate substantial re-examination or further search and
would not unduly delay grant; a request to restrict the claims to avoid
late-found prior art should, if effective for this purpose, be allowed.

### 19.22.1 {#ref19-22-1}

If the application was found in order for grant at a second or later
examination, then amendments can only be made with the consent of the
comptroller. The intention to grant letter sets out that such voluntary
amendments must be made before the date given. However, amendments
received after this date should still be considered so long as they are
received before grant. In some cases the date in the intention to grant
letter may be later than the compliance date. This is not a problem
because it is possible to make voluntary amendments after the compliance
date, [see 19.22](#ref19-22). The amendments must be accompanied by
reasons, and their allowance is at the comptroller's discretion.\
\

\[Voluntary amendments should be processed within the normal timescales
for amendments made in response to an examination report.\]

\[If the substantive examiner finds the voluntary amendments are not
allowable then they should issue EL9 and diary the case to return as set
out above in 19.21. In this situation, the examiner should take care to
ensure that the CHECKP checklist that is created with the EL9 bundle
instructs the ESO to change the OPTICS processing status to 10: Disposed
of - Not In Order\].

\[If, at any time, the substantive examiner finds the voluntary
amendments are allowable they should minute the file to state the
application is in order for grant and send a PDAX message with the text
"ISSUE RELEVANT EL4 LETTER" to the EA GRANT mailbox, including as a
recipient the examiner assistant for their examining group. If the
examining group does not have an examiner assistant then the recipient
field should be left blank. On receiving this message, the EA should
then issue an EL4 or EL4F (if there are outstanding grant fees). If
there are no grant fees outstanding, the EL4 should be accompanied by a
GRANT checklist. The EA should set the application ready for grant and
send a "GRANT CHECKLIST" message to Formalities. If the date in the
intention to grant letter has not yet expired, the EA should instead use
an EL4P checklist to accompany the EL4. An EL4F should be accompanied by
an EL4P checklist.\]\
\

\[If the amendments have decreased the number of excess pages or excess
claims and consequently the applicant has overpaid, the EA should send a
PDAX message to formalities instructing them to issue a refund of the
overpayment. The EA should at the same time continue to prepare the
application for grant. The EA should send a "GRANT CHECKLIST" message to
the relevant formalities group unless the date given in the original
intention to grant letter has not expired (see above). There is no need
to wait for the refund to be issued before sending the application to
grant.\]\
\

\[If the grant fee is not paid by the deadline in the EL4F the Examiner
Assistant should issue a reminder letter to the applicant. This letter
will remind the applicant of the need to pay the grant fee and inform
them that as they have missed the deadline a Form 52, with the
appropriate fee (if any), is now required for them to do so. The 2 month
response date provided in the EL34F letter is a prescribed period set by
rule 30A of the Patents Rules for paying the grant fee. This period can
be extended by 2 months 'as of right' by filing a Form 52 with the
appropriate fee (if any). Further extensions are possible but are
subject to the comptroller's discretion. If the fee remains unpaid and
no further extensions are available the application will be terminated
by formalities once the compliance date has expired. Once terminated the
applicant may apply for reinstatement in the usual way (see section
28).\]

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 19(2)**
  The comptroller may, without an application being made to him for the purpose, amend the specification and abstract contained in an application for a patent so as to acknowledge a registered trade mark.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Registered trade marks

(The allowability and acknowledgement of trade marks in specifications
is discussed in paragraphs
[14.97-14.101](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-97),
[14.137](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-137)).

### 19.23 {#ref19-23}

Under the Trade Marks Act 1994, registered trade mark includes service
mark.

### 19.24 {#ref19-24}

While in general no attempt should be made at the search stage to check
for trade marks, if the search examiner comes across a term in the
specification or abstract which they know, or can readily establish, to
be a registered trade mark they should amend the specification or
abstract to acknowledge the mark and inform the applicant that they have
done so. S.19(2) does not extend to authorising the comptroller to amend
the title on Form 1 to acknowledge a registered trade mark. That title,
complete with any unacknowledged trade mark, is published in the Patents
Journal [see
14.04.10](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-04-10).
The abstract title [see 14.173-
14.174](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-173)
should not normally contain a trade mark, but if this is unavoidable
then the trade mark must be acknowledged.

\[The search examiner should insert "(RTM)" after the trade mark using
the "Enhance" function in PDAX or add an "RTM" document to the bundle in
PROSE. Where the "RTM" document is used in PROSE, the examiner will need
to edit it to include the identified trade mark(s). In either case the
search examiner will need to add a minute to the dossier. However, there
is no need to make this insertion if the symbol ® is already present.
SC2 should be added to SL1 or SL2 before the paragraph on publication.
\]

\[ If any RTMs are added or removed after A-publication a second RTM
document will be needed before grant. Formalities will automatically
only include the most recent RTM document in the B-publication. If this
is not to be the case then formalities should be informed by a minute.
Any RTM document on the dossier which is not to be included in the
B-publication should be appropriately annotated.\]

### 19.25 {#ref19-25}

At substantive examination (when it is not performed at the same time as
the search), if the examiner suspects that an unacknowledged term may be
a registered trade mark, they should check whether or not it is [see
14.100](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-100)
for the procedure). If the use of the trade mark is not objectionable
[see
14.137](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-137)
and the application is otherwise in order the substantive examiner
should amend the specification to acknowledge the mark and inform the
applicant, in the report under s.18(4), that this has been done. If the
application is not in order, the examiner should ask the applicant, in
the report under s.18(3), to acknowledge the trade mark. If the
applicant subsequently refuses or omits to acknowledge the mark, the
substantive examiner should do so and inform the applicant.

\[If the application is otherwise in order, the examiner should insert
"(RTM)" as per [19.24](#ref19-24). The applicant should be informed by
referring to the acknowledged trade mark in EL3. If the applicant is to
be asked to acknowledge the trade mark, RC11 should be added to a
s.18(3) report. If the examiner needs to acknowledge a trade mark either
because the applicant refuses or omits to do so, the examiner should add
SC2 to the report to inform the applicant of the fact. \]

### 19.26 {#ref19-26}

For combined search and examination cases, the examiner should check at
first examination stage whether any unacknowledged terms are registered
trade marks [see 14.100 for the
procedure](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-100).
If the examiner comes across such an unacknowledged registered trade
mark in the specification or abstract, they should either add an "RTM"
document to the PROSE bundle or insert "(RTM)" after the trade mark
amend the specification or abstract to acknowledge the mark [see
19.24](#ref19-24). If the use of the trade mark is not objectionable
[see
14.137](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-137)
and the examiner has no other objections, they should inform the
applicant that the specification or abstract has been amended to
acknowledge the mark. If other objections are to be raised and the "RTM"
document has been used,the examiner should also ask the applicant, in
the report under s.18(3), to acknowledge the trade mark, as this
provides an opportunity for the applicant to acknowledge the mark
themselves in amended pages.

\[If there are no other objections, the examiner should insert "(RTM)"
as per [19.24](#ref19-24). The applicant should be informed by adding
SC2 to SE3 before the paragraph on publication. If there are other
objections and the applicant is to be asked to acknowledge the trade
mark, RC11 should also be added to the s.18(3) report.\]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
