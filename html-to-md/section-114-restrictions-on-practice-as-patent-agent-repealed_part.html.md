::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 114: Restrictions on practice as patent agent \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (114.01 - 114.02) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 114.01

This section imposed limitations on who could do business as a patent
agent. It also allowed patent agents to prepare certain instruments
without committing an offence.

### 114.02

Section 114, together with other provisions of the 1977 Act concerning
patent agents, has been repealed by the CDP Act (section 303(2) and
schedule 8). Those provisions have been replaced by the new regime
provided by Part V of the CDP Act (Patent Agents and Trade Mark Agents),
[see
274.01](/guidance/manual-of-patent-practice-mopp/section-274-persons-permitted-to-carry-on-business-of-a-patent-agent)
et al.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
