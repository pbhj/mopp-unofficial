::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 81: Conversion of European patent applications {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (81.01 - 81.19) last updated October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 81.01

a.135 EPC is also relevant.

This section provides for the conversion of applications for European
patents (UK) into applications under the Patents Act 1977, at the
discretion of the comptroller in response to a request by the applicant
and includes the circumstances in which conversion may be allowed and
the effects of conversion.

\[ For applications resulting from EPC s.81 conversions the order of
documents on a file is similar to that for PCT s.89 conversions ([see
89.01](/guidance/manual-of-patent-practice-mopp/section-89-effect-of-international-application-for-patent/#ref89-01)).
\]

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 81(1)**
  The comptroller may direct that on compliance with the relevant conditions mentioned in subsection (2) below an application for a European patent (UK) shall be treated as an application for a patent under this Act where the application is deemed to be withdrawn under the provisions of the European Patent Convention relating to the time for forwarding applications to the European Patent Office.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 81(2)**

  The relevant conditions referred to above are\
  \
  \[(a) repealed\]\
  \
  (b) that -\
  \
  (i) the applicant requests the comptroller within the relevant
  prescribed period (where the application was filed with the Patent
  Office) to give a direction under this section, or\
  \
  (ii) the central industrial property office of a country which is party
  to the convention, other than the United Kingdom, with which the
  application was filed transmits within the relevant prescribed period a
  request that the application should be converted into an application
  under this Act, together with a copy of the application; and\
  \
  (c) that the applicant within the relevant prescribed period pays the
  application fee and if the application is in a language other than
  English, files a translation into English of the application and of any
  amendments previously made in accordance with the convention.
  -----------------------------------------------------------------------

### Circumstances for convention

### 81.02

Deleted

### Failure to transmit application to EPO

### 81.03 {#ref81-03}

a.77(3) is also relevant

Sections 81(1) and 81(2)(b) provide for the situation where a European
EPC patent application is deemed to be withdrawn because it has not been
received by the EPO within a period of fourteen months from its date of
filing or, if priority is claimed, the declared priority date. The
filing of the application may have been made at the UK Patent Office,
see [81.05](#ref81-05), or at the central industrial property office of
another country which is party to the EPC, see [81.06](#ref81-06).

### 81.04 {#section-2}

a.77(4) EPC S.22(3) are also relevant

Failure to transmit a European application to the EPO arises as a result
of a EPC decision at a national office to make the subject of the
application secret, eg prohibition s.22(3)(b) directions under s.22 in
the case of the UK. It could also arise accidentally.

### 81.05 {#ref81-05}

r.58(1) r.58(3) PR Sch 4, Part 1 r.58(4) PR Sch 4, Parts 2 & 3 are also
relevant

Where the application was filed with the UK Patent Office, a request for
r.58(3) conversion should be made in writing by the applicant with the
prescribed fee (if any), within PR Sch 4, three months (which cannot be
extended) from the date on which the applicant is notified by Part 1 the
EPO that their application for a European patent (UK) has been deemed to
be withdrawn. r.58(4) This notification should accompany the request.
Within two months of the date of filing of PR Sch 4, that request, the
applicant should file the application fee, an English language
translation of Parts 2 & 3 the application, where necessary, Patents
Form 9A with fee requesting search, and Patents Form 7 if appropriate
([see 81.11](#ref81-11)). This period may be extended in periods of two
months only in accordance with r.108(2) or (3) and (4) to (7) ([see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34)).
According to r.108(7), no extension may be granted more than two months
after the expiry of the period as prescribed (or previously extended).
If the application fee is paid at the time of requesting conversion, no
surcharge will be payable. However if the application fee is paid at a
later date, a surcharge will be due.

### 81.05.1 {#section-3}

r.58(2) is also relevant

At the same time as making a request for conversion referred to in
[81.05](#ref81-05), the applicant may also request that a copy of the
European patent application, together with a copy of the request, be
sent by the comptroller in accordance with the relevant provisions of
the EPC to the central industrial property office of any specified
Contracting State designated in the application.

### 81.06 {#ref81-06}

r.59 pr Sch 4 are also relevant

Where the application was filed with another national office, a request
for PR Sch 4 conversion should be filed by the applicant at that office
for transmission, together with a copy of the application, to the UK
Patent Office within twenty months (which cannot be extended) from the
declared priority date or, if no priority was claimed, the filing date
of the application for a European patent (UK). The Office sends a
notification of receipt of the request to the applicant who should,
within four months of the date of the notification, file the application
fee, an English language translation of the application, where
necessary, Patents Form 9A with fee requesting search and Patents Form 7
if appropriate see [81.11](#ref81-11). The four month period may be
extended in periods of two months only in accordance with r.108(2) or
(3) and (4) to (7) ([see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34)).
According to r.108(7), no extension may be granted more than two months
after the expiry of the period as prescribed (or previously extended).
If the application fee is paid at the time of requesting conversion, no
surcharge will be payable. However if the application fee is paid at a
later date, a surcharge will be due.

  -----------------------------------------------------------------------
   

  **Section 81(3)(a)**

  Where an application for a European patent falls to be treated as an
  application for a patent under this Act by virtue of a direction under
  this section\
  \
  ( a) the date which is the date of filing the application under the
  European Patent Convention shall be treated as its date of filing for
  the purposes of this Act, but if that date is re-dated under the
  convention to a later date, that later date shall be treated for those
  purposes as the date of filing the application;
  -----------------------------------------------------------------------

### Effects of conversion

### 81.07 {#ref81-07}

Where conversion is allowed, the application for a European patent (UK)
is treated as an application under the Act. The filing date of the
application under EPC Article 80 and EPC rule 40 is treated as its
filing date under the Act, unless it is re-dated under the EPC to a
later date in which case that later date is so treated.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 81(3)(b)**
  if the application satisfies a requirement of the convention corresponding to any of the requirements of this Act or rules designated as formal requirements, it shall be treated as satisfying that formal requirement;
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 81.08 {#ref81-08}

r.25 and r.103 are also relevant

Those requirements of Patents Rules 12(1) and 14(1), (2) and (3) which
are designated as formal requirements ([see
15A.04](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-04))
are treated as having been complied with provided that the corresponding
provisions of the EPC Implementing Regulations have been fulfilled ([see
15A.10](/guidance/manual-of-patent-practice-mopp/section-15a-preliminary-examination/#ref15A-10)).
However, amendments filed after conversion are subject to the Patents
Rules in the normal way. An address for service within the United
Kingdom (including the Isle of Man), Gibraltar or the Channel Islands is
also required in the same way as for any other application under the Act
([see
14.04.13](/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-04-13)).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 81(3)(c)**
  Any document filed with the European Patent Office under any provision of the convention corresponding to any of the following provisions of this Act, that is to say, sections 2(4)(c), 5, 13(2) and 14, or any rule made for the purposes of any of those provisions shall be treated as filed with the Patent Office under that provision or rule; and
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 81.09 {#section-4}

Where the invention to which the application relates was disclosed at an
international exhibition within the six months preceding the filing of
the European patent application, the statement and certificate filed
under EPC Article 55 and EPC rule 25 are treated as filed under
s.2(4)(c) and r.5, [see
2.40](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-40).

### 81.10 {#section-5}

r.3 and r.8 are also relevant

The date of filing of the earliest previous application mentioned in the
declaration of priority filed by the applicant in respect of the
application for a European patent (UK) under EPC Article 88(1) and EPC
rule 53 (where the priority date claimed in the declaration has not been
lost or abandoned and where the declaration has not been withdrawn
before conversion is allowed) is treated as the declared priority date
of the converted application. The declaration of priority and the file
number and certified copy of the or each previous application mentioned
therein, filed under EPC Article 88(1) and EPC rules 52 and 53, are
treated as filed under s.5 and r.8(1) and (2).

### 81.11 {#ref81-11}

rr. 58(4) and 59 (3) are also relevant

A statement of inventorship filed under EPC Article 81 and EPC rule 19
is treated as filed under s.13(2), [see
13.12](/guidance/manual-of-patent-practice-mopp/section-13-mention-of-inventor/#ref13-12).
(Otherwise, Form 7 should be filed within the same period as that
specified for filing Form 9A in [81.05](#ref81-05) or [81.06](#ref81-06)
as appropriate.)

### 81.12 {#section-6}

The description of the invention, the claim(s), any drawings and the
abstract filed under EPC Article 78 are treated as filed under s.14.

### 81.13 {#ref81-13}

PR sch 1, para 3(4) are also relevant

If the invention involves the use of or concerns biological material,
information regarding the biological material and its deposit in a
culture collection filed under EPC Article 83 and EPC rules 31-33 is
treated as filed under the Act. The relevant disclosure is regarded as
clear enough and complete enough if EPC rule 31 has been complied with,
[see
125A.07](/guidance/manual-of-patent-practice-mopp/section-125a-disclosure-of-invention-by-specification-availability-of-samples-of-biological-material/#ref125A-07).

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 81(3)(d)**
  the comptroller shall refer the application for only so much of the examination and search required by sections 15A, 17 and 18 above as he considers appropriate in view of any examination and search carried out under the convention, and those sections shall apply with any necessary modifications accordingly.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Examination and search

### 81.14 {#section-7}

Search should be requested by the filing of Patents Form 9A as mentioned
in [81.05](#ref81-05) and [81.06](#ref81-06).

### 81.15 {#section-8}

Preliminary examination under s.15A is modified in that many
requirements are regarded as met if those of corresponding EPC
provisions have been fulfilled, see [81.07](#ref81-07) to
[81.13](#ref81-13).

### 81.16 {#section-9}

r.31(3) & (5) are also relevant

No search will have been carried out under the EPC. A search under s.17
is (5) therefore conducted in the normal way on the basis of the
documents filed under the EPC and treated as filed under the Act but
incorporating with the comptroller's consent any amendments filed since
conversion.

### 81.17 {#ref81-17}

r.60 PR Sch 4, Parts 2 & 3 r.28(1) are also relevant

A request for substantive examination of the converted application
should PR Sch 4, be made on Patents Form 10 with fee, within two years
(extensible under r.108 (2) or (3) and Parts 2 & 3 (4) to (7), however
r.108(7) states that no extension may be granted after two months
r.28(1) following the expiry of the period as prescribed or previously
extended - [see
123.36.10-12](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-36-10))
from the declared priority date, if any, or the date of filing of the
application for a European patent (UK). Substantive examination under
s.18 follows in due course.

### 81.18 {#section-10}

\[deleted\]

### 81.19 {#ref81-19}

r.30 PR Sch 4A, Parts 2 & 3 are also relevant

The periods prescribed (including permissible extensions thereto) for
the PR Sch 4A, purposes of ss.18(4) and 20(1) as set out in
[20.02](/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02)
also apply to a converted application Parts 2 & 3 except that any
reference to the date of filing is taken to refer to the date of filing
of the application for the European patent (UK). These periods may be
extended in tranches of two months in accordance with r.108(2) or (3)
and (4) to (7) ([see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34)).
According to r.108(7), no extension may be granted more than two months
after the expiry of the period as prescribed (or previously extended).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
