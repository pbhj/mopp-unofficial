::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 126: Stamp duty \[repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (126.01 126.02) last updated: April 2009.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 126

### 126.01

This section was neither concerned with applications made, or patents
granted, under the 1977 or 1949 Act, nor to European patents (UK). It
concerned the liability for stamp duty of instruments (e.g. assignments)
relating to Community patents or to applications for certain European
patents which were intended to mature into Community patents. However,
the section never had any effect because the Community Patent Convention
did not come into force prior to the section being repealed by s.156 of,
and Schedule 40 to, the Finance Act 2000.

[See
32.09](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-09)
for details of stamp duty requirements for instruments relating
exclusively to intellectual property or in part to intellectual property
and in part to other property.

### 126.02

\[deleted\]
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
