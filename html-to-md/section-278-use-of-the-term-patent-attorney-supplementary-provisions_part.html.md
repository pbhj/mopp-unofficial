::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 278: Use of the term \"patent attorney\": supplementary provisions {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (278.01 - 278.03) last updated: April 2007
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 278.01

This provides that certain uses of the term "patent attorney" do not
contravene certain provisions.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 278(1)**
  The term "patent attorney" may be used in reference to a solicitor, and a firm of solicitors may be described as a firm of "patent attorneys", without any contravention of section 276.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 278.02

Use of that term by solicitors is allowed without contravening [section
276](https://www.gov.uk/guidance/manual-of-patent-practice-mopp/section-276-persons-entitled-to-describe-themselves-as-patent-agents).

  -----------------------------------------------------------------------
   

  **Section 278(2)**

  No offence is committed under the enactments restricting the use of
  certain expressions in reference to persons not qualified to act as
  solicitors\
  (a) by the use of the term "patent attorney" in reference to a
  registered patent agent, or\
  (b) by the use of the term "European patent attorney" in reference to a
  person on the European list.
  -----------------------------------------------------------------------

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 278(3)**
  The enactments referred to in subsection (2) are section 21 of the Solicitors Act 1974, section 31 of the Solicitors (Scotland) Act 1980 and Article 22 of the Solicitors (Northern Ireland) Order 1976.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 278.03

Use of the term "patent attorney" by registered patent agents, and the
term "European patent attorney" by persons on the European list, is
allowed without committing any offence under the enactments listed in
subsection (3) concerning titles reserved for solicitors.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
