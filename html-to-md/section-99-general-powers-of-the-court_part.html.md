::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 99: General powers of the Court {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (99.01 - 99.02) last updated October 2022.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 99: General powers of the Court {#section-99-general-powers-of-the-court}

### Section 99

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 99**
  The court may, for the purpose of determining any question in the exercise of its original or appellate jurisdiction under this Act or any treaty or international convention to which the United Kingdom is a party, make any order or exercise any other power which the comptroller could have made or exercised for the purpose of determining that question.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 99.01 {#ref99-01}

Sch. 2, para 1 and 2 are also relevant

For the purpose of determining any question within its jurisdiction
under 1 and 2 the 1977 or 1949 Act or any relevant treaty etc, the court
may exercise any power which the comptroller could have exercised for
that purpose. This applies not only to actions initiated in the court
but also to appeals to the court from decisions of the comptroller, see
for example
[97.04](/guidance/manual-of-patent-practice-mopp/section-97-appeals-from-the-comptroller/#ref97-04).

### 99.02 {#ref99-02}

s.130(1) is also relevant

The "court" means (a) as respects England and Wales, the High Court (ie
normally the Patents Court) and any patents county court having
jurisdiction by virtue of ss.287-292 of the CDP Act; (b) as respects
Scotland, the Court of Session; (c) as respects Northern Ireland, the
High Court in Northern Ireland; and (d) as respects the Isle of Man, His
Majesty's High Court of Justice in the Isle of Man.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
