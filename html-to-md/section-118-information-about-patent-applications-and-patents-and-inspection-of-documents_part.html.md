::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 118: Information about patent applications and patents, and inspection of documents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (118.01 - 118.23) last updated: January 2023.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 118.01

This section concerns the availability to the public of information and
documents relating to patents and applications for patents under the
Act, including the effects of publication of an application under s.16
on such availability. It applies to international applications which
have been published under the PCT and entered the UK national phase (and
are therefore treated as published under s.16). Relevant procedures are
prescribed by rules 51 to 55.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 118(1)**
  After publication of an application for a patent in accordance with section 16 above the comptroller shall on a request being made to him in the prescribed manner and on payment of the prescribed fee (if any) give the person making the request such information, and permit him to inspect such documents, relating to the application or to any patent granted in pursuance of the application as may be specified in the request, subject, however, to any prescribed restrictions.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Information

### 118.02 {#ref118-02}

r.54 is also relevant

A person may request to be notified of each of the following events: -

in relation to an application for a patent, r.54(5) is also relevant\
\

\(a\) an applicant requesting, or failing to request, a substantive
examination before the end of the period prescribed for the purposes of
section 18(1);

\(b\) the application being published;

\(c\) the notice of grant of the patent being published under section
24;

\(d\) the application being terminated or withdrawn;

in relation to a patent, r.54(6) is also relevant

\(a\) a request for an opinion under section 74A;

\(b\) the patent ceasing to have effect by reason of section 25(3);

\(c\) the renewal fee and any additional fee being paid during the
period specified in section 25(4);

\(d\) an application being made for the restoration of the patent which
has ceased to have effect;

in relation to a patent or application for a patent, r.54(7) is also
relevant

\(a\) an entry being made in the register;

\(b\) a document becoming available for inspection under section 118 (by
reason of a prescribed restriction no longer applying to the document);

\(c\) an application to register a transaction, instrument or event
being made under rule 47;

\(d\) a matter being published in the journal.

### 118.03 {#section-1}

\[deleted\]

### 118.04 {#section-2}

r.54(1) and r.54(4) are also relevant

Requests for any of the information listed in [118.02](#ref118-02) are
often known as caveats. Any such request should be made on Patents Form
49 accompanied by the appropriate fee. A separate form and fee are
required in respect of each item of information requested.

\[ Such caveats are dealt with by the appropriate formalities group --
for further details see the [Patents Formalities
Manual](https://www.gov.uk/government/publications/patents-formalities-manual).

\[ When a Form 49 is received, it should be checked that the correct fee
has been paid. If an underpayment has occurred, the appropriate note
should be sent to the applicant advising them that the Form is
underpaid. When it is found that three months or more have elapsed since
the issue of the note, the applicant should be sent a reminder letter
advising them that unless the outstanding balance is paid within one
month the request under r.54 will be considered as abandoned.

\[ For all Forms 49 on which the correct fee has been paid, whether
initially or subsequently, details from the Form should be entered into
the Caveat Record Book which provides a source of statistical
information.

\[ If more than one of the items listed under r.54 is mentioned on the
Form 49 or it is not clear what information is required, confirmation
should be sought by telephone and the result registered on the Form
together with the name of the person consulted.

\[ The information given on Form 49 should be cross-checked with details
shown on the Register or held by the appropriate formalities group.

\[ Forms 49 on which the correct fee has been paid should be kept as
confidential documents with the cashiers until the caveat action has
been completed. \]

### 118.05 {#section-3}

If an application has been published under s.16, the Office is obliged
to give any properly-requested information listed in
[118.02](#ref118-02) relating to that application or any patent granted
thereon; if it has not been so published, the provisions of s.118(2) to
(5) determine the response to a request [(see 118.16-23)](#ref118-16).
Information relating to a published application or resultant patent is
in many cases not immediately available because the event (eg filing of
Patents Form 10) to which the request relates has not yet occurred; in
such cases the request is retained until the event occurs and the
information is then given.

\[ When the requested information can be provided, it should be sent to
the applicant with the standard letter. If the request relates to a
paper case then Form 49 should be removed from the File and, with the
duplicate of the letter stapled thereto, placed in numerical order on
the Dead File for the year. Dead Files should be destroyed after five
years. If the application is a PDAX dossier a copy of the form will be
archived. \]

### Inspection of documents

### 118.06 {#section-4}

r.51 is also relevant

Subject to certain restrictions (see 118.07), after the date of
publication of an application under s.16, all documents filed at or kept
in the Office in relation to the application or any patent granted
thereon can be inspected at the Office on request and payment of the
prescribed fee. Documents filed at or kept in the Office in relation to
European patents (UK) are similarly open to inspection. In addition,
certain documents are available free of charge on the Office website
through [Ipsum](https://www.ipo.gov.uk/p-ipsum.htm){rel="external"}, the
Office's online patent information and document inspection service.
(With regard to applications containing computer programs or biological
sequence listings, [see
16.27](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-27)).
It should be noted that documents filed at or kept in the Office in
relation to applications and patents are generally not kept
indefinitely. Files of applications under the Patents Act 1977, either
published or unpublished, which have been irrevocably abandoned,
withdrawn, deemed to be withdrawn, refused, voided or otherwise
terminated before grant are kept for at least seven years from
termination. Files of patents (including European patents with UK
designation) which have irrevocably ceased to be effective (due, e.g. to
expiry of term, revocation, surrender, failure to renew) are kept for at
least seven years from the date on which the patent ceased.

### 118.07 {#ref118-07}

r.51(2), (4) is also relevant

No document (or part of a document) may be inspected---

\(a\) where that document was prepared by the comptroller, an examiner
or the Office for internal use only;

\(b\) where the circumstances specified in section 118(4) exist, before
the end of the period of 14 days beginning immediately after the date of
the notification under rule 52(2);

\(c\) where that document is a request or application made under section
118 or rule 46(2), 48(2) or 54(1); or

\(d\) where that document includes matter---

\(i\) which in the comptroller's opinion disparages any person in a way
likely to damage him, or

\(ii\) the inspection of which would in his opinion be generally
expected to encourage offensive, immoral or anti-social behaviour.

The power mentioned in (d) (i) and (ii) above extends to all documents
on file for the patent or application for a patent. Section 16(2)
provides this power in respect of the published application. The
procedure in 16.34-37 should be applied mutatis mutandis.

\[ Where an examiner identifies matter as detailed in (d)(i) or (ii)
above after publication they should immediately annotate the document
NOPIE and 'save' the dossier. This will ensure that the document is not
available on Ipsum while the procedure detailed in
[16.34](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-34)
is carried out.\]

r.51(3), (4), PCT a.38(1) is also relevant

Furthermore, no document (or part of a document) may be inspected if it
falls into the following categories, unless in a particular case the
comptroller otherwise directs

\(a\) where that document was filed at the Office in connection with an
application under section 40(1) or (2) or 41 (8);

\(b\) where that document is treated as a confidential document under
rule 53;

\(c\) where---

\(i\) that document was prepared by the comptroller, an examiner or the
Office other than for internal use, and

\(ii\) it contains information which the comptroller considers should
remain confidential;

\(d\) where that document relates to an international application for a
patent and

the International Bureau would not be permitted to allow access to that
document under the Patent Co-operation Treaty; or

\(e\) where---

\(i\) the comptroller has accepted a person's application under rule
11(1)(a) or (b), and

\(ii\) that person's name and address can be identified from that
document as those of the inventor or of the person believed to be the
inventor (or, as the case may be, his address can be so identified).

Documents open to inspection through Ipsum may be redacted so that
certain personal or sensitive information cannot be viewed on-line ([see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29)),
albeit that they will still be open for inspection in person, by post or
email. This is in addition to any redaction of documents for the reasons
given above. An applicant or third party may request that a document or
entire file relating to an application or patent which is open to
inspection via Ipsum be removed from the system because of the personal
or sensitive information contained within it. If this is requested, the
document or file will be taken off Ipsum as soon as possible, and the
Office will then consider the request and decide what action should be
taken in respect of online availability of the document or file.

\[To remove an entire file from Ipsum, raise a call with the IT
Helpdesk. To remove an individual document from Ipsum, annotate it as
"NOPIE" and then 'save' the dossier. In both cases, a minute should be
added to the file explaining when and why the document or dossier was
taken down and any issues that should be considered before it is
returned to Ipsum. When any document or file is removed from Ipsum using
these procedures the details should be recorded in the NOPIE
spreadsheet. This will permit the reinstatement of the NOPIE marking in
the event of a failure of PDAX and also allows the reasons for making
material NOPIE to be monitored and reviewed.\].

### 118.07.1 {#section-5}

Inspection of the file relating to s.12 proceedings on a PCT
application, which had not entered the UK national phase under s.89 but
had entered the regional phase as a Euro-PCT application designating UK,
was allowed by analogy with the provisions both of the EPC permitting
public inspection of the file of the published application, and of s.118
permitting public inspection of any file relating to a patent
application under the Act. [118.07.2](#ref118-07-2) Inspection of
documents filed before 1 June 1978 is permitted under s.118 at the
Comptroller's discretion. The exercise of this discretion beyond what
would have occurred under the 1949 Act should occur only under
exceptional circumstances, having considered the rights and expectations
of the patentee as well as the desires of the individual making the
request for inspection. For example, reports from the examiner and the
specification as filed were not open to public inspection under the 1949
Act. It should be noted that where a copy of a specification as filed
has been used to support a claim for priority for a subsequent
application, and the subsequent application was published, this
specification as filed would have been published as part of the later
application. Documents filed after 1 June 1978 for applications under
the 1949 Act are available for inspection under s.118 by virtue of
paragraphs 1 and 2(g) of Schedule 2 ([see
127.06-07](/guidance/manual-of-patent-practice-mopp/section-127-existing-patents-and-applications/#ref127-06)).

\[ Advice should be sought from PD/CL before allowing inspection of
documents filed before 1 June 1978 over and above what was allowed under
the 1949 Act following the procedures set out in Chapter 79 of the 1975
edition of the Manual of Office Practice. Where such a question arises,
PD/CL should also liaise with Security Section if the application was at
any time subjected to security restrictions\].

### 118.07.2 {#ref118-07-2}

Inspection of documents filed before 1 June 1978 is permitted under
s.118 at the Comptroller's discretion. The exercise of this discretion
beyond what would have occurred under the 1949 Act should occur only
under exceptional circumstances, having considered the rights and
expectations of the patentee as well as the desires of the individual
making the request for inspection. For example, reports from the
examiner and the specification as filed were not open to public
inspection under the 1949 Act. It should be noted that where a copy of a
specification as filed has been used to support a claim for priority for
a subsequent application, and the subsequent application was published,
this specification as filed would have been published as part of the
later application. Documents filed after 1 June 1978 for applications
under the 1949 Act are available for inspection under s.118 by virtue of
paragraphs 1 and 2(g) of Schedule 2 (see 127.06-07).

\[ Advice should be sought from the Registered Rights Legal Team before
allowing inspection of documents filed before 1 June 1978 over and above
what was allowed under the 1949 Act following the procedures set out in
Chapter 79 of the 1975 edition of the Manual of Office Practice. Where
such a question arises, the Registered Rights Legal Team should also
liaise with Security Section if the application was at any time
subjected to security restrictions\].

### Copies of documents and of entries in Register

### 118.08 {#ref118-08}

r.46(1)-(2), r.48(1)-(4) and r.44(1) are also relevant

Copies of or extracts from the Register (including entries relating to
European patents (UK) and applications therefor) or any document
referred to in rule 48(4) are supplied upon request made on Patents Form
23. An entry in the Register in respect of a particular application is
not made until the application has been published under s.16 (or under
EPC Article 93 in the case of an application for a European patent
(UK)). Section 32(11)(b) refers to any document kept in the Office or an
extract therefrom, any specification of a patent or any application for
a patent which has been published. However, a person is not supplied
with a copy of a document not open to public inspection unless they are
entitled to a copy, eg in the case of an unpublished application they
are the applicant or the applicant's agent. Furthermore, a person is not
supplied with a copy of a document where making or providing such a copy
would infringe copyright. Where a copy cannot be supplied, the person is
so informed, normally by telephone. For certified copies, see
[32.19-20](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-19).
The restrictions in 118.07 also apply to the supply of copies or
extracts, whether certified or not.

### 118.09 {#section-6}

A single Form 23 may be used to request uncertified copies relating to
more than one patent or patent application. On the other hand, when
requesting certified copies, a separate Form 23 should be used for each
patent or patent application. Certified and uncertified copies should
not be requested on the same Form 23.

### 118.09.1 {#section-7}

An electronic uncertified copy of documents from a published UK patent
or patent application can be requested using the [online Form 23
service](https://www.gov.uk/get-uncertified-electronic-copy-patent).
However, it is not possible to use the online service to request a paper
uncertified copy, and it is not possible to request an electronic
uncertified copy by filing a paper Form 23.

### 118.09.2 {#section-8}

Electronic uncertified copies of certain documents are available free of
charge on the Office website through
[Ipsum](https://www.ipo.gov.uk/p-ipsum.htm){rel="external"}.

### Confidential documents

### 118.10 {#ref118-10}

r.53 and 118.10 are also relevant

The comptroller has discretion to direct that a document (or part of a
r.108(1) document) filed at the Office or sent to an examiner or the
comptroller be treated as a confidential document, when so requested by
any person. However the comptroller must refuse any such requests in
relation to a Patents Form or a document filed in connection with a
request for an opinion under section 74A. Documents filed on or after 1
April 2007 will be open to public inspection upon filing, and a request
for confidentiality can be made at the time of filing or within fourteen
days of the filing or sending of the document (extensible at the
discretion of the comptroller) and reasons must be given. If only part
of a document contains confidential matter, for example financial
figures, then confidentiality will not be accorded to the whole
document. The person seeking confidentiality may be asked to identify
the confidential matter in a document. The document, with the
confidential matter removed, will be open to public inspection.

### 118.11 {#section-9}

\[deleted\]

### 118.12 {#section-10}

r.53(4) is also relevant

If a request for confidentiality is made, the document in question is
not open to public inspection while the matter is being determined.

\[ While the matter is being determined and if s.16 publication has
occurred, the document should be imported into the dossier and
appropriately annotated and given the document code confidential. If the
request relates to a paper case, the document should be kept in an
envelope of A4 size, prominently marked not open to and placed adjacent
to the proceedings sheets. The appropriate formalities group normally
attends to this when the document is filed: if however a case examiner
should receive a file where this has not been done the examiner should
dot it themselves. Further details are given in paragraphs 12.39-12.50
of the [Formalities
Manual](https://www.gov.uk/government/publications/patents-formalities-manual).

### 118.13 {#ref118-13}

Since the public are generally entitled to inspect documents relating to
a patent or, after publication of an application, to the application, a
request for confidentiality should not be granted unless it is
considered justified for the reasons given. It is a matter of judging
whether a party's reasons for desiring confidentiality outweigh the
generally overriding public interest for disclosure in each case.
Observations by an applicant in rebuttal to an official objection, for
example one of lack of novelty or inventive step, communications
concerning disclosure of matter obtained unlawfully or in breach of
confidence ([see
2.39](/guidance/manual-of-patent-practice-mopp/section-2-novelty/#ref2-39)),
and observations which are to be taken into consideration under s.21
([see
21.08](/guidance/manual-of-patent-practice-mopp/section-21-observations-by-third-party-on-patentability/#ref21-08))
should not be treated as confidential. Subject to the above comments
correspondence which relates to procedural matters, for example to
reasons in support of a request for exercise of the Comptroller's
discretion, may be treated as confidential. It is likely to be
appropriate for information concerning individuals' health or personal
circumstances to be treated as confidential. In inter partes
proceedings, evidence filed by one party, for example details of licence
agreements, may be treated as confidential if its disclosure risks being
harmful (eg commercially) to the party or the party's associates to an
extent which overrides the requirement for public access. Regard should
be had in particular to the interests of a third party who has not
consented to a private agreement being open to public inspection.
Following the judgment of the Patents Court in [Diamond Shamrock
Technologies S.A.'s Patent \[1987\] RPC
91](http://rpc.oxfordjournals.org/content/104/5.toc){rel="external"} the
criteria to be applied in considering requests for confidentiality were
summarised as follows: -

\(a\) The fact that a document is said to contain "sensitive commercial
information" does not necessarily mean that this material, which would
otherwise become public property, is to be excluded from public
inspection; apart from generalities there must be some real indication
as to why disclosure would be harmful.

\(b\) Those requesting confidentiality should put in evidence, although
this is not necessarily required before the comptroller.

\(c\) Material which is going to form no part of the decision can remain
confidential.

\(d\) Material supplied by a third party on the basis of confidence,
which involves minimal excisions from a decision, should be maintained
as confidential unless there is some overwhelming public interest, which
makes it desirable that the public should have sight of it.

\(e\) The appropriate procedure is for the matter to be dealt with prior
to a substantive hearing so that if ruled against, the person proffering
the document can make up their mind whether they will go forward
publicly, or have the material withdrawn.

In Diamond Shamrock Technologies S.A.'s Patent, the Court allowed
confidentiality in the cases of one category of material (relating to
royalties agreed with licensees other than I.C.I.) none of which formed
any part of the hearing officer's decision and the disclosure of which
had no relevance to anything the public might have an interest in; and
another category of material (relating to information touching royalties
and prices supplied to the patentees by an associate company on a
confidential basis) where disclosure would quite plainly be against the
wishes of a third party. Confidentiality was refused for a further
category of material (relating to royalties agreed in connection with a
previous licence to I.C.I. by the patentees) where I.C.I. did not object
to disclosure and, apart from generalities, no real indication had been
given as to why disclosure would be harmful to the patentees. In
Neo-Inhalation Products Limited's Application
[BLO/154/13](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/154/13){rel="external"}
the hearing officer refused a request for confidentiality in respect of
a document filed as evidence to rebut an inventive step objection. The
request was made on the grounds that the document contained valuable and
commercially sensitive information, though no argument was advanced as
to why putting the document in the public domain would be harmful to the
applicant.

However, evidence treated as confidential must of course be accessible
to at least the advisers of the other party or parties. As decided by
[Upjohn L J in Re K (Infants) \[1963\] Ch
381](http://rpc.oxfordjournals.org/content/121/8/285.short){rel="external"}
and followed in [VNU Business Publications BV v Ziff Davis (UK) Limited
\[1992\] RPC
269](http://rpc.oxfordjournals.org/content/109/12/269.full.pdf+html){rel="external"},
any party to the proceedings has a right to see all the evidence before
the comptroller on which the other party relies. In suitable cases
access to confidential evidence may be restricted to a party's legal
representative and not given to of the party themselves. In re [Schering
A G's Patent \[1986\] RPC
30](http://rpc.oxfordjournals.org/content/103/2/30.full.pdf+html){rel="external"}
it was decided by the hearing officer (and upheld by the Patents Court)
that certain documents filed by applicants for a licence would be kept
confidential from both the public and the patentees but that the agent
who was acting for the patentees and was an employee of a subsidiary of
the patentees would have access to the documents subject to undertakings
as to the confidentiality of the documents to be given by the agent,
their employers and the patentees. Since patent agents, solicitors etc
are subject to a professional code of conduct, undertakings are not
normally required to be given by an independent agent or solicitor. In
Coal Industry's Patent [BL
O/11/90](https://www.gov.uk/government/publications/patent-decision-o01190),
the hearing officer refused to order confidentiality in respect of
evidence which had already been filed in High Court proceedings in which
there was no suggestion that it was to be treated as confidential.

\[ Requests under rule 53(1) are handled by the relevant formalities
group or Tribunal Section, as appropriate, if the documents relate to
formal matters. If however the document relates to substantive matters,
the request is considered by the group Deputy Director (as in the case
of observations filed in response to a report under s.18(3)) or by the
hearing officer. The Deputy Director should freely consult their
Divisional Director or the Registered Rights Legal Team for guidance if
necessary. \]

### 118.14 {#section-11}

When a direction is given to treat a document, or part of a document, as
confidential, the decision should be recorded on the file to which the
document relates. Furthermore, the person who made the request for
confidentiality should be notified that their request has been allowed.

\[ When a decision is made to allow a request for confidential
treatment, the officer responsible for the decision should enter the
reasons for their decision as a minute, identifying the document(s) and
directing that it is (or they are) to be treated as confidential under
rule 53(1) of the Patents Rules 2007. When signing, they should indicate
that they are acting for the comptroller. The file should then be
referred to the divisional Operations Business Manager (Business
Operations Division) or to the Head of Tribunal Section, as appropriate.
If the request relates to a paper case, they will arrange for the
document(s) to be stamped appropriately and placed on the appropriate
part of the file (details are given in Chapter 12 of the Patents
Formalities Manual). They will also arrange for a letter to be sent to
the person who made the request, notifying them that their request has
been allowed. In the very rare case where confidential treatment is to
be accorded for a limited period only, the instructions in the minute
should make this clear, so that the formalities group or Tribunal
Section can arrange for the file to be reviewed at the appropriate time.
See also paragraphs 12.39-12.50 of the [Formalities
Manual](https://www.gov.uk/government/publications/patents-formalities-manual).

### 118.15 {#section-12}

r.53(6) is also relevant

Where the comptroller believes there is no longer a good reason for the
document to be treated as a confidential document, they should revoke
the direction. However, before revoking a direction under r.53(5) or
allowing any person to inspect a document to which such a direction
applies, the comptroller should consult with the person at whose request
the direction was given, unless the comptroller is satisfied that such
prior consultation is not reasonably practicable. When a direction is
withdrawn, a record of that fact is filed with the relevant document.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 118(2)**
  Subject to the following provisions of this section, until an application for a patent is so published documents or information constituting or relating to the application shall not, without the consent of the applicant, be published or communicated to any person by the comptroller.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Unpublished application

### 118.16 {#ref118-16}

Thus documents and information constituting or relating to a particular
application cannot be published or communicated, without the consent of
the applicant, unless the application has been published under s.16
except as provided for in s.118(3) to (5), see
[118.17-23](#ref118-17-23). Prior to s.16 publication, a person making a
request on Patents Form 23 or wanting to inspect a file which thus
cannot be met is so informed [(see also 118.08)](#ref118-08); but any
requests for information on Patents Form 49 which cannot be met are
merely retained and attended to after publication [(see
118.02-05)](#ref118-02). Furthermore, if an examiner is aware of any
unpublished applications, even if by the same applicant, which if
published would be relevant to the application in suit, they should not
be mentioned in a search or examination report or any other
communication relating to the application in suit until after they have
been published since the report or communication might become open to
public inspection prior to publication of the other application and
thereby provide an indication of the contents of the unpublished
application. A limited exception to this is in the case of divisional
applications, where it is already known that the later application is
divided from an earlier one. Here it may be appropriate to make an
objection under section 18(5) before an application is published. Even
in that case detailed information concerning the unpublished application
should not be included in a report on the other application.

### 118.16.1 {#section-13}

All examiners, formalities staff and other Office staff dealing with
unpublished patent applications should be aware of the need to maintain
the security and confidentiality of material and information relating to
such cases. For example, if a telephone call is made or received
concerning an unpublished application, reasonable steps should be taken
to verify that the person being spoken to is in fact the applicant or
their designated representative. Documents (whether paper or electronic)
should be given the appropriate level of protection. Particular care
should be taken when entering into email exchanges with an applicant or
their designated representative, for example responding to an ad hoc
request for a document, such as a search report, or for information
beyond that set out in [118.18](#ref:118-18). Where the applicant has
already provided an email address, for example on patent forms, the
examiner must ensure that the email address being used is the same. If
the address is different or if no email address has previously been
provided, reasonable steps should be taken to verify that the recipient
is in fact the applicant or their designated representative. Examiners
should always check before sending, that emails are correctly addressed.

\[Where the applicant or agent has provided their informed consent to
the sending by email of documents or information (other than that set
out in [118.18](#ref:118-18) concerning an unpublished application, a
record of that informed consent must be placed on the PDAX dossier
together with the emailed correspondence and any attached documents.\]

  -----------------------------------------------------------------------
   

  **Section 118(3)**

  Subsection (2) above shall not prevent the comptroller from -\
  (a) sending the European Patent Office information which it is his duty
  to send that office in accordance with any provision of the European
  Patent Convention;\
  (aa) sending any patent office outside the United Kingdom such
  information about unpublished applications for patents as that office
  requests; or\
  (b) publishing or communicating to others any prescribed bibliographic
  information about an unpublished application for a patent;\
  \
  nor shall that subsection prevent the Secretary of State from
  inspecting or authorising the inspection of an application for a patent
  or any connected documents under section 22(6) above.
  -----------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 118(3A)**
  Information may not be sent to a patent office in reliance on subsection (3)(aa) otherwise than in accordance with the working arrangements that the comptroller has made for that purpose with that office.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **###Section 118(3B)**
  Those arrangements must include provision for ensuring that the confidentiality of information of the kind referred to in subsection (3)(aa) sent by the comptroller to the patent office in question is protected.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 118(3C)**
  The reference in subsection (3)(aa) to a patent office is to an organisation which carries out, in relation to patents, functions of the kind carried out at the Patent Office.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 118.17 {#ref118-17-23}

The prohibition in s.118(2) does not apply to the transmission of
information to the EPO as required by the EPC. The UK Office, as the
central industrial property office of a contracting State, is obliged by
EPC Article 130 to communicate to the EPO, on request, information
regarding the filing of national or European patent applications and
proceedings concerning such applications and the resulting patents.
Under this obligation, the Office now provides the EPO with citation
data on live (i.e. not withdrawn) unpublished applications searched in
the UK Office. This data is provided for the EPO's utilisation scheme,
the purpose of which is to provide the EPO with the results of searches
performed on priority applications from national offices.

### 118.17.1 {#section-14}

Sections 118(3)(aa), 118(3A), 118(3B) and 118(3C) were inserted on 1
October 2014 by the Intellectual Property Act 2014. These provisions
mean that the prohibition in s.118(2) does not apply to the sending of
pre-publication information to other patent offices, as long as it is
done in accordance with a working agreement between the Office and that
other office.

### 118.17.2 {#section-15}

Under section 118(3B), the working agreement required by section 118(3A)
must ensure that any pre-publication information shared is treated
confidentially by the other office whilst that information remains
confidential in the UK.

### 118.17.3 {#section-16}

In practice, any working agreements put in place will restrict the
sharing of pre-publication information to those circumstances where
doing so is likely to lead to a reduction in duplication of work.
Information relating to a UK patent application will therefore not be
shared until a search has been conducted by the IPO in relation to that
patent application. The agreements may also limit pre-publication
worksharing to situations where that other office is dealing with an
application which claims priority from the unpublished UK patent
application. Only information likely to reduce duplication will fall
within the scope of the worksharing arrangements, such as:

-   UK search and examination reports
-   examination opinions issued during the search/examination process
-   patent claims (so that the other office has the context of the
    search/examination)
-   classification data.

### 118.17.4 {#section-17}

The IPO will not enter into a worksharing agreement if sharing such
information would not conform with the UK's robust requirements for
sending data abroad, as set out in the Data Protection Act 1998.

### 118.17.5 {#section-18}

Details of current and future worksharing arrangements may be found on
the IPO's pages on
<https://www.gov.uk/government/publications/changes-to-patents-worksharing-business-guidance>.

### 118.18 {#ref118-18}

r.55 is also relevant

The bibliographic information about an unpublished application which can
be published or communicated is -

\(a\) the name of the applicant;

\(b\) the title of the invention;

\(c\) the number of the application;

\(d\) the date of filing of the application;

\(e\) where a priority declaration has been made for the purposes of
s.5(2)---

\(i\) the date of filing of each earlier relevant application specified
in the declaration,

\(ii\) its application number, and

\(iii\) the country it was filed in or in respect of;

\(f\) where an application has been terminated or withdrawn, that
information; and

\(g\) where a transaction, instrument or event mentioned in section
32(2)(b) or 33(3) is notified to the comptroller, that information.

Items (a) to (e) in fact appear in the Journal for all applications
about five weeks after the date of filing of each application.

### 118.19 {#section-19}

s.22(5)(c) s.22(6)(a) and s.22(6)(b) are also relevant

The Secretary of State is required to periodically review prohibition
directions given under s.22 with respect to any application, see [22.12
to
22.15](/guidance/manual-of-patent-practice-mopp/section-22-information-prejudicial-to-national-security-or-safety-of-public/#ref22-12).
For that purpose, where the application contains information relating to
the production or use of atomic energy or research into matters
connected with such production or use, the Secretary of State may at any
time inspect and/or authorise the United Kingdom Atomic Energy Authority
to inspect the application and any documents sent to the comptroller in
connection with it. In other cases, the Secretary of State may inspect
the application and any such documents at any time after (or, with the
applicant's consent before) the end of the eighteen month period
prescribed for s.16 publication ([see
16.01](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-01)).
The prohibition in s.118(2) does not apply to such inspection or
authorisation. The scope of this exclusion was extended to cover
s.22(6)(b) as well as s.22(6)(a) by paragraph 28 of schedule 5 to the
CDP Act.

### Section 118(4)

Where a person is notified that an application for a patent has been
made, but not published in accordance with section 16 above, and that
the applicant will, if the patent is granted, bring proceedings against
that person in the event of his doing an act specified in the
notification after the application is so published, that person may make
a request under subsection (1) above, notwithstanding that the
application has not been published, and that subsection shall apply
accordingly.

### 118.20 {#ref118-20}

The documents and information available under s.118(1) in respect of
published applications are also available in respect of an unpublished
application to a person in the circumstances set out in s.118(4), ie a
potential infringer who has been warned (between filing and s.16
publication of the application) that proceedings may be brought after
grant if they perform a specified act after s.16 publication. Where
s.118(4) bites, there is nothing in s.118(1) that permits the
Comptroller to prescribe any additional restrictions unique to
inspection under s.118(4). In [Buralls of Wisbech Ltd's Applications
\[2004\] RPC
14](http://rpc.oxfordjournals.org/content/121/8/285.short){rel="external"},
the hearing officer refused a request from the patent applicant to
impose confidentiality restrictions on the inspection of documents since
these could not be prescribed under s.118(1).

### 118.21 {#ref118-21}

r.52 r.51(2)(b) and r.48(3)(b) are also relevant

Where circumstances set out in s.118(4) exist, a request under s.118(1)
should be accompanied by evidence verifying their existence. Tribunal
Section sends a copy of the request and the accompanying evidence to the
applicant for the patent and until the expiry of a fourteen day period
thereafter the request is held in abeyance in accordance with rule
51(2)(b). In accordance with established practice, a person who is
entitled to inspect a document is also entitled to be supplied with a
copy of that document (subject to the payment of any fee for the copy
and where making or providing such a copy does not infringe copyright).

### 118.22 {#section-20}

When the circumstances specified in s.118(4) (or those specified in
s.118(5), see 118.23) exist, and subject to rule 52 ([see
118.21](#ref118-21)) and to the restrictions set out in
[118.07](#ref118-07), the documents relating to the unpublished
application can be inspected upon request and payment of the prescribed
fee.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 118(5)**
  Where an application for a patent is filed, but not published, and a new application is filed in respect of any part of the subject-matter of the earlier application (either in accordance with rules or in pursuance of an order under section 8 above) and is published, any person may make a request under subsection (1) above relating to the earlier application and on payment of the prescribed fee the comptroller shall give him such information and permit him to inspect such documents as could have been given or inspected if the earlier application had been published.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 118.23 {#section-21}

Where a new application is filed, notably under s.8(3) or 15(9), in
respect of any part of an earlier, unpublished application as specified
in s.118(5) and is published, the documents and information available
under s.118(1) in respect of published applications may be obtained in
respect of the unpublished application. With regard to requests for
inspection of documents relating to the unpublished application, see
[118.02](#ref118-02) (but note that r.52 does not apply to the
circumstances specified in s.118(5). Note also that the files of
applications from which priority is claimed under s.5 are not open to
inspection by virtue of s.118(5)).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
