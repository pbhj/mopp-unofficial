::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 70E: Supplementary: pending registration {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (70E.01-70E.02) last published: September 2017.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### Section 70E(1)

In sections 70 and 70B references to a patent include references to an
application for a patent that has been published under section 16.

### 70E.01 {#e01}

Section 70E(1) ensures that a threat made in relation to a published
patent application is subject to the threats regime set out in section
70 onwards. Threats proceedings can take place before (as well as after)
the patent is granted. Infringement proceedings cannot be brought until
after grant; however the alleged infringer may be liable from the
publication date ([see
69.06](/guidance/manual-of-patent-practice-mopp/section-69-infringement-of-rights-conferred-by-publication-of-application/#ref69-06)).

### Section 70E(2)

Where the threat of infringement proceedings is made after an
application has been published (but before grant) the reference in
section 70C(3) to "the patent" is to be treated as a reference to the
patent as granted in pursuance of that application.

### 70E.02 {#e02}

Section 70E(2) sets out that, when the infringement defence ([see
70C.02](/guidance/manual-of-patent-practice-mopp/section-70c-remedies-and-defences/#ref70C-02))
is relied upon, it relates to the scope of the patent as granted rather
than the scope of the patent application when published under section
16.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
