::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 37: Determination of right to patent after grant {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (37.01 - 37.22) last updated: April 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 37.01

This is the last of the three sections (8, 12 and 37) under which
questions about entitlement to patents may be referred to the
comptroller. Section 37 relates to such questions in the case of patents
granted under the Act and granted European patents (UK).

### 37.02 {#ref37-02}

s.25(1) is also relevant.

The procedure for a reference under s.37 is prescribed by Part 7
(Proceedings heard before the comptroller) of the Patents Rules 2007
([see 123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05)).
Questions may also be determined under s.37 as a result of a reference
under s.8 (which provides for questions about entitlement to be referred
to the comptroller at any time before a patent has been granted for the
invention in question). This is by virtue of s.9 which provides that, if
a question referred to the comptroller under s.8 has not been determined
when a patent is granted, it may then be treated as having been referred
under s.37. Section 37 takes effect from the date on which the notice of
grant appears in the Journal; when a question is referred between the
date on which the grant letter is issued and this date, it is treated as
having been referred under s.37 on the date on which the notice of grant
appears in the Journal ([see
8.03](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-03)).

### 37.03 {#section-1}

Certain effects of orders made under s.37, particularly their effects
with regard to licences, are laid down by s.38.

### 37.04 {#section-2}

Some guidance with regard to the determination of questions about
entitlement is given by the judgments and decisions referred to in [8.06
to
8.09](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-06),
some of which were under s.8 but others of which were under s.12 or
s.37. The considerations applying to such questions under s.8, s.12 or
s.37 are essentially the same.

  -----------------------------------------------------------------------
   

  **Section 37(1)**

  After a patent has been granted for an invention any person having or
  claiming a proprietary interest in or under the patent may refer to the
  comptroller the question\
  ­(a) who is or are the true proprietor or proprietors of the patent,\
  (b) whether the patent should have been granted to the person or
  persons to whom it was granted, or\
  (c) whether any right in or under the patent should be transferred or
  granted to any other person or persons;\
  and the comptroller shall determine the question and make such order as
  he thinks fit to give effect to the determination.
  -----------------------------------------------------------------------

### 37.05 {#ref37-05}

s.37(5) is also relevant.

A question concerning entitlement to a granted patent may be referred to
the comptroller under s.37 by any person having or claiming a
proprietary interest in a patent at any time after publication of the
mention of its grant ([see
25.02](/guidance/manual-of-patent-practice-mopp/section-25-term-of-patent/#ref25-02)).
However, the remedies available may be restricted if the reference is
made on or after the second anniversary of the mention of grant in the
journal, [see 37.19-20.1](#ref37-19).

### 37.06 {#section-3}

\[deleted\]

### 37.07 {#section-4}

The question (or one referred under s.8 but treated as referred under
s.3, [see 37.02](#ref37-02)) is normally in due course determined by the
comptroller although they may instead decline to deal with it, [see
37.21](#ref37-12). The comptroller has a general power to make such
order as they think fit to give effect to the determination, which order
may if appropriate contain inter alia any of the provisions set out in
subsection (2), [see 37.12](#ref37-12), or allow the making of a new
application, [see 37.15](#ref37-15). The order may over-ride the
specific provisions of s.36(3) -
[36.07](/guidance/manual-of-patent-practice-mopp/section-36-co-ownership-of-patents-and-applications-for-patents/#ref36-07).

### Procedure

### 37.08 {#ref37-08}

PR part 7 is also relevant.

A reference under s.37(1) should be made on Patents Form 2 accompanied
by a copy thereof and a statement of grounds in duplicate. This starts
proceedings before the comptroller, the procedure for which is discussed
at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).

### 37.09 {#section-5}

\[deleted\]

### \[37.10-11 Not used.\] {#not-used}

  -----------------------------------------------------------------------
   

  **Section 37(2)**

  Without prejudice to the generality of subsection (1) above, an order
  under that subsection may contain provision\
  (a) directing that the person by whom the reference is made under that
  subsection shall be included (whether or not to the exclusion of any
  other person) among the persons registered as proprietors of the
  patent;\
  (b) directing the registration of a transaction, instrument or event by
  virtue of which that person has acquired any right in or under the
  patent;\
  (c) granting any licence or other right in or under the patent;\
  (d) directing the proprietor of the patent or any person having any
  right in or under the patent to do anything specified in the order as
  necessary to carry out the other provisions of the order.
  -----------------------------------------------------------------------

### Specific remedies available

### 37.12 {#ref37-12}

Orders under s.37(1) may, in respect of the patent in question, inter
alia (a) direct that the claimant should be on the register as a
proprietor; (b) direct registration of a transaction, instrument or
event in favour of the claimant; (c) grant any licence. Moreover, the
existing proprietor or any person having rights in respect of the patent
may be given directions as necessary to carry out the other provisions
of the order ([see 37.13](#ref37-13) if those directions are not
complied with). However, no order is made by virtue of s.37(2) unless
notice of the reference has been given to interested parties as required
by s.37(7) (such notice normally having been given by the Office).

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 37(3)**
  If any person to whom directions have been given under subsection (2)(d) above fails to do anything necessary for carrying out any such directions within 14 days after the date of the order containing the directions, the comptroller may, on application made to him by any person in whose favour or on whose reference the order containing the directions was made, authorise him to do that thing on behalf of the person to whom the directions were given.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Directions not complied with

### 37.13 {#ref37-13}

If a person given directions under s.37(2)(d) fails to comply with them
within 14 days, any person in whose favour or on whose reference the
order containing those directions was made may apply for authority to do
whatever is necessary on behalf of that person. An application under
s.37(2)(d) is not included in Schedule 3 to the Patents Rules 2007.
Patents Form 2 need not therefore be filed in order to start the
application, which should however set out fully the facts upon which the
applicant relies and the nature of the authorisation sought.

### 37.14 {#section-6}

The Office sends a copy of the application to the person alleged to have
failed to comply with the directions. The comptroller may give such
directions as they may think fit with regard to the subsequent
procedure, and may grant authorisation if they think fit.

  -----------------------------------------------------------------------
   

  **Section 37(4)**

  Where the comptroller finds on a reference under this section that the
  patent was granted to a person not entitled to be granted that patent
  (whether alone or with other persons) and on application made under
  section 72 below makes an order on that ground for the conditional or
  unconditional revocation of the patent, the comptroller may order that
  the person by whom the application was made or his successor in title
  may, subject to section 76 below, make a new application for a patent
  -\
  (a) in the case of unconditional revocation, for the whole of the
  matter comprised in the specification of that patent; and\
  (b) in the case of conditional revocation, for the matter which in the
  opinion of\
  the comptroller should be excluded from that specification by amendment
  under section 75 below;\
  and where such new application is made, it shall be treated as having
  been filed on the date of filing the application for the patent to
  which the reference relates.
  -----------------------------------------------------------------------

### Making of new application

### 37.15 {#ref37-15}

s.72(2)(a) is also relevant.

Subsection (4) provides, at the discretion of the comptroller, for the
making of a new application by the claimant where the comptroller has
(as well as finding that an existing proprietor of the patent was not
entitled thereto) ordered conditional or unconditional revocation of the
patent in question under s.72 on entitlement grounds, [see
72.40](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-40).
The application for revocation under s.72 on those grounds may be made
only by a person who has already been found under s.37 (or by the court
in an action for a declaration or declarator) to be entitled to be
granted that patent or a patent for part of its content. If the
revocation ordered is unconditional, the permitted new application is
for the whole content of the revoked patent whereas if conditional, the
permitted new application is for that part of the patent's content which
should be excised (by amendment under s.75).

### 37.16 {#section-7}

r.20(2)-(4), r.108(1) is also relevant.

Where the decision to make an order has not been appealed, the new
r.108(1) application should be made within a period of three months
calculated from the day after the order under section 37(4) was made.
However, where an appeal is brought, the new application should be made
within a period of three months beginning immediately after the date on
which the appeal was finally disposed of. In either case, this period
may be extended or shortened at the discretion of the comptroller.

### 37.17 {#section-8}

s.76(1) is also relevant.

The new application is treated as having been filed on the date of
filing of the earlier application. However, the application requires
amendment in order to proceed if it discloses matter which extends
beyond that disclosed in the earlier application as filed, as discussed
in
[8.25](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-25).

### 37.17.1 {#ref37-17-1}

The compliance period for putting in order an application under s.37(4)
is prescribed by r.30(3) and is the same as that for an application
under s.8(3) or 12(6) - [see
8.25.1](/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-25-1).

### 37.18 {#section-9}

An order under s.37(4) allowing the making of such a new application may
be precluded by s.37(5), [see 37.19](#ref37-19). Moreover, such an order
is not made unless notice of the reference has been given to interested
parties as required by s.37(7) (such notice normally having been given
by the Office).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 37(5)**
  On any such reference no order shall be made under this section transferring the patent to which the reference relates on the ground that the patent was granted to a person not so entitled, and no order shall be made under subsection (4) above on that ground, if the reference was made after the second anniversary of the date of the grant, unless it is shown that any person registered as a proprietor of the patent knew at the time of the grant or, as the case may be, of the transfer of the patent to him that he was not entitled to the patent.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Effect of timing of reference

### 37.19 {#ref37-19}

Orders under s.37 transferring the patent in question on the ground that
it was granted to a person not so entitled, or under s.37(4) allowing a
new application on the same ground, can be made only (with one
exception) if the reference is made within a period of two years from
the date of publication of the mention of grant of the patent ([see
37.20.1](#ref37-20-1) below). The exception to this time limit is where
it is shown that a registered proprietor knew at the time of grant or,
if applicable, of transfer of the patent to them that they were not
entitled to the patent. It was held in [Cartwright's Patent (BL
O/74/93)](https://www.gov.uk/government/publications/patent-decision-o07493)
that the words "not so entitled" in s.37(5) are not restricted to a
reference under s.37(1)(b) but are also applicable in the case of a
reference under s.37(1)(a).

### 37.19.1 {#section-10}

The fact that a reference is not opposed does not override the
limitation on the comptroller's powers imposed by s.37(5) and the
consequential onus on the referrer to show that the registered
proprietor knew at the relevant time that they were not entitled to a
patent [(Parr's Patent, BL
O/46/94)](https://www.gov.uk/government/publications/patent-decision-o04694).

### 37.20 {#ref37-20}

s.130(7) is also attached.

Subsection (5) is so framed as to have, as nearly as practicable, the
same effects as the corresponding provisions of the EPC, CPC and PCT.
Article 23(3) of the CPC \[1989\] appears to be equivalent. In Yeda
Research and Development Co Ltd v RhonePoulenc Rorer International
Holdings Inc. \[2007\] UKHL 43, the House of Lords allowed an amendment
to a statement initially claiming joint ownership to claim sole
ownership even though the 2 year limitation period had expired, on the
basis that the original reference had been made within that period.

### 37.20.1 {#ref37-20-1}

The reference must be made by, at the latest, the second anniversary of
the date of grant.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 37(6)**
  An order under this section shall not be so made as to affect the mutual rights or obligations of trustees or of the personal representatives of a deceased person, or their rights or obligations as such.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 37(7)**
  Where a question is referred to the comptroller under this section an order shall not be made by virtue of subsection (2) or under subsection (4) above on the reference unless notice of the reference is given to all persons registered as proprietor of the patent or as having a right in or under the patent, except those who are parties to the reference.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 37(8)**
  If it appears to the comptroller on a reference under this section that the question referred to him would more properly be determined by the court, he may decline to deal with it and, without prejudice to the court's jurisdiction to determine any such question and make a declaration, or any declaratory jurisdiction of the court in Scotland, the court shall have jurisdiction to do so.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Comptroller declines to deal with question

### 37.21 {#section-11}

CPR 63.11 is also relevant.

The comptroller has discretion to decline to deal with a question they
consider would more properly be determined by the court. The relevant
procedure, subject to rule 63.11 of Part 63 of the Civil Procedure
Rules, and comments on the exercise of this discretion are given in 8.28
to 8.30, and chapter 2 of the Patent Hearings Manual.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 37(9)**
  The court shall not in the exercise of any such declaratory jurisdiction determine a question whether a patent was granted to a person not entitled to be granted the patent if the proceedings in which the jurisdiction is invoked were commenced after the second anniversary of the date of the grant of the patent, unless it is shown that any person registered as a proprietor of the patent knew at the time of the grant or, as the case may be, of the transfer of the patent to him that he was not entitled to the patent.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Restriction on determination by court

### 37.22 {#section-12}

Subsection (9) places a limitation (similar to that in subsection (5),
[see 37.19](#ref37-19) on the power of the court to determine a question
whether a patent was granted to a person not entitled thereto. The use
of the phrase "declaratory jurisdiction" may refer back to the way in
which it is used in subsection (8) and thus restrict the effect of
subsection (9) to Scotland, but appears more likely to have the broader
meaning of the jurisdiction of the court, both in Scotland and
elsewhere, to make a declaration.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
