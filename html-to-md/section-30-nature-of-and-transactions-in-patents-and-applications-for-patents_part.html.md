::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 30: Nature of, and transactions in, patents and applications for patents {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (30.1 - 30.9) last updated April 2007.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 30.01

The nature of patents and applications as property, and transactions
which may be made therein, are laid down in this section, except that it
does not extend to Scotland where s.31 applies instead.

### 30.02

According to s.30, particularly sub-sections (1), (3) and (4), any
patent or application is personal property; the patent or application
and rights therein vest by operation of law in the same way as any other
personal property.

### 30.03

s.77(1), s.78(2) is also relevant.

Section 30 applies in relation to not only 1977 Act patents and
applications but also granted European patents (UK) and applications for
European patents (UK).

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 30(1)**
  Any patent or application for a patent is personal property (without being a thing in action), and any patent or any such application and rights in or under it may be transferred, created or granted in accordance with subsections (2) to (7) below.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------------------------------------------------------
   
  **Section 30(2)**
  Subject to section 36(3) below, any patent or any such application, or any right in it, may be assigned or mortgaged.
  -----------------------------------------------------------------------------------------------------------------------

### 30.04

s.36(3) is also relevant.

A patent or application or right therein may be assigned or mortgaged
provided that, where two or more persons are proprietors of or
applicants for a patent, all of the proprietors or applicants have
consented (subject to ss.8, 12 and 37 (which relate to the determination
of questions about entitlement to applications and patents) and to any
agreement in force).

### 30.05 {#ref30-05}

s.130(1) is also relevant.

The term "right" in relation to any patent or application includes an
interest in the patent or application; it has also been held to extend
to the right to file a future application ([see
130.22.1](/guidance/manual-of-patent-practice-mopp/section-130-interpretation/#ref130-22-1)).
Any reference to a right in a patent includes a reference to a share in
the patent. A "mortgage" includes a charge for securing money or money's
worth.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 30(3)**
  Any patent or any such application or right shall vest by operation of law in the same way as any other personal property and may be vested by an assent of personal representatives.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 30(4)**

  Subject to section 36(3) below, a licence may be granted under any
  patent or any such application for working the invention which is the
  subject of the patent or the application; and\
  (a) to the extent that the licence so provides, a sub-licence may be
  granted under any such licence and any such licence or sub-licence may
  be assigned or mortgaged; and\
  (b)any such licence or sub-licence shall vest by operation of law in
  the same way as any other personal property and may be vested by an
  assent of personal representatives.
  -----------------------------------------------------------------------

### 30.6 {#section-4}

Following the death of the proprietor or applicant, a patent or
application or right therein or licence or sub-licence there under may
be vested by an assent of personal representatives and can thus be
disposed of in the same way as any other personal property ([see
32.10](/guidance/manual-of-patent-practice-mopp/sections-32-register-of-patents-etc/#ref32-10)
for registration procedure).

### 30.07 {#section-5}

s.36(3) is also relevant.

The grant of licences etc as detailed in s.30(4), where two or more
persons are proprietors of or applicants for a patent, again requires
the consent of all of the proprietors or applicants (subject to ss.8, 12
and 37 (which relate to the determination of entitlement to applications
and patents) and to any agreement in force).

  -------------------------------------------------------------------------------------------------
   
  **Section 30(5)**
  Subsections (2) to (4) above shall have effect subject to the following provisions of this Act.
  -------------------------------------------------------------------------------------------------

  -----------------------------------------------------------------------
   

  **Section 30(6)**

  Any of the following transactions, that is to say -\
  (a) any assignment or mortgage of a patent or any such application, or
  any right in a patent or any such application;\
  (b) any assent relating to any patent or any such application or
  right;\
  shall be void unless it is in writing and is signed by or on behalf of
  the assignor or mortgagor (or, in the case of an assent or other
  transaction by a personal representative, by or on behalf of the
  personal representative).
  -----------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 30(6A)**
  If a transaction mentioned in subsection (6) above is by a body corporate, reference in that subsection to such a transaction being signed by or on behalf of the assignor or mortgagor shall be taken to include references to its being under the seal of the body corporate.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 30.8 {#ref30-8}

Subsection (6) defines the form which certain transactions must take to
avoid being treated as void for the purposes of the section. This
subsection was deregulated by the Regulatory Reform (Patents) Order 2004
which replaced the requirement that a transaction must be signed by all
parties to the transaction with a requirement that it only has to be
signed by the assignor. The amended subsection applies to all
transactions done outside Scotland and signed on or after 1 January
2005. Transactions done before that date will need to be signed by all
parties to the assignment. [See 30.05](#ref30-05) with regard to the
meanings of "right" and "mortgage" in the Act. In Hartington Conway
Ltd's Patent Applications \[2004\] RPC 6, where the right in question
was the right to file a future application, it was held that s.30(6)
applied prior to filing a patent application and would therefore bite on
assignments made at that time.

  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 30(7)**
  An assignment of a patent or any such application or a share in it, and an exclusive licence granted under any patent or any such application, may confer on the assignee or licensee the right of the assignor or licensor to bring proceedings by virtue of section 61 or 69 below for a previous infringement or to bring proceedings under section 58 below for a previous act.
  -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 30.9 {#ref30-09}

An assignment or exclusive licence may also transfer rights under ss.58
(with regard to previous Crown use), 61 and 69 (with regard to previous
infringement) to the assignee or licensee.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
