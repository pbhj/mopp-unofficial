::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 288: Financial limits in relation to proceedings within special jurisdiction of patents county court \[Repealed\] {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Section (288.01 - 288.02) last updated April 2014.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 288.01

This section enabled a financial limit to be set for proceedings within
the patents county court's jurisdiction.

### 288.02

Section 288 has been repealed by Schedule 9, paragraph 30 of the Crimes
and Courts Act 2013.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
