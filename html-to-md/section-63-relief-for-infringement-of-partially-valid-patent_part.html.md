::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 63: Relief for infringement of partially valid patent {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (63.01 - 63.08) last updated July 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 63.01

This section allows the court or the comptroller to grant relief for
infringement of a patent found to be partially valid, but restricts the
circumstances in which certain kinds of relief may be granted for such
infringement. It also provides that amendment under s.75 may be made a
condition of relief.

### 63.02

Section 63 is applicable only in relation to granted patents and not to
applications for patents. For its applicability in relation to European
patents, [see
60.02](/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-02).

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 63(1)**
  If the validity of a patent is put in issue in proceedings for infringement of the patent and it is found that the patent is only partially valid, the court or the comptroller may, subject to subsection (2) below, grant relief in respect of that part of the patent which is found to be valid and infringed.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 63.03 {#ref63-03}

The court or the comptroller may find in infringement proceedings where
the validity of a patent is questioned that the patent is partially
invalid but partially valid and infringed. In other words, although some
claims may be invalid, the remaining claims can nevertheless be found to
be valid and infringed. Relief may be granted in respect of these valid
claims, taking into account the factors in s.63(2) ([see
63.04](#ref63-04)).

  -----------------------------------------------------------------------
   

  **Section 63(2)**

  Where in any such proceedings it is found that a patent is only
  partially valid, the court or the comptroller shall, when awarding
  damages, costs or expenses or making an order for an account of
  profits, take into account the following -\
  \
  (a) whether at the date of the infringement the defendant or defender
  knew, or had reasonable grounds to know, that he was infringing the
  patent;\
  \
  (b) whether the specification of the patent was framed in good faith
  and with reasonable skill and knowledge;\
  \
  (c) whether the proceedings are brought in good faith; and any relief
  granted shall be subject to the discretion of the court or the
  comptroller as to costs or expenses and as to the date from which
  damages or an account should be reckoned.
  -----------------------------------------------------------------------

### 63.04 {#ref63-04}

Section 63(2) therefore applies to infringement proceedings where the
patent is found to be partially valid and infringed. When awarding
damages, costs or expenses or making an order for an account of profits
in such circumstances, the court or comptroller is required to take into
account the various factors set out in paragraphs (a) to (c) of that
provision. These factors were modified and expanded upon by the
Intellectual Property (Enforcement, etc.) Regulations 2006 (S.I. 2006
No. 1028), which came into force on 29 April 2006, in order to implement
Directive 2004/48/EC on the enforcement of intellectual property rights.
(See also paragraphs [62.06 to
62.08](/guidance/manual-of-patent-practice-mopp/section-62-restrictions-on-recovery-of-damages-for-infringement/#ref62-06)).
In SmithKline Beecham Plc v Apotex Europe Ltd (No.2) \[2005\] FSR 24,
the Court of Appeal held that s.63(2) only applied when a finding had
been made in proceedings that the patent was partially invalid; this
involved determination of a dispute about the point. If the court acts
on a concession by the patentee, e.g. that certain claims are invalid
and should be deleted, that is not making a finding and s.63(2) should
not apply.

### 63.05 {#ref63-05}

Paragraph (a) requires the court or comptroller to take account of the
defendant's actual or constructive knowledge that they were infringing
the patent. This reflects the requirement in article 13.1 of the
Directive that, in such circumstances, the proprietor is entitled to
damages appropriate to the actual prejudice suffered. For this reason,
the absolute bar on damages, costs or expenses that was imposed in
section 63(2) previously, in the circumstances where the specification
was not framed in good faith and with reasonable skill and knowledge,
was not compatible with the Directive. Thus no absolute bar on damages,
costs or expenses is maintained, but paragraph (b) requires account to
be taken of the framing of the specification when determining what level
of award is appropriate. ([See 62.08 to
62.13](/guidance/manual-of-patent-practice-mopp/section-62-restrictions-on-recovery-of-damages-for-infringement/#ref62-08)
for guidance on interpretation of the phrase "framed in good faith and
with reasonable skill and knowledge").

### 63.06 {#ref63-06}

Paragraph (c) requires the court or comptroller to take into account
whether the infringement proceedings are brought in good faith.
Therefore, where it is shown that the proprietor knew that the infringed
patent was only partially valid, the court or comptroller can take that
matter into account when determining the appropriate level of damages,
costs or expenses.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 63(3)**
  As a condition of relief under this section the court or the comptroller may direct that the specification of the patent shall be amended to its or his satisfaction upon an application made for that purpose under section 75 below, and an application may be so made accordingly, whether or not all other issues in the proceedings have been determined.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 63.07 {#section-2}

The grant of relief may be made subject to the condition that the
specification of the partially valid patent should be amended under s.75
to the satisfaction of the court or the comptroller. An application
under s.75 for that purpose in response to a direction of the court or
the comptroller imposing such a condition may be made regardless of
whether or not other issues remain to be determined. The procedure
followed should be generally similar to that for amendment of a patent
found to be partially invalid in a revocation action, where a similar
condition can be imposed under s.72(4) ([see
72.43](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-43),
[72.44](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-44)
and the chapter on s.75).

### Section 63(4)

  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 63(4)**
  The court or the comptroller may also grant relief under this section in the case of a European patent (UK) on condition that the claims of the patent are limited to its or his satisfaction by the European Patent Office at the request of the proprietor.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 63.07 {#section-3}

The grant of relief may be made subject to the condition that the
specification of the partially valid patent should be amended under s.75
to the satisfaction of the court or the comptroller. An application
under s.75 for that purpose in response to a direction of the court or
the comptroller imposing such a condition may be made regardless of
whether or not other issues remain to be determined. The procedure
followed should be generally similar to that for amendment of a patent
found to be partially invalid in a revocation action, where a similar
condition can be imposed under s.72(4) ([see
72.43](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-43),
[72.44](/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-44)
and the chapter on s.75).

### 63.08 {#section-4}

Article 138(3) EPC provides a central amendment process for European
Patents. This is an alternative to the existing possibility of the
proprietor amending the patent under the 1977 Act. In the former case,
the amendments are effective in each Contracting State designated by the
patent whereas the latter would only affect the European patent (UK).
This section provides that relief may be granted on the condition that
the proprietor of a European patent (UK) limits the patent at the EPO.
The limitation would have to be done to the satisfaction of the court or
comptroller for relief to be granted.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
