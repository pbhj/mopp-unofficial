::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 132: Short title, extent, commencement, consequential amendments and repeals {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (132.01 - 132.11) last updated: October 2022.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 132.01

This section specifies the short title of the Act (the Patents Act 1977)
and certain territories and acts to which the Act applies; provides for
the commencement of the Act; and authorises Schedules 5 and 6 to the Act
which respectively set out consequential amendments and repeals of other
enactments.

  ------------------------------------------------
   
  **Section 132(1)**
  This Act may be cited as the Patents Act 1977.
  ------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 132(2)**
  This Act shall extend to the Isle of Man, subject to any modifications contained in an Order made by His Majesty in Council, and accordingly, subject to any such order, references in this Act to the United Kingdom shall be construed as including references to the Isle of Man.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  ---------------------------------------------------------------------------------------------------------------------------
   
  **Section 132(3)**
  For the purposes of this Act the territorial waters of the United Kingdom shall be treated as part of the United Kingdom.
  ---------------------------------------------------------------------------------------------------------------------------

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 132(4)**
  This Act applies to acts done in an area designated by order under section 1(7) of the Continental Shelf Act 1964, or specified by Order under section 10(8) of the Petroleum Act 1998 in connection with any activity falling within section 11(2) of that Act, as it applies to acts done in the United Kingdom.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Extent of Act

### 132.02

Sch.2, paras 1 & 2 are also relevant

Subsections (2) to (4) have the effect of extending the 1977 Act to the
Isle of Man and the territorial waters of the UK; and applying those
Acts to acts done in the offshore areas to which subsection (4) refers.

### 132.03 {#ref132-03}

s.130(1) is also relevant

Subsection (2) provides for the making of Orders in Council with regard
to the application of the Act to the Isle of Man. The current Order in
force is the Patents (Isle of Man) Order 2013 (SI 2013/2602) which
revoked the Patents Act 1977 (Isle of Man) Order 2003 (SI 2003 No.
1249), while the 2003 Order previously revoked the Patents Act 1977
(Isle of Man) Order 1978 (S.I. 1978 No. 621) and the Patents Act 1977
(Isle of Man) (Variation) Order (SI 1990 No 2295). The 2003 Order
consolidated the two earlier Orders and also gave effect to provisions
introduced into the Act using powers under the European Communities Act
1972 or the Electronic Communications Act 2000. The 2013 order replaced
the 2003 Order and made additional modifications to correspond to the
amendments made to the Act (as it has effect in the United Kingdom) by
the Medicines (Marketing Authorisations Etc.) Amendment Regulations 2005
(SI 2005/2759), the Intellectual Property (Enforcement, etc.)
Regulations 2006 (SI 2006/1028), the Registered Designs Act 1949 and
Patents Act 1977 (Electronic Communications) Order 2006 (SI 2006/1229),
the Patents (Compulsory Licensing and Supplementary Protection
Certificates) Regulations 2007 (SI2007/3293) and the Patents Act 1977
(Amendment) Regulations 2011 (SI 2011/2059). Section 130(1)(d) of the
Act states that, as respects the Isle of Man, "court" in the Act means
His Majesty's High Court of Justice in the Isle of Man (unless the
context otherwise requires).

### 132.04 {#section-2}

The wording of subsection (4) was amended by the Oil and Gas
(Enterprise) Act 1982 so as to incorporate a reference to that Act and
subsequently amended by the Petroleum Act 1998 to refer to that Act
instead. The application of the Patents Act was thus extended to certain
offshore activities in specified areas which are in a foreign sector of
the continental shelf and which comprise part of a geological structure
which extends into the foreign sector from an area designated under
s.1(7) of the Continental Shelf Act 1964. The activities in question are
connected with the exploration of, or the exploitation of the natural
resources of, the shore or bed of waters or the subsoil beneath it; and
activities carried on from, by means of, or for purposes connected with,
installations for the exploitation, exploration, storage or conveyance
of mineral resources or accommodation for persons working on such an
installation.

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 132(5)**
  This Act (except sections 77(6), (7) and (9), 78(7) and (8), this subsection and the repeal of section 41 of the 1949 Act) shall come into operation on such day as may be appointed by the Secretary of State by order, and different days may be appointed under this subsection for different purposes.
  Commencement of Act
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 132.05 {#section-3}

The passing of the 1977 Act (on 29 July 1977) brought into immediate
effect only s.132(5) of the Act. It also had the effect of repealing
s.41 of the 1949 Act which made special provision for licences in the
case of patents for inventions relating to food, medicine or surgical or
curative devices. Although ss.77(7) and (9) and 78(8) of the 1977 Act
(concerning translations of European patents (UK) and of claims of
applications therefor) came into operation at the same time, they had no
effect except to provide for the making of rules under ss.77(9) and
78(8) to bring s.77(6) and s.78(7) into force.

### 132.06 {#ref132-06}

Subsection (5) provides for the making of orders by the Secretary of
State whereby the remaining sections of the 1977 Act come into operation
on respective appointed days.

### 132.07 {#section-4}

The Patents Act 1977 (Commencement No. 1) Order 1977 (S.I. 1977 No.
2090) appointed 31 December 1977 as the day for coming into operation of
ss.84, 85 and 114 (concerning patent agents and other representatives)
and s.130 (interpretation) of the 1977 Act. It is also repealed s.88 of
the 1949 Act containing restrictions on practice as a patent agent.

### 132.08 {#section-5}

Subsequently, the Patents Act 1977 (Commencement No. 2) Order 1978 (S.I.
1978 No. 586) appointed 1 June 1978 as the day for coming into operation
of the rest of the 1977 Act (with the exception of provisions in respect
of Community patents, ie ss.53(1), 60(4) and 86 to 88).

### 132.09 {#section-6}

See also
[124.03](/guidance/manual-of-patent-practice-mopp/section-124-rules-regulations-and-orders-supplementary/#ref124-03)
to
[124.06](/guidance/manual-of-patent-practice-mopp/section-124-rules-regulations-and-orders-supplementary/#ref124-06)
and
[124.08](/guidance/manual-of-patent-practice-mopp/section-124-rules-regulations-and-orders-supplementary/#ref124-08)
with regard to the making of such rules and orders.

  ---------------------------------------------------------------
   
  **Section 64(6)**
  The consequential amendments in Schedule 5 shall have effect.
  ---------------------------------------------------------------

### Consequential amendments of other enactments

### 132.10 {#section-7}

Schedule 5 set out consequential amendments of the Crown Proceedings Act
1947, Registered Designs Act 1949, Defence Contracts Act 1958,
Administration of Justice Act 1970, Atomic Energy Authority (Weapons
Group) Act 1973, Fair Trading Act 1973 and Restrictive Trade Practices
Act 1976. The paragraphs of Schedule 5 relating to the Crown Proceedings
Act 1947, the Fair Trading Act 1973 and the Restrictive Trade Practices
Act 1976 were deleted, and those relating to the Registered Designs Act
1949 were amended, by the CDP Act.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 132(7)**
  Subject to the provisions of Schedule 4 to this Act, the enactments specified in Schedule 6 to this Act (which include certain enactments which were spent before the passing of this Act) are hereby repealed to the extent specified in column 3 of that Schedule.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Repeals of other enactments

### 132.11 {#section-8}

Certain provisions of a number of enactments, as set out in Schedule 6,
were repealed under s.132(7). The enactments affected include the
Patents and Designs Act 1907, Patents Act 1949, Patents Act 1957 and
Patents and Designs (Renewals, Extensions and Fees) Act 1961. The list
in Schedule 6 of repealed sections of the 1949 Act includes those listed
in Schedule 3, [see
127.09](/guidance/manual-of-patent-practice-mopp/section-127-existing-patents-and-applications/#ref127-09).
The repeals were subject to the transitional provisions of Schedule 4,
[see
127.10](/guidance/manual-of-patent-practice-mopp/section-127-existing-patents-and-applications/#ref127-10).
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
