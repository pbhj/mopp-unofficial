::: {#content .manual-section role="main" lang="en"}
[]{#Top}

::: {#manuals-frontend .manuals-frontend-body}
::: govuk-grid-row
::: manual-body
::: govuk-grid-column-full
# Section 125A: Disclosure of invention by specification: availability of samples of biological material {#section-title .gem-c-heading .govuk-heading-l .govuk-!-margin-bottom-4}
:::

::: govuk-grid-column-two-thirds
Sections (125A.01 - 125A.34) last updated October 2021.
:::

::: govuk-grid-column-two-thirds
::: {.gem-c-govspeak .govuk-govspeak module="govspeak"}
### 125A.01 {#a01}

This section was introduced by the CDP Act, and has subsequently been
amended by the Patents Regulations 2000 (SI 2000 No.2037). It sets out
the provisions as to how an invention which involves the use of or
concerns biological material may be disclosed and makes clear that
ceasing to comply with the requirements on availability of samples of
the biological material after a patent has been granted is a ground for
revocation. S.125A has four subsections which essentially re-enact
pre-CDP Act law and introduce a provision that access to samples of the
biological material may be restricted to prescribed persons. The Patents
Regulations 2000 substituted references to "microorganisms" in the first
and second subsections of s.125A with references to "biological
material" (see 25A.02.1) and also introduced into s.130(1) a definition
of the term "biological material" -see 130.04.1. The Patents Act 1977
(Isle of Man) Order 2003 (SI 2003 No. 1249) amended these definitions
for the Isle of Man.

  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 125A(1)**
  Provision may be made by rules prescribing the circumstances in which the specification of an application for a patent, or of a patent, for an invention which involves the use of or concerns biological material is to be treated as disclosing the invention in a manner which is clear enough and complete enough for the invention to be performed by a person skilled in the art.
  -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 125A.02 {#a02}

No action is necessary under this section if the biological material in
question is available to the public at the date of filing of the
application (and remains so throughout the life of the application and
resultant patent) or can be described in the specification in such a
manner as to enable the invention to be performed by a person skilled in
the art. The section provides an alternative way of disclosing the
invention in such a manner.

### 125A.02.1 {#a021}

PR Sch 5, paras 7 & 8, PR Sch 1, para 2 is also relevant

Subsection (1) enables rules to be made prescribing how such inventions
may be disclosed. The relevant rules are contained in Schedule 1 to the
Patents Rules 2007 which is given effect by r.13(1) of those Rules.
(However, for patent applications filed before 27 July 2000, see
paragraphs 7 and 8 of Schedule 5 to the Patents Rules 2007 for details
of the provisions which apply). The Rules relating to biological
material were amended by the Patents (Amendment) Rules 2001 (SI 2001
No.1412) to refer throughout to "biological material" instead of
"micro-organisms". The Patents (Amendment) Rules 2001 also made a number
of other changes to the Schedule, most notably that it is now possible,
under certain conditions, for a patent applicant to rely on biological
material which has previously been deposited by another party -instead
of having to deposit their own sample of the material see
[125A.04](#ref125A-04).

### 125A.03 {#a03}

The specification of an application relating to such an invention is, in
relation to the biological material itself, treated as complying with
s.14(3) if the first and second requirements set out in paragraph 3 of
Schedule 1 to the Patents Rules 2007 are satisfied, and if the
application as filed contains such relevant information as is available
to the applicant on the characteristics of the biological material.
Similarly, the disclosure in a patent specification is treated as clear
enough and complete enough in this respect, and thus to not fall foul of
s.72(1)(c) in this respect, if those requirements are satisfied. The
requirements in question are described in 125A.04. The requirements for
an application for a European patent (UK) or an international
application for a patent (UK) are described in [125A.08](#ref125A-08).
If a new deposit should prove necessary it should be made in accordance
with paragraph 8 of Schedule 1 to the Patents Rules 2007, see
[125A.24-25](#ref125A-24).

### 125A.04 {#ref125A-04}

PR Sch 1, par 1 and par 3 is also relevant

The first requirement is that, on or before the date of filing of the
application, the biological material has been deposited in a depository
institution which is able to furnish a sample of the biological
material. The second requirement is that, before the end of the period
prescribed by paragraph 3(3) of Schedule 1 to the Patents Rules 2007[see
125A.06](#ref125A-06), the name of the depository institution and the
accession number of the deposit are given in the specification of the
application; and where the biological material has been deposited by a
person other than the applicant, a statement is filed which identifies
the name and address of the depositor and a statement by the depositor
is filed authorising the applicant to refer to the biological material
in their application and giving the depositor's irrevocable consent to
it being made available to the public. A "depository institution" means
an institution which carries out the functions of receiving, accepting
and storing biological material and the furnishing of samples of such
biological material (whether generally or of a specific type); and
conducts its affairs, in so far as they relate to the carrying out of
those functions, in an objective and impartial manner.

### 125A.05 {#a05}

It does not follow that failure to comply with this condition renders
the specification not complete enough; the effect of this rule, which
takes into account the Budapest Treaty on the International Recognition
of the Deposit of Micro-organisms, to which the United Kingdom is a
party, is to specify measures which will ensure that the part of the
description of an invention which requires for its performance the use
of biological material will be regarded as clear and complete. It is
open for an applicant to argue that, despite the absence of a deposit,
the specification gives sufficient directions to enable the invention to
be performed. If however this argument is rejected by the Office (and,
as with inventions in other areas of subject-matter, the examiner will
raise objection under s.14(3) only in the clearest cases) or by the
courts, it is not possible to rectify the situation; the deposit cannot
be made after the date of filing the application.

### 125A.05.1 {#ref125A05-1}

If an application relies on a biological deposit to enable the invention
to be performed by the skilled person, and the deposit was made before
the filing date but after the claimed priority date, then there will be
no objection under s.14(3) as sufficiency is determined at the date of
filing. However, the application will not be entitled to its priority
date (for those aspects of the invention that rely on the deposit for
their performance) and so may lack novelty and/or inventive step over
documents published in the intervening period between the priority and
filing dates. This was the conclusion of the Hearing Officer in
[Cellartis AB's Application BL
O/050/11](https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/050/11){rel="external"},
as it was observed that the test for determining whether an invention is
supported by matter disclosed in an earlier relevant application under
s.5 is essentially the same as the test for determining sufficiency
under s.14(3) -- [see
5.23](/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-23).

### 125A.06 {#ref125A-06}

PR Sch 1, para 3(3) is also relevant

The information identifying the deposit, as referred to in
[125A.04](#ref125A-04), may be added to the application after the filing
date provided that this is done

\(a\) before the end of the period of sixteen months after the declared
priority date or, where there is no declared priority date, the date of
filing of the application;

\(b\) where the applicant has made a request under section 16(1) to
publish the application during the period prescribed for the purposes of
that section, on or before the date of that request; or

\(c\) where the applicant was notified under rule 52(2) that, in
accordance with section 118(4), the comptroller has received a request
by any person for information and inspection of documents under section
118(1), within one month of the date of the notification,

whichever is the earliest. This period may be extended in accordance
with r.108(2) or (3) (and in accordance with r.108(4)-(6)), [see
123.34-41](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34).
In any such case the front page of the application as published under
s.16 should carry the following notice ([see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29)):
"The information required by Schedule 1 to the Patents Rules 2007
paragraph 3(2)(a) or 3(2)(b) was not contained in the application as
filed, but was supplied later in accordance with paragraph 3(3) of that
Schedule." This notice should be amended to avoid ambiguity when more
than one biological material is disclosed. The relevant information
should be reproduced on the back of the front page of the published
application.

\[ The case examiner should give instructions via a dossier minute and
PDAX message to the appropriate formalities group to arrange for the
information to be published on the back of the front page of the 'A'
document. Those instructions should specify the exact wording to be
used, embodying the information provided by the applicant preceded by a
heading "Information required by paragraph 3(2)(a) or 3(2)(b) of
Schedule 1 to the Patents Rules 2007". If the standard text for the
notice for the front page needs to be amended to avoid ambiguity where
more than one biological material is disclosed, it is necessary for the
case examiner to make this clear in the minute or to enter a suitable
non-standard text on the "Notices for front page of 'A' document" form
instead. \]

### 125A.07 {#ref125A-07}

Following amendment by the Patents (Amendment) Rules 2001, there is no
longer a requirement to supply the date when the biological material was
deposited at the depositary institution. The date of making the deposit
is readily ascertainable from the name of the depository institution and
the accession number. There is also no longer a requirement to mention
any international agreement (eg the Budapest Treaty) under which the
biological material is deposited.

### 125A.08 {#ref125A-08}

PR Sch 1, para 3(4) is also relevant

In the case of an application for a European patent (UK) which has been
filed under the provisions of the EPC which correspond to the second
requirement set out in paragraph 3(2) of Schedule 1 to the Patents Rules
2007 (ie EPC r.31) or an international application for a patent (UK)
which has been filed under the corresponding provisions of the PCT (ie
PCT r.13bis.3), the second requirement should be treated as having been
met.

### 125A.09 {#a09}

\[Deleted\]

### 125A.10 {#a10}

If not all of the requirements of paragraph 3 of Schedule 1 to the
Patents Rules 2007 have been complied with, the examiner must form an
opinion as to whether or not the biological material is available to the
public. There are several possibilities. The biological material may be
known to be readily available to those skilled in the art, eg a
micro-organism such as baker's yeast or Bacillus natto which is
commercially available; or it may be a standard preserved strain, or
other micro-organism which the examiner knows to have been preserved in
a culture collection and to be available to the public. Alternatively
the applicant may have given in the description sufficient information
as to the identifying characteristics of the biological material and as
to the prior availability in a culture collection. In any of these cases
no further action is called for. If however the applicant has given no
information, or insufficient information, on public availability (and
the biological material is of a particular type not falling within the
known categories such as those already mentioned) then the examiner must
assume that the material is not available to the public. They must also
examine whether the biological material is described in such a manner as
to enable the invention to be carried out by a person skilled in the
art.

### 125A.11 {#a11}

If the biological material is not available to the public and if it is
not described in the application in such a manner as to enable the
invention to be carried out by a person skilled in the art, and
paragraph 3 of Schedule 1 to the Patents Rules 2007 has not been
adequately complied with, then objection under s.14(3) will be
necessary. The view is taken that it is virtually impossible to comply
with s.14(3) only by deposit of biological material in accordance with
paragraph 3 where the invention concerns a new species or higher
classification of micro-organisms. Consequently broad claims to a new
species or higher classification of micro-organism should not normally
be allowed.

\[ If it is considered that a specification containing an invention
claimed in such broad terms can meet the requirements of s.14(3) by
other means the case should be referred to the Divisional Director with
a brief note of the circumstances. \]

### 125A.12 {#a12}

Until such matters are tested in the courts, no guidance can be given as
to what extent deposit will be necessary in other circumstances, eg when
claims are directed to a new micro-organism produced by genetic
manipulation from a known micro-organism, whether a deposit of the new
micro-organism will be necessary. It could be argued that in this case
the new micro-organism (which is the product of the process disclosed)
is not necessary for the performance of the invention. The prudent
applicant will in general resolve any doubts as to whether a deposit is
needed in favour of making the deposit.

### 125A.13 {#a13}

So long as paragraph 3 of Schedule 1 to the Patents Rules 2007 is
satisfied in respect of a deposited strain, then claims may be allowed
to the deposited strain and to mutant or variant strains derived
therefrom provided that those mutants or variants produce the desired
product (eg antibiotic) identified in the specification. If specific
methods of producing such mutants or variants are not given in the
description, it is considered that the courts will construe such claims
as being restricted to mutants or variants produced by standard or
conventional methods well known to those working in the micro-organism
field and thus as being unobjectionable. If however the claims are
restricted to a single strain of a micro-organism then deposition of a
sample and disclosure of the species name of the micro-organism can be
sufficient to meet the requirements of paragraph 3.

### 125A.14 {#a14}

Where the obtaining of novel biological material, e.g. a novel
microorganism, depends on a random event with little likelihood of
repetition the requirements of s.14(3) are not regarded as satisfied
unless a deposit has been made. In particular, since a cell line is
dependent for its origin on the random selection of a cell, a deposit
will be necessary when an invention requires a cell line for its
performance.

### 125A.15 {#a15}

Compliance with paragraph 3 of Schedule 1 to the Patents Rules 2007
ensures that the disclosure meets the requirements of s.14(3) only to
the extent that the invention requires the otherwise unavailable
biological material for its performance, and there are other ways in
which the disclosure in a specification relating to biological material
can be deficient. For example when the claims are directed to the
production of a new micro-organism (from or using available
micro-organisms), a description as to how the new micro-organism has
been obtained will be necessary to satisfy s.14(3), even if a deposit of
the new micro-organism has been made.

### 125A.16 {#a16}

The deposit of biological material may be made in any depository
institution, anywhere in the world. The depository institution need not
have International Depository Authority (IDA) status. (A deposit in an
IDA is recognised for the purposes of patent procedure in all
Contracting States of the Budapest Treaty and in the EPO). It is the
applicant's responsibility to satisfy themselves that a particular
biological material will be accepted by a collection which they have
selected and that samples will be made available in accordance with
Schedule 1 to the Patents Rules 2007.

  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 125A(2)**
  The rules may in particular require the applicant or patentee (a) to take such steps as may be prescribed for the purposes of making available to the public samples of the biological material, and (b) not to impose or maintain restrictions on the uses to which such samples may be put, except as may be prescribed.
  ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 125A.17 {#a17}

Rules made under this section may prescribe how samples of the
biological material shall be made available to the public. The giving of
the information identifying the deposit, as referred to in
[125A.04](#ref125A-04), is interpreted as providing the applicant's
consent to the release of a sample by the depository institution on
receipt of the comptroller's certificate authorising such release to the
person who is named therein and who makes a valid request to the
institution.

### 125A.18 {#ref125A-18}

PR Sch 1, paras 4 and 7 is also relevant

In order to obtain the comptroller's certificate, Patents Form 8
(Request for a certificate authorising the release of a sample of
biological material) should be submitted to the Office. After the
publication of an application for a patent, Form 8 may be filed by any
person desiring a sample; before such publication, it should only be
filed by a person who has been notified in accordance with section
118(4) see [125A.06(c)](#ref125A-06). If the patent applicant has given
notice to the comptroller on Patents Form 8A of his intention that a
sample of the biological material should be made available only to an
expert, see [125A.26-33](#ref:125A-26); otherwise, the following
procedure should be used.

### 125A.19 {#ref125A-19}

PR Sch 1 para 4(5) is also relevant

If the depository institution in which the material has been deposited
is an international depository authority (in accordance with Article 7
of the Budapest Treaty), the form provided for by the Regulations under
that Treaty (BP12) should be filed with Form 8. Blank Forms BP12 are
obtainable from the Office.

### 125A.20 {#ref125A-20}

PR Sch 1, para 5(1) is also relevant

Form 8 includes an undertaking by the person making the request,

\(a\) not to make the biological material, or any material derived from
it, available to any other person; and

\(b\) not to use the biological material, or any material derived from
it, otherwise than for experimental purposes relating to the subject
matter of the invention.

### P125A.21 {#p125a21}

PR Sch 1 paras 5(2) to 5(5) is also relevant

Both parts (a) and (b) of the undertaking apply for as long as the
application or any resultant patent is extant but the undertakings shall
cease to have when the application is terminated or withdrawn or when
the patent ceases to have effect. These periods include any extension
allowed under r.107 or 108 (but not any period prior to reinstatement
under either rule). For the purpose of any act of Crown use specified in
s.55 in relation to the biological material, such an undertaking is not
required of, or is ineffective if given by, any government department or
person suitably authorised thereby. The undertaking may be varied by way
of derogation by agreement between the parties; and the undertaking is
ineffective to the extent necessary for effect to be given to any
licence of right or compulsory licence under the patent in question.

### 125A.22 {#a22}

PR Sch 1 para 4(6) is also relevant

The Office sends a copy of Form 8 (and BP12, if filed) and of the
comptroller's certificate authorising the release of the sample to the
patent applicant or proprietor, the depository institution and the
person making the request.

### 125A.23 {#a23}

If the patent applicant or proprietor and the depository institution are
agreeable to the release of a sample merely upon the request of a third
party, there is no need for Form 8 to be filed or the comptroller's
certificate to be issued.

### 125A.24 {#ref125A-24}

PR Sch 1 is also relevant

Where a deposit (either original or a replacement) of biological
material has been made at a depository institution, and either

\(a\) the biological material ceases to be available from the
institution because the biological material is no longer viable;

\(b\) the depository institution is, for any other reason, unable to
supply the biological material;

\(c\) the place where the biological material is deposited is no longer
a depositary institution for that type of material (whether temporarily
or permanently); or

\(d\) the biological material is transferred to a different depositary
institution;

PR Sch 1 is also relevant

the first and second requirements described in [125A.04](#ref125A-04)
are treated as having been complied with if, by the end of the period of
three months after the date on which the depositor is notified by the
depositary institution that (a), (b), (c) or (d) occurred or, where it
expires later, three months after the date on which that circumstance is
advertised in the journal:

\(a\) where (a), (b) or (c) above occurs:

\(i\) a new deposit of biological material is made at the relevant
depositary, and

\(ii\) that deposit is accompanied by a statement, signed by the person
making the deposit, that the biological material deposited is the same
as that originally deposited; and

\(b\) in all circumstances set out above, the applicant or proprietor
applies to the comptroller to amend the specification of the application
for the patent, or the patent, so that it meets the second requirement.

Where the biological material is no longer viable, the new deposit
should be made with the same depository institution as the original
deposit. In any other case, it may be made with another depositary
institution.

### 125A.25 {#a25}

PR Sch 1, para 8 is also relevant

No interruption is deemed to have occurred in the availability of the
deposit if the actions described in [125A.24](#ref125A-24) are carried
out within three months of the date on which the depositor was notified
of the interruption by the depositary institution or, where it expires
later, three months after the date on which that circumstance is
advertised in the journal.

  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 125A(3)**
  The rules may provide that, in such cases as may be prescribed, samples need only be made available to such persons or descriptions of persons as may be prescribed; and the rules may identify a description of persons by reference to whether the comptroller has given his certificate as to any matter.
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

### 125A.26 {#ref:125A-26}

The rules may qualify the public availability requirements by allowing
samples to be made available only to prescribed persons or descriptions
of persons. Thus, under paragraph 6 of Schedule 1 to the 2007 Rules, the
patent applicant can choose to restrict the release of samples to
experts, until the patent is granted or until 20 years from the filing
date if the patent is never granted.

### 125A.27 {#a27}

PR Sch 1, para 6(2) PR Sch 1, para 6(4) is also relevant

To take advantage of this provision, the applicant must give notice on
Patents Form 8A before the preparations for s.16 publication of the
application have been and completed. The formalities examiner should
then arrange for a standard footnote to be published on the front page
of the published application ([see
16.29](/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29))
to state that such notice has been given.

### 125A.28 {#a28}

PR Sch 1, para 6(5) PR Sch 1, para 6(6) is also relevant

It is then not possible to obtain the comptroller's certificate
authorising release of a sample as described in
[125A.18-22](#ref125A-18) until the patent is granted, or until 20 years
from the date on which the application was filed, if the application is,
terminated or withdrawn. The following procedure must instead be
followed. (This restriction to experts is however inapplicable to Crown
use by a government department or any person authorised in writing by a
government department, under s.55.)

### 125A.29 {#a29}

PR Sch 1 para 7(1), 7(2) and 5(1) is also relevant

Any third party wishing to have a sample made available has to, apply on
Patents Form 8 (in duplicate) nominating a particular expert. This
should be and accompanied by the form provided for by the Regulations
under the Budapest Treaty (BP12) if the depository institution is an
international depository authority, as described in
[125A.19](#ref125A-19). The undertaking as in [125A.20-21](#ref125A-20)
must be signed by the nominated expert.

### 125A.30 {#a30}

PR Sch 1, paras 7(3) and 7(4) is also relevant

The Office sends a copy of Form 8 to the applicant for the patent,
giving them a period of one month (which may be extended by a further
month under r.108(1) and if an adequate reason is given) in which to
object to a sample being made available to the expert.

\[ The decision whether to extend the period for objection should be
taken by the appropriate divisional Head of Admin. \]

### 125A.31 {#ref125A-31}

PR Sch 1, para 7(6) is also relevant

If the applicant does not object, the Office sends a copy of Form 8 (and
BP12, if filed) and of the comptroller's certificate authorising the
release of the sample to the applicant for the patent, the depository
institution where the sample of the biological material is stored, the
person making the request and the expert.

### 125A.32 {#a32}

PR Sch 1, para 7(4) r.73(1) PR part 7 is also relevant

Where the applicant does object, the comptroller has to determine the
matter, having given the applicant and the third party making the
request the opportunity of being heard. The applicant should notify
their objection by filing (in duplicate) Form 2 and a statement of
grounds. This starts proceedings before the comptroller, which give an
opportunity for the filing of a counter-statement by the third party and
if necessary evidence from the applicant and the third party before the
matter is heard; the procedure is discussed at [123.05 --
123.05.13](/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05).
Evidence may be filed as to the knowledge, experience, independence and
technical qualifications of the expert. If the comptroller decides to
authorise the release of the sample to the expert, action is taken as in
[125A.31](#ref125A-31).

### 125A.33 {#a33}

If the comptroller decides not to issue their certificate in favour of
the expert, the third party may nominate another person as the expert
and (subject to such directions as the comptroller thinks fit) the above
procedure may be repeated until an expert is accepted.

  ---------------------------------------------------------------------------------------------------------------------------------------------------------
   
  **Section 125A(4)**
  An application for revocation of the patent under section 72(1)(c) above may be made if any of the requirements of the rules cease to be complied with.
  ---------------------------------------------------------------------------------------------------------------------------------------------------------

### 125A.34 {#a34}

Ceasing to comply with the requirements of rules made under this
section, eg by ceasing to make samples accessible to the public, is a
ground for revocation of a patent. Compliance with the provisions of
Schedule 1 to the 2007 Rules, so that the patent discloses the invention
clearly enough and completely enough for it to be performed by a person
skilled in the art, is necessary.
:::
:::
:::
:::

::: {.gem-c-print-link .govuk-!-display-none-print .govuk-!-margin-top-3 .govuk-!-margin-bottom-3}
Print this page
:::
:::
:::
