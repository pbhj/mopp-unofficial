#!/bin/bash
#gatherstats.sh
# amalgamate the stats command lines to make running them easier, see "logging" in README.md

# opening brackets are for a subshell
    (
    cd html-to-md || exit
    grep -i -E "\b(he|him|himself|his|she|her|herself|hers)\b" --count -r \
        | sort -V \
        | sed 's/:/,/' > ../section-pronoun-counts.csv
    cp ../section-pronoun-counts.csv ../logs/"$(date -Iminutes)"_section-pronoun-counts.csv
    )

# opening brackets are for a subshell
# -P because we're relying on negative lookahead which is in PCRE (didn't work with -E for ERE, perhaps format was wrong)
    (
    cd html-to-md || exit
    grep -i -P --regexp="^(?!(>|\s{2})).*\b(he|him|himself|his|she|her|herself|hers)\b" --count -r \
        | sort -V \
        | sed 's/:/,/' > ../section-pronoun-counts_nq.csv
    cp ../section-pronoun-counts_nq.csv ../logs/"$(date -Iminutes)"_section-pronoun-counts_nq.csv
    )

# Do total count and log total:

printf "\nWriting out the section-pronoun-counts; the total being: "
cut -d, -f2 section-pronoun-counts.csv | paste -s -d+ | bc -- | tee total-pronouns-rough.txt | ts "%d/%b/%Y" | tee -a logs/section-pronoun-counts.log
printf "\nWriting out the section-pronoun-counts_nq (the important one!); the total being: "
cut -d, -f2 section-pronoun-counts_nq.csv | paste -s -d+ | bc -- | ts "%d/%b/%Y" | tee -a logs/section-pronoun-counts_nq.log