#!/bin/bash
# convertpage.sh
# v0.4 - added a bit of resilience to wget string used, for re-attempting downloads, timeouts, etc.
# v0.3 - webpage structure had changed, do checks on $section to make sure it has content
# v0.2 - added single section download, rewrote, sanitised
# v0.1 - download a section of MoPP to a $wholepage variable and then extract the relevant id="content" 
# part of the html from the variable in which the whole page is held in order to write a _part.html
# page. Tested for manualsection="manual-of-patent-practice-introduction".

# TODO consider removal of xargs
# FIXED unmatched quotes cause `xargs` to fail, so using `-0`
# FIXED  multiple copies of the HTML are getting added to the _part files, was using ">>" instead of ">"
# TODO fix "| tee /dev/fd/3" not working
# TODO include the getsections.sh script and only write out the file when it's going to be different
# TODO add switches to allow conversion to MD or not

# url of the manual
manualurl="https://www.gov.uk/guidance/manual-of-patent-practice-mopp"

# this is now taken from teh sections.txt file, see end of while loop; can be useful for debugging to add this line back in
#manualsection="manual-of-patent-practice-introduction" 

# this is an XPATH reference to the part of the gov.uk pages that holds the MoPP content itself, //*[@id="content"]
name_query="//*[@id=\"content\"]"



# # wrap prog in this loop
# # see https://stackoverflow.com/questions/1521462/looping-through-the-content-of-a-file-in-bash


# # this section can be tested by appending "--spider --no-verbose" (without the quotes) to the wget command (within the braces $()) and then commenting out all the following lines within the while-loop
# # that way wget will spider and check it can find all sections listed in sections.txt

outdir="./scraped-html"
outdirmd="./html-to-md"

# mkdir doesn't clobber an existing dir, so it's fine not to test first, but ... lets test anyway
if [ ! -d "outdir" ]; then
    mkdir "$outdir"
fi

# same for markdown output directory
if [ ! -d "outdirmd" ]; then
    mkdir "$outdirmd"
fi

logdir="./logs"
if [ ! -d "logdir" ]; then
    mkdir "$logdir"
fi
logfile=$logdir/"log_$(date -Iminutes).txt"

# see https://stackoverflow.com/questions/18460186/writing-outputs-to-log-file-and-console
# piping lines into "| tee /dev/fd/3" sends them to both console "3" and to the logs
exec > >(tee -a "$logfile") 2>&1


# # while loop reads in a line of "sections.txt" file and calls that line $manualsection
# # read documentation at https://www.computerhope.com/unix/bash/read.htm
# # in short -r reads in as raw, and -u 4 reads from a file descriptor "4" which is set as "sections.txt" by `4<sections.txt`

while read -r -u 4 manualsection; do
# # wget etc. goes here
    # sleep 1 # add 1s delay for DEBUG

    # cannot test for doing single section outside of loop as, eg with grep, you don't get a file
    # see https://stackoverflow.com/questions/229551/how-to-check-if-a-string-contains-a-substring-in-bash
    # `-n` tests for 'set variable' so if it's not set the string test should be skipped
    if [ -n "$1" ] && [[ "$manualsection" != *"$1"* ]]
    then
        # # DEBUG
        # printf "\n Not getting %s" $manualsection
        continue
    else
        printf "\n\n *** Getting %s\n" "$manualsection"
    fi

    sectionurl=$manualurl/$manualsection
    printf "\n Getting %s using wget \n" "$sectionurl" #| tee /dev/fd/3

    # TODO check if file exists and then get the download, 
    # this allows repeating script to get files that are missed or fail.
    
    # `-O -` makes output to stream rather than file
    wholepage=$(wget -O - "$sectionurl" \
                --no-parent \
                --referer="https://www.gov.uk/guidance/manual-of-patent-practice-mopp" \
                --timeout=30 --retry-connrefused --retry-on-host-error --no-hsts \
                --random-wait \
                &)

    # # debug
    # outwhole=${manualsection}_whole.html
    # echo "$wholepage" >> "$outwhole"
    # printf "\n %s page written to %s.\n" "$manualsection" "$outwhole"
    # # end debug

    # Use xargs to TRIM result; without the `-0` then some files fail as there are unmatched quotes
    partpage=$(echo "$wholepage" | xmllint --html --xpath "$name_query" - 2>/dev/null )

    # check dir exists and if so write out file to that dir
    if [ -d "$outdir" ] && [ -d "$outdirmd" ] 
    then
        outpart=${manualsection}_part.html
        echo "$partpage" > "$outdir"/"$outpart"
        printf "\n %s page written to file.\n" "$outpart" #| tee /dev/fd/3

        #also make .md file using pandoc, background it, log the output
        # pandoc --quiet will suppress warnings
        pandoc -f html -t markdown -i "$outdir"/"$outpart" -o "$outdirmd"/"$outpart".md &
        printf "\n %s markdown file made from html.\n" "$outpart".md #| tee /dev/fd/3
	else
        printf "\n %s directory doesn't exist." "$outdir" #| tee /dev/fd/3
    fi
    
done 4<sections.txt
