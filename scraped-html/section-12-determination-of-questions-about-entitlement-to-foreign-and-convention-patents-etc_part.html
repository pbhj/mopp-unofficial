<main role="main" id="content" class="manual-section" lang="en">
      <span id="Top"/>
        <div id="manuals-frontend" class="manuals-frontend-body">
      

    <div class="govuk-grid-row">
      <div class="manual-body">
        <article aria-labelledby="section-title">
          <div class="govuk-grid-column-full">
            <h1 id="section-title" class="gem-c-heading govuk-heading-l   govuk-!-margin-bottom-4">
  Section 12: Determination of questions about entitlement to foreign and convention patents, etc 
</h1>
          </div>

            <div class="govuk-grid-column-two-thirds">
                <p class="gem-c-lead-paragraph">Sections (12.01 - 12.16.1) last updated: January 2024.</p>

            </div>

          
  <div class="govuk-grid-column-two-thirds">
      
<div class="gem-c-govspeak govuk-govspeak " data-module="govspeak">
    <h3 id="section">12.01</h3>

<p>This is the second of the three sections (8, 12 and 37) under which questions about entitlement to patents may be referred to the comptroller. Section 12 relates to such questions in the case of patent applications under foreign or international law, ie the law of any country other than the UK or any treaty or international convention. This includes the EPC, for which subsections (3) and (6) make special provision, <a href="#ref12-09">see 12.09</a> and <a href="#ref12-14">12.14</a>.</p>

<h3 id="ref12-02">12.02</h3>

<p>s.12(7)(a) is also relevant</p>

<p>This section also applies in relation to applications under foreign or international law which are equivalent to applications for patents or are for protection of inventions by means other than patents.</p>

<h3 id="section-1">12.03</h3>

<p>s.12(4) is also relevant</p>

<p>Section 12 makes additional provision for the resolution of disputes between joint applicants, <a href="#ref12-11">see 12.11</a>.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 12(1)</strong></td>
    </tr>
    <tr>
      <td>At any time before a patent is granted for an invention in pursuance of an application made under the law of any country other than the United Kingdom or under any treaty or international convention (whether or not that application has been made) -<br/>(a) any person may refer to the comptroller the question whether he is entitled to be granted (alone or with any other persons) any such patent for that invention or has or would have any right in or under any such patent or an application for such a patent; or<br/> (b) any of two or more co-proprietors of an application for such a patent for that invention may so refer the question whether any right in or under the application should be transferred or granted to any other person;<br/>and the comptroller shall determine the question so far as he is able to and may make such order as he thinks fit to give effect to the determination.</td>
    </tr>
  </tbody>
</table>

<h3 id="ref12-04">12.04</h3>

<p>Questions about entitlement may be referred to the comptroller under this section at any time before a patent (or other form of protection, <a href="#ref12-02">see 12.02</a>) for an invention is granted, even before the making of an application. Such a reference may be made by any person claiming a right in any application or resultant patent etc. for that invention, in accordance with subsection (1)(a), <a href="#ref12-06">see 12.06</a>, or by a co-owner of an application contending that a right therein should be transferred or granted to any other person, in accordance with subsection (1)(b). In Magill’s International Application (<a rel="external" href="https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/256/00">BL O/256/00</a>) a US patent for the invention had already been granted in pursuance of the international application. Whilst the comptroller thus had no jurisdiction as far as that patent was concerned, s.12 was interpreted as meaning that the comptroller retained jurisdiction in respect of all other live designations in the international application.</p>

<h3 id="ref12-05">12.05</h3>

<p>s.12(2) is also relevant</p>

<p>The comptroller normally in due course determines the question so far as they are able to although they may instead decline to deal with it, <a href="#ref12-08">see 12.08</a>. The comptroller may make such order as they think fit to give effect to the determination. Much of the discussion with regard to the determination of questions of entitlement under s.8, <a href="/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-06">see 8.06 to 8.11</a>, is applicable mutatis mutandis to s.12 proceedings. However, the extent to which the comptroller is able to determine the question may be affected by the particular foreign or international law under which the application in question was or is to be made and the stage reached in the prosecution of the application, as well as the availability of information regarding the issues in question. The exercise of the comptroller’s discretion with regard to the making of orders may be affected by similar considerations. In some cases, the comptroller may be able to make a determination but not to make any effective order, in view of the fact or likelihood that the foreign or international authorities in question also have jurisdiction. In <a rel="external" href="https://doi.org/10.1093/rpc/1992rpc459">Cannings’ United States Application, [1992] RPC 459</a>, where an employee- inventor had refused to execute an assignment of his rights in a US application, which had entered the national phase by the PCT route, it was determined that (a) the comptroller has powers, subject to such other provisions of the Act as are relevant, to determine the question of ownership of an invention, which is the subject of such a US application; (b) although the comptroller’s powers under s.12 were inherently limited by the particular foreign or international law under which an application is made, since the inventor’s employer was entitled to the invention, he was also entitled to the US application itself and to any patent granted thereon; (c) it was within the comptroller’s broad powers under s.12(1) to order the employee to execute an assignment if this was necessary to give effect to the determination of entitlement and if such an assignment was required for the employer to enjoy the full benefit of any patent on the US application; and (d) the generality of s.12 in relation to the orders that may be made to give effect to the determination of entitlement permitted the comptroller to follow the approach sanctioned by s.8(5), which establishes the principle that in appropriate circumstances the comptroller has powers, at least in relation to rights in UK patent applications, effectively to bypass the unwillingness of an uncooperative party by authorising an affected party to sign, for example, a licence or assignment on their behalf.</p>

<h3 id="ref12-05-1">12.05.1</h3>

<p>In <a rel="external" href="https://doi.org/10.1093/rpc/2002rpc44">University of Southampton’s Applications [2002] RPC 44</a>, the hearing officer was mindful of the fact that where an entitlement action under s.8 has been launched, the Office will avoid taking any irrevocable action which might be detrimental to the claimant, should they subsequently be found to be entitled. He therefore held that the same approach should, as far as possible, be taken to actions under s.12 and so ordered that the defendants should identify any foreign equivalent applications to the application in dispute, thus allowing the claimants to draw national or regional offices’ attention to their interest.</p>

<h3 id="procedure">Procedure</h3>

<h3 id="ref12-06">12.06</h3>

<p>The procedure with regard to a reference under s.12(1) is the same as that for a s.8(1) reference, <a href="/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-12">see 8.12</a>.</p>

<h3 id="section-2">12.07</h3>

<p>[moved to 12.06].</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 12(2)</strong></td>
    </tr>
    <tr>
      <td>If it appears to the comptroller on a reference of a question under this section that the question involves matters which would more properly be determined by the court, he may decline to deal with it and, without prejudice to the court’s jurisdiction to determine any such question and make a declaration, or any declaratory jurisdiction of the court in Scotland, the court shall have jurisdiction to do so.</td>
    </tr>
  </tbody>
</table>

<h3 id="comptroller-declines-to-deal-with-question">Comptroller declines to deal with question</h3>

<h3 id="ref12-08">12.08</h3>

<p>CPR 63.11 is also relevant</p>

<p>The comptroller may decline to deal with a question, referred under s.12, involving matters which they consider would more properly be determined by the court. This provision has the same wording as s.8(7) and the comments in <a href="/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-28">8.28 to 8.30</a> and chapter 2 of the Patent Hearings Manual are also relevant here. The procedure for transfer to the court, subject to rule 63.11 of Part 63 of the Civil Procedure Rules, is the same as set out in <a href="/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-28">8.28</a>.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 12(3)</strong></td>
    </tr>
    <tr>
      <td>Subsection (1) above, in its application to a European patent and an application for any such patent, shall have effect subject to section 82 below.</td>
    </tr>
  </tbody>
</table>

<h3 id="entitlement-to-european-patents">Entitlement to European patents</h3>

<h3 id="ref12-09">12.09</h3>

<p>s.82(8) is also relevant</p>

<p>Prior to grant, entitlement questions relating to applications for European patents may be referred under s.12 which is applicable to the extent laid down by s.82 (see the chapter on that section) in line with the EPC Protocol on Recognition. The court and the comptroller each have jurisdiction to determine such a question (including the making of an order under s.12) if the circumstances set out in s.82(4) to (6) are met. Determinations of such questions by authorities of other states which are party to the EPC may have effect in the UK, see s.83. See also <a href="#ref12-14">12.14 to 12.16.1</a> with regard to applications for European patents (UK) which are terminated.</p>

<h3 id="section-3">12.10</h3>

<p>Once a European patent (UK) has been granted, it is treated for the determination of questions about entitlement as if it were a patent under the Act resulting from an application under the Act, and such questions should then be referred under s.37.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 12(4)</strong></td>
    </tr>
    <tr>
      <td>Section 10 above, except so much of it as enables the comptroller to regulate the manner in which an application is to proceed, shall apply to disputes between joint applicants for any such patent as is mentioned in subsection (1) above as it applies to joint applicants for a patent under this Act.</td>
    </tr>
  </tbody>
</table>

<h3 id="disputes-between-joint-applicants">Disputes between joint applicants</h3>

<h3 id="ref12-11">12.11</h3>

<p>PR part 7 is also relevant</p>

<p>This subsection makes s.10 apply (except as follows) to disputes between joint applicants for patents (or other forms of protection, <a href="#ref12-02">see 12.02</a> under foreign or international law as it applies to those under the Act. Any of the parties may therefore make a request to the comptroller for directions to settle a dispute such as referred to in <a href="/guidance/manual-of-patent-practice-mopp/section-10-handling-of-application-by-joint-applicants/#ref10-04">10.04</a>, the procedure for such a request being that described in <a href="/guidance/manual-of-patent-practice-mopp/section-10-handling-of-application-by-joint-applicants/#ref10-08">10.08</a> and <a href="/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05">123.05 – 123.05.13</a>. The exception is that the comptroller cannot give directions under s.12(4) which regulate the manner in which an application is to proceed.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 12(5)</strong></td>
    </tr>
    <tr>
      <td>Section 11 above shall apply in relation to -<br/>(a) any orders made under subsection (1) above and any directions given under section 10 above by virtue of subsection (4) above; and<br/>(b) any orders made and directions given by the relevant convention court with respect to a question corresponding to any question which may be determined under subsection (1) above;<br/>as it applies to orders made and directions given apart from this section under section 8 or 10 above.</td>
    </tr>
  </tbody>
</table>

<h3 id="effect-of-transfer-of-application">Effect of transfer of application</h3>

<h3 id="section-4">12.12</h3>

<p>s.130(1) is also relevant</p>

<p>This subsection makes s.11 apply to orders under s.12(1) <a href="#ref12-04">see 12.04 and 12.05</a> and directions given under s.10 by virtue of s.12(4) <a href="#ref12-11">see 12.11</a> as it applies to other orders and directions under s.8 or 10. It also makes s.11 similarly apply to orders and directions given by the “relevant convention court” with respect to questions such as may be determined under s.12(1). That court, in relation to any proceedings under the EPC, CPC or PCT, means the court or other body which under that convention or treaty has jurisdiction over those proceedings, including (where it has such jurisdiction) any department of the EPO.</p>

<h3 id="section-5">12.13</h3>

<p>Section 11 concerns the effects of orders or directions regarding the name(s) in which an application should proceed. Its provisions and the procedures to be followed thereunder are discussed in the chapter on s.11 which is equally applicable to operation of the section by virtue of s.12(5).</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 12(6)</strong></td>
    </tr>
    <tr>
      <td>In the following cases, that is to say -<br/> (a) where an application for a European patent (UK) is refused or withdrawn, or the designation of the United Kingdom in the application is withdrawn whether before or after publication of the application but before a question relating to the right to the patent has been referred to the comptroller under subsection (1) above  or before proceedings relating to that right have begun before the relevant convention court;<br/>(b) where an application has been made for a European patent (UK) and on a reference under subsection (1) above or any such proceedings as are mentioned in paragraph (a) above the comptroller, the court or the relevant convention court determines by a final decision (whether before or after publication of the application) that a person other than the applicant has the right to the patent, but that person requests the European Patent Office that the application for the patent should be refused; or<br/>(c) where an international application for a patent (UK) is withdrawn, or the designation of the United Kingdom in the application is withdrawn, whether before or after the making of any reference under subsection (1) above or the publication of the application;<br/>the comptroller may order that any person (other than the applicant) appearing to him to be entitled to be granted a patent under this Act may within the prescribed period make an application for such a patent for the whole or part of any matter comprised in the earlier application (subject, however, to section 76 below) and that if the application for a patent under this Act is filed, it shall be treated as having been filed on the date of filing the earlier application.</td>
    </tr>
  </tbody>
</table>

<h3 id="making-of-new-application">Making of new application</h3>

<h3 id="ref12-14">12.14</h3>

<p>Subsection (6) provides, at the discretion of the comptroller, for the making of a new application for a patent under the Act for matter comprised in an earlier application for a European patent (UK) which is no longer proceeding (in the circumstances of subsection (6)(a) or (b)) or international application for a patent (UK) which is no longer proceeding (in the circumstances of subsection (6)(c)). The comptroller may make an order allowing such a new application to be made by any person other than the original applicant who appears to them to be entitled to the grant of a patent for the matter in question. (See 12.12 for the meaning of “relevant convention court” in subsection (6)(a) and (b); and s.12(7)(b) for the meaning of “final decision” in subsection (6)(b)).</p>

<h3 id="section-6">12.15</h3>

<p>The period for making the new application is the same as for an application under s.8(3) - <a href="/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-24">see 8.24</a></p>

<h3 id="section-7">12.16</h3>

<p>s.76(1) is also relevant</p>

<p>The new application is treated as having been filed on the date of filing of the earlier application. However, the application requires amendment in order to be so treated if it discloses matter which extends beyond that disclosed in the earlier application as filed, as discussed in <a href="/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-25">8.25</a>.</p>

<h3 id="ref12-16-1">12.16.1</h3>

<p>The r.30 compliance period for putting in order an application under s.12(6) is the same as that for an application under s.8(3) or 37(4) <a href="/guidance/manual-of-patent-practice-mopp/section-8-determination-before-grant-of-questions-about-entitlement-to-patents-etc/#ref8-25-1">see 8.25.1</a>.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 12(7)</strong></td>
    </tr>
    <tr>
      <td>In this section -<br/>(a)references to a patent and an application for a patent include respectively references to protection in respect of an invention and an application which, in accordance with the law of any country other than the United Kingdom or any treaty or international convention, is equivalent to an application for a patent or for such protection; and<br/>(b) a decision shall be taken to be final for the purposes of this section when  the time for appealing from it has expired without an appeal being brought or, where an appeal is brought, when it is finally disposed of.</td>
    </tr>
  </tbody>
</table>

</div>


        
  </div>

        </article>
      </div>
    </div>

    
<div class="gem-c-print-link govuk-!-display-none-print govuk-!-margin-top-3 govuk-!-margin-bottom-3">
    <button class="govuk-link govuk-body-s gem-c-print-link__button" data-module="print-link">Print this page</button>
</div>
  </div>

    </main>
