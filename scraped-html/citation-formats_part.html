<main role="main" id="content" class="manual-section" lang="en">
      <span id="Top"/>
        <div id="manuals-frontend" class="manuals-frontend-body">
      

    <div class="govuk-grid-row">
      <div class="manual-body">
        <article aria-labelledby="section-title">
          <div class="govuk-grid-column-full">
            <h1 id="section-title" class="gem-c-heading govuk-heading-l   govuk-!-margin-bottom-4">
  Citation formats
</h1>
          </div>

            <div class="govuk-grid-column-two-thirds">
                <p class="gem-c-lead-paragraph">Section last updated December 2015.</p>

            </div>

          
  <div class="govuk-grid-column-two-thirds">
      
<div class="gem-c-govspeak govuk-govspeak " data-module="govspeak">
    <p>Examiners at the Intellectual Property Office need to be consistent in the way they cite patent documents and other prior art for any number of clear reasons, for example:</p>

<ol>
  <li>
    <p>So there is no ambiguity over what is being referred to</p>
  </li>
  <li>
    <p>To ensure the correct documents are sent to the applicant</p>
  </li>
  <li>
    <p>To allow everyone to successfully retrieve the correct document in the future</p>
  </li>
  <li>
    <p>So that the information can be cleanly and predictably transferred to other systems</p>
  </li>
</ol>

<p>Until October 2013 the indented paragraphs below Manual of Patent Practice (MoPP) <a href="/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-75">17.75</a> set out the detailed rules for formatting citations; this page supersedes that MoPP information and so becomes best practice.</p>

<h3 id="citing-patent-documents">Citing patent documents</h3>

<p>Patent citations will be present on the search report in the form of the published document’s country code, publication number and document type followed by name of applicant or patentee in capitals (possibly abbreviated) in brackets. Passages of particular relevance or a summary will follow, eg GB 2470828 A (FURUNO) See especially page 5 lines 8-14. There is no need to include the date of publication, even where the document is in the P or E category.</p>

<p>In general, patent documents should be entered on the search report in the format that appears on the physical document, omitting any commas, spaces, hyphens and full stops, but leaving a space between the country code and number, and between the number and document type (e.g. A, B, U, Y, C, etc). It is important to ensure the correct document type (also known as the kind code) is included in the citation information. The document publication number can be identified from the document’s front page as being labelled by INID code 11. Exceptions to this rule of thumb are detailed below.</p>

<p>Citations are recorded directly within PROSE or using the Internal Search Report form’s Word Add-in. Following PDN 07/13, all patent citations are entered using the “Patent Citation” option. Wherever possible patent citations should be recorded as such, but due to restrictions in the underlying OPTICS database some patent number formats must be flagged for entry in the database as non-patent literature (NPL) - PDN 07/13 para 6. The following paragraphs set out special number format rules and where records should be flagged for entry in OPTICS as NPL.</p>

<h3 id="special-number-formats">Special number formats:</h3>

<ol>
  <li>
    <p>GB patents with less than seven digits (i.e. pre-1949 Act patents published after 1915) should have leading zeros inserted to make up to seven digits in total as in the EPOQUE number format</p>
  </li>
  <li>
    <p>Irish application numbers should be entered in the format IE YYYYNNNNN inserting zeros after the date element to make 9 digits in total</p>
  </li>
  <li>
    <p>Japanese published applications from 2000 onward and references to grant numbers should be entered in the format that is used in EPOQUE but including a space between the country code and year indicator/number (see below)</p>
  </li>
</ol>

<h3 id="requiring-npl-flag">Requiring NPL flag:</h3>

<ol>
  <li>
    <p>GB patents published before 1916 should be entered in the format that is used in EPOQUE but including a space between the country code and year indicator/number (see below), eg GB YYYYNNNNN</p>
  </li>
  <li>
    <p>Australian documents with a year in the publication number should be entered in the format that is used in EPOQUE but including a space between the country code and year indicator/number (see below), eg AU YYYYNNNNNN</p>
  </li>
  <li>
    <p>Japanese published applications before 2000 should be entered in the format that is used in EPOQUE but including a space between the country code and year indicator/number (see below), eg JP EYY[nnnnn]n where E is a letter indicating the emperor period (more information is available JP number formats)</p>
  </li>
  <li>
    <p>Korean documents should be cited using the format shown on the physical document removing any hyphens or spaces.</p>
  </li>
</ol>

<h3 id="examples">Examples</h3>

<p>The following table illustrates and contrasts the correct format for entering citations on search reports in PROSE, and the formats appearing in OPTICS and EPOQUE. The document type (e.g. A or B) should be added as appropriate in the search report; if document types are not specifically mentioned in this table it is an indication that the example may be appropriate to a range of document types - as document type should always be provided.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
      <td> </td>
      <td> </td>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Physical Document</strong></td>
      <td><strong>Prose and search reports</strong></td>
      <td><strong>Optics</strong></td>
      <td><strong>Epoque</strong></td>
    </tr>
    <tr>
      <td>CN 1136129A <br/>(unexamined patent application pre-2007)</td>
      <td>CN 1136129 A</td>
      <td>CN001136129</td>
      <td>CN1136129</td>
    </tr>
    <tr>
      <td>CN 101092954A<br/>(unexamined patent application 2007 onward)</td>
      <td>CN 101092954 A</td>
      <td>CN101092954</td>
      <td>CN101092954</td>
    </tr>
    <tr>
      <td>CN 1155431C<br/>(granted patent 1993-2006)</td>
      <td>CN 1155431 C</td>
      <td>CN001155431</td>
      <td>CN1155431C</td>
    </tr>
    <tr>
      <td>CN 100580249C <br/>(granted patent 2007 onward)</td>
      <td>CN 100580249 C</td>
      <td>CN100580249</td>
      <td>CN100580249C</td>
    </tr>
    <tr>
      <td>CN 2125873U<br/>(utility model pre-1993)</td>
      <td>CN 2125873 U</td>
      <td>CN002125873</td>
      <td>CN2125873U</td>
    </tr>
    <tr>
      <td>CN 2660120Y<br/>(utility model pre-2007)</td>
      <td>CN 2660120 Y</td>
      <td>CN002660120</td>
      <td>CN2660120Y</td>
    </tr>
    <tr>
      <td>CN 201080817Y <br/>(utility model 2007 onward)</td>
      <td>CN 201080817 Y</td>
      <td>CN 201080817</td>
      <td>CN201080817Y</td>
    </tr>
    <tr>
      <td>DE 103 15 123</td>
      <td>DE 10315123</td>
      <td>DE010315123</td>
      <td>DE10315123</td>
    </tr>
    <tr>
      <td>DE 10 2004 002 764</td>
      <td>DE 102004002764</td>
      <td>DE102004002764</td>
      <td>DE102004002764</td>
    </tr>
    <tr>
      <td>EP 0 023 612</td>
      <td>EP 0023612</td>
      <td>EP0023612</td>
      <td>EP0023612</td>
    </tr>
    <tr>
      <td>FR 52.648</td>
      <td>FR 52648</td>
      <td>FR000052648</td>
      <td>FR52648</td>
    </tr>
    <tr>
      <td>GB 232, AD 1914<br/>(pre-1916 GB doc)</td>
      <td>GB 191400232<br/>(Tick “Patent stored in NPL field”)</td>
      <td>GB 191400232<br/>(Stored in NPL field)</td>
      <td>GB191400232</td>
    </tr>
    <tr>
      <td>GB 645,678</td>
      <td>GB 0645678</td>
      <td>GB0645678</td>
      <td>GB645678</td>
    </tr>
    <tr>
      <td>GB 2 136 458</td>
      <td>GB 2136458</td>
      <td>GB2136458</td>
      <td>GB2136458</td>
    </tr>
    <tr>
      <td>IE 1745/86 *</td>
      <td>IE 198601745</td>
      <td>IE198601745</td>
      <td>IE861745</td>
    </tr>
    <tr>
      <td>IE 922616 *</td>
      <td>IE 199202616</td>
      <td>IE199202616</td>
      <td>IE922616</td>
    </tr>
    <tr>
      <td>IE 040006 *</td>
      <td>IE 200400006</td>
      <td>IE200400006</td>
      <td>IE20040006</td>
    </tr>
    <tr>
      <td>IE 2008/0133 *</td>
      <td>IE 200800133</td>
      <td>IE200800133</td>
      <td>IE20080133</td>
    </tr>
    <tr>
      <td>JP 63-123456<br/>(Showa period; before April 1989)</td>
      <td>JP S63123456<br/>(Tick “Patent stored in NPL field”)</td>
      <td>JP S63123456<br/> (Stored in NPL field)</td>
      <td>JPS63123456</td>
    </tr>
    <tr>
      <td>JP 5-132<br/>(Heisei period; April 1989 onward)</td>
      <td>JP H05132<br/>(Tick “Patent stored in NPL field”)</td>
      <td>JP H05132<br/>(Stored in NPL field)</td>
      <td>JPH05132</td>
    </tr>
    <tr>
      <td>JP 5669120B</td>
      <td>JP 5669120 B<br/>(Tick “Patent stored in NPL field”)</td>
      <td>JP5669120 B<br/>(Stored in NPL field)</td>
      <td>JP5669120B</td>
    </tr>
    <tr>
      <td>JP 2000-194710<br/>(from 2000)</td>
      <td>JP 2000194710</td>
      <td>JP2000194710</td>
      <td>JP2000194710</td>
    </tr>
    <tr>
      <td>JP 2003-5005</td>
      <td>JP 2003005005</td>
      <td>JP2003005005</td>
      <td>JP2003005005</td>
    </tr>
    <tr>
      <td>R 1996-0010715<br/>(unexamined patent application pre-2000)</td>
      <td>KR 19960010715 A<br/>(Tick “Patent stored in NPL field”)</td>
      <td>KR 19960010715 A<br/>(Stored in NPL field)</td>
      <td>KR960010715</td>
    </tr>
    <tr>
      <td>KR2001-0091419<br/>(unexamined patent application 2000-2003)</td>
      <td>KR 20010091419 A <br/>(Tick “Patent stored in NPL field”)</td>
      <td>KR 20010091419 A <br/>(Stored in NPL field)</td>
      <td>KR20010091419</td>
    </tr>
    <tr>
      <td>KR 10-2006-0100346<br/>(unexamined patent application 2004 onward)</td>
      <td>KR 1020060100346 A<br/>(Tick “Patent stored in NPL field”)</td>
      <td>KR 1020060100346 A<br/>(Stored in NPL field)</td>
      <td>KR20060100346</td>
    </tr>
    <tr>
      <td>KR 10-0765938<br/>(granted patent 2000 onward)</td>
      <td>KR 100765938 B</td>
      <td>KR 100765938 B</td>
      <td>KR100765938B</td>
    </tr>
    <tr>
      <td>KR 96-17966<br/>(utility model application pre-2000)</td>
      <td>KR 9617966 U</td>
      <td>KR 9617966 U</td>
      <td>KR960017966U</td>
    </tr>
    <tr>
      <td>KR 2001-0002210<br/>(utility model application 2000-2003)</td>
      <td>KR 20010002210 U <br/>(Tick “Patent stored in NPL field”)</td>
      <td>KR 20010002210 U <br/>(Stored in NPL field)</td>
      <td>KR20010002210U</td>
    </tr>
    <tr>
      <td>KR 20-2006-0000068<br/>(utility model application 2004 onward)</td>
      <td>KR 2020060000068 U<br/>(Tick “Patent stored in NPL field”)</td>
      <td>KR 2020060000068 U<br/>(Stored in NPL field)</td>
      <td>KR20060000068U</td>
    </tr>
    <tr>
      <td>KR 20-0237149<br/>(utility model pre-2004)</td>
      <td>KR 200237149 Y</td>
      <td>KR 200237149 Y</td>
      <td>KR200237149Y</td>
    </tr>
    <tr>
      <td>US 4,539,814</td>
      <td>US 4539814</td>
      <td>US4539814</td>
      <td>US4539814</td>
    </tr>
    <tr>
      <td>US 2002/0023230</td>
      <td>US 2002/0023230</td>
      <td>US20020023230</td>
      <td>US2002023230</td>
    </tr>
    <tr>
      <td>US 10,120,240*</td>
      <td>US10120240<br/>(Tick “Patent stored in NPL field”)</td>
      <td>US10120240<br/>(Stored in NPL field)</td>
      <td>US10120240</td>
    </tr>
    <tr>
      <td>WO 88/31536</td>
      <td>WO 88/31536</td>
      <td>WO1988/031536</td>
      <td>WO8831536</td>
    </tr>
    <tr>
      <td>WO 03/061360</td>
      <td>WO 03/061360</td>
      <td>WO2003/061360</td>
      <td>WO03061360</td>
    </tr>
    <tr>
      <td>WO 2004/012345</td>
      <td>WO 2004/012345</td>
      <td>WO2004/012345</td>
      <td>WO2004012345</td>
    </tr>
  </tbody>
</table>

<h3 id="citing-abstracts">Citing abstracts</h3>

<p>It can be assumed that the search examiner will always have available a copy of the source document as well as the abstract, particularly when the document in question is a patent specification. So if the source document is not in English at least the abstract is to be copied to the applicant (see 17.104.1), the search report should be completed as below if the examiner considers it desirable or useful to identify the abstract as well as the source document.</p>

<p>Example if citing the WPI abstract:
SU 1386135 A (Belo) See Figure 2, and also WPI Abstract Accession No. 1988-298325.</p>

<p>Example if citing the EPODOC abstract:
CN 103012102 A (CHINA PETROLEUM &amp; CHEMICAL) See figures, and EPODOC abstract.</p>

<h3 id="citing-foreign-language-documents">Citing foreign language documents</h3>

<p>The examiner may, if he wishes, indicate a publically available online source of machine translation of a foreign language citation.</p>

<p>Examples:
JP nnnnnnn A (Name). Machine translation <a rel="external" href="http://aipn.ipdl.inpit.go.jp/AI2/cgi-bin/AIPNSEARCH">available from</a>, [Accessed date].</p>

<p>KR nn-nnnnnnn A (Name). Machine translation is <a rel="external" href="http://k2epat.kipris.or.kr/k2epat/searchLogina.do?next=MainSearch">available</a>, [Accessed date].</p>

<h3 id="citing-books-and-other-separately-issued-publications-eg-conference-proceedings">Citing books and other separately issued publications (eg conference proceedings)</h3>

<p>Books and other separately issued publications, such as conference proceedings, and individual web pages (for further details see the Internet Searching Manual) entered sequentially separated by commas or semicolons:</p>

<p>Document box: Author or Editor, Title, Year of Publication, Publisher,</p>

<p>Additional information: Relevant passages</p>

<p>For documents obtained through the Web, authorship should be ascribed to the smallest identifiable organisational unit, and the publisher can refer to the organisation maintaining the Internet site. The URL of the site and the date accessed should also be included.</p>

<h3 id="example-1">Example 1</h3>

<p>Document box: H Walton, “Microwave Quantum Theory”, published 1973, Sweet and Maxwell.
Additional information: see especially pages 146 to 148</p>

<h3 id="example-2">Example 2</h3>

<p>Document box: Y Kim et al, “IEEE Engineering in Medicine &amp; Biology Society 11th
Annual Conference”, published 1989, IEEE, pp 1744-5, Vol 6, Smith et al, “Digital demodulator for electrical impedance imaging”</p>

<p>Additional information:</p>

<h3 id="example-3">Example 3</h3>

<p>Document box: M Holland, “Guide to citing references” [online], published 2002, Bournemouth University. <a rel="external" href="http://www.bath.ac.uk/library/help/infoguides/references.pdf">Available from</a>, [Accessed 22 Oct 2005].</p>

<p>Additional information:</p>

<h3 id="citing-periodicals-and-journal-articles">Citing periodicals and journal articles</h3>

<p>Articles in periodicals entered sequentially separated by commas or semicolons:
Document box: Title of periodical, Volume, Date of issue, Place of publication, Author, Title of article, Page numbers where the article begins and ends</p>

<p>Additional information: Relevant passages</p>

<h3 id="example-1-1">Example 1</h3>

<p>Document box: IBM Technical Disclosure Bulletin Vol. 17, No. 5, October 1974 (New York), J G Drop, “Integrated Circuits”, pages 1344 to 1348</p>

<p>Additional information: especially page 1345.</p>

<h3 id="example-2-1">Example 2</h3>

<p>Document box: Applied Physics Letters [online] Vol. 83, No. 17, 27 October 2003, A Sharma et al, “Instability and dynamics of thin slipping films”, <a rel="external" href="http://scitation.aip.org/content/aip/journal/apl/83/17/10.1063/1.1618376">is available</a> [Accessed 28 November 2013] pages 3549 to 3551.</p>

<p>Additional information: especially page 3550.</p>

<h3 id="citing-webpages-online-discussions-and-bulletins">Citing webpages, online discussions and bulletins</h3>

<p>Reference to webpages entered sequentially separated by commas or semicolons:</p>

<p>Document box: Author, date, page title, name of website/company, [online], Available from: URL [Accessed date]</p>

<p>Additional information: relevant passages</p>

<p>As noted above, for documents obtained through the Web, authorship should be ascribed to the smallest identifiable organisational unit.</p>

<h3 id="example-1-2">Example 1:</h3>

<p>Document box: Celbius Ltd, 2012, “Sonobioprocessing”, Celbius.com, [online], <a rel="external" href="http://www.celbius.com/index.php/sonobioprocessing">Available from</a>, [Accessed 29 August 2013].</p>

<p>Additional information: See figure</p>

<p>Reference to contributions to online discussion lists and bulletin boards entered sequentially separated by commas or semicolons:</p>

<p>Document box: Author, date, subject of message, name of discussion list, [online], Available from: list e-mail address or URL, [Accessed date]</p>

<p>Additional information: relevant passages</p>

<h3 id="example-2-2">Example 2:</h3>

<p>Document box: E V Brack, 2 May 1995, “Re: Computing short courses”, Lis-link [online]. <a href="mailbase@mailbase.ac.uk">Available from</a> [Accessed 17 April 1996].</p>

</div>


        
  </div>

        </article>
      </div>
    </div>

    
<div class="gem-c-print-link govuk-!-display-none-print govuk-!-margin-top-3 govuk-!-margin-bottom-3">
    <button class="govuk-link govuk-body-s gem-c-print-link__button" data-module="print-link">Print this page</button>
</div>
  </div>

    </main>
