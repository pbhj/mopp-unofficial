<main role="main" id="content" class="manual-section" lang="en">
      <span id="Top"/>
        <div id="manuals-frontend" class="manuals-frontend-body">
      

    <div class="govuk-grid-row">
      <div class="manual-body">
        <article aria-labelledby="section-title">
          <div class="govuk-grid-column-full">
            <h1 id="section-title" class="gem-c-heading govuk-heading-l   govuk-!-margin-bottom-4">
  Section 4: Industrial application  
</h1>
          </div>

            <div class="govuk-grid-column-two-thirds">
                <p class="gem-c-lead-paragraph">Sections (4.01 - 4.07.2) last updated January 2024.</p>

            </div>

          
  <div class="govuk-grid-column-two-thirds">
      
<div class="gem-c-govspeak govuk-govspeak " data-module="govspeak">
    <h3 id="section-4-industrial-application">Section 4: Industrial application</h3>

<h3 id="ref4-01">4.01</h3>
<p>s.130(7) is also relevant</p>

<p>This section relates to the third of the tests for patentability set out in s.1(1). It is so framed to have, as nearly as practicable, the same effect in the UK as the corresponding provisions of the EPC, PCT and CPC, ie Article 57 of the EPC.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 4(1)</strong></td>
    </tr>
    <tr>
      <td>An invention shall be taken to be capable of industrial application if it can be made or used in any kind of industry, including agriculture.</td>
    </tr>
  </tbody>
</table>

<h3 id="ref4-02">4.02</h3>

<p>“Industry” should be understood in its broad sense as including any useful and practical, as distinct from intellectual or aesthetic, activity. It does not necessarily imply the use of a machine or the manufacture of a product, and covers such things as a process for dispersing fog or a process for converting energy from one form to another. In <a rel="external" href="https://doi.org/10.1093/rpc/1996rpc535">Chiron Corp v Murex Diagnostics Ltd and other [1996] RPC 535</a> (page 607) the Court of Appeal held that the requirement that the invention can be made or used “in any kind of industry” so as to be “capable of industrial application” carries the connotation of trade or manufacture in its widest sense and whether or not for profit. The Court went on to hold that industry does not exist in that sense to make or use that which is useless for any known purpose. Many of the matters which are excluded as lacking industrial application would have been rejected under previous laws as not being manners of manufacture, and in fact the views of the High Court of Australia in <a rel="external" href="https://doi.org/10.1093/rpc/79.3.73">NRDC’s Application [1961] RPC 134</a>, give a good guide to the meaning to be attributed to industrial application; there must be a product, but this need not be an article or substance, but must be something in which a new and useful effect, be it creation or alteration, may be observed. It may for example be a building, a tract or stratum of land, an explosion or an electrical oscillation, but it must be useful in practical affairs. A method of eradicating weeds was held to give rise to a product (an improved crop) because this was an artificially created state of affairs; moreover it was one whose significance was economic.</p>

<h3 id="ref4-03">4.03</h3>
<p>In many cases, an invention which passes (or fails) the test for industrial application under s.4(1) will equally pass (or fail) the test for patentability under s.1(2) (<a href="/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-07">see 1.07-1.40.4</a>). This was the case in Melia’s Application (BL O/153/92), where an application relating to a scheme for exchanging all or part of a prison sentence for corporal punishment was held to lack industrial applicability and also to be a method for doing business. In John Lahiri Khan’s Application <a rel="external" href="https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F356%2F06&amp;submit=Go+%BB">BL O/356/06</a> a method for effecting introductions with a view to making friends was held not to be industrially applicable, even though it could be carried out by a commercial enterprise. It was also found to be excluded as a method of doing business. It should be remembered however that, even though they will frequently give the same answer, the tests for industrial application and patentability are separate and independent.</p>

<h3 id="section">4.04</h3>

<p>[Deleted ]</p>

<h3 id="ref4-05">4.05</h3>

<p>Processes or articles alleged to operate in a manner which is clearly contrary to well-established physical laws, such as perpetual motion machines, are regarded as not having industrial application, as was held in Paez’s Application (BL O/176/83), Webb’s Application (BL O/84/88), and in Peter Crowley v United Kingdom Intellectual Property Office [2014]. An alternative or additional objection may be that the specification is not complete enough to allow the invention to be performed under s.14(3) (<a href="/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-67">see 14.67</a>). For example, in Eastman Kodak Co. v American Photo Booths Inc. <a rel="external" href="https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F457%2F02&amp;submit=Go+%BB">BL O/457/02</a>, which concerned a patent for a photo-booth camera, it was held that the folded optical path as described and claimed could not give rise to the claimed narrowing of the depth of field. As a result, the hearing officer held that the invention could not work as described and claimed, and so lacked both industrial applicability and sufficiency of disclosure. See also NEWMAN/Perpetual motion (EPO decision T 5/86). Objecting to insufficiency may be particularly appropriate if the claims do not refer to the intended function or purpose of the invention, for example if a “flying gyroscope” is claimed merely as an article having a particular specified construction. Regardless of whether objection arises under s.4(1) or s.14(3), one of the procedures set out <a href="/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-94">in 17.94-17.96.4</a> should be followed at the search stage.</p>

<h3 id="ref4-05-1">4.05.1</h3>

<p>In considering whether an invention operates in a manner which is clearly contrary to well-established physical laws, the examiner should consider the material before them on the balance of probabilities. If there is substantial doubt about an issue of fact which could lead to patentability, following the Patents Court judgment in <a rel="external" href="https://doi.org/10.1093/rpc/rcn035">Blacklight Power Inc. v The Comptroller-General of Patents [2009] RPC 6</a>, the examiner should consider whether the evidence provided by the applicant gives rise to a reasonable prospect that the applicant’s theory might turn out to be valid if it were to be fully investigated at a trial with the benefit of expert evidence. In such a case the application should be allowed to proceed. The reasonable prospect must be based on credible material before the Office. Moreover, the greater has been the opportunity for the applicant to produce such material at the application stage, the smaller scope there is for supposing that giving them the benefit of the doubt will lead to a different conclusion before the courts. It should also be noted that the test set out in Blacklight Power should be applied only where there is “substantial doubt” on an issue of fact. In the case of a claim to a perpetual motion machine, there is no substantial doubt, and as pointed out by the judge in this case, there would be no reasonable prospect that matters would turn out differently on a fuller investigation at trial.</p>

<h3 id="ref4-05-2">4.05.2</h3>

<p>The “reasonable prospect … at trial” test set out in <a rel="external" href="https://doi.org/10.1093/rpc/rcn035">Blacklight Power Inc. v The Comptroller-General of Patents [2009] RPC 6</a> was applied by the hearing officer on remittal of this case from the Patents Court back to the Office (Blacklight Power Inc.’s Application <a rel="external" href="https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F170%2F09&amp;submit=Go+%BB">BL O/170/09</a> and also by the hearing officer in Robinson’s Application <a rel="external" href="https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F336%2F08&amp;submit=Go+%BB">BL O/336/08</a> In both cases the claimed invention relied on a scientific theory of doubtful validity. The criteria used by the hearing officers in these cases to determine whether there was a reasonable prospect that the applicant’s theory might turn out to be valid if it were to be fully investigated at a trial with the benefit of expert evidence included; consistency with existing generally accepted theories, testable predictions made by the theory together with evidence to match its predictions, and the level of acceptance of the theory by the community of scientists working in the relevant discipline.</p>

<h3 id="section-1">4.06</h3>

<p>Methods of testing are generally regarded as capable of industrial application if the test is applicable to the improvement or control of a product, apparatus or process which itself is capable of industrial application. It is therefore advisable to indicate the purpose of the test if this is not otherwise apparent (<a href="/guidance/manual-of-patent-practice-mopp/section-14-the-application/#ref14-82">see 14.82</a>). In particular, the use of animals for test purposes in industry, e.g. for testing industrial products or for monitoring water or air pollution, may be patentable.</p>

<h3 id="section-2">4.07</h3>

<p>s1(3)(b) is also relevant</p>

<p>Although “industry” includes agriculture, a process used in agriculture will not be patentable if it is a method of surgery, therapy or diagnosis practised on animals (see section 4A) nor if it is an essentially biological process for the production of animals or plants<br/>(<a href="/guidance/manual-of-patent-practice-mopp/section-1-patentability/#ref1-43">see 1.43-1.46</a>).</p>

<h3 id="ref4-07-1">4.07.1</h3>

<p>Special considerations apply to inventions in the field of biotechnology, as the industrial application (i.e. useful purpose) of a biotechnological invention, such as a gene or protein sequence, is very often not apparent from the invention itself. It is frequently also necessary to take account of the specific requirements of European Directive 98/44/EC on the legal protection of biotechnological inventions (as set out in Schedule A2 to the Patents Act 1977), which provides (amongst other things) that the industrial application of a sequence or partial sequence of a gene must be disclosed in the patent application as filed – see 76A.06. <a rel="external" href="http://www.ipo.gov.uk/biotech.pdf">The Examination Guidelines for Patent Applications relating to Biotechnological Inventions in the Intellectual Property Office</a> provide extensive guidance for dealing with applications in this specific area of technology.</p>

<h3 id="ref4-07-2">4.07.2</h3>

<p>The question of the industrial applicability of a gene was considered by the Supreme Court in <a rel="external" href="http://rpc.oxfordjournals.org/content/129/2/102.abstract?sid=e7095e70-5779-4f8e-bbf6-7d669d92365fhttp://rpc.oxfordjournals.org/content/129/2/102.abstract?sid=e7095e70-5779-4f8e-bbf6-7d669d92365f">Human Genome Sciences v Eli Lilly [2011] UKSC 51, [2012] RPC 6</a>. Previously, in <a rel="external" href="http://rpc.oxfordjournals.org/content/125/10/733.abstract">Eli Lilly &amp; Co. v Human Genome Sciences, Inc. [2008] EWHC 1903 (Pat), [2008] RPC 29</a> (upheld by the Court of Appeal [2010] EWCA Civ 33 <a rel="external" href="http://rpc.oxfordjournals.org/content/127/6/429.abstract">[2010] RPC 14)</a>, Kitchin J applied nine principles which summarized UK and EPO case law in this area, and rejected Page 2 of 3 April 2015 the application on the grounds that it lacked industrial applicability. However the Supreme Court, having considered UK case law and European jurisprudence, concluded that the basis upon which the judge in the lower court had decided the issue was not consistent with the approach adopted by the EPO Technical Board of Appeal. Lord Neuberger summarized the Board’s approach regarding the requirements of Article 57 EPC in relation to biological material in a set of fifteen points, and concluded that in light of these points the patent in suit satisfied the requirement of Article 57 i.e. was industrially applicable. The majority of the fifteen points relate specifically to the biomedical fields; however the principles set out in points 1-4 can be applied broadly when considering the industrial application of a patent. The first four points for consideration are as follows:</p>

<p>i) The patent must disclose “a practical application” and “some profitable use” for the claimed substance, so that the ensuing monopoly “can be expected [to lead to] some … commercial benefit”;</p>

<p>ii) A “concrete benefit”, namely the invention’s “use … in industrial practice” must be “derivable directly from the description”, coupled with common general knowledge;</p>

<p>iii) A merely “speculative” use will not suffice, so “a vague and speculative indication of possible objectives that might or might not be achievable” will not do;</p>

<p>iv) The patent and common general knowledge must enable the skilled person “to reproduce” or “exploit” the claimed invention without “undue burden”, or having to carry out “a research programme”;</p>

<p>Points 5-10 relate to situations where a patent discloses a new protein and its encoding gene, while points 11-15 relate to situations where the protein is said to be a member of a family or superfamily. For further discussion of these points see the Examination Guidelines for Patent Applications relating to Biotechnological Inventions in the Intellectual Property Office.</p>

<h3 id="section-42-repealed">[Section 4(2) Repealed]</h3>

<p>[Subsection 4(2) was concerned with the patentability of methods of treatment or diagnosis. This is now provided for in section 4A(1)]</p>

<p>4.08 [deleted]</p>

<p>[4.09 – 4.20 moved to 4A.02 – 4A.13]</p>

<h3 id="section-3">4.21</h3>

<p>[deleted]</p>

<h3 id="section-4">4.22</h3>

<p>[deleted]</p>

<p>[4.23 moved to 4A.14]</p>

<h3 id="section-43-repealed">[Section 4(3) Repealed]</h3>

<p>[Subsection 4(3) was concerned with the patentability of substances for use in methods of treatment or diagnosis. This is now provided for in section 4A(2)]</p>

<p>[4.24 moved to 4A.15]</p>

</div>


        
  </div>

        </article>
      </div>
    </div>

    
<div class="gem-c-print-link govuk-!-display-none-print govuk-!-margin-top-3 govuk-!-margin-bottom-3">
    <button class="govuk-link govuk-body-s gem-c-print-link__button" data-module="print-link">Print this page</button>
</div>
  </div>

    </main>
