<main role="main" id="content" class="manual-section" lang="en">
      <span id="Top"/>
        <div id="manuals-frontend" class="manuals-frontend-body">
      

    <div class="govuk-grid-row">
      <div class="manual-body">
        <article aria-labelledby="section-title">
          <div class="govuk-grid-column-full">
            <h1 id="section-title" class="gem-c-heading govuk-heading-l   govuk-!-margin-bottom-4">
  Section 21: Observations by third party on patentability 
</h1>
          </div>

            <div class="govuk-grid-column-two-thirds">
                <p class="gem-c-lead-paragraph">Sections (21.01 - 21.26) last updated: April 2023.</p>

            </div>

          
  <div class="govuk-grid-column-two-thirds">
      
<div class="gem-c-govspeak govuk-govspeak " data-module="govspeak">
    <h3 id="section">21.01</h3>

<p>Rule 33 is relevant to this section. Where third party observations are filed at the International Bureau in relation to a PCT application, see 89B.16.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 21(1)</strong></td>
    </tr>
    <tr>
      <td>Where an application for a patent has been published but a patent has not been granted to the applicant, any other person may make observations in writing to the comptroller on the question whether the invention is a patentable invention, stating reasons for the observations, and the comptroller shall consider the observations in accordance with rules.</td>
    </tr>
  </tbody>
</table>

<h3 id="ref21-02">21.02</h3>

<p>r.33(1) is also relevant</p>

<p>Observations under section 21 must relate to patentability (i.e. relating to whether or not the invention fulfils the conditions of s.1(1) – novelty, inventive step, capable of industrial application and not excluded). The form of the observations made under s.21 is entirely a matter for the third party.</p>

<p>[Any observations relating to matters other than patentability should be referred by the examiner assistant to the relevant group head. It may be appropriate to bring another relevant section of the Act to the attention of the third party, e.g. s.8 if the observations appear to relate to entitlement, but care is necessary to avoid any implication that the Office requires the party to take action under that section or guarantees the outcome of any such action.]</p>

<p>[When observations do not relate to patentability, EL18B should be sent to the third party (unless the dedicated section 21 email address has been used) and EL23C to the applicant with a copy of the observations.]</p>

<h3 id="ref21-03">21.03</h3>

<p>s.124A(3) is also relevant</p>

<p>Third party observations can be filed by post or electronically. Electronic filing was allowed by Directions made under section 124A. These can be found online at <a href="https://www.gov.uk/government/publications/submitting-opinion-observations-by-electronic-means">Submitting opinion observations by electronic means</a> and are reprinted in full in the “Relevant Official Notices and Directions” section of this Manual. These Directions require observations to be sent by email to <a href="mailto:Section21@ipo.gov.uk">Section21@ipo.gov.uk</a> or to be delivered on digital media. Observations sent to any other email address may be treated as not filed. However they should not generally be treated in this way if they comply with the other conditions in these Directions and are successfully received at an email address within the Office. Observations received at the dedicated email address will be acknowledged by an automatic email reply. This email states that if the observations can be treated as made under section 21, they will be forwarded to the examiner for consideration, placed on the open part of the file, and a copy sent to the applicant.</p>

<h3 id="ref21-04">21.04</h3>

<p>r.33(3) is also relevant.</p>

<p>Any written communication received in the Office (whether sent by post or email) relating to an application from any person other than the applicant or the agent responsible for the application should be copied to the applicant. This is so they are aware of any document which may be laid open to public inspection. This includes communications which may include comments on areas other than patentability. Additionally if a complaint about the processing of an application is received from a third party, the complaint and the Office’s response should be copied to the applicant.</p>

<p>[On receiving them in the Office, these written communications should be sent to the examiner assistants. To do this, the formalities examiner should send the appropriate PDAX message to the s.21 observations PDAX mailbox. An examiner assistant will monitor this mailbox and should acknowledge the communication (unless the dedicated email address is used, where no acknowledgement is necessary since a reply is sent automatically from this address, <a href="#ref21-03">see 21.03</a>.]</p>

<p>[The examiner assistant will check for and redact potentially offensive or libellous material (<a href="#ref21-06">see 21.06</a>) and personal data where necessary (<a href="#ref21-08">see 21.08</a>). It is the examiner assistant’s responsibility to ensure that the correct documents are made open to public inspection and the OLFI annotation is added as appropriate.]</p>

<h3 id="ref21-05">21.05</h3>

<p>Observations on patentability filed in relation to one application can, where appropriate, be considered in relation to any other application relating to the same invention. In particular, where observations are filed on a priority document, if they appear relevant to the later application, the observations should be considered for that later application. The applicant should be informed and the observations should be copied to them if they have not previously been copied. If the priority document is unpublished, the observations may nevertheless be copied onto the open file of the later application.</p>

<h3 id="ref21-06">21.06</h3>

<p>r.33(2) and r.51(2)(d) is also relevant</p>

<p>If observations filed by a third party contain libellous or obscene matter, the text both as communicated and as placed on the open file, should have the offending remarks omitted. The informant’s observations should merely be acknowledged, while the letter sent to the applicant should be modified to indicate that some non-relevant matter has been deleted from the enclosed copy of the observations.</p>

<p>[Offensive or libellous material should not be open to public inspection or be made available via Ipsum. If an examiner assistant identifies or is made aware of potentially offensive or libellous material they should ensure that the document is not open to public inspection. The examiner assistant should consult the relevant group head when potentially faced with such material. Where there is agreement between the group head and the examiner assistant, the offensive or libellous material should be redacted before the document can be made open for public inspection. If it is determined that there is no offensive or libellous material present, the relevant documents should be made open to public inspection. Where the case requires another action (e.g. search, exam or amendment) while the decision is being made on the potentially offensive or libellous material, the standard message for that action should be sent as normal by the formalities examiner.]</p>

<p>[For handling confidential or sensitive information in observations <a href="#ref21-08">see 21.08</a>]</p>

<h3 id="ref21-07">21.07</h3>

<p>r.33(3) is also relevant.</p>

<p>The 2007 Rules make specific provision with regard to the copying of documents referred to in the informant’s letter. It is at the comptroller’s discretion whether to send the applicant a copy of any document received from the third party and referred to in the observations. However, in general, documents referred to in observations should be copied to the applicant unless the document is clearly readily available to the applicant or photocopying is impractical. If other materials (e.g. samples) have been sent with the letter the applicant should be informed that they are available for inspection in the Office.</p>

<p>[If a copy of a document sent with an informant’s letter is requested under rule 48 (using Form 23), it should be supplied.]</p>

<h3 id="ref21-08">21.08</h3>

<p>r.33(1) and r.53 is also relevant</p>

<p>Documents or other materials which are filed by the third party under s.21 must normally remain on the open file of the application in question. Hence if a third party sends documents or materials to the Office with a view to its being used in this way, but requests that the material be returned to them in due course, they should be informed that either the material must remain on file or that it will be returned to them without being considered under s.21. Having regard to the terms of r.33(1), it would appear that observations on patentability should be sent to the applicant irrespective of any request under r.53 that the observations be treated as confidential. However it is possible for parts of the observations to be treated as confidential to the extent that they are not laid open to public inspection on the file.</p>

<p>[Documents filed containing observations should be put on the open part of the file and made available on Ipsum. Documents or samples which cannot be readily accommodated on the file should be placed in a box file or other suitable container. A minute should be added to the dossier stating where the container is stored and the PDAX front cover should be labelled to note that there are samples included.]</p>

<p>[The examiner assistant should check correspondence from third parties for personal data and redact as necessary. Personal data includes the third party’s names, addresses, email addresses and telephone numbers. Where the personal data of the third party is separable from the comments on patentability (e.g. a cover letter introducing the third party and a separate document containing the patentability comments), the documents should be separated and only the comments on patentability should be copied to the applicant.]</p>

<p>[The examiner assistant should refer any requests for confidentiality under r.53 to the relevant group head who will consider the issue of public interest (<a href="/guidance/manual-of-patent-practice-mopp/section-118-information-about-patent-applications-and-patents-and-inspection-of-documents/#ref118-13">see 118.13</a>). However, objections to patentability should only be made on the basis of documents which are open to public inspection]</p>

<h3 id="ref21-09">21.09</h3>

<p>Observations may be filed anonymously. When anonymous observations are received, they should be treated so far as possible in the same way as those from named third parties.</p>

<p>[If documents are filed with comments about patentability mixed in with personal data and the third party has requested anonymity, the examiner assistant should write to the third party giving them the opportunity to withdraw their observations so that they are not made available and are not considered under s.21. If no response is received within two weeks the observations should be processed under s.21 in the normal way. If the comments on patentability are separable from any personal data and the third party has requested anonymity, there is no need to write to the third party as the procedure in 21.08 can be carried out, making the observations anonymous.]</p>

<h3 id="action-when-observations-are-received-before-section-16-publication">Action when observations are received before Section 16 Publication</h3>

<h3 id="ref21-10">21.10</h3>

<p>If observations relating to patentability are filed prematurely (i.e. before s.16 publication of the application in question), the applicant should be informed that the observations will be treated as having been filed under s.21 if and when the application is published. If termination action has been taken or is appropriate when any such letter is received, receipt should be acknowledged and the letter should merely be copied to the applicant and placed on the file, <a href="#ref21-11">see 21.11</a>. If the terminated application has been published, the letter will be open to public inspection. If the observations do not relate to patentability, <a href="#ref21-02">see 21.02</a>.</p>

<p>[If observations relating to patentability are filed before s.16 publication of the application in question, EL18A should be sent to the third party (unless the dedicated section 21 email address has been used). At the same time, the observations should be copied to the applicant using EL23B.]</p>

<h3 id="action-when-observations-are-received-after-section-16-publication-and-before-a-report-under-section-184">Action when observations are received after Section 16 Publication and before a report under Section 18(4)</h3>

<h3 id="ref21-11">21.11</h3>

<p>For a document to be considered as observations under s.21, it should be filed between s.16 publication and grant. The procedure here should be followed when observations are filed between s.16 publication and a report under s.18(4) has been issued. There is a separate procedure for observations that are filed between the s.18(4) report being issued and the grant.</p>

<p>[Provided that the observations relate, at least in part, to patentability and have been have been received in the Office after s.16 publication but before a report under s.18(4) has been issued, EL18 (which also includes information regarding s.21 observations) should be sent to the third party unless the dedicated section 21 email address has been used and EL23 (or ELC23, see 21.13) should be sent to the applicant, along with a copy of the observations. If the observations do not relate to patentability, see 21.02. Regardless of whether the observations relate to patentability or not, if observations are received when termination action has been taken or is appropriate, EL18B should be sent to the third party (unless the dedicated section 21 email address has been used) and EL23C to the applicant with a copy of the observations.]</p>

<h3 id="ref21-12">21.12</h3>

<p>R.33(4) and r.33(5) is also relevant</p>

<p>The observations should be considered by the examiner, who must make up their own mind whether on the balance of probabilities they support a sustainable objection. For example, the examiner should consider any alleged prior art in exactly the same way as they would if it had been found in the course of the search. If the date at which any alleged prior art was published, used or otherwise made available to the public is not given or cannot be established (e.g. by following the procedure in <a href="#ref21-19">21.19</a> if it is felt that the third party is likely to be aware of the date), no objection should be raised. If an objection does arise, in general, the examiner should raise this in a report under s.18(3) in their own words. However, if the examiner fully agrees with well-argued observations, they can raise an objection of lack of novelty or inventive step by formally citing the relevant documents and then drawing the applicant’s attention to the supporting argument as set out in the observations. If, in the examiner’s view, no objection arises, no comment on the observations is necessary.</p>

<p>[A formal objection based on material provided by a third party should not be dropped unless the applicant makes a response sufficient to counter the objection. In general, no comment on the observations should be made when the examiner is not raising an objection arising out of them. However if it becomes necessary to refer to the observations, for example because of a specific query from the applicant, a comment that the observations have been taken into account in framing the s.18(3) report may be made. Reasons for not raising objections should be briefly recorded as a minute.]</p>

<h3 id="ref21-13">21.13</h3>

<p>Where the observations are received after the issue of a report under s.18(3), they should be copied immediately to the applicant. It is for the examiner to decide whether to accompany them by a further report under s.18(3) taking account of the observations (see 18.50), or whether to defer such action until a reply is received to the outstanding report. Where late-filed observations give rise to a report under s.18(3), the compliance period may be extended as described in <a href="/guidance/manual-of-patent-practice-mopp/section-20-failure-of-application/#ref20-02-1">20.02.1</a>.</p>

<p>[When sending the copy of the observations to the applicant at the same time as the report under s.18, ELC23 should be used in the covering letter to the s.18 report instead of sending EL23.]</p>

<h3 id="ref21-14">21.14</h3>

<p>Section 21 requires the third party to state reasons for the observations. If no reasons are (explicitly) stated the observations should nevertheless be acknowledged, be put on the open file and the applicant sent a copy as normal. The examiner should consider the observations and act upon them as appropriate. If reasons for the observations are not stated, the third party should not be asked for any.</p>

<h3 id="ref21-15">21.15</h3>

<p>When the documents referred to by a third party are numerous and/or lengthy the examiner should do their best to identify those portions likely to be relevant to novelty or obviousness. Exceptionally, if the examiner is unable to identify the relevant passages, the third party may be requested to do so, but, if they do not, the matter should not be pursued with them.</p>

<h3 id="ref21-16">21.16</h3>

<p>If a communication giving the results of a search in another Office is received from a foreign agent apparently acting for the applicant in another country, it should be acknowledged and copied to the applicant. The contents of the communication should be treated as in paragraphs <a href="/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-44">17.44</a> and <a href="/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-46">17.46</a>.</p>

<p>[EL18B and EL23C should be used.]</p>

<h3 id="ref21-17">21.17</h3>

<p>If documents contained in the observations are formally cited (rather than the applicant being merely notified of it under r.33(1)), they should be included on the front page of the grant specification (<a href="/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-85">see 18.85</a>).</p>

<p>[Any document cited which was not listed on the A document should be recorded as soon as possible on COPS and on the internal search report (see <a href="/guidance/manual-of-patent-practice-mopp/section-17-search/#ref17-105">17.105</a>, <a href="/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-11">18.11</a>). If a document which was previously sent to the applicant for notification subsequently becomes relevant and is formally cited, the substantive examiner should send the applicant a copy of it together with the relevant s.18 report.]</p>

<h3 id="ref21-18">21.18</h3>

<p>An allegation of prior use received from a third party should be treated as described in paragraph <a href="/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-24">18.24</a>.</p>

<h3 id="ref21-19">21.19</h3>

<p>If an applicant denies material facts contained in observations made under s.21, for example an allegation of prior use or prior publication, and it is thought probable that the third party could provide evidence to substantiate their allegation, then it is possible to invite them to do so. This procedure may also be used if an examiner considers that s.21 observations give insufficient information to justify action on novelty or obviousness grounds but that there is a strong probability that further information could be provided by the third party enabling the objection to be made. The informant could also be approached for evidence or information concerning the date of publication of a highly relevant document which they have supplied. However such action should not be taken unless the information already available provides a clear indication both of the need for the further information and also of the likelihood that the third party will be in a position to supply it. Any such invitation to the informant should not be a direct request for evidence or information; it should instead be indicated that an objection cannot be raised or pursued unless such material is available. Applicants must accept that the onus is on them to demonstrate that they are entitled to grant but care should always be taken not to take any action which could imply that the third party is a party to the proceedings (<a href="#ref21-25">see 21.25-21.26</a>. If an allegation by the third party is supported by a witness statement or similar formal evidence, then any denial by the applicant should also similarly evidenced.</p>

<p>[The relevant group head should be consulted before approaching the third party. A request for further information or evidence should invite a response within a specified period (e.g. two months); if no response is received within this period the matter should not be pursued.]</p>

<h3 id="ref21-20">21.20</h3>

<p>If observations are received in the Office after the end of the compliance period but before a report under s.18(4) has been issued, the third party should be informed that the observations cannot be considered by the examiner. It is not necessary to inform the third party if the observations have been sent to the dedicated section 21 email address (<a href="#ref21-03">see 21.03</a>. However it is important to note that the observations may need to be considered if the compliance period is subsequently extended. The observations should be copied to the applicant in the usual way. It is for the applicant to decide whether to amend the specification (but see <a href="#ref21-24">21.24</a>).</p>

<p>[EL18B should be sent to the third party (unless the dedicated section 21 email address has been used) and EL23C should be sent to the applicant, along with a copy of the observations.]</p>

<h3 id="action-when-observations-are-received-after-a-report-under-section-184-and-before-grant">Action when observations are received after a report under Section 18(4) and before Grant</h3>

<h3 id="ref21-21">21.21</h3>

<p>r.33(1) and r.33(5) is also relevant</p>

<p>Observations cannot be considered by the examiner if they are received on or after the date a report under a s.18(4) (such as an intention to grant letter) has been issued. If observations are received in the Office in this period, the third party should be informed that, due to the terms of r.33(5), the observations cannot be considered by the examiner. It is not necessary to inform the third party if the observations have been sent to the dedicated section 21 email address (<a href="#ref21-03">see 21.03</a>). Even though the examiner cannot consider the observations, they should still be copied to the applicant and the applicant should be informed that grant of the application will be delayed by two months unless they request that it be granted earlier. This period is to allow for the applicant to decide whether to submit voluntary amendments; it is up to the applicant to decide whether to amend the specification (but see <a href="#ref21-24">21.24</a>). If the s.18(4) report is rendered nugatory by the applicant filing amendments under r.31(4)(a) which are not allowable (<a href="/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-18">see 19.18</a>), then the procedure in <a href="#ref21-11">21.11-21.19</a> above should be followed.</p>

<p>[EL18B should be sent to the third party (unless the dedicated section 21 email address has been used) and EL23D should be sent to the applicant, along with a copy of the observations. As grant will be delayed by two months, the examiner assistant should diary the application to return in two months and one week from the date the EL23D is issued. If the applicant responds with amendments within the two months, they should be dealt with as in <a href="/guidance/manual-of-patent-practice-mopp/section-19-general-power-to-amend-application-before-grant/#ref19-17">19.17-19.21</a>. If the applicant does not respond with amendments within two months, when the diary returns the examiner assistant should send the case on for grant by updating the status to 11: Ready for Grant and sending a “Grant checklist” message to the relevant formalities group. If the applicant responds before the two-month deadline but declines the opportunity to amend then the application can be sent for grant straight away. Regardless of whether the applicant does not respond to the EL23D or responds declining to amend, the examiner assistant should minute the case with a request for the examiner to check whether s.73(1) action is needed as a result of the prior art referred to in the observations, see <a href="#ref21-24">21.24</a>. The examiner assistant should then send a ‘Please see minute’ PDAX message to the examiner.]</p>

<p>[Third parties contemplating filing s.21 observations sometimes enquire about the likely date of issue of the grant letter. If an intention to grant letter has issued (i.e. a report under s.18(4)) they should be informed that it is too late to file observations under s.21. If it has not, they are entitled to no further information as to likely timescales and should merely be advised to file the observations as soon as possible.]</p>

<h3 id="ref21-22">21.22</h3>

<p>r.33(5) and 1.107 is also relevant</p>

<p>If observations are received in the Office before the issue of a report under s.18(4) but too late to prevent its issue and they give rise to a fresh objection, the s.18(4) report may be rescinded and action taken on the observations (see <a href="/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-89">18.89</a>). If, in this situation the examiner concludes that no fresh objection arises from the observations, the s.18(4) report should not be rescinded and they should inform the applicant that the observation have been considered and no objection will be raised. Action may also be taken if the report under s.18(4) has been rescinded for another reason.</p>

<p>[EL23A should be sent to the applicant to communicate the observations when no objection results. However, if fresh objections arise and the case has been sent for grant, the publication liaison officer of the relevant division should be contacted immediately. See also <a href="/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-89">18.89</a>. If, on the other hand, fresh objection arise and the case has not been sent for grant, cancellation of grant is not necessary.]</p>

<h3 id="action-when-observations-are-received-after-a-patent-has-been-granted">Action when observations are received after a patent has been granted</h3>

<h3 id="ref21-23">21.23</h3>

<p>If observations were received in the Office on or after the date of issue of the grant letter (<a href="/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-86">see 18.86</a>), then the third party must be informed that, since the observations we received after grant, they cannot be taken into consideration under s.21 but that they will be open to public inspection under s.118(1). It is not necessary to inform the third party if the observations have been sent to the dedicated section 21 email address (see <a href="#ref21-03">21.03</a>). A copy of the observations should be sent to the patentee for information. Any letter from the patentee commenting on the observations should also be placed on the open file. This procedure also applies, in the absence of any irregularity of office procedure, when observations are filed prior to grant but too late to prevent grant (<a href="/guidance/manual-of-patent-practice-mopp/section-18-substantive-examination-and-grant-or-refusal-of-patent/#ref18-88">see 18.88</a>).</p>

<p>[A suitably amended EL18B should be sent to the third party (unless the dedicated section 21 email address has been used) and EL23C should be sent to the applicant.]</p>

<h3 id="ref21-24">21.24</h3>

<p>Where such observations received too late to be acted upon by the examiner indicate that the invention may lack novelty by reason of a document forming part of the state of the art by virtue of s.2(3), then proceedings under s.73(1) may need to be initiated after grant (<a href="/guidance/manual-of-patent-practice-mopp/section-73-comptroller-s-power-to-revoke-patents-on-his-own-initiative/#ref73-02">see 73.02-73.03</a>). It is desirable that the patentee be warned of this possibility - this also applies if observations are filed before grant but too late to be considered, <a href="#ref21-20">see 21.20 and 21.21</a>.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 21(2)</strong></td>
    </tr>
    <tr>
      <td>It is hereby declared that a person does not become a party to any proceedings under this Act before the comptroller by reason only that he makes observations under this section.</td>
    </tr>
  </tbody>
</table>

<h3 id="ref21-25">21.25</h3>

<p>The receipt of the communication from the third party should be acknowledged (<a href="#ref21-03">see 21.03 and 21.04</a>, and, in a case where the observations have been received too late to be considered by the examiner (<a href="#ref21-21">see 21.21 and 21.23</a>, the third party should be informed of this fact (unless the dedicated section 21 email address has been used). Beyond this, the third party has no right to be kept informed of the progress of the application, or of the reason for action taken (or not taken) by the examiner. If third party attempts to discuss the matter directly with the examiner they should be told that, by virtue of s.21(2), they have no status in the proceedings.</p>

<h3 id="ref21-26">21.26</h3>

<p>A third party can find out whether their observations have been acted upon by consulting the open file. If they are not satisfied with any action taken they may always supplement their observations.</p>

</div>


        
  </div>

        </article>
      </div>
    </div>

    
<div class="gem-c-print-link govuk-!-display-none-print govuk-!-margin-top-3 govuk-!-margin-bottom-3">
    <button class="govuk-link govuk-body-s gem-c-print-link__button" data-module="print-link">Print this page</button>
</div>
  </div>

    </main>
