<main role="main" id="content" class="manual-section" lang="en">
      <span id="Top"/>
        <div id="manuals-frontend" class="manuals-frontend-body">
      

    <div class="govuk-grid-row">
      <div class="manual-body">
        <article aria-labelledby="section-title">
          <div class="govuk-grid-column-full">
            <h1 id="section-title" class="gem-c-heading govuk-heading-l   govuk-!-margin-bottom-4">
  Section 125A: Disclosure of invention by specification: availability of samples of biological material
</h1>
          </div>

            <div class="govuk-grid-column-two-thirds">
                <p class="gem-c-lead-paragraph">Sections (125A.01 - 125A.34) last updated October 2021.</p>

            </div>

          
  <div class="govuk-grid-column-two-thirds">
      
<div class="gem-c-govspeak govuk-govspeak " data-module="govspeak">
    <h3 id="a01">125A.01</h3>

<p>This section was introduced by the CDP Act, and has subsequently been amended by the Patents Regulations 2000 (SI 2000 No.2037). It sets out the provisions as to how an invention which involves the use of or concerns biological material may be disclosed and makes clear that ceasing to comply with the requirements on availability of samples of the biological material after a patent has been granted is a ground for revocation. S.125A has four subsections which essentially re-enact pre-CDP Act law and introduce a provision that access to samples of the biological material may be restricted to prescribed persons. The Patents Regulations 2000 substituted references to “microorganisms” in the first and second subsections of s.125A with references to “biological material” (see 25A.02.1) and also introduced into s.130(1) a definition of the term “biological material” -see 130.04.1. The Patents Act 1977 (Isle of Man) Order 2003 (SI 2003 No. 1249) amended these definitions for the Isle of Man.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 125A(1)</strong></td>
    </tr>
    <tr>
      <td>Provision may be made by rules prescribing the circumstances in which the specification of an application for a patent, or of a patent, for an invention which involves the use of or concerns biological material is to be treated as disclosing the invention in a manner which is clear enough and complete enough for the invention to be performed by a person skilled in the art.</td>
    </tr>
  </tbody>
</table>

<h3 id="a02">125A.02</h3>

<p>No action is necessary under this section if the biological material in question is available to the public at the date of filing of the application (and remains so throughout the life of the application and resultant patent) or can be described in the specification in such a manner as to enable the invention to be performed by a person skilled in the art. The section provides an alternative way of disclosing the invention in such a manner.</p>

<h3 id="a021">125A.02.1</h3>
<p>PR Sch 5, paras 7 &amp; 8, PR Sch 1, para 2 is also relevant</p>

<p>Subsection (1) enables rules to be made prescribing how such inventions may be disclosed. The relevant rules are contained in Schedule 1 to the Patents Rules 2007 which is given effect by r.13(1) of those Rules. (However, for patent applications filed before 27 July 2000, see paragraphs 7 and 8 of Schedule 5 to the Patents Rules 2007 for details of the provisions which apply). The Rules relating to biological material were amended by the Patents (Amendment) Rules 2001 (SI 2001 No.1412) to refer throughout to “biological material” instead of “micro-organisms”. The Patents (Amendment) Rules 2001 also made a number of other changes to the Schedule, most notably that it is now possible, under certain conditions, for a patent applicant to rely on biological material which has previously been deposited by another party -instead of having to deposit their own sample of the material see <a href="#ref125A-04">125A.04</a>.</p>

<h3 id="a03">125A.03</h3>

<p>The specification of an application relating to such an invention is, in relation to the biological material itself, treated as complying with s.14(3) if the first and second requirements set out in paragraph 3 of Schedule 1 to the Patents Rules 2007 are satisfied, and if the application as filed contains such relevant information as is available to the applicant on the characteristics of the biological material. Similarly, the disclosure in a patent specification is treated as clear enough and complete enough in this respect, and thus to not fall foul of s.72(1)(c) in this respect, if those requirements are satisfied. The requirements in question are described in 125A.04. The requirements for an application for a European patent (UK) or an international application for a patent (UK) are described in <a href="#ref125A-08">125A.08</a>. If a new deposit should prove necessary it should be made in accordance with paragraph 8 of Schedule 1 to the Patents Rules 2007, see <a href="#ref125A-24">125A.24-25</a>.</p>

<h3 id="ref125A-04">125A.04</h3>
<p>PR Sch 1, par 1 and par 3 is also relevant</p>

<p>The first requirement is that, on or before the date of filing of the application, the biological material has been deposited in a depository institution which is able to furnish a sample of the biological material. The second requirement is that, before the end of the period prescribed by paragraph 3(3) of Schedule 1 to the Patents Rules 2007<a href="#ref125A-06">see 125A.06</a>, the name of the depository institution and the accession number of the deposit are given in the specification of the application; and where the biological material has been deposited by a person other than the applicant, a statement is filed which identifies the name and address of the depositor and a statement by the depositor is filed authorising the applicant to refer to the biological material in their application and giving the depositor’s irrevocable consent to it being made available to the public. A “depository institution” means an institution which carries out the functions of receiving, accepting and storing biological material and the furnishing of samples of such biological material (whether generally or of a specific type); and conducts its affairs, in so far as they relate to the carrying out of those functions, in an objective and impartial manner.</p>

<h3 id="a05">125A.05</h3>

<p>It does not follow that failure to comply with this condition renders the specification not complete enough; the effect of this rule, which takes into account the Budapest Treaty on the International Recognition of the Deposit of Micro-organisms, to which the United Kingdom is a party, is to specify measures which will ensure that the part of the description of an invention which requires for its performance the use of biological material will be regarded as clear and complete. It is open for an applicant to argue that, despite the absence of a deposit, the specification gives sufficient directions to enable the invention to be performed. If however this argument is rejected by the Office (and, as with inventions in other areas of subject-matter, the examiner will raise objection under s.14(3) only in the clearest cases) or by the courts, it is not possible to rectify the situation; the deposit cannot be made after the date of filing the application.</p>

<h3 id="ref125A05-1">125A.05.1</h3>

<p>If an application relies on a biological deposit to enable the invention to be performed by the skilled person, and the deposit was made before the filing date but after the claimed priority date, then there will be no objection under s.14(3) as sufficiency is determined at the date of filing. However, the application will not be entitled to its priority date (for those aspects of the invention that rely on the deposit for their performance) and so may lack novelty and/or inventive step over documents published in the intervening period between the priority and filing dates. This was the conclusion of the Hearing Officer in <a rel="external" href="https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/050/11">Cellartis AB’s Application BL O/050/11</a>, as it was observed that the test for determining whether an invention is supported by matter disclosed in an earlier relevant application under s.5 is essentially the same as the test for determining sufficiency under s.14(3) – <a href="/guidance/manual-of-patent-practice-mopp/section-5-priority-date/#ref5-23">see 5.23</a>.</p>

<h3 id="ref125A-06">125A.06</h3>
<p>PR Sch 1,  para 3(3) is also relevant</p>

<p>The information identifying the deposit, as referred to in <a href="#ref125A-04">125A.04</a>, may be added to the application after the filing date provided that this is done</p>

<p>(a) before the end of the period of sixteen months after the declared priority date or, where there is no declared priority date, the date of filing of the application;</p>

<p>(b) where the applicant has made a request under section 16(1) to publish the application during the period prescribed for the purposes of that section, on or before the date of that request; or</p>

<p>(c) where the applicant was notified under rule 52(2) that, in accordance with section 118(4), the comptroller has received a request by any person for information and inspection of documents under section 118(1), within one month of the date of the notification,</p>

<p>whichever is the earliest. This period may be extended in accordance with r.108(2) or (3) (and in accordance with r.108(4)-(6)), <a href="/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-34">see 123.34-41</a>. In any such case the front page of the application as published under s.16 should carry the following notice (<a href="/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29">see 16.29</a>): “The information required by Schedule 1 to the Patents Rules 2007 paragraph 3(2)(a) or 3(2)(b) was not contained in the application as filed, but was supplied later in accordance with paragraph 3(3) of that Schedule.” This notice should be amended to avoid ambiguity when more than one biological material is disclosed. The relevant information should be reproduced on the back of the front page of the published application.</p>

<p>[ The case examiner should give instructions via a dossier minute and PDAX message to the appropriate formalities group to arrange for the information to be published on the back of the front page of the ‘A’ document. Those instructions should specify the exact wording to be used, embodying the information provided by the applicant preceded by a heading “Information required by paragraph 3(2)(a) or 3(2)(b) of Schedule 1 to the Patents Rules 2007”. If the standard text for the notice for the front page needs to be amended to avoid ambiguity where more than one biological material is disclosed, it is necessary for the case examiner to make this clear in the minute or to enter a suitable non-standard text on the “Notices for front page of ‘A’ document” form instead. ]</p>

<h3 id="ref125A-07">125A.07</h3>

<p>Following amendment by the Patents (Amendment) Rules 2001, there is no longer a requirement to supply the date when the biological material was deposited at the depositary institution. The date of making the deposit is readily ascertainable from the name of the depository institution and the accession number. There is also no longer a requirement to mention any international agreement (eg the Budapest Treaty) under which the biological material is deposited.</p>

<h3 id="ref125A-08">125A.08</h3>
<p>PR Sch 1, para 3(4) is also relevant</p>

<p>In the case of an application for a European patent (UK) which has been filed under the provisions of the EPC which correspond to the second requirement set out in paragraph 3(2) of Schedule 1 to the Patents Rules 2007 (ie EPC r.31) or an international application for a patent (UK) which has been filed under the corresponding provisions of the PCT (ie PCT r.13bis.3), the second requirement should be treated as having been met.</p>

<h3 id="a09">125A.09</h3>

<p>[Deleted]</p>

<h3 id="a10">125A.10</h3>

<p>If not all of the requirements of paragraph 3 of Schedule 1 to the Patents Rules 2007 have been complied with, the examiner must form an opinion as to whether or not the biological material is available to the public. There are several possibilities. The biological material may be known to be readily available to those skilled in the art, eg a micro-organism such as baker’s yeast or Bacillus natto which is commercially available; or it may be a standard preserved strain, or other micro-organism which the examiner knows to have been preserved in a culture collection and to be available to the public. Alternatively the applicant may have given in the description sufficient information as to the identifying characteristics of the biological material and as to the prior availability in a culture collection. In any of these cases no further action is called for. If however the applicant has given no information, or insufficient information, on public availability (and the biological material is of a particular type not falling within the known categories such as those already mentioned) then the examiner must assume that the material is not available to the public. They must also examine whether the biological material is described in such a manner as to enable the invention to be carried out by a person skilled in the art.</p>

<h3 id="a11">125A.11</h3>

<p>If the biological material is not available to the public and if it is not described in the application in such a manner as to enable the invention to be carried out by a person skilled in the art, and paragraph 3 of Schedule 1 to the Patents Rules 2007 has not been adequately complied with, then objection under s.14(3) will be necessary. The view is taken that it is virtually impossible to comply with s.14(3) only by deposit of biological material in accordance with paragraph 3 where the invention concerns a new species or higher classification of micro-organisms. Consequently broad claims to a new species or higher classification of micro-organism should not normally be allowed.</p>

<p>[ If it is considered that a specification containing an invention claimed in such broad terms can meet the requirements of s.14(3) by other means the case should be referred to the Divisional Director with a brief note of the circumstances. ]</p>

<h3 id="a12">125A.12</h3>

<p>Until such matters are tested in the courts, no guidance can be given as to what extent deposit will be necessary in other circumstances, eg when claims are directed to a new micro-organism produced by genetic manipulation from a known micro-organism, whether a deposit of the new micro-organism will be necessary. It could be argued that in this case the new micro-organism (which is the product of the process disclosed) is not necessary for the performance of the invention. The prudent applicant will in general resolve any doubts as to whether a deposit is needed in favour of making the deposit.</p>

<h3 id="a13">125A.13</h3>

<p>So long as paragraph 3 of Schedule 1 to the Patents Rules 2007 is satisfied in respect of a deposited strain, then claims may be allowed to the deposited strain and to mutant or variant strains derived therefrom provided that those mutants or variants produce the desired product (eg antibiotic) identified in the specification. If specific methods of producing such mutants or variants are not given in the description, it is considered that the courts will construe such claims as being restricted to mutants or variants produced by standard or conventional methods well known to those working in the micro-organism field and thus as being unobjectionable. If however the claims are restricted to a single strain of a micro-organism then deposition of a sample and disclosure of the species name of the micro-organism can be sufficient to meet the requirements of paragraph 3.</p>

<h3 id="a14">125A.14</h3>

<p>Where the obtaining of novel biological material, e.g. a novel microorganism, depends on a random event with little likelihood of repetition the requirements of s.14(3) are not regarded as satisfied unless a deposit has been made. In particular, since a cell line is dependent for its origin on the random selection of a cell, a deposit will be necessary when an invention requires a cell line for its performance.</p>

<h3 id="a15">125A.15</h3>

<p>Compliance with paragraph 3 of Schedule 1 to the Patents Rules 2007 ensures that the disclosure meets the requirements of s.14(3) only to the extent that the invention requires the otherwise unavailable biological material for its performance, and there are other ways in which the disclosure in a specification relating to biological material can be deficient. For example when the claims are directed to the production of a new micro-organism (from or using available micro-organisms), a description as to how the new micro-organism has been obtained will be necessary to satisfy s.14(3), even if a deposit of the new micro-organism has been made.</p>

<h3 id="a16">125A.16</h3>

<p>The deposit of biological material may be made in any depository institution, anywhere in the world. The depository institution need not have International Depository Authority (IDA) status. (A deposit in an IDA is recognised for the purposes of patent procedure in all Contracting States of the Budapest Treaty and in the EPO). It is the applicant’s responsibility to satisfy themselves that a particular biological material will be accepted by a collection which they have selected and that samples will be made available in accordance with Schedule 1 to the Patents Rules 2007.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 125A(2)</strong></td>
    </tr>
    <tr>
      <td>The rules may in particular require the applicant or patentee (a) to take such steps as may be prescribed for the purposes of making available to the public samples of the biological material, and (b) not to impose or maintain restrictions on the uses to which such samples may be put, except as may be prescribed.</td>
    </tr>
  </tbody>
</table>

<h3 id="a17">125A.17</h3>

<p>Rules made under this section may prescribe how samples of the biological material shall be made available to the public. The giving of the information identifying the deposit, as referred to in <a href="#ref125A-04">125A.04</a>, is interpreted as providing the applicant’s consent to the release of a sample by the depository institution on receipt of the comptroller’s certificate authorising such release to the person who is named therein and who makes a valid request to the institution.</p>

<h3 id="ref125A-18">125A.18</h3>
<p>PR Sch 1, paras 4 and 7 is also relevant</p>

<p>In order to obtain the comptroller’s certificate, Patents Form 8 (Request for a certificate authorising the release of a sample of biological material) should  be submitted to the Office. After the publication of an application for a patent, Form 8 may be filed by any person desiring a sample; before such publication, it should only be filed by a person who has been notified in accordance with section 118(4) see <a href="#ref125A-06">125A.06(c)</a>. If the patent applicant has given notice to the comptroller on Patents Form 8A of his intention that a sample of the biological material should be made available only to an expert, see <a href="#ref:125A-26">125A.26-33</a>; otherwise, the following procedure should be used.</p>

<h3 id="ref125A-19">125A.19</h3>
<p>PR Sch 1 para 4(5) is also relevant</p>

<p>If the depository institution in which the material has been deposited is an international depository authority (in accordance with Article 7 of the Budapest Treaty), the form provided for by the Regulations under that Treaty (BP12) should be filed with Form 8. Blank Forms BP12 are obtainable from the Office.</p>

<h3 id="ref125A-20">125A.20</h3>
<p>PR Sch 1, para 5(1) is also relevant</p>

<p>Form 8 includes an undertaking by the person making the request,</p>

<p>(a) not to make the biological material, or any material derived from it, available to any other person; and</p>

<p>(b) not to use the biological material, or any material derived from it, otherwise than for experimental purposes relating to the subject matter of the invention.</p>

<h3 id="p125a21">P125A.21</h3>
<p>PR Sch 1 paras 5(2) to 5(5) is also relevant</p>

<p>Both parts (a) and (b) of the undertaking apply for as long as the application or any resultant patent is extant but the undertakings shall cease to have when the application is terminated or withdrawn or when the patent ceases to have effect. These periods include any extension allowed under r.107 or 108 (but not any period prior to reinstatement under either rule). For the purpose of any act of Crown use specified in s.55 in relation to the biological material, such an undertaking is not required of, or is ineffective if given by, any government department or person suitably authorised thereby. The undertaking may be varied by way of derogation by agreement between the parties; and the undertaking is ineffective to the extent necessary for effect to be given to any licence of right or compulsory licence under the patent in question.</p>

<h3 id="a22">125A.22</h3>
<p>PR Sch 1 para 4(6) is also relevant</p>

<p>The Office sends a copy of Form 8 (and BP12, if filed) and of the comptroller’s certificate authorising the release of the sample to the patent applicant or proprietor, the depository institution and the person making the request.</p>

<h3 id="a23">125A.23</h3>

<p>If the patent applicant or proprietor and the depository institution are agreeable to the release of a sample merely upon the request of a third party, there is no need for Form 8 to be filed or the comptroller’s certificate to be issued.</p>

<h3 id="ref125A-24">125A.24</h3>
<p>PR Sch 1 is also relevant</p>

<p>Where a deposit (either original or a replacement) of biological material has been made at a depository institution, and either</p>

<p>(a) the biological material ceases to be available from the institution because the biological material is no longer viable;</p>

<p>(b) the depository institution is, for any other reason, unable to supply the biological material;</p>

<p>(c) the place where the biological material is deposited is no longer a depositary institution for that type of material (whether temporarily or permanently); or</p>

<p>(d) the biological material is transferred to a different depositary institution;</p>

<p>PR Sch 1 is also relevant</p>

<p>the first and second requirements described in <a href="#ref125A-04">125A.04</a> are treated as having been complied with if, by the end of the period of three months after the date on which the depositor is notified by the depositary institution that (a), (b), (c) or (d) occurred or, where it expires later, three months after the date on which that circumstance is advertised in the journal:</p>

<p>(a) where (a), (b) or (c) above occurs:</p>

<p>(i) a new deposit of biological material is made at the relevant depositary, and</p>

<p>(ii) that deposit is accompanied by a statement, signed by the person making the deposit, that the biological material deposited is the same as that originally deposited; and</p>

<p>(b) in all circumstances set out above, the applicant or proprietor applies to the comptroller to amend the specification of the application for the patent, or the patent, so that it meets the second requirement.</p>

<p>Where the biological material is no longer viable, the new deposit should be made with the same depository institution as the original deposit. In any other case, it may be made with another depositary institution.</p>

<h3 id="a25">125A.25</h3>
<p>PR Sch 1, para 8 is also relevant</p>

<p>No interruption is deemed to have occurred in the availability of the deposit if the actions described in <a href="#ref125A-24">125A.24</a> are carried out within three months of the date on which the depositor was notified of the interruption by the depositary institution or, where it expires later, three months after the date on which that circumstance is advertised in the journal.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 125A(3)</strong></td>
    </tr>
    <tr>
      <td>The rules may provide that, in such cases as may be prescribed, samples need only be made available to such persons or descriptions of persons as may be prescribed; and the rules may identify a description of persons by reference to whether the comptroller has given his certificate as to any matter.</td>
    </tr>
  </tbody>
</table>

<h3 id="ref:125A-26">125A.26</h3>

<p>The rules may qualify the public availability requirements by allowing samples to be made available only to prescribed persons or descriptions of persons. Thus, under paragraph 6 of Schedule 1 to the 2007 Rules, the patent applicant can choose to restrict the release of samples to experts, until the patent is granted or until 20 years from the filing date if the patent is never granted.</p>

<h3 id="a27">125A.27</h3>
<p>PR Sch 1, para 6(2) PR Sch 1, para 6(4) is also relevant</p>

<p>To take advantage of this provision, the applicant must give notice on Patents Form 8A before the preparations for s.16 publication of the application have been and completed. The formalities examiner should then arrange for a standard footnote to be published on the front page of the published application (<a href="/guidance/manual-of-patent-practice-mopp/section-16-publication-of-application/#ref16-29">see 16.29</a>) to state that such notice has been given.</p>

<h3 id="a28">125A.28</h3>
<p>PR Sch 1, para 6(5) PR Sch 1, para 6(6) is also relevant</p>

<p>It is then not possible to obtain the comptroller’s certificate authorising release of a sample as described in <a href="#ref125A-18">125A.18-22</a> until the patent is granted, or until 20 years from the date on which the application was filed, if the application is, terminated or withdrawn. The following procedure must instead be followed. (This restriction to experts is however inapplicable to Crown use by a government department or any person authorised in writing by a government department, under s.55.)</p>

<h3 id="a29">125A.29</h3>
<p>PR Sch 1 para 7(1), 7(2) and 5(1) is also relevant</p>

<p>Any third party wishing to have a sample made available has to, apply on Patents Form 8 (in duplicate) nominating a particular expert. This should be and accompanied by the form provided for by the Regulations under the Budapest Treaty (BP12) if the depository institution is an international depository authority, as described in <a href="#ref125A-19">125A.19</a>. The undertaking as in <a href="#ref125A-20">125A.20-21</a> must be signed by the nominated expert.</p>

<h3 id="a30">125A.30</h3>
<p>PR Sch 1, paras 7(3) and 7(4) is also relevant</p>

<p>The Office sends a copy of Form 8 to the applicant for the patent, giving them a period of one month (which may be extended by a further month under r.108(1) and if an adequate reason is given) in which to object to a sample being made available to the expert.</p>

<p>[ The decision whether to extend the period for objection should be taken by the appropriate divisional Head of Admin. ]</p>

<h3 id="ref125A-31">125A.31</h3>
<p>PR Sch 1, para 7(6) is also relevant</p>

<p>If the applicant does not object, the Office sends a copy of Form 8 (and BP12, if filed) and of the comptroller’s certificate authorising the release of the sample to the applicant for the patent, the depository institution where the sample of the biological material is stored, the person making the request and the expert.</p>

<h3 id="a32">125A.32</h3>
<p>PR Sch 1, para 7(4) r.73(1) PR part 7 is also relevant</p>

<p>Where the applicant does object, the comptroller has to determine the matter, having given the applicant and the third party making the request the opportunity of being heard. The applicant should notify their objection by filing (in duplicate) Form 2 and a statement of grounds. This starts proceedings before the comptroller, which give an opportunity for the filing of a counter-statement by the third party and if necessary evidence from the applicant and the third party before the matter is heard; the procedure is discussed at <a href="/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05">123.05 – 123.05.13</a>. Evidence may be filed as to the knowledge, experience, independence and technical qualifications of the expert. If the comptroller decides to authorise the release of the sample to the expert, action is taken as in <a href="#ref125A-31">125A.31</a>.</p>

<h3 id="a33">125A.33</h3>

<p>If the comptroller decides not to issue their certificate in favour of the expert, the third party may nominate another person as the expert and (subject to such directions as the comptroller thinks fit) the above procedure may be repeated until an expert is accepted.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 125A(4)</strong></td>
    </tr>
    <tr>
      <td>An application for revocation of the patent under section 72(1)(c) above may be made if any of the requirements of the rules cease to be complied with.</td>
    </tr>
  </tbody>
</table>

<h3 id="a34">125A.34</h3>

<p>Ceasing to comply with the requirements of rules made under this section, eg by ceasing to make samples accessible to the public, is a ground for revocation of a patent. Compliance with the provisions of Schedule 1 to the 2007 Rules, so that the patent discloses the invention clearly enough and completely enough for it to be performed by a person skilled in the art, is necessary.</p>

</div>


        
  </div>

        </article>
      </div>
    </div>

    
<div class="gem-c-print-link govuk-!-display-none-print govuk-!-margin-top-3 govuk-!-margin-bottom-3">
    <button class="govuk-link govuk-body-s gem-c-print-link__button" data-module="print-link">Print this page</button>
</div>
  </div>

    </main>
