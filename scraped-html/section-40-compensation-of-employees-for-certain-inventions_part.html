<main role="main" id="content" class="manual-section" lang="en">
      <span id="Top"/>
        <div id="manuals-frontend" class="manuals-frontend-body">
      

    <div class="govuk-grid-row">
      <div class="manual-body">
        <article aria-labelledby="section-title">
          <div class="govuk-grid-column-full">
            <h1 id="section-title" class="gem-c-heading govuk-heading-l   govuk-!-margin-bottom-4">
  Section 40: Compensation of employees for certain inventions 
</h1>
          </div>

            <div class="govuk-grid-column-two-thirds">
                <p class="gem-c-lead-paragraph">Sections (40.01 - 40.20) last updated: July 2021.</p>

            </div>

          
  <div class="govuk-grid-column-two-thirds">
      
<div class="gem-c-govspeak govuk-govspeak " data-module="govspeak">
    <h3 id="section">40.01</h3>

<p>This is the second of the group of sections relating to inventions made by employees. It provides for the court or the comptroller to award compensation to be paid by an employer to an employee in respect of an invention made by the employee, in certain circumstances. The procedure for an employee to apply for compensation to the Patents Court or the comptroller, respectively, is prescribed by rule 63.12 of Part 63 of the Civil  Procedure Rules (CPR 63) and by rule 59 of the Patents Rules 1995.</p>

<h3 id="ref40-02">40.02</h3>

<p>PA 2004 s.10(8) is also relevant.</p>

<p>The concept of an employee having a statutory right to such compensation, for their invention from which their employer has derived benefit did not exist in UK law prior to the Patents Act 1977. This section was amended by the Patents Act 2004 to allow compensation to be awarded in respect of all outstanding benefits deriving from a patented invention, removing the requirement for an employee to show that the patent itself is of outstanding benefit. The amended legislation only applies to inventions for which a patent application is filed from 1 January 2005 onwards; for patent applications made prior to this date, employee compensation can only be obtained from benefits deriving from the patent (<a href="#ref40-04">see 40.04-40.04.1</a>).</p>

<h3 id="section-1">40.03</h3>

<p>For the applicability of s.40 and interpretation of certain terms used therein, <a href="/guidance/manual-of-patent-practice-mopp/section-39-right-to-employees-inventions/#ref39-01">see 39.01-04</a>.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 40(1)</strong></td>
    </tr>
    <tr>
      <td>Where it appears to the court or the comptroller on an application made by an employee within the prescribed period that<br/>(a) the employee has made an invention belonging to the employer for which a patent has been granted,<br/>(b) having regard among other things to the size and nature of the employer’s undertaking, the invention or the patent for it (or the combination of both) is of outstanding benefit to the employer, and <br/>(c) by reason of those facts it is just that the employee should be awarded compensation to be paid by the employer, the court or the comptroller may award him such compensation of an amount determined under section 41 below.</td>
    </tr>
  </tbody>
</table>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 40(2)</strong></td>
    </tr>
    <tr>
      <td>Where it appears to the court or the comptroller on an application made by an employee within the prescribed period that <br/>(a) a patent has been granted for an invention made by and belonging to the employee;<br/>(b) his rights in the invention, or in any patent or application for a patent for the invention, have since the appointed day been assigned to the employer or an exclusive licence under the patent or application has since the appointed day been granted to the employer;<br/>(c) the benefit derived by the employee from the contract of assignment, assignation or grant or any ancillary contract (“the relevant contract”) is inadequate in relation to the benefit derived by the employer from the invention or the patent for it (or both); and <br/>(d) by reason of those facts it is just that the employee should be awarded compensation to be paid by the employer in addition to the benefit derived from the relevant contract; <br/>the court or the comptroller may award him such compensation of an amount determined under section 41 below.</td>
    </tr>
  </tbody>
</table>

<h3 id="ref40-03-1">40.03.1</h3>

<p>An application under s.40 can relate to a foreign patent, and to more than one patent, provided that any such patent is identified in the application on Form 2, <a href="#ref40-09">see 40.09</a> (GEC Avionics Ltd’s Patent [1992] RPC 107 and British Steel PLC’s Patent [1992] RPC 117).</p>

<h3 id="ref40-03-2">40.03.2</h3>

<p>In Fellerman’s Application (<a href="https://www.gov.uk/government/publications/patent-decision-o01196">BL O/11/96</a>) the hearing officer observed that s.40 requires the applicant for an award of compensation to apply to the comptroller (or court) with reference to the employer, not the current proprietor of the patent. Similarly, in Price v Elf Print Media Ltd (Patents Court, 1 February 2001, unreported, and [2001] EWCA Civ 622), an employee failed in making a claim for compensation against two former directors of his employer company, to whom the patent had been assigned by the company, since the former directors were not his employer.</p>

<h3 id="ref40-04">40.04</h3>

<p>s.43(7), s.40(7) is also relevant.</p>

<p>Compensation (of an amount determined under s.41, see 41.03 to 41.06) to be paid by the employer may be awarded to the employee, under s.40, in either of two sets of circumstances (unless s.40(3) applies, see 40.16). Common to both sets is that the invention in question must have been made by the employee, a patent must have been granted for it (see 39.04), and the employer must have been derived benefit (i.e. benefit in
money or money’s worth) from the invention or the patent for it. However, for patent applications made before 1 January 2005, the benefit must derive from the patent itself (<a href="#ref40-04-01">see 40.04.1</a>). In Kelly &amp; Anor v GE Healthcare Ltd [2009] EWHC 181 (Pat), [2009] RPC 12, Floyd J stated that only actual inventors (i.e. “the natural person[s] who came up with the inventive concept”), and not those who merely contributed to the invention, can be compensated. The circumstances of subsection (1) must be met if the invention belongs to the employer (see below) but those of subsection (2) must be met if it belongs to the employee (<a href="#ref40-06">see 40.06</a>).</p>

<h3 id="ref40-04-1">40.04.1</h3>

<p>The relationship between benefit derived from the invention and benefit derived from the patent per se was considered (under the pre-2004 Act law) by the Patents Court in Memco-Med Ltd’s Patent [1992] RPC 403. It was decided that the onus of proof that the benefit was derived from the patent rather than the invention lay on the employee but may thereafter shift to the employer depending on the evidence. Aldous J acknowledged that while the benefit from a patent may be readily recognisable when the patent is licensed and royalties paid, it is more difficult to determine in cases (a) where the employer exploits the patent by manufacturing articles in accordance with the invention of the patent or (b) where the patent is not licensed and the invention is never put into practice but the patent is nevertheless of great benefit in preventing activities which would compete with those of the patentee. In answering the question whether the patent has been of benefit to the employer, Aldous J indicated that it could be useful to assume that the patent was never granted due to some failure by the Patent Agents and thereafter to decide what would have been the position of the employer. It would then be possible to ascertain the benefit from the patent by comparing the actual position of the employer with the position they would have been in if the patent had not been granted bearing in mind the benefit must be in money or money’s worth.</p>

<h3 id="ref40-04-2">40.04.2</h3>

<p>Under s.40(1), the court or comptroller may award compensation only if it appears that the invention or the patent for it is, considering inter alia the size and nature of the employer’s undertaking, of outstanding benefit to the employer and an award is therefore “just”. In Memco-Med Ltd’s Patent (<a href="#ref40-04-1">see 40.04.1</a>) Aldous J affirmed the views of the hearing officers in GEC Avionics’ Patent and British Steel PLC’s Patent (<a href="#ref40-03-1">see 40.03.1</a>) that for benefit to be “outstanding”, it must be something out of the ordinary when looked at in the total context of the activities of the employer concerned and not something that one would normally expect to arise from the duties that the employee is paid for. Thus to assess whether the benefit is outstanding, it is necessary to look at the employer’s undertaking, which may be the whole or a division of the employer’s business, and ascertain the benefit to the employer taking into account the size and nature of that business and all the surrounding circumstances. In Ian Alexander Shanks v Unilever Plc, Unilever NV and Unilever UK Central Resources Limited <a rel="external" href="https://www.ipo.gov.uk/pro-types/pro-patent/pro-p-os/o25913.pdf">BLO/259/13 (PDF, 371KB)</a> the hearing officer found that the benefit due to the patents was around £24 million over a number of years. He considered this benefit in light of the defendant’s profits and turnover, the defendant’s other licensing and patent activities and the defendant’s activities in general. Considering the totality of the evidence the hearing officer held that the benefits arising from the patents fell short of being outstanding. The hearing officer’s decision was upheld by the Patents Court in <a rel="external" href="http://www.bailii.org/cgi-bin/markup.cgi?doc=/ew/cases/EWHC/Patents/2014/1647.html&amp;query=shanks+and+unilever&amp;method=boolean">Shanks v Unilever Plc &amp; Ors [2014] EWHC 1647 (Pat)</a> and by the <a rel="external" href="http://www.bailii.org/ew/cases/EWCA/Civ/2017/2.html">Court of Appeal in [2017] EWCA Civ 2</a>.</p>

<p>However, at the Supreme Court (Shanks v Unilever [2019] UKSC 45), the court considered the same question and overturned the lower courts. They held that “the focus of the inquiry into whether any one of those patents is of outstanding benefit to the company must be the extent of the benefit of that patent to the group [meaning Unilever Group] and how that compares with the benefits derived by the group from other patents for inventions arising from the research carried out by that company [meaning CRL, a wholly owned subsidiary of the Unilever Group]”.</p>

<h3 id="ref40-04-3">40.04.3</h3>

<p>The need to consider the employer’s undertaking when assessing whether a patent is of outstanding benefit was also demonstrated in Kelly &amp; Anor v GE Healthcare Ltd [2009] EWHC 181 (Pat), [2009] RPC 12, the first UK judgment to award compensation to employees under s.40. In this case, Floyd J held (under the pre-2005 form of s.40) that the patents in question were of outstanding benefit to the company, having regard to all of the circumstances, including the size and nature of the employer’s undertaking. The benefits went far beyond anything which one could normally expect to arise from the sort of work the employees were doing. In particular, the benefit of patent protection is not limited to profits from sales. In this case, as well as protecting the business against generic competition, the patents were a major factor in achieving corporate deals. Floyd J went on to hold that it was just that the employees should receive an award of compensation, the fact that the employees had waited for some years before claiming being considered to be irrelevant. In this case, the fact that the patents had expired enabled the court to quantify the benefit those patents had brought to the employer. This is in contrast with previous cases under s.40, brought prior to expiry of the patent, wherein the courts have been unwilling to speculate on future benefits those patents might bring.</p>

<h3 id="ref40-05">40.05</h3>

<p>In Memco-Med’s Patent (<a href="#ref40-04-01">see 40.04.1</a>) Aldous J upheld the hearing officer’s earlier decision not to order discovery of documents relating to Memco-Med’s sales and profits partly because the applicant had not established that the patent had played a major part in securing the sales obtained and partly since a request for discovery should be made promptly, holding that discovery should only be ordered when necessary and should be limited to those issues which on the pleadings are essential for a decision. In Communication &amp; Control Engineering Company Limited’s Patent 2115226 (<a href="https://www.gov.uk/government/publications/patent-decision-o08293">BL O/82/93</a>), the hearing officer followed Memco-Med when refusing a request for discovery not only on the grounds of lack of sufficient clarity and precision in the request but also, and more fundamentally, on the grounds that it was not in his judgment necessary for disposing of the matter, in the sense that it effectively precluded a making good of any fault at the price of further delay.</p>

<h3 id="ref40-06">40.06</h3>

<p>s.40(4) is also relevant.</p>

<p>Under s.40(2), the court or comptroller may award compensation if it appears to be “just” because the benefit derived by the employee (from a contract whereby they assigned their rights, or granted an exclusive licence, to the employer) is inadequate in relation to the benefit derived by the employer. This applies notwithstanding anything to the contrary in the contract (or in any agreement applicable to the invention other than a relevant
collective agreement as defined in s.40(3) and (6)).</p>

<h3 id="ref40-07">40.07</h3>

<p>s.43(5), s.43(6) is also relevant.</p>

<p>Where the employer dies before any award is made under s.40, the benefit or expected benefit to the employer includes that to the employer’s personal representatives or any person in whom the relevant patent was vested by their assent. Where the employee dies before any such award is made, the employee’s personal representatives or their successors in title may exercise the employee’s right to make or pursue an application for compensation.</p>

<h3 id="section-2">40.08</h3>

<p>Section 41(7) to (12) provide for certain actions to be taken after an application for compensation under s.40 has been determined, <a href="/guidance/manual-of-patent-practice-mopp/section-41-amount-of-compensation/#ref41-07">see 41.07 to 41.11</a>.</p>

<h3 id="procedure">Procedure</h3>

<h3 id="application-to-the-comptroller">Application to the comptroller</h3>

<p>PR part 7, s.40(5), r.51(3)(a) is also relevant.</p>

<h3 id="ref40-09">40.09</h3>

<p>PR part 7, s.40(5), r.51(3)(a) is also relevant.</p>

<p>An application to the comptroller under s.40 for an award of compensation should be made by the employee on Patents Form 2 accompanied by a copy thereof and a statement of grounds in duplicate. This starts proceedings before the comptroller, the procedure for which is discussed at <a href="/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05">123.05 – 123.05.13</a> The comptroller may decline to deal with the application, <a href="#ref40-17">see 40.17</a>. Documents filed at the Office in connection with the application are not open to public inspection unless the comptroller otherwise directs.</p>

<h3 id="ref40-10">40.10</h3>
<p>r.91, CPR 63.12 is also relevant.</p>

<p>The application should be made within the period which begins when the relevant patent is granted and which expires one year after it has ceased to have effect. However, where the patent has ceased to have effect by reason of a failure to pay any renewal fee in time and an application for restoration is made to the comptroller under s.28, the period­ (a) if restoration is continuously in effect; or ordered, continues as if the patent had remained (b) if restoration is refused, is treated as expiring one year after the patent ceased to have effect or six months after the refusal, whichever is the later.</p>

<p>r.108(1) is also relevant.</p>

<p>The period is extensible at the discretion of the comptroller.</p>

<h3 id="section-3">40.11</h3>

<p>[deleted]</p>

<h3 id="section-4">40.12</h3>

<p>[deleted]</p>

<h3 id="section-5">40.13</h3>

<p>[deleted]</p>

<h4 id="application-to-the-court">Application to the court</h4>

<h3 id="section-6">40.14</h3>

<p>CPR 63.12, s.130(1) is also relevant.</p>

<p>An application to the Patents Court under s.40 should be made by issue of a claim form within the period specified in <a href="#ref40-10">40.10</a>. An application to the court in the UK other than England or Wales should be made to the High Court of that part of the UK or, in Scotland, to the Court of Session. The subsequent procedure is in general outside the scope of this Manual.</p>

<h3 id="section-7">40.15</h3>
<p>s.97(1) is also relevant</p>

<p>It is also possible to appeal to the court against a decision of the comptroller under s.40. The award of costs in proceedings before the court under s.40, whether on an application or on appeal, is subject to s.106 (<a href="/guidance/manual-of-patent-practice-mopp/section-106-costs-and-expenses-in-proceedings-before-the-court/#ref106-01">see 106.01-06</a>). This requires inter alia that the court should have regard to the financial position of the employer and employee in determining whether to make an award of costs and the amount thereof.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 40(3)</strong></td>
    </tr>
    <tr>
      <td>Subsections (1) and (2) above shall not apply to the invention of an employee where a relevant collective agreement provides for the payment of compensation in respect of inventions of the same description as that invention to employees of the same description as that employee.</td>
    </tr>
  </tbody>
</table>

<h3 id="collective-agreements-regarding-compensation">Collective agreements regarding compensation</h3>

<h3 id="section-8">40.16</h3>

<p>An employee who has made an invention cannot take advantage of the provisions of s.40(1) and (2) if they and their employer are covered by a relevant collective agreement (as defined in s.40(6)) regarding the payment of compensation in respect of such inventions. The agreement must relate to inventions of the same description as that in question and employees of the same description as the employee who made the invention in question. The employee’s rights to any compensation are then governed by the agreement instead of by those provisions.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 40(4)</strong></td>
    </tr>
    <tr>
      <td>Subsection (2) above shall have effect notwithstanding anything in the relevant contract or any agreement applicable to the invention (other than any such collective agreement).</td>
    </tr>
  </tbody>
</table>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 40(5)</strong></td>
    </tr>
    <tr>
      <td>If it appears to the comptroller on an application under this section that the application involves matters which would more properly be determined by the court, he may decline to deal with it.</td>
    </tr>
  </tbody>
</table>

<h3 id="comptroller-declines-to-deal-with-application">Comptroller declines to deal with application</h3>

<h3 id="ref40-17">40.17</h3>

<p>The comptroller has discretion to decline to deal with an application under s.40 (or one under s.41(8), <a href="/guidance/manual-of-patent-practice-mopp/section-41-amount-of-compensation/#ref41-08">see 41.08-10</a> if it appears to them that it involves matters which would more properly be determined by the court. The observations in 8.29 on the exercise of such discretion to decline to deal with questions of entitlement appear to apply mutatis mutandis here.</p>

<h3 id="section-9">40.18</h3>

<p>CPR 63.11 is also relevant.</p>

<p>In such a case any person entitled to do may, within 14 days after the comptroller’s decision, apply to the court by originating summons to determine the application.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 40(6)</strong></td>
    </tr>
    <tr>
      <td>In this section <br/>“the prescribed period”, in relation to proceedings before the court, means the period prescribed by rules of court, and <br/>“relevant collective agreement” means a collective agreement within the meaning of the Trade Union and Labour Relations (Consolidation) Act 1992, made by or on behalf of a trade union to which the employee belongs, and by the employer or an employers’ association to which the employer belongs which is in force at the time of the making of the invention.</td>
    </tr>
  </tbody>
</table>

<h3 id="section-10">40.19</h3>

<p>The definition of “relevant collective agreement” was amended by s.300(2), Sch 2, para 9 of the Trade Union and Labour Relations (Consolidation) Act 1992 to include a reference to this Act.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 40(7)</strong></td>
    </tr>
    <tr>
      <td>References in this section to an invention belonging to an employer or employee are references to it belonging as between the employer and the employee.</td>
    </tr>
  </tbody>
</table>

<h3 id="invention-belonging-to-employer-or-employee">Invention belonging to employer or employee</h3>

<h3 id="section-11">40.20</h3>

<p>The references in s.40(1) and (2) to an invention belonging to an employer or employee relate to questions of ownership by one or the other in accordance with s.39. Ownership jointly with a third party or parties is not precluded.</p>

</div>


        
  </div>

        </article>
      </div>
    </div>

    
<div class="gem-c-print-link govuk-!-display-none-print govuk-!-margin-top-3 govuk-!-margin-bottom-3">
    <button class="govuk-link govuk-body-s gem-c-print-link__button" data-module="print-link">Print this page</button>
</div>
  </div>

    </main>
