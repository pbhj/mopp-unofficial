<main role="main" id="content" class="manual-section" lang="en">
      <span id="Top"/>
        <div id="manuals-frontend" class="manuals-frontend-body">
      

    <div class="govuk-grid-row">
      <div class="manual-body">
        <article aria-labelledby="section-title">
          <div class="govuk-grid-column-full">
            <h1 id="section-title" class="gem-c-heading govuk-heading-l   govuk-!-margin-bottom-4">
  Section 8: Determination before grant of questions about entitlement to patents etc 
</h1>
          </div>

            <div class="govuk-grid-column-two-thirds">
                <p class="gem-c-lead-paragraph">Sections (8.01 - 8.31) last updated: January 2024.</p>

            </div>

          
  <div class="govuk-grid-column-two-thirds">
      
<div class="gem-c-govspeak govuk-govspeak " data-module="govspeak">
    <h3 id="section">8.01</h3>

<p>Questions about entitlement to patents may be referred to the comptroller under s.8 (patents under the 1977 Act which have not yet been granted), s.12 (patents under foreign or international law which have not yet been granted) or s.37 (granted patents under the 1977 Act). Such entitlement is a matter of property in a patent or application and is distinct from the right under s.13 to be mentioned as an “inventor” as defined in s.7(3). The comptroller is the primary jurisdiction for entitlement disputes.</p>

<h3 id="ref8-02">8.02</h3>

<p>A granted European patent (UK) is treated for the purposes of these sections as if it were a patent under the 1977 Act resulting from an application made under the 1977 Act, so that questions relating thereto may be referred under s.37. Questions arising before the grant of a European patent are however dealt with in accordance with s.82, by virtue of which s.12 is to some extent applicable.</p>

<h3 id="section-1">8.03</h3>

<p>s.9 is also relevant</p>

<p>The procedure for a reference under s.8 is prescribed by Part 7 of the Patents Rules 2007 (<a href="/guidance/manual-of-patent-practice-mopp/section-123-rules">see 123.05 – 123.05.13</a>). Certain effects of orders or directions given under s. 8 are laid down by s.11. Where an order is made under s.8 (or s.12) that a patent application should proceed in name(s) none of whom was an original applicant, or that a new application for a patent may be made, the Office notifies all original applicants and their licensees of whom it is aware of the making of the order.</p>

<h3 id="ref8-03-1">8.03.1</h3>

<p>s.25(1) is also relevant</p>

<p>If a question referred to the comptroller under s.8 has not been determined by the time the patent in question is granted, the question shall, in accordance with s.9, then be treated as having been referred under s.37. For the purpose of section 8 (as with all of sections 1-24), the date of grant is the date on which the grant letter is issued informing the applicant under s.18(4) that the application complies with the requirements of the Act and Rules and that the patent is therefore granted. However, for the purpose of s.37, the effective date of grant is the date on which the notice of grant appears in the Journal and the patent is published, and so further action under s.37 on an existing reference cannot take place until after this date. If a question relating to entitlement is referred to the comptroller between the date on which the grant letter is issued and the date on which the notice of grant appears in the Journal, then no immediate action can take place under either of sections 8 or 37. Instead, the question is treated as having been referred under s.37 on the date on which the notice of grant appears in the Journal.</p>

<h3 id="section-2">8.04</h3>

<p>Some guidance with regard to the determination of questions of entitlement is given by the judgments and decisions referred to below, some of which were under s.8, but others of which were under s.12 or s.37. The considerations applying to such questions under s.8, s.12 or s.37 are essentially the same, subject to the effects of the relevant foreign or international law in the case of a s.12 reference.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 8(1)</strong></td>
    </tr>
    <tr>
      <td>At any time before a patent has been granted for an invention (whether or not an application has been made for it) -<br/>(a) any person may refer to the comptroller the question whether he is entitled to be granted (alone or with any other persons) a patent for that invention or has or would have any right in or under any patent so granted or any application for such a patent; <br/>or<br/>(b) any of two or more co-proprietors of an application for a patent for that invention may so refer the question whether any right in or under the application should be transferred or granted to any other person;<br/>and the comptroller shall determine the question and may make such order as he thinks fit to give effect to the determination.</td>
    </tr>
  </tbody>
</table>

<h3 id="ref8-05">8.05</h3>

<p>s.8(7) is also relevant</p>

<p>A question about entitlement may be referred to the comptroller under s.8 at any time before a patent has been granted for an invention, even before the making of an application. Such a reference may be made by any person claiming a right in any application or resultant patent for that invention, in accordance with subsection (1)(a), see <a href="#ref8-12">8.12</a> to <a href="#ref8-16">8.16</a>, or by a co-owner of an application contending that a right therein should be transferred or granted to any other person, in accordance with subsection (1)(b), <a href="#ref8-20">see 8.20</a>. The comptroller normally in due course determines the question but they may instead decline to deal with it, see <a href="#ref8-28">8.28</a> - <a href="#ref8-30">8.30</a>.</p>

<p>[ The action officer in Tribunal Section should check at each stage of the proceedings whether or not a patent has been granted on the application. If and when a patent is granted, action should be initiated under s.9 (<a href="/guidance/manual-of-patent-practice-mopp/section-9-determination-after-grant-of-questions-referred-before-grant/#ref9-04">see 9.04</a>). ]</p>

<h3 id="ref8-05-1">8.05.1</h3>

<p>In line with the preliminary decision in Brooks and Cope’s Application (<a href="https://www.gov.uk/government/publications/patent-decision-o07193">BL O/71/93</a>), in general the preliminary examination, search, publication under s.16 and substantive examination of a patent application should not be deferred pending the determination of a reference under s.8. Similarly, grant of the patent in general should not be deferred if the patent is in order for grant before the determination of a reference under s.8; the claimant is then treated as having made the reference under s.37 (<a href="/guidance/manual-of-patent-practice-mopp/section-9-determination-after-grant-of-questions-referred-before-grant/#ref9-03">see 9.03</a>).</p>

<h3 id="ref8-06">8.06</h3>

<p>The law on entitlement was considered by the House of Lords in <a rel="external" href="https://www.bailii.org/uk/cases/UKHL/2007/43.html">Yeda Research and Development Company Limited (Appellants) v. Rhone-Poulenc Rorer International Holdings Inc and others (Respondents) [2007] UKHL 43</a>. Lord Hoffmann giving the leading judgement noted:</p>

<blockquote>

  <p>Section 7(2), and the definition in section 7(3), are in my opinion an exhaustive code for determining who is entitled to the grant of a patent. That is made clear by the words “and to no other person.” In saying that the patent may be granted “primarily” to the inventor, section 7(2) emphasises that a patent may be granted only to the inventor or someone claiming through him. The claim through an inventor may be made under one of the rules mentioned in paragraph (b), by which someone may be entitled to patent an invention which has been made by someone else (the right of an employer under section 39 is the most obvious example) or the claim may be made under paragraph (c) as successor in title to an inventor or to someone entitled under paragraph (b).</p>

  <p>In my opinion, therefore, the first step in any dispute over entitlement must be to decide who was the inventor or inventors of the claimed invention. Only when that question has been decided can one consider whether someone else may be entitled under paragraphs (b) or (c).</p>

  <p>The inventor is defined in section 7(3) as “the actual deviser of the invention”. The word “actual” denotes a contrast with a deemed or pretended deviser of the invention; it means, as Laddie J said in <a rel="external" href="https://doi.org/10.1093/rpc/2005rpc11">University of Southampton’s Applications [2005] RPC 11</a> (at page 234), the natural person who “came up with the inventive concept.” It is not enough that someone contributed to the claims, because they may include non-patentable integers derived from prior art: see <a rel="external" href="https://doi.org/10.1093/rpc/1997rpc693">Henry Brothers (Magherafelt) Ltd v Ministry of Defence [1997] RPC 693</a> (at page 706); <a rel="external" href="https://doi.org/10.1093/rpc/1999rpc442">[1999] RPC 442</a>. As Laddie J said in the University of Southampton case, the “contribution must be to the formulation of the inventive concept”. Deciding upon inventorship will therefore involve assessing the evidence adduced by the parties as to the nature of the inventive concept and who contributed to it. In some cases this may be quite complex because the inventive concept is a relationship of discontinuity between the claimed invention and the prior art. Inventors themselves will often not know exactly where it lies.</p>

  <p class="last-child">The effect of section 7(4) is that a person who seeks to be added as a joint inventor bears the burden of proving that he contributed to the inventive concept underlying the claimed invention and a person who seeks to be substituted as sole inventor bears the additional burden of proving that the inventor named in the patent did not contribute to the inventive concept. But that, in my opinion, is all. The statute is the code for determining entitlement and there is nothing in the statute which says that entitlement depends upon anything other than being the inventor.</p>

</blockquote>

<h3 id="deleted">8.07 [deleted]</h3>

<h3 id="deleted-1">8.07.1 [deleted]</h3>

<h3 id="deleted-2">8.08 [deleted]</h3>

<h3 id="deleted-3">8.08.1 [deleted]</h3>

<h3 id="ref8-08-2">8.08.2</h3>

<p>Where a dispute concerns a claim under section 7(2)(b) involving a contract, then the issues often centre on implied, rather than express, terms. For example, in <a rel="external" href="https://doi.org/10.1093/rpc/1996rpc141">Goddin and Rennie’s Application [1996] RPC 141</a> the claimant had shown prototype fish tank covers to the defendant in confidence. The claimant subsequently entered into a contractual agreement with the defendant for him to make improved net covers for use with an arched frame devised by the claimant. On appeal to the Court of Session it was held that the claimant was entitled to the benefit of the improved net covers. It would not have made business sense if there were an implied term of the contractual relationship between the parties that any improved design which was worked out by the defendant was the property of the defendant rather than the claimant. On the other hand the Court held that the defendant was entitled to the benefit of claimed improvements suggested by him prior to and separately from the contractual arrangement. The Court therefore ordered that the patent should be granted in the name of the claimant alone subject to the condition that he granted to the defendant an irrevocable exclusive licence with power to sub-licence under the claims for the non-contracted improvements.</p>

<h3 id="ref8-09">8.09</h3>

<p>Although section 74 does not allow validity to be put in issue in inventorship or entitlement disputes, where an unanswerable case of validity was raised, the Court of Appeal in <a rel="external" href="https://doi.org/10.1093/rpc/2005rpc31">Markem Corp v Zipher Ltd [2005] RPC 31</a> held that the Comptroller can act upon it. Jacob LJ said: “If a patent or part of it is clearly and unarguably invalid, then we see no reason why as a matter of convenience, the Comptroller should not take it into account in exercising his wide discretion. The sooner an obviously invalid monopoly is removed, the better from the public point of view. But we emphasise that the attack on validity should be clear and unarguable. Only when there is self-evidently no bone should the dogs be prevented from fighting over it”. In this case, the claimants alleged that a claim was invalid, but that this should be considered irrelevant to their entitlement claim. The court disagreed, saying that there is simply no point in the Comptroller handing rights in an invalid monopoly from one side to another, and concluded that if an inherent part of a claim to entitlement is also an assertion of or acceptance of invalidity, the entitlement claim must fail. The hearing officer in Statoil ASA v University of Southampton (<a rel="external" href="https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl?BL_Number=O/204/05">BL O/204/05</a>) held that this principle should apply to both parties, so that a defendant should not be allowed to get away with pleading invalidity as an inherent part of their defence.</p>

<h3 id="section-3">8.10-8.11</h3>

<p>[moved to 8.21]</p>

<h3 id="procedure">Procedure</h3>

<h3 id="ref8-12">8.12</h3>

<p>PR part 7 is also relevant</p>

<p>A reference under s.8(1) (or s.12(1) or 37(1)) should be made on Patents Form 2 accompanied by a copy thereof and a statement of grounds in duplicate. This starts proceedings before the comptroller, the procedure for which is discussed at <a href="/guidance/manual-of-patent-practice-mopp/section-123-rules/#ref123-05">123.05 – 123.05.13</a>.</p>

<h3 id="reference-by-person-other-than-co-owner-s81a">Reference by person other than co-owner (s.8(1)(a))</h3>

<h3 id="section-4">8.13</h3>

<p>[deleted]</p>

<h3 id="section-5">8.14</h3>

<p>[deleted]</p>

<h3 id="section-6">8.15</h3>

<p>[deleted]</p>

<h3 id="ref8-16">8.16</h3>

<p>In proceedings under s.8(1)(a), the hearing officer may where appropriate grant any of the orders or reliefs set out in s.8(2) and (3), see <a href="#ref8-21">8.21</a> to <a href="#ref8-25-1">8.25.1</a>. This is without prejudice to the general power to make orders under s.8(1), see <a href="#ref8-21">8.21.1</a>.</p>

<h3 id="reference-by-co-owner-s81b">Reference by co-owner (s.8(1)(b))</h3>

<h3 id="section-7">8.17</h3>

<p>[deleted]</p>

<h3 id="section-8">8.18</h3>

<p>[deleted]</p>

<h3 id="section-9">8.19</h3>

<p>[deleted]</p>

<h3 id="ref8-20">8.20</h3>

<p>s.8(4) is also relevant</p>

<p>In proceedings under s.8(1)(b), any order made by the hearing officer under s.8(1) may contain directions to any person for transferring or granting any right in or under the patent application in question (see also <a href="#ref8-26">8.26</a> where such directions are not complied with).</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 8(2)</strong></td>
    </tr>
    <tr>
      <td>Where a person refers a question relating to an invention under subsection (1)(a) above to the comptroller after an application for a patent for the invention has been filed and before a patent is granted in pursuance of the application, then, unless the application is refused or withdrawn before the reference is disposed of by the comptroller, the comptroller may, without prejudice to the generality of subsection (1) above and subject to subsection (6) below –<br/>(a) order that the application shall proceed in the name of that person, either solely or jointly with that of any other applicant, instead of in the name of the applicant or any specified applicant;<br/>(b) where the reference was made by two or more persons, order that the application shall proceed in all their names jointly;<br/>(c) refuse to grant a patent in pursuance of the application or order the application to be amended so as to exclude any of the matter in respect of which the question was referred;<br/>(d) make an order transferring or granting any licence or other right in or under the application and give directions to any person for carrying out the provisions of any such order.</td>
    </tr>
  </tbody>
</table>

<h3 id="specific-remedies-available">Specific remedies available</h3>

<h3 id="ref8-21">8.21</h3>

<p>The remedies available under s.8(1)(a), provided that the patent application in question has not been granted, refused or withdrawn, thus include orders under subsections (2)(a), (b) or (d) with regard to the name(s) in which the application or any licence or other right therein should proceed or be vested. However, where there is alleged to be a transaction, instrument or event as set out in s.8(6), no such order is made unless notice of the s.8 reference has been given to the affected parties as required by subsection (6) (such notice normally having been given by the Office). See also <a href="#ref8-26">8.26</a> if directions for putting into effect an order under subsection (2)(d) are not complied with.</p>

<h3 id="ref8-21-1">8.21.1</h3>

<p>Section 8(1) provides that the comptroller may make such order as they think fit to give effect to their determination of an entitlement question. In Szucs’ Application (<a href="https://www.gov.uk/government/publications/patent-decision-o00486">BL O/4/86</a>) where relief under s.8(2) or (3) was not available under s.8(1), the hearing officer held that the claimants were entitled to a declaration that certain matter (a beam of particular cross-sectional form) was their property.</p>

<h3 id="ref8-21-2">8.21.2</h3>

<p>In <a rel="external" href="https://doi.org/10.1093/rpc/1984rpc467">Georgia Pacific Corp’s Application [1984] RPC 467</a>, the applicants claimed that their application should be deemed to have as its priority date one of the priority dates of an earlier application which they alleged was made in contravention of their rights. They also sought revocation of the patent resulting from the earlier application. The hearing officer held that there was no provision for an application to be accorded an earlier priority date unless it was claimed in a declaration of priority at the time of filing of the application; and that s.37 did not provide any facility whereby the application in suit could be deemed to have a more favourable priority date, whatever the outcome of the revocation action. (The same is apparently true of s.8).</p>

<h3 id="section-10">8.22</h3>

<p>Other remedies in s.8(1)(a) proceedings include refusal of the application or an order for amendment of the application to exclude any of the disputed matter, under subsection (2)(c).</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 8(3)</strong></td>
    </tr>
    <tr>
      <td>Where a question is referred to the comptroller under subsection (1)(a) above and -<br/>(a) the comptroller orders an application for a patent for the invention to which the question relates to be so amended;<br/>(b) any such application is refused under subsection 2(c) above before the comptroller has disposed of the reference (whether the reference was made before or after the publication of the application); or<br/>(c) any such application is refused under any other provision of this Act or is withdrawn before the comptroller has disposed of the reference, (whether the application is refused or withdrawn before or after its publication);<br/>the comptroller may order that any person by whom the reference was made may within the prescribed period make a new application for a patent for the whole or part of any matter comprised in the earlier application or, as the case may be, for all or any of the matter excluded from the earlier application, subject in either case to section 76 below, and in either case that, if such a new application is made, it shall be treated as having been filed on the date of filing the earlier application.</td>
    </tr>
  </tbody>
</table>

<h3 id="making-of-new-application">Making of new application</h3>

<h3 id="ref8-23">8.23</h3>

<p>Subsection (3) provides, at the discretion of the comptroller, for the making of a new application by the claimant under s.8(1)(a) when the original application is no longer proceeding or (as a result of amendment ordered under s.8(2)(c)) no longer contains the matter to which the claimant is held to be entitled, with the new application being treated as having been filed on the filing date of the original application. If no longer proceeding, the original application must have been refused under s.8(2)(c), refused under any other provision of the Act or withdrawn before the comptroller has disposed of the reference. In Stevens’ Application (<a href="https://www.gov.uk/government/publications/patent-decision-o06393">BL O/63/93</a>) the hearing officer held that where a claimant was seeking an order under s.8(3)(c) allowing him to file a fresh application in respect of matter comprised in an earlier application, it was incumbent on him clearly to identify the subject matter of which he was claiming ownership. It was not sufficient for the claimant merely to state that there was subject matter somewhere in the specification to which he was entitled and which could form the basis for a fresh application. Amendment of the register as to ownership of a withdrawn application was allowed by the hearing officer in Shape and Potemkin’s Applications (BL O/140/92) so that the claimant could claim priority in a new application made under s.8(3)(c).</p>

<h3 id="ref8-24">8.24</h3>

<p>r.20(1), r.20(3), r.20(4), r.108(1) is also relevant</p>

<p>The new application should be made within a period of three months calculated from the day on which the order was made under s.8(3) or, where an appeal is brought, from the day on which it is finally disposed of. This period may be extended or shortened at the discretion of the comptroller.</p>

<h3 id="ref8-25">8.25</h3>

<p>The new application under s.8(3), 12 or 37(4) is treated as having been filed on the date of filing of the earlier application. If matter extending beyond that disclosed in the earlier application or patent is disclosed, s.76(1) requires that such an application shall not be allowed to proceed unless it is amended so as to exclude the additional matter, and the examiner should make an objection (much as in the case of an attempted application under s.15(9), <a href="/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-35">see 15.35</a> and <a href="/guidance/manual-of-patent-practice-mopp/section-15-date-of-filing-application/#ref15-45">15.45</a>.</p>

<h3 id="ref8-25-1">8.25.1</h3>

<p>In the case of new applications filed under s.8(3),12(6) or 37(4) as the result of entitlement proceedings, the compliance period for putting the application in order is prescribed by r.30(3).</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 8(4)</strong></td>
    </tr>
    <tr>
      <td>Where a person refers a question under subsection (1)(b) above relating to an application, any order under subsection (1) above may contain directions to any person for transferring or granting any right in or under the application</td>
    </tr>
  </tbody>
</table>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 8(5)</strong></td>
    </tr>
    <tr>
      <td>If any person to whom directions have been given under subsection (2)(d) or (4) above fails to do anything necessary for carrying out any such directions within 14 days after the date of the directions, the comptroller may, on application made to him by any person in whose favour or on whose reference the directions were given, authorise him to do that thing on behalf of the person to whom the directions were given.</td>
    </tr>
  </tbody>
</table>

<h3 id="directions-not-complied-with">Directions not complied with</h3>

<h3 id="ref8-26">8.26</h3>

<p>An application under s.8(5) for authority to do anything on behalf of a person to whom directions have been given under s.8(2)(d) or (4) is not included in Schedule 3 to the Patents Rules 2007. Patents Form 2 need not therefore be filed in order to start an application under s.8(5); the application should however set out fully the facts upon which the applicant relies and the nature of the authorisation sought. Such an application may be made by any person in whose favour or on whose reference the directions were given.</p>

<h3 id="section-11">8.27</h3>
<p>The Office sends a copy of the application to the person alleged to have failed to comply with the directions. The comptroller may give such directions as they may think fit with regard to the subsequent procedure, and may grant authorisation if they think fit.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 8(6)</strong></td>
    </tr>
    <tr>
      <td>Where on a reference under this section it is alleged that, by virtue of any transaction, instrument or event relating to an invention or an application for a patent, any person other than the inventor or the applicant for the patent has become entitled to be granted (whether alone or with any other persons) a patent for the invention or has or would have any right in or under any patent so granted or any application for any such patent, an order shall not be made under subsection (2)(a), (b) or (d) above on the reference unless notice of the reference is given to the applicant and any such person, except any of them who is a party to the reference.</td>
    </tr>
  </tbody>
</table>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 8(7)</strong></td>
    </tr>
    <tr>
      <td>If it appears to the comptroller on a reference of a question under this section that the question involves matters which would more properly be determined by the court, he may decline to deal with it and, without prejudice to the court’s jurisdiction to determine any such question and make a declaration, or any declaratory jurisdiction of the court in Scotland, the court shall have jurisdiction to do so.</td>
    </tr>
  </tbody>
</table>

<h3 id="comptroller-declines-to-deal-with-question">Comptroller declines to deal with question</h3>

<h3 id="ref8-28">8.28</h3>

<p>CPR 63.11 is also relevant</p>

<p>The comptroller has discretion under <a href="https://www.gov.uk/government/publications/the-patents-act-1977">Patents Act 1977 s.8(7) refers</a> (and similarly under s.12(2) and s.37(8)) to decline to deal with a question if it appears to them that it involves matters which “would more properly be determined by the court”. In such a case any person entitled to do must, within 14 days of the comptroller’s decision, issue a claim form to the court to determine the question.</p>

<h3 id="ref8-29">8.29</h3>

<p>There is no corresponding provision for the comptroller to transfer s.13 inventorship proceedings to the court. However, if the comptroller declines to deal with the entitlement reference, the comptroller can order a stay in the s.13 proceedings pending decision by the court, as occurred in BioProgress Technology Limited v Stanelco Fibre Optics Limited (<a rel="external" href="https://www.ipo.gov.uk/p-challenge-decision-results/p-challenge-decision-results-bl.htm?BL_Number=O%2F351%2F03&amp;submit=Go+%BB">BL O/351/03</a>).</p>

<h3 id="ref8-30">8.30</h3>

<p>The questions to be considered by the comptroller in declining to deal with entitlement cases under sections 8 and 12 of the Patents Act 1977 were considered in <a rel="external" href="https://www.bailii.org/ew/cases/EWHC/Ch/2007/1624.html">Luxim Corp v Ceravision Ltd [2007] EWHC 1624 (Ch)</a>. Whereas previous practice had been to consider declining to deal only where one or both of the parties requested it, following Luxim, it is necessary for hearing officers to consider the matter in all cases. Furthermore in some cases where both parties request it, the hearing officer may decide that it is nevertheless proper for the matter to be determined by the comptroller. The most common reason for a request is that there are parallel High Court proceedings covering much the same issues, and it would be undesirable for the both the court and the comptroller to be deciding the same issues. However, other arguments may be advanced and the hearing officer will have to decide where the balance lies. If they do decline to deal then the court can exercise all the powers given to the comptroller by section 8, which it could not do if the parties had simply gone first to the court and launched proceedings for a declaration as to entitlement.</p>

<h3 id="section-12">8.31</h3>

<p>[deleted]</p>

<p>[Further guidance on declining to deal is given in chapter 2 of the Patents Hearings Manual.]</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 8(8)</strong></td>
    </tr>
    <tr>
      <td>No directions shall be given under this section so as to affect the mutual rights or obligations of trustees or of the personal representatives of deceased persons, or their rights or obligations as such.</td>
    </tr>
  </tbody>
</table>

</div>


        
  </div>

        </article>
      </div>
    </div>

    
<div class="gem-c-print-link govuk-!-display-none-print govuk-!-margin-top-3 govuk-!-margin-bottom-3">
    <button class="govuk-link govuk-body-s gem-c-print-link__button" data-module="print-link">Print this page</button>
</div>
  </div>

    </main>
