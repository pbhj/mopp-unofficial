<main role="main" id="content" class="manual-section" lang="en">
      <span id="Top"/>
        <div id="manuals-frontend" class="manuals-frontend-body">
      

    <div class="govuk-grid-row">
      <div class="manual-body">
        <article aria-labelledby="section-title">
          <div class="govuk-grid-column-full">
            <h1 id="section-title" class="gem-c-heading govuk-heading-l   govuk-!-margin-bottom-4">
  Section 63: Relief for infringement of partially valid patent 
</h1>
          </div>

            <div class="govuk-grid-column-two-thirds">
                <p class="gem-c-lead-paragraph">Sections (63.01 - 63.08) last updated July 2021.</p>

            </div>

          
  <div class="govuk-grid-column-two-thirds">
      
<div class="gem-c-govspeak govuk-govspeak " data-module="govspeak">
    <h3 id="section">63.01</h3>

<p>This section allows the court or the comptroller to grant relief for infringement of a patent found to be partially valid, but restricts the circumstances in which certain kinds of relief may be granted for such infringement. It also provides that amendment under s.75 may be made a condition of relief.</p>

<h3 id="section-1">63.02</h3>

<p>Section 63 is applicable only in relation to granted patents and not to applications for patents. For its applicability in relation to European patents, <a href="/guidance/manual-of-patent-practice-mopp/section-60-meaning-of-infringement/#ref60-02">see 60.02</a>.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 63(1)</strong></td>
    </tr>
    <tr>
      <td>If the validity of a patent is put in issue in proceedings for infringement of the patent and it is found that the patent is only partially valid, the court or the comptroller may, subject to subsection (2) below, grant relief in respect of that part of the patent which is found to be valid and infringed.</td>
    </tr>
  </tbody>
</table>

<h3 id="ref63-03">63.03</h3>

<p>The court or the comptroller may find in infringement proceedings where the validity of a patent is questioned that the patent is partially invalid but partially valid and infringed. In other words, although some claims may be invalid, the remaining claims can nevertheless be found to be valid and infringed. Relief may be granted in respect of these valid claims, taking into account the factors in s.63(2) (<a href="#ref63-04">see 63.04</a>).</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 63(2)</strong></td>
    </tr>
    <tr>
      <td>Where in any such proceedings it is found that a patent is only partially valid, the court or the comptroller shall, when awarding damages, costs or expenses or making an order for an account of profits, take into account the following -<br/><br/> (a) whether at the date of the infringement the defendant or defender knew, or had reasonable grounds to know, that he was infringing the patent;<br/><br/> (b) whether the specification of the patent was framed in good faith and with reasonable skill and knowledge;<br/><br/> (c) whether the proceedings are brought in good faith; and any relief granted shall be subject to the discretion of the court or the comptroller as to costs or expenses and as to the date from which damages or an account should be reckoned.</td>
    </tr>
  </tbody>
</table>

<h3 id="ref63-04">63.04</h3>

<p>Section 63(2) therefore applies to infringement proceedings where the patent is found to be partially valid and infringed. When awarding damages, costs or expenses or making an order for an account of profits in such circumstances, the court or comptroller is required to take into account the various factors set out in paragraphs (a) to (c) of that provision. These factors were modified and expanded upon by the Intellectual Property (Enforcement, etc.) Regulations 2006 (S.I. 2006 No. 1028), which came into force on 29 April 2006, in order to implement Directive 2004/48/EC on the enforcement of intellectual property rights. (See also paragraphs <a href="/guidance/manual-of-patent-practice-mopp/section-62-restrictions-on-recovery-of-damages-for-infringement/#ref62-06">62.06 to 62.08</a>). In SmithKline Beecham Plc v Apotex Europe Ltd (No.2) [2005] FSR 24, the Court of Appeal held that s.63(2) only applied when a finding had been made in proceedings that the patent was partially invalid; this involved determination of a dispute about the point. If the court acts on a concession by the patentee, e.g. that certain claims are invalid and should be deleted, that is not making a finding and s.63(2) should not apply.</p>

<h3 id="ref63-05">63.05</h3>

<p>Paragraph (a) requires the court or comptroller to take account of the defendant’s actual or constructive knowledge that they were infringing the patent. This reflects the requirement in article 13.1 of the Directive that, in such circumstances, the proprietor is entitled to damages appropriate to the actual prejudice suffered. For this reason, the absolute bar on damages, costs or expenses that was imposed in section 63(2) previously, in the circumstances where the specification was not framed in good faith and with reasonable skill and knowledge, was not compatible with the Directive. Thus no absolute bar on damages, costs or expenses is maintained, but paragraph (b) requires account to be taken of the framing of the specification when determining what level of award is appropriate. (<a href="/guidance/manual-of-patent-practice-mopp/section-62-restrictions-on-recovery-of-damages-for-infringement/#ref62-08">See 62.08 to 62.13</a> for guidance on interpretation of the phrase “framed in good faith and with reasonable skill and knowledge”).</p>

<h3 id="ref63-06">63.06</h3>

<p>Paragraph (c) requires the court or comptroller to take into account whether the infringement proceedings are brought in good faith. Therefore, where it is shown that the proprietor knew that the infringed patent was only partially valid, the court or comptroller can take that matter into account when determining the appropriate level of damages, costs or expenses.</p>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 63(3)</strong></td>
    </tr>
    <tr>
      <td>As a condition of relief under this section the court or the comptroller may direct that the specification of the patent shall be amended to its or his satisfaction upon an application made for that purpose under section 75 below, and an application may be so made accordingly, whether or not all other issues in the proceedings have been determined.</td>
    </tr>
  </tbody>
</table>

<h3 id="section-2">63.07</h3>

<p>The grant of relief may be made subject to the condition that the specification of the partially valid patent should be amended under s.75 to the satisfaction of the court or the comptroller. An application under s.75 for that purpose in response to a direction of the court or the comptroller imposing such a condition may be made regardless of whether or not other issues remain to be determined. The procedure followed should be generally similar to that for amendment of a patent found to be partially invalid in a revocation action, where a similar condition can be imposed under s.72(4) (<a href="/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-43">see 72.43</a>, <a href="/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-44">72.44</a> and the chapter on s.75).</p>

<h3 id="section-634">Section 63(4)</h3>

<table>
  <tbody>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><strong>Section 63(4)</strong></td>
    </tr>
    <tr>
      <td>The court or the comptroller may also grant relief under this section in the case of a European patent (UK) on condition that the claims of the patent are limited to its or his satisfaction by the European Patent Office at the request of the proprietor.</td>
    </tr>
  </tbody>
</table>

<h3 id="section-3">63.07</h3>

<p>The grant of relief may be made subject to the condition that the specification of the partially valid patent should be amended under s.75 to the satisfaction of the court or the comptroller. An application under s.75 for that purpose in response to a direction of the court or the comptroller imposing such a condition may be made regardless of whether or not other issues remain to be determined. The procedure followed should be generally similar to that for amendment of a patent found to be partially invalid in a revocation action, where a similar condition can be imposed under s.72(4) (<a href="/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-43">see 72.43</a>, <a href="/guidance/manual-of-patent-practice-mopp/section-72-power-to-revoke-patents-on-application/#ref72-44">72.44</a> and the chapter on s.75).</p>

<h3 id="section-4">63.08</h3>

<p>Article 138(3) EPC provides a central amendment process for European Patents. This is an alternative to the existing possibility of the proprietor amending the patent under the 1977 Act. In the former case, the amendments are effective in each Contracting State designated by the patent whereas the latter would only affect the European patent (UK). This section provides that relief may be granted on the condition that the proprietor of a European patent (UK) limits the patent at the EPO. The limitation would have to be done to the satisfaction of the court or comptroller for relief to be granted.</p>


</div>


        
  </div>

        </article>
      </div>
    </div>

    
<div class="gem-c-print-link govuk-!-display-none-print govuk-!-margin-top-3 govuk-!-margin-bottom-3">
    <button class="govuk-link govuk-body-s gem-c-print-link__button" data-module="print-link">Print this page</button>
</div>
  </div>

    </main>
